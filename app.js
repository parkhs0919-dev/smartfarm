const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const userAgent = require('koa-useragent');
const CONFIG = require('./server/config');
const render = require('./server/methods/render');
const serve = require('./server/methods/serve');
const initDirectories = require('./server/methods/initialize').initDirectories;
const initDatabase = require('./server/methods/initialize').initDatabase;
const initLogic = require('./server/methods/initialize').initLogic;
const initAlter = require('./server/methods/initialize').initAlter;
const sequelize = require('./server/methods/sequelize');
const models = require('./server/models');
const socketIo = require('./server/methods/socket-io');
const control = require('./server/methods/control');
const pestControl = require('./server/methods/pest-control');
const cctv = require('./server/methods/cctv');
const sensor = require('./server/methods/sensor');
const serialPort = require('./server/methods/serial-port');
const alter = require('./server/methods/alter');
const cron = require('./server/cron');

const Log = require('./server/middleware/log');
const Api = require('./server/middleware/api');
const Ip = require('./server/middleware/ip');
const Language = require('./server/middleware/language');
const Validator = require('./server/middleware/validator');
const Sanitize = require('./server/middleware/sanitize');
const Model = require('./server/middleware/model');
const Meta = require('./server/middleware/meta');
const Util = require('./server/middleware/util');
const Verify = require('./server/middleware/verify');
const Response = require('./server/middleware/response');

const routes = require('./routes');

alter().then(() => {
    sequelize.defineAll(models);
    sequelize.sequelize.sync({
        force: false
    }).then(async () => {
        const app = new Koa();

        // middleware
        app.use(bodyParser({
            enableTypes: ['json', 'form', 'text'],
        }));
        app.use(userAgent);
        app.use(Log);
        app.use(Ip);
        app.use(Language);
        app.use(Validator);
        app.use(Sanitize);
        app.use(Meta);
        app.use(Util);
        app.use(Verify);
        app.use(Response);
        app.use(Model);
        app.use(Api);

        // methods
        serve(app);
        render(app);
        await initDirectories();
        await initDatabase();
        await initAlter();

        // router
        routes(app);

        const server = app.listen(CONFIG.port, () => {
            const host = server.address().address;
            const port = server.address().port;
            console.log(`Server listening at ${host}:${port}`);
        });
        socketIo(server);

        serialPort();

        return Promise.all([
            control(),
            // cctv(),
            sensor(),
            pestControl()
        ]);
    }).then(async () => {
        await cron();
        // return initLogic();
        return true;
    }).catch((e) => {
        console.error(e);
    });
});
