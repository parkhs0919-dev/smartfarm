const fs = require('fs');
const path = require('path');
const multer = require('koa-multer');
const crypto = require('crypto');

const FILE = require('../config').file;

const storage = multer.diskStorage({
    destination: `${FILE.dest}`,
    filename: (req, file, cb) => {
        crypto.pseudoRandomBytes(16, (err, raw) => {
            if (err) return cb(err);
            cb(null, `${FILE.dest}_${raw.toString('hex')}${path.extname(file.originalname)}`);
        });
    }
});

module.exports.store = (file = {}) => {
    const upload = multer(Object.assign(Object.assign({
        storage: storage
    }, FILE), file));

    return upload.any();
};

module.exports.parse = () => async (ctx, next) => {
    if (ctx.req.files) {
        ctx.request.files = ctx.req.files;
    } else {
        ctx.request.files = [];
    }
    if (ctx.req.body) {
        ctx.request.body = ctx.req.body;
    } else {
        ctx.request.body = {};
    }

    await next();
};

module.exports.deleteAll = async (ctx) => {
    if (ctx.request.files && ctx.request.files.length) {
        ctx.request.files.forEach(file => {
            fs.unlinkSync(file.path);
        });
    }
    return true;
};

module.exports.moveAll = async (ctx, dir) => {
    if (ctx.request.files && ctx.request.files.length) {
        ctx.request.files.forEach(file => {
            let path = file.path.split('/');
            path.splice(1, 0, dir);
            fs.renameSync(file.path, path.join('/'));
        });
    }
    return true;
};
