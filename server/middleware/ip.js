module.exports = async (ctx, next) => {
    const ips = ctx.ip.split(':');
    ctx.request.ip = ips[ips.length - 1];

    await next();
};
