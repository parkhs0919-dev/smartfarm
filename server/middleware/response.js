const CODE = require('../metadata/codes');

module.exports = async (ctx, next) => {

    ctx.json = async (data, status = 200, m) => {
        ctx.status = status;
        ctx.body = {
            "code": "0000",
            "data": null,
            "message": null
        };
        if (data) ctx.body['data'] = data;
        if (m) ctx.body['message'] = m;
    };

    ctx.error = async (code, status = 400) => {
        let key;
        if (code && code.indexOf(':') !== -1) {
            let temp = code.split(':');
            key = temp[1];
            code = temp[temp.length - 1];
        }
        ctx.status = status;
        ctx.body = {
            "code": code,
            "data": null,
            "message": null
        };
        if (CODE[ctx.language] && CODE[ctx.language][code]) {
            let m = CODE[ctx.language][code];
            if (key) {
                m = m.replace(':key:', key);
            }
            ctx.body['message'] = m;
        }
    };

    ctx.portable = async (data, status = 200, m) => {
        ctx.status = status;
        ctx.body = data; 
    };
   
    await next();
};
