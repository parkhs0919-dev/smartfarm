const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const CONFIG = require('../config');
const jwt = require('../utils/jwt');

module.exports = async (ctx, next) => {
    const tokenInfo = jwt.parseToken(ctx);

    if (tokenInfo) {
        try {  
            const payload = await jwt.verifyAccessToken(tokenInfo.token);
            /**
             * todo database 조회 로직 추가
             */
            if (tokenInfo.from === 'query') {
                await sequelize.models.LoginHistory.upsertLoginHistory(payload, tokenInfo.token);
                ctx.user = payload;
                
            } else if (tokenInfo.from === 'cookie') {
                if (payload.is_control || await sequelize.models.LoginHistory.getLoginHistory(payload, tokenInfo.token)) {
                    ctx.user = payload;
                }
            }
            
        } catch (e) {
            // token expired or verify failed
        }
    }

    if (!ctx.user) {  
        const current_ip = ctx.ips.length > 0 ? ctx.ips[ctx.ips.length - 1] : ctx.ip;
        if (current_ip.indexOf('192.168') !== -1) { 
            ctx.user = Object.assign({}, CONFIG.defaultUser);
        }  
    }   

    if(ctx.user && (ctx.user.grade==undefined || ctx.user.grade=='none' || ctx.user.grade=='member' )){  
        //ctx.user.id ='341';  
        if(ctx.user.id!=='LOCAL') {        
            const userData = await sequelize.models.User.getUserGrade(ctx.user.id);
            if(userData) {
                ctx.user.grade =userData.grade;
                //ctx.user.uid =userData.id;
                //ctx.user.name =userData.name;
            }else{
                if(ctx.user.grade != 'member'||ctx.user.grade ==undefined) ctx.user.grade = 'owner';
                else ctx.user='';   
                
            }       
        }else{  
            ctx.user.grade = 'owner'; 
        }      

    }        
    if(ctx.user) ctx.user.com_type = (CONFIG.farm.com_type)? CONFIG.farm.com_type : 'farmlabs' ;      
    
    //console.log(ctx.user);
    await next();      
};
      