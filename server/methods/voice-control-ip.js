const tcpp = require('tcp-ping');
const request = require('request');
const Sequelize = require('sequelize');
const sequelize = require('./sequelize').sequelize;

let controls = {};

module.exports.sendSignal = (user_id, control, state, callback) => {
    if ((control.address && control.io1 && control.io2 && (control.type === 'motor' ? control.io3 : true)) ||
        (control.address && control.io1 && (control.type === 'motor' ? control.io2 : true))) {
        const address = control.address.split(':');
        if (address.length === 2) {  
            const ip = address[0];
            const port = address[1];
            tcpp.probe(ip, port, (err, available) => {
                if (err) {
                    sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${ip}:${port}' 연결에 실패했습니다.`);
                    callback(false);
                } else {
                    if (available) {
                        send(user_id, control, state, ip, port, callback);  
                    } else {
                        sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${ip}:${port}' 사용 불가능합니다.`);
                        callback(false);
                    }   
                } 
            });  
        } else {
            sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.id}'에 할당된 정보가 잘못됐습니다.`);
            callback(false);
        }
    } else {
        sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.id}'에 할당된 정보가 없습니다.`);
        callback(false);
    }
};

module.exports.sendSignalTime = async (user_id, control, state,  callback) => {       
    if (control.address && control.io1 ) {  
        const address = control.address.split(':');    
        if (address.length === 2) {  
            const ip = address[0];
            const port = address[1];
            tcpp.probe(ip, port, (err, available) => {
                if (err) {
                    sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${ip}:${port}' 연결에 실패했습니다.`);
                    callback(false);
                } else {  
                    if (available) {      
                        sendTime(user_id, control, state, ip, port, callback);      
                    } else {    
                        sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${ip}:${port}' 사용 불가능합니다.`);
                        callback(false);
                    }   
                } 
            });  
        } else {
            sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.id}'에 할당된 정보가 잘못됐습니다.`);
            callback(false);
        }
    } else {
        sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.id}'에 할당된 정보가 없습니다.`);
        callback(false);
    }       
};            
          
module.exports.sendSignalDelay = (time,  callback) => {     
    setTimeout(function() {     
        callback(true);   
    }, time);               
}; 

module.exports.clearTimeoutInstance = async (id,  callback) => { 
    clearTimeoutInstance(controls, id);       
    await clearTimeoutInstance(controls, id);  
    callback(true);       
};             
module.exports.allClearTimeoutInstance = async (  callback) => { 
    await allClearTimeoutInstance() 
    callback(true);    
};               

async function sendTime(user_id, control, state, ip, port, callback) {
    await clearTimeoutInstance(controls, control.id);
    await allClearTimeoutInstance();   
    if(control.time){            
        controls[control.id] = setTimeout(async () => {                                       
            await send(user_id, control, state, ip, port, callback);  
        }, control.time);     
    }else{ 
        await send(user_id, control, state, ip, port, callback);  
    }             
}   

async function clearTimeoutInstance(obj, key) {
    if (obj[key]) {
        await clearTimeout(obj[key]);
        obj[key] = null;     
    }
}   

async function allClearTimeoutInstance() {
    for(key in controls){       
        await clearTimeout(controls[key]);
        controls[key] =null;
    } 
    controls ={};           
}                 
 
function send(user_id, control, state, ip, port, callback) {   
    console.log(generateBody(control, state));
    request.put({           
        uri: `http://${ip}:${port}/remoteio`,   
        method: 'PUT',
        body: generateBody(control, state)      
    }, (error, response, body) => {
        if (error) {
            console.error(error);
            sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.id}' 신호 송신에 실패하였습니다.`);
            callback(false);
        } else {
            if (response.statusCode === 200) { 
                callback(true);       
            } else {
                sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.id}' 신호 응답이 실패하였습니다.`);
                callback(false);
            }
        }
    });
}   

function generateBody(control, state) {
    switch(control.type) {
        case 'motor':
            return JSON.stringify({
                'type': 'motor',
                'mode': returnMode(control.type, state),
                'fwd_io': control.io1,
                'rwd_io': control.io2
            });
        case 'power':
            return JSON.stringify({
                'type': 'consent',
                'mode': returnMode(control.type, state),
                'io': control.io1
            });
        case 'control':
            return JSON.stringify({
                'type': 'localmode',
                'mode': returnMode(control.type, state),
                'io': control.io1
            });
        default:
            return null;  
    }
}

function returnMode(type, state) {
    if (type === 'motor') {
        switch(state) {
            case 'open':
                return 'fwd';
            case 'stop':
                return 'stop';
            case 'close':
                return 'rwd';
            default:
                return state;
        }
    } else {  
        return state;
    }
} 
