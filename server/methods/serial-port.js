const SerialPort = require('serialport');
const Sequelize = require('sequelize');
const sequelize = require('./sequelize').sequelize;
const CONFIG = require('../config');
const getIo = require('../methods/socket-io').getIo;
const CONTROL_UTIL = require('../utils/control');

const setSensorValue = require('./sensor').setSensorValue;

const reConnectPeriod = 5000;
let reConnectInstance = {
    legacy: null,
    newSensor: null,
    newControl: null
};

let serialPort = {
    legacy: null,
    newSensor: null,
    newControl: null
};

module.exports.sendControlSignal = (user_id, control, state, callback) => {
    const port = serialPort.newControl;
    if (!port) {
        sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.control_name}' 연결에 실패했습니다.`);
        return callback();
    }
    /**
     * todo 3.0 send
     * todo priority queue
     */



    callback();
};
module.exports = () => {
    if (CONFIG.serialPort.isUseNewSensor) {
        const sensorPort = new SerialPort(CONFIG.serialPort.newSensorAddress, {
            baudRate: 19200,
        });

        sensorPort.on('open', () => {
            serialPort.newSensor = sensorPort;
            console.log('sensor-serial-port connected');
        });

        sensorPort.on('close', () => {
            console.error('sensor-serial-port connection close');
            serialPort.newSensor = null;
            sequelize.models.Report.createReports('error', '[SENSOR SERIAL PORT] 연결이 해제됐습니다.');
            reConnect(sensorPort, 'newSensor', 'sensor');
        });

        sensorPort.on('error', (e) => {
            console.error('sensor-serial-port connection fail', e);
            serialPort.newSensor = null;
            sequelize.models.Report.createReports('error', '[SENSOR SERIAL PORT] 연결에 실패했습니다.');
            reConnect(sensorPort, 'newSensor', 'sensor');
        });
    }
    if (CONFIG.serialPort.isUseNewControl) {
        const controlPort = new SerialPort(CONFIG.serialPort.newControlAddress, {
            baudRate: 19200,
        });

        controlPort.on('open', () => {
            serialPort.newControl = controlPort;
            console.log('control-serial-port connected');
            // controlPort.on('data', (data) => {
            //     console.log('received data', data);
            // });    

            // const xor = require('buffer-xor');

            // const startCode = Buffer.from('F3');
            // console.log('start code', startCode);

            // let dataLength = new Uint8Array(1);
            // dataLength[0] = 11;
            // dataLength = Buffer.from(dataLength);
            // console.log('data length', dataLength);

            // let boardAddress = new Uint8Array(1);
            // boardAddress[0] = 0;
            // boardAddress = Buffer.from(boardAddress);
            // console.log('board address', '\t\t\t', boardAddress);

            // let opCode = new Uint8Array(1);
            // opCode[0] = 82;
            // opCode = Buffer.from(opCode);
            // console.log('op code', '\t\t\t', opCode);

            // let relayBit = new Uint8Array(3);
            // relayBit[0] = 0;
            // relayBit[1] = 255;
            // relayBit[2] = 255;
            // relayBit = Buffer.from(relayBit);
            // console.log('relay bit', '\t\t\t', relayBit);

            // let timeUnit = new Uint8Array(1);
            // timeUnit[0] = 0;
            // timeUnit = Buffer.from(timeUnit);
            // console.log('time unit', '\t\t\t', timeUnit);

            // let onTime = new Uint8Array(1);
            // onTime[0] = 255;
            // onTime = Buffer.from(onTime);
            // console.log('on time', '\t\t\t', onTime);

            // let offTime = new Uint8Array(1);
            // offTime[0] = 0;
            // offTime = Buffer.from(offTime);
            // console.log('off time', '\t\t\t', offTime);

            // let repeat = new Uint8Array(1);
            // repeat[0] = 0;
            // repeat = Buffer.from(repeat);
            // console.log('repeat', '\t\t\t', repeat);

            // let data = Buffer.concat([startCode, dataLength, boardAddress, opCode, relayBit, timeUnit, onTime, offTime, repeat]);
            // console.log('data', '\t\t\t', data);

            // console.log('length', '\t\t\t', data.byteLength);

            // let crc = data.slice(0, 1);
            // console.log('crc', crc);
            // for (let i=1; i<data.byteLength; i++) {
            //     crc = xor(crc, data.slice(i, i + 1));
            // }
            // console.log('crc', '\t\t\t', crc);

            // let payload = Buffer.concat([data, crc]);
            // console.log('payload', '\t\t\t', payload);

            // let payload2 = new Buffer(6);
            // payload2[0] = 0x46;
            // payload2[1] = 0x33;
            // payload2[2] = 0x04;
            // payload2[3] = 0x00;
            // payload2[4] = 0x42;
            // payload2[5] = 0x33;
            // console.log(payload2);

            // // 46 33 06 00 62 00 00 11

            // controlPort.write(payload2);
            // controlPort.write(payload);
        });

        controlPort.on('close', () => {
            console.error('control-serial-port connection close');
            serialPort.newControl = null;
            sequelize.models.Report.createReports('error', '[CONTROL SERIAL PORT] 연결이 해제됐습니다.');
            reConnect(controlPort, 'newControl', 'control');
        });

        controlPort.on('error', (e) => {
            console.error('control-serial-port connection fail', e);
            serialPort.newControl = null;
            sequelize.models.Report.createReports('error', '[CONTROL SERIAL PORT] 연결에 실패했습니다.');
            reConnect(controlPort, 'newControl', 'control');
        });
    }
    if (CONFIG.serialPort.isUseLegacy) {
        const regExp = new RegExp('\'', 'g');
        const port = new SerialPort(CONFIG.serialPort.legacyAddress, {
            baudRate: 19200,
            dataBits: 8,
            parity: 'none',
            stopBits: 1,
            flowControl: false,
            parser: new SerialPort.parsers.Readline('\n')
        });

        port.on('open', () => {
            serialPort.legacy = port;
            console.log('serial-port connected');
            let buff = '';
            let parseTarget = '';
            let isEnd = false;
            port.on('data', (data) => {
                data = data.toString().replace(regExp, '"');
                if (data.indexOf('{"') !== -1) {
                    buff = '{"' + data.split('{"')[1];
                    isEnd = false;
                } else if (data.indexOf('}') !== -1) {
                    parseTarget = buff + data.split('}')[0] + '}';
                    buff = data.split('}')[1];
                    isEnd = true;
                } else {
                    buff += data;
                }
                if (isEnd) {
                    try {
                        console.log('parsed', JSON.parse(parseTarget));
                        const res = JSON.parse(parseTarget);
                        Object.keys(res).forEach((key) => {
                            if (key !== 'Name') {
                                setSensorValue(res.Name, key, res[key]);
                            }
                        });
                    } catch (e) {}
                }
            });
        });

        port.on('close', () => {
            console.error('serial-port connection close');
            serialPort.legacy = null;
            sequelize.models.Report.createReports('error', '[SERIAL PORT] 연결이 해제됐습니다.');
            reConnect(port, 'legacy');
        });

        port.on('error', (e) => {
            console.error('serial-port connection fail', e);
            serialPort.legacy = null;
            sequelize.models.Report.createReports('error', '[SERIAL PORT] 연결에 실패했습니다.');
            reConnect(port, 'legacy');
        });
    }
};
module.exports.getSerialPorts = () => {
    return new Promise((resolve, reject) => {
        try {
            SerialPort.list((err, ports) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(ports);
                }
            });
        } catch (e) {
            reject(e);
        }
    });
};
module.exports.getSerialPort = (key) => {
    if (serialPort[key]) {
        return serialPort[key];
    } else {
        return null;
    }
};
function reConnect(port, key, prefix) {
    if (reConnectInstance[key]) {
        clearInterval(reConnectInstance[key]);
        reConnectInstance[key] = null;
    }
    reConnectInstance[key] = setInterval(() => {
        port.open((err) => {
            if (err) {
                console.error(`${prefix ? prefix + '-' : ''}serial-port connection fail`, err);
                sequelize.models.Report.createReports('error', `[${prefix ? prefix.toUpperCase() + ' ' : ''}SERIAL PORT] 연결에 실패했습니다.`);
            } else {
                if (reConnectInstance[key]) {
                    clearInterval(reConnectInstance[key]);
                    reConnectInstance[key] = null;
                }
            }
        });
    }, reConnectPeriod);
}
