const mysql = require('mysql2');
const ALTER_LIST = require('./alter_list');
const CONFIG = require('../config');

module.exports = () => {
    return new Promise(async (resolve, reject) => {
        const connection = mysql.createConnection({
            host: CONFIG.db.host,
            user: CONFIG.db.username,
            database: CONFIG.db.database,
            password: CONFIG.db.password,
            timezone: CONFIG.db.timezone,
            charset: CONFIG.db.charset
        });

        connection.query('CREATE TABLE IF NOT EXISTS `alters` (`id` INTEGER NOT NULL auto_increment , `query` LONGTEXT NOT NULL, `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;', (err, results) => {
            if (err) {
                reject(err);
            } else {
                return activeAlterList(connection, isSuccess => {
                    if (isSuccess) {
                        connection.end();
                        resolve(true);
                    } else {
                        reject(false);
                    }
                });
            }
        });
    });
};

async function activeAlterList(connection, callback) {
    try {
        for (let i=0; i<ALTER_LIST.length; i++) {
            const queryInfo = ALTER_LIST[i];
            await activeQuery(connection, queryInfo);
        }
        callback(true);
    } catch (e) {
        console.error(e);
        callback(false);
    }
}

function activeQuery(connection, queryInfo) {
    return new Promise(async (resolve, reject) => {
        try {
            const query = `ALTER TABLE \`${queryInfo.table}\` ADD \`${queryInfo.column}\` ${queryInfo.type} ${queryInfo.allowNull ? 'NULL' : 'NOT NULL'} default ${queryInfo.default}${queryInfo.after ? " AFTER `" + queryInfo.after + "`" : ''}`;
            connection.query(`SELECT COUNT(*) AS count FROM alters WHERE query = '${query}'`, (err, results) => {
                if (err) {
                    reject(err);
                } else {
                    if (results[0].count) {
                        resolve(true);
                    } else {
                        connection.query(`SELECT (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "${CONFIG.db.database}" AND TABLE_NAME = "${queryInfo.table}") AS tableCount, (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "${CONFIG.db.database}" AND TABLE_NAME = "${queryInfo.table}" AND COLUMN_NAME = "${queryInfo.column}") AS columnCount`, (err, results) => {
                            if (err) {
                                reject(err);
                            } else {
                                if (results[0].tableCount && results[0].columnCount) {
                                    connection.query(`INSERT INTO alters(query) VALUES ('${query}')`, (err, results) => {
                                        if (err) {
                                            reject(err);
                                        } else {
                                            resolve(true);
                                        }
                                    });
                                } else if (results[0].tableCount && !results[0].columnCount) {
                                    connection.query(query, (err, results) => {
                                        if (err) {
                                            reject(err);
                                        } else {
                                            connection.query(`INSERT INTO alters(query) VALUES ('${query}')`, (err, results) => {
                                                if (err) {
                                                    reject(err);
                                                } else {
                                                    resolve(true);
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    resolve(true);
                                }
                            }
                        });
                    }
                }
            });
        } catch (e) {
            reject(e);
        }
    });
}
