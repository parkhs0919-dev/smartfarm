const Sequelize = require('sequelize');
const sequelize = require('../../server/methods/sequelize').sequelize;
const STD = require('../metadata/standards');
const getTime = require('../cron/auto-control').getTime;
const ERP = require('../utils/erp');

module.exports = {
    farm: async (ctx) => {
        try {
            ctx.houses = await sequelize.models.House.findAll({
                where: {
                    platform: 'farm'
                }
            });
            const house_id = ctx.houses[0].id;

            ctx.currentAutoCompleteTime = getTime(house_id);

            await Promise.all([
                sequelize.models.Screen.findAll({
                    order: [['order', 'ASC']],
                    where: {
                        house_id,
                        is_visible: true
                    }
                }).then(data => ctx.screens = data),
                sequelize.models.Mode.findOne({
                    where: {house_id}
                }).then(data => ctx.mode = data),
                sequelize.models.Control.count({
                    where: {
                        house_id,
                        type: 'control'
                    }
                }).then(data => ctx.controlCount = data),
                sequelize.models.Control.findAll({
                    order: [['order', 'ASC'], ['id', 'ASC']],
                    where: {
                        house_id,
                        type: 'control'
                    }
                }).then(data => ctx.controls = data),
                sequelize.models.Sensor.findOne({
                    include: [{
                        model: sequelize.models.SensorMainChart,
                        as: 'sensorMainCharts',
                        where: {house_id}
                    }]
                }).then(data => ctx.sensorMainChart = data)
            ]);

            let query = `select AA.value as name from init_config as AA  where AA.type='farm' and AA.key='farmName' `;
            const userInfo = await sequelize.query(query, {   
                type: Sequelize.QueryTypes.SELECT
            });
            
            if(userInfo && userInfo.length>0)  ctx.userInfo =  {farmName : userInfo[0].name };
            else ctx.userInfo = {};

        } catch (e) {      
            ctx.screens = [];
            ctx.currentAutoCompleteTime = 0;
            ctx.mode = null;
            ctx.houses = [];
            ctx.controlCount = 0;
            ctx.controls = [];
            ctx.sensorMainChart = null;
            ctx.userInfo = {};
        }
    },
    animal: async (ctx) => {

    }
};

// async function getCenterUserInfoFarmName() {
//     return new Promise(async (resolve, reject) => {
//         try {  
//             let userInfo = {};   
//             const data = await ERP.getFarmlName('farm');
//             if(data) userInfo.farmName=data.name; 
//             resolve(userInfo);        
//         } catch (e) {        
//             reject(e);        
//         }  
//     });   
// }    