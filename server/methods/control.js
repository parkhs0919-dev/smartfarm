const Sequelize = require('sequelize');
const sequelize = require('./sequelize').sequelize;
const CONFIG = require('../config');
const LANG = require('../metadata/langs').ko;
const STD = require('../metadata/standards');
let io = require('./socket-io').getIo();

let motors = {};
let powers = {};
let controls = {};
let motorCounts = {};

const maxMotorSignalCount = CONFIG.control.maxMotorSignalCount;

global.controlWorkList=[];
global.controlWorkListData=[];

function sendSignal(user_id, control, state) {
    return new Promise((resolve, reject) => {
        /**
         * todo send signal by protocol
         */

        // if (control.type === 'motor') {
        //     if (state === 'open' || state === 'close') {
        //         if (motorCounts[control.id].state === state) {
        //             if ((state === 'open' && control.current === control.range) || (state === 'close' && control.current === 0)) {
        //                 if (motorCounts[control.id].count < maxMotorSignalCount) {
        //                     motorCounts[control.id].count++;
        //                 }
        //             }
        //         } else {
        //             motorCounts[control.id].state = state;
        //             motorCounts[control.id].count = 0;
        //         }
        //     }
        //     if (motorCounts[control.id].count >= maxMotorSignalCount) {
        //         return resolve(true);
        //     }
        // }

        syncIo(() => {
            if (io) {
                io.emit('control-state', {
                    id: control.id,
                    state
                });
            }
        });

        if (CONFIG.control.isUseHttp) {
            require('./control-ip').sendSignal(user_id, control, state, (isSuccess) => {
                if (isSuccess) {
                    resolve(true);
                } else {
                    reject(false);
                }
            });
        } else if (CONFIG.control.isUseSerialPort) {
            require('./control-board').sendControlSignal(user_id, control, state, (isSuccess) => {
                if (isSuccess) {
                    resolve(true);
                } else {
                    reject(false);
                }
            });
        } else if (CONFIG.control.isUseHttpBoard) {
            require('./control-ip-board').sendSignal(user_id, control, state, isSuccess => {
                if (isSuccess) {
                    resolve(true);
                } else {
                    reject(false);
                }
            });
        } else {
            resolve(true);
        }
    });
}

module.exports = async () => {
    const motorControls = await sequelize.models.Control.findAll({
        include: [{
            model: sequelize.models.ControlReservation,
            as: 'controlReservation'
        }],
        where: {
            type: 'motor'
        },
    });
    const powerControls = await sequelize.models.Control.findAll({
        include: [{
            model: sequelize.models.ControlReservation,
            as: 'controlReservation'
        }],
        where: {
            type: 'power'
        },
    });
    const controlControls = await sequelize.models.Control.findAll({
        include: [{
            model: sequelize.models.ControlReservation,
            as: 'controlReservation'
        }],
        where: {
            type: 'control'
        },
    });
    for (let i=0; i<motorControls.length; i++) {
        const control = motorControls[i];
        motors[control.id] = {
            control,
            beforeWork: null,
            timeoutInstance: null,
            intervalInstance: null,
            type: null,
        };
        motorCounts[control.id] = {
            state: null,
            count: 0
        };
        if (control.controlReservation) {
            motors[control.id].intervalInstance = setInterval(async () => {
                let query;
                if (control.controlReservation.before_state === 'open') {
                    query = `UPDATE controls SET \`state\` = '${control.controlReservation.before_state}', \`current\` = (CASE WHEN \`current\` + 1 < \`range\` THEN \`current\` + 1 ELSE \`range\` END) WHERE id = ${control.id}`;
                } else {
                    query = `UPDATE controls SET \`state\` = '${control.controlReservation.before_state}', \`current\` = (CASE WHEN \`current\` - 1 > 0 THEN \`current\` - 1 ELSE 0 END) WHERE id = ${control.id}`;
                }
                await sequelize.query(query, {
                    type: Sequelize.QueryTypes.UPDATE
                });
                const motor = await sequelize.models.Control.findById(control.id);
                syncIo(() => {
                    io && io.emit('motor', {motor});
                });
            }, 1000);
            if (control.controlReservation.active_time && control.controlReservation.after_state) {
                const time = control.controlReservation.active_time - new Date().getTime();
                if (time > 0) {
                    motors[control.id].timeoutInstance = setTimeout(async () => {
                        await sequelize.models.ControlReservation.destroy({where: {control_id: control.id}});
                        await motorReservationProcess(control);
                    }, time);
                } else {
                    await sequelize.models.ControlReservation.destroy({where: {control_id: control.id}});
                    await motorReservationProcess(control);
                }
            }
        }
    }
    for (let i=0; i<powerControls.length; i++) {
        const control = powerControls[i];
        powers[control.id] = {
            control,
            beforeWork: null,
            timeoutInstance: null,
            type: null,
        };
        if (control.controlReservation) {
            const time = control.controlReservation.active_time - new Date().getTime();
            if (time > 0) {
                powers[control.id].timeoutInstance = setTimeout(async () => {
                    await sequelize.models.ControlReservation.destroy({where: {control_id: control.id}});
                    await powerReservationProcess(control);
                }, time);
            } else {
                await sequelize.models.ControlReservation.destroy({where: {control_id: control.id}});
                await powerReservationProcess(control);
            }
        }
    }
    for (let i=0; i<controlControls.length; i++) {
        const control = controlControls[i];
        controls[control.id] = {
            control
        };
    }
};

async function motorReservationProcess(control) {
    const controlReservation = control.controlReservation;
    await sendSignal(controlReservation.user_id, control, 'stop');

    clearBeforeWork(motors, control.id);
    clearIntervalInstance(motors, control.id);
    await clearTimeoutInstance(motors, control.id);

    await sequelize.models.Control.update({state: 'stop'}, {where: {id: control.id}});
    const motor = await sequelize.models.Control.findById(control.id);
    if (controlReservation.mode === 'manual') {
        const mode = await sequelize.models.Mode.getModeByControlId(control.id);
        await sequelize.models.Report.create({
            user_id: controlReservation.user_id,
            house_id: motor.house_id,
            control_id: motor.id,
            type: 'motor',
            contents: `[${mode === 'individual' ? '개별/' : ''}수동제어] ` + generateContents(motor, 'stop')
        });
    } else {
        await sequelize.models.Report.createAutoControlReport({house_id: control.house_id}, motor, {state: 'stop'});
    }
    syncIo(() => {
        if (io) {
            io.emit('motor', {motor});
            io.emit('report', {
                house_id: motor.house_id
            });
        }
    });
}

async function powerReservationProcess(control) {
    const controlReservation = control.controlReservation;
    await sendSignal(controlReservation.user_id, await sequelize.models.Control.findById(control.id), controlReservation.after_state);

    clearBeforeWork(powers, control.id);
    await clearTimeoutInstance(powers, control.id);

    await sequelize.models.Control.update({state: controlReservation.after_state}, {where: {id: control.id}});
    const power = await sequelize.models.Control.findById(control.id);
    if (controlReservation.mode === 'manual') {
        const mode = await sequelize.models.Mode.getModeByControlId(control.id);
        await sequelize.models.Report.create({
            user_id: controlReservation.user_id,
            house_id: control.house_id,
            control_id: control.id,
            type: 'power',
            contents: `[${mode === 'individual' ? '개별/' : ''}수동제어] ` + generateContents(power, controlReservation.after_state)
        });
    } else {
        await sequelize.models.Report.createAutoControlReport({house_id: control.house_id}, power, {state: controlReservation.after_state});
    }
    syncIo(() => {
        if (io) {
            io.emit('power', {power});
            io.emit('report', {
                house_id: power.house_id
            });
        }
    });
}

module.exports.activeMotor = async ({id, state, type, time, user_id = CONFIG.defaultUser.id, autoControl = null, currentStep = null}) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (type !== 'manual' && type !== 'auto') {
                return resolve(false);
            }
            let motor = await sequelize.models.Control.findById(id);
            const currentState = motor.state;
            if (type === 'manual') {
                if (state === 'stop') {
                    if (motors[id].beforeWork) {
                        /**
                         * todo log clear before work
                         */
                    }
                    clearBeforeWork(motors, id);
                    await clearTimeoutInstance(motors, id);
                    clearIntervalInstance(motors, id);

                    await sendSignal(user_id, motor, state);

                    await sequelize.models.Control.update({state}, {where: {id}});
                    await sequelize.models.ControlReservation.destroy({where: {control_id: id}});
                    motor = await sequelize.models.Control.findById(id);
                    syncIo(() => {
                        io.emit('motor', {motor});
                    });
                    resolve(true);
                } else if (state === 'open') {
                    if (currentState === 'stop') {
                        if (motors[id].beforeWork) {
                            /**
                             * todo log clear before work
                             */
                        }
                        clearBeforeWork(motors, id);
                        await clearTimeoutInstance(motors, id);
                        clearIntervalInstance(motors, id);

                        await sendSignal(user_id, motor, state);

                        await sequelize.models.Control.update({state}, {where: {id}});
                        motor = await sequelize.models.Control.findById(id);
                        syncIo(() => {
                            io.emit('motor', {motor});
                        });
                        await sequelize.models.ControlReservation.create({
                            control_id: id,
                            user_id,
                            mode: 'manual',
                            before_state: state,
                            after_state: time ? 'stop' : null,
                            active_time: time ? new Date().getTime() + generateTime(time) : null
                        });
                        motors[id].intervalInstance = setInterval(async () => {
                            const query = `UPDATE controls SET \`state\` = '${state}', \`current\` = (CASE WHEN \`current\` + 1 < \`range\` THEN \`current\` + 1 ELSE \`range\` END) WHERE id = ${id}`;
                            await sequelize.query(query, {
                                type: Sequelize.QueryTypes.UPDATE
                            });
                            motor = await sequelize.models.Control.findById(id);
                            syncIo(() => {
                                io.emit('motor', {motor});
                            });
                        }, 1000);
                        if (time) {
                            const mode = await sequelize.models.Mode.getModeByControlId(id);
                            motors[id].timeoutInstance = setTimeout(async () => {

                                await sendSignal(user_id, motor, 'stop');

                                clearBeforeWork(motors, id);
                                clearIntervalInstance(motors, id);
                                await clearTimeoutInstance(motors, id);

                                await sequelize.models.Control.update({state: 'stop'}, {where: {id}});
                                motor = await sequelize.models.Control.findById(id);
                                await sequelize.models.Report.create({
                                    user_id,
                                    house_id: motor.house_id,
                                    control_id: motor.id,
                                    type: 'motor',
                                    contents: `[${mode === 'individual' ? '개별/' : ''}수동제어] ` + generateContents(motor, 'stop')
                                });
                                syncIo(() => {
                                    io.emit('motor', {motor});
                                    io.emit('report', {
                                        house_id: motor.house_id
                                    });
                                });
                            }, generateTime(time));
                        }
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                } else if (state === 'close') {
                    if (currentState === 'stop') {
                        if (motors[id].beforeWork) {
                            /**
                             * todo log clear before work
                             */
                        }
                        clearBeforeWork(motors, id);
                        await clearTimeoutInstance(motors, id);
                        clearIntervalInstance(motors, id);

                        await sendSignal(user_id, motor, state);

                        await sequelize.models.Control.update({state}, {where: {id}});
                        motor = await sequelize.models.Control.findById(id);
                        syncIo(() => {
                            io.emit('motor', {motor});
                        });
                        await sequelize.models.ControlReservation.create({
                            control_id: id,
                            user_id,
                            mode: 'manual',
                            before_state: state,
                            after_state: time ? 'stop' : null,
                            active_time: time ? new Date().getTime() + generateTime(time) : null
                        });
                        motors[id].intervalInstance = setInterval(async () => {
                            const query = `UPDATE controls SET \`state\` = '${state}', \`current\` = (CASE WHEN \`current\` - 1 > 0 THEN \`current\` - 1 ELSE 0 END) WHERE id = ${id}`;
                            await sequelize.query(query, {
                                type: Sequelize.QueryTypes.UPDATE
                            });
                            motor = await sequelize.models.Control.findById(id);
                            syncIo(() => {
                                io.emit('motor', {motor});
                            });
                        }, 1000);
                        if (time) {
                            const mode = await sequelize.models.Mode.getModeByControlId(id);
                            motors[id].timeoutInstance = setTimeout(async () => {

                                await sendSignal(user_id, motor, 'stop');

                                clearBeforeWork(motors, id);
                                clearIntervalInstance(motors, id);
                                await clearTimeoutInstance(motors, id);

                                await sequelize.models.Control.update({state: 'stop'}, {where: {id}});
                                motor = await sequelize.models.Control.findById(id);
                                await sequelize.models.Report.create({
                                    user_id,
                                    house_id: motor.house_id,
                                    control_id: motor.id,
                                    type: 'motor',
                                    contents: `[${mode === 'individual' ? '개별/' : ''}수동제어] ` + generateContents(motor, 'stop')
                                });
                                syncIo(() => {
                                    io.emit('motor', {motor});
                                    io.emit('report', {
                                        house_id: motor.house_id
                                    });
                                });
                            }, generateTime(time));
                        }
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }
            } else if (autoControl) { // auto
                if (state === 'stop') {
                    if (motors[id].beforeWork) {
                        await sequelize.models.Report.createAutoControlClearReport(user_id, motors[id].control.house_id, id, type, motors[id].beforeWork);
                    }
                    clearBeforeWork(motors, id);
                    await clearTimeoutInstance(motors, id);
                    clearIntervalInstance(motors, id);

                    await sendSignal(user_id, motor, state);

                    await sequelize.models.ControlReservation.destroy({where: {control_id: id}});
                    await sequelize.models.Control.update({state}, {where: {id}});
                    motor = await sequelize.models.Control.findById(id);
                    syncIo(() => {
                        io.emit('motor', {motor});
                    });
                    resolve(true);
                } else if (state === 'open') {
                    if (motors[id].beforeWork) {
                        await sequelize.models.Report.createAutoControlClearReport(user_id, motors[id].control.house_id, id, type, motors[id].beforeWork);
                    }
                    clearBeforeWork(motors, id);
                    await clearTimeoutInstance(motors, id);
                    clearIntervalInstance(motors, id);

                    await sendSignal(user_id, motor, state);

                    await sequelize.models.Control.update({state}, {where: {id}});
                    motor = await sequelize.models.Control.findById(id);
                    syncIo(() => {
                        io.emit('motor', {motor});
                    });
                    await sequelize.models.ControlReservation.destroy({where: {control_id: id}});
                    await sequelize.models.ControlReservation.create({
                        control_id: id,
                        user_id,
                        mode: 'auto',
                        before_state: state,
                        after_state: time ? 'stop' : null,
                        active_time: time ? new Date().getTime() + generateTime(time) : null
                    });
                    motors[id].intervalInstance = setInterval(async () => {
                        const query = `UPDATE controls SET \`state\` = '${state}', \`current\` = (CASE WHEN \`current\` + 1 < \`range\` THEN \`current\` + 1 ELSE \`range\` END) WHERE id = ${id}`;
                        await sequelize.query(query, {
                            type: Sequelize.QueryTypes.UPDATE
                        });
                        motor = await sequelize.models.Control.findById(id);
                        syncIo(() => {
                            io.emit('motor', {motor});
                        });
                    }, 1000);
                    if (time) {
                        motors[id].beforeWork = {
                            order: autoControl.order,
                            auto_control_name: autoControl.auto_control_name,
                            control_name: motor.control_name,
                            state,
                            time
                        };
                        motors[id].timeoutInstance = setTimeout(async () => {

                            await sendSignal(user_id, motor, 'stop');

                            clearBeforeWork(motors, id);
                            clearIntervalInstance(motors, id);
                            await clearTimeoutInstance(motors, id);

                            await sequelize.models.Control.update({state: 'stop'}, {where: {id}});
                            motor = await sequelize.models.Control.findById(id);
                            await sequelize.models.Report.createAutoControlReport(autoControl, motor, {state: 'stop'}, currentStep);
                            syncIo(() => {
                                io.emit('motor', {motor});
                                io.emit('report', {
                                    house_id: motor.house_id
                                });
                            });
                        }, generateTime(time));
                    }
                    resolve(true);
                } else if (state === 'close') {
                    if (motors[id].beforeWork) {
                        await sequelize.models.Report.createAutoControlClearReport(user_id, motors[id].control.house_id, id, type, motors[id].beforeWork);
                    }
                    clearBeforeWork(motors, id);
                    await clearTimeoutInstance(motors, id);
                    clearIntervalInstance(motors, id);

                    await sendSignal(user_id, motor, state);

                    await sequelize.models.Control.update({state}, {where: {id}});
                    motor = await sequelize.models.Control.findById(id);
                    syncIo(() => {
                        io.emit('motor', {motor});
                    });
                    await sequelize.models.ControlReservation.destroy({where: {control_id: id}});
                    await sequelize.models.ControlReservation.create({
                        control_id: id,
                        user_id,
                        mode: 'auto',
                        before_state: state,
                        after_state: time ? 'stop' : null,
                        active_time: time ? new Date().getTime() + generateTime(time) : null
                    });
                    motors[id].intervalInstance = setInterval(async () => {
                        const query = `UPDATE controls SET \`state\` = '${state}', \`current\` = (CASE WHEN \`current\` - 1 > 0 THEN \`current\` - 1 ELSE 0 END) WHERE id = ${id}`;
                        await sequelize.query(query, {
                            type: Sequelize.QueryTypes.UPDATE
                        });
                        motor = await sequelize.models.Control.findById(id);
                        syncIo(() => {
                            io.emit('motor', {motor});
                        });
                    }, 1000);
                    if (time) {
                        motors[id].beforeWork = {
                            order: autoControl.order,
                            auto_control_name: autoControl.auto_control_name,
                            control_name: motor.control_name,
                            state,
                            time
                        };
                        motors[id].timeoutInstance = setTimeout(async () => {

                            await sendSignal(user_id, motor, 'stop');

                            clearBeforeWork(motors, id);
                            clearIntervalInstance(motors, id);
                            await clearTimeoutInstance(motors, id);

                            await sequelize.models.Control.update({state: 'stop'}, {where: {id}});
                            motor = await sequelize.models.Control.findById(id);
                            await sequelize.models.Report.createAutoControlReport(autoControl, motor, {state: 'stop'}, currentStep);
                            syncIo(() => {
                                io.emit('motor', {motor});
                                io.emit('report', {
                                    house_id: motor.house_id
                                });
                            });
                        }, generateTime(time));
                    }
                    resolve(true);
                }
            }
        } catch (e) {
            console.error('active motor fail', e);
            reject(e);
        }
    });
};
module.exports.activePower = ({id, state, type, time, user_id = CONFIG.defaultUser.id, autoControl = null, currentStep = null}) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (type !== 'manual' && type !== 'auto') {
                return resolve(false);
            }
            const power = await sequelize.models.Control.findById(id);
            if (type === 'manual') {
                if (powers[id].beforeWork) {
                    /**
                     * todo log clear before work
                     */
                }
                clearBeforeWork(powers, id);
                await clearTimeoutInstance(powers, id);
                await powerProcess(user_id, id, state, time, power, autoControl, currentStep);
                resolve(true);
            } else { // auto
                if (powers[id].beforeWork) {
                    await sequelize.models.Report.createAutoControlClearReport(user_id, powers[id].control.house_id, id, type, powers[id].beforeWork);
                }
                clearBeforeWork(powers, id);
                await clearTimeoutInstance(powers, id);
                await powerProcess(user_id, id, state, time, power, autoControl);
                resolve(true);
            }
        } catch (e) {
            console.error('active power fail', e);
            reject(e);
        }
    });
};
module.exports.activeControl = ({id, state, user_id = CONFIG.defaultUser.id}) => {
    return new Promise(async (resolve, reject) => {
        try {
            await sendSignal(user_id, await sequelize.models.Control.findById(id), state);

            await sequelize.models.Control.update({state}, {
                where: {id}
            });
            const control = await sequelize.models.Control.findById(id);

            syncIo(() => {
                io.emit('control', {control});
            });
            resolve(true);
        } catch (e) {
            console.error('active control fail', e);
            reject(e);
        }
    });
};
module.exports.getPowers = () => {
    return powers;
};
module.exports.getMotors = () => {
    return motors;
};
module.exports.generateContents = generateContents;
module.exports.generateGroupContents = generateGroupContents;

async function powerProcess(user_id, id, state, time, powerData, autoControl = null, currentStep = null) {

    await sendSignal(user_id, powerData, state);
    await sequelize.models.Control.update({state}, {where: {id}});
    const power = await sequelize.models.Control.findById(id);

    if (time) {
        const mode = await sequelize.models.Mode.getModeByControlId(id);
        if (autoControl) {
            powers[id].beforeWork = {
                order: autoControl.order,
                auto_control_name: autoControl.auto_control_name,
                control_name: power.control_name,
                state,
                time
            };
        }
        const reserveState = reservePowerState(state);

        await sequelize.models.ControlReservation.destroy({where: {control_id: id}});
        await sequelize.models.ControlReservation.create({
            control_id: id,
            user_id,
            mode: autoControl ? 'auto' : 'manual',
            before_state: state,
            after_state: reserveState,
            active_time: new Date().getTime() + generateTime(time)
        });

        powers[id].timeoutInstance = setTimeout(async () => {
            await sendSignal(user_id, await sequelize.models.Control.findById(id), reserveState);

            clearBeforeWork(powers, id);
            await clearTimeoutInstance(powers, id);

            await sequelize.models.Control.update({state: reserveState}, {where: {id}});
            const power = await sequelize.models.Control.findById(id);
            if (autoControl) {
                await sequelize.models.Report.createAutoControlReport(autoControl, power, {state: reserveState}, currentStep);
            } else {
                await sequelize.models.Report.create({
                    user_id,
                    house_id: power.house_id,
                    control_id: power.id,
                    type: 'power',
                    contents: `[${mode === 'individual' ? '개별/' : ''}수동제어] ` + generateContents(power, reserveState)
                });
            }
            syncIo(() => {
                io.emit('power', {power});
                io.emit('report', {
                    house_id: power.house_id
                });
            });
        }, generateTime(time));
    }

    syncIo(() => {
        io.emit('power', {power});
    });
}

async function clearTimeoutInstance(obj, key, instance = 'timeoutInstance') {
    await sequelize.models.ControlReservation.destroy({
        where: {
            control_id: key
        }
    });
    if (obj[key][instance]) {
        clearTimeout(obj[key][instance]);
        obj[key][instance] = null;
    }
}

function clearIntervalInstance(obj, key, instance = 'intervalInstance') {
    if (obj[key][instance]) {
        clearInterval(obj[key][instance]);
        obj[key][instance] = null;
    }
}

function clearBeforeWork(obj, key) {
    if (obj[key].beforeWork) {
        obj[key].beforeWork = null;
    }
}

function reservePowerState(state) {
    if (state === 'on') {
        return 'off';
    } else {
        return 'on';
    }
}

function generateTime(time) {
    return time * 1000 + 100;
}

function syncIo(callback) {
    if (!io) {
        io = require('./socket-io').getIo();
    }
    if (io) {
        callback();
    }
}

function generateContents(control, state, time, percentage, pBandPercentage = null) {
    let stateText = LANG[state];
    if ((state === 'open' || state === 'on') && control.button_name_1) stateText = control.button_name_1;
    else if ((state === 'stop' || state === 'off') && control.button_name_2) stateText = control.button_name_2;
    else if (state === 'close' && control.button_name_3) stateText = control.button_name_3;
    let timeText = '';
    let percentageText = '';
    if (time) {
        if (time > 3600) {
            timeText += ` ${parseInt(time / 3600)}시간`;
            time -= 3600 * parseInt(time / 3600);
        }
        if (time > 60) {
            timeText += ` ${parseInt(time / 60)}분`;
            time -= 60 * parseInt(time / 60);
        }
        if (time) {
            timeText += ` ${time}초`;
        }
        timeText += ' 동안';
    } else if (percentage && percentage !== STD.maxMotorPercentage) {
        percentageText = ' ' + percentage + '%';
    } else if (pBandPercentage) {
        percentageText = ' P-BAND 공식에 의해 ' + pBandPercentage + '%까지';
    }
    return `'${control.control_name}'가(이)${timeText}${percentageText} '${stateText}' 동작합니다.`;
}

function generateGroupContents(controlGroup, state) {
    let stateText = LANG[state];
    return `'${controlGroup.control_group_name}'가(이) '${stateText}' 동작합니다.`;
}
