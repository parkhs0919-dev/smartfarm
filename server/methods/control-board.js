const tcpp = require('tcp-ping');
const request = require('request');
const Sequelize = require('sequelize');
const sequelize = require('./sequelize').sequelize;
const getSerialPort = require('./serial-port').getSerialPort;
      
module.exports.sendControlSignal = (user_id, control, state, callback) => {
    const sensorPort = getSerialPort('newSensor');
    if (!sensorPort) {
        sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.control_name}' 연결에 실패했습니다.`);
        return callback();
    }else{
        makeControlWorkList(user_id, control, state) 
        setTimeout(function() {   
            //console.log('controlWorkListData ischeck ======'+controlWorkListData[control.address+"_"+control.id].ischeck);   
            if( controlWorkListData[control.address+"_"+control.id] &&  controlWorkListData[control.address+"_"+control.id].ischeck==true) {    
                callback(true);  
            }else{
                callback(false);      
            }                         
        }, 3000);                               
    }                 
};  

function makeControlWorkList(user_id, control, state){       
    controlWorkList[control.address+"_"+control.id]={
        buffer1Crc :false
    };       
    controlWorkListData[control.address+"_"+control.id] = {     
        bitmask : getHex(setBitMask(control)),   
        mode : setMode(state),    
        ontime : '0000',                      
        offtime : '0000',                       
        address :control.address,
        ischeck : false    
    }     
    // console.log(setBitMask(control)) 
    // console.log(getHex(setBitMask(control)))
    // console.log(controlWorkListData) 
}  
function setMode(state){    
    let result='';
    if(state=='on') result='03';
    else if(state=='off') result='04';
    return result;
}
function setBitMask(data){                
    let result=['0', '0', '0', '0', '0', '0', '0', '0'];         
    
    if(data.io1==1) result.splice(7, 1, '1');
    else if(data.io1==2) result.splice(6, 1, '1');
    else if(data.io1==3) result.splice(5, 1, '1');
    else if(data.io1==4) result.splice(4, 1, '1');
           
    return result.join('');                               
}    

function getHex(data)
{   
    let str = parseInt(data, 2).toString(16);
    if(str.length==1){
       str='0'+str;   
    }
    return str;  
}   