const http = require('http');
const async = require('async');
const onvif = require('node-onvif');
const Stream = require('node-rtsp-stream');
const Sequelize = require('sequelize');
const sequelize = require('./sequelize').sequelize;
const CONFIG = require('../config');

let server = {};

module.exports = async () => {
    if (process.env.CCTV_ENV) {
        await sequelize.models.Cctv.update({url: null}, {where: {}});
        const cctvs = await sequelize.models.Cctv.findAll({});

        let funcs = [];

        cctvs.forEach((cctv) => {
            (function (cctv) {
                funcs.push((callback) => {
                    server[cctv.id] = {
                        cctv,
                        stream: null,
                        streamUrl: null,
                        activeTime: 0,
                        instance: null
                    };
                    callback(null, true);
                    // const device = new onvif.OnvifDevice({
                    //     xaddr: 'http://' + cctv.address + '/onvif/device_service',
                    //     user : cctv.user,
                    //     pass : cctv.pass
                    // });
                    //
                    // device.init().then(() => {
                    //     const url = device.getUdpStreamUrl();
                    //     console.log('streaming', url);
                    //     let streamUrl = url.replace('rtsp://', `rtsp://${cctv.user}:${cctv.pass}@`);
                    //     if (cctv.type === 'legacy') {
                    //         streamUrl = streamUrl.replace('udp', 'tcp');
                    //     }
                    //     server[cctv.id].streamUrl = streamUrl;
                    //     return sequelize.models.Cctv.update({url}, {
                    //         where: {
                    //             id: cctv.id
                    //         }
                    //     });
                    // }).then(() => {
                    //     callback(null, true);
                    // }).catch(e => {
                    //     console.error(e);
                    //     sequelize.models.Cctv.update({
                    //         url: null
                    //     }, {
                    //         where: {
                    //             id: cctv.id
                    //         }
                    //     });
                    //     callback(null, true);
                    // });
                });
            })(cctv);
        });

        async.parallel(funcs, (error, results) => {
            if (error) {
                console.error(error);
            }
        });
    }
};
module.exports.start = (cctv) => {
    return new Promise((resolve, reject) => {
        if (server[cctv.id].instance && server[cctv.id].stream) {
            clearTimeout(server[cctv.id].instance);
            server[cctv.id].instance = setTimeout(() => {
                try {
                    server[cctv.id].stream.stop();
                    server[cctv.id].stream = null;
                    server[cctv.id].instance = null;
                } catch (e) {
                    console.error('CCTV SERVER CLOSE ERROR', e);
                }
            }, CONFIG.cctv.expiry);
            resolve(true);
        } else {
            const device = new onvif.OnvifDevice({
                xaddr: 'http://' + cctv.address + '/onvif/device_service',
                user : cctv.user,
                pass : cctv.pass
            });

            device.init().then(() => {
                const url = device.getUdpStreamUrl();
                console.log('streaming', url);
                let streamUrl = url.replace('rtsp://', `rtsp://${cctv.user}:${cctv.pass}@`);
                if (cctv.type === 'legacy') {
                    streamUrl = streamUrl.replace('udp', 'tcp');
                }
                server[cctv.id].streamUrl = streamUrl;
                server[cctv.id].stream = new Stream({
                    name: cctv.name,
                    streamUrl: server[cctv.id].streamUrl,
                    wsPort: cctv.port,
                    ffmpegOptions: { // options ffmpeg flags
                        '-stats': '', // an option with no neccessary value uses a blank string
                        '-f': 'mpegts',
                        '-codec:v': 'mpeg1video',
                        '-r': 20, // options with required values specify the value after the key
                        '-loglevel': 'quiet',
                    }
                });
                if (server[cctv.id].stream) {
                    if (server[cctv.id].instance) clearTimeout(server[cctv.id].instance);
                    server[cctv.id].activeTime = new Date().getTime();
                    server[cctv.id].instance = setTimeout(() => {
                        try {
                            server[cctv.id].stream.stop();
                            server[cctv.id].stream = null;
                            server[cctv.id].instance = null;
                        } catch (e) {
                            console.error('CCTV SERVER CLOSE ERROR', e);
                        }
                    }, CONFIG.cctv.expiry);
                    resolve(true);
                } else {
                    reject(false);
                }
            }).catch(e => {
                console.error(e);
                reject(false);
            });
        }
    });
};
