const request = require('request');
const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('./sequelize').sequelize;
const CONFIG = require('../config');

module.exports.sendSignal = (user_id, control, state, callback) => {
    if (control.is_topeng) {
        if (control.address && control.io1 && control.io2 && (control.type === 'motor' ? control.io3 : true)) {
            let urlList = [], targetIo;
            if (control.type === 'motor') {
                if (state === 'open') {
                    targetIo = control.io1;
                } else if (state === 'close') {
                    targetIo = control.io3;
                } else if(state === 'stop') {
                    targetIo = control.io2;
                }
                urlList.push(`http://${control.address}/ecmd?pin set ${control.io1} off`);
                urlList.push(`http://${control.address}/ecmd?pin set ${control.io2} off`);
                urlList.push(`http://${control.address}/ecmd?pin set ${control.io3} off`);
                send(user_id, control, state, urlList, isSuccess => {
                    if (isSuccess) {
                        setTimeout(() => send(user_id, control, state, [`http://${control.address}/ecmd?pin set ${targetIo} on`], isSuccess => {
                            if (isSuccess) {
                                setTimeout(() => send(user_id, control, state, [`http://${control.address}/ecmd?pin set ${targetIo} off`], callback), CONFIG.control.topengDelay);
                            } else {
                                callback(false);
                            }
                        }), CONFIG.control.motorDelay);
                    } else {
                        callback(false);
                    }
                });
            } else {
                urlList.push(`http://${control.address}/ecmd?pin set ${control.io1} off`);
                urlList.push(`http://${control.address}/ecmd?pin set ${control.io2} off`);
                send(user_id, control, state, urlList, isSuccess => {
                    if (isSuccess) {
                        setTimeout(() => send(user_id, control, state, [`http://${control.address}/ecmd?pin set ${state === 'on' ? control.io1 : control.io2} on`], isSuccess => {
                            if (isSuccess) {
                                setTimeout(() => send(user_id, control, state, [`http://${control.address}/ecmd?pin set ${state === 'on' ? control.io1 : control.io2} off`], callback), CONFIG.control.topengDelay);
                            } else {
                                callback(false);
                            }
                        }), CONFIG.control.motorDelay);
                    } else {
                        callback(false);
                    }
                });
            }
        } else {
            sequelize.models.Report.createControlReport(user_id, control, 'error', `[제어보드] '${control.control_name}'에 할당된 정보가 없습니다.`);
            callback(false);
        }
    } else {
        if (control.address && control.io1 && (control.type === 'motor' ? control.io2 : true)) {
            let urlList = [];
            if (control.type === 'motor') {
                if (state === 'open') {
                    urlList.push(`http://${control.address}/ecmd?pin set ${control.io1} off`);
                    urlList.push(`http://${control.address}/ecmd?pin set ${control.io2} off`);
                    send(user_id, control, state, urlList, isSuccess => {
                        if (isSuccess) {
                            setTimeout(() => send(user_id, control, state, [`http://${control.address}/ecmd?pin set ${control.io1} on`], callback), CONFIG.control.motorDelay);
                        } else {
                            callback(false);
                        }
                    });
                } else if (state === 'close') {
                    urlList.push(`http://${control.address}/ecmd?pin set ${control.io1} off`);
                    urlList.push(`http://${control.address}/ecmd?pin set ${control.io2} off`);
                    send(user_id, control, state, urlList, isSuccess => {
                        if (isSuccess) {
                            setTimeout(() => send(user_id, control, state, [`http://${control.address}/ecmd?pin set ${control.io2} on`], callback), CONFIG.control.motorDelay);
                        } else {
                            callback(false);
                        }
                    });
                } else if (state === 'stop') {
                    urlList.push(`http://${control.address}/ecmd?pin set ${control.io1} off`);
                    urlList.push(`http://${control.address}/ecmd?pin set ${control.io2} off`);
                    send(user_id, control, state, urlList, callback);
                }
            } else {
                urlList.push(`http://${control.address}/ecmd?pin set ${control.io1} ${state === 'on' ? 'on' : 'off'}`);
                send(user_id, control, state, urlList, callback);
            }
        } else {
            sequelize.models.Report.createControlReport(user_id, control, 'error', `[제어보드] '${control.control_name}'에 할당된 정보가 없습니다.`);
            callback(false);
        }
    }
};

function send(user_id, control, state, urlList, callback) {
    let funcs = [];

    urlList.forEach(url => {
        funcs.push(subCallback => {
            request.get(url, (error, response, body) => {
                if (error) {
                    subCallback(error, false);
                } else {
                    subCallback(null, true);
                }
            });
        });
    });

    async.parallel(funcs, (error, results) => {
        if (error) {
            sequelize.models.Report.createControlReport(user_id, control, 'error', `[제어보드] '${control.control_name}' 신호 송신에 실패하였습니다.`);
            callback(false);
        } else {
            callback(true);
        }
    });
}
