const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const activeMotor = require('../methods/control').activeMotor;
const STD = require('../metadata/standards');
const CONFIG = require('../config');

const uploadsPath = path.join(__dirname, '../../uploads');
const dumpsPath = path.join(__dirname, '../../dumps');
const csvPath = path.join(__dirname, '../../csv');
const cctvPath = path.join(__dirname, '../../cctv');

const initPath = path.join(__dirname, '../../init.json');

const database = async () => {
    try {
        if (!await sequelize.models.CctvSetting.count({})) {
            await sequelize.models.CctvSetting.create({});
        }
        if (!await sequelize.models.SunDateDefault.count({})) {
            await sequelize.models.SunDateDefault.create(STD.sunDate.default);
        }
        if (fs.existsSync(initPath)) {
            const init = require(initPath);
            if (init.houses && !await sequelize.models.House.count({})) {
                await sequelize.models.House.bulkCreate(init.houses, {});
            }
            if (init.cctvs && !await sequelize.models.Cctv.count({})) {
                await sequelize.models.Cctv.bulkCreate(init.cctvs);
            }
            if (init.sensors && !await sequelize.models.Sensor.count({})) {
                await sequelize.models.Sensor.bulkCreate(init.sensors);
            }
            if (init.houseCctvRels && !await sequelize.models.HouseCctvRel.count({})) {
                await sequelize.models.HouseCctvRel.bulkCreate(init.houseCctvRels);
            }
            if (init.houseSensorRels && !await sequelize.models.HouseSensorRel.count({})) {
                await sequelize.models.HouseSensorRel.bulkCreate(init.houseSensorRels);
            }
            if (init.modes && !await sequelize.models.Mode.count({})) {
                await sequelize.models.Mode.bulkCreate(init.modes);
            }
            if (init.controls && !await sequelize.models.Control.count({})) {
                await sequelize.models.Control.bulkCreate(init.controls);
            }
            if (init.send_sensors && !await sequelize.models.SendSensor.count({})) {
                await sequelize.models.SendSensor.bulkCreate(init.send_sensors);
            }
        }

        await sequelize.models.PBand.destroyLegacyAutoControls();
        const queries = [
            `ALTER TABLE \`auto_controls\` MODIFY COLUMN \`type\` ENUM("time", "sensor", "control", "mix", "step", "table") NOT NULL DEFAULT "time"`,
            `ALTER TABLE \`cctvs\` MODIFY COLUMN \`type\` ENUM("normal", "legacy", "ptz") NOT NULL DEFAULT "normal"`,
            `ALTER TABLE \`auto_control_items\` MODIFY COLUMN \`control_id\` INTEGER(10) UNSIGNED NULL DEFAULT NULL`,
            `ALTER TABLE \`auto_control_steps\` MODIFY COLUMN \`control_id\` INTEGER(10) UNSIGNED NULL DEFAULT NULL`,
            `ALTER TABLE \`screens\` MODIFY COLUMN \`type\` ENUM(${STD.screen.enumTypes.map(type => ('"' + type + '"'))}) NOT NULL DEFAULT "${STD.screen.defaultType}"`,
            `ALTER TABLE \`control_groups\` MODIFY COLUMN \`direction\` ENUM("none", "left", "right") DEFAULT NULL`,
            `ALTER TABLE \`control_groups\` MODIFY COLUMN \`window_position\` ENUM("none", "1", "2", "3") DEFAULT NULL`,
        ];

        for (let i=0; i<queries.length; i++) {
            const query = queries[i];
            await sequelize.query(query, {});
        }

        let screen_query = `DELETE FROM screens WHERE LENGTH(type)<2 `;
        await sequelize.query(screen_query, {
            type: Sequelize.QueryTypes.DELETE
        });
        const houses = await sequelize.models.House.findAll({});
        if (await sequelize.models.Screen.count({}) !== STD.screen.enumTypes.length * houses.length) {
            let enumTypes = STD.screen.enumTypes.slice();
            let screens = [];
            houses.forEach(house => {
                enumTypes.forEach((type, order) => {
                    screens.push({
                        house_id: house.id,
                        type,
                        order
                    });
                });
            });

            await sequelize.models.Screen.bulkCreate(screens, {
                ignoreDuplicates: true
            });
        }

        let indexKeys = ['sensor_id', 'created_at', 'index_key'];
        let tableNames = await sequelize.query(`SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '${CONFIG.db.database}' AND TABLE_NAME LIKE 'log_sensor_%'`, {
            type: Sequelize.QueryTypes.SELECT
        });
        for (let i=0; i<tableNames.length; i++) {
            let tableName = tableNames[i].TABLE_NAME;
            for (let j=0; j<indexKeys.length; j++) {
                let indexKey = indexKeys[j];
                let check = await sequelize.query(`SHOW INDEX FROM ${tableName} WHERE Key_name = '${indexKey}'`, {
                    type: Sequelize.QueryTypes.SELECT
                });
                if (!check.length) {
                    if (indexKey === 'index_key') {
                        await sequelize.query(`ALTER TABLE ${tableName} ADD INDEX ${indexKey} (sensor_id, created_at)`, {
                            type: Sequelize.QueryTypes.RAW
                        });
                    } else {
                        await sequelize.query(`ALTER TABLE ${tableName} ADD INDEX ${indexKey} (${indexKey})`, {
                            type: Sequelize.QueryTypes.RAW
                        });
                    }
                }
            }
        }

        // const checkQuery = `SELECT COUNT(*) AS count from information_schema.referential_constraints WHERE constraint_name = 'auto_control_sensor_options_order'`;
        // if (!(await sequelize.query(checkQuery, {
        //     type: Sequelize.QueryTypes.SELECT
        // }))[0].count) {
        //     await sequelize.query(`ALTER TABLE auto_control_sensor_options ADD CONSTRAINT auto_control_sensor_options_order FOREIGN KEY (\`order\`) REFERENCES auto_control_sensors(\`order\`) ON UPDATE CASCADE ON DELETE CASCADE`);
        // }

        return true;
    } catch (e) {
        console.error(e);
        return false;
    }
};

const logic = async () => {
    try {
        const motors = await sequelize.models.Control.findAll({
            where: {
                type: 'motor',
            }
        });
        let promises = [];
        motors.forEach(motor => {
            promises.push(activeMotor({id: motor.id, state: 'stop', type: 'manual'}));
        });
        await Promise.all(promises);
        return true;
    } catch (e) {
        console.error(e);
        return false;
    }
};

const alarms = async () => {
    try {
        
        let sqls = [];
        let promises = [];
        let selectQuery=` SELECT *  FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME  = 'alarms' AND COLUMN_NAME = 'type' `;

        sqls.push(" ALTER TABLE `alarms` ADD COLUMN `name` CHAR(50) NULL AFTER `send_time` ");
        sqls.push(" ALTER TABLE `alarms` ADD COLUMN `type` ENUM('range', 'error') NOT NULL DEFAULT 'range' AFTER `name` ");
        sqls.push(" ALTER TABLE `alarms` ADD COLUMN `is_use` TINYINT(1) NULL DEFAULT '0' AFTER `type` ");
        sqls.push(" ALTER TABLE `alarms` ADD COLUMN `is_no` TINYINT(1) NULL DEFAULT '0' AFTER `is_use` ");
        sqls.push(" ALTER TABLE `alarms` ADD COLUMN `use_start_hour` TINYINT(1) NULL DEFAULT '0' AFTER `is_no` ");
        sqls.push(" ALTER TABLE `alarms` ADD COLUMN `use_end_hour` TINYINT(1) NULL DEFAULT '0' AFTER `use_start_hour` ");
        sqls.push(" ALTER TABLE `alarms` ADD COLUMN `no_start_hour` TINYINT(1) NULL DEFAULT '0' AFTER `use_end_hour` ");
        sqls.push(" ALTER TABLE `alarms` ADD COLUMN `no_end_hour` TINYINT(1) NULL DEFAULT '0' AFTER `no_start_hour` ");
        sqls.push(" ALTER TABLE `alarms` ADD COLUMN `cycle` TINYINT(1) NULL DEFAULT 5 AFTER `no_end_hour` ");
        sqls.push(" ALTER TABLE `alarms` ADD COLUMN `state` ENUM('use', 'unuse') NOT NULL DEFAULT 'use' AFTER `cycle` ");
        sqls.push(" ALTER TABLE `alarms` ADD COLUMN `repeat` INTEGER(10) UNSIGNED NOT NULL DEFAULT '1' AFTER `state` ");

        const count = await sequelize.query(selectQuery, {
            type: Sequelize.QueryTypes.SELECT
        });
        
        if(count.length==0){
            sqls.forEach(data => {
                
                promises.push(sequelize.query(data, {
                    type: Sequelize.QueryTypes.UPDATE
                }));
            });
            await Promise.all(promises);
        }
        return true;
    } catch (e) {
        console.error(e);
        return false;
    }
};

const logs = async () => {
    try {
        let now = new Date();
        let year = now.getFullYear();
        let month = now.getMonth() + 1;
        let table_name = 'log_sensor_' + year + (month < 10 ? '0' + month : month);

        let sqls = [];
        let promises = [];
        let selectQuery=" SELECT *  FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME  = '"+table_name+"' AND COLUMN_NAME = 'data' ";
        sqls.push(" ALTER TABLE `"+table_name+"` ADD COLUMN `data` INT(10) DEFAULT NULL AFTER `created_at`; ");

        const count = await sequelize.query(selectQuery, {
            type: Sequelize.QueryTypes.SELECT
        });
        if(count.length==0){
            sqls.forEach(data => {
                //console.log(data);
                promises.push(sequelize.query(data, {
                    type: Sequelize.QueryTypes.UPDATE
                }));
            });
            await Promise.all(promises);
        }
        return true;
    } catch (e) {
        console.error(e);
        return false;
    }
};

const sensors = async () => {
    try {
        let sqls = [];
        let promises = [];
        let selectQuery=` SELECT *  FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME  = 'sensors' AND COLUMN_NAME = 'type_id' `;

        sqls.push(" ALTER TABLE `sensors` ADD COLUMN `type_id` VARCHAR(191) NULL AFTER `updatedAt`; ");
        sqls.push(" ALTER TABLE `animal_types` DROP INDEX `animal_type_name_UNIQUE` , DROP INDEX `species_UNIQUE`; ");

        const count = await sequelize.query(selectQuery, {
            type: Sequelize.QueryTypes.SELECT
        });
        if(count.length==0){
            sqls.forEach(data => {
                //console.log(data);
                promises.push(sequelize.query(data, {
                    type: Sequelize.QueryTypes.UPDATE
                }));
            });
            await Promise.all(promises);
        }
        return true;
    } catch (e) {
        console.error(e);
        return false;
    }
};

module.exports.initDirectories = async () => {
    if (!fs.existsSync(uploadsPath)) fs.mkdirSync(uploadsPath);
    if (!fs.existsSync(dumpsPath)) fs.mkdirSync(dumpsPath);
    if (!fs.existsSync(csvPath)) fs.mkdirSync(csvPath);
    if (!fs.existsSync(cctvPath)) fs.mkdirSync(cctvPath);
    return true;
};
module.exports.initDatabase = async () => {
    if (await database()) {
        console.log('initialize database success');
    } else {
        console.log('initialize database fail');
    }

    return true;
};
module.exports.initLogic = async () => {
    if (await logic()) {
        console.log('initialize logic success');
    } else {
        console.log('initialize logic fail');
    }
    return true;
};
module.exports.initAlter = async () => {
    // if (await alarms()) {
    //     console.log('initialize alarms alter  success');
    // } else {
    //     console.log('initialize alarms alter  fail');
    // }
    // if (await sensors()) {
    //     console.log('initialize log_sensor alter  success');
    // } else {
    //     console.log('initialize log_sensor alter  fail');
    // }
    return true;
};
