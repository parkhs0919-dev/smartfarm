const tcpp = require('tcp-ping');
const request = require('request');
const Sequelize = require('sequelize');
const sequelize = require('./sequelize').sequelize;

module.exports.sendSignal = (user_id, control, state, callback) => {
    if ((control.is_topeng && control.address && control.io1 && control.io2 && (control.type === 'motor' ? control.io3 : true)) ||
        (!control.is_topeng && control.address && control.io1 && (control.type === 'motor' ? control.io2 : true))) {
        const address = control.address.split(':');
        if (address.length === 2) {
            const ip = address[0];
            const port = address[1];
            tcpp.probe(ip, port, (err, available) => {
                if (err) {
                    sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${ip}:${port}' 연결에 실패했습니다.`);
                    callback(false);
                } else {
                    if (available) {
                        send(user_id, control, state, ip, port, callback);
                    } else {
                        sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${ip}:${port}' 사용 불가능합니다.`);
                        callback(false);
                    }
                }
            });
        } else {
            sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.control_name}'에 할당된 정보가 잘못됐습니다.`);
            callback(false);
        }
    } else {
        sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.control_name}'에 할당된 정보가 없습니다.`);
        callback(false);
    }
};

function send(user_id, control, state, ip, port, callback) {
    request.put({
        uri: `http://${ip}:${port}/remoteio`,
        method: 'PUT',
        body: generateBody(control, state)
    }, (error, response, body) => {
        if (error) {
            console.error(error);
            sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.control_name}' 신호 송신에 실패하였습니다.`);
            callback(false);
        } else {
            if (response.statusCode === 200) {
                callback(true);
            } else {
                sequelize.models.Report.createControlReport(user_id, control, 'error', `[확장노드] '${control.control_name}' 신호 응답이 실패하였습니다.`);
                callback(false);
            }
        }
    });
}

function generateBody(control, state) {
    if (control.is_topeng) {
        if (control.type === 'motor') {
          return JSON.stringify({
              'type': 'motor',
              'mode': state,
              'fwd_io': control.io1,
              'stop_io': control.io2,
              'rwd_io': control.io3
          });
        } else if (control.type === 'power') {
            switch(state) {
                case 'on':
                    return JSON.stringify({
                        'type': 'consent',
                        'mode': 'on',
                        'io': control.io1,
                        'io2': control.io2
                    });
                case 'off':
                    return JSON.stringify({
                        'type': 'consent',
                        'mode': 'off',
                        'io': control.io1,
                        'io2': control.io2
                    });
                default:
                    return null;
            }
        } else if (control.type === 'control') {
            switch(state) {
                case 'on':
                    return JSON.stringify({
                        'type': 'consent',
                        'mode': 'on',
                        'io': control.io1,
                    });
                case 'off':
                    return JSON.stringify({
                        'type': 'consent',
                        'mode': 'off',
                        'io': control.io1,
                    });
                default:
                    return null;
            }
        } else {
            return null;
        }
    } else {
        switch(control.type) {
            case 'motor':
                return JSON.stringify({
                    'type': 'motor',
                    'mode': returnMode(control.type, state),
                    'fwd_io': control.io1,
                    'rwd_io': control.io2
                });
            case 'power':
                return JSON.stringify({
                    'type': 'consent',
                    'mode': returnMode(control.type, state),
                    'io': control.io1
                });
            case 'control':
                return JSON.stringify({
                    'type': 'localmode',
                    'mode': returnMode(control.type, state),
                    'io': control.io1
                });
            default:
                return null;
        }
    }
}

function returnMode(type, state) {
    if (type === 'motor') {
        switch(state) {
            case 'open':
                return 'fwd';
            case 'stop':
                return 'stop';
            case 'close':
                return 'rwd';
            default:
                return state;
        }
    } else {
        return state;
    }
}
