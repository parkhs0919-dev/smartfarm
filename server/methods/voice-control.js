const Sequelize = require('sequelize');
const sequelize = require('./sequelize').sequelize;
const CONFIG = require('../config');
const LANG = require('../metadata/langs').ko;
const STD = require('../metadata/standards');
let io = require('./socket-io').getIo();

function sendSignal(user_id, control, state  ) {
    return new Promise((resolve, reject) => {        
        require('./voice-control-ip').sendSignalTime(user_id, control, state, (isSuccess) => {
            if (isSuccess) {  
                resolve(true); 
            } else {
                reject(false);            
            }  
        }); 
    });        
}  

function sendSignalDelay(time) {
    return new Promise((resolve, reject) => {  
        require('./voice-control-ip').sendSignalDelay(time, (isSuccess) => {
            if (isSuccess) {     
                resolve(true); 
            } else {     
                reject(false);          
            }  
        });  
    });        
}

function sendClearTimeoutInstance(id) {
    return new Promise((resolve, reject) => {  
        require('./voice-control-ip').clearTimeoutInstance(id, (isSuccess) => {
            if (isSuccess) {   
                resolve(true); 
            } else {       
                reject(false);          
            }  
        });    
    });        
}

function sendAllClearTimeoutInstance() {
    return new Promise((resolve, reject) => {  
        require('./voice-control-ip').allClearTimeoutInstance( (isSuccess) => {
            if (isSuccess) {   
                resolve(true);   
            } else {       
                reject(false);          
            }         
        });    
    });        
}  

module.exports = async () => {
};                              
    
module.exports.activeVoiceControl = ({id, type, voice_code ,action, address ,io1 ,io2}) => {
     return new Promise(async (resolve, reject) => {
        try {      
            const user_id = CONFIG.defaultUser.id;
            const house_id = 1;
            let voiceControl = {
                house_id: house_id, 
                address: address,
                type: type,
                io1: io1,
                io2: io2,  
                time: 0
            };  
            console.log(voiceControl);
            await sendSignal(user_id, voiceControl, action);
            // await createPestControlReport(user_id,house_id,null,voiceControl.type,generateContents('따라와', action));
            // await sendSignalDelay(generateTime(1));
         
            resolve(true);
        } catch (e) {
            console.error('active voice-control fail ', e);
            reject(e);
        }
        });
          
};    
 
async function createVoiceControlReport(user_id,house_id,control_id,type,contents) {
    return new Promise(async (resolve, reject) => {
        try {
            const createdData = await sequelize.models.Report.create({
                user_id:user_id,     
                house_id: house_id,  
                control_id: control_id,            
                type: type,
                contents: contents  
            });   
            resolve(true);
        } catch (e) {
            reject(e);
        }
    });   
} 

function generateContents(name, state) {
    let stateText = LANG[state];
    if (state === 'open' ) stateText = `열림`;
    else if ( state === 'on') stateText = `가동`;
    else if (state === 'stop' || state === 'off') stateText = `정지`;
    else if (state === 'close' ) stateText = `닫기`;
   
    return `'${name}'가(이) '${stateText}' 동작합니다.`;
}  

function generateTime(time) {
    return time * 1000;
}