const socketIO = require('socket.io');

let io = null;

module.exports = (server) => {
    io = socketIO(server);
    io.on('connection', socket => {
        console.log('User connected');

        socket.on('disconnect', () => {
            console.log('user disconnected');
        });
    });
};
module.exports.getIo = () => {
    return io;
};
