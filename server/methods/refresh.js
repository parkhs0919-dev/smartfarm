let current;

module.exports = {
    setCurrent: () => {
        current = new Date().getTime();
    },
    getCurrent: () => {
        if (!current) {
            current = new Date().getTime();
        }
        return current;
    }
};
