const arrAdcTemp = require('./convert/arrAdcTemp');
const arrAdcHumidity = require('./convert/arrAdcHumidity');
const arrValueBattery = require('./convert/arrValueBattery');

module.exports = {
    'ph-1': (value) => {
        //return ((value * 5.0) / 1024) * 3.5;
        //return ((value / 4096) * 14) * 4;
        return (((value / 4096 ) * 4 ) * 5) * 3.5;
    },
    'co2-1': (value) => {
        //return ((value * 2) / 1024) * 2000;
        //return (value / 1024) * 3000;
        return ((value / 4096) * 2000) * 4;
    },
    'liter-1': (value) => {
        return value * 10;
    },
    'battery-1': (value) => {
        // let volts = ((value / 4096) * 5) / 3.3 * 12;
        // let resultper = ((volts - 6) / 6) * 100;

        // if(resultper > 100){
        //   resultper = 100;
        // }else if(resultper <= 0){
        //   resultper = 0;
        // }
        let result;
        let data = (((value / 4096) * 5) * 4).toFixed(1);
        for (let i in arrValueBattery) {
            let volt = parseFloat(arrValueBattery[i].volt);
            let per = parseFloat(arrValueBattery[i].value);

            if (i == (arrValueBattery.length - 1) || i === 0) {
                if (volt > data) {
                    result = 0;
                    break;
                } else if (volt < data) {
                    result = 100;
                    break;
                } else {
                    result = per;
                    break;
                }
            } else {
                if (volt == data) {
                    result = per;
                    break;
                }
            }
        }
        return result;
    },
    'lux-1': (value) => {
        //return ((value * 2) / 1024) * 1200;
        //return ((value / 4096) * 1200) * 4;
        let data = (((value * 2) / 1024) * 1200) - 14;
        if(data <= 0){
            return 0;
        }else{
            return (((value * 2) / 1024) * 1200) * 100 /52.16;
        }

    },
    'windSpeed-1': (value) => {
        // let sensorValue = 0;
        // let sensorVoltage = 0;
        // let voltageConversionConstant = .004882814;
        // let voltageMin = .13;
        // let windSpeedMin = 0;
        // let voltageMax = 2.5;
        // let windSpeedMax = 32;
        // sensorValue = value;
        // sensorVoltage = sensorValue * voltageConversionConstant;
        // if (sensorVoltage <= voltageMin) {
        //     return 0;
        // } else {
        //     return (sensorVoltage - voltageMin) * windSpeedMax / (voltageMax - voltageMin);
        // }
        return ((value / 4096) * 32.4) * 4;
    },
    'windSpeed-2': (value) => {
        return (value * 0.44704).toFixed(2);    
    },     
    'windDirection-1': (value) => {
        // let Direction;
        // let CalDirection;
        // Direction = ((value * 2) / 1024) * 360;
        // CalDirection = Direction;
        // if (CalDirection > 360) CalDirection = CalDirection - 360;
        // if (CalDirection < 0) CalDirection = CalDirection + 360;
        // return CalDirection;
        return ((value / 4096) * 360) * 4;
    },
    'temperature-1': (temp) => {
        if (parseInt(temp) > 100) {
            let result;
            for (let i in arrAdcTemp) {
                let rnum = Number(i) + 1;

                if (i == (arrAdcTemp.length - 1)) {
                    let f_adc = Number(arrAdcTemp[i].data);
                    let f_tp = Number(arrAdcTemp[i].name);
                    if (f_adc > temp) {
                        // console.log('tp===100');
                        result = f_tp;
                    }
                } else {
                    let f_adc = Number(arrAdcTemp[i].data);
                    let r_adc = Number(arrAdcTemp[rnum].data);
                    let f_tp = Number(arrAdcTemp[i].name);
                    let r_tp = Number(arrAdcTemp[rnum].name);

                    if (f_adc >= temp && r_adc < temp) {
                        let per = (f_adc - temp) / (f_adc - r_adc);
                        // console.log('f_tp===' + f_tp);
                        // console.log('r_tp===' + r_tp);
                        // console.log('f_adc===' + f_adc);
                        // console.log('r_adc===' + r_adc);

                        // console.log(f_adc + '=============' + r_adc);
                        // console.log('t=============' + (f_adc - r_adc));
                        // console.log('h=============' + (f_adc - temp));

                        // console.log('per===' + per);
                        if (f_tp < 0) {
                            result = -((Number(Math.abs(f_tp)) + Number(per))).toFixed(1);

                        } else {
                            result = (Number(Math.abs(f_tp)) + Number(per)).toFixed(1);

                        }
                        break;
                        // console.log('result 1111==='+result);
                    } else if (f_adc < temp) {
                        result = f_tp;
                        break;
                    }
                }
            }
            return result;
        } else {
            return temp;
        }
    },
    'humidity-1': (temp) => {
        if (parseInt(temp) > 100) {
            let result;
            for (let i in arrAdcHumidity) {
                let rnum = Number(i) + 1;

                if (i == (arrAdcHumidity.length - 1)) {
                    let f_adc = Number(arrAdcHumidity[i].data);
                    let f_tp = Number(arrAdcHumidity[i].name);
                    if( f_adc<temp){
                        //console.log('tp===100');
                        result=f_tp;
                    }
                } else {
                    let f_adc = Number(arrAdcHumidity[i].data);
                    let r_adc = Number(arrAdcHumidity[rnum].data);
                    let f_tp = Number(arrAdcHumidity[i].name);
                    let r_tp = Number(arrAdcHumidity[rnum].name);

                    if(f_adc<=temp && r_adc>temp){
                        let per=(temp-f_adc)/(r_adc-f_adc);
                        // console.log('f_tp==='+f_tp);
                        // console.log('r_tp==='+r_tp);
                        // console.log('f_adc==='+f_adc);
                        // console.log('r_adc==='+r_adc);

                        // console.log(f_adc+'============='+r_adc);
                        // console.log('t============='+(f_adc-r_adc));
                        // console.log('h============='+(f_adc-temp));

                        // console.log('per==='+per);

                        result=(Number(Math.abs(f_tp))+Number(per)).toFixed(1);
                        break;
                    } else if (f_adc > temp) {
                        result = f_tp;
                        break;
                    }
                }
            }
            return result;
        } else {
            return temp;
        }
    },
    'division-1': (value) => {
        return (value / 10).toFixed(2) ;
    },       
    'division-2': (value) => {
        return (value / 100).toFixed(2) ;
    }, 
    'korins-fall-1': (value) => {
        return ((value / 100) * 25.4).toFixed(1) ;
    },      
};
