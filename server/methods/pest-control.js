const Sequelize = require('sequelize');
const sequelize = require('./sequelize').sequelize;
const CONFIG = require('../config');
const LANG = require('../metadata/langs').ko;
const STD = require('../metadata/standards');
const PEST_CONTROL_DATA = require('../config').pest_control;
let io = require('./socket-io').getIo();

let pestControl = {};   
let pestControlZone = {};
let isControlProcessing = {};  
let isResetProcessing = {};
let isResetDataProcessing = {};
let isProcessing = {};   
let sensor_data = {}; 
let pest_control_config={};
let isUseType = {};  
let isUseFrostType = {}; 
  
function sendSignal(user_id, control, state ,isUse ) {
    return new Promise((resolve, reject) => {     
        if(isProcessing[control.house_id] && isUse){       
            require('./pest-control-ip').sendSignalTime(user_id, control, state, (isSuccess) => {
                if (isSuccess) {  
                    resolve(true); 
                } else {
                    reject(false);            
                }  
            }); 
        }else{  
            resolve(true);
        }  
    });        
}  
function sendSignalDelay(time) {
    return new Promise((resolve, reject) => {  
        require('./pest-control-ip').sendSignalDelay(time, (isSuccess) => {
            if (isSuccess) {   
                resolve(true); 
            } else {     
                reject(false);          
            }  
        });  
    });        
}
function sendClearTimeoutInstance(id) {
    return new Promise((resolve, reject) => {  
        require('./pest-control-ip').clearTimeoutInstance(id, (isSuccess) => {
            if (isSuccess) {   
                resolve(true); 
            } else {       
                reject(false);          
            }  
        });    
    });        
}
function sendAllClearTimeoutInstance() {
    return new Promise((resolve, reject) => {  
        require('./pest-control-ip').allClearTimeoutInstance( (isSuccess) => {
            if (isSuccess) {   
                resolve(true);   
            } else {       
                reject(false);          
            }         
        });    
    });        
}  

module.exports = async () => {
   await getPestControlData();
   await getHouseData();  
   await setSensorData();
   await getPestcontrolConfig();
};           
                  
module.exports.getPestControl = (house_id) => {
    return pestControl[house_id];
};
 
module.exports.getPestControlZone = () => {
    return pestControlZone;
};         
  
module.exports.getIsControlProcessing = (house_id) => {
    return isControlProcessing[house_id];
};

module.exports.getIsResetProcessing = (house_id) => {
    return isResetProcessing[house_id];  
};  
module.exports.getIsResetDataProcessing = (house_id) => {
    return isResetDataProcessing[house_id];  
};
   
module.exports.setIsControlProcessing = async (house_id,id,state) => {
    isProcessing[house_id] = state;     
    await getPestControlData();
    await getHouseData2();                            
    if(!state) isResetProcessing[house_id] = true
    else  isResetProcessing[house_id] = false;        
    return isControlProcessing[house_id] = state;
};   

module.exports.setSendIsControlProcessing = async (house_id,state) => {
    isControlProcessing[house_id] =  state;   
}; 

module.exports.setIsResetProcessing = async (house_id,state) => {
    await getPestControlData();
    await getHouseData2();           
    return isResetProcessing[house_id] = state;    
};   

module.exports.setIsResetDataProcessing = async (house_id,state) => {
    await getPestControlData();
    await getHouseData2();           
    return isResetDataProcessing[house_id] = state;    
};       

module.exports.setPestControlData = async () => {
    await getPestControlData();
    return isResetProcessing[house_id] = true;     
};      

module.exports.getPestControlConfig = () => {   
    return pest_control_config ;                  
};                        
    
module.exports.activePestControl = (house_id, id ) => {
     return new Promise(async (resolve, reject) => {
        try {  
            isControlProcessing[house_id] = false;          
            const  user_id =CONFIG.defaultUser.id;
            let pestControl={
                house_id: house_id,
                address :'192.168.1.185:8801',
                type : 'power',
                io1 :'24',  
                io2 :'',   
                time : 0   
            };                        
            const pestControlZone =  await sequelize.models.PestControlZone.findById(id);   
            const pestControlZoneOne =  await sequelize.models.PestControlZone.getPestControlZoneOrderByOne(house_id,id);  
            pestControl.id=pestControlZone.id;                       
            if(pestControlZone && pestControlZone.is_frost==0){  
                isUseType[house_id] = true;
                await sendSignal(user_id, pestControlZone, 'stop',isUseType[house_id]);  
                await sendSignal(user_id, pestControlZone, 'open',isUseType[house_id]);
                await createPestControlReport(user_id,house_id,null,pestControlZone.type,generateContents(pestControlZone.pest_control_zone_name, 'open'));
                await sequelize.models.PestControlZone.update({state: 'open'}, {where: {id: id}});     
                //약제 모터 n초 on          
                pestControl.io1=pest_control_config.medicine_io; 
                pestControl.address=pest_control_config.medicine_address;     
                pestControl.time =generateTime(pestControlZone.zone_time);            
                await sendSignal(user_id, pestControl, 'on',isUseType[house_id]);  
                await createPestControlReport(user_id,house_id,null,pestControl.type,generateContents('약재 모터', 'on'));
     
                //약재 n초 off     
                pestControl.io1=pest_control_config.medicine_io;  
                pestControl.address=pest_control_config.medicine_address;   
                pestControl.time =generateTime(pestControlZone.medicine_time); 
                await sendSignal(user_id, pestControl, 'off',isUseType[house_id]); 
                await createPestControlReport(user_id,house_id,null,pestControl.type,generateContents('약재 모터', 'off'));

                await sendSignalDelay(generateTime(1));  
                //컴프레셔 열기    
                pestControl.type='motor';   
                pestControl.io1=pest_control_config.compressor_io1; 
                pestControl.io2=pest_control_config.compressor_io2; 
                pestControl.address=pest_control_config.compressor_address;        
                pestControl.time =0;
                await sendSignal(user_id, pestControl, 'open',isUseType[house_id]);  
                await createPestControlReport(user_id,house_id,null,pestControl.type,generateContents('컴플레셔', 'open'));       
                //컴프레셔 n초 닫기    
                pestControl.type='motor';       
                pestControl.io1=pest_control_config.compressor_io1; 
                pestControl.io2=pest_control_config.compressor_io2; 
                pestControl.address=pest_control_config.compressor_address;       
                pestControl.time =generateTime(pestControlZone.compressor_time);  
                await sendSignal(user_id, pestControl, 'stop',isUseType[house_id]);
                await sendSignalDelay(generateTime(1));                         
                await sendSignal(user_id, pestControl, 'close',isUseType[house_id]); 
                await createPestControlReport(user_id,house_id,null,pestControl.type,generateContents('컴플레셔', 'close'));    
                
                await sendSignalDelay(generateTime(1));
                pestControl.time =0; 
                await sendSignal(user_id, pestControlZone, 'stop',isUseType[house_id]);
                await sendSignalDelay(generateTime(1));         
                await sendSignal(user_id, pestControlZone, 'close',isUseType[house_id]);       
                await createPestControlReport(user_id,house_id,null,pestControlZone.type,generateContents(pestControlZone.pest_control_zone_name, 'close'));
                await sequelize.models.PestControlZone.update({state: 'close'}, {where: {id: id}});                
                 
                if(pestControlZoneOne.id==id){
                    isProcessing[house_id] = false;         
                    isControlProcessing[house_id] = false;
                    isResetProcessing[house_id] = true;  
                    isResetDataProcessing[house_id]=true;
                    isUseType[house_id] = false;
                    await sequelize.models.PestControl.update({state: 'stop'}, {where: {id: pestControlZoneOne.pest_control_id}});
                    syncIo(() => {                 
                        io.emit('report', {house_id: house_id }); 
                        io.emit('pest-control',{key:pestControlZoneOne.pest_control_id,state:'stop'});
                    });      
                }else{     
                    await sendSignalDelay(generateTime(pestControlZone.zone_time));  
                    isControlProcessing[house_id] = isProcessing[house_id];
                    isUseType[house_id] = false;
                }   
                
            }else if(pestControlZone && pestControlZone.is_frost){
                isUseFrostType[house_id] = true;
                let query = `SELECT AA.house_id, AA.pest_control_name,AA.zone_time,AA.medicine_time,AA.compressor_time,AA.is_frost,AA.is_auto, BB.*  FROM pest_controls as AA  `;
                query += `inner join pest_control_zone as BB on AA.id=BB.pest_control_id  `;
                query += `WHERE AA.house_id=${house_id} and AA.state='run' and AA.is_frost=1 and BB.is_use=1  `;
    
                const data = await sequelize.query(query, {   
                    type: Sequelize.QueryTypes.SELECT
                });   
                if(await isCheck(pestControlZone)){                            
                    for (let i = 0; i < data.length; i++) {       
                        await sendSignal(user_id, data[i], 'stop',isUseFrostType[house_id]);
                    }    
                    await sendSignalDelay(generateTime(1));                        
                    for (let i = 0; i < data.length; i++) {     
                        await sendSignal(user_id, data[i], 'open',isUseFrostType[house_id]);
                        await createPestControlReport(user_id,house_id,null,data[i].type,generateContents(data[i].pest_control_zone_name, 'open'));  
                    }                  
                    await sendSignalDelay(generateTime(1));  
                    //컴프레셔 열기                            
                    pestControl.type='motor';   
                    pestControl.io1=pest_control_config.compressor_io1; 
                    pestControl.io2=pest_control_config.compressor_io2; 
                    pestControl.address=pest_control_config.compressor_address;             
                    pestControl.time =0;  
                    await sendSignal(user_id, pestControl, 'stop',isUseFrostType[house_id]);
                    await sendSignalDelay(generateTime(1)); 
                    await sendSignal(user_id, pestControl, 'open',isUseFrostType[house_id]);  
                    await createPestControlReport(user_id,house_id,null,pestControl.type,generateContents('컴플레셔', 'open'));       
                    //컴프레셔 n초 닫기          
                    pestControl.type='motor';     
                    pestControl.io1=pest_control_config.compressor_io1; 
                    pestControl.io2=pest_control_config.compressor_io2; 
                    pestControl.address=pest_control_config.compressor_address;                 
                    pestControl.time =generateTime(pestControlZone.compressor_time); 
                    await sendSignal(user_id, pestControl, 'stop',isUseFrostType[house_id]);   
                    await sendSignalDelay(generateTime(1));     
                    await sendSignal(user_id, pestControl, 'close',isUseFrostType[house_id]); 
                    await createPestControlReport(user_id,house_id,null,pestControl.type,generateContents('컴플레셔', 'close'));    
      
                    await sendSignalDelay(generateTime(1));  
                    for (let i = 0; i < data.length; i++) {   
                        await sendSignal(user_id, data[i], 'stop',isUseFrostType[house_id]);
                    }  
                    await sendSignalDelay(generateTime(1));    
                    for (let i = 0; i < data.length; i++) {   
                        await sendSignal(user_id, data[i], 'close',isUseFrostType[house_id]);  
                        await createPestControlReport(user_id,house_id,null,data[i].type,generateContents(data[i].pest_control_zone_name, 'close'));   
                    }                           
                    await sendSignalDelay(generateTime(1));  
                    if(pestControlZone.is_auto==1){
                        await sendSignalDelay(generateTime(pestControlZone.delay)); 
                        isControlProcessing[house_id] = isProcessing[house_id]; 
                        isUseFrostType[house_id] = false;   
                        syncIo(() => {
                            io.emit('report', {house_id: house_id }); 
                        });                                                
                    }else{  
                        await sendSignalDelay(generateTime(pestControlZone.zone_time)); 
                        isControlProcessing[house_id] = isProcessing[house_id]; 
                        isUseFrostType[house_id] = false;     
                        syncIo(() => {
                            io.emit('report', {house_id: house_id }); 
                        });       
                    } 
                }   
                isUseFrostType[house_id] = false;                 
            }     
            resolve(true);           
        } catch (e) {    
            console.error('active pest-control fail ', e); 
            isResetProcessing[house_id]=true;
            reject(e);     
        }            
    });        
          
};    

module.exports.reSetPestControl = (house_id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const user_id = CONFIG.defaultUser.id;            
            let pestControl = {
                house_id: house_id,
                address: '192.168.1.185',
                type: 'power',
                io1: '',
                io2: '',
                time: 0
            };
            isControlProcessing[house_id] = false;            
            isProcessing[house_id] = true;   
            isUseType[house_id] = false;   
            isUseFrostType[house_id] = false;
            let query = `SELECT AA.house_id, AA.pest_control_name,AA.zone_time,AA.medicine_time,AA.compressor_time,AA.is_frost,AA.is_auto, BB.*  FROM pest_controls as AA  `;
            query += `inner join pest_control_zone as BB on AA.id=BB.pest_control_id  `;
            query += `WHERE AA.house_id=${house_id}  and BB.is_use=1  `;
  
            const data = await sequelize.query(query, {   
                type: Sequelize.QueryTypes.SELECT
            });
 
            if(data.length>0){
                await sequelize.models.PestControl.update({state: 'stop'}, {where: {house_id: house_id}});  
                //await createPestControlReport(user_id,house_id,null,data[0].type,generateContents(data[0].pest_control_name, 'stop'));       
                syncIo(() => {      
                    io.emit('pest-control',{key:data[0].pest_control_id,state:'stop'});
                });                           
                // for (let i = 0; i < data.length; i++) {
                //     await sendClearTimeoutInstance(data[i].id);                      
                // }               
                await sendAllClearTimeoutInstance();  
                await sendSignalDelay(generateTime(1));  
                //약재 밸브 닫기   
                pestControl.id=house_id;     
                pestControl.io1=pest_control_config.medicine_io; 
                pestControl.address=pest_control_config.medicine_address;   
                await sendSignal(user_id, pestControl, 'off',true);  
                await createPestControlReport(user_id, house_id, null, pestControl.type, generateContents('약재 모터', 'off'));
                
                //컴플레셔 밸브 닫기 
                pestControl.type='motor';   
                pestControl.io1=pest_control_config.compressor_io1; 
                pestControl.io2=pest_control_config.compressor_io2; 
                pestControl.address=pest_control_config.compressor_address; 
                await sendSignal(user_id, pestControl, 'stop',true);   
                await sendSignal(user_id, pestControl, 'close',true);
                await createPestControlReport(user_id, house_id, null, pestControl.type, generateContents('컴플레셔 모터', 'close'));
                //정체 구역 닫기           
                //  setTimeout clear 
                for (let i = 0; i < data.length; i++) {
                    await sendSignal(user_id, data[i], 'stop',true);
                }   
                await sendSignalDelay(generateTime(1)); 
                for (let i = 0; i < data.length; i++) {  
                    await sendSignal(user_id, data[i], 'close',true);
                    //await createPestControlReport(user_id,house_id,null,data[i].type,generateContents(data[i].pest_control_zone_name, 'close')); 
                }    
                await sendAllClearTimeoutInstance();  
                await sendSignalDelay(generateTime(1));                                
                await sequelize.models.PestControlZone.update({
                    state: 'close'   
                }, {
                    where: {   
                        pest_control_id: data[0].pest_control_id
                    }
                });   
                isResetProcessing[house_id]=false;                
                isProcessing[house_id] = false;   
                syncIo(() => {
                    io.emit('pest-control-fail');
                });       
            }         
            resolve(true);  
        } catch (e) {
            console.error('active reSetPestControl fail ', e);
            isResetProcessing[house_id]=false;
            syncIo(() => {
                io.emit('pest-control-fail');
            });        
            reject(e);
        }      
    });
};

module.exports.sendPestControlMedicineOff = (house_id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const user_id = CONFIG.defaultUser.id;            
            let pestControl = {
                house_id: house_id,
                address: '192.168.1.185',
                type: 'power',  
                io1: '',
                io2: '',
                time: 0   
            };  
            let query = `SELECT count(*) as cnt from pest_controls as AA where state='run' `;
            const data = await sequelize.query(query, {   
                type: Sequelize.QueryTypes.SELECT
            });
            if(data[0].cnt==0){
                console.error('--- active sendPestControlMedicineOff -----  ');
                isControlProcessing[house_id] = false;            
                isProcessing[house_id] = true;                    
                await sendAllClearTimeoutInstance();          
                pestControl.id=house_id;     
                pestControl.io1=pest_control_config.medicine_io; 
                pestControl.address=pest_control_config.medicine_address;      
                await sendSignal(user_id, pestControl, 'off');  
                //await createPestControlReport(user_id, house_id, null, pestControl.type, generateContents('약재 모터', 'off'));                    
                isProcessing[house_id] = false; 
            }      
            resolve(true);  
        } catch (e) {
            console.error('active sendPestControlMedicineOff fail ', e);
            isProcessing[house_id] = false;    
            reject(e);
        }            
    });
};

async function getPestControlData() {
    const query = `SELECT AA.house_id,AA.is_frost,AA.is_auto, BB.* FROM pest_controls as AA inner join pest_control_zone as BB on AA.id=BB.pest_control_id where AA.state='run' and  BB.is_use=1 order by BB.id ASC `;
    const data = await sequelize.query(query, {
        type: Sequelize.QueryTypes.SELECT
    });  
    pestControl={};
    pestControlZone={};
    for (let i=0; i<data.length; i++) {
        if (pestControl[data[i].house_id] === undefined) {
            pestControl[data[i].house_id]={};
            pestControl[data[i].house_id][data[i].id]={};   
            pestControl[data[i].house_id][data[i].id]=false;           
        }else{
            pestControl[data[i].house_id][data[i].id]=false; 
        }     
             
        if (pestControlZone[data[i].id] === undefined) {
            pestControlZone[data[i].id]={};
            pestControlZone[data[i].id]=data[i];     
        }else{
            pestControlZone[data[i].id]=data[i];  
        }         
    }   
}     
 
async function getHouseData() {
    const query = `SELECT * from houses `;
    const data = await sequelize.query(query, {
        type: Sequelize.QueryTypes.SELECT
    });
    for (let i=0; i<data.length; i++) {
        if (isControlProcessing[data[i].id] === undefined) {
            isResetProcessing[data[i].id]=false;
            isResetDataProcessing[data[i].id]=false;
            if(pestControl[data[i].id]){
                isControlProcessing[data[i].id]=true;
                isProcessing[data[i].id]=true;    
                isUseType[data[i].id]=false;
                isUseFrostType[data[i].id]=false;
            }else{      
                isControlProcessing[data[i].id]=false;
                isProcessing[data[i].id]=false;  
                isUseType[data[i].id]=false; 
                isUseFrostType[data[i].id]=false;  
            }   
            
            
        } 
    }        
} 
async function getHouseData2() {
    const query = `SELECT * from houses `;
    const data = await sequelize.query(query, {
        type: Sequelize.QueryTypes.SELECT
    });
    for (let i=0; i<data.length; i++) {
        if (isControlProcessing[data[i].id] === undefined) {
            isResetProcessing[data[i].id]=false;
            isResetDataProcessing[data[i].id]=false;
            isControlProcessing[data[i].id]=false;       
        } 
    }          
}   
async function isCheck(control){
    let result = false;
    await setSensorData(); 
    if(control.is_auto===1){
        if((parseFloat(sensor_data.max_temp)<=parseFloat(control.max_temp)) && (parseFloat(sensor_data.midnight_tempemp)<=parseFloat(control.midnight_temp))
         && parseFloat(sensor_data.current_temp)<=parseFloat(control.current_temp) ){
            result = true;
        }       
    }else{
        result = true;;
    }    
    return result;      
}          
     
async function setSensorData(){
    try{
        let now = new Date()    
        let year = now.getFullYear(),
        month = now.getMonth() + 1,
        date = now.getDate(),
        hour = now.getHours(),
        minute = now.getMinutes();
        let table_name = 'log_sensor_' + year + (month < 10 ? '0' + month : month) ;
        let current_date=year + (month < 10 ? '0' + month : month)+ (date < 10 ? '0' + date : date);
        
        if(sensor_data.date != current_date){
            sensor_data.date=current_date;       
            let query = `SELECT max(value) as max_temp FROM ${table_name}  `;
            query += ` where  DATE_FORMAT(created_at,  '%Y%m%d') = DATE_FORMAT(date_add(now(), interval -1 day),  '%Y%m%d') `;
            query += ` and position='out' and type='temperature' `;
            const data1 = await sequelize.query(query, {
                type: Sequelize.QueryTypes.SELECT
            });   
            if(data1[0]) sensor_data.max_temp=data1[0].max_temp;  
              
            query = `SELECT  AA.* FROM ${table_name}  as AA `;
            query += ` where  AA.position='out' and AA.type='temperature' `;
            query += ` and DATE_FORMAT(AA.created_at,  '%Y%m%d%H%i%s') ='${current_date}000000' `;
            query += ` limit 1 `;     
    
            const data2 = await sequelize.query(query, {   
                type: Sequelize.QueryTypes.SELECT   
            });     
    
             
            if(data2[0]) sensor_data.midnight_temp=data2[0].value;
        } 
        query = ` select * from sensors  where  position='out' and type='temperature' limit 1 `;
        const data = await sequelize.query(query, {  
            type: Sequelize.QueryTypes.SELECT
        });  
        if(data[0]) sensor_data.current_temp = data[0].value;  
        
        // sensor_data.max_temp=30;  
        // sensor_data.current_temp=20;  
        // sensor_data.midnight_temp=15; 
    }catch(e){
       console.log(e);
    }
         
}    

async function sendReSetPestControl(house_id,id) {
    return new Promise(async (resolve, reject) => {
        try {
            const user_id = CONFIG.defaultUser.id;            
            let pestControl = {
                house_id: house_id,
                address: '192.168.1.185',
                type: 'power',
                io1: '',
                io2: '',  
                time: 0
            };
            isControlProcessing[house_id] = false;  
            isProcessing[house_id] = true;   
            let query = `SELECT AA.house_id, AA.pest_control_name,AA.zone_time,AA.medicine_time,AA.compressor_time,AA.is_frost,AA.is_auto, BB.*  FROM pest_controls as AA  `;
            query += `inner join pest_control_zone as BB on AA.id=BB.pest_control_id  `;
            query += `WHERE AA.house_id=${house_id}   and BB.is_use=1  `;

            const data = await sequelize.query(query, {
                type: Sequelize.QueryTypes.SELECT
            });
            if(data.length>0){
                await sequelize.models.PestControl.update({state: 'stop'}, {where: {house_id: house_id}});  
                //await createPestControlReport(user_id,house_id,null,data[0].type,generateContents(data[0].pest_control_name, 'stop'));       
                syncIo(() => {
                    io.emit('pest-control',{key:data[0].pest_control_id,state:'stop'});
                });                     
                for (let i = 0; i < data.length; i++) {
                    await sendClearTimeoutInstance(data[i].id);                      
                }
                //약재 밸브 닫기     
                pestControl.io1=pest_control_config.medicine_io; 
                pestControl.address=pest_control_config.medicine_address;   
                await sendSignal(user_id, pestControl, 'off');  
                //await createPestControlReport(user_id, house_id, null, pestControl.type, generateContents('약재 모터', 'off'));

                //컴플레셔 밸브 닫기 
                pestControl.type='motor';   
                pestControl.io1=pest_control_config.compressor_io1; 
                pestControl.io2=pest_control_config.compressor_io2; 
                pestControl.address=pest_control_config.compressor_address; 
                await sendSignal(user_id, pestControl, 'stop');   
                await sendSignalDelay(generateTime(1)); 
                await sendSignal(user_id, pestControl, 'close');
                //await createPestControlReport(user_id, house_id, null, pestControl.type, generateContents('컴플레셔 모터', 'close'));
                //정체 구역 닫기 
                //  setTimeout clear   
                for (let i = 0; i < data.length; i++) {
                    await sendSignal(user_id, data[i], 'stop');
                }   
                await sendSignalDelay(generateTime(1)); 
                for (let i = 0; i < data.length; i++) {
                    await sendSignal(user_id, data[i], 'close');
                }                  
                await sequelize.models.PestControlZone.update({
                    state: 'close'
                }, {
                    where: {
                        pest_control_id: data[0].pest_control_id
                    }
                }); 
                
                isResetProcessing[house_id]=false;  
                isProcessing[house_id] = false 
                // syncIo(() => {      
                //     io.emit('pest-control-fail');
                // });       
            }         
            resolve(true);  
        } catch (e) {
            console.error('active reSetPestControl fail ', e);
            isResetProcessing[house_id]=false;
            syncIo(() => {
                io.emit('pest-control-fail');
            });        
            reject(e);
        }     
    });                  
}         
 
async function getPestcontrolConfig() {
    const query1 = `select * from pest_control_config where code="medicine"  `;
    const medicine = await sequelize.query(query1, {
        type: Sequelize.QueryTypes.SELECT
    });
    const query2 = `select * from pest_control_config where code="compressor"  `;
    const compressor = await sequelize.query(query2, {  
        type: Sequelize.QueryTypes.SELECT
    });
    const query3 = `select * from pest_control_config where code="zone"  `;
    const zone = await sequelize.query(query3, {
        type: Sequelize.QueryTypes.SELECT
    }); 
    if(medicine[0] && medicine[0].io1){
        pest_control_config.medicine_io = medicine[0].io1; 
        pest_control_config.medicine_address = medicine[0].address; 
    } 
    if(compressor[0] && compressor[0].io1 && compressor[0].io2){
        pest_control_config.compressor_io1 = compressor[0].io1; 
        pest_control_config.compressor_io2 = compressor[0].io2;
        pest_control_config.compressor_address = compressor[0].address;      
    }   
    if(medicine[0] && medicine[0].io1){
        pest_control_config.zone = zone;
    }

}               

async function createPestControlReport(user_id,house_id,control_id,type,contents) {
    return new Promise(async (resolve, reject) => {
        try {
            if(isProcessing[house_id]){
                const createdData = await sequelize.models.Report.create({
                    user_id:user_id,     
                    house_id: house_id,  
                    control_id: control_id,            
                    type: type,
                    contents: contents  
                });   
            }
            resolve(true);
        } catch (e) {
            reject(e);
        }
    });   
} 

function syncIo(callback) {
    if (!io) {
        io = require('./socket-io').getIo();
    }
    if (io) {
        callback();
    }
}
  
function generateContents(name, state) {
    let stateText = LANG[state];
    if (state === 'open' ) stateText = `열림`;
    else if ( state === 'on') stateText = `운전`;
    else if (state === 'stop' || state === 'off') stateText = `정지`;
    else if (state === 'close' ) stateText = `닫기`;
   
    return `[방제기] '${name}'가(이) '${stateText}' 동작합니다.`;
}  

function generateTime(time) {
    return time * 1000;
}  

   