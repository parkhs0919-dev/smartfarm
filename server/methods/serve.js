const path = require('path');
const serve = require('koa-static');
const mount = require('koa-mount');
const CONFIG = require('../config');

const maxage = CONFIG.cacheMaxAge;

module.exports = (app) => {
    serve(path.join(__dirname, '../../client'));
    app.use(mount('/public', serve(path.join(__dirname, '../../public'), {maxage})));
    app.use(mount('/csv', serve(path.join(__dirname, '../../csv'), {maxage})));
    app.use(mount('/dist', serve(path.join(__dirname, '../../dist'), {maxage})));
    app.use(mount('/uploads', serve(path.join(__dirname, '../../uploads'), {maxage})));
};
       