const arrWaterVapor=[
    {key:100,value:588.208},
    {key:99,value:569.071},
    {key:98,value:550.375},
    {key:97,value:532.125},
    {key:96,value:514.401},
    {key:95,value:497.209},
    {key:94,value:480.394},
    {key:93,value:464.119},
    {key:92,value:448.308},
    {key:91,value:432.885},
    {key:90,value:417.935},
    {key:89,value:403.38},
    {key:88,value:389.225},
    {key:87,value:375.471},
    {key:86,value:362.124},
    {key:85,value:340.186},
    {key:84,value:336.66},
    {key:83,value:324.469},
    {key:82,value:311.616},
    {key:81,value:301.186},
    {key:80,value:290.017},
    {key:79,value:279.278},
    {key:78,value:268.806},
    {key:77,value:258.827},
    {key:76,value:248.84},
    {key:75,value:239.351},
    {key:74,value:230.142},
    {key:73,value:221.212},
    {key:72,value:212.648},
    {key:71,value:204.286},
    {key:70,value:196.213},
    {key:69,value:188.429},
    {key:68,value:180.855},
    {key:67,value:173.575},
    {key:66,value:166.507},
    {key:65,value:159.645},
    {key:64,value:153.103},
    {key:63,value:146.771},
    {key:62,value:140.65},
    {key:61,value:134.684},
    {key:60,value:129.02},
    {key:59,value:123.495},
    {key:58,value:118.199},
    {key:57,value:113.13},
    {key:56,value:108.2},
    {key:55,value:103.453},
    {key:54,value:98.883},
    {key:53,value:94.483},
    {key:52,value:90.247},
    {key:51,value:86.173},
    {key:50,value:82.257},
    {key:49,value:78.491},
    {key:48,value:74.871},
    {key:47,value:71.395},
    {key:46,value:68.056},
    {key:45,value:64.848},
    {key:44,value:61.772},
    {key:43,value:58.82},
    {key:42,value:55.989},
    {key:41,value:53.274},
    {key:40,value:50.672},
    {key:39,value:48.181},
    {key:38,value:45.593},
    {key:37,value:43.508},
    {key:36,value:41.322},
    {key:35,value:39.286},
    {key:34,value:37.229},
    {key:33,value:35.317},
    {key:32,value:33.49},
    {key:31,value:31.744},
    {key:30,value:30.078},
    {key:29,value:28.488},
    {key:28,value:26.97},
    {key:27,value:25.524},
    {key:26,value:24.143},
    {key:25,value:22.83},
    {key:24,value:21.578},
    {key:23,value:20.386},
    {key:22,value:19.252},
    {key:21,value:18.191},
    {key:20,value:17.148},
    {key:19,value:16.172},
    {key:18,value:15.246},
    {key:17,value:14.367},
    {key:16,value:16.531},
    {key:15,value:12.739},
    {key:14,value:11.987},
    {key:13,value:11.276},
    {key:12,value:10.6},
    {key:11,value:9.961},
    {key:10,value:9.356},
    {key:9,value:8.784},
    {key:8,value:8.243},
    {key:7,value:7.732},
    {key:6,value:7.246},
    {key:5,value:6.79},
    {key:4,value:6.359},
    {key:3,value:5.953},
    {key:2,value:5.57},
    {key:1,value:5.209},
    {key:0,value:4.868},
    {key:-1,value:4.487},
    {key:-2,value:4.135},
    {key:-3,value:3.889},
    {key:-4,value:3.313},
    {key:-5,value:3.238},
    {key:-6,value:2.984},
    {key:-7,value:2.751},
    {key:-8,value:2.537},
    {key:-9,value:2.339},
    {key:-10,value:2.156},
    {key:-11,value:1.96},
    {key:-12,value:1.8},
    {key:-13,value:1.65},
    {key:-14,value:1.51},
    {key:-15,value:1.38},
    {key:-16,value:1.27},
    {key:-17,value:1.15},
    {key:-18,value:1.05},
    {key:-19,value:0.96},
    {key:-20,value:0.88},
    {key:-21,value:0.8},
    {key:-22,value:0.73},
    {key:-23,value:0.66},
    {key:-24,value:0.6},
    {key:-25,value:0.55},
    {key:-26,value:0.51},
    {key:-27,value:0.46},
    {key:-28,value:0.41},
    {key:-29,value:0.37},
    {key:-30,value:0.33},
    {key:-31,value:0.301},
    {key:-32,value:0.271},
    {key:-33,value:0.244},
    {key:-34,value:0.22},
    {key:-35,value:0.198},
    {key:-36,value:0.178},
    {key:-37,value:0.16},
    {key:-38,value:0.144},
    {key:-39,value:0.13},
    {key:-40,value:0.117},
    {key:-41,value:0.104},
    {key:-42,value:0.093},
    {key:-43,value:0.083},
    {key:-44,value:0.075},
    {key:-45,value:0.067},
    {key:-46,value:0.06},
    {key:-47,value:0.054},
    {key:-48,value:0.048},
    {key:-49,value:0.043},
    {key:-50,value:0.038},
    {key:-51,value:0.034},
    {key:-52,value:0.03},
    {key:-53,value:0.027},
    {key:-54,value:0.024},
    {key:-55,value:0.021},
    {key:-56,value:0.019},
    {key:-57,value:0.017},
    {key:-58,value:0.015},
    {key:-59,value:0.013},
    {key:-60,value:0.011}
    
]

module.exports.getHumidityDeficit = async (wet, dry) => {
    let water_vapor_pressure = ''; //수증기압 Water vapor pressure
    let relative_humidity = ''; //상대습도 Relative humidity
    let absolute_humidity = ''; //절대습도 Absolute humidity
    let dew_point_temperature = ''; //이슬점온도 Dew point temperature
    let humidity_deficit = ''; // 수분부족분 Humidity Deficit  

    let a = await getWaterVaporPressure(wet);
    water_vapor_pressure = a - 0.00068 * 101325 * (dry - wet);
    //console.log('수증기압============',water_vapor_pressure);

    let b = await getWaterVaporPressure(dry);
    relative_humidity = (water_vapor_pressure / b) * 100;
    //console.log('상대습도============',relative_humidity);

    absolute_humidity = 0.622 * 1293 / (1 + 0.00366 * dry) * (water_vapor_pressure / 101325);
    //console.log('절대습도============',absolute_humidity);

    dew_point_temperature = dry - (100 - relative_humidity) / 5;
    //console.log('이슬점온도============',dew_point_temperature);

    let dryTempVapor = await setDryTemperatureVapor(dry);
    //console.log('건구온도 포화수증기량 ================', c);
 
    humidity_deficit = dryTempVapor.value * (1 - (relative_humidity / 100));
    //humidity_deficit = dryTempVapor.value - absolute_humidity;   
    //console.log('수분 부족량===============',humidity_deficit);      
               
    return {
        dryTemperature: dry,
        wetTemperature: wet,
        water_vapor_pressure: water_vapor_pressure.toFixed(2),
        relative_humidity: relative_humidity.toFixed(2),
        absolute_humidity: absolute_humidity.toFixed(2),
        dew_point_temperature: dew_point_temperature.toFixed(2),
        humidity_deficit: humidity_deficit.toFixed(2)
    };
};

module.exports.getHumidityDeficit2 = async (humidity, temperature) => {
    const dryTempVapor = await setDryTemperatureVapor(temperature);
    const humidity_deficit =dryTempVapor.value * (1 - (humidity / 100));
    const dew_point_temperature = temperature - (100 - humidity) / 5;
    return {      
        humidity_deficit: humidity_deficit.toFixed(2),
        dew_point_temperature: dew_point_temperature.toFixed(2),
    }   
}   

function setDryTemperatureVapor(data) {
    return new Promise(function (resolve, reject) {
        arrWaterVapor.forEach(function (value, index) {
            if (Math.floor(data) === value.key) {
                resolve(value);
            }
        })
    })
}

function getWaterVaporPressure(Temp) {
    return new Promise(function (resolve, reject) {
        resolve(6.11 * (Math.pow(10, (parseFloat(7.5 * Temp / (237.3 + Temp))))) * 100);
    })
}