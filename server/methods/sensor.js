const Sequelize = require('sequelize');
const sequelize = require('./sequelize').sequelize;
const CONVERT = require('./convert');
const CONFIG = require('../config');
const FILTER_UTIL = require('../utils/filter');
const calculation = require('./calculation'); 
const getIo = require('./socket-io').getIo; 
   
let sensorAddressHash = {};
let sensorIdHash = {};   
let arrData=[];

module.exports = async () => {
    let sensorList = [];  
    try {
        sensorList = await sequelize.models.Sensor.findAll({});
    } catch (e) {
        console.error('sensor memory - find sensors failed', e);
    }
    sensorList.forEach(sensor => {
        if (!sensorAddressHash[sensor.address]) {
            sensorAddressHash[sensor.address] = {};
        }  
        sensorAddressHash[sensor.address][sensor.type + (sensor.key ? sensor.key : '')] = {
            sensor: sensor,
            value: sensor.value,
            count: 0,
            time : null,
            queue : []
        };
        sensorIdHash[sensor.id] = sensor;
    });  
};
module.exports.setSensorValue = (address, key, val) => {
    const data = generateType(key, val); 
    if (sensorAddressHash[address] && sensorAddressHash[address][data.type + key] !== undefined) {
        const sensorData = sensorAddressHash[address][data.type + key];
        const sensor = sensorData.sensor;     
        const before_value = sensorData.value;
        const current_value = sensor.convert_key && CONVERT[sensor.convert_key] ? CONVERT[sensor.convert_key](data.value) : data.value; 
        const count = parseInt(sensorData.count);
        const isValue = isValueCheck(data,before_value,current_value);
        const isValueTime = isValueTimeCheck(sensorData);

        if(count>5){    
            if(isValue){
                sensorAddressHash[address][data.type + key].value = current_value;
                sensorAddressHash[address][data.type + key].time = FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'); 
            }     
            if(isValueTime) sensorAddressHash[address][data.type + key].count =  0;                                                      
        }else{   
            const isValueRange = isValueRangeCheck(data,current_value);
            console.log(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), 'sensor value isValueRange', isValueRange); 
            if(isValueRange){
                sensorAddressHash[address][data.type + key].value = current_value;
                sensorAddressHash[address][data.type + key].time = FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss');  
            }     
            sensorAddressHash[address][data.type + key].count =  count+1;           
        }         
        console.log(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), 'sensor value count isValueTime', count ,  isValueTime);  
        console.log(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), 'sensor value',  address, key, val, current_value, isValue);
        let sensor_log=FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss')+' [매칭] ==> ['+address+'] '+key+' '+val +' [변환] ==> '+current_value+'  '+((isValue)? '' : '(실패 '+sensorData.time+')');
        sendSensorDataLog(sensor_log);

    } else {  
        // console.error(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), 'sensor address mismatch', key, val, type, value);
    }
};
    
module.exports.setNewSensorValue = (address, key, val) => {   
    const data = generateNewType(key, val);
    if (sensorAddressHash[address] && sensorAddressHash[address][data.type + key] !== undefined) {
        const sensorData = sensorAddressHash[address][data.type + key];
        const sensor = sensorData.sensor;     
        const before_value = sensorData.value;
        let current_value = sensor.convert_key && CONVERT[sensor.convert_key] ? CONVERT[sensor.convert_key](data.value) : data.value; 
        const count = parseInt(sensorData.count);
        const isValue = isValueCheck(data,before_value,current_value);
        const isValueTime = isValueTimeCheck(sensorData);

        if(count>5){  
            if(isValue) {               
                sensorAddressHash[address][data.type + key].value = current_value; 
                sensorAddressHash[address][data.type + key].time = FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss');              
                if(key=='L') updateHumidityDeficit(address);       
                else if(key=='H') updateHumidityDeficit2(address);     
                else if(key=='A') updateKorinsFallRain(address,before_value,current_value);                                
            } 
            if(isValueTime) sensorAddressHash[address][data.type + key].count =  0;                     
        }else{  
            const isValueRange = isValueRangeCheck(data,current_value);
            console.log(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), 'new sensor value isValueRange', isValueRange); 
            if(isValueRange){
                sensorAddressHash[address][data.type + key].value = current_value; 
                sensorAddressHash[address][data.type + key].time = FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss');  
                if(key=='L') updateHumidityDeficit(address);       
                else if(key=='H') updateHumidityDeficit2(address);     
                else if(key=='A') updateKorinsFallRain(address,before_value,current_value);                  
            }                                     
            sensorAddressHash[address][data.type + key].count =  count+1;
        }     
        console.log(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), 'new sensor value count isValueTime', count ,  isValueTime);              
        console.log(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), 'new sensor value', address, key, val, current_value, isValue);
        let sensor_log=FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss')+' [매칭] ==> ['+address+'] '+key+' '+val +' [변환] ==> '+current_value +'  '+((isValue)? '' : '(실패 '+sensorData.time+')') ;
        sendSensorDataLog(sensor_log);  
    } else {        
        // console.error(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), 'sensor address mismatch', key, val, type, value);
    }   
};       

module.exports.getSensorValue = (id) => {
    if (sensorIdHash[id]) {
        const sensor = sensorIdHash[id];
        return sensorAddressHash[sensor.address][sensor.type + (sensor.key ? sensor.key : '')].value;
    } else {
        return null;
    }
};
module.exports.setSensorValueById = (value, id) => {
    if (sensorIdHash[id]) {
        const sensor = sensorIdHash[id];
        sensorAddressHash[sensor.address][sensor.type + (sensor.key ? sensor.key : '')].value = value;
        return true;
    } else {
        return false;
    }
};  
 
module.exports.setSendSensorDataLog = (data) => {
    sendSensorDataLog(data);
};  

function sendSensorDataLog(data) {
    try {   
        let io = getIo();     
        io.emit('sensor_data_log', data); 
        return true;               
    } catch (e) {                          
        return true;           
    }  
}

async function updateHumidityDeficit  (address) {  
    try {  
        let dry =parseFloat(sensorAddressHash[address]['dryTemperatureY'].value);
        let wet =parseFloat(sensorAddressHash[address]['wetTemperatureL'].value);
        let data= await calculation.getHumidityDeficit(wet,dry);     
        if(sensorAddressHash[address]['humidityDeficit'] !== undefined)sensorAddressHash[address]['humidityDeficit'].value =data.humidity_deficit;    
        if(sensorAddressHash[address]['relativeHumidity'] !== undefined)sensorAddressHash[address]['relativeHumidity'].value =data.relative_humidity; 
        if(sensorAddressHash[address]['dewPointTemperature'] !== undefined)sensorAddressHash[address]['dewPointTemperature'].value =data.dew_point_temperature; 
        return true;               
    } catch (e) {                        
        return true;           
    }  
}  

async function updateHumidityDeficit2  (address) {  
    try {  
        let dry =parseFloat(sensorAddressHash[address]['temperatureT'].value);
        let wet =parseFloat(sensorAddressHash[address]['humidityH'].value);
        let data= await calculation.getHumidityDeficit2(wet,dry);    
        if(sensorAddressHash[address]['humidityDeficit'] !== undefined) sensorAddressHash[address]['humidityDeficit'].value =data.humidity_deficit;  
        if(sensorAddressHash[address]['dewPointTemperature'] !== undefined) sensorAddressHash[address]['dewPointTemperature'].value =data.dew_point_temperature;   
        return true;                    
    } catch (e) {                                  
        return true;              
    }  
}  

async function updateKorinsFallRain  (address,before_value,current_value) {  
    try {  
        let currentInstantRainFall = 0;
        const before =parseFloat(before_value);   
        const current =parseFloat(current_value);     
        const beforeRainFall =sensorAddressHash[address]['korinsCumulativeRainFall'].value;           
        if(before<=current) currentInstantRainFall = current-before;                   
        else currentInstantRainFall = current;   
        if(sensorAddressHash[address]['rainR'] !== undefined){   
            if(currentInstantRainFall!=0){    
                //sensorAddressHash[address]['rainR'].value = 0;
                sensorAddressHash[address]['rainR'].value = getSensorValueQueue(address,'rainR',0);
            }else{      
                //sensorAddressHash[address]['rainR'].value = 1;
                sensorAddressHash[address]['rainR'].value = getSensorValueQueue(address,'rainR',1);      
            }     
        }               
        if(sensorAddressHash[address]['korinsInstantRainFall'] !== undefined) sensorAddressHash[address]['korinsInstantRainFall'].value =currentInstantRainFall;
        if(sensorAddressHash[address]['korinsCumulativeRainFall'] !== undefined) sensorAddressHash[address]['korinsCumulativeRainFall'].value =parseFloat(beforeRainFall)+currentInstantRainFall; 
        return true;                         
    } catch (e) {   
        console.log(e)                               
        return true;                
    }  
}  

function getSensorValueQueue (address,key,value) {
    const queueSize=5; 
    const sensorData=sensorAddressHash[address][key].queue;
    const sensorDataLength=sensorData.length;
    let sensor_value = 1;
    if(queueSize===sensorDataLength){       
        sensorAddressHash[address][key].queue.splice(0, 1);
        sensorAddressHash[address][key].queue.push(value);
    }else{
        sensorAddressHash[address][key].queue.push(value);
    }

    for (let i=0; i<sensorData.length; i++) {
        if(sensorData[i]===0) sensor_value = 0;
    }
    return sensor_value;      
}     

async function updateSensorTime (sensor) {
    try {
        await sequelize.models.Sensor.update({updatedAt: new Date()}, {where: {id: sensor.id}});
        return true;
    } catch (e) {
        return true;
    }
}

async function updateSensorRealTimeLog (address, key, val, value,state) {
    try {
        let selectQuery = " INSERT INTO `init_sensor_log` ";
        selectQuery += " (`address`,`key`,`val`,`value`,`state`) VALUES (";
        selectQuery += ` '${address}', `;
        selectQuery += ` '${key}', `;
        selectQuery += ` '${val}', `;
        selectQuery += ` '${value}', `;
        selectQuery += ` '${state}' `;
        selectQuery += ` ) `;
        arrData.push(selectQuery);

        if(arrData.length>5){
            await sequelize.transaction(t => {
                let promises = [];
                arrData.forEach((data) => {
                    promises.push(sequelize.query(data, {
                        type: Sequelize.QueryTypes.INSERT
                    }))
                });
                arrData=[];
                return Promise.all(promises);
            });
        }
        return true;
    } catch (e) {
        console.log(e);
        return true;
    }
}

function generateType (type, value) {
    switch(type) {
        case 'H':
            return {
                type: 'humidity',
                value: parseFloat(value).toFixed(1),
                start_range: 0,
                end_range: 100,
                deviation: 20                
            };
        case 'T':
            return {
                type: 'temperature',
                value: parseFloat(value).toFixed(1),
                start_range: -39,
                end_range: 85,
                deviation: 2                    
            };
        case 'C':
            return {
                type: 'co2',
                value: parseFloat(value),
                start_range: 0,
                end_range: 10000,
                deviation: 500                   
            };
        case 'W':
            return {
                type: 'solar',
                value: parseFloat(value),
                start_range: 0,
                end_range: 2000,
                deviation: 500                  
            };
        case 'P':
            return {
                type: 'ph',
                value: parseFloat(value),
                start_range: 0,
                end_range: 14,
                deviation: 5                     
            };
        case 'D':
            return {
                type: 'windDirection',
                value: parseFloat(value),
                start_range: '0',
                end_range: '360',
                deviation: '9999'                   
            };
        case 'S':
            return {
                type: 'windSpeed',
                value: parseFloat(value),
                start_range: 0,
                end_range: 32.4,
                deviation: 20                      
            };
        case 'c':
            return {
                type: 'co2',
                value: parseFloat(value),
                start_range: 0,
                end_range: 3000,
                deviation: 500                    
            };
        case 't':
            return {
                type: 'temperature',
                value: parseFloat(value).toFixed(1),
                start_range: -39,
                end_range: 85,
                deviation: 2                  
            };
        case 'h':
            return {
                type: 'humidity',
                value: parseFloat(value).toFixed(1),
                start_range: 0,
                end_range: 100,
                deviation: 20                
            };
        case 'X':
            return {
                type: 'temperature',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'R':
            return {
                type: 'rain',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'M':
            return {
                type: 'door',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                      
            };            
        case 'E':
            return {
                type: 'ec',
                value: parseFloat(value),
                start_range: 0,
                end_range: 6,
                deviation: 1                  
            };
        case 'm':
            return {
                type: 'soilTemperature',
                value: parseFloat(value).toFixed(1),
                start_range: 0,
                end_range: 60,
                deviation: 2                  
            };
        case 'u':
            return {
                type: 'soilHumidity',
                value: parseFloat(value).toFixed(1),
                start_range: 0,
                end_range: 99.9,
                deviation: 20                  
            };
        case 'B':
            return {
                type: 'battery',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'wx':
            return {
                type: 'water',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'wy':
            return {
                type: 'water',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'wz':
            return {
                type: 'water',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'f':
            return {
                type: 'liter',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Temp':
            return {
                type: 'temperature',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Hummi':
            return {
                type: 'humidity',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Out_Temp':
            return {
                type: 'temperature',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'DTemp':
            return {
                type: 'temperature',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'DHummi':
            return {
                type: 'humidity',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Wlevel':
            return {
                type: 'water',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Rain':
            return {
                type: 'rain',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Windspeed':
            return {
                type: 'windSpeed',
                value: parseFloat(value).toFixed(2),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Windstring':
            return {
                type: 'windDirection',
                value: parseFloat(value),
                start_range: '0',
                end_range: '360',
                deviation: '9999'                  
            };
        case 'Ec':
            return {
                type: 'ec',
                value: parseFloat(value).toFixed(2),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Soiltemp':
            return {
                type: 'soilTemperature',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Soilhummi':
            return {
                type: 'soilHumidity',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Ph':
            return {
                type: 'ph',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Co2':
            return {
                type: 'co2',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Lux':
            return {
                type: 'solar',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Etc':
            return {
                type: 'power',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        default:
            return {type, value};
    }
}

function generateNewType (type, value) {
    switch(type) {  
        case 'H':
            return {
                type: 'humidity',
                value: parseFloat(value).toFixed(1),
                start_range: 0,
                end_range: 100,
                deviation: 5
            };  
        case 'T':
            return {
                type: 'temperature',
                value: parseFloat(value).toFixed(1),
                start_range: -39,
                end_range: 85,
                deviation: 2               
            };
        case 'C':
            return {
                type: 'co2',
                value: parseFloat(value),
                start_range: 0,
                end_range: 3000,
                deviation: 500                  
            };
        case 'c':   
            return {
                type: 'co2',
                value: parseFloat(value),
                start_range: 0,
                end_range: 10000,
                deviation: 500                  
            };            
        case 'W':
            return {
                type: 'solar',
                value: parseFloat(value),
                start_range: 0,
                end_range: 2000,
                deviation: 500                  
            };
        case 'P':
            return {
                type: 'ph',
                value: parseFloat(value),
                start_range: 0,
                end_range: 14,
                deviation: 1                  
            };  
        case 'D':
            return {
                type: 'windDirection',
                value: parseFloat(value),
                start_range: '0',
                end_range: '360',
                deviation: '9999'                  
            };
        case 'S':
            return {
                type: 'windSpeed',
                value: parseFloat(value),
                start_range: 0,
                end_range: 32.4,
                deviation: 20                  
            };
        case 'R':
            return {
                type: 'rain',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'E':
            return {
                type: 'ec',
                value: parseFloat(value),
                start_range: 0,
                end_range: 6,
                deviation: 1  
            };  
        case 'm':
            return {
                type: 'soilTemperature',
                value: parseFloat(value).toFixed(1),
                start_range: 0,
                end_range: 60,
                deviation: 2                  
            };
        case 'u':
            return {
                type: 'soilHumidity',
                value: parseFloat(value).toFixed(1),
                start_range: 0,
                end_range: 99.9,
                deviation: 5                  
            };
        case 'B':
            return {
                type: 'battery',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'w1':
            return {
                type: 'water',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'w2':
            return {
                type: 'water',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'w3':
            return {
                type: 'water',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'f':
            return {
                type: 'liter',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'U':
            return {
                type: 'frostHumidity',
                value: parseFloat(value),
                start_range: 0,
                end_range: 100,
                deviation: 5  
            };
        case 'r':
            return {
                type: 'frostTemperature',
                value: parseFloat(value),
                start_range: -39,
                end_range: 80,
                deviation: 2                  
            };
        case 'd':
            return { 
                type: 'frost',
                value: parseFloat(value),
                start_range: -39,
                end_range: 60,
                deviation: 10                    
            };
        case 'N1':
            return {
                type: 'window',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'N2':
            return {
                type: 'window',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'N3':
            return { 
                type: 'window',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'N4':
            return { 
                type: 'window',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'F1':
            return {
                type: 'fire',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'F2':
            return {
                type: 'fire',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'F3':
            return { 
                type: 'fire',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'F4':  
            return { 
                type: 'fire',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'b1':
            return {
                type: 'power',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'b2':
            return {
                type: 'power',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'b3':
            return { 
                type: 'power',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'b4':  
            return {   
                type: 'power',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Y':
            return { 
                type: 'dryTemperature',
                value: parseFloat(value),
                start_range: -39,
                end_range: 85,
                deviation: 2                  
            };
        case 'L':  
            return { 
                type: 'wetTemperature',
                value: parseFloat(value),
                start_range: -39,
                end_range: 85,
                deviation: 2                  
            };
        case 'M':
            return {
                type: 'door',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };  
        case 'i':
            return {
                type: 'humidity',
                value: parseFloat(value),
                start_range: 0,
                end_range: 99.9,
                deviation: 5                  
            };
        case 'o':
            return { 
                type: 'co2',
                value: parseFloat(value),
                start_range: 0,
                end_range: 10000,
                deviation: 500                  
            };
        case 'p':  
            return { 
                type: 'temperature',
                value: parseFloat(value),
                start_range: -25,
                end_range: 80,
                deviation: 2                  
            }; 
        case 't':  
            return { 
                type: 'temperature',
                value: parseFloat(value),
                start_range: -39,
                end_range: 85,
                deviation: 2                  
            };   
        case 'h':  
            return { 
                type: 'humidity',
                value: parseFloat(value),
                start_range: 0,
                end_range: 100,
                deviation: 5                
            };                                                
        case 'Temp':
            return {
                type: 'temperature',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Hummi':
            return {
                type: 'humidity',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Out_Temp':
            return {
                type: 'temperature',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                 
            };
        case 'DTemp':
            return {
                type: 'temperature',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'DHummi':
            return {
                type: 'humidity',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Wlevel':
            return {
                type: 'water',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Rain':
            return {
                type: 'rain',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Windspeed':
            return {
                type: 'windSpeed',
                value: parseFloat(value).toFixed(2),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Windstring':
            return {
                type: 'windDirection',
                value: parseFloat(value),
                start_range: '0',
                end_range: '360',
                deviation: '9999'                  
            };
        case 'Ec':
            return {
                type: 'ec',
                value: parseFloat(value).toFixed(2),
                start_range: '',
                end_range: '',
                deviation: ''                 
            };
        case 'Soiltemp':
            return {
                type: 'soilTemperature',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Soilhummi':
            return {
                type: 'soilHumidity',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Ph':
            return {
                type: 'ph',
                value: parseFloat(value).toFixed(1),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Co2':
            return {
                type: 'co2',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Lux':
            return {
                type: 'solar',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'Etc':
            return {
                type: 'power',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                  
            };
        case 'y':
            return {
                type: 'korinsHumidity',
                value: parseFloat(value).toFixed(1),
                start_range: '0',
                end_range: '100',
                deviation: '5'                 
            };
        case 'O':
            return {
                type: 'korinsTemperature',
                value: parseFloat(value).toFixed(1),
                start_range: '0',
                end_range: '60',
                deviation: '2'                  
            };
        case 'g':
            return {
                type: 'korinsPressure',
                value: parseFloat(value).toFixed(1),
                start_range: '540',
                end_range: '1100',
                deviation: '100'                 
            };
        case 'A':
            return {
                type: 'korinsFall',
                value: parseFloat(value),
                start_range: '0',
                end_range: '2090',
                deviation: '9999'                  
            };
        case 's':  
            return {
                type: 'korinsWindSpeed',
                value: parseFloat(value).toFixed(2),
                start_range: '0',
                end_range: '88.8',
                deviation: '20'                  
            };
        case 'e':
            return { 
                type: 'korinsWindDirection',
                value: parseFloat(value),
                start_range: '0',
                end_range: '360',
                deviation: '9999'                  
            };    
        case 'Q':
            return { 
                type: 'korinsSolar',
                value: parseFloat(value),
                start_range: '0',
                end_range: '1800',
                deviation: '500'                     
            }; 
        case 'l':
            return { 
                type: 'FolateTemperature',
                value: parseFloat(value),
                start_range: -39,
                end_range: 85,
                deviation: 2                     
            };    
        case 'a':
            return { 
                type: 'CultureMediumWeight',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                     
            };   
        case 'k':
            return { 
                type: 'CultureMediumWaterWeight',
                value: parseFloat(value),
                start_range: '',
                end_range: '',
                deviation: ''                     
            };                                        
        default:
            return {type, value };
    }  
}

function isValueCheck(data,before,current) {
    let result = true;
    if (data.deviation) {
        const current_value = parseFloat(current);
        const before_value = parseFloat(before);   
        const start_range = parseFloat(data.start_range);
        const end_range = parseFloat(data.end_range);
        const deviation = parseFloat(data.deviation);
        if (start_range <= current_value && end_range >= current_value) {
            let value = Math.abs(current_value - before_value);
            if (deviation < value) {
                result = false;
            }
        }else{   
           result = false; 
        }  
    }
    return result;
}   

function isValueRangeCheck(data,current) {
    let result = false;
    if (data.deviation) {
        const current_value = parseFloat(current); 
        const start_range = parseFloat(data.start_range);
        const end_range = parseFloat(data.end_range);
        if (start_range <= current_value && end_range >= current_value) {
            result = true;
        }else{  
           result = false;   
        }  
    }else{
        result = true; 
    }     
    return result;
}  

function isValueTimeCheck(data) {
    let result = false;         
    const before = (data.time)? new Date(data.time) : new Date();
    const current = new Date();       
    const diffTime = (current.getTime()-before.getTime())/(1000*60);
    if(diffTime>5){        
        result = true;    
    }        
    return result;          
}       