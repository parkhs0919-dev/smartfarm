const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'sensor_avg_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },        
        'sensor_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },                
    },
    options: {
        indexes: [{
            name: 'sensor_avg_id',
            fields: ['sensor_avg_id']
        }],       
        tableName: 'sensor_avg_items',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate      
    },  
    associations: [{
        belongsTo: 'SensorAvg',
        foreignKey: 'sensor_avg_id',
        targetKey: 'id',
        as: 'sensorAvg'      
    }, {
        belongsTo: 'Sensor',
        foreignKey: 'sensor_id',
        targetKey: 'id',
        as: 'sensor'
    }
], 
    methods: {    
        instanceMethods: {},
        classMethods: {
            getSensorAvgItemType: ({house_id,type,position}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let query = ` select sensors.*,"" as avg_sensor_id from sensors as sensors   `;
                        query +=` left join house_sensor_rels as house on sensors.id=house.sensor_id   `;  
                        query +=` where sensors.sensor_id is null and house.house_id='${house_id}' `; 
                        query +=` and sensors.type='${type}' `;    
                        if(position) query +=` and sensors.position='${position}' `;                   
                        const rows = await sequelize.query(query, {               
                            type: Sequelize.QueryTypes.SELECT       
                        });     
                        resolve(rows);      
                    } catch (e) {
                        reject(e);   
                    }
                });    
            }, 
            getSensorAvgItemGroupbyValue: () => { 
                return new Promise(async (resolve, reject) => {
                    try {
                        let query = ` select * from sensor_avgs as sensor_avgs `;
                        query +=` left join ( `;  
                        query +=` SELECT sensor_avg_id as id ,ROUND(avg(BB.value),1) as value  FROM sensor_avg_items as AA  `; 
                        query +=` left join sensors as BB  on AA.sensor_id=BB.id group by sensor_avg_id `; 
                        query +=` ) as sensor_value `; 
                        query +=` on sensor_avgs.id=sensor_value.id  `;   
                        const rows = await sequelize.query(query, {            
                            type: Sequelize.QueryTypes.SELECT       
                        });          
                         resolve(rows);      
                    
                    } catch (e) {
                        reject(e);   
                    }
                });    
            },                      
        }
    }
};
