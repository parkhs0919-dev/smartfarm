const fs = require('fs');
const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const CONFIG = require('../config');
const DB = CONFIG.db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const CONTROL_UTIL = require('../utils').control;
const FILTER_UTIL = require('../utils').filter;
const LANG = require('../metadata/langs').ko;
const getIo = require('../methods/socket-io').getIo;

const bom = "\ufeff";
const csvSize = 100;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'user_id': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull :false
        },
        'control_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['motor', 'power', 'control', 'warning', 'report', 'error', 'auto', 'noti'],
            defaultValue: 'report',
            allowNull: false
        },
        'contents': {
            type: Sequelize.TEXT('long'),
            allowNull: false
        },
        'created_at': {
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }
    },
    options: {
        indexes: [{
            name: 'type',
            fields: ['type']
        }, {
            name: 'created_at',
            fields: ['created_at']
        }],
        tableName: 'reports',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }, {
        belongsTo: 'Control',
        foreignKey: 'control_id',
        targetKey: 'id',
        as: 'control'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            getReports: ({house_id, type, offset, size}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let where = {};

                        if (house_id) where.house_id = house_id;
                        if (type) where.type = type;

                        const count = 60;//await sequelize.models.Report.count({where});
                        const rows = await sequelize.models.Report.findAll({
                            order: [['created_at', 'DESC']],
                            where,
                            limit: size ? parseInt(size) : undefined,
                            offset: parseInt(offset)
                        });
                        resolve({count, rows});
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            createWindDirectionControlReport: (house_id, wind_direction_type, window_position, {state, time, percentage, p_band_temperature, p_band_integral}, currentStep = null) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const mode = await sequelize.models.Mode.findByPk(house_id);
                        let contents = `[${mode === 'individual' ? '개별/' : ''}자동제어] ${currentStep ? '단계제어 ' + currentStep + '단계: ' : ''} '${window_position}중 ${wind_direction_type === 'forward' ? '풍상창' : '풍하창'}'가(이) `;
                        if (p_band_temperature) {
                            contents += 'P-BAND 공식에 의해 부분'
                        } else if (time || percentage !== 200) {
                            contents += '부분';
                        }
                        if (state === 'open') {
                            contents += '열림 동작합니다.'
                        } else if (state === 'close') {
                            contents += '닫힘 동작합니다.'
                        }
                        const createdData = await sequelize.models.Report.create({
                            user_id: CONFIG.autoUser.id,
                            house_id,
                            type: 'auto',
                            contents
                        });
                        resolve(createdData);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            createAutoControlReport: (autoControl, control, {state, time, percentage, p_band_temperature, p_band_integral}, currentStep = null, finalTime = null, targetMinMax = null, pBandData = {}, logState = {}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const mode = await sequelize.models.Mode.findByPk(autoControl.house_id);
                        let contents = `[${mode === 'individual' ? '개별/' : ''}자동제어] ${currentStep ? '단계제어 ' + currentStep + '단계: ' : ''}`;
                        if (logState.motorLogState) {
                            switch(logState.motorLogState) {
                                case 1: {
                                    contents += '환기설정의 설정시간 내 하강으로 인해 ';
                                    break;
                                }
                                case 2: {
                                    contents += '환기설정의 설정시간 내 미하강으로 인해 ';
                                    break;
                                }
                                case 3: {
                                    contents += '환기설정으로 인해 ';
                                    break;
                                }
                                case 4: {
                                    contents += '환기설정으로 인해 ';
                                    break;
                                }
                                case 5: {
                                    contents += '환기설정의 설정시간 내 상승으로 인해 ';
                                    break;
                                }
                                case 6: {
                                    contents += '환기설정의 설정시간 내 미상승으로 인해 ';
                                    break;
                                }
                                case 7: {
                                    contents += '환기설정으로 인해 ';
                                    break;
                                }
                            }
                        } else if (logState.powerLogState) {
                            switch(logState.powerLogState) {
                                case 1: {
                                    contents += '난방설정의 설정시간 내 하강으로 인해 ';
                                    break;
                                }
                                case 2: {
                                    contents += '난방설정의 설정시간 내 미하강으로 인해 ';
                                    break;
                                }
                                case 3: {
                                    contents += '난방설정으로 인해 ';
                                    break;
                                }
                                case 4: {
                                    contents += '난방설정으로 인해 ';
                                    break;
                                }
                                case 5: {
                                    contents += '난방설정의 설정시간 내 상승으로 인해';
                                    break;
                                }
                                case 6: {
                                    contents += '난방설정의 설정시간 내 미상승으로 인해';
                                    break;
                                }
                                case 7: {
                                    contents += '난방설정으로 인해 ';
                                    break;
                                }
                            }
                        }
                        if (finalTime === 0 && targetMinMax) {
                            contents += `'${control.control_name}'가(이) '${targetMinMax.controlMinMaxRange.name}'에 의해 '${targetMinMax.percentage}%'로 제한되었습니다.`;
                        } else {
                            if (logState.motorLogState === 7) {
                                contents += `'${control.control_name}'가(이) 동작하지 않습니다.`;
                            } else if (pBandData.isPBandIntegral) {
                                contents += `'${control.control_name}'가(이) 적분 값 ${p_band_integral}% '${control.button_name_1 || '열림'}' 동작합니다.`;
                            } else if (pBandData.isAlreadyPBandPercentage) {
                                contents += `'${control.control_name}'가(이) 이미 P-BAND 목표치에 도달했습니다.`;
                            } else if (pBandData.isSatisfiedPBandTemperature) {
                                contents += `희망온도 ${p_band_temperature}℃에 도달했습니다.`;
                            } else {
                                contents += `${CONTROL_UTIL.generateContents(control, state, time, percentage, pBandData.pBandPercentage)}`;
                                if (targetMinMax) {
                                    contents += ` ('${targetMinMax.controlMinMaxRange.name}'에 의해 '${targetMinMax.percentage}%'로 제한되었습니다.)`;
                                }
                            }
                        }
                        const createdData = await sequelize.models.Report.create({
                            user_id: CONFIG.autoUser.id,
                            house_id: autoControl.house_id,
                            control_id: control.id,
                            type: 'auto',
                            contents
                        });
                        resolve(createdData);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            createAutoControlClearReport: (user_id, house_id, control_id, type, beforeWork) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const mode = await sequelize.models.Mode.getModeByControlId(control_id);
                        const createdData = await sequelize.models.Report.create({
                            user_id,
                            house_id,
                            control_id,
                            type,
                            contents: `[${mode === 'individual' ? '개별/' : ''}자동제어] '${beforeWork.control_name}'의 '${beforeWork.auto_control_name}'이 중단 됐습니다.`
                        });
                        resolve(createdData);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            createIgnoreAutoControlReport: (control, ignoreAutoControl, orderAutoControl) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const mode = await sequelize.models.Mode.getModeByControlId(control.id);
                        const createdData = await sequelize.models.Report.create({
                            user_id: CONFIG.autoUser.id,
                            house_id: ignoreAutoControl.house_id,
                            control_id: control.id,
                            type: 'auto',
                            contents: `[${mode === 'individual' ? '개별/' : ''}자동제어] '${orderAutoControl.auto_control_name}'에 의해 '${ignoreAutoControl.auto_control_name}' - '${control.control_name}' 동작이 무시됐습니다.`
                        });
                        resolve(createdData);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            createControlReport: (user_id, control, type, contents) => {
                sequelize.models.Report.create({
                    user_id,
                    house_id: control.house_id,
                    control_id: control.id,
                    type,
                    contents
                }).then(() => {
                    let io = getIo();
                    if (io) io.emit('report');
                    return true;
                }).catch(e => {});
            },
            createReports: (type, contents) => {
                sequelize.models.House.findAll({}).then(data => {
                    if (data.length) {
                        let promises = [];
                        data.forEach(house => {
                            promises.push(sequelize.models.Report.create({
                                user_id: CONFIG.defaultUser.id,
                                house_id: house.id,
                                type,
                                contents
                            }));
                        });
                        return Promise.all(promises);
                    }
                }).then(() => {
                    let io = getIo();
                    if (io) io.emit('report');
                    return true;
                }).catch((e) => {});
            },
            createCsv: (filePath, startDate, endDate, house, type) => {
                return new Promise(async (resolve, reject) => {
                    try {                        
                        let funcs = [], count = 0;
                        const startTimestamp = new Date(startDate).getTime();
                        const endTimestamp = new Date(endDate).getTime();
                        
                        const countQuery = `SELECT COUNT(*) AS count FROM reports
                        WHERE house_id = ${house.id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}' AND ${type=='warning'?`type='warning'`:`type!='warning'`}`;
                        
                        const data = await sequelize.query(countQuery, {
                            type: Sequelize.QueryTypes.SELECT
                        });                        
                        if (data.length && data[0].count !== undefined) {
                            count = data[0].count;
                        }

                        fs.appendFileSync(filePath, bom + "하우스명, 유형, 내용, 등록시각\n");

                        for (let i=0; i<Math.ceil(count / csvSize); i++) {
                            ((index) => {
                                funcs.push(callback => {
                                    const selectQuery = `SELECT type, contents, DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") AS created_at FROM reports
                                    WHERE house_id = ${house.id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}' AND ${type=='warning'?`type='warning'`:`type!='warning'`}
                                    ORDER BY created_at ASC LIMIT ${csvSize} OFFSET ${index * csvSize}`;
                                    sequelize.query(selectQuery, {
                                        type: Sequelize.QueryTypes.SELECT
                                    }).then(data => {                                        
                                        data.forEach(report => {
                                            fs.appendFileSync(filePath, `${house.house_name}, ${LANG[report.type]}, ${report.contents}, ${report.created_at}\n`);
                                        });
                                        callback(null, true);
                                        return true;
                                    }).catch(e => {
                                        callback(e, false);
                                    });
                                });
                            })(i);
                        }

                        async.series(funcs, (error, results) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(true);
                            }
                        });
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getReportGrade: ({house_id, type, offset, size ,user}) => {
                return new Promise(async (resolve, reject) => {
                    try {

                        let query = ` SELECT * FROM reports `;
                        query += ` WHERE user_id in('${user.id}','AUTO') `;
                        query += ` OR control_id in ( `;
                        query += ` SELECT BB.control_id from users as AA `;
                        query += ` LEFT join user_control_rels as BB on AA.user_id=BB.user_id `;
                        query += ` WHERE AA.user_id=${user.id} ) `;

                        if(house_id) query += ` AND house_id=${house_id} `;
                        if(type) query += ` AND type=${house_id} `;
                        query += ` ORDER BY created_at DESC `;
                        query += ` limit ${size ? parseInt(size) : undefined} offset ${parseInt(offset)} `;

                        const rows = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });

                        resolve({count:rows.length, rows});
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            createPortableReport: (user_id, house_id , type, contents) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const createdData = await sequelize.models.Report.create({
                            user_id,
                            house_id,
                            type,
                            contents: contents
                        });  
                        resolve(createdData);
                    } catch (e) {
                        reject(e);
                    }   
                });  
            },                          
        }
    }
};
