const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'task': {
            type: Sequelize.STRING(getDBStringLength()),
            primaryKey: true,
            allowNull: false
        },
        'created_at': {
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }
    },
    options: {
        tableName: 'auto_control_logs',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    methods: {
        instanceMethods: {},
        classMethods: {
            createLog: (house_id, task) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        await sequelize.models.AutoControlLog.create({house_id, task});
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
