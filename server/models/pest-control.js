const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const STD = require('../metadata/standards');
 
module.exports = {   
    fields: {     
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },             
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },  
        'pest_control_name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },                   
        'state': {
            type: Sequelize.ENUM,
            values: ['run', 'stop'],
            defaultValue: 'stop',
            allowNull: false
        },
        'zone_time': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        },    
        'medicine_time': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        },  
        'compressor_time': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        },                      
        'is_frost': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },   
        'is_auto': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },  
        'max_temp': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'midnight_temp': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'current_temp': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },                              
        'delay': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        },
        'created_at': {
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }        
    },
    options: {
        indexes: [{
            name: 'state',
            fields: ['state']
        }, {
            name: 'is_frost',
            fields: ['is_frost']  
        }],
        tableName: 'pest_controls',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }, {
        hasMany: 'PestControlZone',
        foreignKey: 'pest_control_id',
        as: 'pestControlZone'       
    }],              
    methods: {   
        instanceMethods: {},
        classMethods: {
            createPestControl: ({house_id,pest_control_name,zone_time,medicine_time, compressor_time, zone_list, zone_use_list}) => {
                return new Promise(async (resolve, reject) => {
                    try {                                             
                        const pestControlConfig = require('../methods/pest-control').getPestControlConfig();  
                        const createdData = await sequelize.models.PestControl.create({
                            house_id: house_id,      
                            pest_control_name: pest_control_name,
                            zone_time : zone_time,   
                            medicine_time : medicine_time,   
                            compressor_time : compressor_time                            
                        });
                        const list=zone_list.split(',');  
                        const use_list=zone_use_list.split(',');  

                        if(createdData && list.length>0){                             
                            await sequelize.transaction(t => {
                                let promises = [];
                                for (let i=0; i<list.length; i++) {
                                    let device = getPestControlZoneDevice(list[i],pestControlConfig);
                                    promises.push(sequelize.models.PestControlZone.create({
                                        pest_control_id: createdData.id,
                                        pest_control_zone_name: list[i]+'구역',
                                        key:  list[i],
                                        is_use :use_list[i],
                                        io1 :device.io1,
                                        io2 :device.io2,
                                        address :device.address,
                                        order: i          
                                    }, {
                                        transaction: t                                        
                                    }));   
                                }   
                                return Promise.all(promises);
                              });  
                        }   
                        resolve(createdData);
                    } catch (e) {
                        console.log(e);
                        reject(e);   
                    }
                });
            },   
           updatePestControl: (pest_control_id,{pest_control_name,zone_time,medicine_time, compressor_time, zone_list, zone_use_list}) => {
                return new Promise(async (resolve, reject) => {
                    try {                                             
                        const pestControlConfig = require('../methods/pest-control').getPestControlConfig();  
                        const updatedData = await sequelize.models.PestControl.update({
                            pest_control_name: pest_control_name,
                            zone_time : zone_time, 
                            medicine_time : medicine_time,   
                            compressor_time : compressor_time,                            
                        }, {
                            where: 
                            {
                                id : pest_control_id
                            }  
                        });  
                        
                        const result = await sequelize.models.PestControlZone.destroy({
                            where: {
                                pest_control_id: pest_control_id
                            }  
                        });      
   
                        const list=zone_list.split(',');
                        const use_list=zone_use_list.split(',');

                        if(updatedData && result && list.length>0){                          
                            await sequelize.transaction(t => {
                                let promises = [];
                                for (let i = 0; i < list.length; i++) {
                                    let device =getPestControlZoneDevice(list[i],pestControlConfig); 
                                    promises.push(sequelize.models.PestControlZone.create({
                                        pest_control_id: pest_control_id,
                                        pest_control_zone_name: list[i] + '구역',
                                        key: list[i],    
                                        is_use:use_list[i],
                                        io1 :device.io1,
                                        io2 :device.io2,
                                        address :device.address,                                          
                                        order: i   
                                    }, {
                                        transaction: t
                                    })); 
                                }  
                                return Promise.all(promises);
                            });
                        }   
                        resolve(updatedData);
                    } catch (e) {                       
                        reject(e);     
                    }  
                });
            }, 
            updatePestControlActvie: (pest_control_id,body) => {
                return new Promise(async (resolve, reject) => {
                    try {                                          
                        const updatedData = await sequelize.models.PestControl.update({
                           ...body              
                        }, {   
                            where: 
                            {
                                id : pest_control_id
                            }  
                        });    
                        //stop 일때 기존 동작 초기화 기능 추가
                        resolve(updatedData);
                    } catch (e) {
                        console.log(e);
                        reject(e);     
                    }
                });
            },  
            getPestControl: ({house_id}) => {
                return new Promise(async (resolve, reject) => {
                    try {
  
                        let where = {};
                        if (house_id) where.house_id = house_id;  

                        const count = await sequelize.models.PestControl.count({
                            where
                        });
                        const rows = await sequelize.models.PestControl.findAll({
                            where
                           // limit: size ? parseInt(size) : undefined,
                           // offset: parseInt(offset)
                        });  
                        resolve({count, rows});
                    } catch (e) {
                        reject(e);
                    }
                });
            },   
            getPestControlId: (pest_control_id) => {
                return new Promise(async (resolve, reject) => {
                    try {   
                        const pest_control = await sequelize.models.PestControl.findOne({
                            where :
                            {      
                                id : pest_control_id
                            }
                        });  
                        const pest_control_zone = await sequelize.models.PestControlZone.findAll({
                            where :
                            {
                                pest_control_id : pest_control_id    
                            }
                        });                       
                        resolve({pest_control, pest_control_zone});
                    } catch (e) {  
                        //console.log(e) 
                        reject(e);      
                    }
                });
            },
            getPestControlByControlId:  (pest_control_id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const query = `select * from pest_controls where id = ${pest_control_id} LIMIT 1`;
                        const result = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        if (result.length) {
                            resolve(result[0]);   
                        } else {
                            resolve(null);
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            }                                                                             
        }
    }  
};
  
function getPestControlZoneDevice(key,DATA){;
    const zone_list = DATA.zone;  
    let io1,io2,address;     
    for (let i = 0; i < zone_list.length; i++) {
        let data = zone_list[i];        
        if(data.zone_key==key){            
            io1=data.io1;    
            io2=data.io2;  
            address=data.address;     
            break;            
        }         
    }        
    return {     
        io1: io1,  
        io2: io2,
        address:address
    };   
}  
