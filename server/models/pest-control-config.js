const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const STD = require('../metadata/standards');

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },                                       
        'code': {
            type: Sequelize.ENUM,
            values: ['medicine','zone','compressor'],
            defaultValue: 'zone',
            allowNull: false     
        },                    
        'type': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true   
        }, 
        'zone_key': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true   
        },
        'address': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true   
        },                                          
        'io1': {  
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'io2': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        }                           
    },         
    options: {
        indexes: [{
            name: 'type',
            fields: ['type']
        }],     
        tableName: 'pest_control_config',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate      
    },  
    associations: [],             
    methods: {      
        instanceMethods: {},
        classMethods: {
                                   
        }
    }  
};
