const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'user_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'platform': {
            type: Sequelize.ENUM,
            values: ['android', 'ios'],
            defaultValue: 'android',
            primaryKey: true,
            allowNull: false
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['fcm'],
            defaultValue: 'fcm',
            primaryKey: true,
            allowNull: false
        },
        'token': {
            type: Sequelize.STRING(getDBStringLength()),
            primaryKey: true,
            allowNull: false
        },
        'created_at': {
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }
    },
    options: {
        indexes: [{
            name: 'created_at',
            fields: ['created_at']
        }],
        tableName: 'device_tokens',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    methods: {
        instanceMethods: {},
        classMethods: {
            createDeviceToken: ({user_id, platform, type, token}) => {
                return new Promise(async (resolve, reject) => {
                    const maxSize = 10;
                    try {
                        await sequelize.transaction(t => {
                            return sequelize.models.DeviceToken.findAll({
                                order: [['created_at', 'DESC']],
                                where: {
                                    user_id,
                                },
                                transaction: t
                            }).then(data => {
                                let deviceTokens = [{
                                    user_id,
                                    platform,
                                    type,
                                    token
                                }];
                                const length = data.length >= maxSize ? maxSize - 1 : data.length;
                                for (let i=0; i<length; i++) {
                                    deviceTokens.push({
                                        user_id,
                                        platform: data[i].platform,
                                        type: data[i].type,
                                        token: data[i].token,
                                        created_at: new Date(data[i].created_at)
                                    });
                                }
                                return sequelize.models.DeviceToken.destroy({
                                    where: {
                                        user_id
                                    },
                                    transaction: t
                                }).then(() => {
                                    return sequelize.models.DeviceToken.bulkCreate(deviceTokens, {
                                        ignoreDuplicates: true,
                                        transaction: t
                                    });
                                });
                            });
                        });
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
