const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'sensor_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'min_value': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'start_date': {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        'end_date': {
            type: Sequelize.DATEONLY,
            allowNull: false
        }
    },
    options: {
        tableName: 'sensor_infos',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Sensor',
        foreignKey: 'sensor_id',
        targetKey: 'id',
        as: 'sensor'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            createSensorInfo: (sensor_id, body) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let createdData = null;
                        await sequelize.transaction(t => {
                            return sequelize.models.SensorInfo.destroy({
                                where: {sensor_id},
                                transaction: t
                            }).then(() => {
                                return sequelize.models.SensorInfo.create({
                                    sensor_id,
                                    ...body
                                }, {
                                    transaction: t
                                });
                            }).then((data) => {
                                createdData = data;
                                return true;
                            });
                        });
                        resolve(createdData);
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
