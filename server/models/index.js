const LoginHistory = require('./login-history');
const Screen = require('./screen');
const House = require('./house');
const Cctv = require('./cctv');
const CctvSetting = require('./cctv-setting');
const HouseCctvRel = require('./house-cctv-rel');
const Sensor = require('./sensor');
const HouseSensorRel = require('./house-sensor-rel');
const Control = require('./control');
const ControlMinMaxRange = require('./control-min-max-range');
const Log = require('./log');
const Report = require('./report');
const Mode = require('./mode');
const SunDate = require('./sun-date');
const Alarm = require('./alarm');
const SensorMainChart = require('./sensor-main-chart');
const SensorInfo = require('./sensor-info');
const DeviceToken = require('./device-token');
const AutoControl = require('./auto-control');
const AutoControlItem = require('./auto-control-item');
const AutoControlSensor = require('./auto-control-sensor');
const AutoControlControl = require('./auto-control-control');
const AutoControlStep = require('./auto-control-step');
const ControlReservation = require('./control-reservation');
const SensorResetHistory = require('./sensor-reset-history');
const SendSensor = require('./send-sensor');
const AnimalType = require('./animal-type');
const Animal = require('./animal');
const AnimalWeight = require('./animal-weight');
const AnimalFeed = require('./animal-feed');
const AnimalWater = require('./animal-water');
const AnimalMate = require('./animal-mate');
const AnimalBreed = require('./animal-breed');
const AnimalKind = require('./animal-kind');
const Alter = require('./alter');
const SunDateDefault = require('./sun-date-default');
const PestControl = require('./pest-control');
const PestControlZone = require('./pest-control-zone');
const PestControlConfig = require('./pest-control-config');
const AutoControlItemMinMax = require('./auto-control-item-min-max');
const AutoControlStepMinMax = require('./auto-control-step-min-max');
const PBand = require('./p-band');
const HouseWindDirectionSensor = require('./house-wind-direction-sensor');
const AutoControlLog = require('./auto-control-log');
const SensorAvg = require('./sensor_avg');
const SensorAvgItem = require('./sensor_avg_item');
const ControlGroup = require('./control-group');
const ControlGroupItem = require('./control-group-item');
const AutoControlSensorOption = require('./auto-control-sensor-option');
const FlowMeter = require('./flow-meter');
const FlowMeterRound = require('./flow-meter-round');
const User = require('./user');
const UserControlRel = require('./user-control-rel');
const AutoControlExpectTemperatureSensor = require('./auto-control-expect-temperature-sensor');
const FcmLog = require('./fcm_log');
const ControlAlarm = require('./control-alarm');
const VoiceControl = require('./voice-control');

module.exports = {
    LoginHistory,
    Screen,
    House,
    Cctv,
    CctvSetting,
    HouseCctvRel,
    Sensor,
    HouseSensorRel,
    Control,
    ControlMinMaxRange,
    Log,
    Report,
    Mode,
    SunDate,
    Alarm,
    SensorMainChart,
    SensorInfo,
    DeviceToken,
    AutoControl,
    AutoControlItem,
    AutoControlSensor,
    AutoControlControl,
    AutoControlStep,
    ControlReservation,
    SensorResetHistory,
    SendSensor,
    AnimalType,
    Animal,
    AnimalWeight,
    AnimalFeed,
    AnimalWater,
    AnimalMate,
    AnimalBreed,
    AnimalKind,
    Alter,
    SunDateDefault,
    PestControl,
    PestControlZone,
    PestControlConfig,
    AutoControlItemMinMax,
    AutoControlStepMinMax,
    PBand,
    HouseWindDirectionSensor,
    AutoControlLog,
    SensorAvg,
    SensorAvgItem,
    ControlGroup,
    ControlGroupItem,
    AutoControlSensorOption,
    FlowMeter,
    FlowMeterRound,
    AutoControlExpectTemperatureSensor,
    User,
    UserControlRel,
    FcmLog,
    ControlAlarm,
    VoiceControl
};
