const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false 
        },          
        'animal_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'date': {
            type: Sequelize.DATEONLY,
            primaryKey: true,
            allowNull: false
        },
        'weight': {
            type: Sequelize.DOUBLE(8, 2),
            defaultValue: 0,
            allowNull: false
        },
        'memo': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
    },
    options: {  
        tableName: 'animal_weights',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Animal',
        foreignKey: 'animal_id',
        targetKey: 'id',
        as: 'animal'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
          getAnimalWeight: (animal_id,{offset,size}) => {
              return new Promise(async (resolve, reject) => {
                  try {
                      let where = {};
                      where.animal_id = animal_id;

                      const count = await sequelize.models.AnimalWeight.count({where});
                      const rows = await sequelize.models.AnimalWeight.findAll({
                          order: [['date', 'DESC']],
                          where,
                          limit: size ? parseInt(size) : 10,
                          offset: offset ? parseInt(offset) :0
                      });
                      resolve({count, rows});
                  } catch (e) {
                      reject(e);
                  }
              });
          },
          createAnimalWeight: ({animal_id, date ,weight ,memo}) => {
              return new Promise(async (resolve, reject) => {
                  try {
                      const createdData = await sequelize.models.AnimalWeight.create({
                          animal_id:animal_id,
                          date:date,
                          weight:weight,
                          memo:memo
                      });
                      resolve(createdData);
                  } catch (e) {
                      reject(e);
                  }
              });
            },
            deleteAnimalWeight: (id) => {
              return new Promise(async (resolve, reject) => {
                  try {
                      let where = {};
                      where.id = id;    
  
                      const count = await sequelize.models.AnimalWeight.destroy({where});
                      resolve(true);
                  } catch (e) {  
                    console.log(e);
                      reject(e);
                  }
              });
            } 
        }
    }
};
