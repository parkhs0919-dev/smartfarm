const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'mode': {
            type: Sequelize.ENUM,
            values: ['manual', 'individual', 'auto'],
            defaultValue: 'manual',
            allowNull: false
        },
        'period': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 60,
            allowNull: false,
            comment: '자동제어 주기 (초)'
        }
    },
    options: {
        tableName: 'modes',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            getModeByControlId: controlId => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const query = `SELECT modes.mode FROM modes INNER JOIN controls ON controls.house_id = modes.house_id AND controls.id = ${controlId} LIMIT 1`;
                        const result = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        if (result.length) {
                            resolve(result[0].mode);
                        } else {
                            resolve(null);
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getModeByControlGroupId: controlGroupId => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const query = `SELECT modes.mode FROM modes INNER JOIN control_groups ON control_groups.house_id = modes.house_id AND control_groups.id = ${controlGroupId} LIMIT 1`;
                        const result = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        if (result.length) {
                            resolve(result[0].mode);
                        } else {
                            resolve(null);
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
