const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const STD = require('../metadata/standards');

module.exports = {
    fields: {
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'type': {
            type: Sequelize.ENUM,
            values: STD.screen.enumTypes,
            defaultValue: STD.screen.defaultType,
            primaryKey: true,
            allowNull: false
        },
        'is_visible': {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        'order': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        }
    },
    options: {
        indexes: [{
            name: 'is_visible',
            fields: ['is_visible']
        }, {
            name: 'order',
            fields: ['order']
        }],
        tableName: 'screens',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            updateScreens: (screens, house_id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let body = [];
                        screens.forEach(({type, is_visible}, order) => {
                            body.push({
                                house_id,
                                type,
                                is_visible,
                                order
                            });
                        });
                        await sequelize.transaction(t => {
                            return sequelize.models.Screen.destroy({
                                where: {house_id},
                                transaction: t
                            }).then(() => {
                                return sequelize.models.Screen.bulkCreate(body, {
                                    transaction: t
                                });
                            });
                        });
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
