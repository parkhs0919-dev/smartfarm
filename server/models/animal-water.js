const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false 
        },        
        'animal_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'date': {
            type: Sequelize.DATE,
            primaryKey: true,
            allowNull: false
        },
        'water': {
            type: Sequelize.DOUBLE(8, 2),
            defaultValue: 0,
            allowNull: false
        },
        'memo': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
    },
    options: {
        tableName: 'animal_waters',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Animal',
        foreignKey: 'animal_id',
        targetKey: 'id',
        as: 'animal'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
          getAnimalWater: (animal_id, {offset,size}) => {
              return new Promise(async (resolve, reject) => {
                  try {
                      let where = {};
                      where.animal_id = animal_id;

                      const count = await sequelize.models.AnimalWater.count({where});
                      const rows = await sequelize.models.AnimalWater.findAll({
                          order: [['date', 'DESC']],
                          where,
                          limit: size ? parseInt(size) : 10,
                          offset: offset ? parseInt(offset) :0
                      });
                      resolve({count, rows});
                  } catch (e) {
                      reject(e);
                  }
              });   
          },
          createAnimalWater: ({animal_id, date ,water ,memo}) => {
            console.log({animal_id, date ,water ,memo});
              return new Promise(async (resolve, reject) => {
                  try {
                      const createdData = await sequelize.models.AnimalWater.create({
                          animal_id:animal_id,
                          date:date,
                          water:water,
                          memo:memo
                      });
                      resolve(createdData);
                  } catch (e) {
                      reject(e);
                  }
              });
          },
          deleteAnimalWater: (id) => {
            return new Promise(async (resolve, reject) => {
                try {
                    let where = {};
                    where.id = id;  

                    const count = await sequelize.models.AnimalWater.destroy({where});
                    resolve(true);
                } catch (e) {
                  console.log(e);
                    reject(e);
                }
            });
          }          
        }  
    }
};
