const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const STD = require('../metadata/standards');

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },                                       
        'pest_control_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['motor', 'power'],
            defaultValue: 'motor',
            allowNull: false     
        },                   
        'state': { 
            type: Sequelize.ENUM,
            values: ['open', 'stop', 'close', 'on', 'off'],
            defaultValue: 'stop',
            allowNull: false
        },   
        'pest_control_zone_name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true   
        }, 
        'key': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true   
        },
        'address': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true   
        },                                          
        'io1': {  
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'io2': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'is_use': {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },                            
    },         
    options: {
        indexes: [{
            name: 'type',
            fields: ['type']
        }, {
            name: 'pest_control_id',
            fields: ['pest_control_id']
        }, {
            name: 'state',
            fields: ['state']              
        }],     
        tableName: 'pest_control_zone',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate      
    },  
    associations: [{
        belongsTo: 'PestControl',
        foreignKey: 'pest_control_id',
        targetKey: 'id',
        as: 'pestControl'      
    }],             
    methods: {      
        instanceMethods: {},
        classMethods: {
            findById: (id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let query = ` SELECT AA.house_id, AA.pest_control_name,AA.zone_time,AA.medicine_time,AA.compressor_time,AA.is_frost,AA.is_auto `;
                        query +=` ,AA.max_temp ,AA.midnight_temp ,AA.current_temp ,AA.delay, BB.is_use , BB.*  FROM pest_controls as AA   `;  
                        query +=` inner join pest_control_zone as BB on AA.id=BB.pest_control_id   `; 
                        query +=` WHERE BB.id = ${id} `;           
                        const controls = await sequelize.query(query, {            
                            type: Sequelize.QueryTypes.SELECT       
                        });     
                        if (controls.length) {
                            resolve(controls[0]);      
                        } else {  
                            resolve(null);  
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getPestControlZoneOrderByOne: (house_id,id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let query = ` SELECT *  FROM pest_controls as AA  `;
                        query +=` inner join pest_control_zone as BB on AA.id=BB.pest_control_id     `;  
                        query +=` WHERE AA.house_id =${house_id}  and BB.is_use=1   `; 
                        query +=` and AA.id=(select AA.pest_control_id from pest_control_zone as AA where AA.id=${id} limit 1)  `; 
                        query +=` order by BB.id desc  limit 1  `;           
                        const controls = await sequelize.query(query, {              
                            type: Sequelize.QueryTypes.SELECT       
                        });     
                        if (controls.length) {
                            resolve(controls[0]);      
                        } else {  
                            resolve(null);  
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            },                                   
        }
    }  
};
