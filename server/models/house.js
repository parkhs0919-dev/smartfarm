const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const STD = require('../metadata/standards');
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const getIo = require('../methods/socket-io').getIo;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'platform': {
            type: Sequelize.ENUM,
            values: ['farm', 'fluid', 'animal'],
            defaultValue: 'farm',
            allowNull: false
        },
        'house_name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'direction': {
            type: Sequelize.ENUM,
            values: STD.house.enumDirections,
            defaultValue: STD.house.defaultDirection,
            allowNull: false
        },
        'is_use_alarm': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
    },
    options: {
        indexes: [{
            name: 'platform',
            fields: ['platform']
        }],
        tableName: 'houses',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        hasMany: 'Control',
        foreignKey: 'house_id',
        as: 'controls'
    }, {
        hasMany: 'HouseCctvRel',
        foreignKey: 'house_id',
        as: 'houseCctvRels'
    }, {
        hasMany: 'HouseSensorRel',
        foreignKey: 'house_id',
        as: 'houseSensorRels'
    }, {
        hasMany: 'Report',
        foreignKey: 'house_id',
        as: 'reports'
    }, {
        hasMany: 'Mode',
        foreignKey: 'house_id',
        as: 'modes'
    }, {
        hasMany: 'SensorMainChart',
        foreignKey: 'house_id',
        as: 'sensorMainCharts'
    }, {
        hasMany: 'AutoControl',
        foreignKey: 'house_id',
        as: 'autoControls'
    }, {
        hasMany: 'Screen',
        foreignKey: 'house_id',
        as: 'screens'
    }, {
        hasMany: 'ControlMinMaxRange',
        foreignKey: 'house_id',
        as: 'controlMinMaxRanges'
    }, {
        hasOne: 'PBand',
        foreignKey: 'house_id',
        as: 'pBand'
    }, {
        hasOne: 'HouseWindDirectionSensor',
        foreignKey: 'house_id',
        as: 'houseWindDirectionSensor'
    }, {
        hasMany: 'SensorAvg',
        foreignKey: 'house_id',
        as: 'sensorAvg'            
    }, {     
        hasMany: 'ControlGroup',
        foreignKey: 'house_id',
        as: 'controlGroups'
    }, {     
        hasMany: 'FlowMeter',
        foreignKey: 'house_id',
        as: 'flowMeter'        
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            getHouse: id => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const house = await sequelize.models.House.findByPk(id, {
                            include: [{
                                model: sequelize.models.HouseWindDirectionSensor,
                                as: 'houseWindDirectionSensor',
                                include: [{
                                    model: sequelize.models.Sensor,
                                    as: 'sensor'
                                }]
                            }]
                        });
                        if (house) {
                            resolve(house);
                        } else {
                            reject(false);
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getHouses: ({offset, size}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let where = {};

                        const count = await sequelize.models.House.count({where});
                        const rows = await sequelize.models.House.findAll({
                            include: [{
                                model: sequelize.models.HouseWindDirectionSensor,
                                as: 'houseWindDirectionSensor',
                                include: [{
                                    model: sequelize.models.Sensor,
                                    as: 'sensor'
                                }]
                            }],
                            where,
                            limit: size ? parseInt(size) : undefined,
                            offset: parseInt(offset)
                        });
                        resolve({count, rows});
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            updateHouse: (body, id) => {
                return new Promise(async (resolve, reject) => {
                    let transaction;
                    try {
                        transaction = await sequelize.transaction();

                        let set = {};

                        if(body.direction) set.direction = body.direction;
                        if(body.is_use_alarm !== null && body.is_use_alarm !== undefined) set.is_use_alarm = body.is_use_alarm;

                        await sequelize.models.House.update(set, {
                            where: {id},
                            transaction
                        });

                        await sequelize.models.HouseWindDirectionSensor.destroy({
                            where: {
                                house_id: id
                            },
                            transaction
                        });

                        if (body.sensor_id) {
                            await sequelize.models.HouseWindDirectionSensor.create({
                                house_id: id,
                                sensor_id: body.sensor_id
                            }, {transaction});
                        }

                        await transaction.commit();

                        let io = getIo();
                        if (io) io.emit('house', {house_id :body.house_id, is_use_alarm : body.is_use_alarm});

                        resolve(true);
                    } catch (e) {
                        transaction && await transaction.rollback();
                        reject(e);
                    }
                });
            }
        }
    }
};
