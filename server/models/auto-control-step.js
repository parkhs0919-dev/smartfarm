const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'user_id': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'auto_control_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'wind_direction_type': {
            type: Sequelize.ENUM,
            values: ['none', 'forward', 'backward'],
            defaultValue: 'none',
            allowNull: false
        },
        'window_position': {
            type: Sequelize.ENUM,
            values: ['none', '1', '2', '3'],
            defaultValue: 'none',
            allowNull: false
        },
        'control_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['open', 'stop', 'close', 'on', 'off'],
            defaultValue: 'stop',
            allowNull: false
        },
        'time': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'percentage': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'p_band_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'p_band_temperature': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'p_band_integral': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'is_temperature_control_option':{
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'rise_time':{
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'drop_time':{
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'expect_temperature':{
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'expect_temperature_sensor_id' : {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'start_temperature_time': {
            type: Sequelize.BIGINT.UNSIGNED,
            allowNull: true
        },
        'start_temperature': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'is_min_max_range': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'delay': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'recount': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'active_time': {
            type: Sequelize.BIGINT.UNSIGNED,
            allowNull: true
        },
        'current_count': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
    },
    options: {
        indexes: [{
            name: 'wind_direction_type',
            fields: ['wind_direction_type']
        }, {
            name: 'window_position',
            fields: ['window_position']
        }],
        tableName: 'auto_control_steps',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'AutoControl',
        foreignKey: 'auto_control_id',
        targetKey: 'id',
        as: 'autoControl'
    }, {
        belongsTo: 'Control',
        foreignKey: 'control_id',
        targetKey: 'id',
        as: 'control'
    }, {
        hasMany: 'AutoControlStepMinMax',
        foreignKey: 'auto_control_step_id',
        as: 'autoControlStepMinMaxes'
    }, {
        belongsTo: 'PBand',
        foreignKey: 'p_band_id',
        targetKey: 'id',
        as: 'pBand'
    }, {
        belongsTo: 'Sensor',
        foreignKey: 'expect_temperature_sensor_id',
        targetKey: 'id',
        as: 'expectTemperatureSensor'
    },],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
