const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const ERP = require('../utils/erp');

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['motor', 'power'],
            defaultValue: 'power',
            allowNull: false
        },         
        'user_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },  
        'control_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        }              
    },     
    options: {
        indexes: [{
            name: 'type',
            fields: ['type']
        },{
            name: 'user_id',   
            fields: ['user_id']
        }],
        tableName: 'user_control_rels',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },  
    associations: [{
        belongsTo: 'User',
        foreignKey: 'user_id',
        targetKey: 'user_id',
        as: 'user'       
    }],     
    methods: {  
        instanceMethods: {},
        classMethods: {
            getUserControl: ( {type}) => {  
                return new Promise(async (resolve, reject) => {
                    try {                                                      
                        let where = {};
                        if (type) where.type = type;

                        const count = await sequelize.models.Control.count({where});
                        const rows = await sequelize.models.Control.findAll({
                            order: [['order', 'DESC']],
                            where       
                        });       
                        resolve({count, rows});             
                    } catch (e) {                            
                        reject(e);  
                    }
                });
            },    
            getUserCheck: (body) => {  
                return new Promise(async (resolve, reject) => {
                    try {                                                               
                        const platform = (body.platform)? body.platform : 'farm';                                  
                        const data = await ERP.getUserCheck(platform, body);  
                        resolve(data);                                    
                    } catch (e) {                            
                        reject(e);  
                    }
                });
            },                                                       
        }      
    }  
}; 
