const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },  
        'sensor_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },        
        'is_auto_reset': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },         
    },     
    options: {
        indexes: [{
            name: 'house_id',
            fields: ['house_id']
        },{
            name: 'sensor_id',   
            fields: ['sensor_id']
        }],
        tableName: 'flow_meter',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },  
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }, {
        belongsTo: 'Sensor',
        foreignKey: 'sensor_id',
        targetKey: 'id',
        as: 'sensor'        
    }, {
        hasMany: 'FlowMeterRound',
        foreignKey: 'flow_meter_id',
        as: 'flowMeterRound'       
    }],  
    methods: {  
        instanceMethods: {},
        classMethods: {
            updateFlowMeter: (id,body) => {
                return new Promise(async (resolve, reject) => {
                    try {   
                        const updateData = await sequelize.models.FlowMeter.update({
                            is_auto_reset : body.is_auto_reset
                        }, {    
                            where:
                                { id: id }
                        });         
                        resolve(updateData);    
                    } catch (e) {    
                        reject(e);   
                    }  
                });
            }, 
            updateFlowMeterPassWd: (id,body) => {
                return new Promise(async (resolve, reject) => {
                    try {   
                        const updateData = await sequelize.models.FlowMeter.update({
                            is_auto_reset : body.is_auto_reset
                        }, {    
                            where:
                                { id: id }
                        });         
                        resolve(updateData);    
                    } catch (e) {    
                        reject(e);   
                    }  
                });
            },             
            getFlowMeter: (sensor_id,body) => {
                return new Promise(async (resolve, reject) => {
                    try {   
                        const count = await sequelize.models.FlowMeter.count({
                            where: {
                                sensor_id : sensor_id
                            }
                        })   
                        if(!count){
                            const createData = await sequelize.models.FlowMeter.create({
                                sensor_id: sensor_id,
                                house_id: body.house_id,
                                is_auto_reset: false
                            });
                            await sequelize.models.FlowMeterRound.create({
                                flow_meter_id: createData.id,
                                start_hour : 0 ,
                                end_hour : 24   
                            });   
                        }      
                        const flow_meter = await sequelize.models.FlowMeter.findOne({
                            where: {
                                sensor_id : sensor_id
                            }
                        }) 
                        let selectQuery = ` SELECT AA.house_id,AA.sensor_id, BB.* FROM flow_meter as AA `;
                        selectQuery += ` left join flow_meter_round  as BB on AA.id=BB.flow_meter_id `;
                        selectQuery += ` where AA.house_id="${body.house_id}"  and AA.sensor_id="${sensor_id}" `;
  
                        const flow_meter_round = await sequelize.query(selectQuery, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        resolve({flow_meter,flow_meter_round});  
                    } catch (e) {   
                        reject(e);   
                    }  
                });
            },              
            getFlowMeterAddress: () => {
                return new Promise(async (resolve, reject) => {
                    try {        
  
                        let selectQuery = ` SELECT AA.id ,AA.sensor_id,BB.address FROM flow_meter as AA `;
                        selectQuery += ` left join sensors as BB on AA.sensor_id=BB.id `;
                        selectQuery += ` WHERE AA.is_auto_reset=1 `;
                        selectQuery += ` AND LENGTH(BB.address)>4 `;
  
                        const rows = await sequelize.query(selectQuery, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        resolve(rows);  
                    } catch (e) {   
                        reject(e);   
                    }  
                });
            },                                           
        }
    }
};
