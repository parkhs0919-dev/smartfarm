const fs = require('fs');
const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const FILTER_UTIL = require('../utils').filter;

const bom = "\ufeff";
const csvSize = 100;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'sensor_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'position': {
            type: Sequelize.ENUM,
            values: ['in', 'out'],
            defaultValue: 'in',
            allowNull: false
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['temperature','soilTemperature','humidity','soilHumidity','solar','windSpeed','ec','ph','co2','windDirection','rain','power','water','battery','liter','door','frostHumidity','frostTemperature','frost','window','fire','dryTemperature','WetTemperature','co2Temperature','co2Humidity','humidityDeficit','korinsPressure','korinsFall','korinsHumidity','korinsTemperature','korinsWindSpeed','korinsWindDirection','korinsSolar','sumOfTemperature','FolateTemperature','CultureMediumWeight','CultureMediumWaterWeight','sugar','korinsInstantRainFall','korinsCumulativeRainFall','relativeHumidity','dewPointTemperature'],
            defaultValue: 'temperature',
            allowNull: false
        },
        'sensor_name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'unit': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'value': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'revision': { 
            type: Sequelize.DOUBLE(8, 2),
            defaultValue: 0,
            allowNull: false
        },
        'address': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'key': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'out_key': {
            type: Sequelize.STRING(getDBStringLength()),
            unique: true,
            allowNull: true
        },
        'convert_key': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'order': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        },
        'updatedAt': {
            type: Sequelize.DATE,
            allowNull: true
        },
        'type_id': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        }
    },
    options: {
        indexes: [{
            name: 'out_key',
            fields: ['out_key']
        }],
        tableName: 'sensors',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Sensor',
        foreignKey: 'sensor_id',
        targetKey: 'id',
        as: 'originSensor'
    }, {
        hasMany: 'HouseSensorRel',
        foreignKey: 'sensor_id',
        as: 'houseSensorRels'
    }, {
        hasMany: 'Alarm',
        foreignKey: 'sensor_id',
        as: 'alarms'
    }, {
        hasMany: 'SensorMainChart',
        foreignKey: 'sensor_id',
        as: 'sensorMainCharts'
    }, {
        hasOne: 'SensorInfo',
        foreignKey: 'sensor_id',
        as: 'sensorInfo'
    }, {
        hasMany: 'AutoControlSensor',
        foreignKey: 'sensor_id',
        as: 'autoControlSensors'
    }, {
        hasMany: 'SendSensor',
        foreignKey: 'sensor_id',
        as: 'sendSensors'
    }, {
        hasMany: 'ControlMinMaxRange',
        foreignKey: 'sensor_id',
        as: 'controlMinMaxRanges'
    }, {
        hasMany: 'PBand',
        foreignKey: 'in_temperature_sensor_id',
        as: 'inTemperaturePBands'
    }, {
        hasMany: 'PBand',
        foreignKey: 'out_temperature_sensor_id',
        as: 'outTemperaturePBands'
    }, {
        hasMany: 'PBand',
        foreignKey: 'wind_speed_sensor_id',
        as: 'windSpeedPBands'
    }, {
        hasMany: 'HouseWindDirectionSensor',
        foreignKey: 'sensor_id',
        as: 'houseWindDirectionSensors'
    }, {
        hasMany: 'SensorAvgItem',
        foreignKey: 'sensor_id',
        as: 'sensorAvgItem'         
    }, {
        hasMany: 'AutoControlSensorOption',
        foreignKey: 'sensor_id',
        as: 'autoControlSensorOptions'
    }, {
        hasMany: 'AutoControlItem',
        foreignKey: 'expect_temperature_sensor_id',
        as: 'autoControlItems'
    }, {
        hasMany: 'AutoControlStep',
        foreignKey: 'expect_temperature_sensor_id',
        as: 'autoControlSteps'
    }, {
        hasMany: 'FlowMeter',
        foreignKey: 'sensor_id',
        as: 'flowMeter'          
    }, {
        hasMany: 'AutoControlExpectTemperatureSensor',
        foreignKey: 'sensor_id',
        as: 'autoControlExpectTemperatureSensor'
    },],
    methods: {
        instanceMethods: {},
        classMethods: {
            getSensors: ({house_id, size, offset, position, type, types, isTotal}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let include = undefined;
                        if (house_id) {
                            include = [{
                                model: sequelize.models.HouseSensorRel,
                                as: 'houseSensorRels',
                                required: true,
                                where: {house_id}
                            }];
                        }

                        let where = {};
                        let order = [['order', 'ASC'], ['id', 'ASC']];
                        if (position) where.position = position;
                        if (type) where.type = type;
                        if (types) where.type = types.split(',');


                        if (isTotal && house_id) {
                            let screens = await sequelize.models.Screen.findAll({
                                order: [['order', 'ASC']],
                                where: {
                                    house_id,
                                    type: ['outerSensor', 'innerSensor']
                                }
                            });
                            if (screens.length) {
                                if (screens[0].type === 'outerSensor') {
                                    order.unshift(['position', 'DESC']);
                                } else {
                                    order.unshift(['position', 'ASC']);
                                }
                            }
                        }

                        const count = await sequelize.models.Sensor.count({
                            include,
                            where
                        });
                        const rows = await sequelize.models.Sensor.findAll({
                            include,
                            order,
                            where,
                            limit: size ? parseInt(size) : undefined,
                            offset: parseInt(offset)
                        });
                        resolve({count, rows});
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            updateSensors: (sensors) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        // await sequelize.transaction(t => {
                        //     let promises = [];
                        //     sensors.forEach((sensor, order) => {
                        //         promises.push(sequelize.models.Sensor.update({
                        //             sensor_name: sensor.sensor_name,
                        //             revision: sensor.revision,
                        //             order
                        //         }, {
                        //             where: {
                        //                 id: sensor.id
                        //             },
                        //              transaction: t
                        //         }));
                        //     });
                        //     return Promise.all(promises);
                        // });
                        let promises = [];
                        sensors.forEach((sensor, order) => {
                            promises.push(sequelize.models.Sensor.update({
                                sensor_name: sensor.sensor_name,
                                revision: sensor.revision,
                                order
                            }, {
                                where: {
                                    id: sensor.id
                                }
                            }));
                        });
                        await Promise.all(promises);
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getMinMax: id => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let table_names = [], now = new Date().getTime();
                        const startDate = FILTER_UTIL.date(now - 86400000, 'yyyy-MM-dd HH:mm:ss');
                        const endDate = FILTER_UTIL.date(now, 'yyyy-MM-dd HH:mm:ss');
                        const startTime = new Date(startDate);
                        const endTime = new Date(endDate);
                        const startYear = startTime.getFullYear();
                        const startMonth = startTime.getMonth() + 1;
                        const endYear = endTime.getFullYear();
                        const endMonth = endTime.getMonth() + 1;

                        if (startYear === endYear && startMonth === endMonth) {
                            table_names.push('log_sensor_' + startYear + (FILTER_UTIL.attachZero(startMonth)));
                        } else {
                            const endTableName = 'log_sensor_' + endYear + (FILTER_UTIL.attachZero(endMonth));
                            let tableName = 'log_sensor_' + startYear + (FILTER_UTIL.attachZero(startMonth)),
                                year = startYear,
                                month = startMonth;

                            while (tableName !== endTableName) {
                                table_names.push(tableName);

                                const next = FILTER_UTIL.nextYearMonth(year, month);
                                year = next.year;
                                month = next.month;
                                tableName = 'log_sensor_' + year + (FILTER_UTIL.attachZero(month));
                            }
                            table_names.push(endTableName);
                        }

                        let funcs = [], min = null, max = null;
                        const startTimestamp = startTime.getTime();
                        const endTimestamp = endTime.getTime();

                        table_names.forEach(table_name => {
                            funcs.push(callback => {
                                const query = `SELECT MAX(value) AS max, MIN(value) AS min FROM ${table_name}
                                WHERE sensor_id = ${id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'`;
                                sequelize.query(query, {
                                    type: Sequelize.QueryTypes.SELECT
                                }).then(data => {
                                    if (data.length) {
                                        if (min === null) {
                                            min = data[0].min;
                                        } else if (min > data[0].min) {
                                            min = data[0].min;
                                        }
                                        if (max === null) {
                                            max = data[0].max;
                                        } else if (max < data[0].max) {
                                            max = data[0].max;
                                        }
                                        callback(null, true);
                                    } else {
                                        callback(null, true);
                                    }
                                }).catch(e => {
                                    callback(null, true);
                                });
                            });
                        });

                        async.series(funcs, (error, results) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve({
                                    min,
                                    max
                                });
                            }
                        });
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getAvg: (id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let endTime = new Date(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd') + ' 00:00:00');
                        let startTime = new Date(endTime.getTime() - 86400000);
                        let table_name = 'log_sensor_' + startTime.getFullYear() + (FILTER_UTIL.attachZero(startTime.getMonth() + 1));
                        let startTimestamp = startTime.getTime();
                        let endTimestamp = endTime.getTime();
                        const query = `SELECT ROUND(AVG(value), 1) AS value FROM ${table_name}
                        WHERE sensor_id = ${id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'`;
                        sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        }).then(data => {
                            if (data && data.length) {
                                resolve(data[0].value || 0);
                            } else {
                                resolve(null);
                            }
                            return true;
                        }).catch(e => {
                            resolve(null);
                        });
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getSunAvg: (id, sunDate) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let endTime = new Date(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd') + ' 00:00:00');
                        let startTime = new Date(endTime.getTime() - 86400000);
                        let table_name = 'log_sensor_' + startTime.getFullYear() + (FILTER_UTIL.attachZero(startTime.getMonth() + 1));
                        let startTimestamp = startTime.getTime();
                        let endTimestamp = endTime.getTime();
                        let riseTimestamp = new Date(FILTER_UTIL.date(startTime, 'yyyy-MM-dd') + ` ${FILTER_UTIL.attachZero(sunDate.rise_hour)}:${FILTER_UTIL.attachZero(sunDate.rise_minute)}:00`).getTime();
                        let setTimestamp = new Date(FILTER_UTIL.date(startTime, 'yyyy-MM-dd') + ` ${FILTER_UTIL.attachZero(sunDate.set_hour)}:${FILTER_UTIL.attachZero(sunDate.set_minute)}:00`).getTime();
                        let result = {};
                        const nightQuery = `SELECT ROUND(AVG(value), 1) AS value FROM ${table_name}
                        WHERE sensor_id = ${id}
                        AND ((created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(riseTimestamp + 1)}')
                        OR (created_at > '${FILTER_UTIL.dateToDb(setTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'))`;
                        const dayQuery = `SELECT ROUND(AVG(value), 1) AS value FROM ${table_name}
                        WHERE sensor_id = ${id}
                        AND created_at > '${FILTER_UTIL.dateToDb(riseTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(setTimestamp + 1)}'`;
                        sequelize.query(nightQuery, {
                            type: Sequelize.QueryTypes.SELECT
                        }).then(data => {
                            if (data && data.length) {
                                result.night = data[0].value;
                                return sequelize.query(dayQuery, {
                                    type: Sequelize.QueryTypes.SELECT
                                }).then(data => {
                                    if (data && data.length) {
                                        result.day = data[0].value || 0;
                                        resolve(result);
                                    } else {
                                        resolve(null);
                                    }
                                    return true;
                                });
                            } else {
                                resolve(null);
                                return true;
                            }
                        }).catch(e => {
                            resolve(null);
                        });
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getSum: (sensorInfo) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let table_names = [];
                        const sensor_id = sensorInfo.sensor_id;
                        const min_value = sensorInfo.min_value;
                        const startTime = new Date(`${sensorInfo.start_date} 00:00:00`);
                        const endTime = new Date(`${sensorInfo.end_date} 00:00:00`);
                        const startYear = startTime.getFullYear();
                        const startMonth = startTime.getMonth() + 1;
                        const endYear = endTime.getFullYear();
                        const endMonth = endTime.getMonth() + 1;

                        const start = parseInt(sensorInfo.start_date.replace(/\-/g,''));    
                        const end = parseInt(sensorInfo.end_date.replace(/\-/g,''));  
                        if(start<=end){       
                            if (startYear === endYear && startMonth === endMonth) {
                                table_names.push('log_sensor_' + startYear + (FILTER_UTIL.attachZero(startMonth)));
                            } else {
                                const endTableName = 'log_sensor_' + endYear + (FILTER_UTIL.attachZero(endMonth));
                                let tableName = 'log_sensor_' + startYear + (FILTER_UTIL.attachZero(startMonth)),
                                    year = startYear,
                                    month = startMonth;

                                while (tableName !== endTableName) {
                                    table_names.push(tableName);

                                    const next = FILTER_UTIL.nextYearMonth(year, month);
                                    year = next.year;
                                    month = next.month;
                                    tableName = 'log_sensor_' + year + (FILTER_UTIL.attachZero(month));
                                }
                                table_names.push(endTableName);
                            }
                        }

                        let funcs = [], value = 0;
                        const startTimestamp = startTime.getTime();
                        const endTimestamp = endTime.getTime();

                        table_names.forEach(table_name => {
                            funcs.push(callback => {
                                const query = `SELECT ROUND(SUM(a.value), 1) AS value FROM (SELECT AVG(a.value) AS value, FROM_UNIXTIME(a.created_at * 86400) AS created_at FROM
                                (SELECT \`value\`, FLOOR(UNIX_TIMESTAMP(created_at) / 86400) AS created_at FROM ${table_name}
                                WHERE sensor_id = ${sensor_id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'
                                ) a GROUP BY a.created_at HAVING value >= ${min_value}) a`;
                                sequelize.query(query, {
                                    type: Sequelize.QueryTypes.SELECT
                                }).then(data => {
                                    if (data && data.length) {
                                        value += data[0].value || 0;
                                    }
                                    callback(null, true);
                                    return true;
                                }).catch(e => {
                                    callback(null, true);
                                });
                            });
                        });

                        async.series(funcs, (error, results) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(value);
                            }
                        });
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            createCsv: (filePath, startDate, endDate, sensor) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let table_names = [];
                        const startTime = new Date(startDate);
                        const endTime = new Date(endDate);
                        const startYear = startTime.getFullYear();
                        const startMonth = startTime.getMonth() + 1;
                        const endYear = endTime.getFullYear();
                        const endMonth = endTime.getMonth() + 1;

                        if (startYear === endYear && startMonth === endMonth) {
                            table_names.push('log_sensor_' + startYear + (FILTER_UTIL.attachZero(startMonth)));
                        } else {
                            const endTableName = 'log_sensor_' + endYear + (FILTER_UTIL.attachZero(endMonth));
                            let tableName = 'log_sensor_' + startYear + (FILTER_UTIL.attachZero(startMonth)),
                                year = startYear,
                                month = startMonth;

                            while (tableName !== endTableName) {
                                table_names.push(tableName);

                                const next = FILTER_UTIL.nextYearMonth(year, month);
                                year = next.year;
                                month = next.month;
                                tableName = 'log_sensor_' + year + (FILTER_UTIL.attachZero(month));
                            }
                            table_names.push(endTableName);
                        }

                        let funcs = [], count = 0;
                        const startTimestamp = startTime.getTime();
                        const endTimestamp = endTime.getTime();

                        let control_query =` select group_concat(AA.control_name order by AA.order asc) as name, `;
                        control_query +=` group_concat(AA.current order by AA.order asc ) as current, `;
                        control_query +=` group_concat(CASE WHEN AA.state='on' THEN '가동' `;
                        control_query +=` WHEN AA.state='off' THEN '정지' `;
                        control_query +=` WHEN AA.state='stop' THEN '정지' `;
                        control_query +=` ELSE  AA.state END  `;
                        control_query +=` order by AA.order asc ) as state `;
                        control_query +=` from controls as AA  `;

                        const controls = await sequelize.query(control_query, {
                            type: Sequelize.QueryTypes.SELECT
                        });

                        if(controls && controls[0]) fs.appendFileSync(filePath, bom + "센서명, 센서값, 측정시각 ,"+controls[0].name+" ,"+controls[0].name+"\n");
                        else fs.appendFileSync(filePath, bom + "센서명, 센서값, 측정시각\n");

                        table_names.forEach(table_name => {
                            funcs.push(callback => {
                                const countQuery = `SELECT COUNT(*) AS count FROM ${table_name}
                                WHERE sensor_id = ${sensor.id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'`;
                                sequelize.query(countQuery, {
                                    type: Sequelize.QueryTypes.SELECT
                                }).then(data => {
                                    if (data.length && data[0].count !== undefined) {
                                        count = data[0].count;
                                        callback(null, true);
                                    } else {
                                        count = 0;
                                        callback(null, true);
                                    }
                                    return true;
                                }).catch(e => {
                                    callback(e, false);
                                });
                            });

                            funcs.push(callback => {
                                let subFuncs = [];

                                for (let i=0; i<Math.ceil(count / csvSize); i++) {
                                    ((index) => {
                                        subFuncs.push(subCallback => {
                                            const selectQuery = `SELECT value, DATE_FORMAT(created_at, "%Y-%m-%d %H:%i") AS created_at ,data FROM ${table_name}
                                            WHERE sensor_id = ${sensor.id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'
                                            ORDER BY created_at ASC LIMIT ${csvSize} OFFSET ${index * csvSize}`;
                                            sequelize.query(selectQuery, {
                                                type: Sequelize.QueryTypes.SELECT
                                            }).then(data => {
                                                data.forEach(log => {
                                                    try{
                                                        let control_data=(log.data)? JSON.parse(log.data) : null;
                                                        if(control_data && control_data[0]) fs.appendFileSync(filePath, `${sensor.sensor_name}, ${log.value}, ${log.created_at} ,`+control_data[0].current+` ,`+control_data[0].state+`\n`);
                                                        else fs.appendFileSync(filePath, `${sensor.sensor_name}, ${log.value}, ${log.created_at}\n`);
                                                    }catch(e){
                                                        fs.appendFileSync(filePath, `${sensor.sensor_name}, ${log.value}, ${log.created_at}\n`);
                                                    }          
                                                });
                                                subCallback(null, true);
                                                return true;
                                            }).catch(e => {
                                                subCallback(e, false);
                                            });
                                        });
                                    })(i);
                                }

                                async.series(subFuncs, (error, results) => {
                                    if (error) {
                                        callback(error, false);
                                    } else {
                                        callback(null, true);
                                    }
                                });
                            });
                        });

                        async.series(funcs, (error, results) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(true);
                            }
                        });
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getChart: (sensor_id, startDate, endDate, isDate = false, startHour = null, startMinute = null, endHour = null, endMinute = null) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let table_names = [];
                        const startTime = new Date(startDate);
                        if(startHour !== null && startMinute !== null) startTime.setHours(startHour, startMinute);
                        const endTime = new Date(endDate);
                        if(endHour !== null && endMinute !== null) endTime.setHours(endHour, endMinute);
                        const startYear = startTime.getFullYear();
                        const startMonth = startTime.getMonth() + 1;
                        const endYear = endTime.getFullYear();
                        const endMonth = endTime.getMonth() + 1;

                        if (startYear === endYear && startMonth === endMonth) {
                            table_names.push('log_sensor_' + startYear + (FILTER_UTIL.attachZero(startMonth)));
                        } else {
                            const endTableName = 'log_sensor_' + endYear + (FILTER_UTIL.attachZero(endMonth));
                            let tableName = 'log_sensor_' + startYear + (FILTER_UTIL.attachZero(startMonth)),
                                year = startYear,
                                month = startMonth;

                            while (tableName !== endTableName) {
                                table_names.push(tableName);

                                const next = FILTER_UTIL.nextYearMonth(year, month);
                                year = next.year;
                                month = next.month;
                                tableName = 'log_sensor_' + year + (FILTER_UTIL.attachZero(month));
                            }
                            table_names.push(endTableName);
                        }

                        const rows = await sequelize.models.Sensor.findOne({
                            where: {
                                id: sensor_id
                            }
                        });

                        let funcs = [], chartData = [];
                        const startTimestamp = startTime.getTime();
                        const endTimestamp = endTime.getTime();

                        table_names.forEach(table_name => {
                            funcs.push(callback => {
                                let query=``;
                                if (rows.type === 'power' || rows.type === 'rain' || rows.type === 'water' || rows.type === 'fire' || rows.type === 'door' || rows.type === 'window') {
                                    query = `SELECT ROUND(AVG(a.value), 0) AS value, FROM_UNIXTIME(a.created_at * ${isDate ? 60 : 7200}) AS created_at FROM
                                    (SELECT \`value\`, FLOOR(UNIX_TIMESTAMP(created_at) / ${isDate ? 60 : 7200}) AS created_at FROM ${table_name}
                                    WHERE sensor_id = ${sensor_id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'
                                    ) a GROUP BY a.created_at`;
                                } else if (rows.type === 'ec') {
                                    query = `SELECT ROUND(AVG(a.value), 2) AS value, FROM_UNIXTIME(a.created_at * ${isDate ? 60 : 7200}) AS created_at FROM
                                    (SELECT \`value\`, FLOOR(UNIX_TIMESTAMP(created_at) / ${isDate ? 60 : 7200}) AS created_at FROM ${table_name}
                                    WHERE sensor_id = ${sensor_id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'
                                    ) a GROUP BY a.created_at`;
                                } else {
                                    query = `SELECT ROUND(AVG(a.value), 1) AS value, FROM_UNIXTIME(a.created_at * ${isDate ? 60 : 7200}) AS created_at FROM
                                    (SELECT \`value\`, FLOOR(UNIX_TIMESTAMP(created_at) / ${isDate ? 60 : 7200}) AS created_at FROM ${table_name}
                                    WHERE sensor_id = ${sensor_id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'
                                    ) a GROUP BY a.created_at`;
                                }

                                sequelize.query(query, {
                                    type: Sequelize.QueryTypes.SELECT
                                }).then(data => {
                                    chartData = [
                                        ...chartData,
                                        ...data
                                    ];
                                    callback(null, true);
                                    return true;
                                }).catch(e => {
                                    callback(null, true);
                                });
                            });
                        });

                        async.series(funcs, (error, results) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(chartData);
                            }
                        });
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getIntegratedChart: (sensor_id, control_id, startDate, endDate, isDate = false, startHour = null, startMinute = null, endHour = null, endMinute = null) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let table_names = [];
                        const startTime = new Date(startDate);
                        if(startHour !== null && startMinute !== null) startTime.setHours(startHour, startMinute);
                        const endTime = new Date(endDate);
                        if(endHour !== null && endMinute !== null) endTime.setHours(endHour, endMinute);
                        const startYear = startTime.getFullYear();
                        const startMonth = startTime.getMonth() + 1;
                        const endYear = endTime.getFullYear();
                        const endMonth = endTime.getMonth() + 1;

                        if (startYear === endYear && startMonth === endMonth) {
                            table_names.push({tableName : 'log_sensor_' + startYear + (FILTER_UTIL.attachZero(startMonth)),
                            controlTableName : 'log_control_' + startYear + (FILTER_UTIL.attachZero(startMonth))});
                        } else {
                            const endTableName = 'log_sensor_' + endYear + (FILTER_UTIL.attachZero(endMonth)),
                                endControlTableName = 'lob_control_' + endYear + (FILTER_UTIL.attachZero(endMonth));

                            let tableName = 'log_sensor_' + startYear + (FILTER_UTIL.attachZero(startMonth)),
                                controlTableName = 'log_control_'+startYear + (FILTER_UTIL.attachZero(startMonth)),
                                year = startYear,
                                month = startMonth;

                            while (tableName !== endTableName) {
                                table_names.push({tableName : tableName, controlTableName : controlTableName});

                                const next = FILTER_UTIL.nextYearMonth(year, month);
                                year = next.year;
                                month = next.month;
                                tableName = 'log_sensor_' + year + (FILTER_UTIL.attachZero(month));
                                controlTableName = 'log_control_'+year + (FILTER_UTIL.attachZero(startMonth));
                            }
                            table_names.push({tableName : endTableName, controlTableName : endControlTableName});
                        }

                        const rows = await sequelize.models.Sensor.findOne({
                            where: {
                                id: sensor_id
                            }
                        });

                        let funcs = [], chartData = [];
                        const startTimestamp = startTime.getTime();
                        const endTimestamp = endTime.getTime();

                        table_names.forEach(table_name => {
                            funcs.push(callback => {
                                let query=``;
                                if (rows.type === 'power' || rows.type === 'rain' || rows.type === 'water' || rows.type === 'fire' || rows.type === 'door' || rows.type === 'window') {
                                    query = `SELECT ROUND(AVG(a.value), 0) AS 'value'
                                                , ROUND(AVG(a.state), 0) AS 'state'
                                                , ROUND(AVG(a.current), 1) AS 'current', ROUND(AVG(a.range), 1) AS 'range'
                                                , FROM_UNIXTIME(a.created_at * 60) AS created_at FROM
                                            (SELECT ${table_name.tableName}.value, (CASE WHEN ${table_name.controlTableName}.state = 'on' THEN 1 ELSE 0 END) AS state, (${table_name.controlTableName}.current*10) as 'current', ${table_name.controlTableName}.range, 
                                            FLOOR(UNIX_TIMESTAMP(${table_name.tableName}.created_at) / ${isDate ? 60 : 7200}) AS created_at FROM ${table_name.tableName}
                                            LEFT JOIN ${table_name.controlTableName} ON ${table_name.tableName}.created_at = ${table_name.controlTableName}.created_at
                                            WHERE ${table_name.tableName}.sensor_id = ${sensor_id} AND ${table_name.controlTableName}.control_id = 1
                                            AND ${table_name.tableName}.created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND ${table_name.tableName}.created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}') a GROUP BY a.created_at`;
                                } else if (rows.type === 'ec') {
                                    query = `SELECT ROUND(AVG(a.value), 2) AS 'value'
                                                , ROUND(AVG(a.state), 0) AS 'state'
                                                , ROUND(AVG(a.current), 1) AS 'current', ROUND(AVG(a.range), 1) AS 'range'
                                                , FROM_UNIXTIME(a.created_at * 60) AS created_at FROM
                                            (SELECT ${table_name.tableName}.value, (CASE WHEN ${table_name.controlTableName}.state = 'on' THEN 1 ELSE 0 END) AS state, (${table_name.controlTableName}.current*10) as 'current', ${table_name.controlTableName}.range, 
                                            FLOOR(UNIX_TIMESTAMP(${table_name.tableName}.created_at) / ${isDate ? 60 : 7200}) AS created_at FROM ${table_name.tableName}
                                            LEFT JOIN ${table_name.controlTableName} ON ${table_name.tableName}.created_at = ${table_name.controlTableName}.created_at
                                            WHERE ${table_name.tableName}.sensor_id = ${sensor_id} AND ${table_name.controlTableName}.control_id = 1
                                            AND ${table_name.tableName}.created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND ${table_name.tableName}.created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}') a GROUP BY a.created_at`;

                                } else {
                                    query = `SELECT ROUND(AVG(a.value), 1) AS 'value'
                                                , ROUND(AVG(a.state), 0) AS 'state'
                                                , ROUND(AVG(a.current), 1) AS 'current', ROUND(AVG(a.range), 1) AS 'range'
                                                , FROM_UNIXTIME(a.created_at * 60) AS created_at FROM
                                            (SELECT ${table_name.tableName}.value, (CASE WHEN ${table_name.controlTableName}.state = 'on' THEN 1 ELSE 0 END) AS state, (${table_name.controlTableName}.current*10) as 'current', ${table_name.controlTableName}.range, 
                                            FLOOR(UNIX_TIMESTAMP(${table_name.tableName}.created_at) / ${isDate ? 60 : 7200}) AS created_at FROM ${table_name.tableName}
                                            LEFT JOIN ${table_name.controlTableName} ON ${table_name.tableName}.created_at = ${table_name.controlTableName}.created_at
                                            WHERE ${table_name.tableName}.sensor_id = ${sensor_id} AND ${table_name.controlTableName}.control_id = 1
                                            AND ${table_name.tableName}.created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND ${table_name.tableName}.created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}') a GROUP BY a.created_at`;
                                }

                                sequelize.query(query, {
                                    type: Sequelize.QueryTypes.SELECT
                                }).then(data => {
                                    chartData = [
                                        ...chartData,
                                        ...data
                                    ];
                                    callback(null, true);
                                    return true;
                                }).catch(e => {
                                    callback(null, true);
                                });
                            });
                        });

                        async.series(funcs, (error, results) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(chartData);
                            }
                        });
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getChartDate: (sensor_id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const now = new Date();
                        let year = now.getFullYear(),
                            month = now.getMonth() + 1,
                            date = now.getDate(),
                            hour = now.getHours();
                        let table_name = 'log_sensor_' + year + (month < 10 ? '0' + month : month);

                        const endTimestamp = new Date(`${year}-${FILTER_UTIL.attachZero(month)}-${FILTER_UTIL.attachZero(date)} ${FILTER_UTIL.attachZero(parseInt(hour / 2) * 2)}:00:00`).getTime();
                        const startTimestamp = new Date(endTimestamp - 86400000).getTime();

                        let chartData = [];

                        if (date === 1) {
                            let prev_table_name = 'log_sensor_';
                            if (month === 1) {
                                prev_table_name += (year - 1) + '12';
                            } else {
                                const prevMonth = month - 1;
                                prev_table_name += year + (prevMonth < 10 ? '0' + prevMonth : '' + prevMonth);
                            }
                            const prevQuery = `SELECT AVG(a.value) AS value, FROM_UNIXTIME(a.created_at * 7200) AS created_at FROM
                            (SELECT \`value\`, FLOOR(UNIX_TIMESTAMP(created_at) / 7200) AS created_at FROM ${prev_table_name}
                            WHERE sensor_id = ${sensor_id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'
                            ) a GROUP BY a.created_at`;
                            chartData = await sequelize.query(prevQuery, {
                                type: Sequelize.QueryTypes.SELECT
                            });
                        }

                        const query = `SELECT AVG(a.value) AS value, FROM_UNIXTIME(a.created_at * 7200) AS created_at FROM
                        (SELECT \`value\`, FLOOR(UNIX_TIMESTAMP(created_at) / 7200) AS created_at FROM ${table_name}
                        WHERE sensor_id = ${sensor_id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'
                        ) a GROUP BY a.created_at`;
                        let result = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });

                        if (chartData.length && result.length) {
                            if (chartData[chartData.length - 1].created_at === result[0].created_at) {
                                let temp = result.splice(0, 1);
                                chartData[chartData.length - 1].value = (chartData[chartData.length - 1].value + temp.value) / 2;
                            }
                            chartData = [
                                ...chartData,
                                ...result
                            ];
                        } else {
                            chartData = result;
                        }

                        resolve(chartData);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getSensorDryWet: (sensor_id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let query1 = ` SELECT value FROM sensors     `;
                        query1 +=` where address =( SELECT address FROM  sensors WHERE id= ${sensor_id} ) `;
                        query1 +=` and type = "dryTemperature"  `;

                        let query2 = ` SELECT value FROM sensors     `;
                        query2 +=` where address =( SELECT address FROM  sensors WHERE id= ${sensor_id} ) `;
                        query2 +=` and type = "wetTemperature"  `;

                        let dryTemperature = await sequelize.query(query1, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        let wetTemperature = await sequelize.query(query2, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        resolve({
                            dryTemperature : dryTemperature[0].value,
                            wetTemperature : wetTemperature[0].value
                        });

                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getSensorDryWet2: (sensor_id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let query1 = ` SELECT value FROM sensors     `;
                        query1 +=` where address =( SELECT address FROM  sensors WHERE id= ${sensor_id} ) `;
                        query1 +=` and type = "temperature"  `;

                        let query2 = ` SELECT value FROM sensors     `;
                        query2 +=` where address =( SELECT address FROM  sensors WHERE id= ${sensor_id} ) `;
                        query2 +=` and type = "humidity"  `;

                        let temperature = await sequelize.query(query1, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        let humidity = await sequelize.query(query2, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        resolve({
                            temperature : temperature[0].value,
                            humidity : humidity[0].value
                        });

                    } catch (e) {
                        console.log(e)
                        reject(e);
                    }
                });
            },
            getSensorAddress: () => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let query = ` SELECT * FROM sensors    `;
                        query +=` where LENGTH(address)=8  `;
                        let data = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        resolve(data);

                    } catch (e) {
                        reject(e);
                    }
                });
            },
            updateSensorsReset: (id,body) => {
                return new Promise(async (resolve, reject) => {   
                    try {                          
                        const updatedData = await sequelize.models.Sensor.update({
                            ...body              
                         }, {   
                             where: 
                             {
                                 id : id
                             }  
                         });    
                         resolve(updatedData);          
                    } catch (e) {  
                        //console.log(e);
                        reject(e);   
                    }
                });
            },
            getPanelAlarm: (house_id, id) => {
                return new Promise (async (resolve, reject) => {
                    try {

                        let rows = [];
                        const count = await sequelize.models.House.count({
                            where :{
                                id : house_id,
                                is_use_alarm : true
                            }
                        });

                        if(count){
                            rows = await sequelize.models.Sensor.findAll({
                                include : [{
                                    model: sequelize.models.HouseSensorRel,
                                    as: 'houseSensorRels',
                                    required: true,
                                    where : {
                                        house_id : house_id
                                    }
                                }],
                                where :{
                                    id : id
                                }
                            });
                        }

                        resolve(rows);
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
