const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const CONFIG = require('../config');

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'sensor_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'condition': {
            type: Sequelize.ENUM,
            values: ['over', 'under', 'equal'],
            defaultValue: 'over',
            allowNull: false
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['on', 'off'],
            defaultValue: 'on',
            allowNull: false
        },
        'value': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
    },
    options: {
        indexes: [{
            name: 'unique_key',
            fields: ['house_id', 'sensor_id', 'name', 'condition', 'value']
        }, {
            name: 'state',
            fields: ['state']
        }],
        tableName: 'control_min_max_range',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }, {
        belongsTo: 'Sensor',
        foreignKey: 'sensor_id',
        targetKey: 'id',
        as: 'sensor'
    }, {
        hasMany: 'AutoControlItemMinMax',
        foreignKey: 'control_min_max_range_id',
        as: 'autoControlItemMinMaxes'
    }, {
        hasMany: 'AutoControlStepMinMax',
        foreignKey: 'control_min_max_range_id',
        as: 'autoControlStepMinMaxes'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            getControlMinMaxRanges: query => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let where = {};
                        if (query.house_id) where.house_id = query.house_id;
                        if (query.state) where.state = query.state;

                        const count = await sequelize.models.ControlMinMaxRange.count({where});
                        if (count) {
                            const rows = await sequelize.models.ControlMinMaxRange.findAll({
                                include: [{
                                    model: sequelize.models.Sensor,
                                    as: 'sensor'
                                }],
                                where
                            });
                            if (rows.length) {
                                resolve({
                                    count,
                                    rows
                                });
                            } else {
                                reject(false);
                            }
                        } else {
                            reject(false);
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
