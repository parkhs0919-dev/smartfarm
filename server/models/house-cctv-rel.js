const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'cctv_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        }
    },
    options: {
        tableName: 'house_cctv_rels',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }, {
        belongsTo: 'Cctv',
        foreignKey: 'cctv_id',
        targetKey: 'id',
        as: 'cctv'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
