const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false  
        },
        'name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
    },       
    options: {
        tableName: 'animal_kinds',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate          
    },
    associations: [],
    methods: {   
        instanceMethods: {},
        classMethods: {
          getAnimalKind: () => {
              return new Promise(async (resolve, reject) => {
                  try {
                    let selectQuery = `select AA.name from animal_kinds AA`;

                    const rows = await sequelize.query(selectQuery, {
                        type: Sequelize.QueryTypes.SELECT
                    });
                    resolve({ rows});
                } catch (e) {    
                    reject(e);
                }
              });
          }
        }
    }
};
