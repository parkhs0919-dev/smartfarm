const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false 
        },
        'animal_type_id': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'animal_code': {
            type: Sequelize.STRING(getDBStringLength()),
            unique: true,
            allowNull: true
        },
        'kind': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'weight': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: false
        },
        'birth_day': {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        'move_day': {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        'health_state': {
            type: Sequelize.ENUM,
            values: ['good', 'normal', 'bad'],
            defaultValue: 'good',
            allowNull: false
        },
        'no': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'barcode': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'mate_count': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'image_url': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'memo': {  
            type: Sequelize.TEXT('long'),
            allowNull: true
        },
        'created_at': {  
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: true
        }      
    },
    options: {            
        tableName: 'animals',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },  
    associations: [{    
        hasMany: 'AnimalWeight',
        foreignKey: 'animal_id',
        as: 'animalWeights'
    }, {
        hasMany: 'AnimalFeed',
        foreignKey: 'animal_id',
        as: 'animalFeeds'
    }, {
        hasMany: 'AnimalWater',
        foreignKey: 'animal_id',
        as: 'animalWaters'
    }, {
        hasMany: 'AnimalMate',
        foreignKey: 'animal_id',
        as: 'animalMates'
    }, {
        hasMany: 'AnimalBreed',
        foreignKey: 'animal_id',
        as: 'animalBreeds'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
          createAnimal: ({animal_type_id,animal_code,kind,weight, birth_day, move_day, health_state, no, barcode, mate_count, image_url, memo}) => {
              return new Promise(async (resolve, reject) => {
                  try {                        
                      let where = {};
                      where.animal_code = animal_code;
      
                      const count = await sequelize.models.Animal.count({where});
                      if(count==0){
                        const createdData = await sequelize.models.Animal.create({
                            animal_type_id:animal_type_id,
                            animal_code:animal_code,
                            kind:kind,
                            weight : weight,
                            birth_day : birth_day,
                            move_day : move_day,
                            health_state : health_state,
                            no : no,
                            barcode : barcode,
                            mate_count : (mate_count)? mate_count : 0,
                            image_url : image_url,
                            memo : memo
                        });
                        resolve(true);
                      }else{
                        resolve(false);
                      }  
                      
                  } catch (e) {
                      reject(e);
                  }
              });
          },
          getAnimal: (animal_type_id,{animal_code,offset,size}) => {
              return new Promise(async (resolve, reject) => {
                  try {
                      let where = {};
                      where.animal_type_id = animal_type_id;
                      if (animal_code) where.animal_code = animal_code;
      
                      const count = await sequelize.models.Animal.count({where});
                      let selectQuery = `SELECT id, animal_type_id, animal_code, kind,  birth_day`;
                      selectQuery +=` ,move_day, health_state, no, barcode, mate_count`;
                      selectQuery +=` ,image_url, memo, AnimalBreed.* ,ifnull(FLOOR(AnimalWeight.weight) ,Animal.weight ) as weight FROM animals AS Animal `;
   
                      selectQuery +=` LEFT OUTER JOIN ( `; 
                      selectQuery +=` SELECT AA.animal_id, `;
                      selectQuery +=` SUM(CASE WHEN AA.type='estrus' THEN AA.cnt ELSE 0 END) AS 'estrus', `;
                      selectQuery +=` SUM(CASE WHEN AA.type='insemination' THEN AA.cnt ELSE 0 END) AS 'insemination',`;
                      selectQuery +=` SUM(CASE WHEN AA.type='check' THEN AA.cnt ELSE 0 END) AS 'check',`;
                      selectQuery +=` SUM(CASE WHEN AA.type='dryUp' THEN AA.cnt ELSE 0 END) AS 'dryUp',`;
                      selectQuery +=` SUM(CASE WHEN AA.type='birth' THEN AA.cnt ELSE 0 END) AS 'birth'`;
                      selectQuery +=` from (`;
                      selectQuery +=` SELECT ZZ.animal_id , ZZ.type ,count(*) as cnt FROM animal_breeds ZZ `;
                      selectQuery +=` WHERE REPLACE(ZZ.start_date, '-', '')<=date_format(curdate( ), '%Y%m%d' ) and REPLACE(ZZ.end_date, '-', '')>=date_format(curdate( ), '%Y%m%d' )`;
                      selectQuery +=` GROUP BY ZZ.animal_id,ZZ.type`;
                      selectQuery +=` ) AS AA group by AA.animal_id `;   
                      selectQuery +=` ) AS AnimalBreed ON Animal.id=AnimalBreed.animal_id `;

                      selectQuery +=` LEFT OUTER JOIN ( `;
                      selectQuery +=` SELECT animal_id, `;
                      selectQuery +=` SUBSTRING_INDEX(group_concat(weight  ORDER BY date DESC ,id DESC separator   ","), "," ,1) AS weight `;
                      selectQuery +=` FROM animal_weights `;       
                      selectQuery +=` GROUP BY animal_id `;
                      selectQuery +=` ) AS AnimalWeight ON  Animal.id=AnimalWeight.animal_id `;
    
                      selectQuery +=` WHERE Animal.animal_type_id=${animal_type_id}`;  
                      if (animal_code) selectQuery +=` and Animal.animal_code like '%${animal_code}%' `;
                      selectQuery +=` ORDER BY Animal.created_at DESC LIMIT ${size ? parseInt(size) : 0} OFFSET ${parseInt((offset)?offset:0)}`;
  
                      const rows = await sequelize.query(selectQuery, {  
                          type: Sequelize.QueryTypes.SELECT
                      });
                      resolve({count, rows});
                  } catch (e) {
                      reject(e);  
                  }
              });
            },
            getAnimalBreed: (animal_type_id,{offset,size}) => {
                return new Promise(async (resolve, reject) => {
                    try {  
                        let where = {};
                        where.animal_type_id = animal_type_id;
        
                        const count = await sequelize.models.Animal.count({where});
                        let selectQuery = `SELECT id, animal_type_id, animal_code, kind, weight, birth_day`;
                        selectQuery +=` ,move_day, health_state, no, barcode, mate_count`;
                        selectQuery +=` ,image_url, memo, AnimalBreed.* FROM animals AS Animal `;

                        selectQuery +=` LEFT OUTER JOIN ( `;  
                        selectQuery +=` SELECT AA.animal_id, `;
                        selectQuery +=` SUM(CASE WHEN AA.type='estrus' THEN AA.cnt ELSE 0 END) AS 'estrus', `;
                        selectQuery +=` SUM(CASE WHEN AA.type='insemination' THEN AA.cnt ELSE 0 END) AS 'insemination',`;
                        selectQuery +=` SUM(CASE WHEN AA.type='check' THEN AA.cnt ELSE 0 END) AS 'check',`;
                        selectQuery +=` SUM(CASE WHEN AA.type='dryUp' THEN AA.cnt ELSE 0 END) AS 'dryUp',`;
                        selectQuery +=` SUM(CASE WHEN AA.type='birth' THEN AA.cnt ELSE 0 END) AS 'birth'`;
                        selectQuery +=` from (`;
                        selectQuery +=` SELECT ZZ.animal_id , ZZ.type ,count(*) as cnt FROM animal_breeds ZZ `;
                        selectQuery +=` WHERE REPLACE(ZZ.start_date, '-', '')<=date_format(curdate( ), '%Y%m%d' ) and REPLACE(ZZ.end_date, '-', '')>=date_format(curdate( ), '%Y%m%d' )`;
                        selectQuery +=` GROUP BY ZZ.animal_id,ZZ.type`;
                        selectQuery +=` ) AS AA group by AA.animal_id `;   
                        selectQuery +=` ) AS AnimalBreed ON Animal.id=AnimalBreed.animal_id `;
     
                        selectQuery +=` where Animal.animal_type_id=${animal_type_id}`;  
                        selectQuery +=` ORDER BY Animal.created_at DESC LIMIT ${size ? parseInt(size) : 0} OFFSET ${parseInt((offset)?offset:0)}`;
                            
                        const rows = await sequelize.query(selectQuery, {
                            type: Sequelize.QueryTypes.SELECT
                        });   
   
                        resolve({count, rows});
                    } catch (e) {       
                        reject(e);  
                    }
                });  
            },            
            getAnimalDetail: (animal_id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let selectQuery = `select Animal.id, Animal.animal_type_id, Animal.animal_code, Animal.kind,Animal.birth_day, Animal.move_day `;
                        selectQuery +=` , Animal.health_state, Animal.no, Animal.barcode, Animal.mate_count, Animal.image_url, Animal.memo, Animal.created_at `;
                        selectQuery +=`,ifnull(BB.cnt ,0 ) as cnt , "" as breed_type  ,AnimalBreed.*  `;
                        selectQuery +=` ,ifnull(FLOOR(AnimalWeights.weight) ,Animal.weight ) as weight  `;
                        selectQuery +=` from animals AS Animal  `;

                        selectQuery +=` left join `;  
                        selectQuery +=` ( `;  
                        selectQuery +=` select animal_id , count(*) as cnt from animal_mates group by animal_id  `;
                        selectQuery +=` ) as BB  `;
                        selectQuery +=` on Animal.id=BB.animal_id `;

                        selectQuery +=` LEFT OUTER JOIN ( `;   
                        selectQuery +=` SELECT AA.animal_id, `;
                        selectQuery +=` SUM(CASE WHEN AA.type='estrus' THEN AA.cnt ELSE 0 END) AS 'estrus', `;
                        selectQuery +=` SUM(CASE WHEN AA.type='insemination' THEN AA.cnt ELSE 0 END) AS 'insemination',`;
                        selectQuery +=` SUM(CASE WHEN AA.type='check' THEN AA.cnt ELSE 0 END) AS 'check',`;
                        selectQuery +=` SUM(CASE WHEN AA.type='dryUp' THEN AA.cnt ELSE 0 END) AS 'dryUp',`;
                        selectQuery +=` SUM(CASE WHEN AA.type='birth' THEN AA.cnt ELSE 0 END) AS 'birth'`;
                        selectQuery +=` from (`;
                        selectQuery +=` SELECT ZZ.animal_id , ZZ.type ,count(*) as cnt FROM animal_breeds ZZ `;
                        selectQuery +=` WHERE REPLACE(ZZ.start_date, '-', '')<=date_format(curdate( ), '%Y%m%d' ) and REPLACE(ZZ.end_date, '-', '')>=date_format(curdate( ), '%Y%m%d' )`;
                        selectQuery +=` GROUP BY ZZ.animal_id,ZZ.type`;
                        selectQuery +=` ) AS AA group by AA.animal_id `;   
                        selectQuery +=` ) AS AnimalBreed ON Animal.id=AnimalBreed.animal_id `;

                        selectQuery +=` LEFT OUTER JOIN ( `;
                        selectQuery +=` SELECT * FROM animal_weights WHERE animal_id=${animal_id} ORDER BY date DESC LIMIT 1 `;
                        selectQuery +=` ) AS AnimalWeights ON Animal.id=AnimalWeights.animal_id  `;
                                                 
                        selectQuery +=` where Animal.id=${animal_id} `;  
          
                        const rows = await sequelize.query(selectQuery, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        resolve({ rows});
                    } catch (e) {  
                        reject(e);
                    }
                });
            },
            deleteAnimal: (animal_id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let where = {};
                        where.id = animal_id;

                        const count = await sequelize.models.Animal.destroy({where});
                        resolve(true);
                    } catch (e) {
                      console.log(e);
                        reject(e);
                    }
                });
            },
            updateAnimalImg: (animal_id,image_url) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        await sequelize.models.Animal.update({
                            image_url:image_url,
                        }, {    
                            where: {id : animal_id}
                        });
                        resolve(true);
                    } catch (e) {
                      console.log(e)
                        reject(e);
                    }
                });
            },
            updateAnimal: ({animal_code,kind,weight, birth_day, move_day, health_state, no, barcode, mate_count}, id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        await sequelize.models.Animal.update({
                            animal_code,
                            kind,
                            weight,  
                            birth_day,
                            move_day,
                            health_state,
                            no,
                            barcode,
                            mate_count
                        }, {
                            where: {id : id}
                        });
                        resolve(true);
                    } catch (e) {
                      console.log(e);
                        reject(e);
                    }
                });
              },
              updateAnimalMemo: ({memo}, id) => {
                  return new Promise(async (resolve, reject) => {
                      try {
                          await sequelize.models.Animal.update({
                              memo
                          }, {
                              where: {id : id}
                          });
                          resolve(true);
                      } catch (e) {
                        console.log(e);
                          reject(e);
                      }
                  });
              },    
              createMultiAnimal: (params) => {
                return new Promise(async (resolve, reject) => {
                    try {  
                        let i=0;
                        let selectQuery =' select animal_code from animals where animal_code in (';  
                        params.forEach((data) => {
                            i++;        
                            if(params.length==i)  selectQuery +=`'`+data.animal_code+`')`;
                            else  selectQuery +=`'`+data.animal_code+`',`;   
                        });      
                        selectQuery+=' group by animal_code';                   
   
                        const rows = await sequelize.query(selectQuery, {
                            type: Sequelize.QueryTypes.SELECT
                        });  
                        if(rows.length==0){                          
                            await sequelize.transaction(t => {
                                let promises = []; 
                                params.forEach((animal) => {    
                                    promises.push(sequelize.models.Animal.create({
                                        animal_type_id:animal.animal_type_id,
                                        animal_code:animal.animal_code,
                                        kind:animal.kind,
                                        weight : animal.weight,    
                                        birth_day : animal.birth_day,
                                        move_day : animal.move_day,
                                        health_state : animal.health_state,
                                        no : animal.no,
                                        barcode : animal.barcode,
                                        mate_count : (animal.mate_count)? animal.mate_count : 0,
                                        image_url : animal.image_url,
                                        memo :animal.memo  
                                    }, {  
                                        transaction: t
                                    }));  
                                });        
                                return Promise.all(promises);
                            });  
                            resolve(true); 
                        }else{
                            resolve(rows); 
                        }        
                          
                    } catch (e) {  
                        console.log(e);      
                        reject(e);
                    }
                });
              }               
        }   
    }
};
