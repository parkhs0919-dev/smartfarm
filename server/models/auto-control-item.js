const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'auto_control_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'wind_direction_type': {
            type: Sequelize.ENUM,
            values: ['none', 'forward', 'backward'],
            defaultValue: 'none',
            allowNull: false
        },
        'window_position': {
            type: Sequelize.ENUM,
            values: ['none', '1', '2', '3'],
            defaultValue: 'none',
            allowNull: false
        },
        'control_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['open', 'stop', 'close', 'on', 'off'],
            defaultValue: 'stop',
            allowNull: false
        },
        'time': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'percentage': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'p_band_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'p_band_temperature': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'p_band_integral': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'is_temperature_control_option':{
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'rise_time':{
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'drop_time':{
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'expect_temperature':{
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'expect_temperature_sensor_id' : {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'start_temperature_time': {
            type: Sequelize.BIGINT.UNSIGNED,
            allowNull: true
        },
        'start_temperature': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'start_hour': {
            type: Sequelize.TINYINT(1),
            allowNull: true
        },
        'start_minute': {
            type: Sequelize.TINYINT(1),
            allowNull: true
        },
        'end_hour': {
            type: Sequelize.TINYINT(1),
            allowNull: true
        },
        'end_minute': {
            type: Sequelize.TINYINT(1),
            allowNull: true
        },
        'is_sun_date': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'sun_date_start_type': {
            type: Sequelize.ENUM,
            values: ['rise', 'set'],
            allowNull: true
        },
        'sun_date_end_type': {
            type: Sequelize.ENUM,
            values: ['rise', 'set'],
            allowNull: true
        },
        'sun_date_start_time': {
            type: Sequelize.INTEGER(10),
            allowNull: true
        },
        'sun_date_end_time': {
            type: Sequelize.INTEGER(10),
            allowNull: true
        },
        'sensor_operator': {
            type: Sequelize.ENUM,
            values: ['and', 'or'],
            defaultValue: 'and',
            allowNull: false
        },
        'control_operator': {
            type: Sequelize.ENUM,
            values: ['and', 'or'],
            defaultValue: 'and',
            allowNull: false
        },
        'period_type': {
            type: Sequelize.ENUM,
            values: ['oneMinute', 'immediately'],
            defaultValue: 'oneMinute',
            allowNull: false
        },
        'is_min_max_range': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'delay': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'recount': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'is_using_period': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'period': {
            type: Sequelize.INTEGER(10),
            allowNull: true
        },
        'is_expect_temperature_option': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'expect_temperature_max_value': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'expect_temperature_min_value': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'active_time': {
            type: Sequelize.BIGINT.UNSIGNED,
            allowNull: true
        },
        'current_count': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'active_count': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        },
        'current_active_count': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        },
        'active': {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
    },
    options: {
        indexes: [{
            name: 'wind_direction_type',
            fields: ['wind_direction_type']
        }, {
            name: 'window_position',
            fields: ['window_position']
        }, {
            name: 'period_type',
            fields: ['period_type']
        }],
        tableName: 'auto_control_items',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'AutoControl',
        foreignKey: 'auto_control_id',
        targetKey: 'id',
        as: 'autoControl'
    }, {
        belongsTo: 'Control',
        foreignKey: 'control_id',
        targetKey: 'id',
        as: 'control'
    }, {
        hasMany: 'AutoControlSensor',
        foreignKey: 'auto_control_item_id',
        as: 'autoControlSensors'
    }, {
        hasMany: 'AutoControlControl',
        foreignKey: 'auto_control_item_id',
        as: 'autoControlControls'
    }, {
        hasMany: 'AutoControlItemMinMax',
        foreignKey: 'auto_control_item_id',
        as: 'autoControlItemMinMaxes'
    }, {
        hasMany: 'AutoControlExpectTemperatureSensor',
        foreignKey: 'auto_control_item_id',
        as: 'autoControlExpectTemperatureSensors'
    }, {
        belongsTo: 'PBand',
        foreignKey: 'p_band_id',
        targetKey: 'id',
        as: 'pBand'
    },{
        belongsTo: 'Sensor',
        foreignKey: 'expect_temperature_sensor_id',
        targetKey: 'id',
        as: 'expectTemperatureSensor'
    }, ],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
