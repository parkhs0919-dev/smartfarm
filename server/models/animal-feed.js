const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false 
        },        
        'animal_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'date': {
            type: Sequelize.DATE,
            primaryKey: true,
            allowNull: false
        },
        'feed': {
            type: Sequelize.DOUBLE(8, 2),
            defaultValue: 0,
            allowNull: false
        },
        'unit': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },        
        'memo': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
    },  
    options: {
        tableName: 'animal_feeds',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Animal',
        foreignKey: 'animal_id',
        targetKey: 'id',
        as: 'animal'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {   
          getAnimalFeed: (animal_id , {offset,size}) => {
              return new Promise(async (resolve, reject) => {
                  try {
                      let where = {};
                      where.animal_id = animal_id;

                      const count = await sequelize.models.AnimalFeed.count({where});
                      const rows = await sequelize.models.AnimalFeed.findAll({
                          order: [['date', 'DESC']],
                          where,
                          limit: size ? parseInt(size) : 10,
                          offset: offset ? parseInt(offset) :0
                      });
                      resolve({count, rows});
                  } catch (e) {
                      reject(e);
                  }
              });
          },
          createAnimalFeed: ({ animal_id ,date ,feed ,memo,unit}) => {
              return new Promise(async (resolve, reject) => {
                  try {
                      const createdData = await sequelize.models.AnimalFeed.create({
                          animal_id:animal_id,
                          date:date,
                          feed:feed,
                          unit:unit,  
                          memo:memo 
                      });
                      resolve(createdData);
                  } catch (e) {
                      reject(e);
                  }
              });
            },
            deleteAnimalFeed: (id) => {
              return new Promise(async (resolve, reject) => {
                  try {    
                      let where = {};
                      where.id = id;
  
                      const count = await sequelize.models.AnimalFeed.destroy({where});
                      resolve(true);
                  } catch (e) {        
                    console.log(e);
                      reject(e);
                  }
              });
            }
        }
    }
};
