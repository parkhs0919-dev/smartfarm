const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const STD = require('../metadata/standards');
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },        
        'user_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },    
        'platform': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'body': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },        
        'data': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },  
        'channel': {   
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },  
        'created_at': {
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }   
    },
    options: {
        indexes: [],
        tableName: 'fcm_log',  
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        name: 'user_id',
        fields: ['user_id']
    }],    
    methods: { 
        instanceMethods: {},
        classMethods: {
            createFcmLog: (user_id ,plaform ,body ,data ,channel) => {
                return new Promise(async (resolve, reject) => {  
                    try {  
                        const createdData = await sequelize.models.FcmLog.create({
                            user_id:user_id,
                            plaform:plaform,    
                            body:body,
                            data:JSON.stringify(data),  
                            channel:channel 
                        });    
                        resolve(createdData);
                    } catch (e) {      
                        reject(e);
                    }
                });  
            }, 
        }   
    } 
};
