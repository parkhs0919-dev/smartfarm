const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'query': {
            type: Sequelize.TEXT('long'),
            allowNull: false
        },
        'created_at': {
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }
    },
    options: {
        tableName: 'alters',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
