const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['normal', 'legacy', 'ptz'],
            defaultValue: 'normal',
            allowNull: false
        },
        'cctv_name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'address': {
            type: Sequelize.STRING(getDBStringLength()),
            unique: true,
            allowNull: true
        },
        'port': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            unique: true,
            allowNull: false
        },
        'user': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'pass': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'url': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'order': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        }
    },
    options: {
        tableName: 'cctvs',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        hasMany: 'HouseCctvRel',
        foreignKey: 'cctv_id',
        as: 'houseCctvRels'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            getCctvs: ({house_id, size, offset}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let include = undefined;
                        if (house_id) {
                            include = [{
                                model: sequelize.models.HouseCctvRel,
                                as: 'houseCctvRels',
                                required: true,
                                where: {house_id}
                            }];
                        }

                        let where = {};

                        const count = await sequelize.models.Cctv.count({
                            include,
                            where
                        });
                        const rows = await sequelize.models.Cctv.findAll({
                            order: [['order', 'ASC'], ['id', 'ASC']],
                            include,
                            where,
                            limit: size ? parseInt(size) : undefined,
                            offset: parseInt(offset)
                        });
                        resolve({count, rows});
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            updateCctvs: (cctvs) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        await sequelize.transaction(t => {
                            let promises = [];
                            cctvs.forEach((cctv, order) => {
                                promises.push(sequelize.models.Cctv.update({
                                    cctv_name: cctv.cctv_name,
                                    order
                                }, {
                                    where: {
                                        id: cctv.id
                                    },
                                    transaction: t
                                }));
                            });
                            return Promise.all(promises);
                        });
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
