const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'auto_control_item_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'sensor_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'value': {
            type: Sequelize.DOUBLE(8, 2),
            primaryKey: true,
            allowNull: false
        },
        'condition': {
            type: Sequelize.ENUM,
            values: ['over', 'excess', 'below', 'under', 'equal'], // 이상 초과 이하 미만 같음
            defaultValue: 'over',
            primaryKey: true,
            allowNull: false
        },
        'order': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            unique: true,
            allowNull: false
        },
        'is_use_option': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'option_min_value': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'option_max_value': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        }
    },
    options: {
        tableName: 'auto_control_sensors',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'AutoControlItem',
        foreignKey: 'auto_control_item_id',
        targetKey: 'id',
        as: 'autoControlItem'
    }, {
        belongsTo: 'Sensor',
        foreignKey: 'sensor_id',
        targetKey: 'id',
        as: 'sensor'
    }, {
        hasMany: 'AutoControlSensorOption',
        foreignKey: 'order',
        as: 'autoControlSensorOptions'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
