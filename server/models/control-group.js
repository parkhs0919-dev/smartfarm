const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const STD = require('../metadata/standards');

const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['windDirection', 'motor', 'power'],
            defaultValue: 'windDirection',
            allowNull: false
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['open', 'stop', 'close', 'on', 'off'],
            allowNull: true
        },
        'on_off_state': {
            type: Sequelize.ENUM,
            values: ['on', 'off'],
            allowNull: true
        },
        'control_group_name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'direction': {
            type: Sequelize.ENUM,
            values: ['none', 'left', 'right'],
            allowNull: true
        },
        'window_position': {
            type: Sequelize.ENUM,
            values: ['none', '1', '2', '3'],
            allowNull: true
        },
        'created_at': {
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }
    },
    options: {
        indexes: [{
            name: 'unique_key',
            fields: ['house_id', 'type', 'direction', 'window_position'],
            unique: true,
        }, {
            name: 'type',
            fields: ['type']
        }, {
            name: 'direction',
            fields: ['direction']
        }, {
            name: 'window_position',
            fields: ['window_position']
        }, {
            name: 'created_at',
            fields: ['created_at']
        }],
        tableName: 'control_groups',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }, {
        hasMany: 'ControlGroupItem',
        foreignKey: 'control_group_id',
        as: 'controlGroupItems'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            createControlGroup: body => {
                return new Promise(async (resolve, reject) => {
                    let transaction;
                    try {
                        transaction = await sequelize.transaction();

                        const createdData = await sequelize.models.ControlGroup.create(body, {
                            include: [{
                                model: sequelize.models.ControlGroupItem,
                                as: 'controlGroupItems'
                            }],
                            transaction
                        });

                        await transaction.commit();
                        resolve(createdData);
                    } catch (e) {
                        transaction && await transaction.rollback();
                        reject(e);
                    }
                });
            },
            updateControlGroups: (body, id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let createdData = null;
                        await sequelize.transaction(t => {
                            return sequelize.models.ControlGroup.update({
                                house_id: body.house_id,
                                type: body.type,
                                on_off_state: body.on_off_state,
                                control_group_name: body.control_group_name,
                                direction: body.direction || null,
                                window_position: body.window_position || null,
                            }, {
                                where: {id: id},
                                transaction: t
                            }).then(() => {
                                return sequelize.models.ControlGroupItem.destroy({
                                    where: {control_group_id: id}
                                }, {
                                    transaction: t
                                });
                            }).then(() => {
                                let promises = [];
                                body.controlGroupItems.forEach((data, index) => {
                                    promises.push(sequelize.models.ControlGroupItem.create({
                                        control_group_id: id,
                                        control_id: data.control_id
                                    }, {
                                        transaction: t
                                    }));

                                });
                                return Promise.all(promises);
                            }).then((data) => {
                                createdData = data;
                                return true;
                            });
                        });
                        resolve(createdData);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getControlGroups: ({house_id}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let query = `SELECT *
                        , ((SELECT COUNT(*) FROM auto_control_items INNER JOIN auto_controls ON auto_control_items.auto_control_id = auto_controls.id
                        WHERE auto_control_items.window_position = temp.window_position AND auto_control_items.wind_direction_type = temp.wind_direction_type AND auto_controls.house_id = temp.house_id) 
                        + ((SELECT COUNT(*) FROM auto_control_steps INNER JOIN auto_controls ON auto_control_steps.auto_control_id = auto_controls.id
                        WHERE auto_control_steps.window_position = temp.window_position AND auto_control_steps.wind_direction_type = temp.wind_direction_type AND auto_controls.house_id = temp.house_id) )) AS autoControlCount
                        FROM ((SELECT control_groups.house_id, control_groups.type, control_groups.window_position, 'forward' AS wind_direction_type FROM control_groups
                        WHERE control_groups.house_id = ${house_id}
                        AND control_groups.type = 'windDirection'
                        GROUP BY control_groups.house_id, control_groups.type, control_groups.window_position)
                        UNION ALL
                        (SELECT control_groups.house_id, control_groups.type, control_groups.window_position, 'backward' AS wind_direction_type FROM control_groups
                        WHERE control_groups.house_id = ${house_id}
                        AND control_groups.type = 'windDirection'
                        GROUP BY control_groups.house_id, control_groups.type, control_groups.window_position)) AS temp
                        ORDER BY temp.window_position ASC, temp.wind_direction_type DESC`;

                        const rows = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });

                        resolve({count: rows.length, rows});
                    } catch (e) {
                        console.log(e);
                        reject(e);
                    }
                });
            },
            getWindDirectionControlGroups: house => {
                return new Promise(async (resolve, reject) => {
                    try {
                        if (house.houseWindDirectionSensor && house.houseWindDirectionSensor.sensor) {
                            let results = {
                                '1': {
                                    forward: [],
                                    backward: []
                                },
                                '2': {
                                    forward: [],
                                    backward: []
                                },
                                '3': {
                                    forward: [],
                                    backward: []
                                },
                            };

                            const leftControlGroups = await sequelize.models.ControlGroup.findAll({
                                where: {
                                    house_id: house.id,
                                    direction: 'left'
                                },
                                include: [{
                                    model: sequelize.models.ControlGroupItem,
                                    as: 'controlGroupItems',
                                    required: true,
                                    include: [{
                                        model: sequelize.models.Control,
                                        as: 'control',
                                        required: true,
                                        where: {
                                            mode: 'auto',
                                        }
                                    }]
                                }]
                            });
                            const rightControlGroups = await sequelize.models.ControlGroup.findAll({
                                where: {
                                    house_id: house.id,
                                    direction: 'right'
                                },
                                include: [{
                                    model: sequelize.models.ControlGroupItem,
                                    as: 'controlGroupItems',
                                    required: true,
                                    include: [{
                                        model: sequelize.models.Control,
                                        as: 'control',
                                        required: true,
                                        where: {
                                            mode: 'auto',
                                        }
                                    }]
                                }]
                            });
                            let direction = STD.house.enumDirections.indexOf(house.direction) * 22.5;
                            const sensorValue = parseFloat(house.houseWindDirectionSensor.sensor.value);

                            if (direction <= 180) {
                                if (direction <= sensorValue && 180 + direction > sensorValue) {
                                    rightControlGroups.forEach(rightControlGroup => {
                                        rightControlGroup.controlGroupItems.forEach(controlGroupItem => {
                                            results[rightControlGroup.window_position].forward.push(controlGroupItem.control);
                                        });
                                    });
                                    leftControlGroups.forEach(leftControlGroup => {
                                        leftControlGroup.controlGroupItems.forEach(controlGroupItem => {
                                            results[leftControlGroup.window_position].backward.push(controlGroupItem.control);
                                        });
                                    });
                                } else {
                                    leftControlGroups.forEach(leftControlGroup => {
                                        leftControlGroup.controlGroupItems.forEach(controlGroupItem => {
                                            results[leftControlGroup.window_position].forward.push(controlGroupItem.control);
                                        });
                                    });
                                    rightControlGroups.forEach(rightControlGroup => {
                                        rightControlGroup.controlGroupItems.forEach(controlGroupItem => {
                                            results[rightControlGroup.window_position].backward.push(controlGroupItem.control);
                                        });
                                    });
                                }
                            } else {
                                direction -= 180;
                                if (direction <= sensorValue && 180 + direction > sensorValue) {
                                    leftControlGroups.forEach(leftControlGroup => {
                                        leftControlGroup.controlGroupItems.forEach(controlGroupItem => {
                                            results[leftControlGroup.window_position].forward.push(controlGroupItem.control);
                                        });
                                    });
                                    rightControlGroups.forEach(rightControlGroup => {
                                        rightControlGroup.controlGroupItems.forEach(controlGroupItem => {
                                            results[rightControlGroup.window_position].backward.push(controlGroupItem.control);
                                        });
                                    });
                                } else {
                                    rightControlGroups.forEach(rightControlGroup => {
                                        rightControlGroup.controlGroupItems.forEach(controlGroupItem => {
                                            results[rightControlGroup.window_position].forward.push(controlGroupItem.control);
                                        });
                                    });
                                    leftControlGroups.forEach(leftControlGroup => {
                                        leftControlGroup.controlGroupItems.forEach(controlGroupItem => {
                                            results[leftControlGroup.window_position].backward.push(controlGroupItem.control);
                                        });
                                    });
                                }
                            }

                            resolve(results);
                        } else {
                            resolve({
                                '1': {
                                    forward: [],
                                    backward: []
                                },
                                '2': {
                                    forward: [],
                                    backward: []
                                },
                                '3': {
                                    forward: [],
                                    backward: []
                                },
                            });
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
