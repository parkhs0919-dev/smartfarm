const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'is_auto_close': {
            type: Sequelize.BOOLEAN,
            primaryKey: true,
            defaultValue: false,
            allowNull: false
        }
    },
    options: {
        tableName: 'cctv_settings',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
