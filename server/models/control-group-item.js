const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'control_group_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'control_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'order': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        },
    },
    options: {
        tableName: 'control_group_items',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'ControlGroup',
        foreignKey: 'control_group_id',
        targetKey: 'id',
        as: 'controlGroup'
    }, {
        belongsTo: 'Control',
        foreignKey: 'control_id',
        targetKey: 'id',
        as: 'control'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
