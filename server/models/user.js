const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const ERP = require('../utils/erp');

module.exports = {
    fields: {
        'user_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'id': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },  
        'password': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },  
        'name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },  
        'grade': {
            type: Sequelize.ENUM,
            values: ['owner', 'member'],
            defaultValue: 'member',
            allowNull: false
        },                                      
        'is_use': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },  
        'created_at': {
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: true  
        },                 
    },     
    options: {
        indexes: [{
            name: 'grade',
            fields: ['grade']
        }],
        tableName: 'users',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },  
    associations: [{
        hasMany: 'UserControlRel',
        foreignKey: 'user_id',
        as: 'userControlRel'         
    }],  
    methods: {  
        instanceMethods: {},
        classMethods: {
            createUser: (body) => {
                return new Promise(async (resolve, reject) => {   
                    try {                                             
                        const platform = (body.platform)? body.platform : 'farm';
                        body.uid=body.id;    
                        const data = await ERP.addUser(platform, body);                                                
                        if(data && data.id){   
                            const createdData = await sequelize.models.User.create({
                                user_id: data.id,        
                                id: body.id,                 
                                password :body.password,       
                                name : body.name,    
                                is_use : body.is_use                              
                            });       
                            if(createdData && createdData.user_id){
                                if(body.motor_list){
                                    const motor_list = body.motor_list.split(',');
                                    if (motor_list && motor_list.length > 0) {
                                        await sequelize.transaction(t => {
                                            let promises = [];  
                                            for (let i = 0; i < motor_list.length; i++) {
                                                promises.push(sequelize.models.UserControlRel.create({
                                                    type : 'motor',
                                                    user_id: createdData.user_id,
                                                    control_id: motor_list[i],
                                                }, {
                                                    transaction: t
                                                }));
                                            }  
                                            return Promise.all(promises);
                                        });
                                    }
                                }
                                if(body.power_list){
                                    const power_list = body.power_list.split(',');
                                    if (power_list && power_list.length > 0) {
                                        await sequelize.transaction(t => {
                                            let promises = [];
                                            for (let i = 0; i < power_list.length; i++) {
                                                promises.push(sequelize.models.UserControlRel.create({
                                                    type : 'power',
                                                    user_id: createdData.user_id,
                                                    control_id: power_list[i],
                                                }, {          
                                                    transaction: t
                                                }));
                                            }
                                            return Promise.all(promises);
                                        });
                                    }    
                                }                              
                            }
                            resolve(true);
                        }else{
                            reject(null); 
                        }    
                    } catch (e) {
                        console.log(e);
                        reject(e);   
                    }
                });
            },    
            updateUser: (user_id,body) => {
                return new Promise(async (resolve, reject) => {   
                    try {
                        if (user_id) {
                            await sequelize.models.User.update({
                                is_use: body.is_use,
                            }, {
                                where: { user_id: user_id }
                            });
                            await sequelize.models.UserControlRel.destroy({
                                where: {
                                    user_id: user_id
                                }
                            });
                            if (body.motor_list && body.motor_list.length > 0) {
                                const motor_list = body.motor_list.split(',');
                                await sequelize.transaction(t => {
                                    let promises = [];
                                    for (let i = 0; i < motor_list.length; i++) {
                                        promises.push(sequelize.models.UserControlRel.create({
                                            type: 'motor',
                                            user_id: user_id,
                                            control_id: motor_list[i],
                                        }, {
                                            transaction: t
                                        }));
                                    }
                                    return Promise.all(promises);
                                });
                            }
                            if (body.power_list && body.power_list.length > 0) {
                                const power_list = body.power_list.split(',');
                                await sequelize.transaction(t => {
                                    let promises = [];
                                    for (let i = 0; i < power_list.length; i++) {
                                        promises.push(sequelize.models.UserControlRel.create({
                                            type : 'power',
                                            user_id: user_id,
                                            control_id: power_list[i],
                                        }, {       
                                            transaction: t
                                        }));   
                                    }
                                    return Promise.all(promises);
                                });
                            }                            
                        }  
                        resolve(true);   
                    } catch (e) {
                        console.log(e);
                        reject(e);   
                    }
                });
            },      
            updateUserPw: (user_id,body) => {
                return new Promise(async (resolve, reject) => {   
                    try {                        
                        let platform='farm'
                        if (user_id) {
                            body.user_id=user_id;
                            const data = await ERP.modUserPw(platform, body);   
                            if(data){
                                await sequelize.models.User.update({
                                    password: body.newpassword,
                                }, {
                                    where: { user_id: user_id }
                                });  
                                resolve(true); 
                            }else{
                                reject(true); 
                            }
                        }else{
                            reject(true); 
                        }   
                    } catch (e) {
                        //console.log(e);
                        reject(e);   
                    }
                });
            },                                
            getUsers: ( {name,size,offset}) => {
                return new Promise(async (resolve, reject) => {
                    try {     
                        let query = `SELECT * FROM  ( `; 
                        query += `SELECT @ROWNUM:=@ROWNUM+1 as no, AA.* FROM  `;     
                        query += `(SELECT * FROM users  `;  
                        if (name) query += `WHERE name like '%${name}%'  `; 
                        query += `ORDER BY created_at ASC)  `;    
                        query += `AA, (SELECT @ROWNUM:=0) R  `; 
                        query += `) as BB order by BB.no DESC  `; 
                        query += `LIMIT ${parseInt(size)} OFFSET ${parseInt(offset)}  `;
   
                        const rows = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });  
                        resolve({count:rows.length, rows});            
                    } catch (e) {                                    
                        reject(e);  
                    }
                });
            },
            getUserId: ( user_id ,{house_id,type}) => {  
                return new Promise(async (resolve, reject) => {
                    try {    
                        
                        let where = {};
                        if (user_id) where.user_id = user_id;   
                        const userData = await sequelize.models.User.findOne({where});
                        
                        let query = `SELECT CC.* FROM users as AA `;
                        query += `left join user_control_rels as BB on AA.user_id = BB.user_id `;
                        query += `left join controls as CC on BB.control_id=CC.id `;
                        query += `where AA.user_id=${user_id} `;
                        if(type) query += `and BB.type='${type}' `;

                        const userControlData = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });  
                        
                        resolve({userData,userControlData});                     
                    } catch (e) {                          
                        
                        reject(e);       
                    }
                });
            },  
            deleteUser: (user_id) => {
                return new Promise(async (resolve, reject) => {
                    try {               
                        const platform = 'farm';                                               
                        const data = await ERP.delUser(platform, user_id);                        
                        if(data && data.code=='0000'){                             
                            await sequelize.models.User.destroy({
                                where: {
                                    user_id: user_id
                                }           
                            });     
                            resolve(true);      
                        }else{
                            reject(null);
                        }                          
                    } catch (e) {                         
                        reject(e);  
                    }
                });  
            },
            getUserGrade: (user_id) => {
                return new Promise(async (resolve, reject) => {
                    try {  
                        let where = {};
                        where.user_id = user_id;   
                        where.is_use = 1;
                        const userData = await sequelize.models.User.findOne({where});
                        if(userData && userData.grade) resolve(userData);
                        else resolve(null);
                    } catch (e) {                             
                        reject(e);  
                    }
                });  
            },                                                                         
        } 
    }
};
