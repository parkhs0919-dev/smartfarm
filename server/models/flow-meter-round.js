const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'flow_meter_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },     
        'start_hour': {
            type: Sequelize.TINYINT(1),
            defaultValue: null,
            allowNull: true
        },   
        'end_hour': {
            type: Sequelize.TINYINT(1),
            defaultValue: null,
            allowNull: true
        },        
    },     
    options: {
        indexes: [{
            name: 'flow_meter_id',
            fields: ['flow_meter_id']
        }],
        tableName: 'flow_meter_round',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },  
    associations: [{
        belongsTo: 'FlowMeter',
        foreignKey: 'flow_meter_id',
        targetKey: 'id',
        as: 'flowMeter'      
    }],
    methods: {  
        instanceMethods: {},
        classMethods: {
            createFlowMeterRound: ({flow_meter_id}) => {
                return new Promise(async (resolve, reject) => {  
                    try {                                             
                        const createdData = await sequelize.models.FlowMeterRound.create({
                            flow_meter_id: flow_meter_id                      
                        });   
                        resolve(createdData);
                    } catch (e) {
                        console.log(e);   
                        reject(e);   
                    }  
                });  
            },
            updateFlowMeterRound: (id,body) => {
                return new Promise(async (resolve, reject) => {
                    try {      
                        await sequelize.models.FlowMeter.update({
                            is_auto_reset : body.is_auto_reset
                        }, {    
                            where:
                                { id: id }
                        });                                         
                        if (id && body.id_list) {    
                            const id_list = body.id_list.split(',');  
                            const start_hour_list = body.start_hour_list.split(','); 
                            const end_hour_list = body.end_hour_list.split(','); 
    
                            if (id_list.length > 0) {
                                await sequelize.transaction(t => {
                                    let promises = [];  
                                    for (let i = 0; i < id_list.length; i++) {
                                        promises.push(sequelize.models.FlowMeterRound.update({
                                            start_hour : start_hour_list[i],
                                            end_hour : end_hour_list[i]
                                        }, {   
                                            where: {  
                                                id: id_list[i],
                                                flow_meter_id : id 
                                            },   
                                            transaction: t    
                                        }));
                                    }
                                    return Promise.all(promises);
                                });
                            }
                        }  
                        resolve(true);
                    } catch (e) {
                        reject(e);   
                    }  
                });
            },                
            getFlowMeterRoundDate: (body) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        
                        const table_name = `log_sensor_${body.year}${body.month < 10 ? '0' + body.month : body.month}`;
                        if (body.type == 'translate') {
                            let query = ` SELECT AA.house_id,AA.sensor_id, BB.* FROM flow_meter as AA  `;
                            query += ` left join flow_meter_round  as BB on AA.id=BB.flow_meter_id  `;
                            query += ` where AA.id="${body.id}" order by BB.id asc  `;
                            const data = await sequelize.query(query, {
                                type: Sequelize.QueryTypes.SELECT
                            });

                            if (data.length > 0) {
                                let query_select = ``;
                                for (let i = 0; i < data.length; i++) {
                                    let subData = data[i];
                                    const start_date = getDateText(parseInt(body.year), parseInt(body.month), parseInt(body.day), parseInt(subData.start_hour), 0);
                                    const end_date = getDateText(parseInt(body.year), parseInt(body.month), parseInt(body.day), parseInt(subData.end_hour), 0);
                                    query_select += ` SELECT "${subData.id}" as id, ifnull(MAX(value),0) AS max_step, ifnull(MIN(value),0) as min_step  `;
                                    query_select += ` ,"${subData.start_hour}" as start_hour , "${subData.end_hour}" as end_hour   `;
                                    query_select += ` ,(ifnull(MAX(value),0) - ifnull(MIN(value),0)) as result FROM ${table_name} `;
                                    query_select += ` where DATE_FORMAT(created_at,'%Y%m%d%H%m')>="${start_date}" `;
                                    query_select += ` and DATE_FORMAT(created_at,'%Y%m%d%H%m')<"${end_date}" `;
                                    query_select += ` and sensor_id="${subData.sensor_id}" `;
                                    if (i !== (data.length - 1)) query_select += ` UNION `;
                                }
                                const flow_meter_round = await sequelize.query(query_select, {
                                    type: Sequelize.QueryTypes.SELECT
                                });
                                resolve(flow_meter_round);

                            }
                        } else if (body.type == 'time') {
                            let query_select = ``;
                            const start_date = getDateText(parseInt(body.year), parseInt(body.month), parseInt(body.day), parseInt(body.start_hour), 0);
                            const end_date = getDateText(parseInt(body.year), parseInt(body.month), parseInt(body.day), parseInt(body.end_hour), 0);
                            query_select += ` SELECT  ifnull(MAX(value),0) AS max_step, ifnull(MIN(value),0) as min_step  `;
                            query_select += ` ,(ifnull(MAX(value),0) - ifnull(MIN(value),0)) as result FROM ${table_name} `;
                            query_select += ` where DATE_FORMAT(created_at,'%Y%m%d%H%m')>="${start_date}" `;
                            query_select += ` and DATE_FORMAT(created_at,'%Y%m%d%H%m')<"${end_date}" `;
                            query_select += ` and sensor_id=(SELECT sensor_id FROM flow_meter WHERE id=${body.id} limit 1 ) `;

                            const flow_meter_round = await sequelize.query(query_select, {
                                type: Sequelize.QueryTypes.SELECT
                            });                            
                            resolve(flow_meter_round);
                        } else {
                            resolve([]);
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getFlowMeterRoundDateHour: (body) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const table_name=`log_sensor_${body.year}${body.month < 10 ? '0' + body.month : body.month}`;

                        let query_select = ``;
                        const start_date= getDateText(parseInt(body.year),parseInt(body.month),parseInt(body.day),parseInt(body.start_hour),0);  
                        const end_date= getDateText(parseInt(body.year),parseInt(body.month),parseInt(body.day),parseInt(body.end_hour),0);  
                        query_select += ` SELECT  ifnull(MAX(value),0) AS max_step, ifnull(MIN(value),0) as min_step  `;
                        query_select += ` ,(ifnull(MAX(value),0) - ifnull(MIN(value),0)) as result FROM ${table_name} `;
                        query_select += ` where DATE_FORMAT(created_at,'%Y%m%d%H%m')>="${start_date}" `;  
                        query_select += ` and DATE_FORMAT(created_at,'%Y%m%d%H%m')<"${end_date}" `;
                        query_select += ` and sensor_id=(SELECT sensor_id FROM flow_meter WHERE id=${body.id} limit 1 ) `;
      
                        const flow_meter_round = await sequelize.query(query_select, {              
                            type: Sequelize.QueryTypes.SELECT       
                        });       
                        resolve(flow_meter_round);  
                    } catch (e) {
                        reject(e);
                    }
                });
            },                                
        }
    }
};

function getDateText(year,month,date,hour,minute){
    return  `${year}${month < 10 ? '0' + month : month}${date < 10 ? '0' + date : date}${hour < 10 ? '0' + hour : hour}${minute < 10 ? '0' + minute : minute}`;
}   