const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        }, 
        'position': {
            type: Sequelize.ENUM,
            values: ['in', 'out'],
            defaultValue: 'in',
            allowNull: false
        },                 
        'type': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },  
        'name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },         
        'value': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: true
        },
        'is_use': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },   
    },
    options: {
        indexes: [{
            name: 'is_use',
            fields: ['is_use']
        }],          
        tableName: 'sensor_avgs',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }, {
        hasMany: 'SensorAvgItem',
        foreignKey: 'sensor_avg_id',
        as: 'sensorAvgItem'       
    }],    
    methods: {
        instanceMethods: {},
        classMethods: {
            getSensorAvg: ( {house_id,position,is_use}) => {
                return new Promise(async (resolve, reject) => {
                    try {                                                      
                     
                        let query = `select AA.*,BB.value from sensor_avgs as AA `;
                        query +=` left join  `;  
                        query +=` (SELECT sensor_avgs.id ,sensor_value.value from sensor_avgs as sensor_avgs `; 
                        query +=` left join ( SELECT sensor_avg_id as id ,ROUND(avg(BB.value),1) as value  FROM sensor_avg_items as AA `; 
                        query +=` left join sensors as BB  on AA.sensor_id=BB.id group by sensor_avg_id `; 
                        query +=` ) as sensor_value `;    
                        query +=` on sensor_avgs.id=sensor_value.id  `;   
                        query +=` ) as BB `; 
                        query +=` on AA.id=BB.id `;        
                        query += `where AA.house_id=${house_id} and AA.position='${position}'`;
                        if(is_use) query +=` and AA.is_use='${is_use}' `; 
                        
                        const rows = await sequelize.query(query, {            
                            type: Sequelize.QueryTypes.SELECT       
                        });  
                        
                        resolve(rows);      
           
                    } catch (e) {   
                        
                        reject(e);  
                    }
                });
            },              
            getSensorAvgId: (id ,{type,position}) => {
                return new Promise(async (resolve, reject) => {
                    try {      
                        let where = {};  
                        where.id=id;
                        const sensor_avgs = await sequelize.models.SensorAvg.findOne({
                            where  
                        });    
                        
                        let query = ` select sensors.*,sensor_avg_items.sensor_id as avg_sensor_id from sensors as sensors `;
                        query +=` left outer join  `;  
                        query +=` (SELECT BB.sensor_id  FROM sensor_avgs as AA   `; 
                        query +=` left join sensor_avg_items as BB  `; 
                        query +=` on AA.id=BB.sensor_avg_id  `; 
                        query +=` WHERE AA.id = ${id} `;    
                        query +=` ) as sensor_avg_items  on sensors.id=sensor_avg_items.sensor_id `;   
                        query +=` where sensors.sensor_id is null `; 
                        query +=` and sensors.type='${type}' and sensors.position='${position}'`;           
                        
                        const sensor_avg_items = await sequelize.query(query, {            
                            type: Sequelize.QueryTypes.SELECT       
                        });                             
                         resolve({sensor_avgs,sensor_avg_items});      
           
                    } catch (e) {
                        reject(e);  
                    }
                });
            },            
            createSensorAvg: ({house_id,type,name,items,position}) => {
                return new Promise(async (resolve, reject) => {
                    try {                                             
                        const createdData = await sequelize.models.SensorAvg.create({
                            house_id:house_id,
                            type: type,      
                            name: name,
                            position:position                          
                        });
                        const list=items.split(',');  
                       
                        if(createdData && list.length>0){                             
                            await sequelize.transaction(t => {
                                let promises = [];
                                for (let i=0; i<list.length; i++) {
                                    promises.push(sequelize.models.SensorAvgItem.create({
                                        sensor_avg_id: createdData.id,
                                        sensor_id: list[i],
                                    }, {
                                        transaction: t                                        
                                    }));   
                                }   
                                return Promise.all(promises);
                              });  
                        }   
                       
                        resolve(createdData);
                    } catch (e) {
                        console.log(e);
                        reject(e);   
                    }
                });
            }, 
            updateSensorAvg: (sensor_avg_id,body) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const updateData = await sequelize.models.SensorAvg.update({
                            ...body
                        }, {
                            where:
                                { id: sensor_avg_id }
                        });
                        if (body.items && body.items.length > 1) {
                            const list = body.items.split(',');

                            const result = await sequelize.models.SensorAvgItem.destroy({
                                where: {
                                    sensor_avg_id: sensor_avg_id
                                }
                            });

                            if (updateData && result && list.length > 0) {
                                await sequelize.transaction(t => {
                                    let promises = [];
                                    for (let i = 0; i < list.length; i++) {
                                        promises.push(sequelize.models.SensorAvgItem.create({
                                            sensor_avg_id: sensor_avg_id,
                                            sensor_id: list[i],
                                        }, {
                                            transaction: t
                                        }));
                                    }
                                    return Promise.all(promises);
                                });
                            }
                        }
                        resolve(updateData);
                    } catch (e) {
                        console.log(e);
                        reject(e);   
                    }
                });
            }                                    
        }
    }
};  
