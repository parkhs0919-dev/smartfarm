const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'table_name': {
            type: Sequelize.STRING(getDBStringLength()),
            unique: true,
            allowNull: false
        },
        'is_send': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'created_at': {
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }
    },
    options: {
        indexes: [{
            name: 'is_send',
            fields: ['is_send']
        }],
        tableName: 'logs',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
