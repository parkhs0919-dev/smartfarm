const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'species': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'animal_type_name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        }    
    },
    options: {
        tableName: 'animal_types',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [],
    methods: {
        instanceMethods: {},   
        classMethods: {
          createAnimalType: ({animal_type_name}) => {
              return new Promise(async (resolve, reject) => {
                  try {
                      const createdData = await sequelize.models.AnimalType.create({
                          animal_type_name:animal_type_name,
                      });
                      resolve(createdData);
                  } catch (e) {
                    console.log(e);
                      reject(e);
                  }
              });
          },
          getAnimalType: ({offset, size}) => { 
              return new Promise(async (resolve, reject) => {
                  try {
                      let where = {};

                      const count = await sequelize.models.AnimalType.count({where});
                      const rows = await sequelize.models.AnimalType.findAll({
                          order: [['id', 'ASC']],
                          where,
                          limit: size ? parseInt(size) : undefined,
                          offset: parseInt(offset)
                      });
                      resolve({count, rows});
                  } catch (e) {
                    console.log(e);  
                      reject(e);
                  }
              });
          }

        }
    }
};
