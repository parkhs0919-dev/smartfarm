const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['motor', 'power'],
            defaultValue: 'motor',
            allowNull: false
        },        
        'voice_code': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        }, 
        'action': {
            type: Sequelize.ENUM,
            values: ['open', 'stop', 'close', 'on', 'off'],
            defaultValue: 'stop',
            allowNull: false
        }, 
        'address': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },         
        'io1': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        }, 
        'io2': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },                         
    }, 
    options: {
        tableName: 'voice_control',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [],
    methods: {
        instanceMethods: {},
        classMethods: {
            getVoiceControl: ({code}) => {
                return new Promise(async (resolve, reject) => {
                    try {    
                        let query = ` SELECT * FROM  voice_control `; 
                        query += ` where voice_code ='${code}' `;
                        query += ` limit 1 `;     
                        const rows = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });  
                        resolve(rows[0]);            
                    } catch (e) {                                     
                        reject(e);  
                    }
                });
            },           
        }
    }
};
