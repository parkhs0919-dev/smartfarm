const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'auto_control_step_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'control_min_max_range_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['on', 'off'],
            defaultValue: 'on',
            allowNull: false
        },
        'percentage': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        }
    },
    options: {
        tableName: 'auto_control_step_min_maxes',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'AutoControlStep',
        foreignKey: 'auto_control_step_id',
        targetKey: 'id',
        as: 'autoControlStep'
    }, {
        belongsTo: 'ControlMinMaxRange',
        foreignKey: 'control_min_max_range_id',
        targetKey: 'id',
        as: 'controlMinMaxRange'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
