const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const CONFIG = require('../config');

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'user_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'token': {
            type: Sequelize.TEXT('long'),
            allowNull: false
        },
        'iat': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'exp': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'created_at': {
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            allowNull: false
        }
    },
    options: {
        indexes: [{
            name: 'token',
            fields: [{
                attribute: 'token',
                length: getDBStringLength()
            }]
        }],
        tableName: 'login_histories',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    methods: {
        instanceMethods: {},
        classMethods: {
            upsertLoginHistory: (payload, token) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const loginHistories = await sequelize.models.LoginHistory.findAll({
                            order: [['created_at', 'ASC']],
                            where: {
                                user_id: payload.id
                            }
                        });
                        if (loginHistories.length >= CONFIG.jwt.maxCount) {
                            for (let i=0; i<loginHistories.length - CONFIG.jwt.maxCount + 1; i++) {
                                await sequelize.models.LoginHistory.destroy({
                                    where: {
                                        id: loginHistories[i].id
                                    }
                                });
                            }
                        }
                        await sequelize.models.LoginHistory.create({
                            user_id: payload.id,
                            iat: payload.iat,
                            exp: payload.exp,
                            token
                        });
                        resolve(true);
                    } catch(e) {
                        reject(e);
                    }
                });
            },
            getLoginHistory: (payload, token) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const loginHistory = await sequelize.models.LoginHistory.findOne({
                            where: {
                                user_id: payload.id,
                                token
                            }
                        });
                        resolve(loginHistory);
                    } catch(e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
