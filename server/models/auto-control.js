const deepcopy = require('deepcopy');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const FILTER_UTIL = require('../utils/filter');

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'user_id': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['time', 'sensor', 'control', 'mix', 'step', 'table'],
            defaultValue: 'time',
            allowNull: false
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['on', 'off'],
            defaultValue: 'on',
            allowNull: false
        },
        'auto_control_name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'date_type': {
            type: Sequelize.ENUM,
            values: ['day', 'per'],
            defaultValue: 'day',
            allowNull: false
        },
        'is_sun': {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        'is_mon': {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        'is_tue': {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        'is_wed': {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        'is_thur': {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        'is_fri': {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        'is_sat': {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
            allowNull: false
        },
        'start_date': {
            type: Sequelize.DATEONLY,
            allowNull: true
        },
        'end_date': {
            type: Sequelize.DATEONLY,
            allowNull: true
        },
        'per_date': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'step_delay': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'current_step': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'order': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        },
    },
    options: {
        indexes: [{
            name: 'unique_key',
            fields: ['house_id', 'auto_control_name']
        }, {
            name: 'state',
            fields: ['state']
        }, {
            name: 'date_type',
            fields: ['date_type']
        }, {
            name: 'start_date',
            fields: ['start_date']
        }, {
            name: 'end_date',
            fields: ['end_date']
        }, {
            name: 'order',
            fields: ['order']
        }],
        tableName: 'auto_controls',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }, {
        hasMany: 'AutoControlItem',
        foreignKey: 'auto_control_id',
        as: 'autoControlItems'
    }, {
        hasMany: 'AutoControlStep',
        foreignKey: 'auto_control_id',
        as: 'autoControlSteps'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            deleteAutoControl: id => {
                return new Promise(async (resolve, reject) => {
                    let transaction;
                    try {
                        transaction = await sequelize.transaction();

                        const query = `DELETE auto_control_sensor_options FROM auto_control_sensor_options
                        INNER JOIN auto_control_sensors ON auto_control_sensor_options.order = auto_control_sensors.order
                        INNER JOIN auto_control_items ON auto_control_sensors.auto_control_item_id = auto_control_items.id
                        AND auto_control_items.auto_control_id = ${id}`;

                        await sequelize.query(query, {
                            type: Sequelize.QueryTypes.DELETE,
                            transaction
                        });

                        await sequelize.models.AutoControl.destroy({
                            where: {id},
                            cascade: true,
                            transaction
                        });

                        await transaction.commit();
                        resolve(true);
                    } catch (e) {
                        transaction && await transaction.rollback();
                        reject(e);
                    }
                });
            },
            getAutoControlWindDirectionCount: house_id => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const query = `SELECT
                        ((SELECT COUNT(*) FROM auto_control_items INNER JOIN auto_controls ON auto_control_items.auto_control_id = auto_controls.id WHERE auto_control_items.wind_direction_type = 'forward' AND auto_controls.state = 'on') +
                        (SELECT COUNT(*) FROM auto_control_steps INNER JOIN auto_controls ON auto_control_steps.auto_control_id = auto_controls.id WHERE auto_control_steps.wind_direction_type = 'forward' AND auto_controls.state = 'on')) AS windDirectionForwardCount,
                        ((SELECT COUNT(*) FROM auto_control_items INNER JOIN auto_controls ON auto_control_items.auto_control_id = auto_controls.id WHERE auto_control_items.wind_direction_type = 'backward' AND auto_controls.state = 'on') +
                        (SELECT COUNT(*) FROM auto_control_steps INNER JOIN auto_controls ON auto_control_steps.auto_control_id = auto_controls.id WHERE auto_control_steps.wind_direction_type = 'backward' AND auto_controls.state = 'on')) AS windDirectionBackwardCount`;

                        const result = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        if (result.length) {
                            resolve(result[0]);
                        } else {
                            reject(false);
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            createAutoControl: (body) => {
                return new Promise(async (resolve, reject) => {
                    let transaction;
                    try {
                        const autoControlBody = generateCreateBody(body);

                        transaction = await sequelize.transaction();
                        const createdData = await sequelize.models.AutoControl.create(autoControlBody, {
                            include: [{
                                model: sequelize.models.AutoControlItem,
                                as: 'autoControlItems',
                                include: [{
                                    model: sequelize.models.Control,
                                    as: 'control'
                                }, {
                                    model: sequelize.models.AutoControlSensor,
                                    as: 'autoControlSensors',
                                    include: [{
                                        model: sequelize.models.Sensor,
                                        as: 'sensor'
                                    }, {
                                        model: sequelize.models.AutoControlSensorOption,
                                        as: 'autoControlSensorOptions',
                                        include : [{
                                            model : sequelize.models.Sensor,
                                            as : 'sensor'
                                        }]
                                    }]
                                }, {
                                    model: sequelize.models.AutoControlControl,
                                    as: 'autoControlControls',
                                    include: [{
                                        model: sequelize.models.Control,
                                        as: 'control'
                                    }]
                                }, {
                                    model: sequelize.models.AutoControlItemMinMax,
                                    as: 'autoControlItemMinMaxes',
                                    include: [{
                                        model: sequelize.models.ControlMinMaxRange,
                                        as: 'controlMinMaxRange'
                                    }]
                                }, {
                                    model: sequelize.models.PBand,
                                    as: 'pBand'
                                }, {
                                    model: sequelize.models.AutoControlExpectTemperatureSensor,
                                    as : 'autoControlExpectTemperatureSensors',
                                    include : [{
                                        model : sequelize.models.Sensor,
                                        as: 'sensor'
                                    }]
                                }]
                            }, {
                                model: sequelize.models.AutoControlStep,
                                as: 'autoControlSteps',
                                include: [{
                                    model: sequelize.models.Control,
                                    as: 'control'
                                }, {
                                    model: sequelize.models.AutoControlStepMinMax,
                                    as: 'autoControlStepMinMaxes',
                                    include: [{
                                        model: sequelize.models.ControlMinMaxRange,
                                        as: 'controlMinMaxRange'
                                    }]
                                }, {
                                    model: sequelize.models.PBand,
                                    as: 'pBand'
                                }]
                            }],
                            transaction
                        });

                        const autoControlControlBody = generateCreateControlBody(createdData.autoControlItems, body);
                        if (autoControlControlBody.length) {
                            await sequelize.models.AutoControlControl.bulkCreate(autoControlControlBody, {
                                transaction
                            });
                        }

                        const autoControlSensorBody = generateCreateSensorBody(createdData.autoControlItems, body);
                        if (autoControlSensorBody.length) {
                            let promises = [];

                            autoControlSensorBody.forEach((data) => {
                                promises.push(sequelize.models.AutoControlSensor.create(data,{
                                    transaction
                                }).then(autoControlSensor => {
                                    if(data.autoControlSensorOptions){
                                        for (let i=0; i<data.autoControlSensorOptions.length; i++) {
                                            data.autoControlSensorOptions[i].order = autoControlSensor.order;
                                        }
                                        return sequelize.models.AutoControlSensorOption.bulkCreate(data.autoControlSensorOptions, {
                                            transaction
                                        });
                                    }
                                }))
                            });

                            await Promise.all(promises);

                            // await sequelize.models.AutoControlSensor.bulkCreate(autoCoƒntrolSensorBody, {
                            //     include: [{
                            //         model: sequelize.models.AutoControlSensorOption,
                            //         as: 'autoControlSensorOptions'
                            //     }],
                            //     transaction
                            // });
                        }

                        const autoControlExpectTemperatureSensorBody = generateCreateExpectTemperatureSensorBody(createdData.autoControlItems, body);
                        if(autoControlExpectTemperatureSensorBody.length){
                            await sequelize.models.AutoControlExpectTemperatureSensor.bulkCreate(autoControlExpectTemperatureSensorBody, {
                                transaction
                            });
                        }

                        const autoControlItemMinMaxBody = generateCreateItemMinMaxBody(createdData.autoControlItems, body);
                        if (autoControlItemMinMaxBody.length) {
                            await sequelize.models.AutoControlItemMinMax.bulkCreate(autoControlItemMinMaxBody, {
                                transaction
                            });
                        }

                        const autoControlStepMinMaxBody = generateCreateStepMinMaxBody(createdData.autoControlSteps, body);
                        if (autoControlStepMinMaxBody.length) {
                            await sequelize.models.AutoControlStepMinMax.bulkCreate(autoControlStepMinMaxBody, {
                                transaction
                            });
                        }

                        const query = `SELECT MAX(auto_controls.order) AS maxOrder FROM auto_controls WHERE auto_controls.house_id = ${body.house_id}`;
                        const data = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT,
                            transaction
                        });
                        if (data.length && data[0].maxOrder !== undefined) {
                            await sequelize.models.AutoControl.update({
                                order: data[0].maxOrder + 1
                            }, {
                                where: {
                                    id: createdData.id
                                },
                                transaction
                            });
                        } else {
                            await transaction.rollback();
                            return reject(false);
                        }
                        await transaction.commit();
                        resolve(createdData);
                    } catch (e) {
                        transaction && await transaction.rollback();
                        reject(e);
                    }
                });
            },
            updateAutoControl: (body, id) => {
                return new Promise(async (resolve, reject) => {
                    let transaction;
                    try {
                        const updateBody = generateUpdateBody(body);
                        const autoControlItemBody = generateCreateItemBody(body, id);
                        const autoControlStepBody = generateCreateStepBody(body, id);

                        transaction = await sequelize.transaction();

                        await sequelize.models.AutoControl.update({
                            active_time: null,
                            current_count: null,
                            current_step: null,
                            ...updateBody
                        }, {
                            where: {id},
                            transaction
                        });
                        await sequelize.models.AutoControlItem.destroy({
                            where: {
                                auto_control_id: id
                            },
                            cascade: true,
                            transaction
                        });
                        await sequelize.models.AutoControlItem.bulkCreate(autoControlItemBody, {
                            transaction
                        });
                        const autoControlItems = await sequelize.models.AutoControlItem.findAll({
                            where: {
                                auto_control_id: id
                            },
                            transaction
                        });
                        const autoControlSensorBody = generateCreateSensorBody(autoControlItems, body);
                        if (autoControlSensorBody.length) {
                            let promises = [];

                            autoControlSensorBody.forEach((data) => {
                                promises.push(sequelize.models.AutoControlSensor.create(data,{
                                    transaction
                                }).then(autoControlSensor => {
                                    if(data.autoControlSensorOptions){
                                        for (let i=0; i<data.autoControlSensorOptions.length; i++) {
                                            data.autoControlSensorOptions[i].order = autoControlSensor.order;
                                        }
                                        return sequelize.models.AutoControlSensorOption.bulkCreate(data.autoControlSensorOptions, {
                                            transaction
                                        });
                                    }
                                }))
                            });


                            await Promise.all(promises);
                            // await sequelize.models.AutoControlSensor.bulkCreate(autoControlSensorBody, {
                            //     include: [{
                            //         model: sequelize.models.AutoControlSensorOption,
                            //         as: 'autoControlSensorOptions'
                            //     }],
                            //     transaction
                            // });
                        }
                        const autoControlControlBody = generateCreateControlBody(autoControlItems, body);
                        if (autoControlControlBody.length) {
                            await sequelize.models.AutoControlControl.bulkCreate(autoControlControlBody, {
                                transaction
                            });
                        }

                        const autoControlExpectTemperatureSensorBody = generateCreateExpectTemperatureSensorBody(autoControlItems, body);
                        if(autoControlExpectTemperatureSensorBody.length){
                            await sequelize.models.AutoControlExpectTemperatureSensor.bulkCreate(autoControlExpectTemperatureSensorBody, {
                                transaction
                            });
                        }

                        const autoControlItemMinMaxBody = generateCreateItemMinMaxBody(autoControlItems, body);
                        if (autoControlItemMinMaxBody.length) {
                            await sequelize.models.AutoControlItemMinMax.bulkCreate(autoControlItemMinMaxBody, {
                                transaction
                            });
                        }
                        await sequelize.models.AutoControlStep.destroy({
                            where: {
                                auto_control_id: id
                            },
                            cascade: true,
                            transaction
                        });
                        if (autoControlStepBody && autoControlStepBody.length) {
                            await sequelize.models.AutoControlStep.bulkCreate(autoControlStepBody, {
                                transaction
                            });
                            const autoControlSteps = await sequelize.models.AutoControlStep.findAll({
                                where: {
                                    auto_control_id: id
                                },
                                transaction
                            });
                            const autoControlStepMinMaxBody = generateCreateStepMinMaxBody(autoControlSteps, body);
                            if (autoControlStepMinMaxBody.length) {
                                await sequelize.models.AutoControlStepMinMax.bulkCreate(autoControlStepMinMaxBody, {
                                    transaction
                                });
                            }
                        }

                        await transaction.commit();
                        resolve(true);
                    } catch (e) {
                        transaction && await transaction.rollback();
                        reject(e);
                    }
                });
            },
            updateAutoControlDelayRecount: ({delay = null, recount = null}, id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        await sequelize.models.AutoControl.update({
                            delay,
                            recount
                        }, {
                            where: {id}
                        });
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            updateAutoControlOrders: ({house_id, ids}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const autoControls = await sequelize.models.AutoControl.findAll({
                            order: [['order', 'ASC']],
                            where: {
                                house_id,
                                id: ids
                            }
                        });
                        if (autoControls.length === ids.length) {
                            await sequelize.transaction(t => {
                                let promises = [];
                                for (let i=0; i<autoControls.length; i++) {
                                    promises.push(sequelize.models.AutoControl.update({
                                        order: autoControls[i].order
                                    }, {
                                        where: {id: ids[i]},
                                        transaction: t
                                    }));
                                }
                                return Promise.all(promises);
                            });
                            resolve(true);
                        } else {
                            reject(false);
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            updateAutoControlOrder: (id, house_id, order) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const target = await sequelize.models.AutoControl.findByPk(id);
                        const targetOrder = target.order;
                        const autoControls = await sequelize.models.AutoControl.findAll({
                            order: [['order', 'ASC']],
                            where: {house_id}
                        });
                        let targets = [];

                        if (targetOrder < order) {
                            for (let i=0; i<autoControls.length; i++) {
                                if (autoControls[i].order !== targetOrder) {
                                    targets.push(autoControls[i]);
                                }
                                if (autoControls[i].order === order) {
                                    targets.push(target);
                                }
                            }
                        } else {
                            for (let i=0; i<autoControls.length; i++) {
                                if (autoControls[i].order === order) {
                                    targets.push(target);
                                }
                                if (autoControls[i].order !== targetOrder) {
                                    targets.push(autoControls[i]);
                                }
                            }
                        }

                        await sequelize.transaction(t => {
                            let promises = [];
                            targets.forEach(({id}, order) => {
                                promises.push(sequelize.models.AutoControl.update({
                                    order
                                }, {
                                    where: {id},
                                    transaction: t
                                }));
                            });
                            return Promise.all(promises);
                        });
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            updateAutoControlState: (state, id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const query = `UPDATE auto_controls
                        LEFT JOIN auto_control_steps ON auto_control_steps.auto_control_id = auto_controls.id
                        LEFT JOIN auto_control_items ON auto_control_items.auto_control_id = auto_controls.id
                        SET auto_controls.state = '${state}'
                        , auto_control_steps.active_time = NULL
                        , auto_control_steps.current_count = NULL
                        , auto_control_items.active_time = NULL
                        , auto_control_items.current_count = NULL
                        , auto_control_items.current_active_count = 0
                        WHERE auto_controls.id = ${id}`;
                        await sequelize.query(query, {
                            type: Sequelize.QueryTypes.UPDATE
                        });
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            initAutoControls: (house_id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const query = `UPDATE auto_controls
                        LEFT JOIN auto_control_steps ON auto_control_steps.auto_control_id = auto_controls.id
                        LEFT JOIN auto_control_items ON auto_control_items.auto_control_id = auto_controls.id
                        SET auto_control_steps.active_time = NULL
                        , auto_control_steps.current_count = NULL
                        , auto_control_items.active_time = NULL
                        , auto_control_items.current_count = NULL
                        , auto_control_items.current_active_count = 0
                        WHERE auto_controls.house_id = ${house_id}`;
                        await sequelize.query(query, {
                            type: Sequelize.QueryTypes.UPDATE
                        });
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getAutoControls: (query) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let now = new Date();
                        let today = FILTER_UTIL.date(now, 'yyyy-MM-dd'), getDay = null;
                        let whereQuery = 'WHERE auto_controls.id > 0';

                        if (query.wind_direction_type) {
                            whereQuery += ` AND (auto_control_items.wind_direction_type = '${query.wind_direction_type}' OR auto_control_steps.wind_direction_type = '${query.wind_direction_type}')`
                        }
                        if(query.window_position) {
                            whereQuery += ` AND (auto_control_items.window_position = '${query.window_position}' OR auto_control_steps.window_position = '${query.window_position}')`;
                        }
                        if(query.type){
                            whereQuery += ` AND (item_controls.type = '${query.type}' OR step_controls.type = '${query.type}')`;
                        }

                        if (query.house_id) {
                            whereQuery += ` AND auto_controls.house_id = ${query.house_id}`;
                        }
                        if (query.control_id) {
                            whereQuery += ` AND (auto_control_items.control_id = ${query.control_id} OR auto_control_steps.control_id = ${query.control_id})`;
                        }

                        if (query.state) {
                            whereQuery += ` AND auto_controls.state = '${query.state}'`;
                        }

                        if (query.auto_control_type){
                            whereQuery += ` AND auto_controls.type = '${query.auto_control_type}'`;
                        }

                        if(query.is_temperature_control_option){
                            whereQuery += ` AND (auto_control_items.is_temperature_control_option = ${query.is_temperature_control_option} OR auto_control_steps.is_temperature_control_option = ${query.is_temperature_control_option})`;
                        }

                        if(query.temperature_type){
                            if(query.temperature_type === 'vantilation'){
                                whereQuery += ` AND ((auto_control_items.control_id IS NOT NULL AND item_controls.type = 'motor') OR (auto_control_items.window_position != 'none' AND auto_control_items.wind_direction_type != 'none' ))`;
                            }else if(query.temperature_type === 'boiler'){
                                whereQuery += ` AND auto_control_items.control_id IS NOT NULL AND item_controls.type = 'power'`;
                            }
                        }

                        if (query.isActiveDate) {
                            switch(now.getDay()) {
                                case 0:
                                    getDay = 'is_sun';
                                    break;
                                case 1:
                                    getDay = 'is_mon';
                                    break;
                                case 2:
                                    getDay = 'is_tue';
                                    break;
                                case 3:
                                    getDay = 'is_wed';
                                    break;
                                case 4:
                                    getDay = 'is_thur';
                                    break;
                                case 5:
                                    getDay = 'is_fri';
                                    break;
                                case 6:
                                    getDay = 'is_sat';
                                    break;
                            }
                            whereQuery += ` AND (auto_controls.start_date IS NULL OR auto_controls.start_date <= '${today}')`;
                            whereQuery += ` AND (auto_controls.end_date IS NULL OR auto_controls.end_date >= '${today}')`;
                            whereQuery += ` AND ((auto_controls.date_type = 'day' AND auto_controls.${getDay} IS TRUE) OR auto_controls.date_type = 'per')`;
                        }

                        const idQuery = `SELECT a.id FROM (
                        SELECT auto_controls.id FROM auto_controls
                        LEFT JOIN auto_control_items ON auto_control_items.auto_control_id = auto_controls.id
                        LEFT JOIN controls AS item_controls ON auto_control_items.control_id = item_controls.id
                        LEFT JOIN auto_control_steps ON auto_control_steps.auto_control_id = auto_controls.id
                        LEFT JOIN controls AS step_controls ON auto_control_steps.control_id = step_controls.id 
                        ${whereQuery}
                        GROUP BY auto_controls.id) a`;

                        const results = await sequelize.query(idQuery, {
                            type: Sequelize.QueryTypes.SELECT
                        });

                        let ids = [];
                        results.forEach(({id}) => {
                            ids.push(id);
                        });

                        if (ids.length) {
                            const rows = await sequelize.models.AutoControl.findAll({
                                include: [{
                                    model: sequelize.models.AutoControlItem,
                                    as: 'autoControlItems',
                                    separate: true,
                                    order: [['id', 'ASC']],
                                    include: [{
                                        model: sequelize.models.Control,
                                        as: 'control'
                                    }, {
                                        model: sequelize.models.AutoControlSensor,
                                        as: 'autoControlSensors',
                                        separate: true,
                                        order: [['order', 'ASC']],
                                        include: [{
                                            model: sequelize.models.Sensor,
                                            as: 'sensor'
                                        }, {
                                            model: sequelize.models.AutoControlSensorOption,
                                            as: 'autoControlSensorOptions',
                                            on: {
                                                order: Sequelize.where(Sequelize.col('AutoControlSensor.order'), '=', Sequelize.col('autoControlSensorOptions.order'))
                                            },
                                            include : [{
                                                model : sequelize.models.Sensor,
                                                as :'sensor'
                                            }]
                                        }]
                                    }, {
                                        model: sequelize.models.AutoControlControl,
                                        as: 'autoControlControls',
                                        separate: true,
                                        order: [['order', 'ASC']],
                                        include: [{
                                            model: sequelize.models.Control,
                                            as: 'control'
                                        }]
                                    }, {
                                        model: sequelize.models.AutoControlItemMinMax,
                                        as: 'autoControlItemMinMaxes',
                                        separate: true,
                                        order: [['id', 'ASC']],
                                        include: [{
                                            model: sequelize.models.ControlMinMaxRange,
                                            as: 'controlMinMaxRange'
                                        }]
                                    }, {
                                        model: sequelize.models.AutoControlExpectTemperatureSensor,
                                        as: 'autoControlExpectTemperatureSensors',
                                        separate: true,
                                        order: [['id', 'ASC']],
                                        include: [{
                                            model: sequelize.models.Sensor,
                                            as: 'sensor'
                                        }]
                                    }, {
                                        model: sequelize.models.PBand,
                                        as: 'pBand'
                                    }, {
                                        model: sequelize.models.Sensor,
                                        as: 'expectTemperatureSensor'
                                    }]
                                }, {
                                    model: sequelize.models.AutoControlStep,
                                    as: 'autoControlSteps',
                                    separate: true,
                                    order: [['id', 'ASC']],
                                    include: [{
                                        model: sequelize.models.Control,
                                        as: 'control'
                                    }, {
                                        model: sequelize.models.AutoControlStepMinMax,
                                        as: 'autoControlStepMinMaxes',
                                        separate: true,
                                        order: [['id', 'ASC']],
                                        include: [{
                                            model: sequelize.models.ControlMinMaxRange,
                                            as: 'controlMinMaxRange'
                                        }]
                                    }, {
                                        model: sequelize.models.PBand,
                                        as: 'pBand'
                                    }, {
                                        model: sequelize.models.Sensor,
                                        as: 'expectTemperatureSensor'
                                    }]
                                }],
                                order: [['house_id', 'ASC'], ['order', 'ASC']],
                                where: {
                                    id: ids
                                }
                            });

                            if (query.isActiveDate) {
                                const todayZeroTimestamp = new Date(`${today} 00:00:00`).getTime();
                                for (let i=0; i<rows.length; i++) {
                                    const autoControl = rows[i];
                                    if (autoControl.date_type === 'per') {
                                        const startDateTimestamp = new Date(`${autoControl.start_date} 00:00:00`).getTime();
                                        if ((todayZeroTimestamp - startDateTimestamp) % (autoControl.per_date * 86400000)) {
                                            rows.splice(i, 1);
                                            i--;
                                        }
                                    }
                                }
                            }

                            resolve({
                                count: rows.length,
                                rows
                            });
                        } else {
                            resolve({
                                count: 0,
                                rows: []
                            });
                        }
                    } catch (e) {
                        console.log(e);
                        reject(e);
                    }
                });
            }
        }
    }
};

function generateCreateBody (data) {
    let body = deepcopy(data);
    for (let i=0; i<body.autoControlItems.length; i++) {
        delete body.autoControlItems[i].autoControlSensors;
        delete body.autoControlItems[i].autoControlControls;
        delete body.autoControlItems[i].autoControlItemMinMaxes;
        delete body.autoControlItems[i].autoControlExpectTemperatureSensors;
    }
    if (body.autoControlSteps) {
        for (let i=0; i<body.autoControlSteps.length; i++) {
            delete body.autoControlSteps[i].autoControlStepMinMaxes;
        }
    }
    return body;
}

function generateCreateSensorBody (autoControlItems, data) {
    const temp = deepcopy(data);
    let body = [];
    for (let i=0; i<temp.autoControlItems.length; i++) {
        if (temp.autoControlItems[i].autoControlSensors) {
            const auto_control_item_id = autoControlItems[i].id;
            for (let j=0; j<temp.autoControlItems[i].autoControlSensors.length; j++) {
                delete temp.autoControlItems[i].autoControlSensors[j].id;
                body.push({
                    auto_control_item_id,
                    ...temp.autoControlItems[i].autoControlSensors[j]
                });
            }
        }
    }
    return body;
}

function generateCreateControlBody (autoControlItems, data) {
    const temp = deepcopy(data);
    let body = [];
    for (let i=0; i<temp.autoControlItems.length; i++) {
        if (temp.autoControlItems[i].autoControlControls) {
            const auto_control_item_id = autoControlItems[i].id;
            for (let j=0; j<temp.autoControlItems[i].autoControlControls.length; j++) {
                delete temp.autoControlItems[i].autoControlControls[j].id;
                body.push({
                    auto_control_item_id,
                    ...temp.autoControlItems[i].autoControlControls[j]
                });
            }
        }
    }
    return body;
}

function generateCreateItemMinMaxBody (autoControlItems, data) {
    const temp = deepcopy(data);
    let body = [];
    for (let i=0; i<temp.autoControlItems.length; i++) {
        if (temp.autoControlItems[i].autoControlItemMinMaxes) {
            const auto_control_item_id = autoControlItems[i].id;
            for (let j=0; j<temp.autoControlItems[i].autoControlItemMinMaxes.length; j++) {
                delete temp.autoControlItems[i].autoControlItemMinMaxes[j].id;
                body.push({
                    auto_control_item_id,
                    ...temp.autoControlItems[i].autoControlItemMinMaxes[j]
                });
            }
        }
    }
    return body;
}

function generateCreateExpectTemperatureSensorBody (autoControlItems, data){
    const temp = deepcopy(data);
    let body = [];
    for (let i=0; i<temp.autoControlItems.length; i++){
        if(temp.autoControlItems[i].autoControlExpectTemperatureSensors) {
            const auto_control_item_id = autoControlItems[i].id;
            for(let j=0; j<temp.autoControlItems[i].autoControlExpectTemperatureSensors.length; j++){
                delete temp.autoControlItems[i].autoControlExpectTemperatureSensors[j].id;
                body.push({
                    auto_control_item_id,
                    ...temp.autoControlItems[i].autoControlExpectTemperatureSensors[j]
                });
            }
        }
    }
    return body;
}

function generateCreateStepMinMaxBody (autoControlSteps, data) {
    const temp = deepcopy(data);
    let body = [];
    if (temp.autoControlSteps) {
        for (let i=0; i<temp.autoControlSteps.length; i++) {
            if (temp.autoControlSteps[i].autoControlStepMinMaxes) {
                const auto_control_step_id = autoControlSteps[i].id;
                for (let j=0; j<temp.autoControlSteps[i].autoControlStepMinMaxes.length; j++) {
                    delete temp.autoControlSteps[i].autoControlStepMinMaxes[j].id;
                    body.push({
                        auto_control_step_id,
                        ...temp.autoControlSteps[i].autoControlStepMinMaxes[j]
                    });
                }
            }
        }
    }
    return body;
}

function generateUpdateBody(data) {
    let body = deepcopy(data);
    delete body.autoControlItems;
    delete body.autoControlSteps;
    return body;
}

function generateCreateItemBody(data, id) {
    let body = deepcopy(data);
    for (let i=0; i<body.autoControlItems.length; i++) {
        delete body.autoControlItems[i].id;
        delete body.autoControlItems[i].autoControlSensors;
        delete body.autoControlItems[i].autoControlControls;
        delete body.autoControlItems[i].autoControlItemMinMaxes;
        delete body.autoControlItems[i].autoControlExpectTemperatureSensors;
        body.autoControlItems[i].auto_control_id = id;
    }
    return body.autoControlItems;
}

function generateCreateStepBody(data, id) {
    let body = deepcopy(data);
    if (!body.autoControlSteps) {
        body.autoControlSteps = [];
    }
    for (let i=0; i<body.autoControlSteps.length; i++) {
        delete body.autoControlSteps[i].id;
        delete body.autoControlSteps[i].autoControlStepMinMaxes;
        body.autoControlSteps[i].auto_control_id = id;
    }
    return body.autoControlSteps;
}
