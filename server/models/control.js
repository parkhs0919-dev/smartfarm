const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const async = require('async');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const STD = require('../metadata/standards');
const FILTER_UTIL = require('../utils').filter;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'control_name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['motor', 'power', 'control'],
            defaultValue: 'motor',
            allowNull: false
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['open', 'stop', 'close', 'on', 'off'],
            defaultValue: 'stop',
            allowNull: false
        },
        'mode': {
            type: Sequelize.ENUM,
            values: ['manual', 'auto'],
            defaultValue: 'manual',
            allowNull: false
        },
        'direction': {
            type: Sequelize.ENUM,
            values: ['none', 'left', 'right'],
            defaultValue: 'none',
            allowNull: false
        },
        'current': {
            type: Sequelize.DOUBLE(8, 2),
            defaultValue: 0,
            allowNull: false
        },
        'range': {
            type: Sequelize.DOUBLE(8, 2),
            defaultValue: 0,
            allowNull: false
        },
        'button_name_1': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'button_name_2': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'button_name_3': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'address': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'io1': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'io2': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'io3': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'is_topeng': {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        'order': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            defaultValue: 0,
            allowNull: false
        }
    },
    options: {
        indexes: [{
            name: 'type',
            fields: ['type']
        }, {
            name: 'direction',
            fields: ['direction']
        }],
        tableName: 'controls',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }, {
        hasMany: 'Report',
        foreignKey: 'control_id',
        as: 'reports'
    }, {
        hasMany: 'AutoControlItem',
        foreignKey: 'control_id',
        as: 'autoControlItems'
    }, {
        hasMany: 'AutoControlStep',
        foreignKey: 'control_id',
        as: 'autoControlSteps'
    }, {
        hasOne: 'ControlReservation',
        foreignKey: 'control_id',
        as: 'controlReservation'
    }, {
        hasMany: 'AutoControlControl',
        foreignKey: 'control_id',
        as: 'autoControlControls'
    }, {
        hasMany: 'ControlGroupItem',
        foreignKey: 'control_id',
        as: 'controlGroupItems'
    }, {
        hasMany: 'ControlAlarm',
        foreignKey: 'control_id',
        as: 'controlAlarms'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            getWindDirectionControls: house => {
                return new Promise(async (resolve, reject) => {
                    try {
                        if (house.houseWindDirectionSensor && house.houseWindDirectionSensor.sensor) {
                            let result = {
                                forward: [],
                                backward: []
                            };
                            const leftControls = await sequelize.models.Control.findAll({
                                where: {
                                    mode: 'auto',
                                    direction: 'left'
                                }
                            });
                            const rightControls = await sequelize.models.Control.findAll({
                                where: {
                                    mode: 'auto',
                                    direction: 'right'
                                }
                            });
                            let direction = STD.house.enumDirections.indexOf(house.direction) * 22.5;
                            const sensorValue = parseFloat(house.houseWindDirectionSensor.sensor.value);

                            if (direction <= 180) {
                                if (direction <= sensorValue && 180 + direction > sensorValue) {
                                    result.forward = rightControls;
                                    result.backward = leftControls;
                                } else {
                                    result.forward = leftControls;
                                    result.backward = rightControls;
                                }
                            } else {
                                direction -= 180;
                                if (direction <= sensorValue && 180 + direction > sensorValue) {
                                    result.forward = leftControls;
                                    result.backward = rightControls;
                                } else {
                                    result.forward = rightControls;
                                    result.backward = leftControls;
                                }
                            }

                            resolve(result);
                        } else {
                            resolve({
                                forward: [],
                                backward: []
                            });
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            findById: (id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const query = `SELECT *, (SELECT COUNT(*) FROM auto_control_items WHERE auto_control_items.control_id = controls.id) AS autoControlItemCount, (SELECT COUNT(*) FROM auto_control_steps WHERE auto_control_steps.control_id = controls.id) AS autoControlStepCount FROM controls WHERE id = ${id}`;
                        const controls = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        if (controls.length) {
                            resolve(controls[0]);
                        } else {
                            resolve(null);
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            checkControlStates: (ids, states) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        if (ids.length) {
                            const selectQuery = `SELECT * FROM controls WHERE \`type\` IN('motor', 'power') AND id IN(${ids.join(', ')}) ORDER BY FIELD(id, ${ids.join(', ')})`;
                            const controls = await sequelize.query(selectQuery, {
                                type: Sequelize.QueryTypes.SELECT
                            });
                            if (controls.length === ids.length) {
                                if (checkStates(controls, states)) {
                                    resolve(true);
                                } else {
                                    reject(false);
                                }
                            } else {
                                reject(false);
                            }
                        } else {
                            resolve(true);
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getControls: ({house_id, type, types, state, size, offset, ipAddress, direction, user}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let where = {};
                        if (house_id) where.house_id = house_id;
                        if (type) where.type = type;
                        if (types) where.type = types.split(',');
                        if (state) where.state = state;
                        if (ipAddress) where.address = {
                            [Op.like]: `${ipAddress}:%`
                        };
                        if (direction) where.direction = direction;
                        let screens;
                        if (types && house_id) {
                            screens = await sequelize.models.Screen.findAll({
                                order: [['order', 'ASC']],
                                where: {
                                    house_id,
                                    type: types.split(',')
                                }
                            });
                        }

                        const count = await sequelize.models.Control.count({where});
                        let query = `SELECT controls.*, houses.house_name
                        , (SELECT COUNT(*) FROM auto_control_items INNER JOIN auto_controls ON auto_control_items.auto_control_id = auto_controls.id AND auto_controls.state = 'on' WHERE auto_control_items.control_id = controls.id) AS autoControlItemCount
                        , (SELECT COUNT(*) FROM auto_control_steps INNER JOIN auto_controls ON auto_control_steps.auto_control_id = auto_controls.id AND auto_controls.state = 'on' WHERE auto_control_steps.control_id = controls.id) AS autoControlStepCount 
                        FROM controls
                        INNER JOIN houses ON controls.house_id = houses.id
                        WHERE controls.id > 0`;
                        if (house_id) {
                            query += ` AND controls.house_id = ${house_id}`;
                        }
                        if (type) {
                            query += ` AND controls.type = '${type}'`;
                        }
                        if (types) {
                            query += ' AND controls.type IN(';
                            types.split(',').forEach((type, index) => {
                                if (index) {
                                    query += ', ';
                                }
                                query += `'${type.trim()}'`;
                            });
                            query += ')';
                        }
                        if (ipAddress) {
                            query += ` AND controls.address LIKE "${ipAddress}:%"`;
                        }
                        if (direction) {
                            query += ` AND controls.direction = "${direction}" `;
                        }

                        if (user.grade && user.grade === 'member') {
                            query += ` AND controls.id in ( `;
                            query += ` SELECT BB.control_id from users as AA `;
                            query += ` LEFT join user_control_rels as BB on AA.user_id=BB.user_id `;
                            query += ` WHERE AA.user_id=${user.id} `;

                            if (type) {
                                query += ` AND BB.type = '${type}' `;
                            }
                            if (types) {
                                query += ' AND BB.type IN(';
                                types.split(',').forEach((type, index) => {
                                    if (index) {
                                        query += ', ';
                                    }
                                    query += `'${type.trim()}'`;
                                });
                                query += ') ';
                            }
                            query += ') ';
                        }

                        query += ' ORDER BY controls.house_id ASC';
                        if (types) {
                            query += `, FIELD(\`type\``;
                            if (screens) {
                                screens.forEach(screen => {
                                    query += `, '${screen.type}'`;
                                });
                            } else {
                                types.split(',').forEach(type => {
                                    query += `, '${type.trim()}'`;
                                });
                            }
                            query += ')';
                        }
                        query += `, \`order\` ASC, id ASC`;
                        if (size) {
                            query += ` LIMIT ${size}`;
                        }
                        if (offset) {
                            query += ` OFFSET ${offset}`;
                        }

                        const rows = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        resolve({count, rows});
                    } catch (e) {
                        console.log(e);
                        reject(e);
                    }
                });
            },
            updateMotors: (motors) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        // await sequelize.transaction(t => {
                        //     let promises = [];
                        //     motors.forEach((motor, order) => {
                        //         promises.push(sequelize.models.Control.update({
                        //             control_name: motor.control_name,
                        //             direction: motor.direction,
                        //             range: motor.range,
                        //             order
                        //         }, {
                        //             where: {
                        //                 id: motor.id
                        //             },
                        //             transaction: t
                        //         }));
                        //     });
                        //     return Promise.all(promises);
                        // });
                        let promises = [];
                        motors.forEach((motor, order) => {
                            promises.push(sequelize.models.Control.update({
                                control_name: motor.control_name,
                                direction: motor.direction,
                                range: motor.range,
                                order
                            }, {
                                where: {
                                    id: motor.id
                                }
                            }));
                        });
                        await Promise.all(promises);
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            updatePowers: (powers) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        // await sequelize.transaction(t => {
                        //     let promises = [];
                        //     powers.forEach((power, order) => {
                        //         promises.push(sequelize.models.Control.update({
                        //             control_name: power.control_name,
                        //             order
                        //         }, {
                        //             where: {
                        //                 id: power.id
                        //             },
                        //             transaction: t
                        //         }));
                        //     });
                        //     return Promise.all(promises);
                        // });
                        let promises = [];
                        powers.forEach((power, order) => {
                            promises.push(sequelize.models.Control.update({
                                control_name: power.control_name,
                                order
                            }, {
                                where: {
                                    id: power.id
                                }
                            }));
                        });
                        await Promise.all(promises);
                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getChart: (control_id, startDate, endDate, isDate = false, startHour = null, startMinute = null, endHour = null, endMinute = null) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let table_names = [];
                        const startTime = new Date(startDate);
                        if(startHour && startMinute) startTime.setHours(startHour, startMinute);
                        const endTime = new Date(endDate);
                        if(endHour && endMinute) endTime.setHours(endHour, endMinute);

                        const startYear = startTime.getFullYear();
                        const startMonth = startTime.getMonth() + 1;
                        const endYear = endTime.getFullYear();
                        const endMonth = endTime.getMonth() + 1;

                        if (startYear === endYear && startMonth === endMonth) {
                            table_names.push('log_control_' + startYear + (FILTER_UTIL.attachZero(startMonth)));
                        } else {
                            const endTableName = 'log_control_' + endYear + (FILTER_UTIL.attachZero(endMonth));
                            let tableName = 'log_control_' + startYear + (FILTER_UTIL.attachZero(startMonth)),
                                year = startYear,
                                month = startMonth;

                            while (tableName !== endTableName) {
                                table_names.push(tableName);

                                const next = FILTER_UTIL.nextYearMonth(year, month);
                                year = next.year;
                                month = next.month;
                                tableName = 'log_control_' + year + (FILTER_UTIL.attachZero(month));
                            }
                            table_names.push(endTableName);
                        }

                        const rows = await sequelize.models.Control.findOne({
                            where: {
                                id: control_id
                            }
                        });

                        let funcs = [], chartData = [];
                        const startTimestamp = startTime.getTime();
                        const endTimestamp = endTime.getTime();

                        table_names.forEach(table_name => {
                            funcs.push(callback => {

                                let query = ``;

                                if(rows.type === 'motor'){
                                    query = `SELECT ROUND(AVG(a.current), 0) AS value, FROM_UNIXTIME(a.created_at * ${isDate ? 60 : 7200}) AS created_at FROM
                                    (SELECT current AS 'current', FLOOR(UNIX_TIMESTAMP(created_at) / ${isDate ? 60 : 7200}) AS created_at FROM ${table_name}
                                    WHERE control_id = ${control_id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'
                                    ) a GROUP BY a.created_at`;
                                }else if(rows.type === 'power'){
                                    query = `SELECT ROUND(AVG(a.state), 0) AS value, FROM_UNIXTIME(a.created_at * ${isDate ? 60 : 7200}) AS created_at FROM
                                    (SELECT (CASE WHEN state = 'on' THEN 1 ELSE 0 END) as state, FLOOR(UNIX_TIMESTAMP(created_at) / ${isDate ? 60 : 7200}) AS created_at FROM ${table_name}
                                    WHERE control_id = ${control_id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'
                                    ) a GROUP BY a.created_at`;
                                }else if(rows.type === 'control'){
                                    query = `SELECT ROUND(AVG(a.current), 1) AS value, FROM_UNIXTIME(a.created_at * ${isDate ? 60 : 7200}) AS created_at FROM
                                    (SELECT state AS 'state', current AS 'current', range AS 'range', FLOOR(UNIX_TIMESTAMP(created_at) / ${isDate ? 60 : 7200}) AS created_at FROM ${table_name}
                                    WHERE control_id = ${control_id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'
                                    ) a GROUP BY a.created_at`;
                                }

                                sequelize.query(query, {
                                    type: Sequelize.QueryTypes.SELECT
                                }).then(data => {
                                    chartData = [
                                        ...chartData,
                                        ...data
                                    ];
                                    callback(null, true);
                                    return true;
                                }).catch(e => {
                                    callback(null, true);
                                });
                            });
                        });

                        async.series(funcs, (error, results) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(chartData);
                            }
                        });
                    } catch (e) {
                        reject(e);
                    }
                });
            },
        }
    }
};

function checkStates(controls, states) {
    for (let i = 0; i < controls.length; i++) {
        const control = controls[i];
        const state = states[i];
        if (control.type === 'motor' || control.type === 'windDirection') {
            if (state !== 'open' && state !== 'stop' && state !== 'close') {
                return false;
            }
        } else if (control.type === 'power') {
            if (state !== 'on' && state !== 'off') {
                return false;
            }
        } else {
            return false;
        }
    }
    return true;
}
