const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },        
        'animal_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'start_date': {
            type: Sequelize.DATEONLY,
            primaryKey: true,
            allowNull: false
        },
        'end_date': {
            type: Sequelize.DATEONLY,
            primaryKey: true,
            allowNull: false
        },    
        'type': {
            type: Sequelize.ENUM,
            values: ['estrus', 'insemination', 'check', 'dryUp', 'birth'],
            defaultValue: 'estrus',
            primaryKey: true,
            allowNull: false
        },
        'memo': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
    },
    options: {
        tableName: 'animal_breeds',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Animal',
        foreignKey: 'animal_id',
        targetKey: 'id',
        as: 'animal'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {           
          getAnimalBreedCalendar: (animal_id,date) => {
            return new Promise(async (resolve, reject) => {
                try {  
                    let animalBreeds;
                    let animalBreed_query=``;  
                    let select_date=date.replace(/-/gi, "");
                    let selectQuery = ` SELECT AA.id ,AA.animal_id , AA.memo, AA.type   `;
                    selectQuery+=` ,DATE_FORMAT(AA.start_date,'%Y%m%d') AS start_date , DATE_FORMAT(AA.end_date,'%Y%m%d') AS end_date FROM animal_breeds AA`;
                    selectQuery+=` WHERE (DATE_FORMAT(AA.start_date,'%Y%m')<='${select_date}' AND  DATE_FORMAT(AA.end_date,'%Y%m')>='${select_date}') `;
                    selectQuery+=` AND AA.animal_id=${animal_id} ORDER BY AA.start_date ASC`;     

                    sequelize.query(selectQuery, {          
                        type: Sequelize.QueryTypes.SELECT    
                    }).then(rows => {       
                        let query=``;    
                        let rows_count=rows.length;  
                        let count=0;   
                        animalBreeds=rows;
 
                        rows.forEach(data => {
                            count++;  
                            data.animal_id=animal_id;   
                            data.date=date;  
                            query+= getSqlAnimalBreedCalendar(data);
                            if(rows_count!=count) query+=` union `;
               
                        });  
                        animalBreed_query+=` SELECT ZZ.date `;
                        animalBreed_query+=` , SUM(case when ZZ.type =  'estrus' then 1 end) as  'estrus' `;
                        animalBreed_query+=` , SUM(case when ZZ.type =  'check' then 1 end) as 'check' `;
                        animalBreed_query+=` , SUM(case when ZZ.type =  'insemination' then 1 end) as 'insemination' `;
                        animalBreed_query+=` , SUM(case when ZZ.type =  'dryUp' then 1 end) as 'dryUp' `;
                        animalBreed_query+=` , SUM(case when ZZ.type =  'birth' then 1 end) as 'birth'  `;
                        animalBreed_query+=` FROM (`;
                        animalBreed_query+=query;
                        animalBreed_query+=` ) as ZZ `;     
                        animalBreed_query+=` GROUP BY ZZ.date `;  
                           
                        if(rows_count==0)  return null ;           
                        else return sequelize.query(animalBreed_query, {type: Sequelize.QueryTypes.SELECT});
                    }).then(data => {  
                        console.log({animalBreeds , data});   
                        resolve({animalBreeds , data});
                    }).catch(e => {       
                        reject(e);                  
                    });  

                     
                } catch (e) {
                    reject(e);
                }
            });
          },          
          createAnimalBreed: ({ animal_id ,start_date ,end_date ,type ,memo}) => {
              return new Promise(async (resolve, reject) => {
                  try {
                      const createdData = await sequelize.models.AnimalBreed.create({
                          animal_id:animal_id,
                          start_date:start_date,
                          end_date:end_date,       
                          type:type, 
                          memo:memo   
                      });
                      resolve(createdData);
                  } catch (e) {
                      reject(e);
                  }
              });
          },
          updateAnimalBreed: ( id ,{ start_date ,end_date ,type ,memo}) => {
            return new Promise(async (resolve, reject) => {
                try {     
                    await sequelize.models.AnimalBreed.update({
                        start_date :start_date,
                        end_date :end_date,   
                        type :type,
                        memo :memo,
                    }, {
                        where: {id : id}   
                    });
                    resolve(true);
                } catch (e) {
                  console.log(e);
                    reject(e);
                }
            });
          }, 
          deleteAnimalBreed: (id) => {
            return new Promise(async (resolve, reject) => {
                try {
                    let where = {};
                    where.id = id; 

                    const row = await sequelize.models.AnimalBreed.destroy({where});
                    resolve(true);
                } catch (e) {
                  console.log(e);
                    reject(e);
                }
            });
          }                   
        }
    }
};

function getSqlAnimalBreedCalendar(data){
    let query=``;        
   
    query+=` SELECT "${data.type}" as type , x.date FROM `;  
    query+=` ( `;      
    query+=` SELECT `;
    query+=` t.n, DATE_ADD("${data.date}-01", INTERVAL t.n DAY)  as date `;
    query+=` FROM (`;   
    query+=` SELECT `;
    query+=` a.N + b.N * 10 + c.N * 100 AS n `;
    query+=` FROM `;
    query+=` (SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a `;
    query+=` ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b `;
    query+=` ,(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4) c `;
    query+=` ORDER BY n `;
    query+=` ) t  `;      
    query+=` WHERE  t.n <= 31  `;  
    query+=`  ) AS x `;  
    query+=` WHERE x.date like "${data.date}%" `;
    query+=` AND x.date between  ${data.start_date} and ${data.end_date} `;
        
    return query;       
}        