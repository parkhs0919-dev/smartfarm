const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'control_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['open', 'stop', 'close', 'on', 'off'],
            defaultValue: 'stop',
            allowNull: false
        },
        'text' :{
            type: Sequelize.STRING(20),
            allowNull: false
        }
    },
    options: {
        indexes: [],
        tableName: 'control_alarms',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Control',
        foreignKey: 'control_id',
        targetKey: 'id',
        as: 'control'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            getControlAlarms: ({control_id, state}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let include = [{
                            model: sequelize.models.Control,
                            as: 'control',
                            required: true
                        }];

                        let where = {};
                        if (control_id) where.control_id = control_id;
                        if (state) where.state = state;

                        const count = await sequelize.models.ControlAlarm.count({
                            include,
                            where
                        });
                        const rows = await sequelize.models.ControlAlarm.findAll({
                            include,
                            where
                        });
                        resolve({count, rows});
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getControlAlarmsDuplicateCount: ({ control_id, state}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let include = [];
                        let where = {};

                        if (control_id) where.control_id = control_id;
                        if (state) where.state = state;

                        const count = await sequelize.models.ControlAlarm.count({
                            include,
                            where
                        });

                        resolve({count});
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
