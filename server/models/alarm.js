const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'sensor_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'condition': {
            type: Sequelize.ENUM,
            values: ['over', 'under', 'equal'],
            defaultValue: 'over',
            allowNull: false
        },
        'value': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: false
        },
        'is_bar': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'is_sound': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'is_image': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'send_time': {
            type: Sequelize.BIGINT.UNSIGNED,
            defaultValue: 0,
            allowNull: false
        },
        'name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['range', 'error',],
            defaultValue: 'range',
            allowNull: true
        },
        'is_use': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: true
        },
        'is_no': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: true
        },
        'use_start_hour': {
            type: Sequelize.TINYINT(1),
            defaultValue: 0,
            allowNull: true
        },
        'use_end_hour': {
            type: Sequelize.TINYINT(1),
            defaultValue: 0,
            allowNull: true
        },
        'no_start_hour': {
            type: Sequelize.TINYINT(1),
            defaultValue: 0,
            allowNull: true
        },
        'no_end_hour': {
            type: Sequelize.TINYINT(1),
            defaultValue: 0,
            allowNull: true
        },
        'cycle': {
            type: Sequelize.TINYINT(1),
            defaultValue: 5,
            allowNull: true
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['use', 'unuse'],
            defaultValue: 'use',
            allowNull: false
        },
        'repeat': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false,
            defaultValue: '1'
           
        }
    },
    options: {
        indexes: [{
            name: 'condition',
            fields: ['condition']
        }, {
            name: 'value',
            fields: ['value']
        }, {
            name: 'unique_key',
            fields: ['sensor_id', 'condition', 'value'],
            unique: true
        }, {
            name: 'send_time',
            fields: ['send_time']
        }],
        tableName: 'alarms',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Sensor',
        foreignKey: 'sensor_id',
        targetKey: 'id',
        as: 'sensor'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            getAlarms: ({house_id, sensor_id, condition, size, offset, position}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let include = [{
                            model: sequelize.models.Sensor,
                            as: 'sensor',
                            required: true
                        }];
                        if (house_id) {
                            include[0].include = [{
                                model: sequelize.models.HouseSensorRel,
                                as: 'houseSensorRels',
                                required: true,
                                where: {house_id}
                            }];
                        }
                        if (position) include[0].where = {position};

                        let where = {};
                        if (sensor_id) where.sensor_id = sensor_id;
                        if (condition) where.condition = condition;

                        const count = await sequelize.models.Alarm.count({
                            include,
                            where
                        });
                        const rows = await sequelize.models.Alarm.findAll({
                            include,
                            where,
                            limit: size ? parseInt(size) : undefined,
                            offset: parseInt(offset)
                        });
                        resolve({count, rows});
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            getAlarmsDuplicateCount: ({ sensor_id, condition,value}) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        let include = [];
                        let where = {};

                        if (sensor_id) where.sensor_id = sensor_id;
                        if (condition) where.condition = condition;
                        where.value = value

                        const count = await sequelize.models.Alarm.count({
                            include,
                            where
                        });

                        resolve({count});
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
