const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'rise_hour': {
            type: Sequelize.STRING(2),
            allowNull: false
        },
        'rise_minute': {
            type: Sequelize.STRING(2),
            allowNull: false
        },
        'set_hour': {
            type: Sequelize.STRING(2),
            allowNull: false
        },
        'set_minute': {
            type: Sequelize.STRING(2),
            allowNull: false
        }
    },
    options: {
        tableName: 'sun_date_defaults',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    methods: {
        instanceMethods: {},
        classMethods: {}
    }
};
