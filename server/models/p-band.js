const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'p_band_name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'in_temperature_sensor_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'out_temperature_sensor_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'wind_speed_sensor_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: false
        },
        'is_min_max_p': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'wind_direction_forward_min_p': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'wind_direction_forward_max_p': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'wind_direction_backward_min_p': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        },
        'wind_direction_backward_max_p': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            allowNull: true
        }
    },
    options: {
        tableName: 'p_bands',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }, {
        belongsTo: 'Sensor',
        foreignKey: 'in_temperature_sensor_id',
        targetKey: 'id',
        as: 'inTemperatureSensor'
    }, {
        belongsTo: 'Sensor',
        foreignKey: 'out_temperature_sensor_id',
        targetKey: 'id',
        as: 'outTemperatureSensor'
    }, {
        belongsTo: 'Sensor',
        foreignKey: 'wind_speed_sensor_id',
        targetKey: 'id',
        as: 'windSpeedSensor'
    }, {
        hasMany: 'AutoControlItem',
        foreignKey: 'p_band_id',
        as: 'autoControlItems'
    }, {
        hasMany: 'AutoControlStep',
        foreignKey: 'p_band_id',
        as: 'autoControlSteps'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            destroyLegacyAutoControls: () => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const query = `DELETE auto_controls FROM auto_controls
                        LEFT JOIN auto_control_items ON auto_control_items.auto_control_id = auto_controls.id
                        LEFT JOIN auto_control_steps ON auto_control_steps.auto_control_id = auto_controls.id
                        WHERE (auto_control_items.p_band_temperature IS NOT NULL AND auto_control_items.p_band_id IS NULL)
                        OR (auto_control_steps.p_band_temperature IS NOT NULL AND auto_control_steps.p_band_id IS NULL)`;

                        await sequelize.query(query, {
                            type: Sequelize.QueryTypes.DELETE,
                            cascade: true
                        });

                        resolve(true);
                    } catch (e) {
                        reject(e);
                    }
                });
            },
            checkDelete : id => {
                return new Promise(async (resolve, reject) => {
                    try{

                        let where = {
                            p_band_id : id
                        }

                        const itemCount = await sequelize.models.AutoControlItem.count({where});
                        const stepCount = await sequelize.models.AutoControlStep.count({where});

                        if(itemCount+stepCount === 0){
                            resolve(true);
                        }else{
                            reject(false);
                        }

                    }catch(e){
                        reject(e);
                    }
                })

            },
        }
    }
};
