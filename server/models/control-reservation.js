const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'control_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'user_id': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'mode': {
            type: Sequelize.ENUM,
            values: ['manual', 'auto'],
            defaultValue: 'manual',
            allowNull: false
        },
        'before_state': {
            type: Sequelize.ENUM,
            values: ['open', 'stop', 'close', 'on', 'off'],
            defaultValue: 'stop',
            allowNull: false
        },
        'after_state': {
            type: Sequelize.ENUM,
            values: ['open', 'stop', 'close', 'on', 'off'],
            allowNull: true
        },
        'active_time': {
            type: Sequelize.BIGINT.UNSIGNED,
            allowNull: true
        }
    },
    options: {
        tableName: 'control_reservations',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Control',
        foreignKey: 'control_id',
        targetKey: 'id',
        as: 'control'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
