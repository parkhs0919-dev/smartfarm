const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'house_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        },
        'sensor_id': {
            type: Sequelize.INTEGER(10).UNSIGNED,
            primaryKey: true,
            allowNull: false
        }
    },
    options: {
        tableName: 'sensor_main_charts',
        timestamps: false,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'House',
        foreignKey: 'house_id',
        targetKey: 'id',
        as: 'house'
    }, {
        belongsTo: 'Sensor',
        foreignKey: 'sensor_id',
        targetKey: 'id',
        as: 'sensor'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            createSensorMainChart: ({house_id, sensor_id}) => {
                return new Promise(async(resolve, reject) => {
                    try {
                        let createdData = null;
                        await sequelize.transaction((t) => {
                            return sequelize.models.SensorMainChart.destroy({
                                where: {house_id},
                                transaction: t
                            }).then(() => {
                                return sequelize.models.SensorMainChart.create({
                                    house_id,
                                    sensor_id
                                }, {
                                    transaction: t
                                });
                            }).then(data => createdData = data);
                        });
                        resolve(createdData);
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
