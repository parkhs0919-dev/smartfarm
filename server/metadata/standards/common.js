const standards = {
    defaultSize: 12,
    defaultCctvSize: 4,
    screen: {
        enumTypes: ['cctv', 'control', 'motor', 'power', 'house', 'outerSensorAvg','outerSensor','innerSensorAvg','innerSensor', 'chart', 'notice', 'report', 'csv', 'pesticide', 'fluid'],
        defaultType: 'cctv',
        typePesticide: 'pesticide',
        typeFluid: 'fluid'
    },
    house: {
        enumDirections: ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW'],
        defaultDirection: 'N'
    },
    sunDate: {
        enumTypes: ['rise', 'set'],
        typeRise: 'rise',
        typeSet: 'set',
        default: {
            rise_hour: 6,
            rise_minute: 8,
            set_hour: 18,
            set_minute: 43
        }
    },
    fcm: {
        sound: {
            android: {
                notification: 'default',
                etc: 'etc.mp3',
                rainon: 'rainon.mp3',
                rainoff: 'rainoff.mp3',
                over: 'over.mp3',
                under: 'under.mp3'
            },
            ios: {
                notification: 'default',
                etc: 'etc',
                rainon: 'rainon',
                rainoff: 'rainoff',
                over: 'over',
                under: 'under'
            }
        },
    },
    maxMotorPercentage: 200
};

module.exports = standards;
