const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');

const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'position',
                'type',
                'types',
                'offset',
                'size',
                'isTotal'
            ],
            essential: [],
            explains: {
                'house_id': '하우스 ID',
                'position': '위치 (in, out)',
                'type': '유형',
                'types': '유형 (,)로 구분',
                'offset': 'offset',
                'size': 'size',
                'isTotal': '화면 순서로 조회 (true)'
            },
            title: '센서 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getSensors(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
};

router.get('/', api.gets());

module.exports.router = router;
module.exports.api = api;
