let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isEnum('position', ['in', 'out']);
    ctx.check.isInt('size');
    ctx.check.isInt('offset');
    ctx.check.isBoolean('isTotal');
    ctx.sanitize.toBoolean('isTotal');

    // if (!ctx.query.size) ctx.query.size = ctx.meta.std.defaultSize;
    if (!ctx.query.offset) ctx.query.offset = 0;

    ctx.data = {};
};

gets.getSensors = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.Sensor.getSensors(ctx.query);
        for (let i=0; i<ctx.data.rows.length; i++) {
            if (ctx.data.rows[i].type === 'temperature') {
                const {min, max} = await ctx.models.Sensor.getMinMax(ctx.data.rows[i].id);
                console.log(min, max);
                ctx.data.rows[i].dataValues.min = min;
                ctx.data.rows[i].dataValues.max = max;
            }
        }
    } catch (e) {
        ctx.throw(400, '0101');
    }
};

gets.supplement = async (ctx, next) => {
    if (!ctx.data.count) {
        ctx.throw(404);
    }
    ctx.json(ctx.data);
};

module.exports = gets;
