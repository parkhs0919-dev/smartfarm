let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');

    ctx.data = null;
};

get.getControlGroup = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.ControlGroup.findOne({
                where : {
                    id : ctx.params.id
                },
                include : [{
                    model: ctx.models.ControlGroupItem,
                    as: 'controlGroupItems'
                }],
            }

        );
        if (!ctx.data) {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(404, '0109');
    }
};

get.checkControlGroup = async (ctx, next) => {
    try {
        if (ctx.data){
            const item = ctx.data;
            let where = {
                window_position :item.window_position,
            }

            let itemCount = await ctx.models.AutoControlItem.count({where});

            let stepCount = await ctx.models.AutoControlStep.count({where});

            ctx.data = {
                count: itemCount + stepCount,
                rows: item
            };
        }
    } catch(e) {
        ctx.throw(400);
    }
}

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
