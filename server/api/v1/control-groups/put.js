let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.check.isInt('id');
    ctx.check.isInt('house_id');
    ctx.check.isEnum('type', ['windDirection', 'motor', 'power']);
    ctx.check.isEnum('on_off_state', ['on', 'off']);
    ctx.check.len('control_group_name', 1, 191);
    ctx.check.isEnum('direction', ['none', 'left', 'right']);
    ctx.check.isInt('window_position');

};

put.updateControlGroup = async (ctx, next) => {
    try {
        await ctx.models.ControlGroup.updateControlGroups(ctx.request.body, ctx.params.id);
    } catch (e) {
        //console.log(e);
        ctx.throw(400, '0224');
    }
};

put.supplement = async (ctx, next) => {
    const io = ctx.utils.io();
    if (io) io.emit('control-groups', {
        house_id: ctx.request.body.house_id
    });
    ctx.json(null);
};

module.exports = put;
