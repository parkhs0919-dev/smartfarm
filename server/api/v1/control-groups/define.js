const Router = require('koa-router');
const router = new Router();
const get = require('./get');
const gets = require('./gets');
const post = require('./post');
const put = require('./put');
const del = require('./del');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'house id'
            },
            title: '그룹설정 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.getControlGroup(ctx, next);
                await get.checkControlGroup(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'type',
                'on_off_state',
                'control_group_name',
                'state',
                'direction',
                'window_position'
            ],
            essential: [],
            explains: {},
            title: '제어 그룹 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getControlGroups(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'type',
                'state',
                'on_off_state',
                'direction',
                'state',
                'window_position',
                'controlGroupItems'
            ],
            essential: [
                'house_id',
                'type',
            ],
            explains: {
                'house_id': 'house id',
                'type': '제어 타입(`windDirection`, `motor`, `power`)',
                'state': '상태 (`open`, `stop`, `close`, `on`, `off`)',
                'on_off_state': 'on off',
                'control_group_name': '제어 그룹 이름',
                'direction' : '바람 방향',
                'window_position' : '창 위치',
                'controlGroupItems' : '제어 아이템 리스트'
            },
            title: '제어 그룹 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.checkDuplicateControlGroup(ctx, next);
                await post.checkControlGroup(ctx, next);
                await post.createControlGroup(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'control_group_name',
                'type',
                'state',
                'on_off_state',
                'direction',
                'window_position',
                'controlGroupItems'
            ],
            essential: [
                'house_id',
                'type',
            ],
            params: ['id'],
            explains: {
                'house_id': 'house id',
                'type': '제어 타입(`windDirection`, `motor`, `power`)',
                'on_off_state': 'on off',
                'control_group_name': '제어 그룹 이름',
                'direction' : '바람 방향',
                'window_position' :'창 위치',
                'controlGroupItems' : '제어 아이템 리스트'
            },
            title: '제어 그룹 수정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.updateControlGroup(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'control group id'
            },
            title: '그룹 제어 삭제',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.findControlGroup(ctx, next);
                await del.deleteControlGroup(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.gets());
router.get('/:id' , api.get());
router.post('/', api.post());
router.put('/:id', api.put());
router.del('/:id', api.del());

module.exports.router = router;
module.exports.api = api;
