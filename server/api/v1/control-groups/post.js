let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.check.isInt('house_id');
    ctx.check.isEnum('type', ['windDirection', 'motor', 'power']);
    ctx.check.isEnum('state', ['open', 'stop', 'close', 'on', 'off']);
    ctx.check.isEnum('on_off_state', ['on', 'off']);
    ctx.check.len('control_group_name', 1, 191);
    ctx.check.isEnum('direction', ['none', 'left', 'right']);
    ctx.check.isInt('window_position');

    ctx.data = {};
};

post.checkDuplicateControlGroup = async (ctx, next) => {
    try {
        let body = ctx.request.body;
        if (body.type === 'windDirection') {
            let where = {
                type: body.type,
                house_id: body.house_id,
                direction: body.direction,
                window_position: body.window_position,
            };

            if (await ctx.models.ControlGroup.count({where})) {
                ctx.throw(400);
            }
        }
    } catch (e) {
        ctx.throw(400, '0285');
    }
};

post.checkControlGroup = async (ctx, next) => {
    try {
        let body = ctx.request.body;
        if (body.type === 'motor') {
            if (body.state !== 'open' && body.state !== 'stop' && body.state !== 'close') {
                ctx.throw();
            }
        } else if (body.type === 'power') {
            if (body.state !== 'on' && body.state !== 'off') {
                ctx.throw();
            }
        }
    } catch (e) {
        ctx.throw(400, '0299');
    }
};

post.createControlGroup = async (ctx, next) => {
    try {

        const createdData = await ctx.models.ControlGroup.createControlGroup(ctx.request.body);

        ctx.data = await createdData.reload();

    } catch (e) {
        ctx.throw(400, '0284');
    }
};

post.supplement = async (ctx, next) => {
    const io = ctx.utils.io();
    if (io) io.emit('control-groups', {
        house_id: ctx.request.body.house_id
    });
    ctx.json(ctx.data);
};

module.exports = post;
