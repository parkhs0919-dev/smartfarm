let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');

    ctx.data = {};
};

del.findControlGroup = async (ctx, next) => {
    try {
        ctx.controlGroup = await ctx.models.ControlGroup.findByPk(ctx.params.id);
        if (!ctx.controlGroup) {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(404);
    }
};

del.deleteControlGroup = async (ctx, next) => {
    try {
        await ctx.models.ControlGroup.destroy({
            where: {
                id: ctx.params.id
            },
            include: [{
                model: ctx.models.ControlGroupItem,
                as: 'controlGroupItems'
            }]
        });
    } catch (e) {
        ctx.throw(400, '0219');
    }
};

del.supplement = async (ctx, next) => {
    const io = ctx.utils.io();
    if (io) io.emit('control-groups', {
        house_id: ctx.controlGroup.house_id
    });
    ctx.json(null);
};

module.exports = del;
