let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.data = {};
};

gets.getFlowMeterRoundDate = async (ctx, next) => {
    try {          
        ctx.data = await ctx.models.FlowMeterRound.getFlowMeterRoundDate(ctx.query);        
    } catch (e) {         
        console.log(e);     
        ctx.throw(404, '0609');
    }      
};

gets.supplement = async (ctx, next) => {
    console.log(808080,ctx.data)
    ctx.json(ctx.data);
};

module.exports = gets;
