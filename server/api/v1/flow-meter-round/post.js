let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.check.isInt('house_id');
    ctx.check.isInt('start_hour');
    ctx.check.isInt('end_hour');
    ctx.data = {};
};     

post.createFlowMeterRound = async (ctx, next) => {
    try {          
        const createdData = await ctx.models.FlowMeterRound.createFlowMeterRound({
            ...ctx.request.body
        });        
        if (createdData) {
            ctx.data = await createdData.reload();
            const io = ctx.utils.io();
            const house_id=ctx.request.body.house_id;
            if (io) io.emit('flow_meter_round', house_id);  
        } else {     
            ctx.throw(400, '0606');     
        } 
    } catch (e) { 
        console.log(e);
        ctx.throw(400, '0606');
    }  
};

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};
  
module.exports = post;
