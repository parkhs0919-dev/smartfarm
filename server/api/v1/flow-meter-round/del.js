let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');

    ctx.data = {};
};

del.deleteFlowMeterRound = async (ctx, next) => {
    try {
       
        await ctx.models.FlowMeterRound.destroy({
            where: {
                id: ctx.params.id
            }  
        });

    } catch (e) {
        ctx.throw(400, '0608');
    }
};
   
del.supplement = async (ctx, next) => {
    ctx.json(null); 
    const io = ctx.utils.io();
    const house_id=ctx.request.body.house_id;
    if (io) io.emit('flow_meter_round',house_id);     
    //if (io) io.emit(`${ctx.alarm.sensor.position === 'in' ? 'inner' : 'outer'}-alarm`);
};

module.exports = del;
