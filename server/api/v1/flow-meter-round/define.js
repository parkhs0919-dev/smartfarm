const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');
const post = require('./post');
const put = require('./put');
const del = require('./del');
   
const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                `id`,
                `house_id`,
                `start_hour`,                
                `end_hour`
            ],
            essential: [],
            explains: {
            },
            title: '유량계 회차  조회',
            state: 'develop'
        };
  
        if (!isParam) {
            try {                                
                await gets.validate(ctx, next);
                await gets.getFlowMeterRoundDate(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },  
    post: (isParam) => async (ctx, next) => {
        const params = {     
            acceptable: [
                `flow_meter_id`
            ],
            essential: [
            ],
            explains: {
                'flow_meter_id': 'flow_meter_id'

            },
            title: '유량계 회차 설정 추가',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.createFlowMeterRound(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {   
                ctx.error(e.message, e.status);
            }   
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {   
            acceptable: [
                `id_list`,
                `start_hour_list`,  
                `end_hour_list`,
            ],
            essential: [
                `id_list`,
                `start_hour_list`,  
                `end_hour_list`,
            ],
            explains: {
                'id_list': 'id_list  ',
                'start_hour_list': 'start_hour_list 명 ',
                'end_hour_list': 'end_hour_list '
            },
            title: '유량계 회차 설정 수정',
            state: 'develop' 
        };
     
        if (!isParam) {
            try {
                
                await put.validate(ctx, next);
                await put.updateFlowMeterRound(ctx, next);
                await put.supplement(ctx, next);   
            } catch (e) {  
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },  
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'id'
            },
            title: '유량계 회차 설정 삭제',
            state: 'develop'
        };  

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.deleteFlowMeterRound(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};
 
router.get('/', api.gets());   
router.post('/', api.post());
router.put('/:id', api.put());  
router.del('/:id', api.del());         

module.exports.router = router;
module.exports.api = api;
