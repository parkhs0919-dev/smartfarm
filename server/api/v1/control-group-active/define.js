const Router = require('koa-router');
const router = new Router();

const put = require('./put');

const api = {
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'state'
            ],
            essential: [
                'state'
            ],
            params: ['id'],
            explains: {
                'id': 'control group id',
                'state': '상태 (open, stop, close, on, off)',
            },
            title: '수동 그룹 조작',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx);
                await put.findControlGroup(ctx);
                await put.updateControlGroup(ctx);
                await put.createReport(ctx);
                await put.activeControls(ctx);
                await put.supplement(ctx);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.put('/:id', api.put());

module.exports.router = router;
module.exports.api = api;
