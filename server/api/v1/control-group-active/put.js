let put = {};

put.validate = async ctx => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.isEnum('state', ['open', 'stop', 'close', 'on', 'off']);
};

put.findControlGroup = async ctx => {
    try {
        ctx.controlGroup = await ctx.models.ControlGroup.findByPk(ctx.params.id, {
            include: [{
                model: ctx.models.ControlGroupItem,
                as: 'controlGroupItems',
                include: [{
                    model: ctx.models.Control,
                    as: 'control'
                }]
            }]
        });
        if (!ctx.controlGroup || (ctx.controlGroup.type !== 'motor' && ctx.controlGroup.type !== 'power')) {
            ctx.throw();
        }
        for (let i=0; i<ctx.controlGroup.controlGroupItems.length; i++) {
            if (ctx.controlGroup.controlGroupItems[i].control.mode !== 'manual') {
                ctx.throw();
            }
        }
    } catch (e) {
        ctx.throw(400, '0296');
    }
};

put.updateControlGroup = async ctx => {
    try {
        const io = ctx.utils.io();
        if (io) {
            io.emit('control-group-active-start', {
                house_id: ctx.controlGroup.house_id
            });
        }

        let state = ctx.request.body.state;
        if (ctx.controlGroup.type === 'motor') {
            if (state !== 'open' && state !== 'stop' && state !== 'close') {
                ctx.throw();
            }
        } else if (ctx.controlGroup.type === 'power') {
            if (state !== 'on' && state !== 'off') {
                ctx.throw();
            }
        } else {
            ctx.throw();
        }
        await ctx.models.ControlGroup.update({
            state
        }, {
            where: {
                id: ctx.params.id
            }
        });
        if (io) {
            io.emit('control-group', {
                controlGroup: {
                    id: ctx.params.id,
                    type: ctx.controlGroup.type,
                    state
                }
            });
        }
    } catch (e) {
        ctx.throw(400, '0297');
    }
};

put.createReport = async ctx => {
    try {
        const io = ctx.utils.io();
        await ctx.models.Report.create({
            user_id: ctx.user.id,
            house_id: ctx.controlGroup.house_id,
            control_id: null,
            type: ctx.controlGroup.type,
            contents: `[그룹제어] ${ctx.utils.control.generateGroupContents(ctx.controlGroup, ctx.request.body.state)}`
        });
        if (io) io.emit('report', {
            house_id: ctx.controlGroup.house_id
        });
    } catch (e) {
        ctx.throw(400, '0298');
    }
};

put.activeControls = async ctx => {
    try {
        const io = ctx.utils.io();
        const user_id = ctx.user.id;
        const state = ctx.request.body.state;
        const type = 'manual';
        if (ctx.controlGroup.type === 'motor') {
            for (let i=0; i<ctx.controlGroup.controlGroupItems.length; i++) {
                const control = ctx.controlGroup.controlGroupItems[i].control;
                try {
                    if (state === 'open' || state === 'close') {
                        if (control.state !== 'stop') {
                            await ctx.utils.control.activeMotor({
                                id: control.id,
                                state: 'stop',
                                type,
                                time: null,
                                user_id
                            });
                        }
                        await ctx.utils.control.activeMotor({
                            id: control.id,
                            state,
                            type,
                            time: null,
                            user_id
                        });
                    } else {
                        await ctx.utils.control.activeMotor({
                            id: control.id,
                            state,
                            type,
                            time: null,
                            user_id
                        });
                    }
                } catch (e) {
                    if (io) io.emit('report', {
                        house_id: ctx.controlGroup.house_id
                    });
                }
            }
        } else if (ctx.controlGroup.type === 'power') {
            for (let i=0; i<ctx.controlGroup.controlGroupItems.length; i++) {
                const control = ctx.controlGroup.controlGroupItems[i].control;
                try {
                    await ctx.utils.control.activePower({
                        id: control.id,
                        state,
                        type,
                        time: null,
                        user_id
                    });
                } catch (e) {
                    if (io) io.emit('report', {
                        house_id: ctx.controlGroup.house_id
                    });
                }
            }
        }
    } catch (e) {
        ctx.throw(400, '0297');
    }
};

put.supplement = async ctx => {
    const io = ctx.utils.io();
    if (io) {
        io.emit('control-group-active-end', {
            house_id: ctx.controlGroup.house_id
        });
    }
    ctx.json(null);
};

module.exports = put;
