const getSerialPorts = require('../../../methods/serial-port').getSerialPorts;
let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.data = null;
};

gets.getSerialPorts = async (ctx, next) => {
    try {
        ctx.data = await getSerialPorts();
    } catch (e) {
        ctx.throw(400, '0901');
    }
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
