const Router = require('koa-router');
const router = new Router();

const post = require('./post');
const gets = require('./gets');
const del = require('./del');

const api = {
    post: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_id',
            'date',
            'memo'
          ],
          essential: [
            'animal_id',
            'date',
            'memo'
          ],
          explains: {
            'animal_id' : 'animal_id',
            'date': 'date',
            'memo': 'memo'
          },
          title: '교배차수 입력',
          state: 'develop'
      };

      if (!isParam) {
          try {
              //await post.validate(ctx, next);
              await post.createAnimalMate(ctx, next);
              await post.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }
    },
    gets: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_id',
            'date',
            'memo'
          ],
          essential: [
            'animal_id',
            'date',
            'memo'
          ],
          explains: {
            'animal_id' : 'animal_id',
            'date': 'date',
            'memo': 'memo'
          },
          title: '교배차수 조회',
          state: 'develop'
      };

      if (!isParam) {
          try {
              //await post.validate(ctx, next);
              await gets.getAnimalMate(ctx, next);
              await gets.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }  
    },
    del: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_id',
          ],
          essential: [
            'animal_id',
          ],
          explains: {
            'animal_id' : 'animal_id',
          },
          title: ' 삭제',
          state: 'develop'
        };

        if (!isParam) {
            try {
                //await del.validate(ctx, next);
                await del.deleteAnimalMate(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);  
            }
        } else {  
            return params;
        }
    }       
};

router.post('/', api.post());
router.get('/:id', api.gets());
router.delete('/:id', api.del());  

module.exports.router = router;
module.exports.api = api;
