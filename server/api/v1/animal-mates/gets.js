let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};
gets.getAnimalMate = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.AnimalMate.getAnimalMate(ctx.params.id,ctx.query);
    } catch (e) {
        ctx.throw(400, '0516');
    }
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
