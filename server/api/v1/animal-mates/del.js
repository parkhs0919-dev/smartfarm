let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};

del.deleteAnimalMate = async (ctx, next) => {
  try {
       ctx.data = await ctx.models.AnimalMate.deleteAnimalMate(ctx.params.id);
  } catch(e) {
      ctx.throw(400, '0517');
  }    
};

del.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = del;
