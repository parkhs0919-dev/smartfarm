let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');

    ctx.data = null;
};

get.getAutoControlWindDirectionCount = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.AutoControl.getAutoControlWindDirectionCount(ctx.params.house_id);
    } catch (e) {
        ctx.throw(400, '0280');
    }
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
