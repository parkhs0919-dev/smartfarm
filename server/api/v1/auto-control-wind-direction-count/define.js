const Router = require('koa-router');
const router = new Router();

const get = require('./get');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['house_id'],
            explains: {
                'house_id': '하우스 id'
            },
            title: '풍상 풍하 자동제어 count 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.getAutoControlWindDirectionCount(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:house_id', api.get());

module.exports.router = router;
module.exports.api = api;
