let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.check.isInt('sensor_id');
    ctx.check.isInt('cycle');
    ctx.check.isEnum('condition', ['over', 'under', 'equal']);
    ctx.check.isFloat('value');

    ctx.check.isInt('use_start_hour');
    ctx.check.isInt('use_end_hour');
    ctx.check.isInt('no_start_hour');
    ctx.check.isInt('no_end_hour');
    ctx.check.isInt('repeat');

    ctx.check.isBoolean('is_bar');
    ctx.check.isBoolean('is_sound');
    ctx.check.isBoolean('is_image');
    ctx.check.isBoolean('is_use');
    ctx.check.isBoolean('is_no');

    ctx.sanitize.toBoolean('is_bar');
    ctx.sanitize.toBoolean('is_sound');
    ctx.sanitize.toBoolean('is_image');
    ctx.sanitize.toBoolean('is_use');
    ctx.sanitize.toBoolean('is_no');

    ctx.check.isInt('use_start_hour');
    ctx.check.isInt('use_end_hour');
    ctx.check.isInt('no_start_hour');
    ctx.check.isInt('no_end_hour');

    ctx.data = {};
};

post.createAlarm = async (ctx, next) => {
    try {
        const body = ctx.request.body;
        console.log(body)
        const data = await ctx.models.Alarm.getAlarmsDuplicateCount(body);
        if(data.count==0){
            const createdData = await ctx.models.Alarm.create({
                ...body,
            }, {
                include: [{
                    model: ctx.models.Sensor,
                    as: 'sensor'
                }]
            });
            ctx.data = await createdData.reload();
        }else{
            ctx.throw(400, '0601');
        }

    } catch (e) {
        ctx.throw(400, '0218');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
    const io = ctx.utils.io();
    if (io) io.emit(`${ctx.data.sensor.position === 'in' ? 'inner' : 'outer'}-alarm`);
};

module.exports = post;
