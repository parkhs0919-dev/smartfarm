let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.isInt('cycle');
    ctx.check.isInt('use_start_hour');
    ctx.check.isInt('use_end_hour');
    ctx.check.isInt('no_start_hour');
    ctx.check.isInt('no_end_hour');
    ctx.check.isInt('repeat');

    ctx.check.isBoolean('is_bar');
    ctx.check.isBoolean('is_sound');
    ctx.check.isBoolean('is_image');
    ctx.check.isBoolean('is_use');
    ctx.check.isBoolean('is_no');

    ctx.sanitize.toBoolean('is_bar');
    ctx.sanitize.toBoolean('is_sound');
    ctx.sanitize.toBoolean('is_image');
    ctx.sanitize.toBoolean('is_use');
    ctx.sanitize.toBoolean('is_no');
};

put.getAlarm = async (ctx, next) => {
    try {
        ctx.alarm = await ctx.models.Alarm.findByPk(ctx.params.id, {
            include: [{
                model: ctx.models.Sensor,
                as: 'sensor'
            }]
        });
        if (!ctx.alarm) {
            ctx.throw(404, '0217');
        }
    } catch (e) {
        ctx.throw(404, '0217');
    }
};

put.updateAlarm = async (ctx, next) => {
    try {
        await ctx.models.Alarm.update(ctx.request.body, {
            where: {
                id: ctx.params.id
            }
        });
    } catch (e) {
        //console.log(e);
        ctx.throw(400, '0224');
    }
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit(`${ctx.alarm.sensor.position === 'in' ? 'inner' : 'outer'}-alarm`);
};

module.exports = put;
