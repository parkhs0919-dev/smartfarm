const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');
const post = require('./post');
const put = require('./put');
const del = require('./del');

const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'sensor_id',
                'condition',
                'offset',
                'size',
                'position'
            ],
            essential: [],
            explains: {
                'house_id': 'house id',
                'sensor_id': 'sensor id',
                'condition': '조건 (over, under)',
                'offset': 'offset',
                'size': 'size',
                'position': 'position (in, out)'
            },
            title: '경보설정 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getAlarms(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'sensor_id',
                'condition',
                'value'
            ],
            essential: [
                'sensor_id',
                'condition',
                'value'
            ],
            explains: {
                'sensor_id': 'sensor id',
                'condition': '조건 (over, under, equal)',
                'value': '값'
            },
            title: '경보설정 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
    
                await post.validate(ctx, next);
                await post.createAlarm(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'is_bar',
                'is_sound',
                'is_image',
                'is_use',
                'is_no'
            ],
            essential: [
                'is_bar',
                'is_sound',
                'is_image',
                'is_use',
                'is_no'
            ],
            params: ['id'],
            explains: {
                'id': 'alarm id',
                'is_bar': 'bar (true / false)',
                'is_sound': 'sound (true / false)',
                'is_image': 'image (true / false)',
                'is_use': 'use (true / false)',
                'is_no': 'no (true / false)'
            },
            title: '경보 알림설정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.getAlarm(ctx, next);
                await put.updateAlarm(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'alarm id'
            },
            title: '경보설정 삭제',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.getAlarm(ctx, next);
                await del.deleteAlarm(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.gets());
router.post('/', api.post());
router.put('/:id', api.put());
router.del('/:id', api.del());

module.exports.router = router;
module.exports.api = api;
