let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isInt('sensor_id');
    ctx.check.isEnum('condition', ['over', 'under']);
    ctx.check.isInt('size');
    ctx.check.isInt('offset');
    ctx.check.isEnum('position', ['in', 'out']);

    // if (!ctx.query.size) ctx.query.size = ctx.meta.std.defaultSize;
    if (!ctx.query.offset) ctx.query.offset = 0;

    ctx.data = {};
};

gets.getAlarms = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.Alarm.getAlarms(ctx.query);
    } catch (e) {
        ctx.throw(404, '0217');
    }
};

gets.supplement = async (ctx, next) => {
    if (!ctx.data.count) {
        ctx.throw(404);
    }
    ctx.json(ctx.data);
};

module.exports = gets;
