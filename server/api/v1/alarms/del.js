let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');

    ctx.data = {};
};

del.getAlarm = async (ctx, next) => {
    try {
        ctx.alarm = await ctx.models.Alarm.findByPk(ctx.params.id, {
            include: [{
                model: ctx.models.Sensor,
                as: 'sensor'
            }]
        });
        if (!ctx.alarm) {
            ctx.throw(404, '0217');
        }
    } catch (e) {
        ctx.throw(404, '0217');
    }
};

del.deleteAlarm = async (ctx, next) => {
    try {
        await ctx.models.Alarm.destroy({
            where: {
                id: ctx.params.id
            }
        });
    } catch (e) {
        ctx.throw(400, '0219');
    }
};

del.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit(`${ctx.alarm.sensor.position === 'in' ? 'inner' : 'outer'}-alarm`);
};

module.exports = del;
