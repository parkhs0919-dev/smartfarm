let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');

    ctx.data = {};
};

del.getReport = async (ctx, next) => {
    try {
        const report = await ctx.models.Report.findByPk(ctx.params.id);
        if (!report) {
            ctx.throw(404, '0104');
        }
        ctx.house_id = report.house_id;
    } catch (e) {
        ctx.throw(404, '0104');
    }
};

del.deleteReport = async (ctx, next) => {
    try {
        await ctx.models.Report.destroy({
            where: {
                id: ctx.params.id,
                type: 'report'
            }
        });
    } catch (e) {
        ctx.throw(400, '0208');
    }
};

del.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit('report', {
        house_id: ctx.house_id
    });
};

module.exports = del;
