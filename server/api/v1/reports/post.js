let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');

    ctx.data = {};
};

post.createReport = async (ctx, next) => {
    try {
        const body = ctx.request.body;
        ctx.data = await ctx.models.Report.create({
            user_id: ctx.user.id,
            ...body,
            type: 'report',
        });
    } catch (e) {
        ctx.throw(400, '0208');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
    const io = ctx.utils.io();
    if (io) io.emit('report', {
        house_id: ctx.request.body.house_id
    });
};

module.exports = post;
