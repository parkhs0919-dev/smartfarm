let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isInt('size');
    ctx.check.isInt('offset');

    if (!ctx.query.size) ctx.query.size = ctx.meta.std.defaultSize;
    if (!ctx.query.offset) ctx.query.offset = 0;
    if (ctx.query.type) ctx.query.type = ctx.query.type.split(',');

    ctx.data = {};
};

gets.getReports = async (ctx, next) => {
    try {       
        if(ctx.user && ctx.user.grade=='member'){ 
            ctx.query.user = ctx.user;
            ctx.data = await ctx.models.Report.getReportGrade(ctx.query);
        }else{
            ctx.data = await ctx.models.Report.getReports(ctx.query);
        }
    } catch (e) {
        ctx.throw(400, '0101');
    }
};

gets.supplement = async (ctx, next) => {
    if (!ctx.data.count) {
        ctx.throw(404);
    }
    ctx.json(ctx.data);
};

module.exports = gets;
