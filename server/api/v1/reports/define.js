const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');
const post = require('./post');
const del = require('./del');

const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'type',
                'offset',
                'size'
            ],
            essential: [],
            explains: {
                'house_id': '하우스 id',
                'type': '유형 (motor, power, mode, warning, error, report, auto, noti) (,)로 구분',
                'offset': 'offset',
                'size': 'size'
            },
            title: '일지 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getReports(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'contents'
            ],
            essential: [
                'house_id',
                'contents'
            ],
            explains: {
                'house_id': 'house_id',
                'contents': 'contents'
            },
            title: '일지 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.createReport(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'report id'
            },
            title: '일지 삭제',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.getReport(ctx, next);
                await del.deleteReport(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.gets());
router.post('/', api.post());
router.del('/:id', api.del());

module.exports.router = router;
module.exports.api = api;
