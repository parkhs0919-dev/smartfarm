let get = {};
const REFRESH = require('../../../methods/refresh');

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.data = null;
};

get.getCurrent = async (ctx, next) => {
    ctx.data = {
        current: REFRESH.getCurrent()
    };
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
