const Router = require('koa-router');
const router = new Router();

const get = require('./get');
const post = require('./post');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['house_id'],
            explains: {
                'house_id': 'house id'
            },
            title: '메인 차트 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.getSensorMainChart(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'sensor_id'
            ],
            essential: [
                'house_id',
                'sensor_id'
            ],
            explains: {
                'house_id': 'house id',
                'sensor_id': 'sensor id'
            },
            title: '메인 차트 센서 설정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.check(ctx, next);
                await post.createSensorMainChart(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:house_id', api.get());
router.post('/', api.post());

module.exports.router = router;
module.exports.api = api;
