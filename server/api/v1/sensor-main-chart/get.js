let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');

    ctx.data = null;
};

get.getSensorMainChart = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.Sensor.findOne({
            include: [{
                model: ctx.models.SensorMainChart,
                as: 'sensorMainCharts',
                where: {
                    house_id: ctx.params.house_id
                },
                required: true,
            }]
        });
    } catch (e) {
        ctx.throw(400, '0222');
    }
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
