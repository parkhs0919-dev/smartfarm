let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isInt('sensor_id');
};

post.check = async (ctx, next) => {
    try {
        const sensor = await ctx.models.Sensor.findOne({
            include: [{
                model: ctx.models.HouseSensorRel,
                as: 'houseSensorRels',
                where: {
                    house_id: ctx.request.body.house_id
                },
                required: true
            }],
            where: {
                id: ctx.request.body.sensor_id,
                position: 'in'
            }
        });
        if (!sensor) {
            ctx.throw(404, '0101');
        }
    } catch (e) {
        ctx.throw(404, '0101');
    }
};

post.createSensorMainChart = async (ctx, next) => {
    try {
        ctx.sensorMainChart = await ctx.models.SensorMainChart.createSensorMainChart(ctx.request.body);
    } catch (e) {
        ctx.throw(400, '0223');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit('sensor-main-chart', {
        house_id: ctx.request.body.house_id
    });
};

module.exports = post;
