const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');

const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'sensor_id',
                'control_id',
                'type',
                'startHour',
                'startMinute',
                'endHour',
                'endMinute',
                'isDate',
                'isWeek',
                'isMonth',
            ],
            essential: [],
            params: [],
            explains: {
                'id': 'id',
                'name': 'name',
                'type' : 'type',
                'unit' : 'unit'
            },
            title: '센서 차트 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.generateDate(ctx, next);
                await gets.getChart(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
};

router.get('/', api.gets());

module.exports.router = router;
module.exports.api = api;
