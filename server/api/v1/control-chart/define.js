const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');

const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'startDate',
                'endDate',
                'isDate',
                'isWeek',
                'isMonth'
            ],
            essential: [],
            params: ['control_id'],
            explains: {
                'control_id': 'control id',
                'startDate': 'start date (yyyy-MM-dd)',
                'endDate': 'end date (yyyy-MM-dd)',
                'isDate': '1일 (true / false)',
                'isWeek': '1주 (true / false)',
                'isMonth': '1달 (true / false)'
            },
            title: '센서 차트 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.generateDate(ctx, next);
                await gets.getChart(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
};

router.get('/:control_id', api.gets());

module.exports.router = router;
module.exports.api = api;
