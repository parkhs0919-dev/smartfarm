let gets = {};
const dateExp = new RegExp('^[0-9]{4}-[0-9]{2}-[0-9]{2}$');

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('control_id');
    ctx.check.isRegExp('startDate', dateExp);
    ctx.check.isRegExp('endDate', dateExp);
    ctx.check.isBoolean('isDate');
    ctx.check.isBoolean('isWeek');
    ctx.check.isBoolean('isMonth');
    ctx.sanitize.toBoolean('isDate');
    ctx.sanitize.toBoolean('isWeek');
    ctx.sanitize.toBoolean('isMonth');

    ctx.data = null;
};

gets.generateDate = async (ctx, next) => {
    if (ctx.query.isDate) {
        const now = new Date().getTime();
        ctx.startDate = ctx.utils.filter.date(now - 86400000, 'yyyy-MM-dd HH:mm:ss');
        ctx.endDate = ctx.utils.filter.date(now, 'yyyy-MM-dd HH:mm:ss');
    } else if (ctx.query.isWeek) {
        const now = new Date().getTime();
        ctx.startDate = ctx.utils.filter.date(now - 604800000, 'yyyy-MM-dd HH:mm:ss');
        ctx.endDate = ctx.utils.filter.date(now, 'yyyy-MM-dd HH:mm:ss');
    } else if (ctx.query.isMonth) {
        const now = new Date().getTime();
        ctx.startDate = ctx.utils.filter.date(now - 2592000000, 'yyyy-MM-dd HH:mm:ss');
        ctx.endDate = ctx.utils.filter.date(now, 'yyyy-MM-dd HH:mm:ss');
    } else if (ctx.query.startDate && ctx.query.endDate) {
        ctx.query.isDate = ctx.query.startDate === ctx.query.endDate;
        const startTime = new Date(`${ctx.query.startDate} 00:00:00`).getTime();
        const endTime = new Date(`${ctx.query.endDate} 00:00:00`).getTime();
        ctx.startDate = ctx.utils.filter.date(startTime, 'yyyy-MM-dd HH:mm:ss');
        ctx.endDate = ctx.utils.filter.date(endTime + 86400000, 'yyyy-MM-dd HH:mm:ss');
    }
};

gets.getChart = async (ctx, next) => {
    if (ctx.startDate && ctx.endDate) {
        try {
            ctx.data = await ctx.models.Control.getChart(ctx.params.control_id, ctx.startDate, ctx.endDate, ctx.query.isDate);
        } catch (e) {
            ctx.throw(400, '0220');
        }
    }
};

gets.supplement = async (ctx, next) => {
    if (ctx.data) {
        ctx.json(ctx.data);
    } else {
        ctx.throw(400, '0220');
    }
};

module.exports = gets;
