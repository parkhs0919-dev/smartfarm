const CONFIG = require('../../../config');
let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
};

del.logout = async (ctx, next) => {
    try {
        ctx.cookies.set(CONFIG.jwt.cookie, '');
    } catch (e) {
        ctx.throw(400, '0402');
    }
};

del.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = del;
