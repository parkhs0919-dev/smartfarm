const Router = require('koa-router');
const router = new Router();

const del = require('./del');

const api = {
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: [],
            explains: {},
            title: '로그아웃',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.logout(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.del('/', api.del());

module.exports.router = router;
module.exports.api = api;
