const Router = require('koa-router');
const router = new Router();

const get = require('./get');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'platform'
            ],
            essential: [
                'platform'
            ],
            explains: {
                'platform': 'platform (farm, fluid, animal)'
            },
            title: 'ping',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.ping(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.get());

module.exports.router = router;
module.exports.api = api;
