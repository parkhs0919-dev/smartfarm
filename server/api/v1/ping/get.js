let get = {};
const CONFIG = require('../../../config');

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isEnum('platform', ['farm', 'fluid', 'animal']);

    ctx.data = null;
};

get.ping = async (ctx, next) => {
    let level = 0;
    try {
        const start = new Date().getTime();
        await ctx.utils.erp.ping(ctx.query.platform);
        const end = new Date().getTime();

        const time = parseInt((end - start) / 2);

        if (time < CONFIG.erp.goodResTime) {
            level = 3;
        } else if (time < CONFIG.erp.badResTime) {
            level = 2;
        } else {
            level = 1;
        }
    } catch (e) {}

    ctx.data = {level};
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
