let puts = {};

puts.validate = async (ctx, next) => {
    if (ctx.request.body.value === undefined) {
        ctx.throw(400, '0254');
    }

    ctx.check.isFloat('value');
    ctx.check.len('out_key', 1, 191);
};

puts.updateSensorValue = async (ctx, next) => {
    try {
        const sensor = await ctx.models.Sensor.findOne({
            where: {
                out_key: ctx.request.body.out_key
            }
        });
        if (sensor) {
            ctx.utils.sensor.setSensorValueById(parseFloat(ctx.request.body.value), sensor.id);
            await ctx.models.Sensor.update({
                value: parseFloat(ctx.request.body.value) + parseFloat(sensor.revision)
            }, {
                where: {
                    id: sensor.id
                }
            });
        } else {
            ctx.throw(400, '0254');
        }
    } catch (e) {
        ctx.throw(400, '0254');
    }
};

puts.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = puts;
