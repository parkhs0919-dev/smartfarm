const Sequelize = require('sequelize');
const sequelize = require('../../../methods/sequelize').sequelize;
  
let gets = {};  

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    } 
    ctx.data = {};
};

gets.getSensors = async (ctx, next) => {
    try {
        let selectQuery=` select * from sensors where type='power' and out_key is not null `;
        selectQuery+=` and  address='${ctx.params.ip}' `;
        const rows = await sequelize.query(selectQuery, {  
            type: Sequelize.QueryTypes.SELECT 
        });        
        ctx.data=rows;        
    } catch (e) {            
        ctx.throw(400, '0254');    
    }    
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};
  
module.exports = gets;
