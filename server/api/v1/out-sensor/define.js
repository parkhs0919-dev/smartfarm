const Router = require('koa-router');
const router = new Router();

const put = require('./put');
const gets = require('./gets');

const api = {
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'out_key',
                'value'
            ],
            essential: [
                'out_key',
                'value'
            ],
            params: [],
            explains: {
                'out_key': 'out_key',
                'value': '값 double(8, 2)'
            },
            title: '외부 센서값 입력',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.updateSensorValue(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    device: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'sensor_id',
                'position',
                'type',
                'sensor_name',
                'address',
                'out_key'
            ],
            essential: [],
            explains: {
                'sensor_id': '센서 ID',
                'position': '위치 (in, out)',
                'type': '유형',
                'sensor_name': '명칭',
                'address': 'address',
                'out_key': 'out_key'  
            }, 
            title: '센서 설정값 조회 ',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getSensors(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {  
                ctx.error(e.message, e.status);
            }
        } else {
            return params;  
        }
    }    
};

router.put('/', api.put());
router.get('/device/:ip', api.device());
 
module.exports.router = router;
module.exports.api = api;
   