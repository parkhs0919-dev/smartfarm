let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('sensor_id');

    ctx.data = null;
};

get.getSensorInfo = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.SensorInfo.findOne({
            where: {
                sensor_id: ctx.params.sensor_id
            }
        });
    } catch (e) {
        ctx.throw(400, '0222');
    }
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
