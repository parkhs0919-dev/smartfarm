let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
};

gets.getSensors = async (ctx, next) => {
    try {
        let include = [{
            model: ctx.models.SensorInfo,
            as: 'sensorInfo'
        }];
        if (ctx.query.house_id) {
            include.push({
                model: ctx.models.HouseSensorRel,
                as: 'houseSensorRels',
                required: true,
                where: {
                    house_id: ctx.query.house_id
                }
            });
        }
        const rows = await ctx.models.Sensor.findAll({
            include,
            where: {
                position: 'in',
                type: 'temperature'
            }
        });
        if (rows.length) {
            ctx.data = {
                count: rows.length,
                rows
            };
        } else {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(404, '0101');
    }
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
