const Router = require('koa-router');
const router = new Router();

const get = require('./get');
const gets = require('./gets');
const post = require('./post');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['sensor_id'],
            explains: {
                'sensor_id': 'sensor id'
            },
            title: '적산온도 정보 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.getSensorInfo(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id'
            ],
            essential: [],
            explains: {},
            title: '적산온도 정보 포함 센서 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getSensors(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'min_value',
                'start_date',
                'end_date'
            ],
            essential: [
                'start_date',
                'end_date'
            ],
            params: ['sensor_id'],
            explains: {
                'sensor_id': 'sensor id',
                'min_value': '최소 온도 값',
                'start_date': '작기 시작일 (yyyy-MM-dd)',
                'end_date': '작기 끝일 (yyyy-MM-dd)'
            },
            title: '적산온도 정보 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.createSensorInfo(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:sensor_id', api.get());
router.get('/', api.gets());
router.post('/:sensor_id', api.post());

module.exports.router = router;
module.exports.api = api;
