let post = {};

const dateExp = new RegExp('^[0-9]{4}-[0-9]{2}-[0-9]{2}$');

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('sensor_id');
    ctx.check.isFloat('min_value');
    ctx.check.isRegExp('start_date', dateExp);
    ctx.check.isRegExp('end_date', dateExp);

    let body = ctx.request.body;
    if (!body.start_date) {
        ctx.throw(400, '0293');
    }
    if (!body.end_date) {
        ctx.throw(400, '0294');
    }
    if (body.start_date > body.end_date) {
        ctx.throw(400, '0295');
    }
};

post.createSensorInfo = async (ctx, next) => {
    try {
        await ctx.models.SensorInfo.createSensorInfo(ctx.params.sensor_id, ctx.request.body);
    } catch (e) {
        ctx.throw(400, '0227');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit('sensor-info');
};

module.exports = post;
