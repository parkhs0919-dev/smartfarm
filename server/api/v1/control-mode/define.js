const Router = require('koa-router');
const router = new Router();

const put = require('./put');

const api = {
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'mode'
            ],
            essential: [
                'mode'
            ],
            params: ['id'],
            explains: {
                'id': 'control id',
                'mode': '상태 (manual, auto)'
            },
            title: '기기별 자동제어 설정 변경',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.findControl(ctx, next);
                await put.updateControl(ctx, next);
                await put.activeControl(ctx, next);
                await put.createReport(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.put('/:id', api.put());

module.exports.router = router;
module.exports.api = api;
