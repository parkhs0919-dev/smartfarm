let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.isEnum('mode', ['manual', 'auto']);
};

put.findControl = async (ctx, next) => {
    try {
        ctx.control = await ctx.models.Control.findById(ctx.params.id);
        if (!ctx.control) {
            ctx.throw(404);
        } else if (ctx.control.type === 'mode') {
            ctx.throw(400, '0214');
        }
    } catch (e) {
        ctx.throw(400, '0103');
    }
};

put.updateControl = async (ctx, next) => {
    try {
        await ctx.models.Control.update({
            mode: ctx.request.body.mode
        }, {
            where: {
                id: ctx.params.id
            }
        });
    } catch (e) {
        ctx.throw(400, '0212');
    }
};

put.activeControl = async (ctx, next) => {
    const id = ctx.params.id;
    if (ctx.control.type === 'motor' && ctx.request.body.mode === 'auto') {
        const type = 'manual';
        const state = 'stop';
        const user_id = ctx.user.id;
        const result = await ctx.utils.control.activeMotor({id, state, type, user_id});
        if (!result) {
            try {
                ctx.control = await ctx.models.Control.findById(id);
                const io = ctx.utils.io();
                if (io) io.emit(ctx.control.type, {
                    motor: ctx.control
                });
            } catch (e) {
                ctx.throw(400, '0213');
            }
        }
    } else {
        try {
            ctx.control = await ctx.models.Control.findById(id);
            const io = ctx.utils.io();
            let temp = {};
            temp[ctx.control.type] = ctx.control;
            if (io) io.emit(ctx.control.type, temp);
        } catch (e) {
            ctx.throw(400, '0213');
        }
    }
};

put.createReport = async (ctx, next) => {
    try {
        await ctx.models.Report.create({
            user_id: ctx.user.id,
            house_id: ctx.control.house_id,
            control_id: ctx.params.id,
            type: 'auto',
            contents: `'${ctx.control.control_name}' - '${ctx.request.body.mode === 'manual' ? '수동제어' : '자동제어'}' 모드가 실행되었습니다.`
        });
        const io = ctx.utils.io();
        if (io) io.emit('report', {
            house_id: ctx.control.house_id
        });
    } catch (e) {}
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = put;
