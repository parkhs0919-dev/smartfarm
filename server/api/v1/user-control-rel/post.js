let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.len('uid', 1, 191);
};   

post.getUserCheck = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.UserControlRel.getUserCheck(ctx.request.body);          
    } catch (e) {        
        ctx.throw(400, '0615');
    }   
};

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};  

module.exports = post;
