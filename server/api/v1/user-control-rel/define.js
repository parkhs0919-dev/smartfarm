const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');
const post = require('./post');
  
const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                `id`,
                `type`, 
                `control_name`            
            ],
            essential: [],
            explains: {
                'id': 'id',
                'type': 'type',
                'control_name': 'control_name'                
            },
            title: '유저 제어 조회',  
            state: 'develop'
        };

        if (!isParam) {
            try {                
                await gets.validate(ctx, next);
                await gets.getUserControl(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx, next) => {
        const params = {     
            acceptable: [
                `uid`
            ],
            essential: [
                `uid`
            ],
            explains: {
                'uid': 'uid  '
            },
            title: '유저 중복 확인',
            state: 'develop'
        };

        if (!isParam) {
            try {                
                await post.validate(ctx, next);
                await post.getUserCheck(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {   
                ctx.error(e.message, e.status);
            }   
        } else {
            return params;
        }
    }, 
};

router.get('/', api.gets());  
router.post('/', api.post());

module.exports.router = router;
module.exports.api = api;
