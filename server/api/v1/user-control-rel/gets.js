let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isEnum('type', ['motor', 'power', 'control']);

    ctx.data = {};
};  

gets.getUserControl = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.UserControlRel.getUserControl(ctx.query);
    } catch (e) {   
        ctx.throw(400, '0614');     
    }   
};
   
gets.supplement = async (ctx, next) => {
    if (!ctx.data.count) {
        ctx.throw(404);
    }
    ctx.json(ctx.data);
};

module.exports = gets;
