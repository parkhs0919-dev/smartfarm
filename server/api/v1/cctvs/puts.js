let puts = {};

puts.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    if (ctx.request.body.cctvs instanceof Array && ctx.request.body.cctvs.length) {
        for (let i=0; i<ctx.request.body.cctvs.length; i++) {
            const cctv = ctx.request.body.cctvs[i];
            if (!cctv.id || !cctv.cctv_name) {
                ctx.throw(400, '0225');
            }
        }
    } else {
        ctx.throw(400, '0225');
    }
};

puts.updateCctvs = async (ctx, next) => {
    try {
        await ctx.models.Cctv.updateCctvs(ctx.request.body.cctvs);
    } catch (e) {
        ctx.throw(400, '0226');
    }
};

puts.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit('cctvs');
};

module.exports = puts;
