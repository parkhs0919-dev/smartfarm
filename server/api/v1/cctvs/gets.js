let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isEnum('type', ['motor', 'power']);
    ctx.check.isEnum('state', ['open', 'stop', 'close', 'on', 'off']);
    ctx.check.isInt('size');
    ctx.check.isInt('offset');

    // if (!ctx.query.size) ctx.query.size = ctx.meta.std.defaultCctvSize;
    if (!ctx.query.offset) ctx.query.offset = 0;

    ctx.data = {};
};

gets.getCctvs = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.Cctv.getCctvs(ctx.query);
    } catch (e) {
        ctx.throw(400, '0102');
    }
};

gets.supplement = async (ctx, next) => {
    if (!ctx.data.count) {
        ctx.throw(404);
    }
    ctx.json(ctx.data);
};

module.exports = gets;
