const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');
const puts = require('./puts');

const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'offset',
                'size'
            ],
            essential: [],
            explains: {
                'house_id': '하우스 ID',
                'offset': 'offset',
                'size': 'size'
            },
            title: 'cctv 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getCctvs(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    puts: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'cctvs'
            ],
            essential: [
                'cctvs'
            ],
            explains: {
                'cctvs': 'cctv id, cctv_name object array ex) cctvs: [{"id": 1, "cctv_name": "cctv"}]'
            },
            title: 'cctv 명칭 변경',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await puts.validate(ctx, next);
                await puts.updateCctvs(ctx, next);
                await puts.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.gets());
router.put('/', api.puts());

module.exports.router = router;
module.exports.api = api;
