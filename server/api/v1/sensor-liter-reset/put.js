let put = {};
let sensor_path='';
const CONFIG = require('../../../config');
 
if(CONFIG.serialPort.UseNewSensorType==2) sensor_path='../../../cron/new-sensor-value';
else sensor_path='../../../cron/set-new-sensor-value';
       
let pushOtherWork = require(sensor_path).pushOtherWork;

put.validate = async (ctx, next) => {
    if (!ctx.user) {     
        ctx.throw(401, '0401');     
    }   
    ctx.check.isInt('id'); 
};

put.findSensor = async (ctx, next) => {
    try {
        
        ctx.sensor = await ctx.models.Sensor.findOne({
            where: {
                id: ctx.params.id,
                type: 'liter'
            }
        });
        
        if (!ctx.sensor) {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(404, '0101');
    }
};

put.resetLiterSensor = async (ctx, next) => {
    try {
        if (ctx.sensor.address) {            
            await pushOtherWork({
                type: 'resetLiter',
                address: ctx.sensor.address
            });
        } else {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(400, '0268');
    }
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = put;
