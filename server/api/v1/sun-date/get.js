let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isEnum('platform', ['farm', 'fluid', 'animal']);

    ctx.data = null;
};

get.getSunDate = async (ctx, next) => {
    try {
        const result = await ctx.models.SunDate.findOne({
            where: {
                date: ctx.utils.filter.date(new Date(), 'yyyy-MM-dd')
            }
        });
        if (result) {
            ctx.data = result;
        }
    } catch (e) {
        ctx.throw(400, '0221');
    }
};

get.getSunDates = async (ctx, next) => {
    try {
        if (!ctx.data) {
            const result = await ctx.utils.erp.getSunDates(ctx.query.platform);
            await ctx.models.SunDate.bulkCreate(result, {
                ignoreDuplicates: true
            });
        }
    } catch (e) {
        ctx.throw(400, '0221');
    }
};

get.getSunDateDefault = async ctx => {
    try {
        if (!ctx.data) {
            ctx.data = await ctx.models.SunDateDefault.findOne({});
        }
    } catch (e) {
        ctx.throw(400, '');
    }
};

get.supplement = async (ctx, next) => {
    if (ctx.data) {
        ctx.json(ctx.data);
    } else {
        ctx.throw(404, '0221');
    }
};

module.exports = get;
