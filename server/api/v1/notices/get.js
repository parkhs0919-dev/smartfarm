let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isEnum('platform', ['farm', 'fluid', 'animal']);

    ctx.data = null;
};

get.getRecentNotice = async (ctx, next) => {
    try {
        ctx.data = await ctx.utils.erp.getRecentNotice(ctx.query.platform,ctx.query);
    } catch (e) {
        ctx.throw(400, '0215');
    }
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
