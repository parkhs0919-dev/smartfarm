let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isEnum('code', ['forward', 'backward', 'stop']);

    ctx.data = null;
};

get.getSoundControl = async (ctx, next) => {
    try {
        if(ctx.query.code == "forward" || ctx.query.code == "backward" || ctx.query.code == "stop"){      
            const control= await ctx.models.VoiceControl.getVoiceControl(ctx.query);
            if(control){   
                try {     
                    await ctx.utils.control.activeVoiceControl(control);
                    ctx.data = 'SUCCESS';
                } catch (e) {  
                    const io = ctx.utils.io();
                    if (io) io.emit('report', {
                        house_id: ctx.control.house_id
                    });
                    ctx.throw(400, '0210');
                }   
            }
        }         
        
    } catch (e) {
        ctx.throw(400, '0540');
    }
};

get.supplement = async (ctx, next) => {
    if (ctx.data) {
        ctx.json(ctx.data);
    } else {
        ctx.throw(404, '0540');
    }  
};

function makeControlWorkList(user_id, control, state){       
    controlWorkList[control.address+"_"+control.id]={
        buffer1Crc :false
    };       
    controlWorkListData[control.address+"_"+control.id] = {     
        bitmask : getHex(setBitMask(control)),   
        mode : setMode(state),    
        ontime : '0000',                      
        offtime : '0000',                       
        address :control.address,
        ischeck : false    
    }     
    //console.log(setBitMask(control)) 
    //console.log(getHex(setBitMask(control)))
    console.log("sound_order",controlWorkListData); 
}      
function setMode(state){    
    let result='';
    if(state=='start') result='03';
    else if(state=='stop') result='02';
    return result;
}

function setBitMask(data){                
    let result=['0', '0', '0', '0', '0', '0', '0', '0'];         
    
    if(data.io1==1){
        result.splice(7, 1, '1');  
    }else if(data.io1==2){   
        result.splice(6, 1, '1');
    }else if(data.io1==3){
        result.splice(6, 2, '1','1');
    }    
           
    return result.join('');                               
}    

function getHex(data)
{   
    let str = parseInt(data, 2).toString(16);
    if(str.length==1){
       str='0'+str;   
    }
    return str;  
}  

module.exports = get;
