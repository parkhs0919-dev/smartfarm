const Router = require('koa-router');
const router = new Router();

const get = require('./get');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'platform'
            ],
            essential: [
                'platform'
            ],
            params: ['sensor_id'],
            explains: {
                'sensor_id': 'sensor id',
                'platform': 'platform (farm, fluid, animal)'
            },
            title: '온도 센서 상세 정보',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.findSensor(ctx, next);
                await get.getAvg(ctx, next);
                await get.getSunAvg(ctx, next);
                await get.getSum(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:sensor_id', api.get());

module.exports.router = router;
module.exports.api = api;
