let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('sensor_id');
    ctx.check.isEnum('platform', ['farm', 'fluid', 'animal']);

    ctx.data = {};
};

get.findSensor = async (ctx, next) => {
    try {
        ctx.sensor = await ctx.models.Sensor.findOne({
            include: [{
                model: ctx.models.SensorInfo,
                as: 'sensorInfo'
            }],
            where: {
                id: ctx.params.sensor_id,
                position: 'in',
                type: 'temperature'
            }
        });
        if (!ctx.sensor) {
            ctx.throw(404, '0101');
        }
        ctx.data.sensor = ctx.sensor;
        ctx.yesterday = ctx.utils.filter.date(new Date().getTime() - 86400000, 'yyyy-MM-dd');
    } catch (e) {
        ctx.throw(400, '0101');
    }
};

get.getAvg = async (ctx, next) => {
    try {
        ctx.data.avg = await ctx.models.Sensor.getAvg(ctx.params.sensor_id);
    } catch (e) {
        ctx.throw(400, '0263');
    }
};

get.getSunAvg = async (ctx, next) => {
    try {
        let sunDate = await ctx.models.SunDate.findOne({
            where: {
                date: ctx.yesterday
            }
        });
        if (!sunDate) {
            const result = await ctx.utils.erp.getSunDates(ctx.query.platform);
            await ctx.models.SunDate.bulkCreate(result, {
                ignoreDuplicates: true
            });
            sunDate = await ctx.models.SunDate.findOne({
                where: {
                    date: ctx.yesterday
                }
            });
        }
        if (sunDate) {
            ctx.data.sunAvg = await ctx.models.Sensor.getSunAvg(ctx.params.sensor_id, sunDate);
        } else {
            ctx.data.sunAvg = null;
        }
    } catch (e) {
        ctx.throw(400, '0261');
    }
};

get.getSum = async (ctx, next) => {
    try {
        if (ctx.sensor.sensorInfo) {
            ctx.data.sum = await ctx.models.Sensor.getSum(ctx.sensor.sensorInfo);
        } else {
            ctx.data.sum = null;
        }
    } catch (e) {
        ctx.throw(400, '0262')
    }
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
