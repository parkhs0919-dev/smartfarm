let gets = {};
const CONFIG = require('../../../config');

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isEnum('platform', ['farm', 'fluid', 'animal']);

    ctx.data = null;
};

gets.getRecommendAutoControl = async (ctx, next) => {
    try {
        ctx.data = await ctx.utils.erp.getAutoControl(ctx.query.platform, ctx.query.crop_type);
    } catch (e) {
        ctx.throw(404, '0290');
    }
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
