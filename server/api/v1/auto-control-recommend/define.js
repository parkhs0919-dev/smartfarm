const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');

const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'platform',
                'crop_name'
            ],
            essential: [
                'platform'
            ],
            explains: {
                'platform': 'platform (farm, fluid, animal)'
            },
            title: 'ping',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getRecommendAutoControl(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.gets());

module.exports.router = router;
module.exports.api = api;
