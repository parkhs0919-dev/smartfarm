let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};

del.deleteAnimalWeight = async (ctx, next) => {
  try {
       ctx.data = await ctx.models.AnimalWeight.deleteAnimalWeight(ctx.params.id);
  } catch(e) {
      ctx.throw(400, '0508');     
  }
};   

del.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = del;
