let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
};
  
post.createAnimalWeight = async (ctx, next) => {
  try {
      ctx.data = await ctx.models.AnimalWeight.createAnimalWeight(ctx.request.body);
  } catch (e) {
      ctx.throw(400, '0506');
  }
};   

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = post;
