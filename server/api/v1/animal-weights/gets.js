let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};
gets.getAnimalWeight = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.AnimalWeight.getAnimalWeight(ctx.params.id,ctx.query);
    } catch (e) {
        ctx.throw(400, '0507');
    }
};   

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
