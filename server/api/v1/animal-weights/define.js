const Router = require('koa-router');
const router = new Router();

const post = require('./post');
const gets = require('./gets');
const del = require('./del');

const api = {
    post: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_id',
            'date',
            'weight',
            'memo'
          ],
          essential: [
            'animal_id',
            'date',
            'weight',
            'memo'
          ],
          explains: {
            'animal_id' : 'animal_id',
            'date': 'date',
            'weight':'weight',
            'memo': 'memo'
          },
          title: '체중입력',
          state: 'develop'
      };

      if (!isParam) {
          try {
              //await post.validate(ctx, next);
              await post.createAnimalWeight(ctx, next);
              await post.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }
    },
    gets: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_id',
            'date',
            'weight',
            'memo'
          ],
          essential: [
            'animal_id',
            'date',
            'weight',
            'memo'
          ],
          explains: {
            'animal_id' : 'animal_id',
            'date': 'date',
            'weight':'date',
            'memo': 'memo'
          },
          title: '체중입력',
          state: 'develop'
      };

      if (!isParam) {
          try {
              //await post.validate(ctx, next);
              await gets.getAnimalWeight(ctx, next);
              await gets.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }  
    },
    del: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_id',
          ],
          essential: [
            'animal_id',
          ],
          explains: {
            'animal_id' : 'animal_id',
          },
          title: ' 삭제',
          state: 'develop'
        };

        if (!isParam) {
            try {
                //await del.validate(ctx, next);
                await del.deleteAnimalWeight(ctx, next);
                await del.supplement(ctx, next); 
            } catch (e) {
                ctx.error(e.message, e.status);  
            }  
        } else {
            return params;
        }
    }     
};

router.post('/', api.post());
router.get('/:id', api.gets());
router.delete('/:id', api.del());

module.exports.router = router;
module.exports.api = api;
