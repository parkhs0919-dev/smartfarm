let post = {};

post.validate = async (ctx, next) => {
    ctx.check.isInt('user_id');
    ctx.check.isEnum('platform', ['android', 'ios']);
    ctx.check.isEnum('type', ['fcm']);
    ctx.check.len('token', 1, 191);
};

post.createDeviceToken = async (ctx, next) => {
    try {
        await ctx.models.DeviceToken.createDeviceToken(ctx.request.body);
    } catch (e) {
        ctx.throw(400, '0264');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = post;
