const Router = require('koa-router');
const router = new Router();

const post = require('./post');

const api = {
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'user_id',
                'platform',
                'type',
                'token'
            ],
            essential: [
                'user_id',
                'platform',
                'type',
                'token'
            ],
            explains: {
                'user_id': '사용자 ID',
                'platform': 'platform (android, ios)',
                'type': 'type (fcm)',
                'token': 'device token'
            },
            title: 'device token 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.createDeviceToken(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
};

router.post('/', api.post());

module.exports.router = router;
module.exports.api = api;
