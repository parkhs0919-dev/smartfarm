let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};
gets.getAnimalType = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.AnimalType.getAnimalType(ctx.query);
    } catch (e) {
        ctx.throw(400, '0101');
    }
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};     

module.exports = gets;
