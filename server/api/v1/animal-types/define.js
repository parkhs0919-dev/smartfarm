const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');
const post = require('./post');
const put = require('./put');

const api = {
    gets: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_type_name',

          ],
          essential: [],
          explains: {
            'animal_type_name' : 'animal_type_name',
            'offset': 'offset',
            'size': 'size'
          },
          title: '개체 카테고리 조회',
          state: 'develop'
      };
       
      if (!isParam) {
          try {
              //await gets.validate(ctx, next);
              await gets.getAnimalType(ctx, next);
              await gets.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }
    },
    post: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_type_name'
          ],
          essential: [
            'animal_type_name'
          ],
          explains: {
            'animal_type_name' : 'animal_type_name'
          },
          title: '개체 카테고리 등록',
          state: 'develop'
      };

      if (!isParam) {
          try {
              //await post.validate(ctx, next);
              await post.createAnimalType(ctx, next);
              await post.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'animal_type_name'
            ],
            essential: [
                'animal_type_name'
            ],
            params: ['id'],
            explains: {
                'id': 'id',
                'animal_type_name': '개체 카테고리 명'
            },
            title: '개체 카테고리 변경',
            state: 'develop'
        };

        if (!isParam) {
            try {
                //await put.validate(ctx, next);
                await put.updateAnimalType(ctx, next);
                //await put.emitControl(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.gets());
router.post('/', api.post());
router.put('/:id', api.put());

module.exports.router = router;
module.exports.api = api;
