let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
  
};

put.updateAnimalType = async (ctx, next) => {
    try {
        await ctx.models.AnimalType.update({
            animal_type_name: ctx.request.body.animal_type_name
        }, {
            where: {
                id: ctx.params.id
            }
        });
    } catch (e) {
        ctx.throw(400, '0212');
    }
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = put;
