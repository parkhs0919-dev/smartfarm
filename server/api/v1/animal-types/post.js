let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isEnum('health_state', ['good', 'normal', 'bad']);
    ctx.check.isRegExp('birth_day', birth_day);
    ctx.check.isRegExp('move_day', move_day);
    ctx.data = {};
};

post.createAnimalType = async (ctx, next) => {
  try {
      ctx.data = await ctx.models.AnimalType.createAnimalType(ctx.request.body);
  } catch (e) {       
      ctx.throw(400, '0233');
  }
};
post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = post;
