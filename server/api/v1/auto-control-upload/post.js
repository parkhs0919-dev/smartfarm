let post = {};
const fs = require('fs');
const Upload = require('../../../middleware/upload');

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    if (ctx.request.files.length !== 1) {
        ctx.throw(400, '0108');
    }

    ctx.data = {};
};

post.checkHouseId = async (ctx, next) => {
    try {
        const house = await ctx.models.House.findByPk(ctx.params.house_id);
        if (!house) {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(404, '0109');
    }
};

post.checkFile = async (ctx, next) => {
    let autoControls = JSON.parse(fs.readFileSync(ctx.request.files[0].path).toString().trim());
    if (!checkAutoControls(autoControls, ctx.user.id, ctx.params.house_id)) {
        ctx.throw(400, '0267');
    }
    ctx.autoControls = autoControls;
    let hash = {};
    let controlIds = [];
    let controlStates = [];
    autoControls.forEach(autoControl => {
        autoControl.autoControlItems.forEach(autoControlItem => {
            if (!hash[autoControlItem.control_id]) {
                hash[autoControlItem.control_id] = true;
                controlIds.push(autoControlItem.control_id);
                controlStates.push(autoControlItem.state);
            }
        });
        if (autoControl.autoControlSteps) {
            autoControl.autoControlSteps.forEach(autoControlStep => {
                if (!hash[autoControlStep.control_id]) {
                    hash[autoControlStep.control_id] = true;
                    controlIds.push(autoControlStep.control_id);
                    controlStates.push(autoControlStep.state);
                }
            });
        }
    });
    try {
        await ctx.models.Control.checkControlStates(controlIds, controlStates);
    } catch (e) {
        ctx.throw(400, '0245');
    }
};

post.createAutoControls = async (ctx, next) => {
    ctx.data = {
        successCount: 0,
        success: [],
        failCount: 0,
        fail: []
    };
    for (let i=0; i<ctx.autoControls.length; i++) {
        const autoControl = ctx.autoControls[i];
        try {
            const createdData = await ctx.models.AutoControl.createAutoControl(autoControl);
            if (createdData) {
                ctx.data.successCount++;
                ctx.data.success.push({
                    autoControl: await createdData.reload()
                });
            }
        } catch (e) {
            ctx.data.failCount++;
            ctx.data.fail.push({
                autoControl,
                message: e
            });
        }
    }
};

post.deleteUploadFile = async (ctx, next) => {
    try {
        await Upload.deleteAll(ctx);
    } catch (e) {}
};

post.supplement = async (ctx, next) => {
    const house_id = ctx.params.house_id;
    ctx.json(ctx.data);
    const io = ctx.utils.io();
    if (io) {
        io.emit('auto-controls', {house_id});
        io.emit('motors', {house_id});
        io.emit('powers', {house_id});
    }
};

module.exports = post;

function checkAutoControls(autoControls, user_id, house_id) {
    if (!autoControls.length) {
        return false;
    }
    for (let i=0; i<autoControls.length; i++) {
        let autoControl = autoControls[i];
        autoControl.user_id = user_id;
        autoControl.house_id = house_id;
        if (!checkAutoControlItems(autoControl.type, autoControl.autoControlItems, autoControl.autoControlSteps)) {
            return false;
        }
    }
    return true;
}

function checkAutoControlItems(type, autoControlItems, autoControlSteps = []) {
    if (!autoControlItems || !autoControlItems.length) {
        console.log('autoControlItems');
        return false;
    }
    if (!type) {
        console.log('type');
        return false;
    } else if (type !== 'mix' && autoControlItems.length > 1) {
        console.log('type');
        return false;
    } else if (type !== 'step' && autoControlSteps.length > 1) {
        console.log('type');
        return false;
    }
    for (let i=0; i<autoControlItems.length; i++) {
        const autoControlItem = autoControlItems[i];
        if (!autoControlItem.control_id) {
            console.log('control_id');
            return false;
        } else if (!autoControlItem.state) {
            console.log('state');
            return false;
        } else if (!(autoControlItem.start_hour !== undefined && autoControlItem.start_minute !== undefined && autoControlItem.end_hour !== undefined && autoControlItem.end_minute !== undefined) &&
            !(autoControlItem.start_hour === undefined && autoControlItem.start_minute === undefined && autoControlItem.end_hour === undefined && autoControlItem.end_minute === undefined) &&
            !(autoControlItem.start_hour !== undefined && autoControlItem.start_minute !== undefined)) {
            console.log('hour_minute');
            return false;
        } else if (autoControlItem.time && autoControlItem.time < 0) {
            console.log('time');
            return false;
        } else if (autoControlItem.delay && autoControlItem.delay < 0) {
            console.log('delay');
            return false;
        } else if (autoControlItem.recount && (autoControlItem.recount < 0 || autoControlItem.recount > 100)) {
            console.log('recount');
            return false;
        } else if (autoControlItem.autoControlSensors) {
            if (type === 'time') {
                console.log('type');
                return false;
            }
            for (let j=0; j<autoControlItem.autoControlSensors.length; j++) {
                const autoControlSensor = autoControlItem.autoControlSensors[j];
                if (!autoControlSensor.sensor_id) {
                    console.log('sensor_id');
                    return false;
                } else if (autoControlSensor.value === undefined) {
                    console.log('value');
                    return false;
                } else if (!autoControlSensor.condition) {
                    console.log('condition');
                    return false;
                }
            }
        }
    }
    for (let i=0; i<autoControlSteps.length; i++) {
        const autoControlStep = autoControlSteps[i];
        if (!autoControlStep.control_id) {
            console.log('control_id');
            return false;
        } else if (!autoControlStep.state) {
            console.log('state');
            return false;
        } else if (autoControlStep.time && autoControlStep.time < 0) {
            console.log('time');
            return false;
        } else if (autoControlStep.delay && autoControlStep.delay < 0) {
            console.log('delay');
            return false;
        } else if (autoControlStep.recount && (autoControlStep.recount < 0 || autoControlStep.recount > 100)) {
            console.log('recount');
            return false;
        }
    }
    return true;
}

