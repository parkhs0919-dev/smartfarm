const Router = require('koa-router');
const router = new Router();

const Upload = require('../../../middleware/upload');
const post = require('./post');

const api = {
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: 'house_id',
            file: 1,
            explains: {
                'file': 'json file',
            },
            title: '자동제어 일괄 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.checkHouseId(ctx, next);
                await post.checkFile(ctx, next);
                await post.createAutoControls(ctx, next);
                await post.deleteUploadFile(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                await Upload.deleteAll(ctx);
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
};

router.use(Upload.store({
    fileFilter: (req, file, cb) => {
        if (file.mimetype.indexOf('json') === -1) {
            cb(null, false);
        } else {
            cb(null, true);
        }
    }
}));
router.use(Upload.parse());
router.post('/:house_id', api.post());

module.exports.router = router;
module.exports.api = api;
