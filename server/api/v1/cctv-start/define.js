const Router = require('koa-router');
const router = new Router();

const post = require('./post');

const api = {
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'cctv id'
            },
            title: 'cctv server start',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.findCctv(ctx, next);
                await post.startCctv(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.post('/:id', api.post());

module.exports.router = router;
module.exports.api = api;
