let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
};

post.findCctv = async (ctx, next) => {
    try {
        ctx.cctv = await ctx.models.Cctv.findByPk(ctx.params.id);
        if (!ctx.cctv) {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(404, '0102');
    }
};

post.startCctv = async (ctx, next) => {
    try {
        // await ctx.utils.cctv.start(ctx.cctv);
        await ctx.utils.cctv.request(ctx.cctv);
    } catch (e) {
        ctx.throw(400, '0265');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = post;
