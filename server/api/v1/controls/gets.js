let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isEnum('type', ['motor', 'power', 'control']);
    ctx.check.isEnum('state', ['open', 'stop', 'close', 'on', 'off']);
    ctx.check.isEnum('direction', ['none', 'right', 'left']);
    ctx.check.isInt('size');
    ctx.check.isInt('offset');
    ctx.check.len('ipAddress', 1, 15);

    // if (!ctx.query.size) ctx.query.size = ctx.meta.std.defaultSize;
    if (!ctx.query.offset) ctx.query.offset = 0;

    if (ctx.request.body.ipAddress) {
        let temp = ctx.request.body.body.ipAddress.split(':');
        if (temp.length !== 4) {
            ctx.throw(400, '0253');
        }
    }

    ctx.data = {};
};

gets.getControls = async (ctx, next) => {
    try {
        ctx.query.user = ctx.user;
        ctx.data = await ctx.models.Control.getControls(ctx.query);
    } catch (e) {   
        ctx.throw(400, '0103');  
    }
};  

gets.supplement = async (ctx, next) => {
    if (!ctx.data.count) {
        ctx.throw(404);
    }
    ctx.json(ctx.data);
};

module.exports = gets;
