let puts = {};

puts.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    if (ctx.request.body.motors instanceof Array && ctx.request.body.motors.length) {
        for (let i=0; i<ctx.request.body.motors.length; i++) {
            const motor = ctx.request.body.motors[i];
            if (!motor.id || !motor.control_name || !motor.direction || motor.range === undefined) {
                ctx.throw(400, '0229');
            }
        }
    } else if (ctx.request.body.powers instanceof Array && ctx.request.body.powers.length) {
        for (let i=0; i<ctx.request.body.powers.length; i++) {
            const power = ctx.request.body.powers[i];
            if (!power.id || !power.control_name) {
                ctx.throw(400, '0229');
            }
        }
    } else {
        ctx.throw(400, '0229');
    }
};

puts.getHouseId = async (ctx, next) => {
    let id;
    if (ctx.request.body.motors) {
        id = ctx.request.body.motors[0].id;
    } else if (ctx.request.body.powers) {
        id = ctx.request.body.powers[0].id;
    }
    const control = await ctx.models.Control.findByPk(id);
    if (!control) {
        ctx.throw(404, '0103');
    }
    ctx.house_id = control.house_id;
};

puts.updateControls = async (ctx, next) => {
    if (ctx.request.body.motors) {
        try {
            await ctx.models.Control.updateMotors(ctx.request.body.motors);
        } catch (e) {
            ctx.throw(400, '0230');
        }
    } else if (ctx.request.body.powers) {
        try {
            await ctx.models.Control.updatePowers(ctx.request.body.powers);
        } catch (e) {
            ctx.throw(400, '0231');
        }
    }
};

puts.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (ctx.request.body.motors) {
        if (io) io.emit('motors', {
            house_id: ctx.house_id
        });
    } else if (ctx.request.body.powers) {
        if (io) io.emit('powers', {
            house_id: ctx.house_id
        });
    }
};

module.exports = puts;
