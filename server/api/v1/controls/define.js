const Router = require('koa-router');
const router = new Router();

const get = require('./get');
const gets = require('./gets');
const put = require('./put');
const puts = require('./puts');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'control id'
            },
            title: '제어 아이디 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.getControl(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'type',
                'types',
                'direction',
                'state',
                'offset',
                'size',
                'ipAddress'
            ],
            essential: [],
            explains: {
                'house_id': '하우스 ID',
                'type': '유형 (motor, power, control)',
                'types': '유형 (,)로 구분',
                'direction' : '방향 (none, right, left)',
                'state': '상태 (open, stop, close, on, off)',
                'offset': 'offset',
                'size': 'size',
                'ipAddress': 'ip (192.168.0.100)'
            },
            title: '제어 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getControls(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'control_name',
                'button_name_1',
                'button_name_2',
                'button_name_3',
            ],
            essential: [
                'control_name'
            ],
            params: ['id'],
            explains: {
                'id': 'control id',
                'control_name': '컨트롤 명',
                'button_name_1': '버튼명',
                'button_name_2': '버튼명',
                'button_name_3': '버튼명',
            },
            title: '정보 변경',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.updateControl(ctx, next);
                await put.emitControl(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    puts: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'motors',
                'powers'
            ],
            essential: [],
            params: [],
            explains: {
                'motors': 'motor id, control_name, range object array ex) motors: [{"id": 1, "control_name": "name", "direction": "none", "range": 10.0}]',
                'powers': 'power id, control_name, object array ex) powers: [{"id": 1, "control_name": "name"}]'
            },
            title: 'control 정보 변경',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await puts.validate(ctx, next);
                await puts.getHouseId(ctx, next);
                await puts.updateControls(ctx, next);
                await puts.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:id', api.get());
router.get('/', api.gets());
router.put('/:id', api.put());
router.put('/', api.puts());

module.exports.router = router;
module.exports.api = api;
