let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.len('control_name', 1, 191);
    ctx.check.len('button_name_1', 1, 191);
    ctx.check.len('button_name_2', 1, 191);
    ctx.check.len('button_name_3', 1, 191);
};

put.updateControl = async (ctx, next) => {
    try {
        await ctx.models.Control.update(ctx.request.body, {
            where: {
                id: ctx.params.id
            }
        });
    } catch (e) {
        ctx.throw(400, '0207');
    }
};

put.emitControl = async (ctx, next) => {
    try {
        const control = await ctx.models.Control.findById(ctx.params.id);
        if (control) {
            const io = ctx.utils.io();
            if (io) io.emit(control.type, control);
        }
    } catch (e) {}
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = put;
