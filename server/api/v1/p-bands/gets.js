let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');

    ctx.data = {};
};

gets.getPBands = async (ctx, next) => {
    try {
        const rows = await ctx.models.PBand.findAll({
            include: [{
                model: ctx.models.Sensor,
                as: 'inTemperatureSensor'
            }, {
                model: ctx.models.Sensor,
                as: 'outTemperatureSensor'
            }, {
                model: ctx.models.Sensor,
                as: 'windSpeedSensor'
            }],
            where: {
                house_id: ctx.query.house_id
            }
        });
        if (rows.length) {
            ctx.data = {
                count: rows.length,
                rows
            };
        } else {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(404, '0111');
    }
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
