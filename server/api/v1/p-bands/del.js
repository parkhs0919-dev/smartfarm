let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
};

del.check = async ctx => {
    try {
        const result = await ctx.models.PBand.checkDelete(ctx.params.id);

        if (!result) {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(400, '0282');
    }
};

del.deletePBand = async ctx => {
    try {
        await ctx.models.PBand.destroy({
            where: {
                id: ctx.params.id
            }
        });
    } catch (e) {
        ctx.throw(400, '0281');
    }
};

del.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = del;
