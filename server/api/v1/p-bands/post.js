let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.len('p_band_name', 1, 191);
    ctx.check.isInt('in_temperature_sensor_id');
    ctx.check.isInt('out_temperature_sensor_id');
    ctx.check.isInt('wind_speed_sensor_id');
    ctx.check.isBoolean('is_min_max_p');
    ctx.sanitize.toBoolean('is_min_max_p');
    ctx.check.isInt('wind_direction_forward_min_p');
    ctx.check.isInt('wind_direction_forward_max_p');
    ctx.check.isInt('wind_direction_backward_min_p');
    ctx.check.isInt('wind_direction_backward_max_p');

    ctx.data = {};
};

post.createPBand = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.PBand.create(ctx.request.body);
    } catch (e) {
        ctx.throw(400, '0279');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = post;
