const Router = require('koa-router');
const router = new Router();

const get = require('./get');
const gets = require('./gets');
const post = require('./post');
const put = require('./put');
const del = require('./del');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'p-band id'
            },
            title: 'p-band 단일 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.getPBand(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
            ],
            essential: [],
            explains: {
                'house_id': '하우스 id',
            },
            title: 'p-band 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getPBands(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'p_band_name',
                'in_temperature_sensor_id',
                'out_temperature_sensor_id',
                'wind_speed_sensor_id',
                'is_min_max_p',
                'wind_direction_forward_min_p',
                'wind_direction_forward_max_p',
                'wind_direction_backward_min_p',
                'wind_direction_backward_max_p',
            ],
            essential: [
                'house_id',
                'p_band_name',
                'in_temperature_sensor_id',
                'out_temperature_sensor_id',
                'wind_speed_sensor_id',
                'is_min_max_p',
            ],
            explains: {
                'house_id': 'house_id',
                'p_band_name': 'p-band 명',
                'in_temperature_sensor_id': '실내온도 센서 id',
                'out_temperature_sensor_id': '실외온도 센서 id',
                'wind_speed_sensor_id': '풍속 센서 id',
                'is_min_max_p': 'min max p 사용 여부 (true, false)',
                'wind_direction_forward_min_p': '풍상 최소 P',
                'wind_direction_forward_max_p': '풍상 최대 P',
                'wind_direction_backward_min_p': '풍하 최소 P',
                'wind_direction_backward_max_p': '풍하 최대 P',
            },
            title: 'p-band 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.createPBand(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'p_band_name',
                'in_temperature_sensor_id',
                'out_temperature_sensor_id',
                'wind_speed_sensor_id',
                'is_min_max_p',
                'wind_direction_forward_min_p',
                'wind_direction_forward_max_p',
                'wind_direction_backward_min_p',
                'wind_direction_backward_max_p',
            ],
            essential: [
                'p_band_name',
                'in_temperature_sensor_id',
                'out_temperature_sensor_id',
                'wind_speed_sensor_id',
                'is_min_max_p',
            ],
            params: ['id'],
            explains: {
                'id': 'p-band id',
                'p_band_name': 'p-band 명',
                'in_temperature_sensor_id': '실내온도 센서 id',
                'out_temperature_sensor_id': '실외온도 센서 id',
                'wind_speed_sensor_id': '풍속 센서 id',
                'is_min_max_p': 'min max p 사용 여부 (true, false)',
                'wind_direction_forward_min_p': '풍상 최소 P',
                'wind_direction_forward_max_p': '풍상 최대 P',
                'wind_direction_backward_min_p': '풍하 최소 P',
                'wind_direction_backward_max_p': '풍하 최대 P',
            },
            title: 'p-band 수정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.updatePBand(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'p-band id'
            },
            title: 'p-band 삭제',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.check(ctx, next);
                await del.deletePBand(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:id', api.get());
router.get('/', api.gets());
router.post('/', api.post());
router.put('/:id', api.put());
router.del('/:id', api.del());

module.exports.router = router;
module.exports.api = api;
