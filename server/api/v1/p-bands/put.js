let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.len('p_band_name', 1, 191);
    ctx.check.isInt('in_temperature_sensor_id');
    ctx.check.isInt('out_temperature_sensor_id');
    ctx.check.isInt('wind_speed_sensor_id');
    ctx.check.isBoolean('is_min_max_p');
    ctx.sanitize.toBoolean('is_min_max_p');
    ctx.check.isInt('wind_direction_forward_min_p');
    ctx.check.isInt('wind_direction_forward_max_p');
    ctx.check.isInt('wind_direction_backward_min_p');
    ctx.check.isInt('wind_direction_backward_max_p');

    ctx.data = {};
};

put.updatePBand = async (ctx, next) => {
    try {
        let body = ctx.request.body;
        if (body.wind_direction_forward_min_p === undefined) body.wind_direction_forward_min_p = null;
        if (body.wind_direction_forward_max_p === undefined) body.wind_direction_forward_max_p = null;
        if (body.wind_direction_backward_min_p === undefined) body.wind_direction_backward_min_p = null;
        if (body.wind_direction_backward_max_p === undefined) body.wind_direction_backward_max_p = null;
        await ctx.models.PBand.update(body, {
            where: {
                id: ctx.params.id
            }
        });
    } catch (e) {
        ctx.throw(400, '0279');
    }
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = put;
