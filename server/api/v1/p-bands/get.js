let get = {};

get.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');

    ctx.data = {};
};

get.getPBand = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.PBand.findByPk(ctx.params.id, {
            include: [{
                model: ctx.models.Sensor,
                as: 'inTemperatureSensor'
            }, {
                model: ctx.models.Sensor,
                as: 'outTemperatureSensor'
            }, {
                model: ctx.models.Sensor,
                as: 'windSpeedSensor'
            }]
        });
    } catch (e) {
        ctx.throw(400, '0111');
    }
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
