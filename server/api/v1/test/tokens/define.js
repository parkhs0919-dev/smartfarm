const Router = require('koa-router');
const router = new Router();

const get = require('./get');
const post = require('./post');

const api = {
    get: (isParam) => async (ctx) => {
        const params = {
            acceptable: [],
            essential: [],
            explains: {},
            title: 'get info',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx);
                await get.supplement(ctx);
            } catch (e) {
                ctx.catch(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx) => {
        const params = {
            acceptable: [],
            essential: [],
            explains: {},
            title: 'create token test',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx);
                await post.createToken(ctx);
                await post.supplement(ctx);
            } catch (e) {
                ctx.catch(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.get());
router.post('/', api.post());

module.exports.router = router;
module.exports.api = api;
