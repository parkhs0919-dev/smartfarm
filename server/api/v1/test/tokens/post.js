let post = {};

post.validate = async (ctx) => {
    ctx.payload = {
        'key': 'value'
    };
};

post.createToken = async (ctx) => {
    ctx.token = await ctx.utils.jwt.generateAccessToken(ctx.payload);
    ctx.utils.jwt.setCookie(ctx, ctx.token);
};

post.supplement = async (ctx) => {
    ctx.json(ctx.token);
};

module.exports = post;
