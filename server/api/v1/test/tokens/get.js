let get = {};

get.validate = async (ctx) => {
    if (!ctx.user) {
        ctx.throw(401);
    }

    ctx.data = {};
};

get.supplement = async (ctx) => {
    ctx.json(ctx.user);
};

module.exports = get;
