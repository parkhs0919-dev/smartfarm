let post = {};

post.validate = (ctx) => {
    ctx.check.len('test', 1, 191);
    ctx.check.isInt('temp');
};

post.createText = async (ctx, next) => {
    try {
        const data = await ctx.models.Test.create(ctx.request.body);
        if (data) {
            ctx.body = data;
        } else {
            ctx.body = {
                message: 'fail'
            };
        }
        await next();
    } catch (e) {
        ctx.throw(400, e);
    }
};

module.exports = post;
