let put = {};

put.validate = (ctx) => {
    ctx.check.isInt('id');
};

put.updateTest = async (ctx, next) => {
    try {
        const result = await ctx.models.Test.update(ctx.request.body, {
            where: {
                id: ctx.params.id
            }
        });
        if (result[0]) {
            ctx.status = 204;
        } else {
            ctx.status = 404;
        }
        await next();
    } catch (e) {
        ctx.throw(400, e);
    }
};

module.exports = put;
