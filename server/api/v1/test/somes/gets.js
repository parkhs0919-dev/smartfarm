let gets = {};

gets.validate = (ctx) => {

};

gets.findTests = async (ctx, next) => {
    try {
        const count = await ctx.models.Test.count();
        const data = await ctx.models.Test.findAll({order: [['id', 'desc']], limit: 5});
        if (count) {
            ctx.body = {
                count: count,
                rows: data
            };
        } else {
            ctx.throw(404);
        }
        await next();
    } catch (e) {
        ctx.throw(400, e);
    }

};

module.exports = gets;
