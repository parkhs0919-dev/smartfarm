let del = {};

del.validate = (ctx) => {
    ctx.check.isInt('id');
};

del.deleteTest = async (ctx, next) => {
    try {
        const result = await ctx.models.Test.destroy({
            where: {
                id: ctx.params.id
            }
        });
        if (result) {
            ctx.status = 204;
        } else {
            ctx.status = 404;
        }
        await next();
    } catch (e) {
        ctx.throw(400, e);
    }
};

module.exports = del;
