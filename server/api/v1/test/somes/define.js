const Router = require('koa-router');
const router = new Router();

const get = require('./get');
const gets = require('./gets');
const post = require('./post');
const put = require('./put');
const del = require('./del');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'ID'
            },
            title: 'title',
            state: 'develop'
        };

        if (!isParam) {
            await get.validate(ctx, next);
            await get.findTest(ctx, next);
        } else {
            return params;
        }
    },
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            explains: {},
            title: 'title',
            state: 'develop'
        };

        if (!isParam) {
            await gets.validate(ctx, next);
            await gets.findTests(ctx, next);
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'test',
                'temp'
            ],
            essential: [],
            explains: {
                'test': 'test',
                'temp': 'temp'
            },
            title: 'title',
            state: 'develop'
        };

        if (!isParam) {
            await post.validate(ctx, next);
            await post.createText(ctx, next);
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'test',
                'temp'
            ],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'ID',
                'test': 'test',
                'temp': 'temp'
            },
            title: 'title',
            state: 'develop'
        };

        if (!isParam) {
            await put.validate(ctx, next);
            await put.updateTest(ctx, next);
        } else {
            return params;
        }
    },
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'ID'
            },
            title: 'title',
            state: 'develop'
        };

        if (!isParam) {
            await del.validate(ctx, next);
            await del.deleteTest(ctx, next);
        } else {
            return params;
        }
    }
};

router.use(async (ctx, next) => {
    console.log('middleware');
    await next();
});
router.get('/:id', api.get());
router.get('/', api.gets());
router.post('/', api.post());
router.put('/:id', api.put());
router.del('/:id', api.del());

module.exports.router = router;
module.exports.api = api;
