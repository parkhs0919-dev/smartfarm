let get = {};

get.validate = (ctx) => {
    ctx.check.isInt('id');
};

get.findTest = async (ctx, next) => {
    try {
        const data = await ctx.models.Test.findByPk(ctx.params.id);
        if (data) {
            ctx.body = data;
        } else {
            ctx.throw(404);
        }
        await next();
    } catch (e) {
        ctx.throw(400, e);
    }
};

module.exports = get;
