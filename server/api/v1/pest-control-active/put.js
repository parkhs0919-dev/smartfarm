const CONTROL_METHOD = require('../../../methods/pest-control');
let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401'); 
    }
  
    ctx.check.isInt('id');
};

put.checkPestControl = async (ctx, next) => {
    try {    
        if (typeof(ctx.request.body.is_auto)==='undefined') {
            if (ctx.request.body.state && ctx.request.body.state === 'run') {
                const state_count = await ctx.models.PestControl.count({
                    where: {
                        state: ctx.request.body.state
                    }
                });
                const count = await ctx.models.PestControl.count({
                    where: {
                        id: ctx.params.id,
                        state: ctx.request.body.state
                    }
                });
                if (state_count) {
                    ctx.throw(400, '0605');
                }
                if (count) {
                    ctx.throw(400, '0605');
                }
            } else if (ctx.request.body.state && ctx.request.body.state === 'stop') {
                const count = await ctx.models.PestControl.count({
                    where: {
                        id: ctx.params.id,
                        state: ctx.request.body.state
                    }
                });
                if (count) {
                    ctx.throw(400, '0605');
                }
            } else {
                ctx.throw(400, '0605');
            }
        }      
    } catch (e) {  
        ctx.throw(400, '0605');
    }
};  

put.updatePestControlActive = async (ctx, next) => {
    try {   
        const body = ctx.request.body;
        const data = await ctx.models.PestControl.updatePestControlActvie(ctx.params.id ,ctx.request.body);
        const io = ctx.utils.io();        
        if(data){
            if (io) io.emit('pest-control', body);
        }     
          
    } catch (e) {       
        //console.log(e);
        ctx.throw(400, '0605');
    }
};   

put.createReport = async (ctx, next) => {
    try {
        const pest_control = await ctx.models.PestControl.getPestControlByControlId(ctx.params.id);    
        await CONTROL_METHOD.setIsControlProcessing(pest_control.house_id,ctx.params.id,((ctx.request.body.state==='run')? true :false))     
        await ctx.models.Report.create({                      
            user_id: ctx.user.id,              
            house_id: pest_control.house_id,      
            control_id: null,   
            type: 'power',   
            contents: `[방제기] ${pest_control.pest_control_name}가(이) '${(pest_control.state===`run`)? `운전`: `정지`}' 동작 합니다.`
        });   
              
        const io = ctx.utils.io();  
        if (io) io.emit('report', {       
            house_id: pest_control.house_id 
        });     
    } catch (e) {console.log(e);}
};   

put.supplement = async (ctx, next) => {
    ctx.json(null);  
};  

module.exports = put;
