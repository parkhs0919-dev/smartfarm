const Router = require('koa-router');
const router = new Router();

const put = require('./put');

const api = {
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'state',
            ],
            essential: [
                'state'
            ],
            params: ['id'],
            explains: {
                'id': 'pest control id',

            },
            title: '방제기 수동 조작',
            state: 'develop'
        }; 

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.checkPestControl(ctx, next);
                await put.updatePestControlActive(ctx, next);
                await put.createReport(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {  
            return params;   
        }
    }
};

router.put('/:id', api.put());

module.exports.router = router;
module.exports.api = api;
