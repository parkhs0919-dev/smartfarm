const Router = require('koa-router');
const router = new Router();

const post = require('./post');

const api = {
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'isHome',
                'isActive',
                'x',
                'y',
                'z',
                'timeout'
            ],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'cctv id',
                'isHome': 'goto home (true)',
                'isActive': 'active (true / false)',
                'x': 'speed.x (-1 ~ 1)',
                'y': 'speed.y (-1 ~ 1)',
                'z': 'speed.z (-1 ~ 1)',
                'timeout': 'timeout default 1second'
            },
            title: 'cctv server move',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.findCctv(ctx, next);
                await post.moveCctv(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.post('/:id', api.post());

module.exports.router = router;
module.exports.api = api;
