let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.isBoolean('isHome');
    ctx.sanitize.toBoolean('isHome');
    ctx.check.isBoolean('isActive');
    ctx.sanitize.toBoolean('isActive');
    ctx.check.isFloat('x');
    ctx.check.isFloat('y');
    ctx.check.isFloat('z');
    ctx.check.isInt('timeout');
};

post.findCctv = async (ctx, next) => {
    try {
        ctx.cctv = await ctx.models.Cctv.findOne({
            where: {
                id: ctx.params.id,
                type: 'ptz'
            }
        });
        if (!ctx.cctv) {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(404, '0102');
    }
};

post.moveCctv = async (ctx, next) => {
    try {
        await ctx.utils.cctv.requestMove(ctx.cctv, ctx.request.body);
    } catch (e) {
        ctx.throw(400, '0273');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = post;
