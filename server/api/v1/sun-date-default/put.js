let put = {};

put.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.check.isInt('rise_hour');
    ctx.check.isInt('rise_minute');
    ctx.check.isInt('sun_hour');
    ctx.check.isInt('sun_minute');
};

put.updateSunDateDefault = async (ctx) => {
    try {
        await ctx.models.SunDateDefault.update(ctx.request.body, {
            where: {}
        });
    } catch (e) {
        ctx.throw(400, '0231');
    }
};

put.supplement = async (ctx) => {
    ctx.json(null);
};

module.exports = put;
