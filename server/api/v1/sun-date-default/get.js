let get = {};

get.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
};

get.findSunDateDefault = async (ctx) => {
    try {
        ctx.data = await ctx.models.SunDateDefault.findOne({});
        if (!ctx.data) {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(400, '0111');
    }
};

get.supplement = async (ctx) => {
    ctx.json(ctx.data);
};

module.exports = get;
