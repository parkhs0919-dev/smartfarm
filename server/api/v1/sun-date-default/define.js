const Router = require('koa-router');
const router = new Router();

const get = require('./get');
const put = require('./put');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: [],
            explains: {},
            title: '기본 일출 일몰 시각 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx);
                await get.findSunDateDefault(ctx);
                await get.supplement(ctx);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'rise_hour',
                'rise_minute',
                'set_hour',
                'set_minute'
            ],
            essential: [
                'rise_hour',
                'rise_minute',
                'set_hour',
                'set_minute'
            ],
            params: [],
            explains: {
                'rise_hour': '일출 시각',
                'rise_minute': '일출 분',
                'set_hour': '일몰 시각',
                'set_minute': '일몰 분'
            },
            title: '기본 일출 일몰 시각 수정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx);
                await put.updateSunDateDefault(ctx);
                await put.supplement(ctx);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
};

router.get('/', api.get());
router.put('/', api.put());

module.exports.router = router;
module.exports.api = api;
