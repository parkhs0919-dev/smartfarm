const CONTROL_METHOD = require('../../../methods/pest-control');
const Sequelize = require('sequelize');
const sequelize = require('../../../methods/sequelize').sequelize;
     
let put = {};
  
put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
  
    ctx.check.isInt('id');
};
  
put.updateFlowMeter = async (ctx, next) => {
    try {  

        const data = await ctx.models.FlowMeter.updateFlowMeter(ctx.params.id,ctx.request.body);         
        if(data){      
            const io = ctx.utils.io();
            const house_id=ctx.request.body.house_id;
            if (io) io.emit('flow_meter', house_id);
        }       
    } catch (e) {      
        console.log(e)            
        ctx.throw(400, '0607');
    }             
};
  
put.supplement = async (ctx, next) => {  
    ctx.json(null);                 
};  

module.exports = put;
