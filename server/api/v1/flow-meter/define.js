const Router = require('koa-router');
const router = new Router();

const get = require('./get');  
const put = require('./put');
 
const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                `id`,
                `house_id`,
                `start_hour`,                
                `end_hour`
            ],
            essential: [],
            explains: {
            },
            title: '유량계 회차 설정 상세 조회',
            state: 'develop'
        };
  
        if (!isParam) {
            try {                  
                await get.validate(ctx, next);
                await get.getFlowMeter(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },    
    put: (isParam) => async (ctx, next) => {
        const params = {   
            acceptable: [
                `type`,
                `name`,  
                `items`,
            ],
            essential: [
                `type`,
                `name`,
                `items`,
            ],
            explains: {
                'type': '타입  ',
                'name': '그룹 명 ',
                'items': 'items '
            },
            title: '유량계 회차 설정 수정',
            state: 'develop' 
        };

        if (!isParam) {
            try {
                
                await put.validate(ctx, next);
                await put.updateFlowMeter(ctx, next);
                await put.supplement(ctx, next);   
            } catch (e) {         
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },  
};

router.get('/:id', api.get());  
router.put('/:id', api.put());  
      

module.exports.router = router;
module.exports.api = api;
