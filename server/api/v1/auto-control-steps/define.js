const Router = require('koa-router');
const router = new Router();

const post = require('./post');
const del = require('./del');

const api = {
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'auto_control_id',
                'control_id',
                'state',
                'time',
                'percentage',
                'delay',
                'recount',
            ],
            essential: [
                'auto_control_id',
                'control_id',
                'state',
            ],
            explains: {
                'auto_control_id': 'auto-control id',
                'control_id': 'control id',
                'state': '제어 상태 (open, stop, close, on, off)',
                'time': '시간 (초)',
                'percentage': '퍼센트 (0 ~ 100%)',
                'delay': '지연시간 (초)',
                'recount': '반복횟수',
            },
            title: '단계제어 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.checkAutoControl(ctx, next);
                await post.checkAutoControlItems(ctx, next);
                await post.checkControls(ctx, next);
                await post.checkControlState(ctx, next);
                await post.createAutoControlStep(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'auto-control-step id'
            },
            title: '단계제어 삭제',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.getAutoControlStep(ctx, next);
                await del.checkControls(ctx, next);
                await del.deleteAutoControlStep(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.post('/', api.post());
router.del('/:id', api.del());

module.exports.router = router;
module.exports.api = api;
