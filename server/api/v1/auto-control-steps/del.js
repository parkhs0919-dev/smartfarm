let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');

    ctx.data = {};
};

del.getAutoControlStep = async (ctx, next) => {
    const autoControlStep = await ctx.models.AutoControlStep.findByPk(ctx.params.id, {
        include: [{
            model: ctx.models.Control,
            as: 'control'
        }, {
            model: ctx.models.AutoControl,
            as: 'autoControl'
        }]
    });
    if (!autoControlStep) {
        ctx.throw(404, '0106');
    }
    ctx.control = autoControlStep.control;
    ctx.house_id = autoControlStep.autoControl.house_id;
};

del.checkControls = async (ctx, next) => {
    try {
        const count = await ctx.models.Control.count({
            where: {
                house_id: ctx.house_id,
                mode: 'auto'
            }
        });
        if (count) {
            ctx.throw(400, '0250');
        }
    } catch (e) {
        ctx.throw(400, '0250');
    }
};

del.deleteAutoControlStep = async (ctx, next) => {
    try {
        await ctx.models.AutoControlStep.destroy({
            where: {
                id: ctx.params.id
            }
        });
    } catch (e) {
        ctx.throw(400, '0242');
    }
};

del.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) {
        io.emit('auto-controls', {
            house_id: ctx.house_id
        });
        if (ctx.control.type === 'motor') {
            io.emit('motors', {
                house_id: ctx.house_id
            });
        } else if (ctx.control.type === 'power') {
            io.emit('powers', {
                house_id: ctx.house_id
            });
        }
    }
};

module.exports = del;
