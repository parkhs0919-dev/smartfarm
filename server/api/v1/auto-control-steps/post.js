let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('auto_control_id');
    ctx.check.isInt('control_id');
    ctx.check.isEnum('state', ['open', 'stop', 'close', 'on', 'off']);
    ctx.check.isInt('time');
    ctx.check.isInt('percentage');
    ctx.check.isInt('delay');
    ctx.check.isInt('recount');

    ctx.data = null;
};

post.checkAutoControl = async (ctx, next) => {
    try {
        const autoControl = await ctx.models.AutoControl.findByPk(ctx.request.body.auto_control_id);
        if (!autoControl || autoControl.type !== 'step') {
            ctx.throw(400, '0260');
        }
    } catch (e) {
        ctx.throw(400, '0260');
    }
};

post.checkAutoControlItems = async (ctx, next) => {
    const count = await ctx.models.AutoControlItem.count({
        where: {
            auto_control_id: ctx.request.body.auto_control_id
        }
    });
    const autoControl = await ctx.models.AutoControl.findByPk(ctx.request.body.auto_control_id);
    if (count > 1) {
        ctx.throw(400, '0246');
    }
    if (!autoControl) {
        ctx.throw(404, '0105');
    }
    ctx.house_id = autoControl.house_id;
};

post.checkControls = async (ctx, next) => {
    try {
        const count = await ctx.models.Control.count({
            where: {
                house_id: ctx.house_id,
                mode: 'auto'
            }
        });
        if (count) {
            ctx.throw(400, '0250');
        }
    } catch (e) {
        ctx.throw(400, '0250');
    }
};

post.checkControlState = async (ctx, next) => {
    const body = ctx.request.body;
    const control = await ctx.models.Control.findById(ctx.request.body.control_id);
    if (control) {
        if (control.type === 'motor' || control.type === 'windDirection') {
            if (body.state !== 'open' && body.state !== 'stop' && body.state !== 'close') {
                ctx.throw(400, '0245');
            }
        } else if (control.type === 'power') {
            if (body.state !== 'on' && body.state !== 'off') {
                ctx.throw(400, '0245');
            }
        } else {
            ctx.throw(400, '0244');
        }
    } else {
        ctx.throw(404, '0103');
    }
    ctx.control = control;
};

post.createAutoControlStep = async (ctx, next) => {
    try {
        const createdData = await ctx.models.AutoControlStep.create({
            user_id: ctx.user.id,
            ...ctx.request.body
        });
        if (createdData) {
            ctx.data = await createdData.reload();
        } else {
            ctx.throw(400, '0243');
        }
    } catch (e) {
        ctx.throw(400, '0243');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
    const io = ctx.utils.io();
    if (io) {
        io.emit('auto-controls', {
            house_id: ctx.house_id
        });
        if (ctx.control.type === 'motor' || ctx.control.type === 'windDirection') {
            io.emit('motors', {
                house_id: ctx.house_id
            });
        } else if (ctx.control.type === 'power') {
            io.emit('powers', {
                house_id: ctx.house_id
            });
        }
    }
};

module.exports = post;
