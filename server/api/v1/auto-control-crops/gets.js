let gets = {};
const CONFIG = require('../../../config');

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isEnum('platform', ['farm', 'fluid', 'animal']);

    ctx.data = null;
};

gets.getCrops = async (ctx, next) => {
    try {
        ctx.data = await ctx.utils.erp.getCrops(ctx.query.platform);
    } catch (e) {
        ctx.throw(400, '0291');
    }
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
