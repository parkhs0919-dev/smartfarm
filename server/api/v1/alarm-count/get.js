let get = {};

get.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.data = {};
};

get.countAlarm = async (ctx, next) => {
    try {
        ctx.data.alarmCount = await ctx.models.Alarm.count({});
    } catch (e) {
        ctx.throw(404, '0217');
    }
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
