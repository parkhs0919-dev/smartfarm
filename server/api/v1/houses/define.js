const Router = require('koa-router');
const router = new Router();

const STD = require('../../../metadata/standards');

const get = require('./get');
const gets = require('./gets');
const put = require('./put');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'house id'
            },
            title: '하우스 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.getHouse(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'offset',
                'size'
            ],
            essential: [],
            explains: {
                'offset': 'offset',
                'size': 'size'
            },
            title: '하우스 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getHouses(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'direction',
                'sensor_id',
                'is_use_alarm',
            ],
            essential: [
                'direction',
            ],
            params: ['id'],
            explains: {
                'id': 'house id',
                'direction': `방향 (${STD.house.enumDirections.join(', ')})`,
                'sensor_id': '풍향 센서 id',
                'is_use_alarm' : '패널 알람 설정'
            },
            title: '전체 자동제어 설정 변경',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.updateHouse(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:id', api.get());
router.get('/', api.gets());
router.put('/:id', api.put());

module.exports.router = router;
module.exports.api = api;
