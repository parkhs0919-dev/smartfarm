let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('size');
    ctx.check.isInt('offset');

    // if (!ctx.query.size) ctx.query.size = ctx.meta.std.defaultSize;
    if (!ctx.query.offset) ctx.query.offset = 0;

    ctx.data = {};
};

gets.getHouses = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.House.getHouses(ctx.query);
    } catch (e) {
        ctx.throw(400, '0101');
    }
};

gets.supplement = async (ctx, next) => {
    if (!ctx.data.count) {
        ctx.throw(404);
    }
    ctx.json(ctx.data);
};

module.exports = gets;
