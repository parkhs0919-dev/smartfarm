let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.isEnum('direction', ctx.meta.std.house.enumDirections);
    ctx.check.isBoolean('is_use_alarm');
    ctx.check.isInt('sensor_id');
};

put.updateHouse = async (ctx, next) => {
    try {
        await ctx.models.House.updateHouse(ctx.request.body, ctx.params.id);
    } catch (e) {
        ctx.throw(400, '0272');
    }
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = put;
