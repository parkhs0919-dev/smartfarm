let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');

    ctx.data = null;
};

get.getHouse = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.House.getHouse(ctx.params.id);
        if (!ctx.data) {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(404, '0109');
    }
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
