const path = require('path');
const fs = require('fs');
const rimraf = require('rimraf');
const dateExp = new RegExp('^[0-9]{4}-[0-9]{2}-[0-9]{2}$');
const csvPath = path.join(__dirname, '../../../../csv');
let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isEnum('type', ['log', 'report','warning']);
    ctx.check.isInt('sensor_id');
    ctx.check.isInt('house_id');
    ctx.check.isRegExp('startDate', dateExp);
    ctx.check.isRegExp('endDate', dateExp);

    if (ctx.query.type === 'log' && !ctx.query.sensor_id) {
        ctx.throw(400, '0257');
    } else if (ctx.query.type === 'report' && !ctx.query.house_id) {
        ctx.throw(400, '0259');
    }

    ctx.data = null;
};

get.generateDate = async (ctx, next) => {
    const startTime = new Date(`${ctx.query.startDate} 00:00:00`).getTime();
    const endTime = new Date(`${ctx.query.endDate} 00:00:00`).getTime();
    if (endTime - startTime > 7776000000) { // 90 days
        ctx.throw(400, '0256');
    }
    ctx.startDate = ctx.utils.filter.date(startTime, 'yyyy-MM-dd HH:mm:ss');
    ctx.endDate = ctx.utils.filter.date(endTime + 86400000, 'yyyy-MM-dd HH:mm:ss');
    ctx.today = ctx.utils.filter.date(new Date(), 'yyyy-MM-dd');
    const todayTime = new Date(`${ctx.today} 00:00:00`).getTime();

    if (todayTime < endTime) {
        ctx.throw(400, '0258');
    }
};

get.removeOldData = async (ctx, next) => {
    fs.readdirSync(csvPath).forEach(dir => {
        if (dir !== ctx.today) {
            rimraf.sync(csvPath + '/' + dir);
        }
    });
};

get.createTodayDirectory = async (ctx, next) => {
    if (!fs.existsSync(csvPath + '/' + ctx.today)) {
        fs.mkdirSync(csvPath + '/' + ctx.today);
    }
};

get.getSensor = async (ctx, next) => {
    try {
        
        if (ctx.query.sensor_id) {
            ctx.sensor = await ctx.models.Sensor.findByPk(ctx.query.sensor_id);
        }
    } catch (e) {
        ctx.throw(404, '0101')
    }
};

get.getHouse = async (ctx, next) => {
    try {
        if (ctx.query.house_id) {
            ctx.house = await ctx.models.House.findByPk(ctx.query.house_id);
        }
    } catch (e) {
        ctx.throw(404, '0109');
    }
};

get.getData = async (ctx, next) => {
    try {
        const now = new Date().getTime();
        if (ctx.query.type === 'log') {
            ctx.fileName = `${ctx.query.type}_${ctx.sensor.id}_${now}.csv`;
            await ctx.models.Sensor.createCsv(`${csvPath}/${ctx.today}/${ctx.fileName}`, ctx.startDate, ctx.endDate, ctx.sensor);
        } else if (ctx.query.type === 'report'||ctx.query.type === 'warning') {
            ctx.fileName = `${ctx.query.type}_${ctx.house.id}_${now}.csv`;
            await ctx.models.Report.createCsv(`${csvPath}/${ctx.today}/${ctx.fileName}`, ctx.startDate, ctx.endDate, ctx.house,ctx.query.type);
        } 
    } catch (e) {
        console.log(e);
        ctx.throw(400, '0255');
    }
};

get.supplement = async (ctx, next) => {
    //ctx.body = fs.createReadStream(`${csvPath}/${ctx.today}/${ctx.fileName}`);
    ctx.set('Content-Disposition', `attachment; filename="${ctx.fileName}"`);
    ctx.set('Content-Type', 'text/csv');
    ctx.json(`csv/${ctx.today}/${ctx.fileName}`);
};

module.exports = get;
