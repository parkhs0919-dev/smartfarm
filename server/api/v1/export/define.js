const Router = require('koa-router');
const router = new Router();

const get = require('./get');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'type',
                'sensor_id',
                'house_id',
                'startDate',
                'endDate'
            ],
            essential: [
                'type',
                'startDate',
                'endDate'
            ],
            explains: {
                'type': 'type (log, report)',
                'sensor_id': 'sensor id (type log)',
                'house_id': 'house id (type report)',
                'startDate': 'yyyy-MM-dd',
                'endDate': 'yyyy-MM-dd'
            },
            title: 'CSV 출력',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.generateDate(ctx, next);
                await get.removeOldData(ctx, next);
                await get.createTodayDirectory(ctx, next);
                await get.getSensor(ctx, next);
                await get.getHouse(ctx, next);
                await get.getData(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.get());

module.exports.router = router;
module.exports.api = api;
