const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');
const post = require('./post');
const put = require('./put');
const del = require('./del');

const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'wind_direction_type',
                'window_position',
                'type',
                'control_id',
                'auto_control_type',
                'temperature_type',
                'is_temperature_control_option',
            ],
            essential: [],
            explains: {
                'house_id': 'house id',
                'wind_direction_type': '풍상 풍하 유형 (none, forward, backward)',
                'window_position' : '창 선택',
                'type': '제어 유형 (motor, power)',
                'control_id': 'control id',
                'auto_control_type' : '자동제어 타입',
                'temperature_type' : '표 설정 데이터',
                'is_temperature_control_option' : '환기 난방 설정 여부',
            },
            title: '자동제어 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getAutoControls(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'type',
                'auto_control_name',
                'date_type',
                'is_sun',
                'is_mon',
                'is_tue',
                'is_wed',
                'is_thur',
                'is_fri',
                'is_sat',
                'start_date',
                'end_date',
                'per_date',
                'autoControlItems',
                'autoControlSteps'
            ],
            essential: [
                'house_id',
                'type',
                'auto_control_name',
                'date_type',
                'is_sun',
                'is_mon',
                'is_tue',
                'is_wed',
                'is_thur',
                'is_fri',
                'is_sat',
                'autoControlItems',
                'autoControlSteps'
            ],
            explains: {
                'house_id': 'house id',
                'type': '유형 (time, sensor, mix, step)',
                'auto_control_name': '자동제어 명',
                'date_type': '기간 유형 선택 (day, per)',
                'is_sun': '일요일 여부 (true / false)',
                'is_mon': '월요일 여부 (true / false)',
                'is_tue': '화요일 여부 (true / false)',
                'is_wed': '수요일 여부 (true / false)',
                'is_thur': '목요일 여부 (true / false)',
                'is_fri': '금요일 여부 (true / false)',
                'is_sat': '토요일 여부 (true / false)',
                'start_date': '시작일 (yyyy-MM-dd)',
                'end_date': '끝일 (yyyy-MM-dd)',
                'per_date': '일자 텀 일수',
                'autoControlItems': '자동제어 항목 array',
                'autoControlSteps': '단계제어 항목 array'
            },
            title: '자동제어 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.checkControls(ctx, next);
                await post.checkControlStates(ctx, next);
                await post.createAutoControl(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'type',
                'auto_control_name',
                'date_type',
                'is_sun',
                'is_mon',
                'is_tue',
                'is_wed',
                'is_thur',
                'is_fri',
                'is_sat',
                'start_date',
                'end_date',
                'per_date',
                'autoControlItems',
                'autoControlSteps'
            ],
            essential: [
                'type',
                'auto_control_name',
                'is_sun',
                'is_mon',
                'is_tue',
                'is_wed',
                'is_thur',
                'is_fri',
                'is_sat',
                'start_date',
                'end_date',
                'autoControlItems',
                'autoControlSteps'
            ],
            params: ['id'],
            explains: {
                'id': 'auto-control id',
                'type': '유형 (time, sensor, mix, step)',
                'auto_control_name': '자동제어 명',
                'date_type': '기간 유형 선택 (day, per)',
                'is_sun': '일요일 여부 (true / false)',
                'is_mon': '월요일 여부 (true / false)',
                'is_tue': '화요일 여부 (true / false)',
                'is_wed': '수요일 여부 (true / false)',
                'is_thur': '목요일 여부 (true / false)',
                'is_fri': '금요일 여부 (true / false)',
                'is_sat': '토요일 여부 (true / false)',
                'start_date': '시작일 (yyyy-MM-dd)',
                'end_date': '끝일 (yyyy-MM-dd)',
                'per_date': '일자 텀 일수',
                'autoControlItems': '자동제어 항목 array',
                'autoControlSteps': '단계제어 항목 array',
            },
            title: '자동제어 수정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.checkControlDelayCount(ctx, next);
                await put.checkControls(ctx, next);
                await put.checkControlSteps(ctx, next);
                await put.checkControlStates(ctx, next);
                await put.updateAutoControl(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'auto-control id'
            },
            title: '자동제어 삭제',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.getAutoControl(ctx, next);
                await del.checkControls(ctx, next);
                await del.deleteAutoControl(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.gets());
router.post('/', api.post());
router.put('/:id', api.put());
router.del('/:id', api.del());

module.exports.router = router;
module.exports.api = api;
