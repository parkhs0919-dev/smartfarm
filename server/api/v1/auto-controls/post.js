let post = {};
const dateExp = new RegExp('^[0-9]{4}-[0-9]{2}-[0-9]{2}$');

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.len('auto_control_name', 1, 191);
    ctx.check.isEnum('type', ['time', 'sensor', 'control', 'mix', 'step', 'table']);
    ctx.check.isEnum('date_type', ['day', 'per']);
    ctx.check.isBoolean('is_sun');
    ctx.check.isBoolean('is_mon');
    ctx.check.isBoolean('is_tue');
    ctx.check.isBoolean('is_wed');
    ctx.check.isBoolean('is_thur');
    ctx.check.isBoolean('is_fri');
    ctx.check.isBoolean('is_sat');
    ctx.sanitize.toBoolean('is_sun');
    ctx.sanitize.toBoolean('is_mon');
    ctx.sanitize.toBoolean('is_tue');
    ctx.sanitize.toBoolean('is_wed');
    ctx.sanitize.toBoolean('is_thur');
    ctx.sanitize.toBoolean('is_fri');
    ctx.sanitize.toBoolean('is_sat');
    ctx.check.isRegExp('start_date', dateExp);
    ctx.check.isRegExp('end_date', dateExp);
    ctx.check.isInt('per_date');

    const body = ctx.request.body;
    if (body.date_type === 'per' && (!body.per_date || !body.start_date)) {
        ctx.throw(400, '0269');
    }

    if (!checkAutoControlItems(ctx.request.body.type, ctx.request.body.autoControlItems, ctx.request.body.autoControlSteps)) ctx.throw(400, '0238');

    ctx.data = null;
};

post.checkControls = async (ctx, next) => {
    try {
        const count = await ctx.models.Control.count({
            where: {
                house_id: ctx.request.body.house_id,
                mode: 'auto'
            }
        });
        if (count) {
            ctx.throw(400, '0250');
        }
    } catch (e) {
        ctx.throw(400, '0250');
    }
};

post.checkControlStates = async (ctx, next) => {
    let hash = {};
    let controlIds = [];
    let controlStates = [];
    ctx.request.body.autoControlItems.forEach(autoControlItem => {
        if (autoControlItem.control_id && !hash[autoControlItem.control_id]) {
            hash[autoControlItem.control_id] = true;
            controlIds.push(autoControlItem.control_id);
            controlStates.push(autoControlItem.state);
        };
    });
    if (ctx.request.body.autoControlSteps) {
        ctx.request.body.autoControlSteps.forEach(autoControlStep => {
            if (autoControlStep.control_id && !hash[autoControlStep.control_id]) {
                hash[autoControlStep.control_id] = true;
                controlIds.push(autoControlStep.control_id);
                controlStates.push(autoControlStep.state);
            }
        });
    }
    try {
        await ctx.models.Control.checkControlStates(controlIds, controlStates);
    } catch (e) {
        ctx.throw(400, '0245');
    }
};

post.createAutoControl = async (ctx, next) => {
    try {
        const createdData = await ctx.models.AutoControl.createAutoControl({
            user_id: ctx.user.id,
            ...ctx.request.body
        });
        if (createdData) {
            ctx.data = await createdData.reload();
        } else {
            ctx.throw(400, '0235');
        }
    } catch (e) {
        console.log(e);
        ctx.throw(400, '0235');
    }
};

post.supplement = async (ctx, next) => {
    const house_id = ctx.request.body.house_id;
    ctx.json(ctx.data);
    const io = ctx.utils.io();
    if (io) {
        io.emit('auto-controls', {house_id});
        io.emit('motors', {house_id});
        io.emit('powers', {house_id});
    }
};

module.exports = post;

function checkAutoControlItems(type, autoControlItems, autoControlSteps = []) {
    if (!autoControlItems || !autoControlItems.length) {
        console.log('autoControlItems');
        return false;
    }
    if (!type) {
        console.log('type');
        return false;
    } else if (!(type === 'mix' || type === 'table' ) && autoControlItems.length > 1) {
        console.log('type');
        return false;
    } else if (type !== 'step' && autoControlSteps.length > 1) {
        console.log('type');
        return false;
    }
    for (let i=0; i<autoControlItems.length; i++) {
        const autoControlItem = autoControlItems[i];
        if (!autoControlItem.control_id && (!autoControlItem.wind_direction_type || !autoControlItem.window_position)) {
            console.log('control_id');
            return false;
        } else if (!autoControlItem.state) {
            console.log('state');
            return false;
        } else if (!(autoControlItem.start_hour !== undefined && autoControlItem.start_minute !== undefined && autoControlItem.end_hour !== undefined && autoControlItem.end_minute !== undefined) &&
            !(autoControlItem.start_hour === undefined && autoControlItem.start_minute === undefined && autoControlItem.end_hour === undefined && autoControlItem.end_minute === undefined) &&
            !(autoControlItem.start_hour !== undefined && autoControlItem.start_minute !== undefined)) {
            console.log('hour_minute');
            return false;
        } else if (autoControlItem.time && autoControlItem.time < 0) {
            console.log('time');
            return false;
        } else if (autoControlItem.p_band_temperature && !autoControlItem.p_band_id) {
            console.log('p_band');
            return false;
        } else if (autoControlItem.p_band_id && !autoControlItem.p_band_temperature) {
            console.log('p_band');
            return false;
        } else if (autoControlItem.p_band_integral && (!autoControlItem.p_band_temperature || !autoControlItem.p_band_id)) {
            console.log('p_band');
            return false;
        } else if (autoControlItem.delay && autoControlItem.delay < 0) {
            console.log('delay');
            return false;
        } else if (autoControlItem.recount && (autoControlItem.recount < 0 || autoControlItem.recount > 100)) {
            console.log('recount');
            return false;
        }else if(autoControlItem.is_temperature_control_option){
            if(!autoControlItem.rise_time){
                console.log('rise_time');
                return false;
            }else if(!autoControlItem.drop_time){
                console.log('drop_time');
                return false;
            }
        }else if (autoControlItem.autoControlSensors) {
            if (type === 'time') {
                console.log('type');
                return false;
            } else if (type === 'control') {
                console.log('type');
                return false;
            } else if (type === 'sensor' && autoControlItem.autoControlSensors.length > 1) {
                console.log('type');
                return false;
            }
            for (let j=0; j<autoControlItem.autoControlSensors.length; j++) {
                const autoControlSensor = autoControlItem.autoControlSensors[j];
                if (!autoControlSensor.sensor_id) {
                    console.log('sensor_id');
                    return false;
                } else if (autoControlSensor.value === undefined || autoControlSensor.value === null) {
                    console.log('value');
                    return false;
                } else if (!autoControlSensor.condition) {
                    console.log('condition');
                    return false;
                } else if (autoControlSensor.autoControlSensorOptions && autoControlSensor.autoControlSensorOptions.length > 0) {
                    if (autoControlSensor.option_min_value === null || autoControlSensor.option_min_value === undefined) {
                        console.log('option_min_value');
                        return false;
                    } else if (autoControlSensor.option_max_value === null || autoControlSensor.option_max_value === undefined) {
                        console.log('option_max_value');
                        return false;
                    }
                    for (let k=0; k<autoControlSensor.autoControlSensorOptions.length; k++) {
                        const autoControlSensorOption = autoControlSensor.autoControlSensorOptions[k];
                        if (!autoControlSensorOption.sensor_id) {
                            console.log('sensor_id');
                            return false;
                        } else if (autoControlSensorOption.min_value === null || autoControlSensorOption.min_value === undefined) {
                            console.log('min_value');
                            return false;
                        } else if (autoControlSensorOption.max_value === null || autoControlSensorOption.max_value === undefined) {
                            console.log('max_value');
                            return false;
                        }
                    }
                }
            }
        }else if(autoControlItem.autoControlExpectTemperatureSensors){
            if(type !== 'table'){
                console.log('type');
                return false;
            }

            for(let j=0; j<autoControlItem.autoControlExpectTemperatureSensors.length; j++ ){
                const autoControlExpectTemperatureSensor = autoControlItem.autoControlExpectTemperatureSensors[j];
                if(!autoControlExpectTemperatureSensor.sensor_id){
                    console.log('sensor_id');
                    return false;
                }else if(!autoControlExpectTemperatureSensor.min_value){
                    console.log('min_value');
                    return false;
                }else if(!autoControlExpectTemperatureSensor.max_value){
                    console.log('max_value');
                }else if(!autoControlExpectTemperatureSensor.percentage){
                    console.log('percentage');
                    return false;
                }

            }
        }

        if (autoControlItem.autoControlControls) {
            if (type === 'time') {
                console.log('type');
                return false;
            } else if (type === 'sensor') {
                console.log('type');
                return false;
            } else if (type === 'control' && autoControlItem.autoControlControls.length > 1) {
                console.log('type');
                return false;
            }
            for (let j=0; j<autoControlItem.autoControlControls.length; j++) {
                const autoControlControl = autoControlItem.autoControlControls[j];
                if (!autoControlItem.control_id && (!autoControlItem.wind_direction_type || !autoControlItem.window_position)) {
                    console.log('control_id');
                    return false;
                } else if (autoControlControl.value === undefined || autoControlControl.value === null) {
                    console.log('value');
                    return false;
                } else if (!autoControlControl.condition) {
                    console.log('condition');
                    return false;
                }
            }
        }
        if (autoControlItem.autoControlItemMinMaxes) {
            for (let j=0; j<autoControlItem.autoControlItemMinMaxes.length; j++) {
                const autoControlItemMinMax = autoControlItem.autoControlItemMinMaxes[j];
                if (!autoControlItemMinMax.control_min_max_range_id) {
                    console.log('control_min_max_range_id');
                    return false;
                } else if (autoControlItemMinMax.percentage === undefined || autoControlItemMinMax.percentage === '' || autoControlItemMinMax.percentage === null) {
                    console.log('percentage');
                    return false;
                }
            }
        }
    }
    for (let i=0; i<autoControlSteps.length; i++) {
        const autoControlStep = autoControlSteps[i];
        if (!autoControlStep.control_id && (!autoControlStep.wind_direction_type || !autoControlStep.window_position)) {
            console.log('control_id');
            return false;
        } else if (!autoControlStep.state) {
            console.log('state');
            return false;
        } else if (autoControlStep.time && autoControlStep.time < 0) {
            console.log('time');
            return false;
        } else if (autoControlStep.p_band_temperature && !autoControlStep.p_band_id) {
            console.log('p_band');
            return false;
        } else if (autoControlStep.p_band_id && !autoControlStep.p_band_temperature) {
            console.log('p_band');
            return false;
        } else if (autoControlStep.p_band_integral && (!autoControlStep.p_band_temperature || !autoControlStep.p_band_id)) {
            console.log('p_band');
            return false;
        } else if (autoControlStep.delay && autoControlStep.delay < 0) {
            console.log('delay');
            return false;
        } else if (autoControlStep.recount && (autoControlStep.recount < 0 || autoControlStep.recount > 100)) {
            console.log('recount');
            return false;
        } else if (autoControlStep.autoControlStepMinMaxes) {
            for (let j=0; j<autoControlStep.autoControlStepMinMaxes.length; j++) {
                const autoControlStepMinMax = autoControlStep.autoControlStepMinMaxes[j];
                if (!autoControlStepMinMax.control_min_max_range_id) {
                    console.log('control_min_max_range_id');
                    return false;
                } else if (!autoControlStepMinMax.state) {
                    console.log('state');
                    return false;
                } else if (autoControlStepMinMax.percentage === undefined || autoControlStepMinMax.percentage === '' || autoControlStepMinMax.percentage === null) {
                    console.log('percentage');
                    return false;
                }
            }
        }
    }
    return true;
}
