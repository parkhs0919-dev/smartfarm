let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
};

del.getAutoControl = async (ctx, next) => {
    try {
        const autoControl = await ctx.models.AutoControl.findByPk(ctx.params.id);
        if (!autoControl) {
            ctx.throw(404, '0105');
        }
        ctx.house_id = autoControl.house_id;
    } catch (e) {
        ctx.throw(404, '0105');
    }
};

del.checkControls = async (ctx, next) => {
    try {
        const count = await ctx.models.Control.count({
            where: {
                house_id: ctx.house_id,
                mode: 'auto'
            }
        });
        if (count) {
            ctx.throw(400, '0250');
        }
    } catch (e) {
        ctx.throw(400, '0250');
    }
};

del.deleteAutoControl = async (ctx, next) => {
    try {
        await ctx.models.AutoControl.deleteAutoControl(ctx.params.id);
        // await ctx.models.AutoControl.destroy({
        //     where: {
        //         id: ctx.params.id
        //     },
        //     cascade: true
        // });
    } catch (e) {
        ctx.throw(400, '0237');
    }
};

del.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) {
        io.emit('auto-controls', {
            house_id: ctx.house_id
        });
        io.emit('motors', {
            house_id: ctx.house_id
        });
        io.emit('powers', {
            house_id: ctx.house_id
        });
    }
};

module.exports = del;
