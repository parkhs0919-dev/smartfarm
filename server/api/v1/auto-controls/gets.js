let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isEnum('wind_direction_type', ['forward', 'backward']);
    ctx.check.isEnum('window_position', ['1', '2', '3'])
    ctx.check.isEnum('type', ['motor', 'power']);
    ctx.check.isInt('control_id');
    ctx.check.isEnum('state', ['on', 'off']);
    ctx.check.isBoolean('is_temperature_control_option');
    ctx.check.isBoolean('isActiveDate');
    ctx.sanitize.toBoolean('isActiveDate');

    ctx.data = null;
};

gets.getAutoControls = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.AutoControl.getAutoControls(ctx.query);
    } catch (e) {
        ctx.throw(400, '0234');
    }
};

gets.supplement = async (ctx, next) => {
    if (!ctx.data.count) {
        ctx.throw(404, '0105');
    }
    ctx.json(ctx.data);
};

module.exports = gets;
