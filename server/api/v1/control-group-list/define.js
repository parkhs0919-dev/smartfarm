const Router = require('koa-router');
const router = new Router();
const gets = require('./gets');

const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'type',
                'direction',
                'window_position',
                'wind_direction'
            ],
            essential: [],
            explains: {},
            title: '제어 그룹 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getControlGroups(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
};

router.get('/', api.gets());

module.exports.router = router;
module.exports.api = api;
