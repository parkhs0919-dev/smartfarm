let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isEnum('type', ['windDirection', 'motor', 'power']);
    ctx.check.isEnum('window_position', ['1', '2', '3']);
    ctx.check.isEnum('direction', ['left', 'right']);

    ctx.data = {};
};

gets.getControlGroups = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.ControlGroup.getControlGroups(ctx.query);
    } catch (e) {
        ctx.throw(404, '0217');
    }
};

gets.supplement = async (ctx, next) => {
    if (!ctx.data.rows) {
        ctx.throw(404);
    }
    ctx.json(ctx.data);
};

module.exports = gets;
