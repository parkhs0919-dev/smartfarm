let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};

put.updateAnimal = async (ctx, next) => {
    try {
      ctx.data = await ctx.models.Animal.updateAnimal(ctx.request.body,ctx.params.id);
    } catch (e) {
        ctx.throw(400, '0502');
    }
};
put.updateAnimalMemo = async (ctx, next) => {
    try {
      ctx.data = await ctx.models.Animal.updateAnimalMemo(ctx.request.body,ctx.params.id);
    } catch (e) {
        ctx.throw(400, '0502');
    }    
};
put.supplement = async (ctx, next) => {
    ctx.json(null);
};
   
module.exports = put;
