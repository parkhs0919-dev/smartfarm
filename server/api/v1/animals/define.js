const Router = require('koa-router');
const multer = require('koa-multer');
var path = require('path');
var fs = require('fs');
const router = new Router();

const upload_path='uploads/animal';
var fileStorage =  multer.diskStorage(
    {
        destination: function (req,file, callback)
        {
            if (!isDirSync(upload_path)) {
              fs.mkdirSync(upload_path);
            }        
            callback(null, upload_path);    
        },
        filename: function (req, file, callback)
        {
            var extention = path.extname(file.originalname);
            var basename = path.basename(file.originalname, extention);
            var fname =Date.now() + extention;
            callback(null, fname);

        }  
    }
);
var upload = multer(
    {
        storage: fileStorage,
        limits:
            {
              //  files: 1,
                fileSize: 1024 * 1024 * 1024
            }
    }
);

const gets = require('./gets');
const post = require('./post');
const put = require('./put');
const del = require('./del');

const api = {
    gets: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_type_id',
            'animal_code',
            'weight',
            'birth_day',
            'move_day',
            'health_state',
            'no',
            'barcode',
            'mate_count',
            'image_url',
            'memo',
          ],
          essential: [],
          explains: {
            'animal_type_id' : 'animal_type_id',
            'animal_code' : 'animal_code',
            'weight' :'weight',
            'birth_day' :'birth_day',
            'move_day' :'move_day',
            'health_state' :'health_state',
            'no' :'no',
            'barcode' :'barcode',
            'mate_count' :'mate_count',
            'image_url' :'image_url',
            'memo' :'memo',
            'offset': 'offset',
            'size': 'size'
          },
          title: '개체 조회',
          state: 'develop'
      };

      if (!isParam) {
          try {
              //await gets.validate(ctx, next);
              await gets.getAnimal(ctx, next);
              await gets.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }
    },
    get: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_type_id',
            'animal_code',
            'weight',
            'birth_day',
            'move_day',
            'health_state',
            'no',
            'barcode',
            'mate_count',
            'image_url',
            'memo'
          ],
          essential: [],
          explains: {
            'animal_type_id' : 'animal_type_id',
            'animal_code' : 'animal_code',
            'weight' :'weight',
            'birth_day' :'birth_day',
            'move_day' :'move_day',
            'health_state' :'health_state',
            'no' :'no',
            'barcode' :'barcode',
            'mate_count' :'mate_count',
            'image_url' :'image_url',
            'memo' :'memo',
            'offset': 'offset',
            'size': 'size'
          },
          title: '개체 상세 조회',
          state: 'develop'
      };

      if (!isParam) {
          try {
              //await gets.validate(ctx, next);
              await gets.getAnimalDetail(ctx, next);
              await gets.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }
    },
    post: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_type_id',
            'animal_code',
            'weight',
            'birth_day',
            'move_day',
            'health_state',
            'no',
            'barcode',
            'mate_count',
            'image_url',
            'memo',
          ],
          essential: [
            'animal_type_id',
            'animal_code',
            'weight',
            'birth_day',
            'move_day',
            'health_state',
            'no',
            'barcode',
            'mate_count',
            'image_url',
            'memo',
          ],
          explains: {
            'animal_type_id' : 'animal_type_id',
            'animal_code' : 'animal_code',
            'weight' :'weight',
            'birth_day' :'birth_day',
            'move_day' :'move_day',
            'health_state' :'health_state',
            'no' :'no',
            'barcode' :'barcode',
            'mate_count' :'mate_count',
            'image_url' :'image_url',
            'memo' :'memo'
          },
          title: '개체 등록',
          state: 'develop'
      };

      if (!isParam) {
          try {
              //await post.validate(ctx, next);
              await post.createAnimal(ctx, next);
              await post.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }
    },
    multi: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_type_id',
            'animal_code',
            'weight',
            'birth_day',
            'move_day',
            'health_state',
            'no',
            'barcode',
            'mate_count',
            'image_url',
            'memo',
          ],
          essential: [
            'animal_type_id',
            'animal_code',
            'weight',
            'birth_day',
            'move_day',
            'health_state',
            'no',
            'barcode',
            'mate_count',
            'image_url',
            'memo',
          ],
          explains: {
            'animal_type_id' : 'animal_type_id',
            'animal_code' : 'animal_code',
            'weight' :'weight',
            'birth_day' :'birth_day',
            'move_day' :'move_day',
            'health_state' :'health_state',
            'no' :'no',
            'barcode' :'barcode',
            'mate_count' :'mate_count',
            'image_url' :'image_url',
            'memo' :'memo'
          },      
          title: '개체 등록 multi',
          state: 'develop'
      };

      if (!isParam) {
          try {
              //await post.validate(ctx, next);
              await post.createMultiAnimal(ctx, next);
              await post.supplement(ctx, next);
          } catch (e) {    
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }
    },    
    put: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_type_id',
            'animal_code',
            'weight',
            'birth_day',
            'move_day',
            'health_state',
            'no',
            'barcode',
            'mate_count',
            'image_url',
            'memo',
          ],
          essential: [
            'animal_type_id',
            'animal_code',
            'weight',
            'birth_day',
            'move_day',
            'health_state',
            'no',
            'barcode',
            'mate_count',
            'image_url',
            'memo',
          ],
          explains: {
            'animal_type_id' : 'animal_type_id',
            'animal_code' : 'animal_code',
            'weight' :'weight',
            'birth_day' :'birth_day',
            'move_day' :'move_day',
            'health_state' :'health_state',
            'no' :'no',
            'barcode' :'barcode',
            'mate_count' :'mate_count',
            'image_url' :'image_url',
            'memo' :'memo'
          },
          title: '개체 수정',
          state: 'develop'
        };

        if (!isParam) {
            try {
                //await put.validate(ctx, next);
                await put.updateAnimal(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
      },
      putMemo: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
              'memo',
            ],
            essential: [
              'memo',
            ],
            explains: {
              'memo' :'memo'
            },
            title: '개체 수정',
            state: 'develop'
          };

          if (!isParam) {
              try {
                  //await put.validate(ctx, next);
                  await put.updateAnimalMemo(ctx, next);
                  await put.supplement(ctx, next);
              } catch (e) {
                  ctx.error(e.message, e.status);
              }
          } else {
              return params;
          }
        },
      del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
              'animal_type_id',
              'animal_code',
              'weight',
              'birth_day',
              'move_day',
              'health_state',
              'no',
              'barcode',
              'mate_count',
              'image_url',
              'memo',
            ],
            essential: [
              'animal_type_id',
              'animal_code',
              'weight',
              'birth_day',
              'move_day',
              'health_state',
              'no',
              'barcode',
              'mate_count',
              'image_url',
              'memo',
            ],
            explains: {
              'animal_type_id' : 'animal_type_id',
              'animal_code' : 'animal_code',
              'weight' :'weight',
              'birth_day' :'birth_day',
              'move_day' :'move_day',
              'health_state' :'health_state',
              'no' :'no',
              'barcode' :'barcode',
              'mate_count' :'mate_count',
              'image_url' :'image_url',
              'memo' :'memo'
            },
            title: '개체 삭제',
            state: 'develop'
          };

          if (!isParam) {
              try {
                  //await del.validate(ctx, next);
                  await del.deleteAnimal(ctx, next);
                  await del.supplement(ctx, next);
              } catch (e) {
                  ctx.error(e.message, e.status);
              }
          } else {
              return params;
          }
      },
      postImg: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
              'image_url',
            ],
            essential: [
              'image_url'
            ],
            explains: {
              'image_url' :'image_url',
            },
            title: '개체 이미지 수정',
            state: 'develop'
          };

          if (!isParam) {
              try {
              //await del.validate(ctx, next);
              await post.updateAnimalImg(ctx, next);
              if(ctx.req.body.old_image_url) await removeFile(upload_path+'/'+ctx.req.body.old_image_url);
              await post.supplement(ctx, next);

              } catch (e) {
                  ctx.error(e.message, e.status);
              }
          } else {
              return params;
          }
      },
      delImg: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
              'image_url',
            ],
            essential: [
              'image_url'
            ],
            explains: {
              'image_url' :'image_url',
            },
            title: '개체 이미지 삭제',
            state: 'develop'
          };
   
          if (!isParam) {
              try {   
              //await del.validate(ctx, next);
              await removeFile(upload_path+'/'+ctx.request.body.image_url);  
              ctx.request.body.image_url=null;  
              await post.updateAnimalImgDel(ctx, next);
              await post.supplement(ctx, next);
              
              } catch (e) {
                  ctx.error(e.message, e.status);
              }
          } else {
              return params;   
          }
      }
};

router.get('/:id', api.gets());
router.get('/detail/:id', api.get());
router.post('/', upload.single('userfile') ,api.post());
router.post('/delimg', api.delImg());  
router.post('/img', upload.single('userfile') ,api.postImg());
router.post('/multi', api.multi());     
router.put('/:id', api.put());
router.put('/:id/memo', api.putMemo());
router.delete('/:id', api.del());
  

module.exports.router = router;
module.exports.api = api;

function removeFile(filename){
  return new Promise(function (resolve, reject){
    fs.unlink(filename,  function (err){
      if (err) reject(err);
      else resolve('0000');
    });
  });
}
   
function isDirSync(aPath) {
  try {
    return fs.statSync(aPath).isDirectory();
  } catch (e) {
    if (e.code === 'ENOENT') {
      return false;
    } else {
      throw e;
    }
  }
}  