let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};
gets.getAnimal = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.Animal.getAnimal(ctx.params.id,ctx.query);
    } catch (e) {
        ctx.throw(400, '0504');
    }
};

gets.getAnimalDetail = async (ctx, next) => {
  try {
       ctx.data = await ctx.models.Animal.getAnimalDetail(ctx.params.id);
  } catch(e) {
      ctx.throw(400, '0505');   
  }
};   

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
