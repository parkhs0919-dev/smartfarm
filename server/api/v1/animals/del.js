let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};

del.deleteAnimal = async (ctx, next) => {
  try {
       ctx.data = await ctx.models.Animal.deleteAnimal(ctx.params.id);
  } catch(e) {
      ctx.throw(400, '0503');
  }
};  

del.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = del;
