let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isEnum('health_state', ['good', 'normal', 'bad']);
    ctx.check.isRegExp('birth_day', birth_day);
    ctx.check.isRegExp('move_day', move_day);
    ctx.data = {};
};

post.createAnimal = async (ctx, next) => {
  try {
      if(ctx.req.file){
         ctx.req.body.image_url=ctx.req.file.filename;
      }
      ctx.data = await ctx.models.Animal.createAnimal(ctx.req.body);
      if(!ctx.data) ctx.data= '0522';
  } catch (e) {                
      console.log(e);
      ctx.throw(400, '0501');      
  }    
};
post.updateAnimal = async (ctx, next) => {
  try {
      ctx.data = await ctx.models.Animal.updateAnimal(ctx.request.body);
  } catch (e) {
      ctx.throw(400, '0502');
  }
};
post.updateAnimalImg = async (ctx, next) => {
  try {
        if(ctx.req.file){
           ctx.req.body.image_url=ctx.req.file.filename;
        }
       ctx.data = await ctx.models.Animal.updateAnimalImg(ctx.req.body.animal_id,ctx.req.body.image_url);
  } catch(e) {
      ctx.throw(400, '0502');  
  }
};
post.updateAnimalImgDel = async (ctx, next) => {
    try {
         ctx.data = await ctx.models.Animal.updateAnimalImg(ctx.request.body.animal_id,ctx.request.body.image_url);
    } catch(e) {    
        ctx.throw(400, '0503');  
    }
};
post.createMultiAnimal = async (ctx, next) => {
    try { 
         if(ctx.request.body.upload && ctx.request.body.upload.length>0){
            ctx.data = await ctx.models.Animal.createMultiAnimal(ctx.request.body.upload);
         }else{ 
            ctx.throw(400, '0501');  
         }
           
    } catch(e) {    
        ctx.throw(400, '0501');    
    }   
};   
post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};  
  
module.exports = post;
