const CONTROL_METHOD = require('../../../methods/pest-control');
const Sequelize = require('sequelize');
const sequelize = require('../../../methods/sequelize').sequelize;
     
let put = {};
  
put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
  
    ctx.check.isInt('id');
    ctx.check.isInt('zone_time');
    ctx.check.isInt('medicine_time');
    ctx.check.isInt('compressor_time');
};
  
put.validate2 = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
};

put.checkPestControlCount = async (ctx, next) => {
    try {    
        const count = await ctx.models.PestControl.count({
            where: {
                state: 'run'
            }
        });
        if(count){
            ctx.throw(400, '0605');
        }
     
    } catch (e) {  
        ctx.throw(400, '0605');
    }
};    

put.updatePestControl = async (ctx, next) => {
    try {
        const data = await ctx.models.PestControl.updatePestControl(ctx.params.id,ctx.request.body);    
        const query = `select * from pest_controls where id = ${ctx.params.id} LIMIT 1`;
        const result = await sequelize.query(query, {
            type: Sequelize.QueryTypes.SELECT
        });   
        if(data){    
            const io = ctx.utils.io();
            if (io) io.emit('pest-control-list', result[0].house_id);
            await CONTROL_METHOD.setIsControlProcessing(result[0].house_id,null,false); 
        }    
    } catch (e) {       
        console.log(e);  
        ctx.throw(400, '0602');
    }           
};
   
put.updatePestControlFrost = async (ctx, next) => {
    try {
       const data =  await ctx.models.PestControl.update(ctx.request.body, {
            where: {
                id: ctx.params.id
            }   
        });  
        const query = `select * from pest_controls where id = ${ctx.params.id} LIMIT 1`;
        const result = await sequelize.query(query, {
            type: Sequelize.QueryTypes.SELECT
        });

        if(data){
            const io = ctx.utils.io();
            if (io) io.emit('pest-control-list',result[0].house_id); 
            await CONTROL_METHOD.setIsControlProcessing(result[0].house_id,null,false);    
        }          
    } catch (e) {
        //console.log(e);
        ctx.throw(400, '0602');
    }
};

put.updatePestControlAuto = async (ctx, next) => {
    try {  
        await ctx.models.PestControl.update(ctx.request.body, {
            where: {
                id: ctx.params.id  
            }   
        });
    } catch (e) {
        //console.log(e);
        ctx.throw(400, '0602');
    }
};
 
put.supplement = async (ctx, next) => {  
    ctx.json(null);             
    const io = ctx.utils.io();       
    //if (io) io.emit(`${ctx.alarm.sensor.position === 'in' ? 'inner' : 'outer'}-alarm`);
};  

module.exports = put;
