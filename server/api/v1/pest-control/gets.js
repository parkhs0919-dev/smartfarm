let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    
    // if (!ctx.query.size) ctx.query.size = ctx.meta.std.defaultSize;
    if (!ctx.query.offset) ctx.query.offset = 0;
    
    ctx.data = {};
};

gets.getPestControl = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.PestControl.getPestControl(ctx.query);
    } catch (e) {       
        ctx.throw(404, '0604');
    } 
};

gets.supplement = async (ctx, next) => {
    if (!ctx.data.count) {
        ctx.throw(404);
    }
    ctx.json(ctx.data);
};

module.exports = gets;
