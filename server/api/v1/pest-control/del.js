let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');

    ctx.data = {};
};

del.deletePestControl = async (ctx, next) => {
    try {
       
        await ctx.models.PestControl.destroy({
            where: {
                id: ctx.params.id
            }  
        });
        await ctx.models.PestControlZone.destroy({
            where: {
                pest_control_id : ctx.params.id
            }
        });           
    } catch (e) {
        ctx.throw(400, '0603');
    }
};

del.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    
    if (io) io.emit('pest-control-list');  
    //if (io) io.emit(`${ctx.alarm.sensor.position === 'in' ? 'inner' : 'outer'}-alarm`);
};

module.exports = del;
