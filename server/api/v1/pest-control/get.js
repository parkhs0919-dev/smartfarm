let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
  
};

get.getPestControlId = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.PestControl.getPestControlId(ctx.params.id);
   } catch(e) {
       ctx.throw(400, '0604');       
   }         
};   

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);   
};   

module.exports = get;
