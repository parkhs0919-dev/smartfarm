let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isInt('zone_time');
    ctx.check.isInt('medicine_time');
    ctx.check.isInt('compressor_time');
    
    ctx.data = {};
};

post.createPestControl = async (ctx, next) => {
    try {
        
        const createdData = await ctx.models.PestControl.createPestControl({
            ...ctx.request.body
        });
        if (createdData) {
            ctx.data = await createdData.reload();
            const io = ctx.utils.io();
            if (io) io.emit('pest-control-list', ctx.house_id);  
        } else {
            ctx.throw(400, '0601');   
        }
    } catch (e) {
        ctx.throw(400, '0601');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};
  
module.exports = post;
