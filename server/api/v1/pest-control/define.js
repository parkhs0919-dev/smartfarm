const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');
const get = require('./get');  
const post = require('./post');
const put = require('./put');
const del = require('./del');
   
const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                `pest_control_name`,
                `house_id`,
                `state`,
                `zone_time`,
                `medicine_time`,
                `compressor_time`,
                `is_frost`,
                `is_auto`,
                `max_temp`,
                `midnight_temp`,
                `current_temp`,
                `delay`,
                `zone_list`
            ],
            essential: [],
            explains: {
            },
            title: '방제기 설정 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getPestControl(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                `pest_control_name`,
                `house_id`,
                `state`,
                `zone_time`,
                `medicine_time`,
                `compressor_time`,
                `is_frost`,
                `is_auto`,
                `max_temp`,
                `midnight_temp`,
                `current_temp`,
                `delay`,
                `zone_list`
            ],
            essential: [],
            explains: {
            },
            title: '방제기 설정 상세 조회',
            state: 'develop'
        };
  
        if (!isParam) {
            try {  
                await get.validate(ctx, next);
                await get.getPestControlId(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },    
    post: (isParam) => async (ctx, next) => {
        const params = {   
            acceptable: [
                `pest_control_name`,
                `house_id`,
                `state`,
                `zone_time`,
                `medicine_time`,
                `compressor_time`,
                `is_frost`,
                `is_auto`,
                `max_temp`,
                `midnight_temp`,
                `current_temp`,
                `delay`,
                `zone_list`
            ],
            essential: [
                `pest_control_name`,
                `house_id`,
                `zone_time`,
                `medicine_time`,
                `compressor_time`,
                `zone_list` 
            ],
            explains: {
                'house_id': 'house_id ',
                'pest_control_name': 'pest_control_name ',
                'zone_time': '구간 밸브 시간 ',
                'medicine_time': '약재 밸브 시간 ',
                'compressor_time': '컴프레셔 밸브 시간 ',
                'zone_list' : '구역 목록'
            },
            title: '방제기 설정 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.createPestControl(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {   
                ctx.error(e.message, e.status);
            }   
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {   
            acceptable: [
                `pest_control_name`,
                `house_id`,
                `state`,
                `zone_time`,
                `medicine_time`,
                `compressor_time`,
                `is_frost`,
                `is_auto`,
                `max_temp`,
                `midnight_temp`,
                `current_temp`,
                `delay`,
                `zone_list`
            ],
            essential: [
                `pest_control_name`,
                `zone_time`,
                `medicine_time`,
                `compressor_time`,
                `zone_list` 
            ],
            explains: {
                'house_id': 'house_id ',
                'pest_control_name': 'pest_control_name ',
                'zone_time': '구간 밸브 시간 ',
                'medicine_time': '약재 밸브 시간 ',
                'compressor_time': '컴프레셔 밸브 시간 ',
                'zone_list' : '구역 목록'
            },
            title: '방제기 설정 수정',
            state: 'develop' 
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.checkPestControlCount(ctx, next);
                await put.updatePestControl(ctx, next);
                await put.supplement(ctx, next);   
            } catch (e) {  
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put_frost: (isParam) => async (ctx, next) => {
        const params = {   
            acceptable: [
                `pest_control_name`,
                `house_id`,
                `state`,
                `zone_time`,
                `medicine_time`,
                `compressor_time`,
                `is_frost`,
                `is_auto`,
                `max_temp`,
                `midnight_temp`,
                `current_temp`,
                `delay`,
                `zone_list`
            ],
            essential: [
                `is_frost`,
                `is_auto`,
                `max_temp`,
                `midnight_temp`,
                `current_temp`,
                `delay`
            ],
            explains: {
                'is_frost': '서리방제 옵션 사용 여부 ',
                'is_auto': '자동/수동 ',
                'max_temp': '전일 최고 온도 ',
                'midnight_temp': '당일 자정 온도',
                'current_temp': '현재 온도 ',
                'delay': '지연 시간 (분) '
            },
            title: '서리 방제기 자동 옵션 설정 수정',
            state: 'develop' 
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.checkPestControlCount(ctx, next);  
                await put.updatePestControlFrost(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {  
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },    
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'pest_control_id'
            },
            title: '방제기설정 삭제',
            state: 'develop'
        };  

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.deletePestControl(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:id', api.get());  
router.get('/', api.gets());  
router.post('/', api.post());
router.put('/:id', api.put());
router.put('/frost/:id', api.put_frost());
router.del('/:id', api.del());         

module.exports.router = router;
module.exports.api = api;
