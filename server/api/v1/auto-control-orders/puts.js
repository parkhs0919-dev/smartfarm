let puts = {};

puts.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.request.body.ids = ctx.request.body.ids.split(',');
};

puts.checkControls = async (ctx, next) => {
    try {
        const count = await ctx.models.Control.count({
            where: {
                house_id: ctx.request.body.house_id,
                mode: 'auto'
            }
        });
        if (count) {
            ctx.throw(400, '0250');
        }
    } catch (e) {
        ctx.throw(400, '0250');
    }
};

puts.updateAutoControlOrders = async (ctx, next) => {
    try {
        await ctx.models.AutoControl.updateAutoControlOrders(ctx.request.body);
    } catch (e) {
        const io = ctx.utils.io();
        if (io) io.emit('auto-controls', {
            house_id: ctx.request.body.house_id
        });
        ctx.throw(400, '0241');
    }
};

puts.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit('auto-controls', {
        house_id: ctx.request.body.house_id
    });
};

module.exports = puts;
