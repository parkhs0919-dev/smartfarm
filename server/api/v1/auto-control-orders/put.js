let puts = {};

puts.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.isInt('order');
};

puts.getHouseId = async (ctx, next) => {
    try {
        const autoControl = await ctx.models.AutoControl.findByPk(ctx.params.id);
        if (autoControl) {
            ctx.house_id = autoControl.house_id;
        } else {
            ctx.throw(404, '0105');
        }
    } catch (e) {
        ctx.throw(404, '0105');
    }
};

puts.checkControls = async (ctx, next) => {
    try {
        const count = await ctx.models.Control.count({
            where: {
                house_id: ctx.house_id,
                mode: 'auto'
            }
        });
        if (count) {
            ctx.throw(400, '0250');
        }
    } catch (e) {
        ctx.throw(400, '0250');
    }
};

puts.updateAutoControlOrder = async (ctx, next) => {
    try {
        await ctx.models.AutoControl.updateAutoControlOrder(ctx.params.id, ctx.house_id, ctx.request.body.order);
    } catch (e) {
        const io = ctx.utils.io();
        if (io) io.emit('auto-controls', {
            house_id: ctx.house_id
        });
        ctx.throw(400, '0241');
    }
};

puts.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit('auto-controls', {
        house_id: ctx.house_id
    });
};

module.exports = puts;
