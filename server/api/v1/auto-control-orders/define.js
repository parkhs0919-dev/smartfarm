const Router = require('koa-router');
const router = new Router();

const puts = require('./puts');
const put = require('./put');

const api = {
    puts: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'ids'
            ],
            essential: [
                'house_id',
                'ids'
            ],
            explains: {
                'house_id': 'house id',
                'ids': '자동제어 ids (,)로 구분, 순서대로'
            },
            title: '자동제어 order 변경',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await puts.validate(ctx, next);
                await puts.checkControls(ctx, next);
                await puts.updateAutoControlOrders(ctx, next);
                await puts.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'id',
                'order'
            ],
            essential: [
                'id',
                'order'
            ],
            params: ['id'],
            explains: {
                'id': 'auto-control id',
                'order': '우선순위 변경 대상'
            },
            title: '자동제어 order 변경',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.getHouseId(ctx, next);
                await put.checkControls(ctx, next);
                await put.updateAutoControlOrder(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
};

router.put('/', api.puts());
router.put('/:id', api.put());

module.exports.router = router;
module.exports.api = api;
