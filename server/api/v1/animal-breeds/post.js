let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};  

post.createAnimalBreed = async (ctx, next) => {
  try {
      ctx.data = await ctx.models.AnimalBreed.createAnimalBreed(ctx.request.body);
  } catch (e) {   
      ctx.throw(400, '0518');
  }    
};          

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = post;
