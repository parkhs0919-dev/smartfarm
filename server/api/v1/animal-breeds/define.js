const Router = require('koa-router');
const router = new Router();

const post = require('./post');
const get = require('./get');
const gets = require('./gets');
const del = require('./del');
const put = require('./put');
  
const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [ 
              'animal_id',
              'start_date',
              'ende_date',
              'type',
              'memo'
            ],
            essential: [
                'animal_id',
                'start_date',
                'ende_date',
                'type',
                'memo'
            ],
            explains: {
                'animal_id' : 'animal_id',
                'start_date' : 'start_date',
                'ende_date' : 'ende_date',
                'type' : 'type',
                'memo' :'memo'   
            },
            title: '번식 상세 조회',
            state: 'develop'
        };   
  
        if (!isParam) {
            try {   
                //await post.validate(ctx, next);
                await get.getAnimalBreedCalendar(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {     
            return params;
        }
      }, 
      gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [ 
              'animal_id',
              'start_date',
              'ende_date',
              'type',
              'memo'
            ],
            essential: [
                'animal_id',
                'start_date',
                'ende_date',
                'type',
                'memo'
            ],
            explains: {
                'animal_id' : 'animal_id',
                'start_date' : 'start_date',
                'ende_date' : 'ende_date',
                'type' : 'type',
                'memo' :'memo'   
            },
            title: '번식 조회',
            state: 'develop'
        };   
  
        if (!isParam) {
            try {   
                //await post.validate(ctx, next);
                await gets.getAnimalbreed(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {      
                ctx.error(e.message, e.status);
            }
        } else {    
            return params;
        }
      },       
      post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
              'animal_id',
              'start_date',
              'ende_date',
              'type',
              'memo'
            ],
            essential: [
                'animal_id',
                'start_date',
                'ende_date',
                'type',
                'memo'
            ],
            explains: {
                'animal_id' : 'animal_id',
                'start_date' : 'start_date',
                'ende_date' : 'ende_date',
                'type' : 'type',
                'memo' :'memo'   
            },
            title: '번식 입력',
            state: 'develop'
        }; 
  
        if (!isParam) {
            try {
                //await post.validate(ctx, next);
                await post.createAnimalBreed(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {          
            return params;
        }
      },
      put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
              'start_date',
              'ende_date',
              'type',
              'memo'           
            ],    
            essential: [
                'start_date',
                'ende_date',
                'type',
                'memo'
            ],
            explains: {
                'start_date' : 'start_date',
                'ende_date' : 'ende_date',
                'type' : 'type',
                'memo' :'memo'   
            },
            title: '번식 수정',
            state: 'develop'
        };   
  
        if (!isParam) {
            try {
                //await post.validate(ctx, next);
                await put.updateAnimalBreed(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }  
        } else {          
            return params;
        }
      },     
      del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
              'id',
            ],
            essential: [
              'id',
            ],
            explains: {
              'id' : 'id',

            },
            title: '번식 삭제',
            state: 'develop'
          };
  
          if (!isParam) {
              try {
                  //await del.validate(ctx, next);
                  await del.deleteAnimalBreed(ctx, next);
                  await del.supplement(ctx, next);
              } catch (e) {
                  ctx.error(e.message, e.status);
              }
          } else {
              return params;   
          }
      },            
};          

router.get('/:id', api.gets());
router.get('/detail/:id', api.get());
router.post('/', api.post());   
router.put('/:id', api.put()); 
router.delete('/:id', api.del());  
       
module.exports.router = router;
module.exports.api = api;
