let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};
gets.getAnimalbreed = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.Animal.getAnimalBreed(ctx.params.id,ctx.query);
    } catch (e) {    
        console.log(e);
        ctx.throw(400, '0519');                 
    }   
};     

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
