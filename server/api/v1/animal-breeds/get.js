let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
  
};

get.getAnimalBreedCalendar = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.AnimalBreed.getAnimalBreedCalendar(ctx.params.id,ctx.query.date);
   } catch(e) {
       ctx.throw(400, '0519');       
   }   
};   

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);   
};   

module.exports = get;
