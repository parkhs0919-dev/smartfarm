let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};
put.updateAnimalBreed = async (ctx, next) => {
    try {
      ctx.data = await ctx.models.AnimalBreed.updateAnimalBreed(ctx.params.id ,ctx.request.body);
    } catch (e) {
        ctx.throw(400, '0521');    
    }
};     

put.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = put;
