let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.data = {};
};

gets.getSensorAvg = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.SensorAvg.getSensorAvg(ctx.query);
        
    } catch (e) {         
        ctx.throw(404, '0609');
    }      
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
