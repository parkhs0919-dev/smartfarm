let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.check.isInt('house_id');
    ctx.check.len('type', 1, 191);
    ctx.check.len('name', 1, 191);
    ctx.data = {};
};  

post.createSensorAvg = async (ctx, next) => {
    try {       
        const createdData = await ctx.models.SensorAvg.createSensorAvg({
            ...ctx.request.body
        });
        if (createdData) {
            ctx.data = await createdData.reload();
            const io = ctx.utils.io();
            const house_id=ctx.request.body.house_id;
            if (io) io.emit('sensor_avg', house_id);  
        } else {   
            ctx.throw(400, '0606');   
        } 
    } catch (e) {
        ctx.throw(400, '0606');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};
  
module.exports = post;
