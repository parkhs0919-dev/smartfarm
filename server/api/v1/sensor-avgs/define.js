const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');
const get = require('./get');  
const post = require('./post');
const put = require('./put');
const del = require('./del');
   
const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                `id`,
                `sensor_name`,
                `value`
            ],
            essential: [],
            explains: {
            },
            title: '복수 센서 평균 설정 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {                
                await gets.validate(ctx, next);
                await gets.getSensorAvg(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                `id`,
                `sensor_name`,
                `value`

            ],
            essential: [],
            explains: {
            },
            title: '복수 센서 평균 설정 상세 조회',
            state: 'develop'
        };
  
        if (!isParam) {
            try {  
                await get.validate(ctx, next);
                await get.getSensorAvgId(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },    
    post: (isParam) => async (ctx, next) => {
        const params = {     
            acceptable: [
                `type`,
                `name`,
                `items`,
                `house_id`
            ],
            essential: [
                `type`,
                `name`,
                `items`,
                `house_id`,
            ],
            explains: {
                'type': '타입  ',
                'name': '그룹 명 ',
                'items': 'items ',
                'house_id':'하우스 id',
            },
            title: '복수 센서 평균 설정 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.createSensorAvg(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {   
                ctx.error(e.message, e.status);
            }   
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {   
            acceptable: [
                `type`,
                `name`,  
                `items`,
            ],
            essential: [
                `type`,
                `name`,
                `items`,
            ],
            explains: {
                'type': '타입  ',
                'name': '그룹 명 ',
                'items': 'items '
            },
            title: '복수 센서 평균 설정 수정',
            state: 'develop' 
        };

        if (!isParam) {
            try {
                
                await put.validate(ctx, next);
                await put.updateSensorAvg(ctx, next);
                await put.supplement(ctx, next);   
            } catch (e) {  
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },  
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'sensor_avg_id'
            },
            title: '복수 센서 평균설정 삭제',
            state: 'develop'
        };  

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.deleteSensorAvg(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:id', api.get());  
router.get('/', api.gets());  
router.post('/', api.post());
router.put('/:id', api.put());
router.del('/:id', api.del());         

module.exports.router = router;
module.exports.api = api;
