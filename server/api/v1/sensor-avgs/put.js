const CONTROL_METHOD = require('../../../methods/pest-control');
const Sequelize = require('sequelize');
const sequelize = require('../../../methods/sequelize').sequelize;
     
let put = {};
  
put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
  
    ctx.check.isInt('id');
};
  
put.validate2 = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
};

put.updateSensorAvg = async (ctx, next) => {
    try {  
        
        const data = await ctx.models.SensorAvg.updateSensorAvg(ctx.params.id,ctx.request.body);         
        if(data){    
            const io = ctx.utils.io();
            const house_id=ctx.request.body.house_id;
            if (io) io.emit('sensor_avg', house_id);
        }    
    } catch (e) {                  
        ctx.throw(400, '0607');
    }             
};

put.supplement = async (ctx, next) => {  
    ctx.json(null);                 
};  

module.exports = put;
