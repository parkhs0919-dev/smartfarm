let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.check.isInt('control_id');
    ctx.check.isEnum('state', ['open', 'close','stop','on','off']);

    ctx.data = {};
};

post.createAlarm = async (ctx, next) => {
    try {
        const body = ctx.request.body;
        const data = await ctx.models.ControlAlarm.getControlAlarmsDuplicateCount(body);
        if(data.count==0){
            const createdData = await ctx.models.ControlAlarm.create({
                ...body,
            }, {
                include: [{
                    model: ctx.models.Control,
                    as: 'control'
                }]
            });
            ctx.data = await createdData.reload();
        }else{
            ctx.throw(400, '0287');
        }

    } catch (e) {
        ctx.throw(400, '0287');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = post;
