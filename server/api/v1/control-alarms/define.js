const Router = require('koa-router');
const router = new Router();

const get = require('./get');
const gets = require('./gets');
const post = require('./post');
const put = require('./put');
const del = require('./del');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'control id'
            },
            title: 'control 알람 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.getAlarm(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'control_id',
                'text',
                'state'
            ],
            essential: [],
            explains: {
                'control_id': 'control id',
                'text': '음성 텍스트',
                'state': '상태(on, off, open, close, stop)',
            },
            title: '장치제어 알람 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getAlarms(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'control_id',
                'text',
                'state'
            ],
            essential: [
                'control_id',
                'text',
                'state'
            ],
            explains: {
                'control_id': 'control id',
                'text': '음성 텍스트',
                'state': '상태(on, off, open, close, stop)',
            },
            title: '장치제어알람 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.createAlarm(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'control_id',
                'text',
                'state'
            ],
            essential: [
            ],
            params: ['id'],
            explains: {
                'id': 'alarm id',
                'control_id': 'control id',
                'text': '음성 텍스트',
                'state': '상태(on, off, open, close, stop)',
            },
            title: '장치제어알람 수정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.getAlarm(ctx, next);
                await put.updateAlarm(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'alarm id'
            },
            title: '장치제어알람 삭제',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.getAlarm(ctx, next);
                await del.deleteAlarm(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:id', api.get());
router.get('/', api.gets());
router.post('/', api.post());
router.put('/:id', api.put());
router.del('/:id', api.del());

module.exports.router = router;
module.exports.api = api;
