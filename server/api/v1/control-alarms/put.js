let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.isInt('control_id');
    ctx.check.isEnum('state', ['open', 'close','stop','on','off']);
};

put.getAlarm = async (ctx, next) => {
    try {
        ctx.alarm = await ctx.models.ControlAlarm.findByPk(ctx.params.id, {
            include: [{
                model: ctx.models.Control,
                as: 'control'
            }]
        });
        if (!ctx.alarm) {
            ctx.throw(404, '0286');
        }
    } catch (e) {
        ctx.throw(404, '0286');
    }
};

put.updateAlarm = async (ctx, next) => {
    try {
        await ctx.models.ControlAlarm.update(ctx.request.body, {
            where: {
                id: ctx.params.id
            }
        });
    } catch (e) {
        ctx.throw(400, '0288');
    }
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = put;
