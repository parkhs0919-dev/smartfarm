let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('control_id');
    ctx.check.isEnum('state', ['open', 'close','stop','on','off']);

    // if (!ctx.query.size) ctx.query.size = ctx.meta.std.defaultSize;
    if (!ctx.query.offset) ctx.query.offset = 0;

    ctx.data = {};
};

gets.getAlarms = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.ControlAlarm.getControlAlarms(ctx.query);
    } catch (e) {
        ctx.throw(404, '0286');
    }
};

gets.supplement = async (ctx, next) => {
    if (!ctx.data.count) {
        ctx.throw(404);
    }
    ctx.json(ctx.data);
};

module.exports = gets;
