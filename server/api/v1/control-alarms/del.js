let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');

    ctx.data = {};
};

del.getAlarm = async (ctx, next) => {
    try {
        ctx.alarm = await ctx.models.ControlAlarm.findByPk(ctx.params.id, {
            include: [{
                model: ctx.models.Control,
                as: 'control'
            }]
        });
        if (!ctx.alarm) {
            ctx.throw(404, '0289');
        }
    } catch (e) {
        ctx.throw(404, '0289');
    }
};

del.deleteAlarm = async (ctx, next) => {
    try {
        await ctx.models.ControlAlarm.destroy({
            where: {
                id: ctx.params.id
            }
        });
    } catch (e) {
        ctx.throw(400, '0219');
    }
};

del.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = del;
