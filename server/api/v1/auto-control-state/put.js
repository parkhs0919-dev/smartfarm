let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.isEnum('state', ['on', 'off']);
};

put.getAutoControl = async (ctx, next) => {
    try {
        const autoControl = await ctx.models.AutoControl.findByPk(ctx.params.id);
        if (!autoControl) {
            ctx.throw(404, '0105');
        }
        ctx.house_id = autoControl.house_id;
    } catch (e) {
        ctx.throw(404, '0105');
    }
};

put.checkControls = async (ctx, next) => {
    try {
        const count = await ctx.models.Control.count({
            where: {
                house_id: ctx.house_id,
                mode: 'auto'
            }
        });
        if (count) {
            ctx.throw(400, '0250');
        }
    } catch (e) {
        ctx.throw(400, '0250');
    }
};

put.updateAutoControlState = async (ctx, next) => {
    try {
        await ctx.models.AutoControl.updateAutoControlState(ctx.request.body.state, ctx.params.id);
        // await ctx.models.AutoControl.update({
        //     state: ctx.request.body.state
        // }, {
        //     where: {
        //         id: ctx.params.id
        //     }
        // });
    } catch (e) {
        ctx.throw(400, '0239');
    }
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit('auto-controls', {
        house_id: ctx.house_id
    });
};

module.exports = put;
