const Router = require('koa-router');
const router = new Router();

const put = require('./put');

const api = {
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'state'
            ],
            essential: [
                'state'
            ],
            params: ['id'],
            explains: {
                'id': 'auto-control id',
                'state': 'state (on, off)',
            },
            title: '자동제어 스위치',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.getAutoControl(ctx, next);
                await put.checkControls(ctx, next);
                await put.updateAutoControlState(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.put('/:id', api.put());

module.exports.router = router;
module.exports.api = api;
