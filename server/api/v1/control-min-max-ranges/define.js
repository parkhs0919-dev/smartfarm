const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');
const post = require('./post');
const put = require('./put');
const del = require('./del');

const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'state'
            ],
            essential: [],
            explains: {
                'house_id': 'house id',
                'state': '(on / off)'
            },
            title: '최소/최대 개폐 범위 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getControlMinMaxRanges(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'sensor_id',
                'name',
                'value'
            ],
            essential: [
                'house_id',
                'sensor_id',
                'name',
                'value'
            ],
            explains: {
                'house_id': 'house id',
                'sensor_id': 'sensor id',
                'name': '이름',
                'value': '값',
            },
            title: '최소/최대 개폐 범위 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.createControlMinMaxRange(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'sensor_id',
                'state',
                'name',
                'value'
            ],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'controlMinMaxRange id',
                'sensor_id': 'sensor id',
                'state': '(on / off)',
                'name': '이름',
                'value': '값',
            },
            title: '최소/최대 개폐 범위 수정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.updateControlMinMaxRange(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'controlMinMaxRange id'
            },
            title: '최소/최대 개폐 범위 삭제',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.check(ctx, next);
                await del.deleteControlMinMaxRange(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.gets());
router.post('/', api.post());
router.put('/:id', api.put());
router.del('/:id', api.del());

module.exports.router = router;
module.exports.api = api;
