let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isEnum('state', ['on', 'off']);

    ctx.data = {};
};

gets.getControlMinMaxRanges = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.ControlMinMaxRange.getControlMinMaxRanges(ctx.query);
    } catch (e) {
        ctx.throw(404, '0110');
    }
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
