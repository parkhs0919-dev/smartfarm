let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');

    ctx.data = {};
};

del.check = async (ctx, next) => {
    try {
        if ((await ctx.models.AutoControlItemMinMax.count({
            where: {
                control_min_max_range_id: ctx.params.id
            }
        })) || (await ctx.models.AutoControlStepMinMax.count({
            where: {
                control_min_max_range_id: ctx.params.id
            }
        }))) {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(400, '0278');
    }
};

del.deleteControlMinMaxRange = async (ctx, next) => {
    try {
        await ctx.models.ControlMinMaxRange.destroy({
            where: {
                id: ctx.params.id
            },
            cascade: true
        });
    } catch (e) {
        ctx.throw(400, '0277');
    }
};

del.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = del;
