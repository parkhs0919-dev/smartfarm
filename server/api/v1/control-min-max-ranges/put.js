let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.isInt('sensor_id');
    ctx.check.isEnum('state', ['on', 'off']);
    ctx.check.len('name', 1, 191);
    ctx.check.isFloat('value');
    ctx.check.isEnum('condition', ['over', 'under', 'equal']);

    ctx.data = {};
};

put.updateControlMinMaxRange = async (ctx, next) => {
    try {
        await ctx.models.ControlMinMaxRange.update(ctx.request.body, {
            where: {
                id: ctx.params.id
            }
        });
    } catch (e) {
        ctx.throw(400, '0276');
    }
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = put;
