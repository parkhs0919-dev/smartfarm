let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isInt('sensor_id');
    ctx.check.len('name', 1, 191);
    ctx.check.isFloat('value');
    ctx.check.isEnum('condition', ['over', 'under', 'equal']);

    ctx.data = {};
};

post.createControlMinMaxRange = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.ControlMinMaxRange.create(ctx.request.body);
    } catch (e) {
        ctx.throw(400, '0275');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = post;
