let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.data = {};
};

gets.getSensorAvgItemType = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.SensorAvgItem.getSensorAvgItemType(ctx.query);
    } catch (e) {         
        ctx.throw(404, '0610');     
    }       
};  

gets.supplement = async (ctx, next) => {    
    
    ctx.json(ctx.data);
};

module.exports = gets;
  