const Router = require('koa-router');
const router = new Router();

const gets = require('./gets'); 
   
const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                `id`,
                `sensor_name`,
                `value`
            ],
            essential: [],
            explains: {
            },
            title: '복수 센서 평균 설정 sensor 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getSensorAvgItemType(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {   
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};
 
router.get('/', api.gets());       

module.exports.router = router;
module.exports.api = api;
