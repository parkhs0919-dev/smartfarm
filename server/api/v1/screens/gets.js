const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const CONFIG = require('../../../config');
let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');
    ctx.check.isBoolean('is_visible');
    ctx.sanitize.toBoolean('is_visible');

    ctx.data = null;
};

gets.getScreens = async (ctx, next) => {
    try {
        const SCREEN = ctx.meta.std.screen;
        let where = {
            type: SCREEN.enumTypes.slice(),
            house_id: ctx.query.house_id
        };
        if (ctx.query.is_visible !== undefined) {
            where.is_visible = ctx.query.is_visible;
        }

        if (!CONFIG.control.isUsePestControl) {
            where.type.splice(where.type.indexOf(SCREEN.typePesticide), 1);
        }
        if (!CONFIG.fluid.isUseScreen) {
            where.type.splice(where.type.indexOf(SCREEN.typeFluid), 1);
        }

        const screens = await ctx.models.Screen.findAll({
            order: [['order', 'ASC']],
            where
        });
        ctx.data = {
            count: screens.length,
            rows: screens
        };
    } catch (e) {
        ctx.throw(404, '0108');
    }
};

gets.supplement = async (ctx, next) => {

    
    ctx.json(ctx.data);
};

module.exports = gets;
