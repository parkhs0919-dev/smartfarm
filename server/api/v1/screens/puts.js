let puts = {};

puts.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');

    if (ctx.request.body.screens) {
        // if (ctx.request.body.screens.length !== ctx.meta.std.screen.enumTypes.length) {
        //     ctx.throw(400, '0252');
        // }
    } else {
        ctx.throw(400, '0252');
    }
};

puts.updateScreens = async (ctx, next) => {
    try {
        await ctx.models.Screen.updateScreens(ctx.request.body.screens, ctx.request.body.house_id);
    } catch (e) {
        ctx.throw(400, '0251');
    }
};

puts.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit('screens');
};

module.exports = puts;
