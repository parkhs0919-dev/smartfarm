const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');
const puts = require('./puts');

const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'is_visible'
            ],
            essential: [],
            explains: {
                'house_id': 'house id',
                'is_visible': '화면 표시 여부 (true, false)'
            },
            title: '화면 순서 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await gets.validate(ctx, next);
                await gets.getScreens(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    puts: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'house_id',
                'screens'
            ],
            essential: [
                'house_id',
                'screens'
            ],
            explains: {
                'house_id': 'house id',
                'screens': 'screens [{type: "cctv", is_visible: true}]'
            },
            title: '화면 순서 및 표시 수정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await puts.validate(ctx, next);
                await puts.updateScreens(ctx, next);
                await puts.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.gets());
router.put('/', api.puts());

module.exports.router = router;
module.exports.api = api;
