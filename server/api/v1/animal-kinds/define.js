const Router = require('koa-router');
const router = new Router();

const gets = require('./gets');

const api = {
    gets: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'id',
            'name'
          ],
          essential: [
            'id',
            'name'
          ],
          explains: {
            'id' : 'id',
            'name' : 'name'
          },
          title: '품종 조회',
          state: 'develop'
      };

      if (!isParam) {
          try {
              //await post.validate(ctx, next);
              await gets.getAnimalKind(ctx, next);
              await gets.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }
    }
};

router.get('/', api.gets());


module.exports.router = router;
module.exports.api = api;
