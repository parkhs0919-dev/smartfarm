let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};
gets.getAnimalKind = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.AnimalKind.getAnimalKind();
    } catch (e) {
        ctx.throw(400, '0234');
    }
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};
   
module.exports = gets;
