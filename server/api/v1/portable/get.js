const Sequelize = require('sequelize');
let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.data = null;
};
  
get.createPortable = async (ctx, next) => {
    try {
        if(ctx.query.code && ctx.query.model){
            let contents ='' ;    
            if(ctx.query.code) contents +=' [업체코드] : '+ctx.query.code ;
            if(ctx.query.model) contents +='  [모델번호(key)] : '+ctx.query.model ;
            if(ctx.query.it_1) contents +='  [EC] : '+ctx.query.it_1 ;  
            if(ctx.query.it_2) contents +='  [지온] : '+ctx.query.it_2 ;
            if(ctx.query.it_3) contents +='  [지습] : '+ctx.query.it_3 ;  
            if(ctx.query.it_4) contents +='  [배터리] : '+ctx.query.it_4 ;  
            ctx.models.Report.createPortableReport('LOCAL', '1', 'noti', contents); 
            ctx.data ={Result:'0000',ResultMsg:'등록완료'}  
        }else{
            ctx.data ={Result:'9999',ResultMsg:'등록실패'} 
        }   
    } catch (e) {        
        //console.log(e);       
        ctx.data ={Result:'9999',ResultMsg:'등록오류'}  
        //ctx.throw(404, '0108');  
    }                
};  
       
get.supplement = async (ctx, next) => {
    ctx.portable(ctx.data);  
};       
    
module.exports = get;
