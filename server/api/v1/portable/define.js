const Router = require('koa-router');
const router = new Router();

const get = require('./get');
  
const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'code',
                'model',
                'it_1',
                'it_2',
                'it_3',
                'it_4'
            ],
            essential: [],
            explains: {
                'code': '발급한 key값',
                'model': '장비의 고유 값',
                'it_1': 'EC 값 ',
                'it_2': '지온 값',
                'it_3': '지습 값',
                'it_4': '배터리 값'
            },
            title: 'portable ',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.createPortable(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {  
                ctx.error(e.message, e.status);
            }  
        } else {
            return params;
        }
    }
};

router.get('/', api.get());

module.exports.router = router;
module.exports.api = api;
