const Router = require('koa-router');
const router = new Router();

const post = require('./post');
const get = require('./get');
const gets = require('./gets');
const put = require('./put');
const del = require('./del');
  
const api = {
    gets: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                `user_id`,
                `id`,
                `password`,
                `name`,
                `grade`,
                `is_use`,
                'offset',
                'size'                
            ],
            essential: [],
            explains: {
                'offset': 'offset',
                'size': 'size'                
            },
            title: '유저 설정 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {                
                
                await gets.validate(ctx, next);
                await gets.getUsers(ctx, next);
                await gets.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                `user_id`,
                `id`,
                `password`,
                `name`,
                `grade`,
                `is_use`
            ],
            essential: [],
            explains: {
            },
            title: '유저 설정 상세 조회',
            state: 'develop'
        };
  
        if (!isParam) {
            try {  
                
                await get.validate(ctx, next);
                await get.getUserId(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {    
            return params;
        }
    },    
    post: (isParam) => async (ctx, next) => {
        const params = {     
            acceptable: [
                `id`,
                `password`,
                `name`,
            ],
            essential: [
                `id`,
                `password`,
                `name`,
            ],
            explains: {
                'id': 'id  ',
                'password': 'password ',
                'name': 'name ',
            },
            title: '유저 등록',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await post.validate(ctx, next);
                await post.createUser(ctx, next);
                await post.supplement(ctx, next);
            } catch (e) {   
                ctx.error(e.message, e.status);
            }   
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {   
            acceptable: [
                `id`,
                `password`,
                `name`,
            ],
            essential: [
                `id`,
                `password`,
                `name`,
            ],
            explains: {
                'id': 'id  ',
                'password': 'password ',
                'name': 'name ',
            },
            title: '유저 수정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                
                await put.validate(ctx, next);
                await put.updateUser(ctx, next);
                await put.supplement(ctx, next);   
            } catch (e) {  
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    putPw: (isParam) => async (ctx, next) => {
        const params = {   
            acceptable: [
                `user_id`,
                `oldPassword`,
                `newPassword`,
            ],
            essential: [
                `user_id`,
                `oldPassword`,
                `newPassword`,
            ],
            explains: {
                'user_id': 'user_id  ',
                'oldPassword': 'oldPassword ',
                'newPassword': 'newPassword ',
            },
            title: '유저 비번 수정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                
                await put.validatePw(ctx, next);
                await put.updateUserPw(ctx, next);
                await put.supplement(ctx, next);   
            } catch (e) {  
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },      
    del: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'id'
            },
            title: '유저 삭제',
            state: 'develop'
        };  

        if (!isParam) {
            try {
                await del.validate(ctx, next);
                await del.deleteUser(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:id', api.get());  
router.get('/', api.gets());  
router.post('/', api.post());
router.put('/:id', api.put());
router.put('/pw/:id', api.putPw());
router.del('/:id', api.del()); 
     
module.exports.router = router;
module.exports.api = api;
