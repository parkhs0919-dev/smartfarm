const Sequelize = require('sequelize');
const sequelize = require('../../../methods/sequelize').sequelize;
     
let put = {};
  
put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.check.isInt('id');
};
put.validatePw = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.check.isInt('id');
};  
  
put.updateUser = async (ctx, next) => {
    try {  
        const data = await ctx.models.User.updateUser(ctx.params.id,ctx.request.body);             
    } catch (e) {      
        console.log(e)            
        ctx.throw(400, '0612');
    }             
};

put.updateUserPw = async (ctx, next) => {
    try {  
        const data = await ctx.models.User.updateUserPw(ctx.params.id,ctx.request.body);             
    } catch (e) {      
        console.log(e)               
        ctx.throw(400, '0616');
    }             
};

put.supplement = async (ctx, next) => {  
    ctx.json(null);                 
};  

module.exports = put;
