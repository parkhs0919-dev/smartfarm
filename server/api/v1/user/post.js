let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isEnum('platform', ['farm', 'animal']);
    ctx.check.len('id', 1, 191);
    ctx.check.len('name', 1, 191);
    ctx.check.len('password', 4, 20);
};   

post.createUser = async (ctx, next) => {
    try {        
        const createData = await ctx.models.User.createUser(ctx.request.body);  
    } catch (e) {
        ctx.throw(400, '0611');
    }
};

post.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = post;
