let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.data = {};
};

del.deleteUser = async (ctx, next) => {
    try {
        const data = await ctx.models.User.deleteUser(ctx.params.id);          
    } catch (e) {
        ctx.throw(400, '0613');
    }
};
   
del.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = del;
