let gets = {};

gets.validate = (ctx) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');    
    ctx.check.isInt('size');
    ctx.check.isInt('offset');

    ctx.data = {};
};

gets.getUsers = async (ctx, next) => {
    try {        
        ctx.data = await ctx.models.User.getUsers(ctx.query);
    } catch (e) {   
        ctx.throw(400, '0614');  
    }
};

gets.supplement = async (ctx, next) => {
    if (!ctx.data.count) {
        ctx.throw(404);
    }
    ctx.json(ctx.data);
};

module.exports = gets;
