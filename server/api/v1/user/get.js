let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    } 
};

get.getUserId = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.User.getUserId(ctx.params.id,ctx.query);
   } catch(e) {
       ctx.throw(400, '0614');                
   }               
};     

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);   
};   

module.exports = get;
  