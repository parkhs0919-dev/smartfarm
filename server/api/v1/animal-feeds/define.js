const Router = require('koa-router');
const router = new Router();

const post = require('./post');
const gets = require('./gets');
const del = require('./del');

const api = {
    post: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_id',
            'date',
            'feed',
            'unit',
            'memo'
          ],
          essential: [
            'animal_id',
            'date',
            'feed',
            'unit',
            'memo'
          ],
          explains: {
            'animal_id' : 'animal_id',
            'date': 'date',
            'feed':'feed',
            'unit' :'unit',
            'memo': 'memo'
          },
          title: '급이 입력',
          state: 'develop'
      };


      if (!isParam) {
          try {
              //await post.validate(ctx, next);
              await post.createAnimalFeed(ctx, next);
              await post.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }
    },
    gets: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_id',
            'date',
            'feed',
            'memo'
          ],
          essential: [
            'animal_id',
            'date',
            'feed',
            'memo'
          ],
          explains: {
            'animal_id' : 'animal_id',
            'date': 'date',
            'feed':'feed',
            'memo': 'memo'
          },
          title: '급이 조회',
          state: 'develop'
      };

      if (!isParam) {
          try {
              //await post.validate(ctx, next);
              await gets.getAnimalFeed(ctx, next);
              await gets.supplement(ctx, next);
          } catch (e) {
              ctx.error(e.message, e.status);
          }
      } else {
          return params;
      }
    },
    del: (isParam) => async (ctx, next) => {
      const params = {
          acceptable: [
            'animal_id',
          ],
          essential: [
            'animal_id',
          ],
          explains: {
            'animal_id' : 'animal_id',
          },
          title: ' 삭제',
          state: 'develop'
        };

        if (!isParam) {
            try {
                //await del.validate(ctx, next);
                await del.deleteAnimalFeed(ctx, next);
                await del.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);  
            }
        } else {
            return params;
        }
    }    
};

router.post('/', api.post());
router.get('/:id', api.gets());
router.delete('/:id', api.del());  

module.exports.router = router;
module.exports.api = api;
