let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};
gets.getAnimalFeed = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.AnimalFeed.getAnimalFeed(ctx.params.id,ctx.query);
    } catch (e) {
        ctx.throw(400, '0513');  
    }
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = gets;
