let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
};

post.createAnimalFeed = async (ctx, next) => {
  try {
      ctx.data = await ctx.models.AnimalFeed.createAnimalFeed(ctx.request.body);
  } catch (e) {
      ctx.throw(400, '0512');
  }      
};  

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = post;
