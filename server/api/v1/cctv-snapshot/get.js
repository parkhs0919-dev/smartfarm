const fs = require('fs');
const rimraf = require('rimraf');
const cctvPath = 'cctv';
let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
};

get.findCctv = async (ctx, next) => {
    try {
        ctx.cctv = await ctx.models.Cctv.findByPk(ctx.params.id);
        if (!ctx.cctv) {
            ctx.throw();
        }
    } catch (e) {
        ctx.throw(404, '0102');
    }
};

get.prevProcess = async (ctx, next) => {
    ctx.today = ctx.utils.filter.date(new Date(), 'yyyy-MM-dd');
    fs.readdirSync(cctvPath).forEach(dir => {
        if (dir !== ctx.today) {
            rimraf.sync(cctvPath + '/' + dir);
        }
    });
    if (!fs.existsSync(cctvPath + '/' + ctx.today)) {
        fs.mkdirSync(cctvPath + '/' + ctx.today);
    }
};

get.snapshotCctv = async (ctx, next) => {
    try {
        await ctx.utils.cctv.requestSnapshot(ctx.cctv, ctx.today);
        ctx.body = fs.createReadStream(`${cctvPath}/${ctx.today}/${ctx.params.id}_temp.jpg`);
        ctx.set('Content-disposition', `attachment; filename=${ctx.params.id}_snapshot.jpg`);
        ctx.set('Content-type', 'image/jpeg');
    } catch (e) {
        console.log(e);
        ctx.throw(400, '0274');
    }
};

module.exports = get;
