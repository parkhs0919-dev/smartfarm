const Router = require('koa-router');
const router = new Router();

const get = require('./get');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['id'],
            explains: {
                'id': 'cctv id',
            },
            title: 'cctv server snapshot',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.findCctv(ctx, next);
                await get.prevProcess(ctx, next);
                await get.snapshotCctv(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:id', api.get());

module.exports.router = router;
module.exports.api = api;
