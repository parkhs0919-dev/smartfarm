const Router = require('koa-router');
const router = new Router();

const put = require('./put');

const api = {
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'id',
            ],
            essential: [],
            params: [],
            explains: {
            },    
            title: '센서 정보 변경',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.updateSensorsReset(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }   
        } else {
            return params;
        }
    },
};

router.put('/:id', api.put());
      
module.exports.router = router;
module.exports.api = api;
