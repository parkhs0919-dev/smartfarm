let puts = {};

puts.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.check.isInt('id');
};   

puts.updateSensorsReset = async (ctx, next) => {
    try {
        ctx.request.body.value = 0;
        ctx.utils.sensor.setSensorValueById(parseFloat(ctx.request.body.value), ctx.params.id);
        await ctx.models.Sensor.updateSensorsReset(ctx.params.id,ctx.request.body);    
    } catch (e) {
        ctx.throw(400, '0233');        
    }    
};   

puts.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit('sensors');
};  

module.exports = puts;
