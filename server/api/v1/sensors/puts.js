let puts = {};

puts.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    if (ctx.request.body.sensors instanceof Array && ctx.request.body.sensors.length) {
        for (let i=0; i<ctx.request.body.sensors.length; i++) {
            const sensor = ctx.request.body.sensors[i];
            if (!sensor.id || !sensor.sensor_name || sensor.revision === undefined) {
                ctx.throw(400, '0232');
            }
        }
    } else {
        ctx.throw(400, '0232');
    }
};

puts.updateSensors = async (ctx, next) => {
    try {
        await ctx.models.Sensor.updateSensors(ctx.request.body.sensors);
    } catch (e) {
        ctx.throw(400, '0233');
    }
};

puts.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit('sensors');
};

module.exports = puts;
