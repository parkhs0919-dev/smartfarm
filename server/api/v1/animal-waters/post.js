let post = {};

post.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
};

post.createAnimalWater = async (ctx, next) => {
  try {
      ctx.data = await ctx.models.AnimalWater.createAnimalWater(ctx.request.body);
  } catch (e) {
      ctx.throw(400, '0509');
  }
};  

post.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = post;
