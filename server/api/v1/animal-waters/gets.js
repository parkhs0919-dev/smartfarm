let gets = {};

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};
gets.getAnimalWater = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.AnimalWater.getAnimalWater(ctx.params.id,ctx.query);
    } catch (e) {
        ctx.throw(400, '0510');
    }
};

gets.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};   

module.exports = gets;
