let del = {};

del.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

};

del.deleteAnimalWater = async (ctx, next) => {
  try {
       ctx.data = await ctx.models.AnimalWater.deleteAnimalWater(ctx.params.id);
  } catch(e) {
      ctx.throw(400, '0511');  
  }  
};

del.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = del;
