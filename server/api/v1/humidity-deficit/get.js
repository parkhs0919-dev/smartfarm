let get = {};
const calculation = require('../../../methods/calculation');

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }   
  
    ctx.check.isInt('sensor_id');
    ctx.data = {};
};   
      
get.getHumidityDeficit = async (ctx, next) => {
    try {
        if(ctx.params.sensor_id){   
            const data = await ctx.models.Sensor.getSensorDryWet(ctx.params.sensor_id);
            ctx.data = await calculation.getHumidityDeficit(data.wetTemperature,data.dryTemperature);
            //console.log(ctx.data);  
        }else{      
            ctx.throw(400, '0101');
        }         
    } catch (e) {           
        ctx.throw(400, '0101');         
    }        
};  

get.getHumidityDeficit2 = async (ctx, next) => {
    try {     
        if(ctx.params.sensor_id){   
            const sensors = await ctx.models.Sensor.getSensorDryWet2(ctx.params.sensor_id);
            ctx.data= await calculation.getHumidityDeficit2(sensors.humidity,sensors.temperature);
        }else{              
            ctx.throw(400, '0101');    
        }                
    } catch (e) {            
        ctx.throw(400, '0101');         
    }        
};  

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};
   

module.exports = get;  