const Router = require('koa-router');
const router = new Router();

const get = require('./get');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
            ],  
            essential: [
            ],   
            params: ['sensor_id'],
            explains: {
            },  
            title: '수분 부족분 상세 ',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.getHumidityDeficit(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {  
                ctx.error(e.message, e.status);
            }  
        } else {   
            return params;     
        }
    },
    get2: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
            ],  
            essential: [
            ],   
            params: ['sensor_id'],
            explains: {
            },  
            title: '수분 부족분 상세 ',
            state: 'develop'
        };

        if (!isParam) {  
            try {
                await get.validate(ctx, next);
                await get.getHumidityDeficit2(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {  
                ctx.error(e.message, e.status);
            }  
        } else {   
            return params;     
        }
    }    
};  

router.get('/:sensor_id', api.get());
router.get('/humidity/:sensor_id', api.get2());
  
module.exports.router = router;
module.exports.api = api;   
  