let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
};

get.getCctvSetting = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.CctvSetting.findOne({});
        if (!ctx.data) {
            ctx.throw(404, '');
        }
    } catch (e) {
        ctx.throw(404, '');
    }
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
