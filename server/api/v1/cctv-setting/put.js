let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isBoolean('is_auto_close');
    ctx.sanitize.toBoolean('is_auto_close');
};

put.updateCctvSetting = async (ctx, next) => {
    try {
        await ctx.models.CctvSetting.update({
            is_auto_close: ctx.request.body.is_auto_close
        }, {
            where: {}
        });
    } catch (e) {
        ctx.throw(400, '0228');
    }
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
    const io = ctx.utils.io();
    if (io) io.emit('cctv-setting');
};

module.exports = put;
