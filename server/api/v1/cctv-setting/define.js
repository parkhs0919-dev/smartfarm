const Router = require('koa-router');
const router = new Router();

const get = require('./get');
const put = require('./put');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            explains: {},
            title: 'cctv setting 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.getCctvSetting(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'is_auto_close'
            ],
            essential: [
                'is_auto_close'
            ],
            explains: {
                'is_auto_close': '자동 connection 해제 (true / false)'
            },
            title: 'cctv setting 수정',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.updateCctvSetting(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/', api.get());
router.put('/', api.put());

module.exports.router = router;
module.exports.api = api;
