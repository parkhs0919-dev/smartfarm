let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.isEnum('mode', ['manual', 'individual', 'auto']);
};

put.updateMode = async (ctx, next) => {
    try {
        await ctx.models.Mode.update({
            mode: ctx.request.body.mode
        }, {
            where: {
                house_id: ctx.params.house_id
            }
        });
    } catch (e) {
        ctx.throw(400, '0212');
    }
};

put.emitMode = async (ctx, next) => {
    try {
        ctx.mode = await ctx.models.Mode.findByPk(ctx.params.house_id);
        const io = ctx.utils.io();
        if (io) io.emit('mode', {mode: ctx.mode});
    } catch (e) {
        ctx.throw(400, '0213');
    }
};

put.initAutoControls = async (ctx, next) => {
    try {
        await ctx.models.AutoControl.initAutoControls(ctx.params.house_id);
        const io = ctx.utils.io();
        if (io) io.emit('auto-controls', {
            house_id: ctx.params.house_id
        });
    } catch (e) {
        ctx.throw(400, '0249');
    }
};

put.updateControls = async (ctx, next) => {
    const mode = ctx.request.body.mode;
    if (mode === 'manual' || mode === 'auto') {
        try {
            await ctx.models.Control.update({
                mode: ctx.request.body.mode
            }, {
                where: {
                    house_id: ctx.params.house_id,
                    type: ['motor', 'power']
                }
            });
        } catch (e) {
            console.error('update control modes', e);
        }
    }
};

put.emitControls = async (ctx, next) => {
    try {
        const controls = await ctx.models.Control.findAll({
            where: {
                house_id: ctx.params.house_id,
                type: ['motor', 'power']
            }
        });
        const io = ctx.utils.io();
        if (controls && controls.length && io) {
            io.emit('motors', {
                house_id: ctx.params.house_id
            });
            io.emit('powers', {
                house_id: ctx.params.house_id
            });
        }
    } catch (e) {
        console.error('emit controls', e);
    }
};

put.activeControls = async (ctx, next) => {
    if (ctx.request.body.mode === 'auto') {
        try {
            const type = 'manual';
            const state = 'stop';
            const motors = await ctx.models.Control.findAll({
                where: {
                    house_id: ctx.params.house_id,
                    type: 'motor'
                }
            });
            let promises = [];
            motors.forEach(({id}) => {
                promises.push(ctx.utils.control.activeMotor({id, state, type}));
            });
            await Promise.all(promises);
        } catch (e) {
            console.error('active controls', e);
        }
    }
};

put.createReport = async (ctx, next) => {
    try {
        let mode;
        switch (ctx.request.body.mode) {
            case 'manual':
                mode = '수동제어';
                break;
            case 'individual':
                mode = '개별제어';
                break;
            case 'auto':
                mode = '자동제어';
                break;
            default:
                mode = ctx.request.body.mode;
                break;
        }
        await ctx.models.Report.create({
            user_id: ctx.user.id,
            house_id: ctx.params.house_id,
            type: 'auto',
            contents: `전체 '${mode}' 모드가 실행되었습니다.`
        });
        const io = ctx.utils.io();
        if (io) io.emit('report', {
            house_id: ctx.params.house_id
        });
    } catch (e) {
        console.error('create report', e);
    }
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = put;
