let get = {};

get.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('house_id');

    ctx.data = null;
};

get.getMode = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.Mode.findByPk(ctx.params.house_id);
        if (!ctx.data) {
            ctx.throw(404, '0107');
        }
    } catch (e) {
        ctx.throw(404, '0107');
    }
};

get.supplement = async (ctx, next) => {
    ctx.json(ctx.data);
};

module.exports = get;
