const Router = require('koa-router');
const router = new Router();

const get = require('./get');
const put = require('./put');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            params: ['house_id'],
            explains: {
                'house_id': 'house id'
            },
            title: '전체 자동제어 설정 조회',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await get.validate(ctx, next);
                await get.getMode(ctx, next);
                await get.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    },
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'mode'
            ],
            essential: [
                'mode'
            ],
            params: ['house_id'],
            explains: {
                'house_id': 'house id',
                'mode': '상태 (manual, individual, auto)'
            },
            title: '전체 자동제어 설정 변경',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.updateMode(ctx, next);
                await put.emitMode(ctx, next);
                await put.initAutoControls(ctx, next);
                await put.updateControls(ctx, next);
                await put.emitControls(ctx, next);
                await put.activeControls(ctx, next);
                await put.createReport(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.get('/:house_id', api.get());
router.put('/:house_id', api.put());

module.exports.router = router;
module.exports.api = api;
