let gets = {};
const dateExp = new RegExp('^[0-9]{4}-[0-9]{2}-[0-9]{2}$');

gets.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }
    ctx.check.isInt('id');
    ctx.check.isInt('sensor_id');

    ctx.data = null;
};

gets.getAlarms = async (ctx, next) => {
    try {
        ctx.data = await ctx.models.Sensor.getPanelAlarm(ctx.query.id, ctx.query.sensor_id);
    } catch (e) {
        ctx.throw(400, '0292');
    }
};

gets.supplement = async (ctx, next) => {
    if (ctx.data) {
        ctx.json(ctx.data);
    } else {
        ctx.throw(400, '0292');
    }
};

module.exports = gets;
