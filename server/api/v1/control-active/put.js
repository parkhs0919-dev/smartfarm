let put = {};

put.validate = async (ctx, next) => {
    if (!ctx.user) {
        ctx.throw(401, '0401');
    }

    ctx.check.isInt('id');
    ctx.check.isEnum('state', ['open', 'stop', 'close', 'on', 'off']);
    ctx.check.isInt('percentage');
    ctx.check.isInt('time');

    if (ctx.request.body.time && ctx.request.body.percentage) {
        ctx.throw(400, '0205');
    }
    if (ctx.request.body.percentage && (ctx.request.body.percentage < 0 || ctx.request.body.percentage > 100)) {
        ctx.throw(400, '0206');
    }
};

put.findControl = async (ctx, next) => {
    try {
        ctx.control = await ctx.models.Control.findById(ctx.params.id);
    } catch (e) {
        ctx.throw(400, '0103');
    }
    ctx.time = null;
    if (ctx.control.mode === 'auto') {
        ctx.throw(400, '0211');
    }
    if (ctx.request.body.time) {
        ctx.time = ctx.request.body.time;
    } else if (ctx.request.body.percentage) {
        ctx.time = parseInt(ctx.request.body.percentage * ctx.control.range / 100);
        if (!ctx.time) ctx.time = 1;
    }
};

put.activeControl = async (ctx, next) => {
    const id = ctx.params.id;
    const state = ctx.request.body.state;
    const type = 'manual';
    const time = ctx.time;
    const user_id = ctx.user.id;
    if (ctx.control.type === 'motor') {
        if (state === 'open' || state === 'stop' || state === 'close') {
            if (state === 'stop' || ctx.control.state === 'stop') {
                try {
                    await ctx.utils.control.activeMotor({id, state, type, time, user_id});
                } catch (e) {
                    const io = ctx.utils.io();
                    if (io) io.emit('report', {
                        house_id: ctx.control.house_id
                    });
                    ctx.throw(400, '0204');
                }
            } else {
                const io = ctx.utils.io();
                if (io) io.emit('report', {
                    house_id: ctx.control.house_id
                });
                ctx.throw(400, '0203');
            }
        } else {
            const io = ctx.utils.io();
            if (io) io.emit('report', {
                house_id: ctx.control.house_id
            });
            ctx.throw(400, '0201');
        }
    } else if (ctx.control.type === 'power') {
        if (state === 'on' || state === 'off') {
            try {
                await ctx.utils.control.activePower({id, state, type, time, user_id});
            } catch (e) {
                const io = ctx.utils.io();
                if (io) io.emit('report', {
                    house_id: ctx.control.house_id
                });
                ctx.throw(400, '0204');
            }
        } else {
            const io = ctx.utils.io();
            if (io) io.emit('report', {
                house_id: ctx.control.house_id
            });
            ctx.throw(400, '0202');
        }
    } else if (ctx.control.type === 'control') {
        if (state === 'on' || state === 'off') {
            try {
                await ctx.utils.control.activeControl({id, state, user_id});
            } catch (e) {
                const io = ctx.utils.io();
                if (io) io.emit('report', {
                    house_id: ctx.control.house_id
                });
                ctx.throw(400, '0210');
            }
        } else {
            const io = ctx.utils.io();
            if (io) io.emit('report', {
                house_id: ctx.control.house_id
            });
            ctx.throw(400, '0209');
        }
    }
};

put.createReport = async (ctx, next) => {
    try {
        const body = ctx.request.body;
        const mode = await ctx.models.Mode.getModeByControlId(ctx.params.id);
        await ctx.models.Report.create({
            user_id: ctx.user.id,
            house_id: ctx.control.house_id,
            control_id: ctx.params.id,
            type: ctx.control.type,
            contents: `[${mode === 'individual' ? '개별/' : ''}수동제어] ${ctx.utils.control.generateContents(ctx.control, body.state, body.time, body.percentage)}`
        });
        const io = ctx.utils.io();
        if (io) io.emit('report', {
            house_id: ctx.control.house_id
        });
    } catch (e) {}
};

put.supplement = async (ctx, next) => {
    ctx.json(null);
};

module.exports = put;
