const Router = require('koa-router');
const router = new Router();

const put = require('./put');

const api = {
    put: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'state',
                'percentage',
                'time'
            ],
            essential: [
                'state'
            ],
            params: ['id'],
            explains: {
                'id': 'control id',
                'state': '상태 (open, stop, close, on, off)',
                'percentage': '퍼센트 (0 ~ 100%)',
                'time': '시간 (초)'
            },
            title: '수동 조작',
            state: 'develop'
        };

        if (!isParam) {
            try {
                await put.validate(ctx, next);
                await put.findControl(ctx, next);
                await put.activeControl(ctx, next);
                await put.createReport(ctx, next);
                await put.supplement(ctx, next);
            } catch (e) {
                ctx.error(e.message, e.status);
            }
        } else {
            return params;
        }
    }
};

router.put('/:id', api.put());

module.exports.router = router;
module.exports.api = api;
