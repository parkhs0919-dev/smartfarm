const jwt = require('jsonwebtoken');
const CONFIG = require('../config');
const ALGORITHM = 'HS256';
const ACCESS = CONFIG.jwt.access;
const REFRESH = CONFIG.jwt.refresh;
const COOKIE = CONFIG.jwt.cookie;

module.exports = {
    parseToken: (ctx) => {
        if (ctx.query[COOKIE]) {
            ctx.cookies.set(COOKIE, ctx.query[COOKIE]);
            return {
                token: ctx.query[COOKIE],
                from: 'query'
            };
        } else if (ctx.cookies.get(COOKIE)) {
            return {
                token: ctx.cookies.get(COOKIE),
                from: 'cookie'
            };
        } else if (ctx.header.authorization) {
            return {
                token: ctx.header.authorization.replace('Bearer ', ''),
                from: 'cookie'
            };
        } else {
            return null;
        }
    },
    generateAccessToken: (payload) => {
        return new Promise((resolve, reject) => {
            jwt.sign(payload, ACCESS.key, {
                algorithm: ALGORITHM,
                expiresIn: ACCESS.ttl
            }, (err, token) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(token);
                }
            });
        });
    },
    verifyAccessToken: (token) => {
        return new Promise((resolve, reject) => {
            jwt.verify(token, ACCESS.key, {
                algorithm: ALGORITHM
            }, (err, payload) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(payload);
                }
            });
        });
    },
    generateRefreshToken: (key) => {
        return new Promise((resolve, reject) => {
            jwt.sign({key}, REFRESH.key, {
                algorithm: ALGORITHM,
                expiresIn: REFRESH.ttl
            }, (err, token) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(token);
                }
            });
        });
    },
    verifyRefreshToken: (token) => {
        return new Promise((resolve, reject) => {
            jwt.verify(token, REFRESH.key, {
                algorithm: ALGORITHM
            }, (err, payload) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(payload);
                }
            });
        });
    },
    setCookie: (ctx, token) => {
        ctx.cookies.set(COOKIE, token, {httpOnly: true});
    }
};
