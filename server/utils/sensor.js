const SENSOR_METHOD = require('../methods/sensor');

module.exports = {
    setSensorValueById: SENSOR_METHOD.setSensorValueById,
};
