const tcpp = require('tcp-ping');
const request = require('request');
const CONFIG = require('../config');
const FLUID = CONFIG.fluid;

module.exports = {
  sendSensors: (key,data) => {
    return new Promise((resolve, reject) => {
        try {
              const address = FLUID.host.split(':');
              const ip = address[0];
              const port = address[1];
              tcpp.probe(ip, port, (err, available) => {
                  if (err) {
                      reject(err);
                  } else {
                      if (available) {
                          request({
                            uri: `http://${ip}:${port}/api/v1/out-sensor`,
                            method: 'PUT',
                            form: {
                              value: data,
                              out_key:key
                            }
                          }, (error, response, body) => {
                              if (err) {
                                  reject(err);
                              } else {
                                  const res = JSON.parse(body);
                                  if (res.code === "0000") {
                                      resolve(true);
                                  }else if (res.code === "0203") {
                                      console.log(body);
                                  } else {
                                      reject(false);
                                  }
                              }
                          });
                      } else {
                          reject(false);
                      }
                  }
              });
        } catch (e) {
            reject(e);
        }
    });

  }
};
