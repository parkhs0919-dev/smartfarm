const admin = require('firebase-admin');
const serviceAccount = require("../config/smartfarm-ffe34-firebase-adminsdk-bberd-19c618141e");
const STD = require('../metadata/standards');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://smartfarm-ffe34.firebaseio.com"
});

const sendToToken = (token, title, body, data, channel, platform = 'android') => {
    return new Promise(async (resolve, reject) => {
        try {
            let message = {
                token,
            };
            if (platform === 'android') {
                message.android = {
                    priority: 'high',
                    notification: {
                        title,
                        body,
                    },
                    data: data
                };
                if (channel) {
                    message.android.notification.sound = STD.fcm.sound.android[channel];
                    message.android.notification.channel_id = channel;
                    message.android.data.channel_id = channel;
                }
            } else {
                message.notification = {
                    title,
                    body,
                };
                message.data = data;
                message.apns = {
                    headers: {
                        'apns-priority': '10'
                    },
                };
                if (channel) {
                    message.apns.payload = {
                        aps: {
                            sound: STD.fcm.sound.ios[channel]
                        }
                    };
                }
            }
            const response = await admin.messaging().send(message);
            resolve(true);
        } catch (e) {
            reject(e);
        }
    });
};

module.exports = {
    sendToToken,
};
