const jwt = require('./jwt');
const io = require('./socket-io');
const control = require('./control');
const erp = require('./erp');
const fluid = require('./fluid');
const filter = require('./filter');
const fcm = require('./fcm');
const cctv = require('./cctv');
const sensor = require('./sensor');

module.exports = {
    jwt,
    io,
    control,
    erp,
    fluid,
    filter,
    fcm,
    cctv,
    sensor,
};
module.exports.jwt = jwt;
module.exports.io = io;
module.exports.control = control;
module.exports.erp = erp;
module.exports.fluid = fluid;
module.exports.filter = filter;
module.exports.fcm = fcm;
module.exports.cctv = cctv;
module.exports.sensor = sensor;
