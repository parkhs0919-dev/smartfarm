const CONTROL_METHOD = require('../methods/control');
const VOICE_CONTROL_METHOD = require('../methods/voice-control');

module.exports = {
    activeMotor: CONTROL_METHOD.activeMotor,
    activePower: CONTROL_METHOD.activePower,
    activeControl: CONTROL_METHOD.activeControl,
    activeVoiceControl: VOICE_CONTROL_METHOD.activeVoiceControl,
    generateContents: CONTROL_METHOD.generateContents,
    generateGroupContents : CONTROL_METHOD.generateGroupContents,
    generateSignal: (control, state) => {
        const startCode = returnStartCode();
        const address = returnAddress(control);
    }
};

function returnStartCode () {
    return 'F3';
}

function returnAddress(control) {
    return control.address;
}

function f() {

}

// console.log(Buffer.from('250001000192CD0000002F6D6E742F72', 'hex'));
