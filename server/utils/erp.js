const fs = require('fs');
const request = require('request');
const CONFIG = require('../config');
const ERP = CONFIG.erp;

function returnHeaders(platform = 'farm') {
    return {
        'FARM-LICENSE': CONFIG[platform].license
    };
}

module.exports = {
    sendLogFile: (platform, tablePath, file_name) => {
        return new Promise((resolve, reject) => {
            try {
                request({
                    uri: `${ERP.host}/api/v1/farm/log`,
                    formData: {
                        file_name,
                        file: fs.createReadStream(tablePath)
                    },
                    method: 'POST',
                    headers: returnHeaders(platform)
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        try {
                            const data = JSON.parse(body);
                            if (data.code === '0000') {
                                resolve(data.data);
                            } else {
                                reject(null);
                            }
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    },
    getSunDates: (platform) => {
        return new Promise((resolve, reject) => {
            try {
                request({
                    uri: `${ERP.host}/api/v1/farm/sun-dates`,
                    method: 'GET',
                    headers: returnHeaders(platform)
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        try {
                            const data = JSON.parse(body);
                            if (data.code === '0000') {
                                resolve(data.data);
                            } else {
                                reject(null);
                            }
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    },
    getRecentNotice: (platform, body) => {
        return new Promise((resolve, reject) => {
            try {
                request({
                    uri: `${ERP.host}/api/v1/farm/recent-notice`,
                    method: 'POST',
                    headers: returnHeaders(platform),
                    form:{offset : body.offset , size : body.size ,searchField: body.searchField, searchItem: body.searchItem},
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        try {
                            const data = JSON.parse(body);
                            if (data.code === '0000') {
                                resolve(data.data);
                            } else {
                                reject(null);
                            }
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    },
    getWeather: (platform) => {
        return new Promise((resolve, reject) => {
            try {
                request({
                    uri: `${ERP.host}/api/v1/farm/weather`,
                    method: 'GET',
                    headers: returnHeaders(platform)
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        try {
                            const data = JSON.parse(body);
                            if (data.code === '0000') {
                                resolve(data.data);
                            } else {
                                reject(null);
                            }
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    },
    ping: (platform) => {
        return new Promise((resolve, reject) => {
            try {
                request({
                    uri: `${ERP.host}/api/v1/farm/ping`,
                    method: 'GET',
                    headers: returnHeaders(platform)
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(true);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    },
    ip: (platform) => {
        return new Promise((resolve, reject) => {
            try {
                request({
                    uri: `${ERP.host}/api/v1/farm/ip`,
                    method: 'PUT',
                    headers: returnHeaders(platform)
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(true);
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    },
    addUser: (platform, body) => {
        return new Promise(async (resolve, reject) => {
            try {
                request({   
                    uri: `${ERP.host}/api/v1/farm/user`,
                    method: 'POST',
                    headers: returnHeaders(platform),
                    form: body
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        try {
                            const data = JSON.parse(body);  
                            if (data.code === '0000') {
                                resolve(data.data);  
                            } else {
                                reject(null);
                            }   
                        } catch (e) {
                            reject(e);
                        }
                    }     
                });
            } catch (e) {    
                reject(e);
            }
        });
    },
    modUserPw: (platform, body) => {
        return new Promise(async (resolve, reject) => {
            try {
                request({   
                    uri: `${ERP.host}/api/v1/farm/uppass`,
                    method: 'PUT',
                    headers: returnHeaders(platform),
                    form: body
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        try {
                            const data = JSON.parse(body);  
                            console.log(data);
                            if (data.code === '0000') {
                                resolve(true);  
                            } else {
                                resolve(false); 
                            }   
                        } catch (e) {
                            reject(e);
                        }
                    }     
                });
            } catch (e) {    
                reject(e);
            }
        });
    },    
    delUser: (platform,id) => {
        return new Promise((resolve, reject) => {
            try {
                request({
                    uri: `${ERP.host}/api/v1/farm/user/${id}`,
                    method: 'DELETE',  
                    headers: returnHeaders(platform)
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        try {
                            const data = JSON.parse(body); 
                            if (data.code === '0000') {
                                resolve(data);    
                            } else {
                                reject(null);
                            }    
                        } catch (e) {
                            reject(e);
                        }
                    }    
                });  
            } catch (e) {
                reject(e);
            }
        });
    },    
    getFarmlName: (platform) => {
        return new Promise((resolve, reject) => {
            try {     
                request({
                    uri: `${ERP.host}/api/v1/farm/info`,
                    method: 'GET',   
                    headers: returnHeaders(platform),
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        try {
                            const data = JSON.parse(body);
                            if (data.code === '0000') {
                                resolve(data.data);
                            } else {    
                                reject(null);
                            }
                        } catch (e) {  
                            reject(e);
                        }
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    }, 
    getUserCheck: (platform,body) => {
        return new Promise((resolve, reject) => {
            try {     
                request({
                    uri: `${ERP.host}/api/v1/farm/usercheck`,
                    method: 'POST',   
                    headers: returnHeaders(platform),
                    form:body
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        try {
                            const data = JSON.parse(body);  
                            if (data.code === '0000') {
                                resolve(data.data);  
                            } else {
                                reject(null);
                            }   
                        } catch (e) {
                            reject(e);
                        }
                    } 
                });  
            } catch (e) {
                reject(e);
            }
        });
    },
    getAutoControl : (platform, crop_type) => {
        return new Promise((resolve, reject) => {
          try {
              request({
                  uri: `${ERP.host}/api/v1/admin/auto-control`,
                  method: 'GET',
                  headers: returnHeaders(platform),
                  qs : {
                      crop_type : crop_type
                  }
              }, (error, response, body) => {
                  if (error) {
                      reject(error);
                  } else {
                      try {
                          const data = JSON.parse(body);
                          if (data.code === '0000') {
                              resolve(data.data);
                          }else{
                              reject(null);
                          }
                      } catch (e) {
                          reject(e);
                      }
                  }
              });
          } catch (e) {
              reject(e);
          }
        })
    },
    getCrops : (platform) => {
        return new Promise((resolve, reject) => {
            try {
                request({
                    uri: `${ERP.host}/api/v1/admin/crop-list`,
                    method: 'GET',
                    headers: returnHeaders(platform),
                }, (error, response, body) => {
                    if (error) {
                        reject(error);
                    } else {
                        try {
                            const data = JSON.parse(body);
                            if (data.code === '0000') {
                                resolve(data.data);
                            } else {
                                reject(null);
                            }
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
            } catch (e) {
                reject(e);
            }
        })
    }
};
