const request = require('request');
const fs = require('fs');
const cctvPath = 'cctv';
const CONFIG = require('../config');
const CCTV_METHOD = require('../methods/cctv');

module.exports = {
    start: CCTV_METHOD.start,
    request: cctv => {
        return new Promise(async (resolve, reject) => {
            try {
                request.post({
                    uri: `http://${CONFIG.cctv.host}:${CONFIG.cctv.port}/api/cctv-start/${cctv.id}`,
                    form: {}
                }, (err, response, body) => {
                    if (err) {
                        reject(err);
                    } else {
                        try {
                            const data = JSON.parse(body);
                            if (data.code === '0000') {
                                resolve(data.data);
                            } else {
                                reject(false);
                            }
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    },
    requestMove: (cctv, form) => {
        return new Promise(async (resolve, reject) => {
            try {
                request.post({
                    uri: `http://${CONFIG.cctv.host}:${CONFIG.cctv.port}/api/cctv-move/${cctv.id}`,
                    form
                }, (err, response, body) => {
                    if (err) {
                        reject(err);
                    } else {
                        try {
                            const data = JSON.parse(body);
                            if (data.code === '0000') {
                                resolve(data.data);
                            } else {
                                reject(false);
                            }
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
            } catch (e) {
                reject(e);
            }
        });
    },
    requestSnapshot: (cctv, today) => {
        return new Promise(async (resolve, reject) => {
            try {
                const stream = request.get(`http://${CONFIG.cctv.host}:${CONFIG.cctv.port}/api/cctv-snapshot/${cctv.id}`)
                    .on('response', response => {
                        if (response.statusCode !== 200) {
                            reject(false);
                        }
                    })
                    .on('error', error => {
                        reject(error);
                    })
                    .pipe(fs.createWriteStream(`${cctvPath}/${today}/${cctv.id}_temp.jpg`));
                stream.on('finish', () => {
                    resolve(true);
                });
            } catch (e) {
                reject(e);
            }
        });
    }
};
module.exports.start = CCTV_METHOD.start;
