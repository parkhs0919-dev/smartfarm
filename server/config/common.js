module.exports = {
    port: 3000,
    logFile: null,
    cacheMaxAge: 8640000000,
    db: {
        host: 'localhost',
        port: '3306',
        database: 'greenlabs_farm',
        username: 'linco',
        password: '123123',
        timezone: '+09:00',
        platform: 'mysql',
        charset: 'utf8mb4',
        collate: 'utf8mb4_general_ci',
        logging: false
    },
    jwt: {
        cookie: 'FARM-ACCESS-TOKEN',
        access: {
            key: 'Rqsomqo7MYi7mHARqq79mcSx1qbODBDEYWYYAinnWO8',
            ttl: 604800 // 60 * 60 * 24 * 7 = 1 weeks
        },
        refresh: {
            key: 'XhG8QoI2ptTVukYcFXfiKPj_c91HZeND2iHQbZUM2n0',
            ttl: 2592000 // 60 * 60 * 24 * 30 = 30 days
        },
        maxCount: 5
    },
    defaultUser: {
        id: 'LOCAL',
        uid: null,       
        name: 'LOCAL',
        email: null,
        phone_num: null,
        grade: 'none',
        com_type : 'farmlabs'
    },
    autoUser: {
        id: 'AUTO'
    },
    file: {
        dest: 'uploads',
        limits: {
            fileSize: 10000000 // 10mb
        }
    },
    farm: {
        license: 'license4',
    },
    animal: {
        license: '',
    },
    erp: {
        host: 'http://localhost:8000',
        goodResTime: 200,
        badResTime: 240
    },
    fluid: {
        host: '192.168.50.110:8801',
        screenIp: '',
        screenPort: 9901,
        isUseScreen: false
    },
    serialPort: {
        isUseLegacy: false, // get sensor data 2.0
        isUseNewSensor: false, // get sensor data 3.0
        isUseNewControl: false, // send control signal 3.0
        legacyAddress: '/dev/tty.usbmodem1421',
        newSensorAddress: '/dev/tty.usbserial-AH06RGFX',
        newControlAddress: '/dev/tty.usbserial-AH06RGFX',
        sensorAddressLength: 8,
        sensorTimeout: 5000, // 10 seconds timeout
    },
    control: {
        isUseHttp: false, // send control signal 2.0
        isUseSerialPort: false, // send control signal 3.0
        isUseHttpBoard: false, // send control signal 3.0 http ip board
        motorDelay: 500, // off delay
        maxMotorSignalCount: 10, // max 10 count (send signal to hardware)
        topengDelay: 2000,
        isUsePestControl: false
    },
    sensor: {
        queueSize: 10,
        onOffDelay: 3000, // 3 seconds
        onOffAddresses: [],
        isUseWaterLiter: false, // water (x/y/z), liter
    },
    alarm: {
        queueSize: 5,
        delay: 300 // re-send seconds
    },
    fcm: {
        serverKey: 'AAAA2UvcRWY:APA91bG5iwxrBx_an1VGaYVNcD-nD59uR_vO4ZRm_UGRLEGoYmEbJ4_gdulYBYpHHM2ua1TRhQVG1fy-U-3n7YWwtm7dIwFFufPhXx1aDUICWozLw_ILO4DJoYV8oGUbw9ya_haf0YJc'
    },
    cctv: {
        host: 'localhost',
        port: 8500,
        expiry: 600000 // 10 minutes
    },
    pest_control: {
        medicine_io: 8,
        compressor_io1: 17,
        compressor_io2: 27,
        medicine_address : "192.168.50.130:8801",
        compressor_address : "192.168.50.131:8801",
        zone:
        [
            {"key":"1","type":"motor","io1":"17","io2":"27","address":"192.168.50.132:8801"},
            {"key":"2","type":"motor","io1":"22","io2":"10","address":"192.168.50.132:8801"},
            {"key":"3","type":"motor","io1":"9","io2":"11","address":"192.168.50.132:8801"},
            {"key":"4","type":"motor","io1":"5","io2":"6","address":"192.168.50.132:8801"}
        ]
    }
};
