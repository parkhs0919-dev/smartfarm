module.exports = {
    port: 10001,
    db: {
        host: '',
        port: '3306',
        database: 'greenlabs_farm',
        username: 'greenlabs_farm',
        password: '1234',
        timezone: '+09:00',
        platform: 'mysql',
        charset: 'utf8mb4',
        collate: 'utf8mb4_general_ci',
        logging: false
    },
    erp: {
        host: 'http://dev.farmlabs.co.kr',
        // host: 'http://localhost:8000',
    },
    fluid : {
        host : '192.168.0.200:10002',
    }
};
