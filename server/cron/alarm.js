const async = require('async');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const sequelize = require('../methods/sequelize').sequelize;
const CONFIG = require('../config');
const FCM_UTIL = require('../utils/fcm');
const getIo = require('../methods/socket-io').getIo;

const queueSize = Number(CONFIG.alarm.queueSize);
const delay = CONFIG.alarm.delay;

let isAlarmProcess = false;
let sensors = [];
let sensorIds = [];
let queue = {};

const etcHash = {
    'battery': true,
    'liter': true
};

module.exports = (finish) => {
    if (isAlarmProcess) {
        return finish(400, 'alarm processing');
    } else {
        isAlarmProcess = true;
    }

    let funcs = [], alarms = [], tokens = [], now = new Date().getTime() , date = new Date(), io = getIo();;

    funcs.push(callback => {
        findInitialSensors(callback);
    });

    funcs.push(callback => {
        findCurrentSensorValues(callback);
    });

    funcs.push(callback => {
        findAlarms(callback);
    });

    funcs.push(callback => {
        findTokens(callback);
    });

    funcs.push(callback => {
        sendFcm(callback);
    });

    async.series(funcs, (error, results) => {
        isAlarmProcess = false;
        if (error) {
            if (error === 'empty' || error === 'collect') {
                finish(204);
            } else {
                finish(400, error);
            }
        } else {
            finish(204);
        }
    });

    async function findInitialSensors(callback) {
        if (sensors.length) {
            callback(null, true);
        } else {
            sensors = await sequelize.models.Sensor.findAll({});
            if (sensors.length) {
                sensors.forEach(sensor => {
                    sensorIds.push(sensor.id);
                });
                callback(null, true);
            } else {
                callback("empty sensors", false);
            }
        }
        return true;
    }

    async function findCurrentSensorValues(callback) {
        const currentSensors = await sequelize.models.Sensor.findAll({
            where: {
                id: sensorIds
            }
        });
        if (currentSensors.length === sensorIds.length) {
            currentSensors.forEach(sensor => {
                if (queue[sensor.id]) {
                    if (queue[sensor.id].values.length === queueSize) {
                        queue[sensor.id].values.splice(0, 1);
                    }
                    queue[sensor.id].values.push(sensor.value);
                } else {
                    queue[sensor.id] = {
                        values: [sensor.value]
                    };
                }
            });
            if (queue[Object.keys(queue)[0]].values.length === queueSize) {
                callback(null, true);
            } else {
                callback('collect', false);
            }
        } else {
            callback('sensor change', false);
        }
        return true;
    }

    async function findAlarms(callback) {
        alarms = await sequelize.models.Alarm.findAll({
            include: [{
                model: sequelize.models.Sensor,
                as: 'sensor',
                required: true
            }],
            where: {
                [Op.and]: [{
                    [Op.or]: [{
                        is_bar: true
                    }, {
                        is_sound: true
                    }, {
                        is_image: true
                    }],
                }, {
                    state: 'use'
                }]
            }
        });
        if (alarms.length) {
            callback(null, true);
        } else {
            callback('empty', false);
        }
        return true;
    }

    async function findTokens(callback) {
        tokens = await sequelize.models.DeviceToken.findAll({});
        if (tokens.length) {
            callback(null, true);
        } else {
            callback('empty', false);
        }
        return true;
    }

    async function sendFcm(callback) {
        for (let i=0; i<alarms.length; i++) {
            const alarm = alarms[i];
            const type = alarm.sensor.type;
            let value;
            if (type === 'rain' || type === 'water' || type === 'power' || type === 'fire' || type === 'door' || type === 'window') {
                value = checkAllTrue(queue[alarm.sensor_id].values);
            } else {
                value = avg(queue[alarm.sensor_id].values);
            }

            let isUseAlarm=(alarm.is_use)? isTimeCheck(alarm.use_start_hour,alarm.use_end_hour,date) : true;
            let isNoAlarm=(alarm.is_no)? isTimeCheck(alarm.no_start_hour,alarm.no_end_hour,date) : false;
            let isTime=isSendTimeCycleCheck(alarm.send_time,alarm.cycle,now);

            if(isUseAlarm && !isNoAlarm && isTime){
                if(alarm.type=='range'){
                    if (!isNaN(value)) {
                        if ((type === 'windDirection'||type === 'korinsWindDirection') && normalizeWindDirectionValue(value) === alarm.value) {
                            /**
                             * wind direction fcm
                             */
                            const body = `'${alarm.sensor.sensor_name}'가(이) '${sanitizeWindDirection(alarm.sensor)}'입니다.`;
                            await sequelize.models.Alarm.update({send_time: now}, {where: {id: alarm.id}});
                            await sendToTokens(tokens, body, {
                                is_bar: alarm.is_bar.toString(),
                                is_image: alarm.is_image.toString(),
                                type: alarm.sensor.type,
                                condition: 'equal',
                                text: body
                            }, alarm.is_sound ? 'etc' : null,alarm.repeat);
                            sequelize.models.Report.createReports('warning', body);

                            if (io) io.emit('alarm-sound', {
                                alarm
                            });
                        } else if (type === 'rain' && value === alarm.value) {
                            /**
                             * rain fcm
                             */
                            const body = `'${alarm.sensor.sensor_name}'가(이) '${sanitizeAlarmSensor(alarm.sensor,value)}'입니다.`;
                            await sequelize.models.Alarm.update({send_time: now}, {where: {id: alarm.id}});
                            await sendToTokens(tokens, body, {
                                is_bar: alarm.is_bar.toString(),   
                                is_image: alarm.is_image.toString(),    
                                type: alarm.sensor.type,
                                condition: 'equal',
                                text: body
                            }, alarm.is_sound ? (alarm.value === 0 ? 'rainon' : 'rainoff') : null,alarm.repeat);
                            sequelize.models.Report.createReports('warning', body);

                            if (io) io.emit('alarm-sound', {
                                alarm
                            });
                        } else if ((type === 'power' || type === 'water' || type === 'fire' || type === 'door' || type === 'window' ) && value === alarm.value) {
                            /**
                             * power & water fcm
                             */
                            const body = `'${alarm.sensor.sensor_name}'가(이) '${sanitizeAlarmSensor(alarm.sensor,value)}'입니다.`;
                            await sequelize.models.Alarm.update({send_time: now}, {where: {id: alarm.id}});
                            await sendToTokens(tokens, body, {
                                is_bar: alarm.is_bar.toString(),
                                is_image: alarm.is_image.toString(),
                                type: alarm.sensor.type,
                                condition: 'equal',
                                text: body
                            }, alarm.is_sound ? 'etc' : null,alarm.repeat);
                            sequelize.models.Report.createReports('warning', body);

                            if (io) io.emit('alarm-sound', {
                                alarm
                            });
                        } else {
                            switch(alarm.condition) {
                                case 'equal':
                                    if (value === alarm.value) {
                                        /**
                                         * equal fcm
                                         */
                                        const body = `'${alarm.sensor.sensor_name}'가(이) 설정하신 '${alarm.value}${alarm.sensor.unit ? ' ' + alarm.sensor.unit : ''}' 입니다.`;
                                        await sequelize.models.Alarm.update({send_time: now}, {where: {id: alarm.id}});
                                        await sendToTokens(tokens, body, {
                                            is_bar: alarm.is_bar.toString(),
                                            is_image: alarm.is_image.toString(),
                                            type: etcHash[alarm.sensor.type] ? 'etc' : alarm.sensor.type,
                                            condition: 'equal',
                                            text: body
                                        }, alarm.is_sound ? 'etc' : null,alarm.repeat);
                                        sequelize.models.Report.createReports('warning', body);

                                        if (io) io.emit('alarm-sound', {
                                            alarm
                                        });
                                    }
                                    break;
                                case 'over':
                                    if (value >= alarm.value) {
                                        /**
                                         * over fcm
                                         */
                                        const body = `'${alarm.sensor.sensor_name}'가(이) 설정하신 '${alarm.value}${alarm.sensor.unit ? ' ' + alarm.sensor.unit : ''}' 이상입니다. (현재: ${value}${alarm.sensor.unit ? ' ' + alarm.sensor.unit : ''})`;
                                        await sequelize.models.Alarm.update({send_time: now}, {where: {id: alarm.id}});
                                        await sendToTokens(tokens, body, {
                                            is_bar: alarm.is_bar.toString(),
                                            is_image: alarm.is_image.toString(),
                                            type: etcHash[alarm.sensor.type] ? 'etc' : alarm.sensor.type,
                                            condition: 'over',
                                            text: body
                                        }, alarm.is_sound ? 'over' : null,alarm.repeat);
                                        sequelize.models.Report.createReports('warning', body);

                                        if (io) io.emit('alarm-sound', {
                                            alarm
                                        });
                                    }
                                    break;
                                case 'under':
                                    if (value < alarm.value) {
                                        /**
                                         * under fcm
                                         */
                                        const body = `'${alarm.sensor.sensor_name}'가(이) 설정하신 '${alarm.value}${alarm.sensor.unit ? ' ' + alarm.sensor.unit : ''}' 미만입니다. (현재: ${value}${alarm.sensor.unit ? ' ' + alarm.sensor.unit : ''})`;
                                        await sequelize.models.Alarm.update({send_time: now}, {where: {id: alarm.id}});
                                        await sendToTokens(tokens, body, {
                                            is_bar: alarm.is_bar.toString(),
                                            is_image: alarm.is_image.toString(),
                                            type: etcHash[alarm.sensor.type] ? 'etc' : alarm.sensor.type,
                                            condition: 'under',
                                            text: body
                                        }, alarm.is_sound ? 'under' : null,alarm.repeat);
                                        sequelize.models.Report.createReports('warning', body);

                                        if (io) io.emit('alarm-sound', {
                                            alarm
                                        });
                                    }
                                    break;
                            }
                        }
                    }
                }else if(alarm.type=='error'){
                    if(isErrorTimeCheck(alarm.sensor,now)){
                        const body = `'${alarm.sensor.sensor_name}'의 값이 정확하지 않습니다. 센서를 확인해주세요. `;
                        await sequelize.models.Alarm.update({send_time: now}, {where: {id: alarm.id}});
                        await sendToTokens(tokens, body, {
                            is_bar: alarm.is_bar.toString(),
                            is_image: alarm.is_image.toString(),
                            type: etcHash[alarm.sensor.type] ? 'etc' : alarm.sensor.type,
                            condition: 'equal',
                            text: body
                        }, alarm.is_sound ? 'etc' : null,alarm.repeat);
                        sequelize.models.Report.createReports('warning', body);

                        if (io) io.emit('alarm-sound', {
                            alarm
                        });
                    }
                }
            }
        }
        callback(null, true);
        return true;
    }
};

async function sendToTokens(tokens, body, data, channel,repeat) {
    for (let i=0; i<tokens.length; i++) {         
        try {                 
            for(let j=0; j<repeat; j++){
                await FCM_UTIL.sendToToken(tokens[i].token, '[스마트팜 센서 경보]', body, data, channel, tokens[i].platform);
            }
            sequelize.models.FcmLog.createFcmLog(tokens[i].user_id ,tokens[i].token ,body ,data ,channel);
        } catch (e) {
            console.log(e);
        }    
    }          
}

function sanitizeWindDirection(sensor) {
    let units = sensor.unit.split(':');
    if (units.length === 8) {
        for (let i=0; i<units.length; i++) {
            if (i === 0) {
                if (sensor.value >= 337.5 || sensor.value < 22.5) {
                    return units[i];
                }
            } else {
                if (sensor.value >= 45 * i - 22.5 && sensor.value < 45 * i + 22.5) {
                    return units[i];
                }
            }
        }
    }
    return '설정하신 풍향';
}

function sanitizeSensor(sensor) {
    let units = sensor.unit.split(':');
    if (units.length === 2) {
        if (sensor.value) {
            return units[0];
        } else {
            return units[1];
        }
    } else {
        return '설정하신 값';
    }
}

function sanitizeAlarmSensor(sensor,value) {
    let units = sensor.unit.split(':');
    if (units.length === 2) {
        if (value) {
            return units[0];
        } else {       
            return units[1];
        }
    } else {
        return '설정하신 값';
    }
}
 
function avg(array) {
    let temp = 0;
    for (let i=0; i<array.length; i++) {
        if (array[i] !== null) {
            temp += parseFloat(array[i]);
        } else {
            return NaN;
        }
    }
    return parseFloat((temp / array.length).toFixed(1));
}

function checkAllTrue(array) {
    for (let i=0; i<array.length; i++) {
        if (parseFloat(array[i]) === 0) {
            return 0;
        }
    }
    return 1;
}

function normalizeWindDirectionValue(value) {
    const normalized = parseInt((value + 22.5) / 45);
    return normalized > 7 ? 0 : normalized * 45;
}

function isTimeCheck(start_hour,end_hour,date){
    let isValue = false;
    let now_hour  = date.getHours();
    if(start_hour<=end_hour &&  start_hour<=now_hour && end_hour>now_hour){
        isValue=true;
    }else if(start_hour>end_hour){
        if(start_hour<=now_hour && 24 > now_hour) isValue=true;
        else if(end_hour>=0 && end_hour > now_hour ) isValue=true;
    }
    return isValue;
}

function isSendTimeCycleCheck(send_time,cycle,now){
    let isValue = false;
    if (send_time!=0) {
        let time_cycle=send_time+(cycle*60*1000);
        if(time_cycle <now) isValue = true;
    }else{
        isValue = true;
    }
    return isValue;
}

function isErrorTimeCheck(sensor,now){
    const checkTime=(10*60*1000);
    let isValue = false;
    let sensor_time = (sensor.updatedAt)? new Date(sensor.updatedAt).getTime() : 0;
    if (checkTime<(Number(now)-Number(sensor_time))) {
        isValue = true;
    }
    return isValue;
}
