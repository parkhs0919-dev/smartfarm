const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
let io = require('../methods/socket-io').getIo();

let isEmitting = false;

module.exports = (finish) => {
    if (isEmitting) {
        return finish(400, 'emitting sensor value');
    } else {
        isEmitting = true;
    }

    let funcs = [],
        sensorsAvg = [];

    funcs.push(callback => {
        getSensors(callback);
    });

    funcs.push(callback => {
        emit(callback);
    });

    async.series(funcs, (error, results) => {
        isEmitting = false;
        if (error) {
            finish(400, error);
        } else {
            finish(204);
        }
    });

    async function getSensors(callback) {
        try { 
            sensorsAvg = await sequelize.models.SensorAvgItem.getSensorAvgItemGroupbyValue(); 
            if (sensorsAvg.length ) {
                callback(null, true);
            } else {   
                callback('empty sensor-avg-values', false);
            }
        } catch (e) {   
            console.error('sensor-avg-value', new Date(), e);
            callback(e, false);
        }
    }    

    function emit(callback) {
        try {
            if (!io) {
                io = require('../methods/socket-io').getIo();
            }
            if (io) {  
                io.emit('sensor-avg-values', {sensorsAvg});
            }
            callback(null, true);
        } catch (e) {
            console.error('emit-sensor-avg-value ', new Date(), e);
            callback(e, false);
        }  
    }
};
