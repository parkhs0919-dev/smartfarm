const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const FLUID_UTIL = require('../utils/fluid');
const CONFIG = require('../config');

let isSending = false;

module.exports = async (finish) => {
    if (isSending) {
        return finish(400, 'send sensor');
    } else {
        isSending = true;
    }

    try { 
        if (CONFIG.farm.license) {
          try {
            let selectQuery = `select AA.id,BB.value from send_sensors as AA`;
            selectQuery +=` left join sensors as BB `;
            selectQuery +=` on AA.sensor_id=BB.id `;

            const rows = await sequelize.query(selectQuery, {
                type: Sequelize.QueryTypes.SELECT
            });

            rows.forEach((data) => {
                FLUID_UTIL.sendSensors(data.id,data.value);
            });
              callback(null, true);
          } catch (e) {
              callback(e, false);
          }

        }else{

        }
    } catch (e) {}

    isSending = false;
    finish(204);
    return true;
};
