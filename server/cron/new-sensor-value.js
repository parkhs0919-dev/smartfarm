const async = require('async');
const tcpp = require('tcp-ping');
const request = require('request');
const xor = require('buffer-xor');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const getSerialPort = require('../methods/serial-port').getSerialPort;
const setSensorValue = require('../methods/sensor').setSensorValue;
const setSendSensorDataLog = require('../methods/sensor').setSendSensorDataLog;
const CONFIG = require('../config');
const FILTER_UTIL = require('../utils/filter');

const START_CODE = 'F3', OP_CODE = '50', RESET_LITER_OP_CODE = '43';
const requestDelay = 500;

let isProcessing = false;
let isOnOffProcessing = false;
let isResetProcessing = false;
let isOtherWorkProcessing = false;
let sensorPort = null, sensors = [], addresses = {}, currentAddress = null;
let buffer1, buffer2, buffer3;
let instance = null;
let delayInstance = null;
let otherWorkList = [];

initBuffer();

module.exports = (finish) => {
    if (isProcessing) {
        return finish(204);
    } else {
        isProcessing = true;
    }

    let funcs = [];

    funcs.push(callback => {
        getSensorPort(callback);
    });

    funcs.push(callback => {
        otherWorkProcess(callback);
    });

    funcs.push(callback => {
        getSensors(callback);
    });

    funcs.push(callback => {
        setAddress(callback);
    });

    funcs.push(callback => {
        resetProcess(callback);
    });

    funcs.push(callback => {
        send(callback);
    });

    async.series(funcs, (error, results) => {
        if (error) {
            if (error === 'otherWork') {
                finish(204);
            } else {
                finish(400, error);
            }
        } else {
            finish(204);
        }
    });

    function getSensorPort(callback) {
        if (sensorPort && sensorPort.isOpen) {
            callback(null, true);
        } else {
            sensorPort = getSerialPort('newSensor');
            if (sensorPort && sensorPort.isOpen) {
                sensorPort.on('data', response);
                callback(null, true);
            } else {
                sensorPort = null;
                /**
                 *
                 * @type {boolean}
                 */
                isProcessing = false;
                callback('not connected sensor serial port', false);
            }
        }
    }

    function otherWorkProcess(callback) {
        isOtherWorkProcessing = true;
        if (otherWorkList.length) {
            const work = otherWorkList.splice(0, 1)[0];

            if (work.type === 'resetLiter' && work.address) {
                const startCode = Buffer.from(START_CODE);

                let dataLength = new Uint8Array(1);
                dataLength[0] = CONFIG.serialPort.sensorAddressLength + 3;

                let data = Buffer.concat([
                    startCode,
                    Buffer.from(dataLength),
                    Buffer.from(work.address),
                    Buffer.from(RESET_LITER_OP_CODE, 'hex')
                ]);

                const payload = Buffer.concat([data, crc(data)]);
                console.log('============= RESET LITER =============', payload);
                sensorPort.write(payload);
            }

            setTimeout(() => {
                isOtherWorkProcessing = false;
                callback(null, true);
            }, 1000);
        } else {
            isOtherWorkProcessing = false;
            callback(null, true);
        }
    }

    async function getSensors(callback) {
        try {
            if (sensors.length) {
                callback(null, true);
            } else {
                sensors = await sequelize.models.Sensor.findAll({
                    order: [['order', 'DESC']]
                });
                if (sensors.length) {
                    sensors.forEach(sensor => {
                        if (sensor.address) {
                            if (addresses[sensor.address] === undefined) {
                                addresses[sensor.address] = {
                                    buffer1Crc: true,
                                    buffer2Crc: true,
                                    buffer3Crc: true
                                };
                            }
                        }
                    });
                    callback(null, true);
                } else {
                    callback('empty sensors', false);
                }
            }
        } catch (e) {
            callback(e, false);
        }
    }

    function setAddress(callback) {
        const addressList = Object.keys(addresses);
        if (addressList.length) {
            if (currentAddress) {
                for (let i=0; i<addressList.length; i++) {
                    if (currentAddress === addressList[i]) {
                        if (i === addressList.length - 1) {
                            currentAddress = addressList[0];
                        } else {
                            currentAddress = addressList[i + 1];
                        }
                        break;
                    }
                }
            } else {
                currentAddress = addressList[0];
            }
            callback(null, true);
        } else {
            callback('empty address', false);
        }
    }

    async function resetProcess(callback) {
        isResetProcessing = true;
        let isActive = true;
        const addressList = Object.keys(addresses);
        for (let i=0; i<addressList.length; i++) {
            const address = addresses[addressList[i]];
            if (address.buffer1Crc) {
                isActive = false;
                break;
            } else if (address.buffer2Crc) {
                isActive = false;
                break;
            } else if (address.buffer3Crc) {
                isActive = false;
                break;
            }
        }
        if (isActive) {
            // console.log("============= CRC ERROR =============");
            try {
                await sequelize.models.SensorResetHistory.create({});
            } catch (e) {}
            sensorPort.write(Buffer.from('4854423A405245535440', 'hex'));
            setTimeout(() => {
                isResetProcessing = false;
                callback(null, true);
            }, 1000);
        } else {
            isResetProcessing = false;
            callback(null, true);
            return true;
        }
    }

    function send(callback) {
        if (currentAddress) {
            requestBoard(currentAddress);
            callback(null, true);
        } else {
            callback('empty address', false);
        }
    }

    async function onOffProcess(callback) {
        let isActive = true;
        const addressList = Object.keys(addresses);
        for (let i=0; i<addressList.length; i++) {
            const address = addresses[addressList[i]];
            if (address.buffer1Crc) {
                isActive = false;
                break;
            } else if (address.buffer2Crc) {
                isActive = false;
                break;
            } else if (address.buffer3Crc) {
                isActive = false;
                break;
            }
        }
        if (isActive) {
            await onOff();
        }
        callback(null, true);
        return true;
    }
};
module.exports.pushOtherWork = work => {
    return new Promise((resolve, reject) => {
        try {
            if (sensorPort && sensorPort.isOpen) {
                otherWorkList.push(work);
                resolve(true);
            } else {
                reject(false);
            }
        } catch (e) {
            reject(e);
        }
    });
};

function requestBoard() {
    const startCode = Buffer.from(START_CODE);

    let dataLength = new Uint8Array(1);
    dataLength[0] = CONFIG.serialPort.sensorAddressLength + 3;

    const boardAddress = Buffer.from(currentAddress);

    const opCode = Buffer.from(OP_CODE, 'hex');

    let data = Buffer.concat([
        startCode,
        Buffer.from(dataLength),
        boardAddress,
        opCode
    ]);

    const payload = Buffer.concat([data, crc(data)]);
    console.log('sensor request ============> ['+ currentAddress +']', FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), payload);
    let sensor_log=FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss')+' [요청] ==> ['+ currentAddress +'] ' + payload.toString('hex');
    setSendSensorDataLog(sensor_log)

    initBuffer();
    sensorPort.write(payload);
    instance = setTimeout(() => {
        addresses[currentAddress].buffer1Crc = false;
        addresses[currentAddress].buffer2Crc = false;
        addresses[currentAddress].buffer3Crc = false;
        initBuffer();
        instance = null;
        if (delayInstance) {
            clearTimeout(delayInstance);
            delayInstance = null;
        }
        delayInstance = setTimeout(() => {
            isProcessing = false;
        }, requestDelay);
    }, CONFIG.serialPort.sensorTimeout); // 10 seconds timeout
}

function response(data) {
    if (isResetProcessing) return true;
    if (isOtherWorkProcessing) return true;
    console.log(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), data);

    let sensor_log=FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss')+' [응답] ==> ['+ currentAddress +'] ' + data.toString("hex");     
    setSendSensorDataLog(sensor_log)   

    if (data.slice(0, 2).toString() === START_CODE) {
        if (buffer1.byteLength === 0) {
            buffer1 = Buffer.concat([buffer1, data]);
        } else if (buffer2.byteLength === 0 && buffer1.byteLength === 49) {
            buffer2 = Buffer.concat([buffer2, data]);
        } else if (buffer3.byteLength === 0 && buffer2.byteLength === 45) {
            buffer3 = Buffer.concat([buffer3, data]);
            if (checkLast(buffer3)) {
                parse();
            }
        } else {
            // console.error(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), "receive sensor value error");
        }
    } else {
        if (buffer2.byteLength === 0) {
            const totalLength = buffer1.byteLength + data.byteLength;
            if (totalLength > 49) {
                const prevLength = data.byteLength - (totalLength - 49);
                buffer1 = Buffer.concat([buffer1, data.slice(0, prevLength)]);
                buffer2 = Buffer.concat([buffer2, data.slice(prevLength, data.byteLength)]);
            } else {
                buffer1 = Buffer.concat([buffer1, data]);
            }
        } else if (buffer3.byteLength === 0) {
            const totalLength = buffer2.byteLength + data.byteLength;
            if (totalLength > 45) {
                const prevLength = data.byteLength - (totalLength - 45);
                buffer2 = Buffer.concat([buffer2, data.slice(0, prevLength)]);
                buffer3 = Buffer.concat([buffer3, data.slice(prevLength, data.byteLength)]);
            } else {
                buffer2 = Buffer.concat([buffer2, data]);
            }
        } else {
            buffer3 = Buffer.concat([buffer3, data]);
            if (checkLast(buffer3)) {
                parse();
            }
        }
    }
    // console.log(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), 'buffer1 size', buffer1.byteLength, ', buffer2 size', buffer2.byteLength, ', buffer3 size', buffer3.byteLength);
}

function parse() {
    let isUpdateTime = true;
    if (instance) {
        clearTimeout(instance);
        instance = null;
    }
    if (buffer1.byteLength === 49) {
        const crcResult = checkCrc(buffer1.slice(0, 48), buffer1.slice(48, 49));
        addresses[currentAddress].buffer1Crc = crcResult;
        if (crcResult) {
            const temp = buffer1.slice(12, 49).toString();
            setSensorValue(currentAddress, temp.slice(0, 1), temp.slice(1, 6));
            setSensorValue(currentAddress, temp.slice(6, 7), temp.slice(7, 12));
            setSensorValue(currentAddress, temp.slice(12, 13), temp.slice(13, 18));
            setSensorValue(currentAddress, temp.slice(18, 19), temp.slice(19, 24));
            setSensorValue(currentAddress, temp.slice(24, 25), temp.slice(25, 30));
            setSensorValue(currentAddress, temp.slice(30, 31), temp.slice(31, 36));
        } else {
            isUpdateTime = false;
        }
    } else {
        isUpdateTime = false;
    }
    if (buffer2.byteLength === 45) {
        const crcResult = checkCrc(buffer2.slice(0, 44), buffer2.slice(44, 45));
        addresses[currentAddress].buffer2Crc = crcResult;
        if (crcResult) {
            const temp = buffer2.slice(12, 45).toString();
            setSensorValue(currentAddress, temp.slice(0, 1), temp.slice(1, 6));
            setSensorValue(currentAddress, temp.slice(6, 7), temp.slice(7, 12));
            setSensorValue(currentAddress, temp.slice(24, 25), temp.slice(25, 32));
        } else {
            isUpdateTime = false;
        }
    } else {
        isUpdateTime = false;
    }
    if (CONFIG.sensor.isUseWaterLiter) {
        if (buffer3.byteLength === 51) {
            const crcResult = checkCrc(buffer3.slice(0, 50), buffer3.slice(50, 51));
            addresses[currentAddress].buffer3Crc = crcResult;
            if (crcResult) {
                let temp = buffer3.slice(12, 51);
                const R_key = temp.slice(0, 1).toString();
                const R_value = new Uint8Array(temp.slice(1, 2))[0];
                setSensorValue(currentAddress, R_key, R_value);

                const wx_value = new Uint8Array(temp.slice(33, 34))[0];
                const wy_value = new Uint8Array(temp.slice(34, 35))[0];
                const wz_value = new Uint8Array(temp.slice(35, 36))[0];
                setSensorValue(currentAddress, 'wx', wx_value);
                setSensorValue(currentAddress, 'wy', wy_value);
                setSensorValue(currentAddress, 'wz', wz_value);

                const f_key = temp.slice(36, 37).toString();
                const f_value = new Uint8Array(temp.slice(37, 38))[0];
                setSensorValue(currentAddress, f_key, f_value);
                temp = temp.slice(2, 39).toString();
                setSensorValue(currentAddress, temp.slice(0, 1), temp.slice(1, 8));
                setSensorValue(currentAddress, temp.slice(8, 9), temp.slice(9, 16));
                setSensorValue(currentAddress, temp.slice(16, 17), temp.slice(17, 24));
                setSensorValue(currentAddress, temp.slice(24, 25), temp.slice(25, 32));
            } else {
                isUpdateTime = false;
            }
        } else {
            isUpdateTime = false;
        }
    } else {
        if (buffer3.byteLength === 45) {
            const crcResult = checkCrc(buffer3.slice(0, 44), buffer3.slice(44, 45));
            addresses[currentAddress].buffer3Crc = crcResult;
            if (crcResult) {
                let temp = buffer3.slice(12, 45);
                const R_key = temp.slice(0, 1).toString();
                const R_value = new Uint8Array(temp.slice(1, 2))[0];
                setSensorValue(currentAddress, R_key, R_value);
                temp = temp.slice(2, 32).toString();
                setSensorValue(currentAddress, temp.slice(0, 1), temp.slice(1, 8));
                setSensorValue(currentAddress, temp.slice(8, 9), temp.slice(9, 16));
                setSensorValue(currentAddress, temp.slice(16, 17), temp.slice(17, 24));
                setSensorValue(currentAddress, temp.slice(24, 25), temp.slice(25, 32));
            } else {
                isUpdateTime = false;
            }
        } else {
            isUpdateTime = false;
        }
    }
    initBuffer();
    if (delayInstance) {
        clearTimeout(delayInstance);
        delayInstance = null;
    }
    delayInstance = setTimeout(() => {
        isProcessing = false;
    }, requestDelay);

    if (isUpdateTime) {
        return updateSensorTime(currentAddress);
    }
}

async function updateSensorTime(address) {
    try {
        await sequelize.models.Sensor.update({updatedAt: new Date()}, {where: {address}});
        return true;
    } catch (e) {
        return true;
    }
}

function crc(data) {
    let temp = data.slice(0, 1);
    for (let i=1; i<data.byteLength; i++) {
        temp = xor(temp, data.slice(i, i + 1));
    }
    return temp;
}

function initBuffer() {
    buffer1 = Buffer.from('');
    buffer2 = Buffer.from('');
    buffer3 = Buffer.from('');
}

function checkCrc(data, crcCode) {
    // console.log('check crc', crc(data).toString(), crcCode.toString());
    return crc(data).toString() === crcCode.toString();
}

function checkLast(buffer) {
    if (buffer.byteLength && buffer.byteLength > 3) {
        return (new Uint8Array(buffer.slice(2, 3))[0] + 2) === buffer.byteLength;
    } else {
        return false;
    }
}

async function onOff() {
    if (isOnOffProcessing) {
        return false;
    } else {
        isOnOffProcessing = true;
    }

    let offPromises = [], onPromises = [];
    CONFIG.sensor.onOffAddresses.forEach(({ip, port, io}) => {
        offPromises.push(onOffRequest(ip, port, io, 'off'));
        onPromises.push(onOffRequest(ip, port, io, 'on'));
    });

    if (offPromises.length) {
        try {
            await Promise.all(offPromises);
            await onOffDelay();
            await Promise.all(onPromises);
        } catch (e) {}
    }
    isOnOffProcessing = false;
}

function onOffDelay() {
    return new Promise((resolve, reject) => {
        try {
            setTimeout(() => {
                resolve(true);
            }, CONFIG.sensor.onOffDelay);
        } catch (e) {
            reject(e);
        }
    });
}

function onOffRequest(ip, port, io, state) {
    return new Promise((resolve, reject) => {
        try {
            if (ip && port && io) {
                tcpp.probe(ip, port, (err, available) => {
                    if (err) {
                        reject(err);
                    } else {
                        if (available) {
                            request.put({
                                uri: `http://${ip}:${port}/remoteio`,
                                method: 'PUT',
                                body: JSON.stringify({
                                    type: 'localmode',
                                    mode: state,
                                    io: io
                                })
                            }, (err, response, body) => {
                                if (err) {
                                    reject(err);
                                } else {
                                    if (response.statusCode === 200) {
                                        resolve(true);
                                    } else {
                                        reject(false);
                                    }
                                }
                            });
                        } else {
                            reject(false);
                        }
                    }
                });
            } else {
                reject(false);
            }
        } catch (e) {
            reject(e);
        }
    });
}
