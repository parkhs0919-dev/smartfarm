const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
let io = require('../methods/socket-io').getIo();

let isEmitting = false;

module.exports = (finish) => {
    if (isEmitting) {
        return finish(400, 'emitting sensor value');
    } else {
        isEmitting = true;
    }

    let funcs = [],
        innerSensors = [],
        outerSensors = [];

    funcs.push(callback => {
        getSensors(callback);
    });

    funcs.push(callback => {
        emit(callback);
    });

    async.series(funcs, (error, results) => {
        isEmitting = false;
        if (error) {
            finish(400, error);
        } else {
            finish(204);
        }
    });

    async function getSensors(callback) {
        try {
            innerSensors = await sequelize.models.Sensor.findAll({
                where: {
                    position: 'in'
                },
                attributes: ['id', 'value']
            });
            outerSensors = await sequelize.models.Sensor.findAll({
                where: {
                    position: 'out'
                },
                attributes: ['id', 'value']
            });
            if (innerSensors.length || outerSensors.length) {
                callback(null, true);
            } else {
                callback('empty sensors', false);
            }
        } catch (e) {
            console.error('emit-sensor-value getSensors', new Date(), e);
            callback(e, false);
        }
    }

    function emit(callback) {
        try {
            if (!io) {
                io = require('../methods/socket-io').getIo();
            }
            if (io) {
                io.emit('inner-sensor-values', {innerSensors});
                io.emit('outer-sensor-values', {outerSensors});
            }
            callback(null, true);
        } catch (e) {
            console.error('emit-sensor-value emit', new Date(), e);
            callback(e, false);
        }
    }
};
