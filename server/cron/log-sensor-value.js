const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const Op = Sequelize.Op;
const CONFIG = require('../config');
const SENSOR_MODEL = require('../models/sensor');

let isLogging = false;

module.exports = (finish) => {
    if (isLogging) {
        return finish(400, 'logging sensor value');
    } else {
        isLogging = true;
    }

    let now = new Date(),
        funcs = [],
        sensors = [],
        controls = [];
    let year = now.getFullYear(),
        month = now.getMonth() + 1,
        date = now.getDate(),
        hour = now.getHours(),
        minute = now.getMinutes();
    let table_name = 'log_sensor_' + year + (month < 10 ? '0' + month : month);
    let created_at = `${year}-${month < 10 ? '0' + month : month}-${date < 10 ? '0' + date : date} ${hour < 10 ? '0' + hour : hour}:${minute < 10 ? '0' + minute : minute}:00`;

    funcs.push(callback => {
        getSensors(callback);
    });

    funcs.push(callback => {
        getControls(callback);
    });

    funcs.push(callback => {
        getLog(callback);
    });

    funcs.push(callback => {
        createLogs(callback);
    });

    async.series(funcs, (error, results) => {
        isLogging = false;
        if (error) {
            finish(400, error);
        } else {
            finish(204);
        }
    });

    async function getSensors(callback) {
        try {
            sensors = await sequelize.models.Sensor.findAll({
                attributes: ['id', 'position', 'type', 'value']
            });
            if (sensors.length) {
                callback(null, true);
            } else {
                callback('empty sensors', false);
            }
        } catch (e) {
            console.error('log-sensor-value getSensors', new Date(), e);
            callback(e, false);
        }
    }

    async function getControls(callback) {
        try {
            let control_query =` select group_concat(AA.control_name order by AA.id asc) as name, `;
            control_query +=` group_concat(AA.current order by AA.id asc ) as current, `;
            control_query +=` group_concat(CASE WHEN AA.state='on' THEN '가동' `;
            control_query +=` WHEN AA.state='off' THEN '정지' `;
            control_query +=` WHEN AA.state='stop' THEN '정지' `;
            control_query +=` WHEN AA.state='open' THEN '열기' `;
            control_query +=` WHEN AA.state='close' THEN '닫기' `;
            control_query +=` ELSE  AA.state END  `;
            control_query +=` order by AA.id asc ) as state `;
            control_query +=` from controls as AA  `;

            controls = await sequelize.query(control_query, {
                type: Sequelize.QueryTypes.SELECT
            });
            if (controls.length) {
                callback(null, true);
            } else {
                callback('empty controls', false);
            }
        } catch (e) {
            console.error('log-sensor-value getControls', new Date(), e);
            callback(e, false);
        }
    }

    async function getLog(callback) {
        try {
            const log = await sequelize.models.Log.findOne({
                where: {table_name}
            });
            if (log) {
                callback(null, true);
            } else {
                const query = `CREATE TABLE \`${table_name}\` (
  \`id\` int(10) unsigned NOT NULL AUTO_INCREMENT,
  \`sensor_id\` int(10) unsigned NOT NULL,
  \`position\` enum('in','out') COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'in',
  \`type\` enum('temperature', 'soilTemperature', 'humidity', 'soilHumidity', 'solar', 'windSpeed', 'ec', 'ph', 'co2', 'windDirection', 'rain', 'power', 'water', 'battery', 'liter', 'door', 'frostHumidity', 'frostTemperature', 'frost', 'window', 'fire', 'dryTemperature', 'wetTemperature', 'co2Temperature', 'co2Humidity','humidityDeficit','korinsPressure','korinsFall','korinsHumidity','korinsTemperature','korinsWindSpeed','korinsWindDirection','korinsSolar','sumOfTemperature','FolateTemperature','CultureMediumWeight','CultureMediumWaterWeight','sugar','korinsInstantRainFall','korinsCumulativeRainFall','relativeHumidity','dewPointTemperature') COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'temperature',
  \`value\` double(8,2) DEFAULT NULL,     
  \`created_at\` datetime NOT NULL,
  \`data\` text DEFAULT NULL,
  PRIMARY KEY (\`id\`),
  KEY \`sensor_id\` (\`sensor_id\`),
  KEY \`created_at\` (\`created_at\`),
  KEY \`index_key\` (\`sensor_id\`,\`created_at\`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;`;
                await sequelize.query(query);
                await sequelize.models.Log.create({table_name});
                callback(null, true);
            }
        } catch (e) {
            console.error('log-sensor-value getLog', new Date(), e);
            callback(e, false);
        }
    }

    async function createLogs(callback) {
        try {
            let query = `INSERT INTO ${table_name} (\`sensor_id\`, \`position\`, \`type\`, \`value\`, \`created_at\` , \`data\`) VALUES `;
            sensors.forEach((sensor, index) => {
                if (index) {
                    query += ', ';
                }
                query += `(${sensor.id}, '${sensor.position}', '${sensor.type}', ${sensor.value}, '${created_at}' , '${JSON.stringify(controls)}')`;
            });
            await sequelize.query(query, {
                type: Sequelize.QueryTypes.INSERT
            });
            callback(null, true);
        } catch (e) {
            console.error('log-sensor-value createLogs', new Date(), e);
            callback(e, false);
        }
    }

//     async function getLog(callback) {
//         const log = await sequelize.models.Log.findOne({
//             where: {table_name}
//         });
//         if (log) {
//             callback(null, true);
//         } else {
//             let query = `CREATE TABLE \`${table_name}\` (`;
//             sensors.forEach((sensor) => {
//                 query += `\`${sensor.position}_${sensor.type}_${sensor.id}\` double(8,2) DEFAULT NULL,`;
//             });
//             query += `\`created_at\` datetime NOT NULL,
//   PRIMARY KEY (\`created_at\`)
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;`;
//             try {
//                 await sequelize.query(query);
//                 await sequelize.models.Log.create({table_name});
//                 callback(null, true);
//             } catch (e) {
//                 callback('create log sensor table failed', false);
//             }
//         }
//     }

    // async function createLogs(callback) {
    //     let query = `INSERT INTO ${table_name} (`;
    //     let subQuery1 = '';
    //     let subQuery2 = '';
    //     sensors.forEach((sensor, index) => {
    //         if (index) {
    //             subQuery1 += ', ';
    //             subQuery2 += ', ';
    //         }
    //         subQuery1 += `\`${sensor.position}_${sensor.type}_${sensor.id}\``;
    //         subQuery2 += `${sensor.value}`;
    //     });
    //     query += subQuery1 + ', `created_at`) VALUES (' + subQuery2 + `, '${created_at}')`;
    //     try {
    //         await sequelize.query(query, {
    //             type: Sequelize.QueryTypes.INSERT
    //         });
    //         callback(null, true);
    //     } catch (e) {
    //         callback('create log sensor value failed', false);
    //     }
    // }
};
