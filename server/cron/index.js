const cron = require('node-cron');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const startTime = require('./auto-control').startTime;
const CONFIG = require('../config');

if(CONFIG.serialPort.UseNewSensorType==2) sensor_path='../cron/new-sensor-value';
else sensor_path='../cron/set-new-sensor-value';
let pushOtherWork = require(sensor_path).pushOtherWork;

global.sensorSetData=[];

let perOneSeconds = [];
for (let i=0; i<60; i++) {
    perOneSeconds.push(i * 1);
}
perOneSeconds = `${perOneSeconds.join(',')} * * * * *`;

let perTwoSeconds = [];
for (let i=0; i<30; i++) {
    perTwoSeconds.push(i * 2);
}
perTwoSeconds = `${perTwoSeconds.join(',')} * * * * *`;

let perThreeSeconds = [];
for (let i=0; i<20; i++) {
    perThreeSeconds.push(i * 3);
}
perThreeSeconds = `${perThreeSeconds.join(',')} * * * * *`;

let perFiveSeconds = [];
for (let i=0; i<12; i++) {
    perFiveSeconds.push(i * 5);
}
perFiveSeconds = `${perFiveSeconds.join(',')} * * * * *`;

let perTenSeconds = [];
for (let i=0; i<6; i++) {
    perTenSeconds.push(i * 10);
}
perTenSeconds = `${perTenSeconds.join(',')} * * * * *`;

let perOneMinute = [];
for (let i=0; i<60; i++) {
    perOneMinute.push(i);
}
perOneMinute = `${perOneMinute.join(',')} * * * *`;

let perOneDay = '0 0 * * *';

module.exports = async () => {
    cron.schedule(perFiveSeconds, () => {
        require('./emit-sensor-value')((status, data) => {
            if (status !== 204) {
                console.log(data);
            }
        });
    });

    cron.schedule(perTenSeconds, () => {
        require('./emit-sensor-avg-value')((status, data) => {
            if (status !== 204) {
                console.log(data);
            }
        });
    });

    cron.schedule(perFiveSeconds, () => {
        require('./alarm')((status, data) => {
            if (status !== 204) {
                console.log(data);
            }
        });
    });

    cron.schedule(perOneMinute, () => {
        require('./ip-refresh')((status, data) => {
            if (status !== 204) {
                console.log(data);
            }
        });
    });

    cron.schedule(perThreeSeconds, () => {
        require('./send-sensor')((status, data) => {
            if (status !== 204) {
                //console.log(data);
            }
        });
    });

    if (process.env.CRON_ENV) {
        setTimeout(() => activeCron(), 5000);
    }
};

async function activeCron() {
    const modes = await sequelize.models.Mode.findAll({});
    for (let i=0; i<modes.length; i++) {
        const house_id = modes[i].house_id;
        const period = 10;

        const rest = 60 / period;
        let per = [];
        for (let j=0; j<rest; j++) {
            per.push(j * period);
        }
        per = `${per.join(',')} * * * * *`;
        startTime(house_id, 60);
        cron.schedule(per, () => {
            require('./auto-control')(house_id, period, (status, data) => {
                if (status !== 204) {
                    console.log(data);
                }
            });
        });
    }

    cron.schedule(perFiveSeconds, () => {
        require('./collect-sensor-value')((status, data) => {
            if (status !== 204) {
                console.log(data);
            }
        });
    });

    cron.schedule(perOneMinute, () => {
        require('./sum-sensor-value')((status, data) => {
            if (status !== 204) {
                console.log(data);
            }
        });
    });

    cron.schedule(perOneMinute, () => {
        require('./sum-of-temperature-value')((status, data) => {
            if (status !== 204) {
                console.log(data);
            }
        });
    });

    if (CONFIG.serialPort.isUseNewSensor) {
        if(CONFIG.serialPort.UseNewSensorType==2){
            cron.schedule(perFiveSeconds, () => {
                require('./set-new-sensor-value')((status, data) => {
                    if (status !== 204) {
                        console.log(data);
                    }
                });
            });
        }else{
            cron.schedule(perFiveSeconds, () => {
                require('./new-sensor-value')((status, data) => {
                    if (status !== 204) {
                        console.log(data);
                    }
                });
            });
        }
    }

    cron.schedule(perOneMinute, () => {
        require('./log-sensor-value')((status, data) => {
            if (status !== 204) {
                console.log(data);
            }
        });
    });

    cron.schedule(perOneMinute, () => {
        require('./log-control-value')((status, data) => {
            if (status !== 204) {
                console.log(data);
            }
        });
    });

    cron.schedule(perOneDay, () => {
        require('./dump-log')((status, data) => {
            if (status !== 204) {
                console.log(data);
            }
        });
    });

    if (CONFIG.control.isUsePestControl) {
        const houses = await sequelize.models.House.findAll({});
        for (let i=0; i<houses.length; i++) {
            const house_id = houses[i].id;
            cron.schedule(perOneSeconds, () => {
                require('./pest-control')(house_id, (status, data) => {
                    if (status !== 204) {
                        //console.log(data);
                    }
                });
            });
        }
    }

    // cron.schedule('0 0 0 * * *', async () => {
    //     const flowMeters = await sequelize.models.FlowMeter.getFlowMeterAddress();
    //     for (let i=0; i<flowMeters.length; i++) {
    //         const flowMeter = flowMeters[i];
    //         //console.log('24시 초기화 ', flowMeter.address);
    //         if(flowMeter.address){
    //             await pushOtherWork({
    //                 type: 'resetLiter',
    //                 address: flowMeter.address
    //             });
    //         }
    //     }
    // });

}
