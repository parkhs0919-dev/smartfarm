const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const FILTER_UTIL = require('../utils/filter');
const CONTROL_METHOD = require('../methods/pest-control');
const CONFIG = require('../config');
const STD = require('../metadata/standards');
const getIo = require('../methods/socket-io').getIo;

let now = new Date();
let io = getIo();
let houses = {};
let pest_control = {};
let pest_control_zone = {};
let currentControlZone = {};
let isControlProcessing = [];
let isResetProcessing = [];
let isResetDataProcessing = [];
let timer = 0;

module.exports = (house_id, finish) => {
    let funcs = [];
    
    funcs.push(callback => {
        check(callback);
    });

    funcs.push(callback => {
        getPestControls(callback);
    });

    funcs.push(callback => {
        setPestControlZone(callback);
    });

    funcs.push(callback => {
        activePestControls(callback);
    });

    async.series(funcs, (error, results) => {
        if (error) {
            if (error === 'empty') {
                finish(204);
            } else {
                finish(400, error);
            }
        } else {
            finish(204);
        }
    });

    async function check(callback) {
        try {
            timer = (timer >= 60) ? 0 : timer + 1;
            isControlProcessing[house_id] = await CONTROL_METHOD.getIsControlProcessing(house_id);
            isResetProcessing[house_id] = await CONTROL_METHOD.getIsResetProcessing(house_id);
            isResetDataProcessing[house_id] = await CONTROL_METHOD.getIsResetDataProcessing(house_id);
            if (isResetProcessing[house_id]) {  
                houses[house_id] = null;     
                currentControlZone[house_id] = null;
                if (isResetDataProcessing[house_id]) {
                    await CONTROL_METHOD.setIsResetDataProcessing(house_id, false);
                    await CONTROL_METHOD.setIsResetProcessing(house_id, false);
                } else {
                    await CONTROL_METHOD.setIsResetProcessing(house_id, false);
                    await CONTROL_METHOD.reSetPestControl(house_id);
                }
            }
            if (isControlProcessing[house_id] && !isResetProcessing[house_id]) { 
                await CONTROL_METHOD.setSendIsControlProcessing(house_id,false);
                callback(null, true);         
            } else {  
                if (timer == 60) {
                    //console.log('time check ', timer);
                    CONTROL_METHOD.sendPestControlMedicineOff(house_id);
                }
                callback('cron pest-control check false', false);
            }
        } catch (e) {
            callback(e, false);
        }
    }

    async function getPestControls(callback) {
        try {
            if (houses[house_id]) {
                callback(null, true);
            } else {
                pest_control = await CONTROL_METHOD.getPestControl(house_id);
                pest_control_zone = await CONTROL_METHOD.getPestControlZone();
                const pest_control_List = (pest_control) ? Object.keys(pest_control) : null;
                let pestControls = {};
                if (pest_control_List && pest_control_List.length) {
                    pest_control_List.forEach(key => {
                        if (key) {
                            if (pestControls[key] === undefined) {
                                pestControls[key] = {};
                                pestControls[key] = pest_control_zone[key];
                            } else {
                                pestControls[key] = pest_control_zone[key];
                            }
                        }
                    });
                    houses[house_id] = pestControls;
                    callback(null, true);
                } else {
                    callback('empty pest_controls[' + house_id + '] false ', false);
                }
            }
        } catch (e) {
            callback(e, false);
        }
    }

    async function setPestControlZone(callback) {
        const controlList = Object.keys(houses[house_id]);  
        isControlProcessing[house_id] = await CONTROL_METHOD.getIsControlProcessing(house_id);
        isResetProcessing[house_id] = await CONTROL_METHOD.getIsResetProcessing(house_id);
        if (controlList.length) {    
            if (currentControlZone[house_id]) {
                for (let i = 0; i < controlList.length; i++) {
                    if (currentControlZone[house_id] === controlList[i]) {
                        if (i === controlList.length - 1) {
                            currentControlZone[house_id] = controlList[0];
                        } else {
                            currentControlZone[house_id] = controlList[i + 1];
                        }
                        break;
                    }
                }
            } else {
                currentControlZone[house_id] = controlList[0];
            }
            callback(null, true);
        } else {
            callback('empty address', false);
        }
    }

    async function activePestControls(callback) {
        try {
            //console.log(house_id+'============',currentControlZone[house_id])
            CONTROL_METHOD.activePestControl(house_id, currentControlZone[house_id]);
            callback(null, true);
        } catch (e) {   
            console.error('pest-control activePestControls', new Date(), e);
            callback(e, false);
        }
    }
};