const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const getSensorValue = require('../methods/sensor').getSensorValue;
const CONFIG = require('../config');
const FILTER_UTIL = require('../utils/filter');

const queueSize = Number(CONFIG.sensor.queueSize);
const Op = Sequelize.Op;

let isCollecting = false;
let sensors = [],
    collects = [];

module.exports = (finish) => {
    if (isCollecting) {
        return finish(400, 'collecting sensor value');
    } else {
        isCollecting = true;
    }

    let funcs = [];

    funcs.push(callback => {
        getSensors(callback);
    });

    funcs.push(callback => {
        collect(callback);
    });

    funcs.push(callback => {
        update(callback);
    });

    async.series(funcs, (error, results) => {
        isCollecting = false;
        if (error) {
            finish(400, error);
        } else {
            finish(204);
        }
    });

    async function getSensors(callback) {
        try {
            if (sensors.length) {
                callback(null, true);
            } else {
                sensors = await sequelize.models.Sensor.findAll({
                    where: {                    
                        [Op.and]: [{
                           type : {[Op.ne] : 'sumOfTemperature'}
                        }, {   
                           type : {[Op.ne] : 'sugar'}
                        }, {                                 
                            sensor_id: null   
                        }]
                    }
                });
                if (sensors.length) {
                    callback(null, true);
                } else {
                    callback('empty sensors', false);
                }
            }
        } catch (e) {
            console.error('collect-sensor-value getSensors', new Date(), e);
            callback(e, false);
        }
    }

    function collect(callback) {
        try {
            let subFuncs = [];
            for (let i=0; i<sensors.length; i++) {
                ((index) => {
                    subFuncs.push((subCallback) => {
                        if (collects.length === sensors.length) {
                            if (collects[index].values.length === queueSize) {
                                collects[index].values.splice(0, 1);
                            }
                            /**
                             * collect sensor value
                             */
                            // collects[index].values.push(Number((Math.random() * 30).toFixed(2)));
                            collects[index].values.push(getSensorValue(sensors[index].id));
                        } else {
                            /**
                             * collect sensor value
                             */
                            // collects.push({
                            //     id: sensors[index].id,
                            //     values: [Number((Math.random() * 30).toFixed(2))]
                            // });
                            collects.push({
                                id: sensors[index].id,
                                values: [getSensorValue(sensors[index].id)]
                            });
                        }
                        subCallback(null, true);
                    });
                })(i);
            }

            async.parallel(subFuncs, (error, results) => {
                if (error) {
                    callback(error, false);
                } else {
                    callback(null, true);
                }
            });
        } catch (e) {
            console.error('collect-sensor-value collect', new Date(), e);
            callback(e, false);
        }
    }

    async function update(callback) {
        try {
            if (collects[0].values.length === queueSize) {
                // let subFuncs = [];
                //
                // for (let i=0; i<sensors.length; i++) {
                //     ((index) => {
                //         subFuncs.push((subCallback) => {
                //             let value, type = sensors[index].type;
                //             if (type === 'rain' || type === 'water' || type === 'power' || type === 'liter') {
                //                 value = collects[index].values[collects[index].values.length - 1];
                //             } else {
                //                 value = avg(collects[index].values);
                //             }
                //             if (isNaN(value)) {
                //                 subCallback(null, true);
                //             } else {
                //                 const query = `UPDATE sensors SET \`value\` = ${value} + revision WHERE id = ${collects[index].id}`;
                //                 sequelize.query(query, {
                //                     type: Sequelize.QueryTypes.UPDATE
                //                 }).then(() => {
                //                     subCallback(null, true);
                //                 }).catch(e => {
                //                     console.log('update sensor value error', e);
                //                     subCallback(null, true);
                //                 });
                //             }
                //         });
                //     })(i);
                // }
                //
                // async.parallel(subFuncs, (error, results) => {
                //     if (error) {
                //         callback(error, false);
                //     } else {
                //         callback(null, true);
                //     }
                // });
                let isActive = false;
                let query = `UPDATE sensors SET \`value\` = (CASE`;
                let ids = [];
                for (let i=0; i<sensors.length; i++) {
                    let value, type = sensors[i].type;
                    if (type === 'rain' || type === 'water' || type === 'power' || type === 'liter' || type === 'fire' || type === 'window' || type === 'door' 
                    || type === 'CultureMediumWeight' || type === 'CultureMediumWaterWeight' || type === 'korinsFall' || type === 'korinsCumulativeRainFall' || type === 'korinsInstantRainFall' ) {
                        value = collects[i].values[collects[i].values.length - 1];  
                    } else {
                        value = avg(collects[i].values);
                        if(type === 'temperature'){
                            console.log(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), 'temperature collects values', collects[i].values);
                        }    
                    }
                    if (!isNaN(value)) {
                        isActive = true;
                        query += ` WHEN id = ${collects[i].id} THEN ${value} + revision`;
                        ids.push(collects[i].id);
                    }
                }
                query += ` ELSE value END) WHERE id IN(${ids.join(',')})`;
                if (isActive) {
                    await sequelize.query(query, {
                        type: Sequelize.QueryTypes.UPDATE
                    });
                }
                callback(null, true);
            } else {
                callback(null, true);
            }
        } catch (e) {
            console.error('collect-sensor-value update', new Date(), e);
            callback(e, false);
        }
    }

    function avg(array) {
        let temp = 0;
        if (array.length >= 3) {
            if (array[0] === null) return NaN;
            let min = {
                index: 0,
                value: parseFloat(array[0])
            };
            let max = {
                index: 0,
                value: parseFloat(array[0])
            };
            for (let i=1; i<array.length; i++) {
                if (array[i] === null) return NaN;
                let target = parseFloat(array[i]);
                if (target < min.value) {
                    min.index = i;
                    min.value = target;
                }
                if (target > max.value) {
                    max.index = i;
                    max.value = target;
                }
            }
            let length = array.length;
            for (let i=0; i<array.length; i++) {
                if (i === min.index || i === max.index) {
                    length--;
                } else {
                    temp += parseFloat(array[i]);
                }
            }
            return (temp / length).toFixed(2);
        } else {
            for (let i=0; i<array.length; i++) {
                if (array[i] !== null) {
                    temp += parseFloat(array[i]);
                } else {
                    return NaN;
                }
            }
            return (temp / array.length).toFixed(2);
        }
    }

    function checkAllTrue(array) {
        for (let i=0; i<array.length; i++) {
            if (parseFloat(array[i]) === 0) {
                return 0;
            }
        }
        return 1;
    }
};
