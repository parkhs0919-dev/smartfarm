const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;

let isLogging = false;

module.exports = finish => {
    if (isLogging) {
        return finish(400, 'logging sensor value');
    } else {
        isLogging = true;
    }

    let now = new Date(),
        funcs = [],
        controls = [];
    let year = now.getFullYear(),
        month = now.getMonth() + 1,
        date = now.getDate(),
        hour = now.getHours(),
        minute = now.getMinutes();
    let table_name = 'log_control_' + year + (month < 10 ? '0' + month : month);
    let created_at = `${year}-${month < 10 ? '0' + month : month}-${date < 10 ? '0' + date : date} ${hour < 10 ? '0' + hour : hour}:${minute < 10 ? '0' + minute : minute}:00`;

    funcs.push(callback => {
        getControls(callback);
    });

    funcs.push(callback => {
        getLog(callback);
    });

    funcs.push(callback => {
        createLogs(callback);
    });

    async.series(funcs, (error, results) => {
        isLogging = false;
        if (error) {
            finish(400, error);
        } else {
            finish(204);
        }
    });

    async function getControls(callback) {
        try {
            controls = await sequelize.models.Control.findAll({
                attributes: ['id', 'house_id', 'type', 'state', 'mode', 'direction', 'current', 'range']
            });

            if (controls.length) {
                callback(null, true);
            } else {
                callback('empty controls', false);
            }
        } catch (e) {
            console.error('log-control-value getControls', new Date(), e);
            callback(e, false);
        }
    }

    async function getLog(callback) {
        let transaction;
        try {
            const log = await sequelize.models.Log.findOne({
                where: {table_name}
            });
            if (log) {
                callback(null, true);
            } else {
                const query = `CREATE TABLE \`${table_name}\` (
  \`id\` int(10) unsigned NOT NULL AUTO_INCREMENT,
  \`control_id\` int(10) unsigned NOT NULL,
  \`house_id\` int(10) unsigned NOT NULL,
  \`type\` enum('motor','power','control') NOT NULL DEFAULT 'motor',
  \`state\` enum('open','stop','close','on','off') NOT NULL DEFAULT 'stop',
  \`mode\` enum('manual','auto') NOT NULL DEFAULT 'manual',
  \`direction\` enum('none','left','right') NOT NULL DEFAULT 'none',
  \`current\` double(8,2) NOT NULL DEFAULT '0.00',
  \`range\` double(8,2) NOT NULL DEFAULT '0.00',
  \`created_at\` datetime NOT NULL,
  PRIMARY KEY (\`id\`),
  KEY \`control_id\` (\`control_id\`),
  KEY \`created_at\` (\`created_at\`),
  KEY \`index_key\` (\`control_id\`,\`created_at\`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;`;

                transaction = await sequelize.transaction();

                await sequelize.query(query, {
                    transaction
                });
                await sequelize.models.Log.create({table_name}, {
                    transaction
                });

                await transaction.commit();
                callback(null, true);
            }
        } catch (e) {
            transaction && await transaction.rollback();
            console.error('log-control-value getLog', new Date(), e);
            callback(e, false);
        }
    }

    async function createLogs(callback) {
        try {
            let query = `INSERT INTO ${table_name} (\`control_id\`, \`house_id\`, \`type\`, \`state\`, \`mode\`, \`direction\`, \`current\`, \`range\`, \`created_at\`) VALUES `;
            controls.forEach((control, index) => {
                if (index) {
                    query += ', ';
                }
                query += `(${control.id}, ${control.house_id}, '${control.type}', '${control.state}', '${control.mode}', '${control.direction}', ${control.current}, ${control.range}, '${created_at}')`;
            });
            await sequelize.query(query, {
                type: Sequelize.QueryTypes.INSERT
            });
            callback(null, true);
        } catch (e) {
            console.error('log-control-value createLogs', new Date(), e);
            callback(e, false);
        }
    }
};
