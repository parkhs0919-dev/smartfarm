const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const FILTER_UTIL = require('../utils/filter');

let isSumming = false;

module.exports = finish => {
    if (isSumming) {
        return finish(400, 'sum of temperature value');
    } else {
        isSumming = true;
    }

    let funcs = [], sensorInfos = [];
 
    
    funcs.push(callback => {
        getSensorInfos(callback);
    });

    funcs.push(callback => {
        store(callback);
    });

    async.series(funcs, (error, results) => {
        isSumming = false;
        if (error) {
            if (error === 'empty') {
                finish(204);
            } else {
                finish(400, error);
            }
        } else {
            finish(204);
        }
    });

    async function getSensorInfos(callback) {
        try {
            const query = `SELECT AA.*, BB.address FROM sensor_infos as AA left join sensors as BB on AA.sensor_id=BB.id where LENGTH(BB.address) >5 `;
            sensorInfos = await sequelize.query(query, {
                type: Sequelize.QueryTypes.SELECT
            });
            if (sensorInfos.length) {
                callback(null, true);  
            } else {
                callback('empty', false);
            }
        } catch (e) {
            console.error('sum-of-temperature-value getSensorInfos', new Date(), e);
            callback(e, false);
        }  
        return true;
    }

    async function store(callback) {
        try {
            for (let i=0; i<sensorInfos.length; i++) {
                
                const sensorInfo = sensorInfos[i];               
                const sum = await sequelize.models.Sensor.getSum(sensorInfo);     
                await sequelize.models.Sensor.update({    
                    value: sum.toFixed(2)
                }, {    
                    where: {   
                        address: sensorInfo.address,
                        type : 'sumOfTemperature'
                    }          
                });
            }     
            callback(null, true);
        } catch (e) {
            console.error('sum-of-temperature-value store', new Date(), e);
            callback(e, false);
        }  
        return true;
    }  
};
