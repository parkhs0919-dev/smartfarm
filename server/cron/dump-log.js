const fs = require('fs');
const path = require('path');
const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const ERP_UTIL = require('../utils/erp');

const logSize = 1000;

let isDumping = false;

module.exports = (finish) => {
    if (isDumping) {
        return finish(400, 'dumping log');
    } else {
        isDumping = true;
    }

    let funcs = [],
        logCount = 0,
        now = new Date(),
        log = null;
    let year = now.getFullYear(),
        month = now.getMonth();
    let table_name = 'log_sensor_' + (month === 0 ? year - 1 : year) + (month === 0 ? 12 : '') + (month > 0 && month < 10 ? '0' + month : '') + (month >= 10 ? month : '');
    let tablePath = path.join(__dirname, `../../dumps/${table_name}.json`);

    funcs.push(callback => {
        findTable(callback);
    });

    funcs.push(callback => {
        count(callback);
    });

    funcs.push(callback => {
        dump(callback);
    });

    funcs.push(callback => {
        sendToErp(callback);
    });

    funcs.push(callback => {
        updateLog(callback);
    });

    async.series(funcs, (error, results) => {
        isDumping = false;
        if (error) {
            if (error === 'empty') {
                finish(204);
            } else {
                finish(400, error);
            }
        } else {
            finish(204);
        }
    });

    async function findTable(callback) {
        try {
            log = await sequelize.models.Log.findOne({
                where: {
                    table_name,
                    is_send: false
                }
            });
            if (log) {
                callback(null, true);
            } else {
                callback('empty', false);
            }
        } catch (e) {
            console.error('dump-log findTable', new Date(), e);
            callback(e, false);
        }
    }

    async function count(callback) {
        try {
            const query = `SELECT COUNT(*) AS count FROM ${table_name}`;
            const result = await sequelize.query(query, {
                type: Sequelize.QueryTypes.SELECT
            });
            if (result && result.length && result[0].count) {
                logCount = result[0].count;
                callback(null, true);
            } else {
                callback('empty', false);
            }
        } catch (e) {
            console.error('dump-log count', new Date(), e);
            callback(e, false);
        }
    }

    function dump(callback) {
        if (!fs.existsSync(tablePath)) {
            let funcs = [];

            const endCount = Math.ceil(logCount / logSize);

            for (let i=0; i<endCount; i++) {
                ((i) => {
                    funcs.push(subCallback => {
                        loadLog(i, endCount - 1, subCallback);
                    });
                })(i);
            }

            async.series(funcs, (error, results) => {
                if (error) {
                    callback(error, false);
                } else {
                    callback(null, true);
                }
            });
        } else {
            callback(null, true);
        }
    }

    async function loadLog(i, endIndex, callback) {
        try {
            const query = `SELECT sensor_id, position, \`type\`, \`value\`, created_at FROM ${table_name} ORDER BY id ASC LIMIT ${logSize} OFFSET ${logSize * i}`;
            const logs = await sequelize.query(query, {
                type: Sequelize.QueryTypes.SELECT
            });
            if (logs.length) {
                let temp = JSON.stringify(logs);
                if (i === 0) {
                    temp.replace(']', ',');
                } else if (i === endIndex) {
                    temp.replace('[', '');
                } else {
                    temp.replace('[', '').replace(']', ',');
                }
                fs.appendFileSync(tablePath, temp);
            }
            callback(null, true);
        } catch (e) {
            callback(e, false);
        }
    }

    async function sendToErp(callback) {
        if (fs.existsSync(tablePath)) {
            try {
                await ERP_UTIL.sendLogFile('farm', tablePath, table_name);
                callback(null, true);
            } catch (e) {
                callback(e, false);
            }
        } else {
            callback('empty', false);
        }
    }

    async function updateLog(callback) {
        try {
            await sequelize.models.Log.update({
                is_send: true
            }, {
                where: {
                    id: log.id
                }
            });
            callback(null, true);
        } catch (e) {
            console.error('dump-log updateLog', new Date(), e);
            callback(e, false);
        }
    }
};
