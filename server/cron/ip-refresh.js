const cmd = require('node-cmd');
const ERP_UTIL = require('../utils/erp');
const CONFIG = require('../config');

let isRefreshing = false;
let ipAddress = null;

const command = 'curl ifconfig.me';
const isWindow = process.platform === "win32";

module.exports = async (finish) => {
    if (isRefreshing) {
        return finish(400, 'refreshing');
    } else {
        isRefreshing = true;
    }

    try {
        if (CONFIG.farm.license) {
            const result = await cmdGet();
            if (result !== ipAddress) {
                await ERP_UTIL.ip('farm');
                ipAddress = result;
            }
        }
        if (CONFIG.animal.license) {
            const result = await cmdGet();
            if (result !== ipAddress) {
                await ERP_UTIL.ip('animal');
                ipAddress = result;
            }
        }
    } catch (e) {}

    isRefreshing = false;
    finish(204);
    return true;
};
module.exports.getIpAddress = async () => {
    try {
        if (ipAddress) {
            return ipAddress;
        } else {
            return await cmdGet();
        }
    } catch (e) {
        return '';
    }
};

function cmdGet () {
    return new Promise(async (resolve, reject) => {
        try {
            cmd.get(command, (err, data, stderr) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        } catch (e) {
            reject(e);
        }
    });
}
