const async = require('async');
const tcpp = require('tcp-ping');
const request = require('request');
const xor = require('buffer-xor');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const getSerialPort = require('../methods/serial-port').getSerialPort;
const setNewSensorValue = require('../methods/sensor').setNewSensorValue;
const setSendSensorDataLog = require('../methods/sensor').setSendSensorDataLog;
const CONFIG = require('../config');   
const FILTER_UTIL = require('../utils/filter');
const BigNumber = require('bignumber.js');
    
const START_CODE = 'F3', OP_CODE = '50', RESET_LITER_OP_CODE = '43';
const requestDelay = 500;

let isProcessing = false;
let isOnOffProcessing = false;
let isResetProcessing = false;
let isOtherWorkProcessing = false;
let sensorPort = null, sensors = [], addresses = {} , currentAddress = null, currentControlAddress = null;
let buffer1;
let instance = null;
let delayInstance = null;   
let otherWorkList = [];
let buffData='';   
let isBuffData=false;  
let isControlProcessing=false;
let buffDataCheck='';


module.exports = (finish) => {
    if (isProcessing) {
        return finish(204);
    } else {
        isProcessing = true;
    }
 
    let funcs = [];
    const controlWork = Object.keys(controlWorkList);
    if(controlWork.length>0) isControlProcessing=true;
    else isControlProcessing=false;
    
    funcs.push(callback => {
        getSensorPort(callback);
    });          

    if(isControlProcessing){
        initBuffer2();  
        funcs.push(callback => {
            setBoardAddress(callback);  
        });
        funcs.push(callback => {
           sendControl(callback)  
        }); 
    }else{       
        funcs.push(callback => { 
            otherWorkProcess(callback);
        });  

        funcs.push(callback => {
            getSensors(callback);
        });
        
        funcs.push(callback => {
            setAddress(callback);
        });

        funcs.push(callback => {
            setConcatSendCode(callback);
        });  
        
        funcs.push(callback => {
            resetProcess(callback);
        });     
        funcs.push(callback => {
            send(callback);
        }); 

    }     
  
    async.series(funcs, (error, results) => {
        if (error) {
            if (error === 'otherWork') {
                finish(204);
            } else {
                finish(400, error);
            }
        } else {
            finish(204);
        }
    });

    function getSensorPort(callback) {

        if (sensorPort && sensorPort.isOpen) {
            callback(null, true);
        } else {
            sensorPort = getSerialPort('newSensor');
            if (sensorPort && sensorPort.isOpen) {
                sensorPort.on('data', response);
                callback(null, true);
            } else {
                sensorPort = null;
                /**   
                 *
                 * @type {boolean}
                 */
                isProcessing = false;
                callback('not connected new sensor serial port', false);
            }
        }
    }
    function otherWorkProcess(callback) {
        isOtherWorkProcessing = true;
        if (otherWorkList.length) {
            const work = otherWorkList.splice(0, 1)[0];

            if (work.type === 'resetLiter' && work.address) {
                const startCode = Buffer.from(START_CODE);

                let dataLength = new Uint8Array(1);
                dataLength[0] = CONFIG.serialPort.sensorAddressLength + 3;

                let data = Buffer.concat([
                    startCode,
                    Buffer.from(dataLength),
                    Buffer.from(work.address),
                    Buffer.from(RESET_LITER_OP_CODE, 'hex')
                ]);

                const payload = Buffer.concat([data, crc(data)]);
                console.log('============= RESET LITER =============', payload);
                sensorPort.write(payload);
            }

            setTimeout(() => {
                isOtherWorkProcessing = false;
                callback(null, true);
            }, 1000);
        } else {
            isOtherWorkProcessing = false;
            callback(null, true);
        }
    }

    async function getSensors(callback) {  
        try {
            if (sensors.length) {                                          
                callback(null, true);       
            } else {
                let where = {};
                //where.position = 'in';
                // sensors = await sequelize.models.Sensor.findAll({
                //     order: [['order', 'DESC']],
                //     where
                // });         
                sensors = await sequelize.models.Sensor.getSensorAddress();                   
                if (sensors.length) {      
                    sensors.forEach(sensor => {
                        if (sensor.address) {   
                            if (addresses[sensor.address] === undefined) {
                                let buf_01 = ['0', '0', '0', '0', '0', '0', '0', '0'];
                                let buf_02 = ['0', '0', '0', '0', '0', '0', '0', '0'];
                                let buf_03 = ['0', '0', '0', '0', '0', '0', '0', '0'];
                                let buf_04 = ['0', '0', '0', '0', '0', '0', '0', '0'];
                                let data = isSensorCodeCheck(buf_01,buf_02,buf_03,buf_04,sensor);      
                                sensorSetData[sensor.address] = {
                                    buffer1 : data.buffer1,
                                    buffer2 : data.buffer2,
                                    buffer3 : data.buffer3,
                                    buffer4 : data.buffer4  
                                }
                                addresses[sensor.address] = {
                                    bufferCrc: true
                                };                       
                            }else{   
                                let data = isSensorCodeCheck(sensorSetData[sensor.address].buffer1,sensorSetData[sensor.address].buffer2,sensorSetData[sensor.address].buffer3,sensorSetData[sensor.address].buffer4,sensor); 
                                sensorSetData[sensor.address].buffer1=data.buffer1;
                                sensorSetData[sensor.address].buffer2=data.buffer2;
                                sensorSetData[sensor.address].buffer3=data.buffer3;   
                                sensorSetData[sensor.address].buffer4=data.buffer4;
                            }                                           
                        } 
                    });  
                    callback(null, true);
                } else {         
                    callback('empty sensors', false);
                }
            }    
        } catch (e) {
            callback(e, false);
        }
    }

    function setAddress(callback) {
        const addressList = Object.keys(addresses);
        if (addressList.length) {
            if (currentAddress) {
                for (let i=0; i<addressList.length; i++) {
                    if (currentAddress === addressList[i]) {
                        if (i === addressList.length - 1) {
                            currentAddress = addressList[0];
                        } else {
                            currentAddress = addressList[i + 1];
                        }
                        break;
                    }
                }
            } else {
                currentAddress = addressList[0];
            }
            callback(null, true);
        } else {
            callback('empty address', false);
        }
    }
    function setBoardAddress(callback) {
        const addressList = Object.keys(controlWorkList);
        if (addressList.length) {
            if (currentControlAddress) {
                for (let i=0; i<addressList.length; i++) {  
                    if (currentControlAddress === addressList[i]) {
                        if (i === addressList.length - 1) {
                            currentControlAddress = addressList[0];
                        } else {
                            currentControlAddress = addressList[i + 1];
                        }
                        break;
                    }
                }
            } else {
                currentControlAddress = addressList[0];
            }
            callback(null, true);
        } else {   
            callback('empty Boardaddress', false);
        }
    }  

    async function resetProcess(callback) {
        isResetProcessing = true;
        let isActive = true;
        const addressList = Object.keys(addresses);
        for (let i=0; i<addressList.length; i++) {
            const address = addresses[addressList[i]];
            if (address.bufferCrc) {
                isActive = false;
                break;
            }      
        }
        if (isActive) {
            // console.log("============= CRC ERROR =============");
            try {
                await sequelize.models.SensorResetHistory.create({});
            } catch (e) {}
            sensorPort.write(Buffer.from('4854423A405245535440', 'hex'));
            setTimeout(() => {
                isResetProcessing = false;
                callback(null, true);
            }, 1000);
        } else {
            isResetProcessing = false;
            callback(null, true);
            return true;
        }
    }

    function send(callback) {
        if (currentAddress) {
            requestBoard(currentAddress);
            callback(null, true);
        } else {
            callback('empty address', false);
        }
    }

    function sendControl(callback) {
        if (currentControlAddress) {  
            requestBoardControl(currentControlAddress);
            callback(null, true);    
        } else {  
            callback('empty address', false);
        }
    }

    function setConcatSendCode(callback) {
        if (sensorSetData) {
            concatSendCode(currentAddress);
            callback(null, true);          
        } else {
            callback('empty concatSendCode', false);
        }   
    }   
};
module.exports.pushOtherWork = work => {
    return new Promise((resolve, reject) => {
        try {
            if (sensorPort && sensorPort.isOpen) {
                otherWorkList.push(work);
                resolve(true);
            } else {
                reject(false);
            }
        } catch (e) {
            reject(e);
        }
    });
};

function requestBoard() {
    const startCode = Buffer.from(START_CODE);
    let dataLength = new Uint8Array(1);
    dataLength[0] = CONFIG.serialPort.sensorAddressLength + 2 + 5;
    
    const boardAddress = Buffer.from(currentAddress);
    //console.log('====================',currentAddress);
    const opCode = Buffer.from(OP_CODE, 'hex');

    const sensorCode = Buffer.from(sensorSetData[currentAddress].code, 'hex');
    
    let data = Buffer.concat([   
        startCode,  
        Buffer.from(dataLength),     
        boardAddress,      
        opCode,
        sensorCode
    ]);

    const payload = Buffer.concat([data, crc(data)]);
    console.log('sensor request ============> ['+ currentAddress +']', FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), payload);
    let sensor_log=FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss')+' [요청] ==> ['+ currentAddress +'] ' + payload.toString('hex');
    setSendSensorDataLog(sensor_log);
    
    sensorPort.write(payload);   
    instance = setTimeout(() => {
        addresses[currentAddress].bufferCrc = false;
        instance = null;    
        if (delayInstance) {
            clearTimeout(delayInstance);
            delayInstance = null;
        }
        delayInstance = setTimeout(() => {
            isProcessing = false;         
        }, requestDelay);
        checBuffDatakLog(currentAddress);
    }, CONFIG.serialPort.sensorTimeout); // 10 seconds timeout
}

function response(data) {
    if(isControlProcessing){
        responseControl(data);        
    }else{
        if (isResetProcessing) return true;
        if (isOtherWorkProcessing) return true;       
        if(data){  
            let hexData =data.toString("hex");
            console.log(hexData);
            let sensor_log=FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss')+' [응답] ==> ['+ currentAddress +'] ' + hexData;
            setSendSensorDataLog(sensor_log);       

            if (hexData.slice(0, 2) == '46'  && hexData.indexOf('5a') != -1 && ( (hexData.indexOf('5a')+4)==hexData.length ) ) {
                //console.log('all start buffer response');  
                buffData='';               
                buffData+=hexData;                                
                isBuffData=true;                       
            }else if(hexData.slice(0, 2) == '46' && hexData.indexOf('5a') == -1 && ( (hexData.indexOf('5a')+4)!=hexData.length )) {  
                //console.log('start buffer response '); 
                buffData='';                     
                buffData+=hexData;                            
                isBuffData=false;  
            }else if (hexData.indexOf('5a') != -1 ) { 
                buffData+=hexData;       
                isBuffData=true;      
                //console.log('end buffer response ');             
            }else{     
                buffData+=hexData; 
                //console.log('buffer response ++++++');      
            }      
    
            if(isBuffData && buffData.length>=28){    
                console.log('isBuffData===='+isBuffData);    
                console.log('buffData===='+buffData);                     
                if(buffData.slice(22, 24)=='70' && buffData.slice(24, 26)=='00'){
                    console.log('buffData check===='+true);        
                    addresses[currentAddress].bufferCrc = true;
                    parse(buffData);  
                }else{
                    let sensor_log=FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss')+' [ERROR] ==> ['+ currentAddress +'] 통신 실패  '+buffData.slice(24, 26);
                    setSendSensorDataLog(sensor_log);              
                    initBuffer();    
                }                          
            }
            buffDataCheck+=hexData;
            //console.log('buffDataCheck==='+buffDataCheck);
                           
        }    
    }     
}  

function parse(data) {
    let isUpdateTime = true;
    if (instance) {
        clearTimeout(instance);
        instance = null;        
    }
    let paresData = setSensorValueCheck(sensorSetData[currentAddress].order,data)
    paresData.forEach(function(data) { 
        setNewSensorValue(data.address, data.key, data.value);                           
    }); 
        
    initBuffer();
    if (delayInstance) {   
        clearTimeout(delayInstance);
        delayInstance = null;
    }
    delayInstance = setTimeout(() => {
        isProcessing = false;     
    }, requestDelay);
    checBuffDatakLog(currentAddress);
    if (isUpdateTime) {
        return updateSensorTime(currentAddress);
    }   
}  

function checBuffDatakLog (address){
    if(buffDataCheck && buffDataCheck.length>28 && buffDataCheck.indexOf('5a') == -1 ) {
        let sensor_log=FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss')+' [ERROR] ==> ['+ address +'] 통신 데이터 불일치  ';
        setSendSensorDataLog(sensor_log);         
    }  
    buffDataCheck=''; 
}  

function requestBoardControl() {
    const startCode = Buffer.from(START_CODE);
    let dataLength = new Uint8Array(1);
    dataLength[0] = CONFIG.serialPort.sensorAddressLength + 2 + 7;
       
    const boardAddress = Buffer.from(controlWorkListData[currentControlAddress].address);

    const opCode = Buffer.from("4F", 'hex');  
        
    //const address = Buffer.from(controlWorkListData[currentControlAddress].address, 'hex');
    const bitmask=Buffer.from(controlWorkListData[currentControlAddress].bitmask, 'hex');
    const mode=Buffer.from(controlWorkListData[currentControlAddress].mode, 'hex');
    const ontime=Buffer.from(controlWorkListData[currentControlAddress].ontime, 'hex');
    const offtime=Buffer.from(controlWorkListData[currentControlAddress].offtime, 'hex');
  
    let data = Buffer.concat([      
        startCode,      
        Buffer.from(dataLength),        
        boardAddress,         
        opCode,  
        bitmask,
        mode,
        ontime,
        offtime
    ]);       
  
    const payload = Buffer.concat([data, crc(data)]);
    console.log('control request ============> ['+ boardAddress +']', FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), payload);
    let sensor_log=FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss')+' [요청] ==> ['+ boardAddress +'] ' + payload.toString('hex');
    setSendSensorDataLog(sensor_log);
          
    initBuffer2();         
    sensorPort.write(payload);
    instance = setTimeout(() => {
        controlWorkList[currentControlAddress].bufferCrc = false;
        initBuffer2();  
        instance = null;         
        if (delayInstance) {   
            clearTimeout(delayInstance);
            delayInstance = null;
        }
        delayInstance = setTimeout(() => {
            isProcessing = false;
        }, requestDelay);
    }, 5000); // 10 seconds timeout
}

function responseControl(data) {
    console.log(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), data);
    let sensor_log=FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss')+' [응답] ==> ['+ currentAddress +'] ' + data.toString("hex");     
    setSendSensorDataLog(sensor_log); 

    if (buffer1.byteLength >= 0 ){         
        buffer1 = Buffer.concat([buffer1, data]);
        if (checkLast(buffer1)) {    
            parseControl();     
        }             
    } else {    
        // console.error(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd HH:mm:ss'), "receive sensor value error");
    }
         
}        

function parseControl() { 
    let isControlState = true;
    if (instance) { 
        clearTimeout(instance);
        instance = null;        
    }        
    console.log(buffer1.toString('hex'))
    if (buffer1.byteLength === 14) {    
        const crcResult = checkCrc(buffer1.slice(0, 13), buffer1.slice(13, 14)); 
        if (crcResult && buffer1.toString('hex').slice(12, 13)) {            
            isControlState = true;     
        } else {        
            isControlState = false;           
        }
    } else {     
        isControlState = false;
    }  
    initBuffer2();
    controlWorkListData[currentControlAddress].ischeck=isControlState;
    delete controlWorkList[currentControlAddress];
    
    if (delayInstance) {
        clearTimeout(delayInstance);
        delayInstance = null;
    }
    delayInstance = setTimeout(() => {
        isProcessing = false;
    }, requestDelay);
}   

async function updateSensorTime(address) {
    try {
        await sequelize.models.Sensor.update({updatedAt: new Date()}, {where: {address}});
        return true;
    } catch (e) {
        return true;
    }
}

function crc(data) {
    let temp = data.slice(0, 1);
    for (let i=1; i<data.byteLength; i++) {
        temp = xor(temp, data.slice(i, i + 1));
    }
    return temp;
}

function checkLast(buffer) {
    if (buffer.byteLength && buffer.byteLength > 3) {
        return (new Uint8Array(buffer.slice(2, 3))[0] + 2) === buffer.byteLength;
    } else {
        return false;
    }
}
  
function initBuffer() {
    buffData+='';        
    isBuffData=false;  
}
function initBuffer2() {
    buffer1 = Buffer.from('');
}
function checkCrc(data, crcCode) {
    // console.log('check crc', crc(data).toString(), crcCode.toString());
    return crc(data).toString() === crcCode.toString();
}
 
function concatSendCode(address){
    for( var key in sensorSetData ) {  
        let result=[];
        let order=[];
        let data = sensorSetData[key];    
        result= getHex(data.buffer1.join(''))+getHex(data.buffer2.join(''))+getHex(data.buffer3.join(''))+getHex(data.buffer4.join(''));
        order=order.concat(setSensorOrder('buffer1',data.buffer1,address));
        order=order.concat(setSensorOrder('buffer2',data.buffer2,address));
        order=order.concat(setSensorOrder('buffer3',data.buffer3,address));
        order=order.concat(setSensorOrder('buffer4',data.buffer4,address));   
        sensorSetData[key].code=result;    
        sensorSetData[key].order=order;    
    }     
    //console.log(sensorSetData);                                    
}            
  
function setSensorOrder(type,buffer,address){    
    let result =[];     

    if(type=='buffer1'){
        if(buffer[0]=='1'){     
            result.push({num : 2 ,key : 'H' ,address : address});
            result.push({num : 2 ,key : 'T' ,address : address});
        }
        if(buffer[1]=='1'){     
            result.push({num : 2 ,key : 'C' ,address : address});
        }  

        if(buffer[3]=='1'){
            result.push({num : 2 ,key : 'W' ,address : address}); 
        }
        if(buffer[4]=='1'){   
            result.push({num : 2 ,key : 'P' ,address : address}); 
        }
        if(buffer[6]=='1'){
            result.push({num : 2 ,key : 'S' ,address : address}); 
        }        
        if(buffer[5]=='1'){
            result.push({num : 2 ,key : 'D' ,address : address}); 
        }   
        if(buffer[2]=='1'){
            result.push({num : 2 ,key : 'c' ,address : address}); 
            result.push({num : 2 ,key : 't' ,address : address});
            result.push({num : 2 ,key : 'h' ,address : address});
        }            
        if(buffer[7]=='1'){  
            result.push({num : 1 ,key : 'R' ,address : address}); 
        }            
    }else if(type=='buffer2'){
        if(buffer[0]=='1'){      
            result.push({num : 2 ,key : 'E' ,address : address});   
            result.push({num : 2 ,key : 'm' ,address : address}); 
            result.push({num : 2 ,key : 'u' ,address : address});   
        }
        if(buffer[1]=='1'){
            result.push({num : 2 ,key : 'B' ,address : address}); 
        }
        if(buffer[2]=='1'){     
            result.push({num : 1 ,key : 'w' ,address : address});  
        }  
        if(buffer[3]=='1'){
            result.push({num : 2 ,key : 'f' ,address : address});   
        }   
        if(buffer[4]=='1'){  
            result.push({num : 2 ,key : 'U' ,address : address});   
            result.push({num : 2 ,key : 'r' ,address : address}); 
            result.push({num : 2 ,key : 'd' ,address : address}); 
        }
        if(buffer[5]=='1'){  
            result.push({num : 1 ,key : 'N' ,address : address}); 
        }
        if(buffer[6]=='1'){
            result.push({num : 1 ,key : 'F' ,address : address}); 
        }
        if(buffer[7]=='1'){  
            result.push({num : 1 ,key : 'b' ,address : address}); 
        }        
    }else if(type=='buffer3'){  
        if(buffer[0]=='1'){    
            result.push({num : 2 ,key : 'Y' ,address : address});   
        }
        if(buffer[1]=='1'){
            result.push({num : 2 ,key : 'L' ,address : address});  
        }
        if(buffer[2]=='1'){
            result.push({num : 1 ,key : 'M' ,address : address});      
        }
        if(buffer[3]=='1'){

        }  
        if(buffer[4]=='1'){
            result.push({num : 2 ,key : 'i' ,address : address});  
            result.push({num : 2 ,key : 'o' ,address : address});
            result.push({num : 2 ,key : 'p' ,address : address});   
        }  
        if(buffer[5]=='1'){
            result.push({num : 2 ,key : 'g' ,address : address});  
            result.push({num : 2 ,key : 'O' ,address : address});   
            result.push({num : 2 ,key : 'y' ,address : address});  
            result.push({num : 1 ,key : 's' ,address : address}); 
            result.push({num : 2 ,key : 'e' ,address : address});  
            result.push({num : 2 ,key : 'A' ,address : address});     
            result.push({num : 2 ,key : 'Q' ,address : address});                     
        }   
        if(buffer[6]=='1'){
            result.push({num : 2 ,key : 'l' ,address : address});                    
        }                
        if(buffer[7]=='1'){
            result.push({num : 2 ,key : 'a' ,address : address});                    
        }  
    }else if(type=='buffer4'){  
        if(buffer[0]=='1'){    
            result.push({num : 2 ,key : 'k' ,address : address});   
        }  
    }    
    return result;
}   

function isSensorCodeCheck(buffer1,buffer2,buffer3,buffer4,sensor){  
           
    let result={};         
    result.buffer1=buffer1;   
    result.buffer2=buffer2;  
    result.buffer3=buffer3;
    result.buffer4=buffer4;  

    if(sensor.type=="temperature" && sensor.type_id=="41") result.buffer1.splice(0, 1, '1');
    if(sensor.type=="humidity" && sensor.type_id=="42") result.buffer1.splice(0, 1, '1');
    if(sensor.type=="rain" && sensor.type_id=="43") result.buffer1.splice(7, 1, '1');  
    if(sensor.type=="windDirection" && sensor.type_id=="44") result.buffer1.splice(5, 1, '1');
    if(sensor.type=="windSpeed" && sensor.type_id=="45") result.buffer1.splice(6, 1, '1');
    if(sensor.type=="solar" && sensor.type_id=="46") result.buffer1.splice(3, 1, '1');
    
    if(sensor.type=="ph" && sensor.type_id=="48") result.buffer1.splice(4, 1, '1');
    if(sensor.type=="ec" && sensor.type_id=="49") result.buffer2.splice(0, 1, '1');  
    if(sensor.type=="soilTemperature" && sensor.type_id=="50") result.buffer2.splice(0, 1, '1');
    if(sensor.type=="soilHumidity" && sensor.type_id=="51") result.buffer2.splice(0, 1, '1');
    if(sensor.type=="solar" && sensor.type_id=="52") result.buffer1.splice(3, 1, '1');
    if(sensor.type=="battery" && sensor.type_id=="53") result.buffer2.splice(1, 1, '1');
    if(sensor.type=="water" ) result.buffer2.splice(2, 1, '1');
    if(sensor.type=="liter" && sensor.type_id=="57") result.buffer2.splice(3, 1, '1');  
    if(sensor.type=="frost" ) result.buffer2.splice(4, 1, '1');
    if(sensor.type=="frostHumidity" ) result.buffer2.splice(4, 1, '1');
    if(sensor.type=="frostTemperature" ) result.buffer2.splice(4, 1, '1');
    if(sensor.type=="window" ) result.buffer2.splice(5, 1, '1');  
    if(sensor.type=="fire" ) result.buffer2.splice(6, 1, '1');  
    if(sensor.type=="power" ) result.buffer2.splice(7, 1, '1');
    if(sensor.type=="dryTemperature" && sensor.type_id=="73") result.buffer3.splice(0, 1, '1');
    if(sensor.type=="wetTemperature" && sensor.type_id=="74") result.buffer3.splice(1, 1, '1');
    if(sensor.type=="door" && sensor.type_id=="75") result.buffer3.splice(2, 1, '1');
    //if(sensor.type=="temperature" && sensor.type_id=="76") result.buffer3.splice(3, 1, '1');
    //if(sensor.type=="humidity" && sensor.type_id=="77") result.buffer3.splice(3, 1, '1');
    
    if(sensor.type=="co2" && sensor.type_id=="47") result.buffer1.splice(1, 1, '1');
    if(sensor.type=="co2" && sensor.type_id=="78") result.buffer1.splice(2, 1, '1');
    if(sensor.type=="co2_t" && sensor.type_id=="79") result.buffer1.splice(2, 1, '1');    
    if(sensor.type=="co2_h" && sensor.type_id=="80") result.buffer1.splice(2, 1, '1');
         
    if(sensor.type=="co2" && sensor.type_id=="81") result.buffer3.splice(4, 1, '1');    
    if(sensor.type=="co2_t" && sensor.type_id=="82") result.buffer3.splice(4, 1, '1');
    if(sensor.type=="co2_h" && sensor.type_id=="83") result.buffer3.splice(4, 1, '1');   
    if(sensor.type=="korinsHumidity" && sensor.type_id=="76") result.buffer3.splice(5, 1, '1');
    if(sensor.type=="korinsTemperature" && sensor.type_id=="77") result.buffer3.splice(5, 1, '1');
    if(sensor.type=="korinsPressure" && sensor.type_id=="85") result.buffer3.splice(5, 1, '1');
    if(sensor.type=="korinsFall" && sensor.type_id=="86") result.buffer3.splice(5, 1, '1');
    if(sensor.type=="korinsWindSpeed" && sensor.type_id=="87") result.buffer3.splice(5, 1, '1');
    if(sensor.type=="korinsWindDirection" && sensor.type_id=="88") result.buffer3.splice(5, 1, '1');
    if(sensor.type=="korinsSolar" && sensor.type_id=="89") result.buffer3.splice(5, 1, '1');
    if(sensor.type=="FolateTemperature" && sensor.type_id=="93") result.buffer3.splice(6, 1, '1');
    if(sensor.type=="CultureMediumWeight" && sensor.type_id=="94") result.buffer3.splice(7, 1, '1');
    
    if(sensor.type=="CultureMediumWaterWeight" && sensor.type_id=="95") result.buffer4.splice(0, 1, '1');
    
    return result;                            
} 

function getHex(data)
{   
    let str = parseInt(data, 2).toString(16);  
    if(str.length==1){
       str='0'+str;   
    }
    return str;  
}   

function setSensorValueCheck(arrKey,str){
    let result=[];  
    let startNum=26;      
    let endNum=26;               
    arrKey.forEach(function(data) { 
        endNum = startNum+(Number(data.num)*2)+2;    
        result=result.concat(getHexStr(str.slice(startNum,endNum),data.address,data.key));  
        startNum = startNum+(Number(data.num)*2)+2;                           
    });                                                 
    console.log(result);      
    return result;              
}                   
    
function getHexStr(data,address,rKey){ 
    let  result=[];
    let  key = hex_to_ascii(data.slice(0,2));
    let  value = '';  
    if(key==rKey){   
        if(key=='w' || key=='N' || key=='F' || key=='b'){
            let bitData = hexTobin(data.slice(2,data.length),1);
            if(key=='w'){       
                result.push({key : key+'1' ,value : bitData.slice(5,6) ,address : address});  
                result.push({key : key+'2' ,value : bitData.slice(6,7) ,address : address});
                result.push({key : key+'3' ,value : bitData.slice(7,8) ,address : address});  
            }else{
                result.push({key : key+'1' ,value : bitData.slice(4,5) ,address : address});       
                result.push({key : key+'2' ,value : bitData.slice(5,6) ,address : address});  
                result.push({key : key+'3' ,value : bitData.slice(6,7) ,address : address});
                result.push({key : key+'4' ,value : bitData.slice(7,8) ,address : address});  
            } 
                                 
        }else{    
            if( key=='t' || key=='r' || key=='d' || key=='Y' || key=='L'  || key=='y'  || key=='O'  || key=='l'){             
                value = getHexTodec2(data.slice(2,data.length));
                result.push({key : key ,value : value ,address : address});
            }else{           
                value = hexTodec(data.slice(2,data.length));
                result.push({key : key ,value : value ,address : address});
            }           
        }   
    }else{
        console.log('key parse  error ' , key+'=='+rKey)
    }       
                    
    return result;                              
}         
       
function hexTobin(s,n){
    let result = '';
    let val = parseInt(s, 16).toString(2);
    if(val.length < 8*n){
        for(let i=0; i<((8*n)-val.length);i++){
            result+='0';    
        }
        result+=val;
    }
    return result;
} 

function hexTodec(s){
    return parseInt(s, 16).toString(10);
}  

function hex_to_ascii(str1)  
 {
	var hex  = str1.toString();
	var str = '';
	for (var n = 0; n < hex.length; n += 2) {
		str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
	}
	return str;    
 }

 function getHexTodec2(hex) {
    let result;        
    hex = hex.replace("0x","");  
    hex = hex.replace("0X","");
    try {
        var x = new BigNumber(hex, 16);			   
    }  
    catch(err) {    
        console.log(err)
        return;
    }
    //let xx=x.toString(10);
    //console.log('Decimal number===' + xx)   
    //console.log('Binary number:===' + x.toString(2))
    if( typeof(x) && x.gte(0) ) {
        if( hex.length==2 && x.gte("80", 16) ) { x=x.minus("100",16); }
        if( hex.length==4 && x.gte("8000", 16) ) { x=x.minus("10000",16); }
        if( hex.length==8 && x.gte("80000000", 16) ) { x=x.minus("100000000",16); }
        let t1 = new BigNumber("8000000000000000",16);
        let t2 = new BigNumber("10000000000000000",16);
        if( hex.length==16 && x.gte(t1) ) { x=x.minus(t2); }
        if( hex.length==2 || hex.length==4 || hex.length==8 || hex.length==16 ){			
            //console.log('Decimal from signed 2s complement:===' + x.toString(10))
            result=x.toString(10);
        }else{   
            console.log('=====',"1N/A")
        }	        
    }else{          
        console.log('=====',"2N/A")
    }
         
//		hex=hex.toUpperCase();
//		let txt=hex+" = ";   
//		let d,e,minus=false;
//		let len=hex.length;
//		if( hex[0]=="-" ) { txt+="-["; hex=hex.substr(1); len--; minus=true; }
//		let idot=hex.indexOf(".");
//		if( idot>=0 ) { hex=hex.substring(0,idot)+hex.substring(idot+1,len); len--; }
//		else idot=len;
//		let etbl = ["\u2070","\u00B9","\u00B2","\u00B3","\u2074","\u2075","\u2076","\u2077","\u2078","\u2079"];
//		for(var i=0; i<len; i++) {
//			d = hex.charCodeAt(i);
//			if( d<58 ) d-=48;
//			else if( d>64 ) d-=55;
//			//e = len-i-1;
//			e = idot-i-1;  
//			e=e.toString();
//			txt+="("+d+" \u00D7 16";
//			for(var k=0; k<e.length; k++)
//				if( e[k]=="-" )
//					txt+="\u207B";
//				else
//					txt+=etbl[e[k]];
//			txt+=")";
//			if( i<len-1 ) txt+=" + ";
//		}
//		if( minus ) txt+="]";
//		txt+=" = "+xx;
    //console.log('txt=====',txt)
    return result;
} 