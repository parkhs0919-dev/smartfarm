const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const Op = Sequelize.Op;
const FILTER_UTIL = require('../utils/filter');
const CONTROL_METHOD = require('../methods/control');
const CONFIG = require('../config');
const STD = require('../metadata/standards');
const getIo = require('../methods/socket-io').getIo;
const P_BAND = require('../methods/p-band');

let isAutoControlling = {};
let currentTimeHash = {};
let sunDate = null;
let house = {};

function setTime(house_id, period) {
    if (currentTimeHash[house_id]) {
        clearInterval(currentTimeHash[house_id].instance);
        currentTimeHash[house_id].time = period;
        currentTimeHash[house_id].instance = setInterval(() => {
            if (currentTimeHash[house_id].time > 0) currentTimeHash[house_id].time--;
        }, 1000);
    } else {
        currentTimeHash[house_id] = {time: period};
        currentTimeHash[house_id].instance = setInterval(() => {
            if (currentTimeHash[house_id].time > 0) currentTimeHash[house_id].time--;
        }, 1000);
    }
}

module.exports = (house_id, period, finish) => {
    if (isAutoControlling[house_id]) {
        return finish(400, 'auto-controlling');
    } else {
        isAutoControlling[house_id] = true;
    }

    let funcs = [],
        windDirectionControlGroups = {
            '1': {
                forward: [],
                backward: []
            },
            '2': {
                forward: [],
                backward: []
            },
            '3': {
                forward: [],
                backward: []
            },
        },
        autoControls = [],
        targets = [],
        ignoreTargets = [],
        motors = null,
        powers = null,
        currentControls = {},
        orderAutoControls = {},
        now = new Date(),
        io = getIo();

    let today = now.getFullYear() + '-' + FILTER_UTIL.attachZero(now.getMonth() + 1) + '-' + FILTER_UTIL.attachZero(now.getDate()),
        currentTime = '' + FILTER_UTIL.attachZero(now.getHours()) + FILTER_UTIL.attachZero(now.getMinutes()),
        currentTimestamp = now.getTime(),
        period_type = now.getSeconds() < 10 ? 'oneMinute' : 'immediately';

    funcs.push(callback => {
        clearTask(callback);
    });

    funcs.push(callback => {
        emit(callback);
    });

    funcs.push(callback => {
        getHouse(callback);
    });

    funcs.push(callback => {
        getWindDirectionControlGroups(callback);
    });

    funcs.push(callback => {
        getSunDate(callback);
    });

    funcs.push(callback => {
        getAutoControls(callback);
    });

    funcs.push(callback => {
        getMotorPowers(callback);
    });

    funcs.push(callback => {
        getTargetAutoControls(callback);
    });

    funcs.push(callback => {
        activeAutoControls(callback);
    });

    async.series(funcs, (error, results) => {
        delete isAutoControlling[house_id];
        if (error) {
            if (error === 'empty') {
                finish(204);
            } else {
                finish(400, error);
            }
        } else {
            finish(204);
        }
    });

    async function clearTask(callback) {
        try {
            await sequelize.models.AutoControlLog.destroy({
                where: {house_id}
            });
            callback(null, true);
        } catch (e) {
            console.error('auto-control clearTask', new Date(), e);
            callback(e, false);
        }
    }

    async function emit(callback) {
        try {
            await sequelize.models.AutoControlLog.createLog(house_id, 'emit');
            if (period_type === 'oneMinute') {
                setTime(house_id, 60);
                if (io) io.emit('auto-control-trigger', {house_id});
            }
            callback(null, true);
        } catch (e) {
            console.error('auto-control emit', new Date(), e);
            callback(e, false);
        }
    }

    async function getHouse(callback) {
        try {
            await sequelize.models.AutoControlLog.createLog(house_id, 'getHouse');
            house[house_id] = await sequelize.models.House.findByPk(house_id, {
                include: [{
                    model: sequelize.models.HouseWindDirectionSensor,
                    as: 'houseWindDirectionSensor',
                    include: [{
                        model: sequelize.models.Sensor,
                        as: 'sensor'
                    }]
                }]
            });
            if (house[house_id]) {
                callback(null, true);
            } else {
                callback('empty-house', false);
            }
            return true;
        } catch (e) {
            console.error('auto-control getHouse', new Date(), e);
            callback(e, false);
            return true;
        }
    }

    async function getWindDirectionControlGroups(callback) {
        try {
            await sequelize.models.AutoControlLog.createLog(house_id, 'getWindDirectionControlGroups');
            windDirectionControlGroups = await sequelize.models.ControlGroup.getWindDirectionControlGroups(house[house_id]);
            callback(null, true);
            return true;
        } catch (e) {
            console.error('auto-control getWindDirectionControlGroups', new Date(), e);
            callback(e, false);
            return true;
        }
    }

    async function getSunDate(callback) {
        try {
            await sequelize.models.AutoControlLog.createLog(house_id, 'getSunDate');
            sunDate = await sequelize.models.SunDate.findOne({
                where: {
                    date: FILTER_UTIL.date(now, 'yyyy-MM-dd')
                }
            });
            if (!sunDate) {
                sunDate = await sequelize.models.SunDateDefault.findOne({});
            }
            callback(null, true);
            return true;
        } catch (e) {
            console.error('auto-control getSunDate', new Date(), e);
            callback(e, false);
            return true;
        }
    }

    async function getAutoControls(callback) {
        try {
            await sequelize.models.AutoControlLog.createLog(house_id, 'getAutoControls');
            let dayWhere = {
                date_type: 'day',
            };
            switch (now.getDay()) {
                case 0:
                    dayWhere.is_sun = true;
                    break;
                case 1:
                    dayWhere.is_mon = true;
                    break;
                case 2:
                    dayWhere.is_tue = true;
                    break;
                case 3:
                    dayWhere.is_wed = true;
                    break;
                case 4:
                    dayWhere.is_thur = true;
                    break;
                case 5:
                    dayWhere.is_fri = true;
                    break;
                case 6:
                    dayWhere.is_sat = true;
                    break;
            }
            let where = {
                house_id,
                state: 'on',
                [Op.and]: [{
                    [Op.or]: [{
                        start_date: null
                    }, {
                        start_date: {
                            [Op.lte]: today
                        }
                    }]
                }, {
                    [Op.or]: [{
                        end_date: null
                    }, {
                        end_date: {
                            [Op.gte]: today
                        }
                    }]
                }, {
                    [Op.or]: [{
                        ...dayWhere
                    }, {
                        date_type: 'per'
                    }]
                }]
            };
            let autoControlItemWhere = {};
            if (period_type === 'immediately') {
                autoControlItemWhere.period_type = period_type;
            }
            autoControls = await sequelize.models.AutoControl.findAll({
                include: [{
                    model: sequelize.models.AutoControlItem,
                    as: 'autoControlItems',
                    where: autoControlItemWhere,
                    separate: true,
                    order: [['id', 'ASC']],
                    include: [{
                        model: sequelize.models.Control,
                        as: 'control'
                    }, {
                        model: sequelize.models.AutoControlSensor,
                        as: 'autoControlSensors',
                        separate: true,
                        order: [['order', 'ASC']],
                        include: [{
                            model: sequelize.models.Sensor,
                            as: 'sensor'
                        }, {
                            model: sequelize.models.AutoControlSensorOption,
                            as: 'autoControlSensorOptions',
                            on: {
                                order: Sequelize.where(Sequelize.col('AutoControlSensor.order'), '=', Sequelize.col('autoControlSensorOptions.order'))
                            },
                            include : [{
                                model : sequelize.models.Sensor,
                                as :'sensor'
                            }]
                        }]
                    }, {
                        model: sequelize.models.AutoControlControl,
                        as: 'autoControlControls',
                        separate: true,
                        order: [['order', 'ASC']],
                        include: [{
                            model: sequelize.models.Control,
                            as: 'control'
                        }]
                    }, {
                        model: sequelize.models.AutoControlItemMinMax,
                        as: 'autoControlItemMinMaxes',
                        separate: true,
                        order: [['id', 'ASC']],
                        include: [{
                            model: sequelize.models.ControlMinMaxRange,
                            as: 'controlMinMaxRange',
                            include: [{
                                model: sequelize.models.Sensor,
                                as: 'sensor'
                            }]
                        }]
                    }, {
                        model: sequelize.models.PBand,
                        as: 'pBand',
                        include: [{
                            model: sequelize.models.Sensor,
                            as: 'inTemperatureSensor'
                        }, {
                            model: sequelize.models.Sensor,
                            as: 'outTemperatureSensor'
                        }, {
                            model: sequelize.models.Sensor,
                            as: 'windSpeedSensor'
                        }]
                    }, {
                        model: sequelize.models.Sensor,
                        as: 'expectTemperatureSensor'
                    }, {
                        model: sequelize.models.AutoControlExpectTemperatureSensor,
                        as: 'autoControlExpectTemperatureSensors',
                        include: [{
                            model: sequelize.models.Sensor,
                            as: 'sensor'
                        }]
                    }]
                }, {
                    model: sequelize.models.AutoControlStep,
                    as: 'autoControlSteps',
                    separate: true,
                    order: [['id', 'ASC']],
                    include: [{
                        model: sequelize.models.Control,
                        as: 'control'
                    }, {
                        model: sequelize.models.AutoControlStepMinMax,
                        as: 'autoControlStepMinMaxes',
                        separate: true,
                        order: [['id', 'ASC']],
                        include: [{
                            model: sequelize.models.ControlMinMaxRange,
                            as: 'controlMinMaxRange',
                            include: [{
                                model: sequelize.models.Sensor,
                                as: 'sensor'
                            }]
                        }]
                    }, {
                        model: sequelize.models.PBand,
                        as: 'pBand',
                        include: [{
                            model: sequelize.models.Sensor,
                            as: 'inTemperatureSensor'
                        }, {
                            model: sequelize.models.Sensor,
                            as: 'outTemperatureSensor'
                        }, {
                            model: sequelize.models.Sensor,
                            as: 'windSpeedSensor'
                        }]
                    }, {
                        model: sequelize.models.Sensor,
                        as: 'expectTemperatureSensor'
                    }]
                }],
                order: [['order', 'ASC']],
                where
            });
            if (autoControls.length) {
                const todayZeroTimestamp = new Date(`${today} 00:00:00`).getTime();
                for (let i=0; i<autoControls.length; i++) {
                    const autoControl = autoControls[i];
                    let isSplice = false;
                    if (autoControl.date_type === 'per') {
                        const startDateTimestamp = new Date(`${autoControl.start_date} 00:00:00`).getTime();
                        if ((todayZeroTimestamp - startDateTimestamp) % (autoControl.per_date * 86400000)) {
                            autoControls.splice(i, 1);
                            i--;
                            isSplice = true;
                        }
                    }
                    if (!isSplice && !autoControl.autoControlItems.length) {
                        autoControls.splice(i, 1);
                        i--;
                        isSplice = true;
                    }
                }
                if (autoControls.length) {
                    callback(null, true);
                } else {
                    callback('empty', false);
                }
            } else {
                callback('empty', false);
            }
            return true;
        } catch (e) {
            console.error('auto-control getAutoControls', new Date(), e);
            callback(e, false);
            return true;
        }
    }

    async function getMotorPowers(callback) {
        try {
            await sequelize.models.AutoControlLog.createLog(house_id, 'getMotorPowers');
            motors = CONTROL_METHOD.getMotors();
            powers = CONTROL_METHOD.getPowers();
            if (motors && powers) {
                callback(null, true);
            } else {
                callback('error', false);
            }
            return true;
        } catch (e) {
            console.error('auto-control getMotorPowers', new Date(), e);
            callback(e, false);
            return true;
        }
    }

    async function getTargetAutoControls(callback) {
        try {
            await sequelize.models.AutoControlLog.createLog(house_id, 'getTargetAutoControls');
            for (let i=0; i<autoControls.length; i++) {
                const autoControl = autoControls[i];
                if (await check(autoControl, currentTime)) {
                    if (autoControl.autoControlSteps.length) {
                        let controls = {};
                        let isTarget = true;
                        const control = autoControl.autoControlItems[0].control;
                        if (control) {
                            if (currentControls[control.id] === undefined) {
                                controls[control.id] = autoControl;
                            } else {
                                orderAutoControls[autoControl.id] = currentControls[control.id];
                                isTarget = false;
                                continue;
                            }
                        } else {
                            if(windDirectionControlGroups[autoControl.autoControlItems[0].window_position][autoControl.autoControlItems[0].wind_direction_type].length){
                                for (let k=0; k<windDirectionControlGroups[autoControl.autoControlItems[0].window_position][autoControl.autoControlItems[0].wind_direction_type].length; k++) {
                                    const control = windDirectionControlGroups[autoControl.autoControlItems[0].window_position][autoControl.autoControlItems[0].wind_direction_type][k];
                                    if (currentControls[control.id] === undefined) {
                                        controls[control.id] = autoControl;
                                    } else {
                                        orderAutoControls[autoControl.id] = currentControls[control.id];
                                        isTarget = false;
                                        break;
                                    }
                                }
                                if (!isTarget) {
                                    continue;
                                }
                            }else{
                                isTarget = false;
                                break;
                            }

                        }
                        for (let j=0; j<autoControl.autoControlSteps.length; j++) {
                            const control = autoControl.autoControlSteps[j].control;
                            if (control) {
                                if (currentControls[control.id] === undefined) {
                                    controls[control.id] = autoControl;
                                } else {
                                    orderAutoControls[autoControl.id] = currentControls[control.id];
                                    isTarget = false;
                                    break;
                                }
                            } else {
                                if(windDirectionControlGroups[autoControl.autoControlSteps[j].window_position][autoControl.autoControlSteps[j].wind_direction_type].length){
                                    for (let k=0; k<windDirectionControlGroups[autoControl.autoControlSteps[j].window_position][autoControl.autoControlSteps[j].wind_direction_type].length; k++) {
                                        const control = windDirectionControlGroups[autoControl.autoControlSteps[j].window_position][autoControl.autoControlSteps[j].wind_direction_type][k];
                                        if (currentControls[control.id] === undefined) {
                                            controls[control.id] = autoControl;
                                        } else {
                                            orderAutoControls[autoControl.id] = currentControls[control.id];
                                            isTarget = false;
                                            break;
                                        }
                                    }
                                    if (!isTarget) {
                                        break;
                                    }
                                }else{
                                    isTarget = false;
                                    break;
                                }

                            }
                        }
                        if (isTarget) {
                            targets.push(autoControl);
                            currentControls = {
                                ...currentControls,
                                ...controls
                            };
                        } else {
                            ignoreTargets.push(autoControl);
                        }
                    } else {
                        let isInclude = false;
                        for (let j=0; j<autoControl.autoControlItems.length; j++) {
                            const autoControlItem = autoControl.autoControlItems[j];
                            if (await checkItem(autoControlItem, currentTime)) {
                                isInclude = true;
                                break;
                            }
                        }
                        if (isInclude) {
                            let controls = {};
                            let isTarget = true;
                            for (let j=0; j<autoControl.autoControlItems.length; j++) {
                                const control = autoControl.autoControlItems[j].control;
                                if (control) {
                                    if (currentControls[control.id] === undefined) {
                                        controls[control.id] = autoControl;
                                    } else {
                                        orderAutoControls[autoControl.id] = currentControls[control.id];
                                        isTarget = false;
                                        break;
                                    }
                                } else {
                                    if(windDirectionControlGroups[autoControl.autoControlItems[j].window_position][autoControl.autoControlItems[j].wind_direction_type].length){
                                        for (let k=0; k<windDirectionControlGroups[autoControl.autoControlItems[j].window_position][autoControl.autoControlItems[j].wind_direction_type].length; k++) {
                                            const control = windDirectionControlGroups[autoControl.autoControlItems[j].window_position][autoControl.autoControlItems[j].wind_direction_type][k];
                                            if (currentControls[control.id] === undefined) {
                                                controls[control.id] = autoControl;
                                            } else {
                                                orderAutoControls[autoControl.id] = currentControls[control.id];
                                                isTarget = false;
                                                break;
                                            }
                                        }
                                        if (!isTarget) {
                                            break;
                                        }
                                    }else{
                                        isTarget = false;
                                        break;
                                    }
                                }
                            }
                            if (isTarget) {
                                targets.push(autoControl);
                                currentControls = {
                                    ...currentControls,
                                    ...controls
                                };
                            } else {
                                ignoreTargets.push(autoControl);
                            }
                        }
                    }
                }
            }
            callback(null, true);
        } catch (e) {
            console.error('auto-control getTargetAutoControls', new Date(), e);
            callback(e, false);
        }
    }

    async function activeAutoControls(callback) {
        try {
            await sequelize.models.AutoControlLog.createLog(house_id, 'activeAutoControls');
            await processTargets(targets, false);
            await processTargets(ignoreTargets, true);
            if (io) io.emit('report', {
                house_id
            });
            callback(null, true);
        } catch (e) {
            console.error('auto-control activeAutoControls', new Date(), e);
            callback(e, false);
        }
    }

    async function processTargets (autoControls, isIgnore = false) {
        for (let i=0; i<autoControls.length; i++) {
            const autoControl = autoControls[i];
            if (await check(autoControl, currentTime)) {
                if (autoControl.autoControlSteps.length) {
                    let autoControlItem = autoControl.autoControlItems[0];
                    if (autoControl.current_step === null) { // 1 step first time
                        const autoControlStep = autoControl.autoControlSteps[autoControl.autoControlSteps.length - 1];
                        const active_time = autoControlStep.active_time;
                        if (autoControlItem.active_count && autoControlItem.active_count <= autoControlItem.current_active_count) {
                            if (active_time) {
                                const active_date = FILTER_UTIL.date(active_time, 'yyyy-MM-dd');
                                if (today === active_date) {
                                    continue;
                                } else {
                                    await sequelize.models.AutoControlItem.update({current_active_count: 0}, {where: {id: autoControlItem.id}});
                                }
                            } else {
                                await sequelize.models.AutoControlItem.update({current_active_count: 0}, {where: {id: autoControlItem.id}});
                            }
                        }
                        autoControlItem = await autoControlItem.reload();
                        if (isIgnore) {
                            if (orderAutoControls[autoControl.id]) {
                                if (autoControlItem.wind_direction_type === 'none' && autoControlItem.window_position === 'none') {
                                    await sequelize.models.Report.createIgnoreAutoControlReport(autoControlItem.control, autoControl, orderAutoControls[autoControl.id]);
                                } else if (autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none') {
                                    for (let j=0; j<windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type].length; j++) {
                                        const control = windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type][j];
                                        await sequelize.models.Report.createIgnoreAutoControlReport(control, autoControl, orderAutoControls[autoControl.id]);
                                    }
                                }
                            }
                        } else {
                            if (autoControlItem.wind_direction_type === 'none' && autoControlItem.window_position === 'none') {
                                const {
                                    activeResult,
                                    finalTime,
                                    targetMinMax,
                                    pBandValue,
                                    pBandPercentage,
                                    isAlreadyPBandPercentage,
                                    isSatisfiedPBandTemperature,
                                    isPBandIntegral,
                                    motorLogState,
                                    powerLogState,
                                } = await active(autoControl, autoControlItem.control, autoControlItem, motors, powers, autoControl.order, 1, autoControlItem.is_min_max_range ? autoControlItem.autoControlItemMinMaxes : [], autoControlItem.wind_direction_type);
                                if (activeResult) {
                                    await sequelize.models.AutoControl.update({current_step: 0}, {where: {id: autoControl.id}});
                                    await sequelize.models.AutoControlItem.update({active_time: currentTimestamp, current_count: 1, current_active_count: autoControlItem.current_active_count + 1}, {where: {id: autoControlItem.id}});
                                    await sequelize.models.Report.createAutoControlReport(autoControl, autoControlItem.control, autoControlItem, 1, finalTime, targetMinMax, {
                                        pBandValue,
                                        pBandPercentage,
                                        isAlreadyPBandPercentage,
                                        isSatisfiedPBandTemperature,
                                        isPBandIntegral,
                                    }, {
                                        motorLogState,
                                        powerLogState
                                    });
                                }
                            } else if (autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none') {
                                await sequelize.models.Report.createWindDirectionControlReport(house_id, autoControlItem.wind_direction_type, autoControlItem.window_position, autoControlItem, 1);
                                for (let j=0; j<windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type].length; j++) {
                                    const control = windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type][j];
                                    const {
                                        activeResult,
                                        finalTime,
                                        targetMinMax,
                                        pBandValue,
                                        pBandPercentage,
                                        isAlreadyPBandPercentage,
                                        isSatisfiedPBandTemperature,
                                        isPBandIntegral,
                                        motorLogState,
                                        powerLogState,
                                    } = await active(autoControl, control, autoControlItem, motors, powers, autoControl.order, 1, autoControlItem.is_min_max_range ? autoControlItem.autoControlItemMinMaxes : [], autoControlItem.wind_direction_type);
                                    if (activeResult) {
                                        await sequelize.models.Report.createAutoControlReport(autoControl, control, autoControlItem, 1, finalTime, targetMinMax, {
                                            pBandValue,
                                            pBandPercentage,
                                            isAlreadyPBandPercentage,
                                            isSatisfiedPBandTemperature,
                                            isPBandIntegral,
                                        }, {
                                            motorLogState,
                                            powerLogState
                                        });
                                    }
                                }
                                await sequelize.models.AutoControl.update({current_step: 0}, {where: {id: autoControl.id}});
                                await sequelize.models.AutoControlItem.update({active_time: currentTimestamp, current_count: 1, current_active_count: autoControlItem.current_active_count + 1}, {where: {id: autoControlItem.id}});
                            }
                        }
                    } else if (autoControl.current_step === 0) { // 1 step
                        const autoControlStep = autoControl.autoControlSteps[0];
                        const active_time = autoControlItem.active_time;
                        const time = generateTimeSecond(autoControlItem);
                        if (autoControlItem.recount && autoControlItem.recount >= autoControlItem.current_count) { // repeat
                            if (autoControlItem.delay && active_time && active_time + time + autoControlItem.delay * 1000 > currentTimestamp) {
                                continue; // skip by delay
                            } else {
                                if (isIgnore) {
                                    if (orderAutoControls[autoControl.id]) {
                                        if (autoControlItem.wind_direction_type === 'none' && autoControlItem.window_position === 'none') {
                                            await sequelize.models.Report.createIgnoreAutoControlReport(autoControlItem.control, autoControl, orderAutoControls[autoControl.id]);
                                        } else if (autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none') {
                                            for (let j=0; j<windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type].length; j++) {
                                                const control = windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type][j];
                                                await sequelize.models.Report.createIgnoreAutoControlReport(control, autoControl, orderAutoControls[autoControl.id]);
                                            }
                                        }
                                    }
                                } else {
                                    if (autoControlItem.wind_direction_type === 'none' && autoControlItem.window_position === 'none') {
                                        const {
                                            activeResult,
                                            finalTime,
                                            targetMinMax,
                                            pBandValue,
                                            pBandPercentage,
                                            isAlreadyPBandPercentage,
                                            isSatisfiedPBandTemperature,
                                            isPBandIntegral,
                                            motorLogState,
                                            powerLogState,
                                        } = await active(autoControl, autoControlItem.control, autoControlItem, motors, powers, autoControl.order, 1, autoControlItem.is_min_max_range ? autoControlItem.autoControlItemMinMaxes : [], autoControlItem.wind_direction_type);
                                        if (activeResult) {
                                            await sequelize.models.AutoControl.update({current_step: 0}, {where: {id: autoControl.id}});
                                            await sequelize.models.AutoControlItem.update({active_time: currentTimestamp, current_count: autoControlItem.current_count + 1}, {where: {id: autoControlItem.id}});
                                            await sequelize.models.Report.createAutoControlReport(autoControl, autoControlItem.control, autoControlItem, 1, finalTime, targetMinMax, {
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                            }, {
                                                motorLogState,
                                                powerLogState
                                            });
                                        }
                                    } else if (autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none') {
                                        await sequelize.models.Report.createWindDirectionControlReport(house_id, autoControlItem.wind_direction_type, autoControlItem.window_position, autoControlItem, 1);
                                        for (let j=0; j<windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type].length; j++) {
                                            const control = windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type][j];
                                            const {
                                                activeResult,
                                                finalTime,
                                                targetMinMax,
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                                motorLogState,
                                                powerLogState,
                                            } = await active(autoControl, control, autoControlItem, motors, powers, autoControl.order, 1, autoControlItem.is_min_max_range ? autoControlItem.autoControlItemMinMaxes : [], autoControlItem.wind_direction_type);
                                            if (activeResult) {
                                                await sequelize.models.Report.createAutoControlReport(autoControl, control, autoControlItem, 1, finalTime, targetMinMax, {
                                                    pBandValue,
                                                    pBandPercentage,
                                                    isAlreadyPBandPercentage,
                                                    isSatisfiedPBandTemperature,
                                                    isPBandIntegral,
                                                }, {
                                                    motorLogState,
                                                    powerLogState
                                                });
                                            }
                                        }
                                        await sequelize.models.AutoControl.update({current_step: 0}, {where: {id: autoControl.id}});
                                        await sequelize.models.AutoControlItem.update({active_time: currentTimestamp, current_count: autoControlItem.current_count + 1}, {where: {id: autoControlItem.id}});
                                    }

                                }
                            }
                        } else { // next step
                            if (autoControl.step_delay && active_time && active_time + time + autoControl.step_delay * 1000 > currentTimestamp) {
                                continue; // skip by delay
                            } else {
                                if (isIgnore) {
                                    if (orderAutoControls[autoControl.id]) {
                                        if (autoControlStep.wind_direction_type === 'none' && autoControlStep.window_position === 'none') {
                                            await sequelize.models.Report.createIgnoreAutoControlReport(autoControlStep.control, autoControl, orderAutoControls[autoControl.id]);
                                        } else if (autoControlStep.wind_direction_type !== 'none' && autoControlStep.window_position !== 'none') {
                                            for (let j=0; j<windDirectionControlGroups[autoControlStep.window_position][autoControlStep.wind_direction_type].length; j++) {
                                                const control = windDirectionControlGroups[autoControlStep.window_position][autoControlStep.wind_direction_type][j];
                                                await sequelize.models.Report.createIgnoreAutoControlReport(control, autoControl, orderAutoControls[autoControl.id]);
                                            }
                                        }
                                    }
                                } else {
                                    if (autoControlStep.wind_direction_type === 'none' && autoControlStep.window_position === 'none') {
                                        const {
                                            activeResult,
                                            finalTime,
                                            targetMinMax,
                                            pBandValue,
                                            pBandPercentage,
                                            isAlreadyPBandPercentage,
                                            isSatisfiedPBandTemperature,
                                            isPBandIntegral,
                                            motorLogState,
                                            powerLogState,
                                        } = await active(autoControl, autoControlStep.control, autoControlStep, motors, powers, autoControl.order, 2, autoControlStep.is_min_max_range ? autoControlStep.autoControlStepMinMaxes : [], autoControlStep.wind_direction_type);
                                        if (activeResult) {
                                            await sequelize.models.AutoControl.update({current_step: 1}, {where: {id: autoControl.id}});
                                            await sequelize.models.AutoControlStep.update({active_time: currentTimestamp, current_count: 1}, {where: {id: autoControlStep.id}});
                                            await sequelize.models.Report.createAutoControlReport(autoControl, autoControlStep.control, autoControlStep, 2, finalTime, targetMinMax, {
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                            }, {
                                                motorLogState,
                                                powerLogState
                                            });
                                        }
                                    } else if (autoControlStep.wind_direction_type !== 'none' && autoControlStep.window_position !== 'none') {
                                        await sequelize.models.Report.createWindDirectionControlReport(house_id, autoControlStep.wind_direction_type, autoControlStep.window_position, autoControlStep, 2);
                                        for (let j=0; j<windDirectionControlGroups[autoControlStep.window_position][autoControlStep.wind_direction_type].length; j++) {
                                            const control = windDirectionControlGroups[autoControlStep.window_position][autoControlStep.wind_direction_type][j];
                                            const {
                                                activeResult,
                                                finalTime,
                                                targetMinMax,
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                                motorLogState,
                                                powerLogState,
                                            } = await active(autoControl, control, autoControlStep, motors, powers, autoControl.order, 2, autoControlStep.is_min_max_range ? autoControlStep.autoControlStepMinMaxes : [], autoControlStep.wind_direction_type);
                                            if (activeResult) {
                                                await sequelize.models.Report.createAutoControlReport(autoControl, control, autoControlStep, 2, finalTime, targetMinMax, {
                                                    pBandValue,
                                                    pBandPercentage,
                                                    isAlreadyPBandPercentage,
                                                    isSatisfiedPBandTemperature,
                                                    isPBandIntegral,
                                                }, {
                                                    motorLogState,
                                                    powerLogState
                                                });
                                            }
                                        }
                                        await sequelize.models.AutoControl.update({current_step: 1}, {where: {id: autoControl.id}});
                                        await sequelize.models.AutoControlStep.update({active_time: currentTimestamp, current_count: 1}, {where: {id: autoControlStep.id}});
                                    }
                                }
                            }
                        }
                    } else if (autoControl.current_step === autoControl.autoControlSteps.length) { // last step
                        const autoControlStep = autoControl.autoControlSteps[autoControl.autoControlSteps.length - 1];
                        const active_time = autoControlStep.active_time;
                        const time = generateTimeSecond(autoControlStep);
                        if (autoControlStep.recount && autoControlStep.recount >= autoControlStep.current_count) { // repeat
                            if (autoControlStep.delay && active_time && active_time + time + autoControlStep.delay * 1000 > currentTimestamp) {
                                continue; // skip by delay
                            } else {
                                if (isIgnore) {
                                    if (orderAutoControls[autoControl.id]) {
                                        if (autoControlStep.wind_direction_type === 'none' && autoControlStep.window_position === 'none') {
                                            await sequelize.models.Report.createIgnoreAutoControlReport(autoControlStep.control, autoControl, orderAutoControls[autoControl.id]);
                                        } else if (autoControlStep.wind_direction_type !== 'none' && autoControlStep.window_position !== 'none') {
                                            for (let j=0; j<windDirectionControlGroups[autoControlStep.window_position][autoControlStep.wind_direction_type].length; j++) {
                                                const control = windDirectionControlGroups[autoControlStep.window_position][autoControlStep.wind_direction_type][j];
                                                await sequelize.models.Report.createIgnoreAutoControlReport(control, autoControl, orderAutoControls[autoControl.id]);
                                            }
                                        }
                                    }
                                } else {
                                    if (autoControlStep.wind_direction_type === 'none' && autoControlStep.window_position === 'none') {
                                        const {
                                            activeResult,
                                            finalTime,
                                            targetMinMax,
                                            pBandValue,
                                            pBandPercentage,
                                            isAlreadyPBandPercentage,
                                            isSatisfiedPBandTemperature,
                                            isPBandIntegral,
                                            motorLogState,
                                            powerLogState,
                                        } = await active(autoControl, autoControlStep.control, autoControlStep, motors, powers, autoControl.order, autoControl.current_step + 2, autoControlStep.is_min_max_range ? autoControlStep.autoControlStepMinMaxes : [], autoControlStep.wind_direction_type);
                                        if (activeResult) {
                                            await sequelize.models.AutoControlStep.update({active_time: currentTimestamp, current_count: autoControlStep.current_count + 1}, {where: {id: autoControlStep.id}});
                                            await sequelize.models.Report.createAutoControlReport(autoControl, autoControlStep.control, autoControlStep, autoControl.current_step + 2, finalTime, targetMinMax, {
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                            }, {
                                                motorLogState,
                                                powerLogState
                                            });
                                        }
                                    } else if (autoControlStep.wind_direction_type !== 'none' && autoControlStep.window_position !== 'none') {
                                        await sequelize.models.Report.createWindDirectionControlReport(house_id, autoControlStep.wind_direction_type, autoControlStep.window_position, autoControlStep, autoControl.current_step + 2);
                                        for (let j=0; j<windDirectionControlGroups[autoControlStep.window_position][autoControlStep.wind_direction_type].length; j++) {
                                            const control = windDirectionControlGroups[autoControlStep.window_position][autoControlStep.wind_direction_type][j];
                                            const {
                                                activeResult,
                                                finalTime,
                                                targetMinMax,
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                                motorLogState,
                                                powerLogState,
                                            } = await active(autoControl, control, autoControlStep, motors, powers, autoControl.order, autoControl.current_step + 1, autoControlStep.is_min_max_range ? autoControlStep.autoControlStepMinMaxes : [], autoControlStep.wind_direction_type);
                                            if (activeResult) {
                                                await sequelize.models.Report.createAutoControlReport(autoControl, control, autoControlStep, autoControl.current_step + 1, finalTime, targetMinMax, {
                                                    pBandValue,
                                                    pBandPercentage,
                                                    isAlreadyPBandPercentage,
                                                    isSatisfiedPBandTemperature,
                                                    isPBandIntegral,
                                                }, {
                                                    motorLogState,
                                                    powerLogState
                                                });
                                            }
                                        }
                                        await sequelize.models.AutoControlStep.update({active_time: currentTimestamp, current_count: autoControlStep.current_count + 1}, {where: {id: autoControlStep.id}});
                                    }
                                }
                            }
                        } else { // next step > to first step
                            if (autoControlItem.active_count && autoControlItem.active_count <= autoControlItem.current_active_count) {
                                if (active_time) {
                                    const active_date = FILTER_UTIL.date(active_time, 'yyyy-MM-dd');
                                    if (today === active_date) {
                                        continue;
                                    } else {
                                        await sequelize.models.AutoControlItem.update({current_active_count: 0}, {where: {id: autoControlItem.id}});
                                    }
                                } else {
                                    await sequelize.models.AutoControlItem.update({current_active_count: 0}, {where: {id: autoControlItem.id}});
                                }
                            }
                            autoControlItem = await autoControlItem.reload();
                            if (autoControlItem.start_hour !== null && autoControlItem.start_minute !== null && autoControlItem.end_hour === null && autoControlItem.end_minute === null) {
                                if (!isIgnore) await sequelize.models.AutoControlItem.update({active: true}, {where: {id: autoControlItem.id}});
                                continue;
                            }
                            if (autoControl.step_delay && active_time && active_time + time + autoControl.step_delay * 1000 > currentTimestamp) {
                                continue; // skip by delay
                            } else {
                                if (isIgnore) {
                                    if (orderAutoControls[autoControl.id]) {
                                        if (autoControlItem.wind_direction_type === 'none' && autoControlItem.window_position === 'none') {
                                            await sequelize.models.Report.createIgnoreAutoControlReport(autoControlItem.control, autoControl, orderAutoControls[autoControl.id]);
                                        } else if (autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none') {
                                            for (let j=0; j<windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type].length; j++) {
                                                const control = windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type][j];
                                                await sequelize.models.Report.createIgnoreAutoControlReport(control, autoControl, orderAutoControls[autoControl.id]);
                                            }
                                        }
                                    }
                                } else {
                                    if (autoControlItem.wind_direction_type === 'none' && autoControlItem.window_position === 'none') {
                                        const {
                                            activeResult,
                                            finalTime,
                                            targetMinMax,
                                            pBandValue,
                                            pBandPercentage,
                                            isAlreadyPBandPercentage,
                                            isSatisfiedPBandTemperature,
                                            isPBandIntegral,
                                            motorLogState,
                                            powerLogState,
                                        } = await active(autoControl, autoControlItem.control, autoControlItem, motors, powers, autoControl.order, 1, autoControlItem.is_min_max_range ? autoControlItem.autoControlItemMinMaxes : [], autoControlItem.wind_direction_type);
                                        if (activeResult) {
                                            await sequelize.models.AutoControl.update({current_step: 0}, {where: {id: autoControl.id}});
                                            await sequelize.models.AutoControlItem.update({active_time: currentTimestamp, current_count: 1, current_active_count: autoControlItem.current_active_count + 1}, {where: {id: autoControlItem.id}});
                                            await sequelize.models.Report.createAutoControlReport(autoControl, autoControlItem.control, autoControlItem, 1, finalTime, targetMinMax, {
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                            }, {
                                                motorLogState,
                                                powerLogState
                                            });
                                        }
                                    } else if (autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none') {
                                        await sequelize.models.Report.createWindDirectionControlReport(house_id, autoControlItem.wind_direction_type, autoControlItem.window_position, autoControlItem, 1);
                                        for (let j=0; j<windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type].length; j++) {
                                            const control = windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type][j];
                                            const {
                                                activeResult,
                                                finalTime,
                                                targetMinMax,
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                                motorLogState,
                                                powerLogState,
                                            } = await active(autoControl, control, autoControlItem, motors, powers, autoControl.order, 1, autoControlItem.is_min_max_range ? autoControlItem.autoControlItemMinMaxes : [], autoControlItem.wind_direction_type);
                                            if (activeResult) {
                                                await sequelize.models.Report.createAutoControlReport(autoControl, control, autoControlItem, 1, finalTime, targetMinMax, {
                                                    pBandValue,
                                                    pBandPercentage,
                                                    isAlreadyPBandPercentage,
                                                    isSatisfiedPBandTemperature,
                                                    isPBandIntegral,
                                                }, {
                                                    motorLogState,
                                                    powerLogState
                                                });
                                            }
                                        }
                                        await sequelize.models.AutoControl.update({current_step: 0}, {where: {id: autoControl.id}});
                                        await sequelize.models.AutoControlItem.update({active_time: currentTimestamp, current_count: 1, current_active_count: autoControlItem.current_active_count + 1}, {where: {id: autoControlItem.id}});
                                    }
                                }
                            }
                        }
                    } else { // middle step
                        const prev = autoControl.autoControlSteps[autoControl.current_step - 1];
                        const next = autoControl.autoControlSteps[autoControl.current_step];
                        const active_time = prev.active_time;
                        const time = generateTimeSecond(prev);
                        if (prev.recount && prev.recount >= prev.current_count) { // repeat
                            if (prev.delay && active_time && active_time + time + prev.delay * 1000 > currentTimestamp) {
                                continue; // skip by delay
                            } else {
                                if (isIgnore) {
                                    if (orderAutoControls[autoControl.id]) {
                                        if (prev.wind_direction_type === 'none' && prev.window_position === 'none') {
                                            await sequelize.models.Report.createIgnoreAutoControlReport(prev.control, autoControl, orderAutoControls[autoControl.id]);
                                        } else if (prev.wind_direction_type !== 'none' && prev.window_position !== 'none') {
                                            for (let j=0; j<windDirectionControlGroups[prev.window_position][prev.wind_direction_type].length; j++) {
                                                const control = windDirectionControlGroups[prev.window_position][prev.wind_direction_type][j];
                                                await sequelize.models.Report.createIgnoreAutoControlReport(control, autoControl, orderAutoControls[autoControl.id]);
                                            }
                                        }
                                    }
                                } else {
                                    if (prev.wind_direction_type === 'none' && prev.window_position === 'none') {
                                        const {
                                            activeResult,
                                            finalTime,
                                            targetMinMax,
                                            pBandValue,
                                            pBandPercentage,
                                            isAlreadyPBandPercentage,
                                            isSatisfiedPBandTemperature,
                                            isPBandIntegral,
                                            motorLogState,
                                            powerLogState,
                                        } = await active(autoControl, prev.control, prev, motors, powers, autoControl.order, autoControl.current_step + 1, prev.is_min_max_range ? prev.autoControlStepMinMaxes : [], prev.wind_direction_type);
                                        if (activeResult) {
                                            await sequelize.models.AutoControlStep.update({active_time: currentTimestamp, current_count: prev.current_count + 1}, {where: {id: prev.id}});
                                            await sequelize.models.Report.createAutoControlReport(autoControl, prev.control, prev, autoControl.current_step + 1, finalTime, targetMinMax, {
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                            }, {
                                                motorLogState,
                                                powerLogState
                                            });
                                        }
                                    } else if (prev.wind_direction_type !== 'none' && prev.window_position !== 'none') {
                                        await sequelize.models.Report.createWindDirectionControlReport(house_id, prev.wind_direction_type, prev.window_position, prev, autoControl.current_step + 2);
                                        for (let j=0; j<windDirectionControlGroups[prev.window_position][prev.wind_direction_type].length; j++) {
                                            const control = windDirectionControlGroups[prev.window_position][prev.wind_direction_type][j];
                                            const {
                                                activeResult,
                                                finalTime,
                                                targetMinMax,
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                                motorLogState,
                                                powerLogState,
                                            } = await active(autoControl, control, prev, motors, powers, autoControl.order, autoControl.current_step + 2, prev.is_min_max_range ? prev.autoControlStepMinMaxes : [], prev.wind_direction_type);
                                            if (activeResult) {
                                                await sequelize.models.Report.createAutoControlReport(autoControl, control, prev, autoControl.current_step + 2, finalTime, targetMinMax, {
                                                    pBandValue,
                                                    pBandPercentage,
                                                    isAlreadyPBandPercentage,
                                                    isSatisfiedPBandTemperature,
                                                    isPBandIntegral,
                                                }, {
                                                    motorLogState,
                                                    powerLogState
                                                });
                                            }
                                        }
                                        await sequelize.models.AutoControlStep.update({active_time: currentTimestamp, current_count: prev.current_count + 1}, {where: {id: prev.id}});
                                    }
                                }
                            }
                        } else { // next step
                            if (autoControl.step_delay && active_time && active_time + time + autoControl.step_delay * 1000 > currentTimestamp) {
                                continue; // skip by delay
                            } else {
                                if (isIgnore) {
                                    if (orderAutoControls[autoControl.id]) {
                                        if (next.wind_direction_type === 'none' && next.window_position === 'none') {
                                            await sequelize.models.Report.createIgnoreAutoControlReport(next.control, autoControl, orderAutoControls[autoControl.id]);
                                        } else if (next.wind_direction_type !== 'none' && next.window_position !== 'none') {
                                            for (let j=0; j<windDirectionControlGroups[next.window_position][next.wind_direction_type].length; j++) {
                                                const control = windDirectionControlGroups[next.window_position][next.wind_direction_type][j];
                                                await sequelize.models.Report.createIgnoreAutoControlReport(control, autoControl, orderAutoControls[autoControl.id]);
                                            }
                                        }
                                    }
                                } else {
                                    if (next.wind_direction_type === 'none' && next.window_position === 'none') {
                                        const {
                                            activeResult,
                                            finalTime,
                                            targetMinMax,
                                            pBandValue,
                                            pBandPercentage,
                                            isAlreadyPBandPercentage,
                                            isSatisfiedPBandTemperature,
                                            isPBandIntegral,
                                            motorLogState,
                                            powerLogState,
                                        } = await active(autoControl, next.control, next, motors, powers, autoControl.order, autoControl.current_step + 2, next.is_min_max_range ? next.autoControlStepMinMaxes : [], next.wind_direction_type);
                                        if (activeResult) {
                                            await sequelize.models.AutoControl.update({current_step: autoControl.current_step + 1}, {where: {id: autoControl.id}});
                                            await sequelize.models.AutoControlStep.update({active_time: currentTimestamp, current_count: 1}, {where: {id: next.id}});
                                            await sequelize.models.Report.createAutoControlReport(autoControl, next.control, next, autoControl.current_step + 2, finalTime, targetMinMax, {
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                            }, {
                                                motorLogState,
                                                powerLogState
                                            });
                                        }
                                    } else if (next.wind_direction_type !== 'none' && next.window_position !== 'none') {
                                        await sequelize.models.Report.createWindDirectionControlReport(house_id, next.wind_direction_type, next.window_position, next, autoControl.current_step + 2);
                                        for (let j=0; j<windDirectionControlGroups[next.window_position][next.wind_direction_type].length; j++) {
                                            const control = windDirectionControlGroups[next.window_position][next.wind_direction_type][j];
                                            const {
                                                activeResult,
                                                finalTime,
                                                targetMinMax,
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                                motorLogState,
                                                powerLogState,
                                            } = await active(autoControl, control, next, motors, powers, autoControl.order, autoControl.current_step + 2, next.is_min_max_range ? next.autoControlStepMinMaxes : [], next.wind_direction_type);
                                            if (activeResult) {
                                                await sequelize.models.Report.createAutoControlReport(autoControl, control, next, autoControl.current_step + 2, finalTime, targetMinMax, {
                                                    pBandValue,
                                                    pBandPercentage,
                                                    isAlreadyPBandPercentage,
                                                    isSatisfiedPBandTemperature,
                                                    isPBandIntegral,
                                                }, {
                                                    motorLogState,
                                                    powerLogState
                                                });
                                            }
                                        }
                                        await sequelize.models.AutoControl.update({current_step: autoControl.current_step + 1}, {where: {id: autoControl.id}});
                                        await sequelize.models.AutoControlStep.update({active_time: currentTimestamp, current_count: 1}, {where: {id: next.id}});
                                    }
                                }
                            }
                        }
                    }
                } else {
                    for (let j=0; j<autoControl.autoControlItems.length; j++) {
                        let autoControlItem = autoControl.autoControlItems[j];
                        if (await checkItem(autoControlItem, currentTime)) {
                            const active_time = autoControlItem.active_time;
                            const time = generateTimeSecond(autoControlItem);
                            if (autoControlItem.active_count && autoControlItem.active_count <= autoControlItem.current_active_count) {
                                if (active_time) {
                                    const active_date = FILTER_UTIL.date(active_time, 'yyyy-MM-dd');
                                    if (today === active_date) {
                                        continue;
                                    } else {
                                        await sequelize.models.AutoControlItem.update({current_active_count: 0}, {where: {id: autoControlItem.id}});
                                    }
                                } else {
                                    await sequelize.models.AutoControlItem.update({current_active_count: 0}, {where: {id: autoControlItem.id}});
                                }
                            }
                            autoControlItem = await autoControlItem.reload();
                            if (autoControlItem.recount && autoControlItem.current_count && autoControlItem.recount >= autoControlItem.current_count) { // repeat
                                if (autoControlItem.delay && active_time && active_time + time + autoControlItem.delay * 1000 > currentTimestamp) {
                                    continue;
                                } else {
                                    if (isIgnore) {
                                        if (orderAutoControls[autoControl.id]) {
                                            if (autoControlItem.wind_direction_type === 'none' && autoControlItem.window_position === 'none') {
                                                await sequelize.models.Report.createIgnoreAutoControlReport(autoControlItem.control, autoControl, orderAutoControls[autoControl.id]);
                                            } else if (autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none') {
                                                for (let k=0; k<windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type].length; k++) {
                                                    const control = windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type][k];
                                                    await sequelize.models.Report.createIgnoreAutoControlReport(control, autoControl, orderAutoControls[autoControl.id]);
                                                }
                                            }
                                        }
                                    } else {
                                        if (autoControlItem.wind_direction_type === 'none' && autoControlItem.window_position === 'none') {
                                            const {
                                                activeResult,
                                                finalTime,
                                                targetMinMax,
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                                motorLogState,
                                                powerLogState,
                                            } = await active(autoControl, autoControlItem.control, autoControlItem, motors, powers, autoControl.order, null, autoControlItem.is_min_max_range ? autoControlItem.autoControlItemMinMaxes : [], autoControlItem.wind_direction_type);
                                            if (activeResult) {
                                                await sequelize.models.AutoControl.update({current_step: 0}, {where: {id: autoControl.id}});
                                                await sequelize.models.AutoControlItem.update({active_time: currentTimestamp, current_count: autoControlItem.current_count + 1, current_active_count: autoControlItem.current_active_count + 1}, {where: {id: autoControlItem.id}});
                                                await sequelize.models.Report.createAutoControlReport(autoControl, autoControlItem.control, autoControlItem, null, finalTime, targetMinMax, {
                                                    pBandValue,
                                                    pBandPercentage,
                                                    isAlreadyPBandPercentage,
                                                    isSatisfiedPBandTemperature,
                                                    isPBandIntegral,
                                                }, {
                                                    motorLogState,
                                                    powerLogState
                                                });
                                            }
                                        } else if (autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none') {
                                            await sequelize.models.Report.createWindDirectionControlReport(house_id, autoControlItem.wind_direction_type, autoControlItem.window_position, autoControlItem, null);
                                            for (let k=0; k<windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type].length; k++) {
                                                const control = windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type][k];
                                                const {
                                                    activeResult,
                                                    finalTime,
                                                    targetMinMax,
                                                    pBandValue,
                                                    pBandPercentage,
                                                    isAlreadyPBandPercentage,
                                                    isSatisfiedPBandTemperature,
                                                    isPBandIntegral,
                                                    motorLogState,
                                                    powerLogState,
                                                } = await active(autoControl, control, autoControlItem, motors, powers, autoControl.order, null, autoControlItem.is_min_max_range ? autoControlItem.autoControlItemMinMaxes : [], autoControlItem.wind_direction_type);
                                                if (activeResult) {
                                                    await sequelize.models.Report.createAutoControlReport(autoControl, control, autoControlItem, null, finalTime, targetMinMax, {
                                                        pBandValue,
                                                        pBandPercentage,
                                                        isAlreadyPBandPercentage,
                                                        isSatisfiedPBandTemperature,
                                                        isPBandIntegral,
                                                    }, {
                                                        motorLogState,
                                                        powerLogState
                                                    });
                                                }
                                            }
                                            await sequelize.models.AutoControl.update({current_step: 0}, {where: {id: autoControl.id}});
                                            await sequelize.models.AutoControlItem.update({active_time: currentTimestamp, current_count: autoControlItem.current_count + 1, current_active_count: autoControlItem.current_active_count + 1}, {where: {id: autoControlItem.id}});
                                        }
                                    }
                                }
                            } else { // next step
                                if (autoControlItem.delay && active_time && active_time + time + autoControlItem.delay * 1000 > currentTimestamp) {
                                    continue;
                                } else {
                                    if (isIgnore) {
                                        if (orderAutoControls[autoControl.id]) {
                                            if (autoControlItem.wind_direction_type === 'none' && autoControlItem.window_position === 'none') {
                                                await sequelize.models.Report.createIgnoreAutoControlReport(autoControlItem.control, autoControl, orderAutoControls[autoControl.id]);
                                            } else if (autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none') {
                                                for (let k=0; k<windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type].length; k++) {
                                                    const control = windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type][k];
                                                    await sequelize.models.Report.createIgnoreAutoControlReport(control, autoControl, orderAutoControls[autoControl.id]);
                                                }
                                            }
                                        }
                                    } else {
                                        if (autoControlItem.wind_direction_type === 'none' && autoControlItem.window_position === 'none') {
                                            const {
                                                activeResult,
                                                finalTime,
                                                targetMinMax,
                                                pBandValue,
                                                pBandPercentage,
                                                isAlreadyPBandPercentage,
                                                isSatisfiedPBandTemperature,
                                                isPBandIntegral,
                                                motorLogState,
                                                powerLogState,
                                            } = await active(autoControl, autoControlItem.control, autoControlItem, motors, powers, autoControl.order, null, autoControlItem.is_min_max_range ? autoControlItem.autoControlItemMinMaxes : [], autoControlItem.wind_direction_type);
                                            if (activeResult) {
                                                await sequelize.models.AutoControl.update({current_step: 0}, {where: {id: autoControl.id}});
                                                await sequelize.models.AutoControlItem.update({active_time: currentTimestamp, current_count: 1, current_active_count: autoControlItem.current_active_count + 1}, {where: {id: autoControlItem.id}});
                                                await sequelize.models.Report.createAutoControlReport(autoControl, autoControlItem.control, autoControlItem, null, finalTime, targetMinMax, {
                                                    pBandValue,
                                                    pBandPercentage,
                                                    isAlreadyPBandPercentage,
                                                    isSatisfiedPBandTemperature,
                                                    isPBandIntegral,
                                                }, {
                                                    motorLogState,
                                                    powerLogState
                                                });
                                            }
                                        } else if (autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none') {
                                            await sequelize.models.Report.createWindDirectionControlReport(house_id, autoControlItem.wind_direction_type, autoControlItem.window_position, autoControlItem, null);
                                            for (let k=0; k<windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type].length; k++) {
                                                const control = windDirectionControlGroups[autoControlItem.window_position][autoControlItem.wind_direction_type][k];
                                                const {
                                                    activeResult,
                                                    finalTime,
                                                    targetMinMax,
                                                    pBandValue,
                                                    pBandPercentage,
                                                    isAlreadyPBandPercentage,
                                                    isSatisfiedPBandTemperature,
                                                    isPBandIntegral,
                                                    motorLogState,
                                                    powerLogState,
                                                } = await active(autoControl, control, autoControlItem, motors, powers, autoControl.order, null, autoControlItem.is_min_max_range ? autoControlItem.autoControlItemMinMaxes : [], autoControlItem.wind_direction_type);
                                                if (activeResult) {
                                                    await sequelize.models.Report.createAutoControlReport(autoControl, control, autoControlItem, null, finalTime, targetMinMax, {
                                                        pBandValue,
                                                        pBandPercentage,
                                                        isAlreadyPBandPercentage,
                                                        isSatisfiedPBandTemperature,
                                                        isPBandIntegral,
                                                    }, {
                                                        motorLogState,
                                                        powerLogState
                                                    });
                                                }
                                            }
                                            await sequelize.models.AutoControl.update({current_step: 0}, {where: {id: autoControl.id}});
                                            await sequelize.models.AutoControlItem.update({active_time: currentTimestamp, current_count: 1, current_active_count: autoControlItem.current_active_count + 1}, {where: {id: autoControlItem.id}});
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
};

function calculateMinMaxes (time, state, current, range, minMaxes) {
    let targetPercentage = null, targetMinMax;
    if (minMaxes && minMaxes.length) {
        for (let i=0; i<minMaxes.length; i++) {
            const minMax = minMaxes[i];
            const controlMinMaxRange = minMax.controlMinMaxRange;
            if (minMax.state === 'on' && controlMinMaxRange.state === 'on') {
                const sensor = controlMinMaxRange.sensor;
                const sensorValue = parseFloat(sensor.value);
                const conditionValue = parseFloat(controlMinMaxRange.value);
                let isTarget = false;
                if (sensor.type === 'windDirection' || sensor.type === 'korinsWindDirection') {
                    if (normalizeWindDirectionValue(sensorValue) === conditionValue) {
                        isTarget = true;
                    }
                } else {
                    switch(controlMinMaxRange.condition) {
                        case 'equal':
                            if (sensorValue === conditionValue) {
                                isTarget = true;
                            }
                            break;
                        case 'over':
                            if (sensorValue >= conditionValue) {
                                isTarget = true;
                            }
                            break;
                        case 'under':
                            if (sensorValue < conditionValue) {
                                isTarget = true;
                            }
                            break;
                    }
                }
                if (isTarget) {
                    if (targetPercentage === null) {
                        targetPercentage = minMax.percentage;
                        targetMinMax = minMax;
                    } else {
                        if (state === 'open') {
                            if (targetPercentage > minMax.percentage) {
                                targetPercentage = minMax.percentage;
                                targetMinMax = minMax;
                            }
                        } else {
                            if (targetPercentage < minMax.percentage) {
                                targetPercentage = minMax.percentage;
                                targetMinMax = minMax;
                            }
                        }
                    }
                }
            }
        }

        if (targetPercentage !== null) {
            const currentPercentage = parseInt(current / range * 100);
            const activePercentage = parseInt(time / range * 100);
            if (state === 'open') {
                if (currentPercentage + activePercentage > targetPercentage) {
                    time = parseInt((targetPercentage - currentPercentage) * range / 100);
                }
            } else {
                if (currentPercentage - activePercentage < targetPercentage) {
                    time = parseInt((currentPercentage - targetPercentage) * range / 100);
                }
            }
        }
    }
    if (time < 0) time = 0;
    return {
        finalTime: time,
        targetMinMax
    };
}

function generatePBand (outTemperature, windSpeed, wind_direction_type, pBand = null) {
    outTemperature = Math.round(outTemperature);
    if (outTemperature < 0) {
        outTemperature = 0;
    } else if (outTemperature > 45) {
        outTemperature = 45;
    }
    windSpeed = Math.round(windSpeed);
    if (windSpeed < 0) {
        windSpeed = 0;
    } else if (windSpeed > 10) {
        windSpeed = 10;
    }
    let pBandValue = P_BAND[windSpeed][outTemperature];
    if (pBand.is_min_max_p) {
        console.log(pBand.is_min_max_p);
        if (wind_direction_type === 'forward') {
            if (pBandValue > pBand.wind_direction_forward_max_p) {
                pBandValue = pBand.wind_direction_forward_max_p;
            } else if (pBandValue < pBand.wind_direction_forward_min_p) {
                pBandValue = pBand.wind_direction_forward_min_p;
            }
        } else if (wind_direction_type === 'backward') {
            if (pBandValue > pBand.wind_direction_backward_max_p) {
                pBandValue = pBand.wind_direction_backward_max_p;
            } else if (pBandValue < pBand.wind_direction_backward_min_p) {
                pBandValue = pBand.wind_direction_backward_min_p;
            }
        }
    }
    return pBandValue;
}

async function active(autoControl, control, {
    id,
    sensor_operator,
    state,
    time,
    percentage,
    pBand,
    p_band_temperature,
    p_band_integral,
    is_temperature_control_option,
    rise_time,
    drop_time,
    expect_temperature,
    expectTemperatureSensor,
    start_temperature_time,
    start_temperature,
    is_expect_temperature_option,
    expect_temperature_max_value,
    expect_temperature_min_value,
    autoControlExpectTemperatureSensors
}, motors, powers, order, currentStep = null, minMaxes = null, wind_direction_type) {
    const now = new Date().getTime();

    if (is_temperature_control_option) {
        if (!start_temperature_time) {
            start_temperature_time = now;
            if (sensor_operator) {
                if(control.type === 'motor'){
                    start_temperature = pBand.inTemperatureSensor.value;
                    await sequelize.models.AutoControlItem.update({
                        start_temperature_time,
                        start_temperature
                    }, {
                        where: {id}
                    });
                }else if(control.type === 'power'){
                    start_temperature = expectTemperatureSensor.value;
                    await sequelize.models.AutoControlItem.update({
                        start_temperature_time,
                        start_temperature
                    }, {
                        where: {id}
                    });
                }

            } else {
                start_temperature = expectTemperatureSensor.value;
                await sequelize.models.AutoControlStep.update({
                    start_temperature_time,
                    start_temperature
                }, {
                    where: {id}
                });
            }
        }
    }

    if (control.type === 'motor') {
        let timeData, pBandValue = null, pBandPercentage = null, isPBandIntegral = false, motorLogState = null;
        if (time) {
            timeData = time;
        } else if (percentage) {
            timeData = parseInt(percentage * control.range / 100);
            if (!timeData) timeData = 1;
        } else if (is_temperature_control_option) {
            if (is_expect_temperature_option) {
                p_band_temperature = calculatePBandTemperature(p_band_temperature, expect_temperature_max_value, expect_temperature_min_value,autoControlExpectTemperatureSensors );
            }
            const currentTemperatureValue = pBand.inTemperatureSensor.value;
            const inTemperatureSensorValue = pBand.inTemperatureSensor.value;
            if (currentTemperatureValue > p_band_temperature) {
                if (start_temperature > currentTemperatureValue) {
                    const gapTemperature = parseInt(start_temperature - currentTemperatureValue);
                    if (gapTemperature > 1 && (now - start_temperature_time) / 60000 < drop_time * gapTemperature) {
                        motorLogState = 1;
                        state = 'close';
                        timeData = parseInt(200 * control.range / 100);
                    } else {
                        motorLogState = 2;
                        pBandValue = generatePBand(pBand.outTemperatureSensor.value, pBand.windSpeedSensor.value, wind_direction_type, pBand);
                        pBandPercentage = parseInt((inTemperatureSensorValue - p_band_temperature) / pBandValue * 100);
                        const currentPercentage = parseInt(control.current / control.range * 100);
                        if (currentPercentage >= pBandPercentage) {
                            if (p_band_integral) {
                                isPBandIntegral = true;
                                timeData = parseInt(p_band_integral * control.range / 100);
                                if (!timeData) timeData = 1;
                            } else {
                                return {
                                    activeResult: true,
                                    finalTime: null,
                                    targetMinMax: null,
                                    isAlreadyPBandPercentage: true,
                                    motorLogState
                                };
                            }
                        } else {
                            timeData = parseInt((pBandPercentage - currentPercentage) * control.range / 100);
                            if (!timeData) {
                                return {
                                    activeResult: true,
                                    finalTime: null,
                                    targetMinMax: null,
                                    isAlreadyPBandPercentage: true,
                                    motorLogState
                                };
                            }
                        }
                    }
                } else {
                    motorLogState = 3;
                    pBandValue = generatePBand(pBand.outTemperatureSensor.value, pBand.windSpeedSensor.value, wind_direction_type, pBand);
                    pBandPercentage = parseInt((inTemperatureSensorValue - p_band_temperature) / pBandValue * 100);
                    const currentPercentage = parseInt(control.current / control.range * 100);
                    if (currentPercentage >= pBandPercentage) {
                        if (p_band_integral) {
                            isPBandIntegral = true;
                            timeData = parseInt(p_band_integral * control.range / 100);
                            if (!timeData) timeData = 1;
                        } else {
                            return {
                                activeResult: true,
                                finalTime: null,
                                targetMinMax: null,
                                isAlreadyPBandPercentage: true,
                                motorLogState
                            };
                        }
                    } else {
                        timeData = parseInt((pBandPercentage - currentPercentage) * control.range / 100);
                        if (!timeData) {
                            return {
                                activeResult: true,
                                finalTime: null,
                                targetMinMax: null,
                                isAlreadyPBandPercentage: true,
                                motorLogState
                            };
                        }
                    }
                }
            } else if (currentTemperatureValue < p_band_temperature) {
                if (start_temperature >= currentTemperatureValue) {
                    motorLogState = 4;
                    state = 'close';
                    timeData = parseInt(200 * control.range / 100);
                } else {
                    const gapTemperature = parseInt(currentTemperatureValue - start_temperature);
                    if (gapTemperature > 1 && (now - start_temperature_time) / 60000 < rise_time * gapTemperature) {
                        motorLogState = 5;
                        pBandValue = generatePBand(pBand.outTemperatureSensor.value, pBand.windSpeedSensor.value, wind_direction_type, pBand);
                        pBandPercentage = parseInt((inTemperatureSensorValue - (start_temperature + 1)) / pBandValue * 100);
                        const currentPercentage = parseInt(control.current / control.range * 100);
                        if (currentPercentage >= pBandPercentage) {
                            if (p_band_integral) {
                                isPBandIntegral = true;
                                timeData = parseInt(p_band_integral * control.range / 100);
                                if (!timeData) timeData = 1;
                            } else {
                                return {
                                    activeResult: true,
                                    finalTime: null,
                                    targetMinMax: null,
                                    isAlreadyPBandPercentage: true,
                                    motorLogState
                                };
                            }
                        } else {
                            timeData = parseInt((pBandPercentage - currentPercentage) * control.range / 100);
                            if (!timeData) {
                                return {
                                    activeResult: true,
                                    finalTime: null,
                                    targetMinMax: null,
                                    isAlreadyPBandPercentage: true,
                                    motorLogState
                                };
                            }
                        }
                    } else {
                        motorLogState = 6;
                        state = 'close';
                        timeData = parseInt(200 * control.range / 100);
                    }
                }
            } else {
                motorLogState = 7;
                return {
                    activeResult: true,
                    finalTime: null,
                    targetMinMax: null,
                    isSatisfiedPBandTemperature: true,
                    motorLogState
                };
            }
        } else if (p_band_temperature) {
            const inTemperatureSensorValue = pBand.inTemperatureSensor.value;
            if (inTemperatureSensorValue >= p_band_temperature + 1) {
                pBandValue = generatePBand(pBand.outTemperatureSensor.value, pBand.windSpeedSensor.value, wind_direction_type, pBand);
                pBandPercentage = parseInt((inTemperatureSensorValue - p_band_temperature) / pBandValue * 100);
                if (pBandPercentage > 100) pBandPercentage = 100;
                const currentPercentage = parseInt(control.current / control.range * 100);
                if (currentPercentage >= pBandPercentage) {
                    if (p_band_integral) {
                        isPBandIntegral = true;
                        timeData = parseInt(p_band_integral * control.range / 100);
                        if (!timeData) timeData = 1;
                    } else {
                        return {
                            activeResult: true,
                            finalTime: null,
                            targetMinMax: null,
                            isAlreadyPBandPercentage: true
                        };
                    }
                } else {
                    timeData = parseInt((pBandPercentage - currentPercentage) * control.range / 100);
                    if (!timeData) {
                        return {
                            activeResult: true,
                            finalTime: null,
                            targetMinMax: null,
                            isAlreadyPBandPercentage: true
                        };
                    }
                }
            } else {
                return {
                    activeResult: true,
                    finalTime: null,
                    targetMinMax: null,
                    isSatisfiedPBandTemperature: true
                };
            }
        }
        if (!motors[control.id].beforeWork ||
            (motors[control.id].beforeWork && motors[control.id].beforeWork.order > order)) {
            try {
                const {finalTime, targetMinMax} = calculateMinMaxes(timeData, state, control.current, control.range, minMaxes);
                if (finalTime) {
                    await CONTROL_METHOD.activeMotor({
                        id: control.id,
                        state,
                        type: 'auto',
                        time: finalTime,
                        user_id: CONFIG.autoUser.id,
                        autoControl,
                        currentStep
                    });
                }
                return {
                    activeResult: true,
                    finalTime,
                    targetMinMax: finalTime !== timeData ? targetMinMax : null,
                    pBandValue,
                    pBandPercentage,
                    isPBandIntegral,
                    motorLogState,
                };
            } catch (e) {
                return {
                    activeResult: false
                };
            }
        } else {
            return {
                activeResult: false
            };
        }
    } else if (control.type === 'power') {
        if (!powers[control.id].beforeWork ||
            (powers[control.id] && powers[control.id].beforeWork.order > order)) {
            try {
                let powerLogState = null;
                if (is_temperature_control_option) {
                    const currentTemperatureValue = expectTemperatureSensor.value;
                    time = null;
                    if (currentTemperatureValue > expect_temperature) {
                        if (start_temperature > currentTemperatureValue) {
                            const gapTemperature = parseInt(start_temperature - currentTemperatureValue);
                            if (gapTemperature > 1 && (now - start_temperature_time) / 60000 < drop_time * gapTemperature) {
                                powerLogState = 1;
                                state = 'on';
                            } else {
                                powerLogState = 2;
                                state = 'off';
                            }
                        } else {
                            powerLogState = 3;
                            state = 'off';
                        }
                    } else if (currentTemperatureValue < expect_temperature) {
                        if (start_temperature >= currentTemperatureValue) {
                            powerLogState = 4;
                            state = 'on';
                        } else {
                            const gapTemperature = parseInt(currentTemperatureValue - start_temperature);
                            if (gapTemperature > 1 && (now - start_temperature_time) / 60000 < rise_time * gapTemperature) {
                                powerLogState = 5;
                                state = 'off';
                            } else {
                                powerLogState = 6;
                                state = 'on';
                            }
                        }
                    } else {
                        powerLogState = 7;
                        state = 'off';
                    }
                }
                await CONTROL_METHOD.activePower({
                    id: control.id,
                    state,
                    type: 'auto',
                    time,
                    user_id: CONFIG.autoUser.id,
                    autoControl,
                    currentStep
                });
                return {
                    activeResult: true,
                    finalTime: time,
                    powerLogState
                };
            } catch (e) {
                return {
                    activeResult: false
                };
            }
        } else {
            return {
                activeResult: false
            };
        }
    }
}

async function check(autoControl, currentTime) {
    for (let i=0; i<autoControl.autoControlItems.length; i++) {
        if (autoControl.autoControlItems[i].control && autoControl.autoControlItems[i].control.mode !== 'auto') {
            return false;
        }
    }
    if (autoControl.autoControlSteps.length) {
        const autoControlItem = autoControl.autoControlItems[0];
        if (!await checkItem(autoControlItem, currentTime)) {
            return false;
        }
        for (let i=0; i<autoControl.autoControlSteps.length; i++) {
            if (autoControl.autoControlSteps[i].control && autoControl.autoControlSteps[i].control.mode !== 'auto') {
                return false;
            }
        }
    }
    return true;
}

async function checkItem(autoControlItem, currentTime) {
    if (autoControlItem.is_sun_date) {
        let startTime, endTime;
        if (autoControlItem.sun_date_start_type === STD.sunDate.typeRise) {
            let temp = parseInt(sunDate.rise_hour) * 60 + parseInt(sunDate.rise_minute) + autoControlItem.sun_date_start_time;
            if (temp < 0) {
                temp += 1440;
            }
            let hour = parseInt(temp / 60);
            let minute = temp % 60;
            startTime = '' + FILTER_UTIL.attachZero(hour) + FILTER_UTIL.attachZero(minute);
        } else {
            let temp = parseInt(sunDate.set_hour) * 60 + parseInt(sunDate.set_minute) + autoControlItem.sun_date_start_time;
            if (temp < 0) {
                temp += 1440;
            }
            let hour = parseInt(temp / 60);
            let minute = temp % 60;
            startTime = '' + FILTER_UTIL.attachZero(hour) + FILTER_UTIL.attachZero(minute);
        }
        if (autoControlItem.sun_date_end_type === STD.sunDate.typeRise) {
            let temp = parseInt(sunDate.rise_hour) * 60 + parseInt(sunDate.rise_minute) + autoControlItem.sun_date_end_time;
            if (temp < 0) {
                temp += 1440;
            }
            let hour = parseInt(temp / 60);
            let minute = temp % 60;
            endTime = '' + FILTER_UTIL.attachZero(hour) + FILTER_UTIL.attachZero(minute);
        } else {
            let temp = parseInt(sunDate.set_hour) * 60 + parseInt(sunDate.set_minute) + autoControlItem.sun_date_end_time;
            if (temp < 0) {
                temp += 1440;
            }
            let hour = parseInt(temp / 60);
            let minute = temp % 60;
            endTime = '' + FILTER_UTIL.attachZero(hour) + FILTER_UTIL.attachZero(minute);
        }
        if (startTime < endTime) {
            if (currentTime < startTime || currentTime > endTime) {
                return false;
            }
        } else {
            if (currentTime < startTime && currentTime > endTime) {
                return false;
            }
        }
    } else {
        if (autoControlItem.start_hour !== null &&
            autoControlItem.end_hour !== null &&
            autoControlItem.start_minute !== null &&
            autoControlItem.end_minute !== null) {
            const startTime = '' + FILTER_UTIL.attachZero(autoControlItem.start_hour) + FILTER_UTIL.attachZero(autoControlItem.start_minute);
            const endTime = '' + FILTER_UTIL.attachZero(autoControlItem.end_hour) + FILTER_UTIL.attachZero(autoControlItem.end_minute);
            if (startTime < endTime) {
                if (currentTime < startTime || currentTime > endTime) {
                    return false;
                }
            } else {
                if (currentTime < startTime && currentTime > endTime) {
                    return false;
                }
            }
        } else if (autoControlItem.start_hour !== null && autoControlItem.start_minute !== null) {
            if(!autoControlItem.is_using_period){
                const startTime = '' + FILTER_UTIL.attachZero(autoControlItem.start_hour) + FILTER_UTIL.attachZero(autoControlItem.start_minute);
                if (currentTime !== startTime) {
                    return false;
                }
            }
        }
    }
    let isSensorActive = false;
    let isControlActive = false;
    if (!autoControlItem.autoControlSensors.length) {
        isSensorActive = true;
    }
    if (!autoControlItem.autoControlControls.length) {
        isControlActive = true;
    }
    if (autoControlItem.sensor_operator === 'and') {
        isSensorActive = true;
        for (let i=0; i<autoControlItem.autoControlSensors.length; i++) {
            const autoControlSensor = autoControlItem.autoControlSensors[i];
            const conditionValue = parseFloat(autoControlSensor.value);
            const sensorValue = calculateOptionValue(autoControlSensor);
            if (autoControlSensor.sensor.type === 'windDirection' ||autoControlSensor.sensor.type === 'korinsWindDirection') {
                if (normalizeWindDirectionValue(sensorValue) !== conditionValue) {
                    return false;
                }
            } else {
                switch(autoControlSensor.condition) {
                    case 'equal':
                        if (sensorValue !== conditionValue) {
                            return false;
                        }
                        break;
                    case 'over':
                        if (sensorValue < conditionValue) {
                            return false;
                        }
                        break;
                    case 'excess':
                        if (sensorValue <= conditionValue) {
                            return false;
                        }
                        break;
                    case 'below':
                        if (sensorValue > conditionValue) {
                            return false;
                        }
                        break;
                    case 'under':
                        if (sensorValue >= conditionValue) {
                            return false;
                        }
                        break;
                }
            }
        }
    } else if (autoControlItem.sensor_operator === 'or') {
        for (let i=0; i<autoControlItem.autoControlSensors.length; i++) {
            const autoControlSensor = autoControlItem.autoControlSensors[i];
            const conditionValue = parseFloat(autoControlSensor.value);
            const sensorValue = calculateOptionValue(autoControlSensor);
            //const sensorValue = parseFloat(autoControlSensor.sensor.value);
            if (autoControlSensor.sensor.type === 'windDirection' ||autoControlSensor.sensor.type === 'korinsWindDirection') {
                if (normalizeWindDirectionValue(sensorValue) === conditionValue) {
                    return true;
                }
            } else {
                switch(autoControlSensor.condition) {
                    case 'equal':
                        if (sensorValue === conditionValue) {
                            isSensorActive = true;
                        }
                        break;
                    case 'over':
                        if (sensorValue >= conditionValue) {
                            isSensorActive = true;
                        }
                        break;
                    case 'excess':
                        if (sensorValue > conditionValue) {
                            isSensorActive = true;
                        }
                        break;
                    case 'below':
                        if (sensorValue <= conditionValue) {
                            isSensorActive = true;
                        }
                        break;
                    case 'under':
                        if (sensorValue < conditionValue) {
                            isSensorActive = true;
                        }
                        break;
                }
                if (isSensorActive) break;
            }
        }
    }
    if (autoControlItem.control_operator === 'and') {
        isControlActive = true;
        for (let i=0; i<autoControlItem.autoControlControls.length; i++) {
            const autoControlControl = autoControlItem.autoControlControls[i];
            const conditionValue = parseInt(autoControlControl.value);
            const control = autoControlControl.control;
            const controlValue = control.type === 'motor' ? parseInt(control.current / control.range * 100) : (control.state === 'on' ? 1 : 0);
            switch(autoControlControl.condition) {
                case 'equal':
                    if (controlValue !== conditionValue) {
                        return false;
                    }
                    break;
                case 'over':
                    if (controlValue < conditionValue) {
                        return false;
                    }
                    break;
                case 'excess':
                    if (controlValue <= conditionValue) {
                        return false;
                    }
                    break;
                case 'below':
                    if (controlValue > conditionValue) {
                        return false;
                    }
                    break;
                case 'under':
                    if (controlValue >= conditionValue) {
                        return false;
                    }
                    break;
            }
        }
    } else if (autoControlItem.control_operator === 'or') {
        for (let i=0; i<autoControlItem.autoControlControls.length; i++) {
            const autoControlControl = autoControlItem.autoControlControls[i];
            const conditionValue = parseInt(autoControlControl.value);
            const control = autoControlControl.control;
            const controlValue = control.type === 'motor' ? parseInt(control.current / control.range * 100) : (control.state === 'on' ? 1 : 0);
            switch(autoControlControl.condition) {
                case 'equal':
                    if (controlValue === conditionValue) {
                        isControlActive = true;
                    }
                    break;
                case 'over':
                    if (controlValue >= conditionValue) {
                        isControlActive = true;
                    }
                    break;
                case 'excess':
                    if (controlValue > conditionValue) {
                        isControlActive = true;
                    }
                    break;
                case 'below':
                    if (controlValue <= conditionValue) {
                        isControlActive = true;
                    }
                    break;
                case 'under':
                    if (controlValue < conditionValue) {
                        isControlActive = true;
                    }
                    break;
            }
            if (isControlActive) break;
        }
    }
    return isSensorActive && isControlActive;
}

function normalizeWindDirectionValue(value) {
    const normalized = parseInt((value + 22.5) / 45);
    return normalized > 7 ? 0 : normalized * 45;
}

function generateTimeSecond(data) {
    let time = 0;
    if (data.time) {
        time = data.time;
    } else if (data.percentage) {
        if (data.control) {
            time = parseInt(data.control.range * data.percentage / 100);
            if (!time) time = 1;
        }
    }
    return time * 1000;
}

function calculatePBandTemperature(p_band_temperature, expect_temperature_max_value, expect_temperature_min_value, autoControlExpectTemperatureSensors) {
    const option_min_value = expect_temperature_min_value;
    let optionMinValue = expect_temperature_min_value;
    let optionMaxValue = expect_temperature_max_value;
    if (option_min_value < 0) {
        optionMaxValue -= option_min_value;
        optionMinValue = 0;
    }
    for (let i=0; i<autoControlExpectTemperatureSensors.length; i++) {
        const option = autoControlExpectTemperatureSensors[i];
        let currentValue = option.sensor.value;
        let min_value = option.min_value;
        let max_value = option.max_value;
        if (min_value > currentValue) {
            currentValue = min_value;
        } else if (max_value < currentValue) {
            currentValue = max_value;
        }
        if (min_value < 0) {
            currentValue -= min_value;
            max_value -= min_value;
            min_value = 0;
        }
        let optionValue = ((currentValue - min_value) / (max_value - min_value) * option.percentage / 100 * (optionMaxValue - optionMinValue)) + optionMinValue;
        if (option_min_value < 0) {
            optionValue += option_min_value;
        }
        p_band_temperature += optionValue;
    }
    return p_band_temperature;
}

function calculateOptionValue(autoControlSensor) {
    let sensorValue = parseFloat(autoControlSensor.sensor.value);
    const option_min_value = autoControlSensor.option_min_value;
    const option_max_value = autoControlSensor.option_max_value;
    let optionMinValue = option_min_value;
    let optionMaxValue = option_max_value;
    if (option_min_value < 0) {
        optionMaxValue -= option_min_value;
        optionMinValue = 0;
    }
    if (autoControlSensor.is_use_option) {
        for (let i=0; i<autoControlSensor.autoControlSensorOptions.length; i++) {
            const autoControlSensorOption = autoControlSensor.autoControlSensorOptions[i];
            let currentValue = autoControlSensorOption.sensor.value;
            let min_value = autoControlSensorOption.min_value;
            let max_value = autoControlSensorOption.max_value;
            if (min_value > currentValue) {
                currentValue = min_value;
            } else if (max_value < currentValue) {
                currentValue = max_value;
            }
            if (min_value < 0) {
                currentValue -= min_value;
                max_value -= min_value;
                min_value = 0;
            }

            let optionValue = ((currentValue - min_value) / (max_value - min_value) * autoControlSensorOption.percentage / 100 * (optionMaxValue - optionMinValue)) + optionMinValue;
            if (option_min_value < 0) {
                optionValue += option_min_value;
            }
            sensorValue -= optionValue;
        }
    }
    return sensorValue;
}

module.exports.startTime = (house_id, period) => {
    setTime(house_id, period);
};
module.exports.getTime = (house_id) => {
    if (currentTimeHash[house_id]) {
        return currentTimeHash[house_id].time;
    } else {
        return 0;
    }
};
