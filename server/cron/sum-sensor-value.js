const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const FILTER_UTIL = require('../utils/filter');

let isSumming = false;

module.exports = finish => {
    if (isSumming) {
        return finish(400, 'summing sensor value');
    } else {
        isSumming = true;
    }

    let funcs = [], sensors = [], startTime = new Date(FILTER_UTIL.date(new Date(), 'yyyy-MM-dd') + ' 00:00:00');
    let endTime = new Date(startTime.getTime() + 86400000);
    let table_names = ['log_sensor_' + startTime.getFullYear() + (FILTER_UTIL.attachZero(startTime.getMonth() + 1))];
    let startTimestamp = startTime.getTime();
    let endTimestamp = endTime.getTime();

    funcs.push(callback => {
        getSensors(callback);
    });

    funcs.push(callback => {
        store(callback);
    });

    async.series(funcs, (error, results) => {
        isSumming = false;
        if (error) {
            if (error === 'empty') {
                finish(204);
            } else {
                finish(400, error);
            }
        } else {
            finish(204);
        }
    });

    async function getSensors(callback) {
        try {
            sensors = await sequelize.models.Sensor.findAll({
                include: [{
                    model: sequelize.models.Sensor,
                    as: 'originSensor',
                    required: true
                }],
                where: {}
            });
            if (sensors.length) {
                callback(null, true);
            } else {
                callback('empty', false);
            }
        } catch (e) {
            console.error('sum-sensor-value getSensors', new Date(), e);
            callback(e, false);
        }
        return true;
    }

    async function store(callback) {
        try {
            for (let i=0; i<sensors.length; i++) {
                const sensor = sensors[i];
                let value = 0;
                for (let j=0; j<table_names.length; j++) {
                    const query = `SELECT COALESCE(SUM(value), 0) * 60 AS value FROM ${table_names}
                WHERE sensor_id = ${sensor.sensor_id} AND created_at > '${FILTER_UTIL.dateToDb(startTimestamp - 1)}' AND created_at < '${FILTER_UTIL.dateToDb(endTimestamp + 1)}'`;
                    try {
                        const sum = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        value = sum[0].value;
                    } catch (e) {
                        console.log(e);
                    }
                }
                await sequelize.models.Sensor.update({
                    value: (value / 10000).toFixed(2)
                }, {
                    where: {
                        id: sensor.id
                    }
                });
            }
            callback(null, true);
        } catch (e) {
            console.error('sum-sensor-value store', new Date(), e);
            callback(e, false);
        }
        return true;
    }
};
