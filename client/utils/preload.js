const preloadId = "lcPreloadWrap";

const stop = () => {
    const element = document.getElementById(preloadId);
    element.style.cssText = "opacity: 0;";
    setTimeout(() => {
        element.style.cssText = "opacity: 0; display: none;";
    }, 300);
};

export {
    stop
};

export default {
    stop
};
