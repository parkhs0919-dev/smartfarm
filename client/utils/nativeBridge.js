const NATIVE_GLOBAL_HASH = 'NATIVE_BRIDGE_' + new Date().getTime();

const nativeBridge = key => {
    function NativeBridge (key) {
        if (!window[NATIVE_GLOBAL_HASH]) window[NATIVE_GLOBAL_HASH] = {};
        if (!window.getNativeData) {
            window.getNativeData = function(key, value) {
                if (window[NATIVE_GLOBAL_HASH][key]) {
                    window[NATIVE_GLOBAL_HASH][key].value = value;
                } else {
                    console.warn('[NATIVE-BRIDGE] UNKNOWN WEB VIEW HANDLER');
                }
            };
        }
        addBridge(key);
    }

    NativeBridge.prototype.send = function (value = null) {
        if (window.NativeBridge) {
            window.NativeBridge.sendData(key, JSON.stringify(value));
        }
    };

    NativeBridge.prototype.listener = function (listener) {
        window[NATIVE_GLOBAL_HASH][key].listener(listener);
    };

    function addBridge(key) {
        if (window[NATIVE_GLOBAL_HASH][key]) {
            window[NATIVE_GLOBAL_HASH][key].value = console.warn('[NATIVE-BRIDGE] DUPLICATE DECLARE NATIVE BRIDGE, DESTROY ALREADY DECLARED');
            delete window[NATIVE_GLOBAL_HASH][key];
        }
        window[NATIVE_GLOBAL_HASH][key] = {
            localValue: '',
            localListener: function (value) {},
            set value(value) {
                this.localValue = value || '';
                this.localListener(value);
            },
            get value() {
                return this.localValue;
            },
            listener: function (listener) {
                this.localListener = listener;
            }
        };
    }

    return new NativeBridge(key);
};

export default nativeBridge;
