import React, {Component, Fragment} from 'react';
import screenManager from '../managers/screen';
import {useSocket} from "../contexts/socket";
import {useHouse} from "../contexts/house";
import {useAlert} from "../contexts/alert";
import {useNavScroll} from "../contexts/navScroll";
import deepcopy from "../../../utils/deepcopy";
import {addListener, on} from "../utils/socket";

const TRANSLATE = {
    cctv: "CCTV",
    motor: "모터제어",
    power: "전원제어",
    outerSensor: "외부센서",
    innerSensor: "내부센서",
    report: "영농일지"
};

const NAVI_NAME = {
    cctv: "CCTV",
    motor: "제어",
    power: "제어",
    outerSensor: "센서",
    innerSensor: "센서",
    report: "영농일지"
};

class MobileNav extends Component {
    constructor(props) {
        super(props);

        this.state = {
            screens: []
        };
    }
    componentDidMount() {
        this.findScreenStates();
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('screens');
    }

    findScreenStates = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId()
        };
        screenManager.findAll(query, (status, data) => {
            if(status === 200) {
                _this.setState({
                    screens: data.data.rows
                });
            } else {
                _this.props.alertError(status, data);
            }
        });
    };

    listener = (socket) => {
        const _this = this;
        on(socket)('screens', (data) => {
            if(data && data.house_id && data.house_id === _this.props.getHouseId()) {
                _this.findScreenStates();
            }
            if(!data || data.house_id === undefined) {
                _this.findScreenStates();
            }
        });
    };

    generateScreen = () => {
        let screens = deepcopy(this.state.screens);
        let screenArray = [];
        const naviScreenNames = {
            cctv: true,
            motor: true,
            power: true,
            outerSensor: true,
            innerSensor: true,
            report: true
        };
        screens.forEach((item) => {
            if(naviScreenNames[item.type]) {
                if(item.type === "motor") {
                    naviScreenNames.power = false;
                }
                if(item.type === "power") {
                    naviScreenNames.motor = false;
                }
                if(item.type === "outerSensor") {
                    naviScreenNames.innerSensor = false;
                }
                if(item.type === "innerSensor") {
                    naviScreenNames.outerSensor = false;
                }
                screenArray.push(item);
            }
        });
        return (
            <Fragment>
                {
                    screenArray.map((item, index) => {
                        return (
                            <li className="mobile-nav-list-item" key={index}>
                                <article className="mobile-nav-wrap" onClick={e => this.moveToContainer(e, item)}>
                                    <span className={`mobile-nav-name ${item.type}`}>{NAVI_NAME[item.type]}</span>
                                </article>
                            </li>
                        )
                    })
                }
            </Fragment>
        );
    };

    moveToContainer = (e, item) => {
        if(!item.is_visible) {
            return this.props.showDialog({
                alertText: `화면설정에서 ${TRANSLATE[item.type]}을<br>활성화 시켜주세요.`
            });
        }

        this.props.triggerScrollCallback(item.type);
    };

    render() {
        return (
            <nav id="mobileNav">
                <ul id="mobileNavList">
                    {this.generateScreen()}
                </ul>
            </nav>
        )
    }
}

export default useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useNavScroll(
    ({actions}) => ({
        triggerScrollCallback: actions.triggerScrollCallback
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(MobileNav))));