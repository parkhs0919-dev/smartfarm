import React, {Component} from 'react';
import {useAlert} from "../contexts/alert";
import {useSubSignUp} from "../contexts/modals/subSignUp";
import {useCCTVSetting} from "../contexts/modals/cctvSetting";
import {useScreenSetting} from "../contexts/modals/screenSetting";
import {useSensorSetting} from "../contexts/modals/sensorSetting";
import {useMotorSetting} from "../contexts/modals/motorSetting";
import {usePowerSetting} from "../contexts/modals/powerSetting";
import {useAccumulatedTemperature} from "../contexts/modals/accumulatedTemperature";
import {useSunDateDefaultSetting} from "../contexts/modals/sunDateDefaultSetting";
import {useHousePositionSetting} from "../contexts/modals/housePositionSetting";
import {useControlMinMaxRange} from "../contexts/modals/controlMinMaxRange";
import {usePBand} from "../contexts/modals/pBand";
import {usePBandSensorSetting} from "../contexts/modals/pBandSensorSetting";
import {usePanelSound} from "../contexts/modals/panelSound";

import {useuser} from '../contexts/modals/User'
import { useuserPasswd } from "../contexts/modals/UserPasswd";
import logoutManager from "../managers/logout";

import href from "../../../utils/href";
import {getQuery} from "../../../utils/route";

class Header extends Component {
    constructor(props) {
        super(props);
        
        this.isPanel = Object.keys(getQuery()).length && getQuery()['p'];
        this.state = {
            erpHost: window.erpHost,
            user: window.user,
            rightSideNavOpen: false,
            rightSideSettingMenuOpen: false,
            logoName:window.user ? (window.user.com_type?window.user.com_type:'farmlabs') : "farmlabs",
            grade : window.user ? (window.user.grade?window.user.grade:'member') : "member"
            
        };
    }

    toggleRightSideNav = () => {
        const _this = this;
        this.setState({
            rightSideNavOpen: !this.state.rightSideNavOpen
        }, () => {
            if(this.state.rightSideNavOpen) {
                document.addEventListener('keyup', _this.closeOnEscapeRightSideNav);
            } else {
                document.removeEventListener('keyup', _this.closeOnEscapeRightSideNav);
            }
        });
    };

    toggleRightSideMenu = () => {
        this.setState({
            rightSideSettingMenuOpen: !this.state.rightSideSettingMenuOpen
        });
    };

    closeOnEscapeRightSideNav = (e) => {
        if (e.keyCode === 27) this.setState({rightSideNavOpen: false});
    };

    openSignUpModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openSignUpModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    openuser = () => {

        this.props.openuser(null)                
    };

    openCCTVSettingModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openCCTVSettingModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    openScreenSettingModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openScreenSettingModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    openSensorSettingModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openSensorSettingModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    openMotorSettingModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openMotorSettingModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    openPowerSettingModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openPowerSettingModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    openAccumulatedTemperatureSettingModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openAccumulatedTemperatureModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    openSunDateSettingModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openSunDateDefaultSettingModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    openHousePositionSettingModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openHousePositionSettingModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    openControlMinMaxRangeModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openControlMinMaxRangeModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    openuserPasswdModal = () => {

        this.props.openuserPasswdModal(null)                
    };
    // openPBandSettingModal = () => {
    //     document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
    //     this.props.openPBandSettingModal(null, () => {
    //         document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
    //     });
    // };

    openPBandSensorSettingModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openPBandSensorSettingModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    openPanelSoundModal = () => {
        document.removeEventListener('keyup', this.closeOnEscapeRightSideNav);
        this.props.openPanelSoundModal(null, () => {
            document.addEventListener('keyup', this.closeOnEscapeRightSideNav);
        });
    };

    backToErp = (e, key) => {
        e.preventDefault();
        history.back();
        // href(this.state.erpHost + (key ? '/' + key : ''));
    };

    refresh = (e) => {
        e.preventDefault();
        location.reload();
    };

    logout = (e) => {
        const _this = this;
        e.preventDefault();
        this.props.show({
            alertText: "로그아웃 하시겠습니까?",
            cancelText: "취소",
            submitText: "로그아웃",
            submitCallback: () => {
                e.preventDefault();
                logoutManager.logout((status, data) => {
                    if (status === 200) {
                        if(this.state.logoName=='farmlabs'){
                            href(this.state.erpHost + '/login');
                        }else if(this.state.logoName=='kdn'){
                            href('http://kdn.farmlabs.co.kr/')
                        }
                    } else {
                        _this.props.alertError(status, data);
                    }
                });
            },
        })
    };

    render() {
        return (
            <header id="lcHeader">
                {
                    this.isPanel ? null :
                        <button type="button" id="lcMenuButton" onClick={e => this.backToErp(e, 'farm')}/>
                }
                <h1 id={"lcLogo_"+this.state.logoName} onClick={this.refresh}>
                    <span>팜랩스</span>
                </h1>
                <div id="lcMyInfoWrap" className={this.state.rightSideNavOpen ? "lc-active" : ""} onClick={this.toggleRightSideNav}>
                    <i/>
            <p> {this.state.user.name!=window.userInfo.farmName? '[ '+window.userInfo.farmName+' ]':null}  {this.state.user.name} 님</p>
                </div>
                <div id="rightSideNavOverlay"
                     onClick={this.toggleRightSideNav}
                     className={this.state.rightSideNavOpen ? "lc-active" : ""}/>
                <nav id="rightSideNav" className={this.isPanel ? "lc-panel-mode" : ""}>
                    <div className="right-side-nav-header">
                        <button id="closeSideNav" onClick={this.toggleRightSideNav}/>
                    </div>
                    <div className="side-nav-content-wrap">
                        <div className="user-profile-wrap">
                            <span id="userName">{this.state.user.name} 님</span>
                            {this.state.user.name!=window.userInfo.farmName?<span id="farmName">[{window.userInfo.farmName}]</span>:null}
                        </div>
                        {/* <ul id="rightSideSign">
                            <li onClick={this.openSignUpModal}>
                                <div className="right-side-nav-menu lc-has-menu">
                                    <span>서브 관리자 계정 추가</span>
                                </div>
                            </li>
                        </ul> */}
                        <ul id="rightSideUl">
                           
                            <li>
                                <div className={"right-side-nav-menu lc-has-menu" + (this.state.rightSideSettingMenuOpen ? " lc-active" : "")} onClick={this.toggleRightSideMenu}><span>설정</span></div>
                                <ul className={"right-side-child-ul" + (this.state.rightSideSettingMenuOpen ? " lc-active" : "")}>
                                    <li onClick={this.openScreenSettingModal}><span>화면 설정</span></li>
                                    <li onClick={this.openCCTVSettingModal}><span>CCTV 설정</span></li>
                                    <li onClick={this.openSensorSettingModal}><span>센서 설정</span></li>
                                    <li onClick={this.openMotorSettingModal}><span>모터 설정</span></li>
                                    <li onClick={this.openPowerSettingModal}><span>전원 설정</span></li>
                                    <li onClick={this.openAccumulatedTemperatureSettingModal}><span>적산온도 설정</span></li>
                                    <li onClick={this.openSunDateSettingModal}><span>일출일몰 시간설정</span></li>
                                    <li onClick={this.openHousePositionSettingModal}><span>풍상/풍하 설정</span></li>
                                    <li onClick={this.openControlMinMaxRangeModal}><span>최소/최대 개폐 기준 설정</span></li>
                                    <li onClick={this.openPBandSensorSettingModal}><span>P-밴드 센서 설정</span></li>
                                    <li onClick={this.openPanelSoundModal}><span>패널 소리 설정</span></li>

                                    {/*<li><span>서브계정 신청</span></li>*/}
                                </ul>
                                { this.state.grade=='owner'? <div className={"right-side-nav-menu"} onClick={this.openuser}><span>사용자 관리</span></div> :null}
                                { this.state.grade=='member'?<div className={"right-side-nav-menu"} onClick={this.openuserPasswdModal}><span>비밀번호 변경</span></div>:null}
                            </li>
                           
                            {/*<li>*/}
                                {/*<div className="right-side-nav-menu"><span>고장 및 문의</span></div>*/}
                            {/*</li>*/}
                            { this.state.user.id !== 'LOCAL' ?
                                <li onClick={this.logout}>
                                    <div className="right-side-nav-menu"><span>로그아웃</span></div>
                                </li> :
                                null
                            }
                        </ul>
                    </div>
                </nav>
            </header>
        );
    }
}

export default usePBand(
    ({actions}) => ({
        openPBandSettingModal: actions.open
    })
)(usePBandSensorSetting(
    ({actions}) => ({
        openPBandSensorSettingModal: actions.open
    })
)(useHousePositionSetting(
    ({actions}) => ({
        openHousePositionSettingModal: actions.open
    })
)(useSubSignUp(
    ({actions}) => ({
        openSignUpModal: actions.open
    })
)(useAccumulatedTemperature(
    ({actions}) => ({
        openAccumulatedTemperatureModal: actions.open
    })
)(useAlert(
    ({actions}) => ({
        show: actions.show,
        alertError: actions.alertError
    })
)(usePowerSetting(
    ({actions}) => ({
        openPowerSettingModal: actions.open
    })
)(useMotorSetting(
    ({actions}) => ({
        openMotorSettingModal: actions.open
    })
)(useSensorSetting(
    ({actions}) => ({
        openSensorSettingModal: actions.open
    })
)(useScreenSetting(
    ({actions}) => ({
        openScreenSettingModal: actions.open
    })
)(useCCTVSetting(
    ({actions}) => ({
        openCCTVSettingModal: actions.open
    })
)(useSunDateDefaultSetting(
    ({actions}) => ({
        openSunDateDefaultSettingModal: actions.open
    })
)(useControlMinMaxRange(
    ({actions}) => ({
        openControlMinMaxRangeModal: actions.open
    })
)(useuser(
    ({actions}) => ({
        openuser: actions.open
    })
)(usePanelSound(
    ({actions}) => ({
        openPanelSoundModal: actions.open
    })
)(useuserPasswd(
    ({actions}) => ({
        openuserPasswdModal: actions.open
    })
)(Header))))))))))))))));
