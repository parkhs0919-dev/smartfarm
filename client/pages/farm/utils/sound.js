function play(text) {
    var synth = window.speechSynthesis;
    let utter = new SpeechSynthesisUtterance();

    utter.text = text;
    utter.rate = 0.8;

    synth.speak(utter);
}

export default {
    play: play
};

export {
    play
};
