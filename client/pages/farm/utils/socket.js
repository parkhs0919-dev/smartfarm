import socketIOClient from 'socket.io-client';

let socketInstance = null;

function addListener(_this, listener) {
    if (_this.props.socket) {
        listener(_this.props.socket);
    } else if (socketInstance) {
        _this.props.setSocket(socketInstance);
        listener(socketInstance);
    } else {
        socketInstance = socketIOClient(location.host);
        _this.props.setSocket(socketInstance);
        listener(socketInstance);
    }
}

function on(socket) {
    return (key, callback) => {
        socket.on(key, callback);
    };
}

export default {
    addListener: addListener,
    on: on
};

export {
    addListener,
    on
};
