function highLightText(text) {
    return `<span class="lc-theme-color">${text}</span>`
}

function boldText(text) {
    return `<span class="lc-bold-text">${text}</span>`
}

export default {
    highLightText,
    boldText
}

export {
    highLightText,
    boldText
}