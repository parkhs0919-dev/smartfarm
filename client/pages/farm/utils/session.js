import href from '../../../utils/href';

const noSession = show => callback => (status, data) => {
    if (status === 401) {
        show({
            alertText: '다중 로그인 제한으로\n로그인이 해제됩니다.\n로그인 화면으로 이동합니다.',
            cancelCallback: () => {
                href(window.erpHost + '/login');
            }
        });
    } else {
        callback && callback(status, data);
    }
};

export {
    noSession
};

export default {
    noSession
};
