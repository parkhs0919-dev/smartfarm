import TRANSLATE from '../constants/translate';
import {attachZero} from "../../../utils/filter";
function returnTimeText(time) {
    let timeText = '';
    if (time !== null) {
        if (time >= 3600) {
            timeText += `${parseInt(time / 3600)}시간`;
            time = time % 3600;
        }
        if (time >= 60) {
            if(timeText.length) timeText = timeText + ' ';
            timeText += `${parseInt(time / 60)}분`;
            time = time % 60;
        }
        if (time) {
            if(timeText.length) timeText = timeText + ' ';
            timeText += ` ${time}초`
        }
        return timeText;
    }
}

function sunDateTimeToText(time) {
    let txt = '';
    if(time <= 0){
        txt += '- ';
    }else{
        txt += '+ ';
    }
    let startTime = Math.abs(time);

    txt += attachZero(parseInt(startTime / 60)) + ':' + attachZero((startTime % 60));

    return txt;
}


export default {
    returnTimeText,
    sunDateTimeToText
};

export {
    returnTimeText,
    sunDateTimeToText
};
