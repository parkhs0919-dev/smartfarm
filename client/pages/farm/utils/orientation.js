const checkOrientation = () => {
    if(isMobile()) {
        if (window.matchMedia("(orientation: portrait)").matches) {
            return "portrait";
        }

        if (window.matchMedia("(orientation: landscape)").matches) {
            return "landscape";
        }
    } else {
        return "pc";
    }
};

function isMobile() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

export {
    checkOrientation
}

export default {
    checkOrientation
}