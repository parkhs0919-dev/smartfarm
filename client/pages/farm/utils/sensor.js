const round = (value) => {
    if (parseInt(value) === parseFloat(value.toFixed(1))) {
        return parseInt(value);
    } else {
        return parseFloat(value.toFixed(1));
    }
};

const round_ec = (value) => {
    if (parseInt(value) === parseFloat(value.toFixed(2))) {
        return parseInt(value);
    } else {
        return parseFloat(value.toFixed(2));
    }
};

const sanitize = (sensor, outsideValue) => {
    let units = [];
    if (sensor.unit) {
        units = sensor.unit.split(':');
    }
   
    let value = outsideValue !== undefined ? outsideValue : sensor.value;
    switch(sensor.type) {
        case 'windDirection':
            if (units.length === 8) {
                for (let i=0; i<units.length; i++) {
                    if (i === 0) {
                        if (value >= 337.5 || value < 22.5) {
                            return units[i];
                        }
                    } else {
                        if (value >= 45 * i - 22.5 && value < 45 * i + 22.5) {
                            return units[i];
                        }
                    }
                }
                return `${value} ${sensor.unit}`;
            } else {
                return `${value} ${sensor.unit}`;
            }
        case 'korinsWindDirection':
            if (units.length === 8) {
                for (let i=0; i<units.length; i++) {
                    if (i === 0) {
                        if (value >= 337.5 || value < 22.5) {
                            return units[i];
                        }
                    } else {
                        if (value >= 45 * i - 22.5 && value < 45 * i + 22.5) {
                            return units[i];
                        }
                    }
                }
                return `${value} ${sensor.unit}`;
            } else {
                return `${value} ${sensor.unit}`;
            }
        case 'rain':
            if (units.length === 2) {
                if (value) {
                    return units[0];
                } else {
                    return units[1];
                }
            } else {
                return `${round(value)} ${sensor.unit}`;
            }
        case 'power':
            if (units.length === 2) {
                if (value) {
                    return units[0];
                } else {
                    return units[1];
                }
            } else {
                return `${round(value)} ${sensor.unit}`;
            }
        case 'water':
            if (units.length === 2) {
                if (value) {
                    return units[0];
                } else {
                    return units[1];
                }
            } else {
                return `${round(value)} ${sensor.unit}`;
            }
        case 'window':
                if (units.length === 2) {
                    if (value) {
                        return units[0];
                    } else {
                        return units[1];
                    }
                } else {
                    return `${round(value)} ${sensor.unit}`;
                }
        case 'fire':
                if (units.length === 2) {
                    if (value) {
                        return units[0];
                    } else {
                        return units[1];
                    }
                } else {
                    return `${round(value)} ${sensor.unit}`;
                }
        case 'door':
                if (units.length === 2) {
                    if (value) {
                        return units[0];
                    } else {
                        return units[1];
                    }
                } else {
                    return `${round(value)} ${sensor.unit}`;
                }
        case 'ec':
            return `${round_ec(value)} ${sensor.unit}`;
        default:
            return `${round(value)} ${sensor.unit}`;
    }
};



const returnSensorSelect = (sensor, isMinor) => {
    let units = [];
    let unit = sensor.unit ? sensor.unit : (sensor.sensor ? sensor.sensor.unit : null);
    if (unit) {
        units = unit.split(':');
    }
    let type = sensor.type ? sensor.type : sensor.sensor.type;

    if(type === 'windDirection' || type === 'korinsWindDirection') {
        return [
            {value: 0, label: units[0]},
            {value: 45, label: units[1]},
            {value: 90, label: units[2]},
            {value: 135, label: units[3]},
            {value: 180, label: units[4]},
            {value: 225, label: units[5]},
            {value: 270, label: units[6]},
            {value: 315, label: units[7]},
        ];
    }

    if(type === 'rain' || type === 'power' || type === 'water'||type==='window'||type==='fire'||type==='door') {

        return [
            {value: 0, label: units[1]},
            {value: 1, label: units[0]},
        ]
    } else {

        if(isMinor) {
            return [
                {value: 'over', label: '이상'},
                {value: 'under', label: '미만'},
            ];
        } else {
            return [
                {value: 'over', label: '이상'},
                {value: 'excess', label: '초과'},
                {value: 'below', label: '이하'},
                {value: 'under', label: '미만'},
                {value: 'equal', label: '같음'},
            ];
        }
    }
};

const returnSensorOption = (type, sensor) => {
    let units = [];
    let sensorUnit = sensor.unit ? sensor.unit : sensor.sensor.unit;
    let unit = sensor.unit ? sensor.unit : (sensor.sensor ? sensor.sensor.unit : null);
    let sensorValue = sensor.value;
    let sensorCondition = sensor.condition ? sensor.condition : sensor.sensor.condition;
    if (sensorUnit) {
        units = sensorUnit.split(':');
    }
    if(type === 'windDirection' ||type==='korinsWindDirection') {
        return {value: sensorValue, label: sensorUnit.split(':')[parseInt(sensorValue / 45)]};
    }

    if(type === 'rain' || type === 'power' || type === 'water'||type==='window'||type==='fire'||type==='door') {
        return {value: sensorValue, label: sensorValue === 1 ? units[0] : units[1]};
    } else {
        let conditions = {
            over: {value: 'over', label: '이상'},
            excess: {value: 'excess', label: '초과'},
            below: {value: 'below', label: '이하'},
            under: {value: 'under', label: '미만'},
            equal: {value: 'equal', label: '같음'},
        };

        return conditions[sensorCondition];
    }
};

const sanitize2 = (sensor) => {
    let units = [];
    if (sensor.unit) {
        units = sensor.unit.split(':');
    }

    let value = sensor.value ? sensor.value : 0
    switch(sensor.type) {        
        case 'ec':
           return `${round_ec(value)} ${sensor.unit}`;
        default:
           return `${round(value)} ${sensor.unit}`;
    }
};

const sanitizeText = (sensor, outsideValue) => {
    let units = [];
    if (sensor.sensor.unit) {
        units = sensor.sensor.unit.split(':');
    }
   
    let value = outsideValue !== undefined ? outsideValue : sensor.value;
    switch(sensor.sensor.type) {
        case 'windDirection':
            if (units.length === 8) {
                for (let i=0; i<units.length; i++) {
                    if (i === 0) {
                        if (value >= 337.5 || value < 22.5) {
                            return units[i];
                        }
                    } else {
                        if (value >= 45 * i - 22.5 && value < 45 * i + 22.5) {
                            return units[i];
                        }
                    }
                }
                return `${value} ${sensor.unit}`;
            } else {
                return `${value} ${sensor.unit}`;
            }
        case 'korinsWindDirection':
            if (units.length === 8) {
                for (let i=0; i<units.length; i++) {
                    if (i === 0) {
                        if (value >= 337.5 || value < 22.5) {
                            return units[i];
                        }
                    } else {
                        if (value >= 45 * i - 22.5 && value < 45 * i + 22.5) {
                            return units[i];
                        }
                    }
                }
                return `${value} ${sensor.unit}`;
            } else {
                return `${value} ${sensor.unit}`;
            }
        case 'rain':            
            if (units.length === 2) {
                if (value) {
                    return units[0];
                } else {
                    return units[1];
                }
            } else {
                return `${round(value)} ${sensor.unit}`;
            }
        case 'power':
            if (units.length === 2) {
                if (value) {
                    return units[0];
                } else {
                    return units[1];
                }
            } else {
                return `${round(value)} ${sensor.unit}`;
            }
        case 'water':
            if (units.length === 2) {
                if (value) {
                    return units[0];
                } else {
                    return units[1];
                }
            } else {
                return `${round(value)} ${sensor.unit}`;
            }
        case 'window':
                if (units.length === 2) {
                    if (value) {
                        return units[0];
                    } else {
                        return units[1];
                    }
                } else {
                    return `${round(value)} ${sensor.unit}`;
                }
        case 'fire':
                if (units.length === 2) {
                    if (value) {
                        return units[0];
                    } else {
                        return units[1];
                    }
                } else {
                    return `${round(value)} ${sensor.unit}`;
                }
        case 'door':
                if (units.length === 2) {
                    if (value) {
                        return units[0];
                    } else {
                        return units[1];
                    }
                } else {
                    return `${round(value)} ${sensor.unit}`;
                }
        case 'ec':
            return `${round_ec(value)} ${sensor.unit}`;
        default:
            return `${round(value)} ${sensor.unit}`;
    }
};
export {
    sanitize,
    sanitize2,
    sanitizeText,
    returnSensorSelect,
    returnSensorOption
};

export default {
    sanitize,
    sanitize2,
    sanitizeText,
    returnSensorSelect,
    returnSensorOption
};
