const THEME_COLOR = "#83c772";

const colourStyles = {
    control: (styles, state) => ({ ...styles,
        backgroundColor: 'white',
        borderColor: state.isFocused ? THEME_COLOR : "#ddd",
        boxShadow: state.isFocused ? null : null,
        "&:hover": {
            borderColor: THEME_COLOR,
            boxShadow: state.isFocused ? null : null
        }
    }),
    option: (styles, temp) => {
        const isDisabled = temp.isDisabled;
        const isFocused = temp.isFocused;
        const isSelected = temp.isSelected;
        return {
            ...styles,
            backgroundColor: isDisabled
                ? null
                : isSelected ? THEME_COLOR : isFocused ? 'rgba(131,199,114,0.1)' : null,
            color: isDisabled
                ? '#ccc'
                : isSelected
                    ? '#fff'
                    : '#000',
            cursor: isDisabled ? 'not-allowed' : 'pointer',
        };
    },
    input: styles => ({ ...styles}),
    placeholder: styles => ({ ...styles}),
    singleValue: (styles, { data }) => ({ ...styles}),
};

export {
    colourStyles,
}

export default {
    colourStyles: colourStyles,
}
