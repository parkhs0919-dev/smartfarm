const UNDER = "under";
const OVER = "over";
const EQUAL = "equal";
import {sanitize} from "./sensor";

function setValue(sensorData, data, alarmHash) {
    let stateData = JSON.parse(JSON.stringify(data));

    stateData.forEach(item => {
        delete item.isWarning;
    });

    if(sensorData) {
        let stateIds = stateData.map(item => {
            return item.id;
        });

        for(let i=0; i<sensorData.length; i++) {
            if(stateIds.indexOf(sensorData[i].id) !== -1) {
                stateData[stateIds.indexOf(sensorData[i].id)].value = sensorData[i].value;
            }
        }
    }
    // console.log(stateData, alarmHash);
    compareWithAlarms(stateData, alarmHash);
    return stateData;
}

function compareWithAlarms(stateData, alarmHash) {
    if(alarmHash !== undefined && stateData.length) {
        stateData.forEach((item, index) => {
            if(alarmHash[item.id]) {
                if(Array.isArray(alarmHash[item.id])) {
                    for(let i=0; i<alarmHash[item.id].length; i++) {
                        let isWarning = returnCalcByCondition(alarmHash[item.id][i], item);
                        if(isWarning) {
                            item.isWarning = isWarning;
                            break;
                        }
                    }
                } else {
                    item.isWarning = returnCalcByCondition(alarmHash[item.id], item);
                }
            }
        });
    }
}

function returnCalcByCondition(hashObj, stateObj) {

    if(hashObj.condition === OVER) {
        return hashObj.value <= stateObj.value;
    } else if (hashObj.condition === UNDER) {
        return hashObj.value > stateObj.value;
    } else if (hashObj.condition === EQUAL) {
        //hashObj의 value가 경보설정한 값, hashObj.sensor의 value가 현재 값
        return hashObj.value === hashObj.sensor.value;
    }
}

export default {
    setValue: setValue,
    compareWithAlarms: compareWithAlarms,
};

export {
    setValue,
    compareWithAlarms,
};
