import React from "react";
import {Route} from 'react-router-dom';

import Header from './layouts/Header';
import MobileNav from './layouts/MobileNav';

import Farm from './routes/Farm';
import ScreenSaver from './routes/ScreenSaver';

import Alert from './components/Alert';
import ModalJournal from './components/modals/Journal';
import ModalWeather from './components/modals/Weather';
import ModalSensorAlarmlist from './components/modals/SensorAlarmlist';
import ModalSensorAlarm from './components/modals/SensorAlarm';
import ModalSensorAlarmDetail from './components/modals/SensorAlarmDetail';
import NoticeDetail from './components/modals/NoticeDetail';
import ModalSensorGraph from './components/modals/SensorGraph';
import ModalIndividualControl from './components/modals/IndividualControl';
import ModalCCTV from './components/modals/CCTV';
import ModalCCTVSetting from './components/modals/CCTVSetting';
import ModalAutoControl from './components/modals/AutoControl';
import ModalAutoControlStep from './components/modals/AutoControlStep';
import ModalAutoControlDelayRecount from './components/modals/AutoControlDelayRecount';
import ModalAutoControlForm from './components/modals/AutoControlForm';
import ModalScreenSetting from './components/modals/ScreenSetting';
import ModalSensorSetting from './components/modals/SensorSetting';
import ModalMotorSetting from './components/modals/MotorSetting';
import ModalPowerSetting from './components/modals/PowerSetting';
import ModalSensorMainChart from './components/modals/SensorMainChart';
import ModalSensorTemperature from './components/modals/SensorTemperature';
import ModalAccumulatedTemperature from './components/modals/AccumulatedTemperature';
import ModalHousePositionSetting from './components/modals/HousePositionSetting';
import ModalSensorGather from './components/modals/SensorGather';
import ModalIntegratedGraph from './components/modals/IntegratedGraph';
import ModalAutoControlFileUpload from './components/modals/AutoControlFileUpload';
import ModalSunDateDefaultSetting from "./components/modals/SunDateDefaultSetting";
import ModalSensorHumi from './components/modals/SensorHumi';
import ModalSensorhumidityDeficit from './components/modals/sensorhumidityDeficit';
import Modalpesticide from './components/modals/pesticide';
import ModalpesticideDetail from './components/modals/pesticideDetail';
import Modalpesticideautomake from './components/modals/pesticideautomake';
import ModalSensorGroup from './components/modals/sensorgroup';
import ModalsensorGroupAdd from './components/modals/sensorgroupAdd';
import ModalsensorGroupDetail from './components/modals/sensorgroupDetail';
import ModalLiterAutoReset from './components/modals/LiterAutoReset'
import ModalWindDirectionSetting from './components/modals/WindDirectionSetting';
import ModalLiterDetail from './components/modals/LiterDetail'
import ModalSubSignUp from './components/modals/SubSignUp';
import ModalControlMinMaxRange from './components/modals/ControlMinMaxRange';
import ModalPBand from './components/modals/PBand';
import ModalPBandSensorSetting from './components/modals/PBandSensorSetting';
import ModalAutoControlTemperatureGraph from './components/modals/AutoControlTemperatureGraph';
import Modaluser from'./components/modals/User';
import ModaluserAdd from'./components/modals/UserAdd';
import ModaluserDetail from './components/modals/UserDetail';
import ModalPanelSoundSetting from './components/modals/PanelSoundSetting';
import ModalPanelSound from './components/modals/PanelSound';
import ModaluserPasswd from './components/modals/UserPasswd';
import ModalControlGroup from './components/modals/ControlGroup';
import ModalControlGroupSetting from './components/modals/ControlGroupSetting';
import {AlertProvider} from "./contexts/alert";
import {SocketProvider} from "./contexts/socket";
import {HouseProvider} from "./contexts/house";
import {CheckPanelProvider} from "./contexts/checkPanel";
import {RemoteProvider} from "./contexts/remote";
import {NavScrollProvider} from "./contexts/navScroll";
import {SunDateProvider} from "./contexts/sunDate";
import {PesticideautomakeProvider} from './contexts/modals/pesticideautomake'
import {SensorHumiProvider} from "./contexts/modals/SensorHumi"
import {LiterAutoResetProvider} from './contexts/modals/LiterAutoReset';
import {JournalProvider} from "./contexts/modals/journal";
import {WeatherProvider} from "./contexts/modals/weather";
import {SensorAlarmProvider} from "./contexts/modals/sensorAlarm";
import {SensorAlarmDetailProvider} from "./contexts/modals/sensorAlarmDetail";
import {SensorAlarmlistProvider} from "./contexts/modals/SensorAlarmlist";
import {NoticeDetailProvider} from "./contexts/modals/noticeDetail";
import {SensorGraphProvider} from "./contexts/modals/sensorGraph";
import {IndividualControlProvider} from "./contexts/modals/individualControl";
import {CCTVProvider} from "./contexts/modals/cctv";
import {CCTVSettingProvider} from "./contexts/modals/cctvSetting";
import {AutoControlProvider} from "./contexts/modals/autoControl";
import {AutoControlStepProvider} from "./contexts/modals/autoControlStep";
import {AutoControlDelayRecountProvider} from "./contexts/modals/autoControlDealyRecount";
import {AutoControlFormProvider} from "./contexts/modals/autoControlForm";
import {ScreenSettingProvider} from "./contexts/modals/screenSetting";
import {SensorSettingProvider} from "./contexts/modals/sensorSetting";
import {MotorSettingProvider} from "./contexts/modals/motorSetting";
import {PowerSettingProvider} from "./contexts/modals/powerSetting";
import {SensorMainChartProvider} from "./contexts/modals/sensorMainChart";
import {SensorTemperatureProvider} from "./contexts/modals/sensorTemperature";
import {AccumulatedTemperatureProvider} from "./contexts/modals/accumulatedTemperature";
import {HousePositionSettingProvider} from "./contexts/modals/housePositionSetting";
import {SensorGatherProvider} from "./contexts/modals/sensorGather";
import {IntegratedGraphProvider} from "./contexts/modals/integratedGraph";
import {AutoControlFileUploadProvider} from "./contexts/modals/autoControlFileUpload";
import {SunDateDefaultSettingProvider} from "./contexts/modals/sunDateDefaultSetting";
import {PesticideProvider} from './contexts/modals/pesticide'
import {PesticideDeatailProvider} from './contexts/modals/pesticideDeatail'
import {sensorhumidityDeficitProvider} from "./contexts/modals/sensorhumidityDeficit";
import {SubSignUpProvider} from "./contexts/modals/subSignUp";
import {ControlMinMaxRangeProvider} from "./contexts/modals/controlMinMaxRange";
import {PBandProvider} from "./contexts/modals/pBand";
import {PBandSensorSettingProvider} from "./contexts/modals/pBandSensorSetting";
import {SensorGroupProvider} from './contexts/modals/sensorgroup';
import {SensorGroupAddProvider} from './contexts/modals/sensorgroupAdd';
import {SensorGroupDetailProvider} from './contexts/modals/sensorgroupDetail';
import {LiterDetailProvider} from './contexts/modals/LiterDetail';
import {WindDirectionSettingProvider} from "./contexts/modals/windDirectionSetting";
import {AutoControlTemperatureGraphProvider} from "./contexts/modals/autoControlTemperatureGraph";
import {userProvider} from './contexts/modals/User';
import {userAddProvider} from './contexts/modals/UserAdd';
import {userDetailProvider} from './contexts/modals/Userdetail';
import {PanelSoundSettingProvider} from "./contexts/modals/panelSoundSetting";
import {PanelSoundProvider} from "./contexts/modals/panelSound";
import {userPasswdProvider} from './contexts/modals/UserPasswd';
import {ControlGroupProvider} from "./contexts/modals/controlGroup";
import {ControlGroupSettingProvider} from "./contexts/modals/controlGroupSetting";

const AppProvider = ({contexts, children}) => contexts.reduce(
    (prev, context) => React.createElement(context, {
        children: prev
    }),
    children
);

const App = () => {
    return (
        <AppProvider contexts={[
            SocketProvider,
            AlertProvider,
            HouseProvider,
            CheckPanelProvider,
            sensorhumidityDeficitProvider,
            RemoteProvider,
            NavScrollProvider,
            SunDateProvider,
            PesticideProvider,
            PesticideautomakeProvider,
            PesticideDeatailProvider,
            SensorHumiProvider,
            JournalProvider,
            WeatherProvider,
            SensorAlarmlistProvider,
            SensorAlarmProvider,
            SensorAlarmDetailProvider,
            NoticeDetailProvider,
            SensorGraphProvider,
            IndividualControlProvider,
            CCTVProvider,
            CCTVSettingProvider,
            AutoControlProvider,
            AutoControlStepProvider,
            AutoControlDelayRecountProvider,
            AutoControlFormProvider,
            ScreenSettingProvider,
            SensorSettingProvider,
            MotorSettingProvider,
            PowerSettingProvider,
            SensorMainChartProvider,
            SensorTemperatureProvider,
            AccumulatedTemperatureProvider,
            HousePositionSettingProvider,
            SensorGatherProvider,
            IntegratedGraphProvider,
            AutoControlFileUploadProvider,
            SunDateDefaultSettingProvider,
            SubSignUpProvider,
            ControlMinMaxRangeProvider,
            PBandProvider,
            PBandSensorSettingProvider,
            SensorGroupProvider,
            SensorGroupAddProvider,
            SensorGroupDetailProvider,
            WindDirectionSettingProvider,
            LiterAutoResetProvider,
            LiterDetailProvider,
            AutoControlTemperatureGraphProvider,
            userProvider,
            userAddProvider,
            userDetailProvider,
            PanelSoundSettingProvider,
            PanelSoundProvider,
            userPasswdProvider,
            ControlGroupProvider,
            ControlGroupSettingProvider,
        ]}>
            <Header/>
            <MobileNav/>
            <Modalpesticide/>
            <ModalpesticideDetail/>
            <Modalpesticideautomake/>
            <Alert/>
            <ModalJournal/>
            <ModalWeather/>
            <ModalSensorAlarmlist/>
            <ModalSensorAlarm/>
            <ModalSensorAlarmDetail/>
            <NoticeDetail/>
            <ModalIndividualControl/>
            <ModalCCTV/>
            <ModalCCTVSetting/>
            <ModalAutoControl/>
            <ModalAutoControlStep/>
            <ModalAutoControlDelayRecount/>
            <ModalAutoControlForm/>
            <ModalScreenSetting/>
            <ModalSensorSetting/>
            <ModalMotorSetting/>
            <ModalPowerSetting/>
            <ModalSensorMainChart/>
            <ModalSensorTemperature/>
            <ModalAccumulatedTemperature/>
            <ModalHousePositionSetting/>
            <ModalSensorGather/>
            <ModalIntegratedGraph/>
            <ModalAutoControlFileUpload/>
            <ModalSunDateDefaultSetting/>
            <ModalSensorHumi/>
            <ModalSensorhumidityDeficit/>
            <ModalSubSignUp/>
            <ModalControlMinMaxRange/>
            <ModalPBand/>
            <ModalPBandSensorSetting/>
            <ModalSensorGroup/>
            <ModalsensorGroupAdd/>
            <ModalsensorGroupDetail/>
            <ModalWindDirectionSetting/>
            <ModalLiterAutoReset/>
            <ModalLiterDetail/>
            <ModalAutoControlTemperatureGraph/>
            <Modaluser/>
            <ModaluserAdd/>
            <ModaluserDetail/>
            <ModaluserPasswd/>
            <ModalSensorGraph/>
            <ModalPanelSound/>
            <ModalPanelSoundSetting/>
            <ModalControlGroup/>
            <ModalControlGroupSetting/>
            <div id="lcMainWrap">
                <Route exact path="/farm" component={Farm}/>
                <Route exact path="/farm/screen" component={ScreenSaver}/>
            </div>
        </AppProvider>
    )
};

export default App;
