const returnToken = (key) => {
    const metas = document.getElementsByTagName('meta');
    for (let i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute('name') === key) {
            return metas[i].getAttribute('content');
        }
    }
};

export default {
    csrfToken: returnToken('csrf-token'),
    fileTypes: {
        JSON: "application/json"
    },
    checkRefreshPeriod: 10000,
    defaultModalState: {
        isVisible: false,
        callback: undefined,
        syncCallback: undefined
    },
    defaultLoadingState: {
        loadingKey: {}
    },
    defaultHouseHandlerState: {
        house_id: window.houses[0] ? window.houses[0].id : null,
        innerSensorCallback: undefined,
        outerSensorCallback: undefined,
    },
    defaultRemoteHandlerState: {
        house_id: window.houses[0] ? window.houses[0].id : null,
    },
    defaultModeState: {
        mode: window.mode.mode,
        motorCallback: undefined,
    },
    defaultSocketState: {
        socket: null
    },
    defaultNumberPageLength: 5,
    defaultLoadSize: 12,
    defaultAlertState: {
        isVisible: false,
        alertText: undefined,
        cancelClassName: undefined,
        cancelText: '확인',
        cancelCallback: undefined,
        cancelStyle: undefined,
        submitClassName: undefined,
        submitText: undefined,
        submitCallback: undefined,
        submitStyle: undefined
    },
    defaultIndividualControlState: {
        loading: false,
        form: {
            state: '',
            unit: '',
            value: ''
        },
        state: '',
        unit: '',
        value: ''
    },
    defaultJournalState: {
        journalContent: ''
    },
    noSelectReuquest : ['temperature','soilTemperature','humidity','soilHumidity','solar','windSpeed','ec','ph','co2','battery','liter','frostHumidity','frostTemperature','frost','dryTemperature','wetTemperature','co2Temperature','co2Humidity','humidityDeficit','korinsPressure','korinsFall','korinsHumidity','korinsTemperature','korinsWindSpeed','korinsSolar','sumOfTemperature','FolateTemperature'],
    noWindDirectionRequest: ['temperature','soilTemperature','humidity','soilHumidity','solar','windSpeed','ec','ph','co2','rain','power','water','battery','window','fire','door','korinsHumidity','korinsTemperature','korinsPressure','korinsFall','korinsWindSpeed','co2Humidity','co2Temperature','korinsSolar','sumOfTemperature','FolateTemperature','CultureMediumWeight','CultureMediumWaterWeight','sugar'],
    sensorType: {
        inner: "in",
        outer: "out",
        innerSensorType:['temperature','soilTemperature','humidity','soilHumidity','solar','ec','ph','co2','power','water','battery','window','fire','door','co2Humidity','co2Temperature','sumOfTemperature','FolateTemperature','CultureMediumWeight','CultureMediumWaterWeight','sugar','humidityDeficit','liter','frostHumidity','frostTemperature','frost','dryTemperature','wetTemperature','relativeHumidity','dewPointTemperature'],
        outerSensorType:['temperature','soilTemperature','humidity','soilHumidity','solar','windSpeed','ec','ph','co2','rain','power','water','battery','window','fire','door','korinsHumidity','korinsTemperature','korinsPressure','korinsInstantRainFall','korinsCumulativeRainFall','korinsWindSpeed','korinsWindDirection','co2Humidity','co2Temperature','korinsSolar','sumOfTemperature','FolateTemperature','CultureMediumWeight','CultureMediumWaterWeight','sugar','windDirection','humidityDeficit','liter','frostHumidity','frostTemperature','frost','dryTemperature','wetTemperature','relativeHumidity','dewPointTemperature'],
    },
    MOTOR: {
        modeAuto: "auto",
        modeManual: "manual",
        
    },
    weatherURL: "https://darksky.net/images/weather-icons/",
    DEFAULT_VALUES: {
        SENSOR_ALARMS: {
            form: {
                value: '',
            },
            alarm:{},
            alarms: [],
            sensorOptions: [],
            sensor_id: null,
            alarm_select:null,
            condition: null,
            firsttime:null,
            endtime:null,
            usefirst:null,
            useend:null,
            cardMode: 'ordinary',
            cycle:{value:'5',label:'05분'},
            alarm_name:'',
            repeat:{value:'1',label:'1번'},
        },

    },
    RETURN_CONTROL_OPTION: {
        power: {
            on: {label: "운전", value: "on"},
            off: {label: "정지", value: "off"},
        },
        powerPartial: {
            on: {label: "운전(예약 정지)", value: "on-partial", hasValue: true},
            off: {label: "정지(예약 운전)", value: "off-partial", hasValue: true},
        },
        powerUnitSec: {label: "초 단위", value: "sec"},
        powerUnitMin: {label: "분 단위", value: "min"},
        powerUnitHour: {label: "시간 단위", value: "hour"},
        motor: {
            open: {label: "전체 열기", value: "open"},
            close: {label: "전체 닫기", value: "close"},
        },
        motorPartial: {
            open: {label: "부분 열기", value: "open-partial", hasValue: true},
            close: {label: "부분 닫기", value: "close-partial", hasValue: true},
        },
        motorUnitTime: {label: "초 단위", value: "time"},
        motorUnitPercentage: {label: "% 단위", value: "percentage"},
        motorUnitPBand: {label: "P밴드", value: "pBand"},
        motorUnitPBandIntegral: {label: "P밴드+적분", value: "pBandIntegral"},
    },
    CONTROL_OPTIONS: {
        returnUnit: {
            percentage: "%",
            sec: "초",
            min: "분",
            hour: "시간",
            time: "초",
        },
        STATE: {
            motor: [
                {label: "전체 열기", value: "open"},
                {label: "전체 닫기", value: "close"},
                {label: "부분 열기", value: "open-partial", hasValue: true},
                {label: "부분 닫기", value: "close-partial", hasValue: true},
            ],
            power: [
                {label: "운전", value: "on"},
                {label: "정지", value: "off"},
                {label: "운전(예약 정지)", value: "on-partial", hasValue: true},
                {label: "정지(예약 운전)", value: "off-partial", hasValue: true},
            ],
            windDirection: [
                {label: "전체 열기", value: "open"},
                {label: "전체 닫기", value: "close"},
                {label: "부분 열기", value: "open-partial", hasValue: true},
                {label: "부분 닫기", value: "close-partial", hasValue: true},
            ]
        },
        INDIVIDUAL_STATE: {
            motor: [
                {label: "부분 열기", value: "open-partial", hasValue: true},
                {label: "부분 닫기", value: "close-partial", hasValue: true},
            ],
            power: [
                {label: "운전(예약 정지)", value: "on-partial", hasValue: true},
                {label: "정지(예약 운전)", value: "off-partial", hasValue: true},
            ],
            windDirection: [
                {label: "부분 열기", value: "open-partial", hasValue: true},
                {label: "부분 닫기", value: "close-partial", hasValue: true},
            ],
        },
        ALARM_STATE: {
            motor: [
                {label: "열림", value: "open", hasValue: true},
                {label: "닫힘", value: "close", hasValue: true},
                {label: "정지", value: "stop", hasValue: true},
            ],
            power: [
                {label: "운전", value: "on", hasValue: true},
                {label: "정지", value: "off", hasValue: true},
            ],
            windDirection: [
                {label: "열림", value: "open", hasValue: true},
                {label: "닫힘", value: "close", hasValue: true},
                {label: "정지", value: "stop", hasValue: true},
            ],
        },
        RETURN_ALARM_STATE : {
          open: '열림',
          close: '닫힘',
          stop: '정지',
          on: '운전',
          off: '정지'
        },
        WAY: {
            motor: [
                {value: "time", label: "초 단위"},
                {value: "percentage", label: "% 단위"},
            ],
            power: [
                {value: "sec", label: "초 단위"},
                {value: "min", label: "분 단위"},
                {value: "hour", label: "시간 단위"},
            ],
            windDirection: [
                {value: "time", label: "초 단위"},
                {value: "percentage", label: "% 단위"},
            ],
        },
        UNIT: {
            motor: [
                {label: "초 단위", value: "time"},
                {label: "% 단위", value: "percentage"},
            ],
            power: [
                {label: "초 단위", value: "sec"},
                {label: "분 단위", value: "min"},
                {label: "시간 단위", value: "hour"},
            ],
            pBand: [
                {label: "P밴드", value: "pBand"},
                {label: "P밴드+적분", value: "pBandIntegral"}
            ],
            windDirection:[
                {label: "초 단위", value: "time"},
                {label: "% 단위", value: "percentage"},
            ]
        },
        VALUE: {
            time: "초",
            percentage: "%",
            sec: "초",
            min: "분",
            hour: "시간"
        },
        RECOMMEND_CONTROL_STATE_UNIT : {
            'open' : {label: "전체 열기", value: "open"},
            'close' : {label: "전체 닫기", value: "close"},
            'open-partial' : {label: "부분 열기", value: "open-partial", hasValue: true},
            'close-partial' :{label: "부분 닫기", value: "close-partial", hasValue: true},
            'on' :{label: "운전", value: "on"},
            'off' :{label: "정지", value: "off"},
            'on-partial' :{label: "운전(예약 정지)", value: "on-partial", hasValue: true},
            'off-partial' :{label: "정지(예약 운전)", value: "off-partial", hasValue: true},
        },
        RECOMMEND_SENSOR_RANGE_OPTION : {

        }
    },
    DAYS: {
        is_fri: "금",
        is_mon: "월",
        is_sat: "토",
        is_sun: "일",
        is_thur: "목",
        is_tue: "화",
        is_wed: "수",
    },
    DAYS_FULL_NAME: {
        is_fri: "금요일",
        is_mon: "월요일",
        is_sat: "토요일",
        is_sun: "일요일",
        is_thur: "목요일",
        is_tue: "화요일",
        is_wed: "수요일",
    },
    canModifyChild: {
        cctv: true,
        control: false,
        motor: true,
        power: true,
        house: false,
        outerSensor: true,
        innerSensor: true,
        chart: false,
        notice: false,
        report: false,
        csv: false,
    },
    SPECIAL_SENSOR:{
        "windDirection": true,
        "power": true,
        "water": true,
        "rain": true,
        "window":true,
        "fire":true,
        "door":true,
        "korinsWindDirection":true
    },
    SENSOR_ALARM_SOUND : {
        etc : "/public/sounds/etc.mp3",
        over : "/public/sounds/over.mp3",
        under : "/public/sounds/under.mp3",
        rainon : "/public/sounds/rainon.mp3",
        rainoff :"/public/sounds/rainoff.mp3"
    },
    defaultAlarm:{
        Type:[
            {value:'range',label:'센서 범위 설정'},
            {value:'error',label:'센서 이상 동작'}
        ],
        Time:[
            {value:'0',label:'00시'},
            {value:'1',label:'01시'},
            {value:'2',label:'02시'},
            {value:'3',label:'03시'},
            {value:'4',label:'04시'},
            {value:'5',label:'05시'},
            {value:'6',label:'06시'},
            {value:'7',label:'07시'},
            {value:'8',label:'08시'},
            {value:'9',label:'09시'},
            {value:'10',label:'10시'},
            {value:'11',label:'11시'},
            {value:'12',label:'12시'},
            {value:'13',label:'13시'},
            {value:'14',label:'14시'},
            {value:'15',label:'15시'},
            {value:'16',label:'16시'},
            {value:'17',label:'17시'},
            {value:'18',label:'18시'},
            {value:'19',label:'19시'},
            {value:'20',label:'20시'},
            {value:'21',label:'21시'},
            {value:'22',label:'22시'},
            {value:'23',label:'23시'},
        ],
        cycle:[
            {value:'1',label:'01분'},
            {value:'2',label:'02분'},
            {value:'3',label:'03분'},
            {value:'4',label:'04분'},
            {value:'5',label:'05분'},
            {value:'6',label:'06분'},
            {value:'7',label:'07분'},
            {value:'8',label:'08분'},
            {value:'9',label:'09분'},
            {value:'10',label:'10분'},
            {value:'11',label:'11분'},
            {value:'12',label:'12분'},
            {value:'13',label:'13분'},
            {value:'14',label:'14분'},
            {value:'15',label:'15분'},
            {value:'16',label:'16분'},
            {value:'17',label:'17분'},
            {value:'18',label:'18분'},
            {value:'19',label:'19분'},
            {value:'20',label:'20분'},
            {value:'21',label:'21분'},
            {value:'22',label:'22분'},
            {value:'23',label:'23분'},
            {value:'24',label:'24분'},
            {value:'25',label:'25분'},
            {value:'26',label:'26분'},
            {value:'27',label:'27분'},
            {value:'28',label:'28분'},
            {value:'29',label:'29분'},
            {value:'30',label:'30분'},
            {value:'31',label:'31분'},
            {value:'32',label:'32분'},
            {value:'33',label:'33분'},
            {value:'34',label:'34분'},
            {value:'35',label:'35분'},
            {value:'36',label:'36분'},
            {value:'37',label:'37분'},
            {value:'38',label:'38분'},
            {value:'39',label:'39분'},
            {value:'40',label:'40분'},
            {value:'41',label:'41분'},
            {value:'42',label:'42분'},
            {value:'43',label:'43분'},
            {value:'44',label:'44분'},
            {value:'45',label:'45분'},
            {value:'46',label:'46분'},
            {value:'47',label:'47분'},
            {value:'48',label:'48분'},
            {value:'49',label:'49분'},
            {value:'50',label:'50분'},
            {value:'51',label:'51분'},
            {value:'52',label:'52분'},
            {value:'53',label:'53분'},
            {value:'54',label:'54분'},
            {value:'55',label:'55분'},
            {value:'56',label:'56분'},
            {value:'57',label:'57분'},
            {value:'58',label:'58분'},
            {value:'59',label:'59분'},
            {value:'60',label:'60분'},
        ],
        repeat:[
            {value:'1',label:'1번'},
            {value:'2',label:'2번'},
            {value:'3',label:'3번'},
            {value:'4',label:'4번'},
            {value:'5',label:'5번'},
            {value:'6',label:'6번'},
            {value:'7',label:'7번'},
            {value:'8',label:'8번'},
            {value:'9',label:'9번'},
            {value:'10',label:'10번'},
        ]

    },
    defaultpesticide:{
        area:[
            {value:'1',label:'1구역',type:'one',order:1,is_visible:true},
            {value:'2',label:'2구역',type:'two',order:2,is_visible:true},
            {value:'3',label:'3구역',type:'three',order:3,is_visible:true},
            {value:'4',label:'4구역',type:'four',order:4,is_visible:true},
            {value:'5',label:'5구역',type:'five',order:5,is_visible:true},
            {value:'6',label:'6구역',type:'six',order:6,is_visible:true},
            {value:'7',label:'7구역',type:'seven',order:7,is_visible:true},
            {value:'8',label:'8구역',type:'eight',order:8,is_visible:true},
        ],
        canModifyChild: {
            one: false,
            two: false,
            three: false,
            four: false,
            five: false,
            six: false,
            seven: false,
            eight: false,
        },
    },
    sensorGroup:{
        Type:[
            {value:'temperature',label:'온도',unit:'℃'},
            {value:'humidity',label:'습도',unit:'%'},
            {value:'windSpeed',label:'풍속',unit:'m/s'},
            {value:'solar',label:'일사량',unit:'W/㎡'},            
            {value:'co2',label:'CO2',unit:'ppm'},   
            {value:'ph',label:'PH',unit:'pH'},
            {value:'ec',label:'EC',unit:'dS/㎡'},
            {value:'soilTemperature',label:'지온',unit:'℃'},
            {value:'soilHumidity',label:'지습',unit:'%'},
            {value:'liter',label:'유량계',unit:'L'},
            {value:'frost',label:'노점',unit:'%'},
            {value:'dryTemperature',label:'건구온도',unit:'℃'},
            {value:'wetTemperature',label:'습구온도',unit:'℃'},
            {value:'humidityDeficit',label:'수분부족분',unit:'g/m3'},
            {value:'korinsPressure',label:'기압',unit:'hpa'},
            {value:'korinsFall',label:'강우량',unit:'mm/hr'}
        ],
    },
    Liter:{
        Time:[
            {value:'0',label:'00시'},
            {value:'1',label:'01시'},
            {value:'2',label:'02시'},
            {value:'3',label:'03시'},
            {value:'4',label:'04시'},
            {value:'5',label:'05시'},
            {value:'6',label:'06시'},
            {value:'7',label:'07시'},
            {value:'8',label:'08시'},
            {value:'9',label:'09시'},
            {value:'10',label:'10시'},
            {value:'11',label:'11시'},
            {value:'12',label:'12시'},
            {value:'13',label:'13시'},
            {value:'14',label:'14시'},
            {value:'15',label:'15시'},
            {value:'16',label:'16시'},
            {value:'17',label:'17시'},
            {value:'18',label:'18시'},
            {value:'19',label:'19시'},
            {value:'20',label:'20시'},
            {value:'21',label:'21시'},
            {value:'22',label:'22시'},
            {value:'23',label:'23시'},
            {value:'24',label:'24시'},
        ],
    },
    TABLE_CONTROL : {
        selectedPeriod : null,
        is_using_period : {label : '미사용', value : false},
        selectedTimeType : {value : "single", label : "고정"},
        selectedTimeCondition : {value : "+", label : "+"},
        selectedStartHour : null,
        selectedStartMinute : null,
        isUseExpectTemperatureControl : false,
        expect_temperature : '',
        rise_time: '',
        drop_time: '',
        expect_temperature_max_value:'',
        expect_temperature_min_value:'',
        tableSensors: [{
            selectedAutoControlSensor : null,
            sensor : null,
            sensor_id : '',
            sensorUnit : '',
            max_value : '',
            min_value : '',
            percentage : '',
        }],
    },
    selectTimeWay : [
        {value: "noSelect", label: "선택 안함"},
        {value: "single", label: "특정 시간"},
        {value: "range", label: "시간 범위"},
        {value: "sunDate", label: "일출 일몰 시간 범위"}
    ],
    selectSunDateTypes : [
        {value: "rise", label: "일출 기준"},
        {value: "set", label: "일몰 기준"},
    ],
    selectTableStartTimeTypes : [
        {value: "rise", label: "일출 기준"},
        {value: "set", label: "일몰 기준"},
        {value: "single", label: "고정"}
    ],
    selectPlusMinus : [
        {value: "plus", label: "+"},
        {value: "minus", label: "-"}
    ],
    selectControlConditions : {
        motor: [
            {value: 'over', label: '이상'},
            {value: 'excess', label: '초과'},
            {value: 'below', label: '이하'},
            {value: 'under', label: '미만'},
            {value: 'equal', label: '같음'},
        ],
        power: [
            {value: 1, label: '켜짐'},
            {value: 0, label: '꺼짐'}
        ],
        windDirection: [
            {value: 'over', label: '이상'},
            {value: 'excess', label: '초과'},
            {value: 'below', label: '이하'},
            {value: 'under', label: '미만'},
            {value: 'equal', label: '같음'},
        ],
    },
    actionCountOptions : [
        {value: 'infinite', label: "계속 실행"},
        {value: 'limit', label: "실행횟수 입력"}
    ],
    delayWayOptions : [
        {value: 'noDelay', label: "1분 타이머"},
        {value: 'delay', label: "지연시간 입력"},
        {value: 'immediately', label: "즉시 실행"},
    ],
    selectDayTypeOption : [
        {value: 'day', label: "요일별"},
        {value: 'per', label: "기간별"},
    ],
    selectDateOption : [
        {value: 'is_mon', label: '월요일'},
        {value: 'is_tue', label: '화요일'},
        {value: 'is_wed', label: '수요일'},
        {value: 'is_thur', label: '목요일'},
        {value: 'is_fri', label: '금요일'},
        {value: 'is_sat', label: '토요일'},
        {value: 'is_sun', label: '일요일'},
    ],
    const_days : {
        is_fri: "금요일",
        is_mon: "월요일",
        is_sat: "토요일",
        is_sun: "일요일",
        is_thur: "목요일",
        is_tue: "화요일",
        is_wed: "수요일",
    }
};

