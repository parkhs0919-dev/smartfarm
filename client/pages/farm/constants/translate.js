export default {
    JOURNAL: {
        motor: "모터제어",
        power: "전원제어",
        control: "모드설정",
        warning: "경보",
        error: "시스템알림",
        report: "영농일지",
        auto: "자동제어",
        noti: "시스템알림"
    },
    SENSOR: {
        inner: "내부",
        outer: "외부",
        condition: {
            over: '이상',
            excess: '초과',
            below: '이하',
            under: '미만',
            equal: '같음',
        },
        open: "부분열기",
        close: "부분닫기",
        on: "운전(예약 정지)",
        off: "정지(예약 운전)",
    },
    SCREEN: {
        cctv: "CCTV",
        control: "제어부",
        motor: "모터제어",
        power: "전원제어",
        house: "하우스 & 날씨",
        outerSensor: "외부센서",
        innerSensor: "내부센서",
        chart: "통합그래프",
        notice: "공지사항",
        report: "영농일지",
        csv: "엑셀 다운로드",
        pesticide: "방제기",
        fluid : "양액기",
        innerSensorAvg:'내부 복수 센서 평균',
        outerSensorAvg:'외부 복수 센서 평균',
    },
    DIRECTION: {
        N: "북",
        NNE: "북북동",
        NE: "북동",
        ENE: "동북동",
        E: "동",
        ESE: "동남동",
        SE: "남동",
        SSE: "남남동",
        S: "남",
        SSW: "남남서",
        SW: "남서",
        WSW: "서남서",
        W: "서",
        WNW: "서북서",
        NW: "북서",
        NNW: "북북서",
    },
    MOTOR_DIRECTION: {
        left: "좌측",
        right: "우측",
        none: "선택 없음",
    },
    WINDOW_POSITION: {
        1: "1중",
        2: "2중",
        3: "3중",
    },
    WIND_DIRECTION : {
        forward : '풍상창',
        backward : '풍하창'
    },
    TIME_TYPE : {
        rise : "일출 기준",
        set : "일몰 기준",
        single : "고정"
    },
    TIME_CONDITION :{
        plus : "+",
        minus :"-"
    }
};
