import React, {Component, Fragment} from 'react';
import {useSocket} from "../contexts/socket";
import {useAlert} from "../contexts/alert";
import {useCCTV} from "../contexts/modals/cctv";
import {useHouse} from "../contexts/house";
import cctvManager from '../managers/cctv';

import Pagination from '../components/Pagination';
import {addListener, on} from "../utils/socket";
import {getQuery} from "../../../utils/route";
import {useNavScroll} from "../contexts/navScroll";
import {noSession} from "../utils/session";
import deepcopy from "../../../utils/deepcopy";
import Spinner from "../components/Spinner";

const CCTV_STATE_ON = 'on';
const CCTV_STATE_OFF = 'off';

class Cctv extends Component {
    constructor(props) {
        super(props);

        let query = location.search.substr(1);
        let queryArr = query.split("&");
        this.isNoCCTV = queryArr.indexOf("c") !== -1;
        this.closeCCTVInterval = null;
        this.cctvRef = React.createRef();
        this.players = [];
        this.state = {
            loading: false,
            modalItem: null,
            switch: CCTV_STATE_OFF,
            innerSwitch: CCTV_STATE_OFF,
            cctvs: [],
            cctvCount: 0,
            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            offset: 0,
            size: 4,
        };
    }

    componentDidMount() {
        this.props.setCCTVCallback(this.changeHouseId);
        this.props.setCCTVScrollCallback(this.scrollCallback);
        this.searchPage(0);
        this.getCCTVSetting();
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        clearInterval(this.closeCCTVInterval);
        this.closeCCTVInterval = null;
        const socket = this.props.socket;
        socket.removeListener('cctvs');
        this.closeSockets();
    }

    listener = (socket) => {
        const _this = this;
        on(socket)('cctvs', () => {
            _this.searchPage(_this.state.offset);
        });
        on(socket)('cctv-setting', () => {
            _this.getCCTVSetting();
        });
    };

    scrollCallback = (refName) => {
        // console.log(this[REF_NAMES[refName]]);
        // this[REF_NAMES[refName]].current.scrollIntoView({behavior: 'smooth'});
        this.cctvRef.current.scrollIntoView({behavior: "smooth"});
    };

    closeSockets = (callback) => {
        let players = this.players;
        this.players = [];
        players.forEach(({player}) => {
            player.source.socket.close();
            player.destroy();
        });
        players = null;
        if (callback) callback();
    };

    getCCTVSetting = () => {
        const _this = this;
        cctvManager.findCCTVSetting(noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    is_auto_close: data.data.is_auto_close
                }, () => {
                    _this.searchPage(_this.state.offset);
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    closeAutomatically = () => {
        const _this = this;
        this.closeCCTVInterval = setInterval(() => {
            if (_this.players && _this.players.length) {
                _this.closeSockets(() => {
                    this.setState({
                        switch: CCTV_STATE_OFF,
                        innerSwitch: CCTV_STATE_OFF,
                    });
                });
            }
            clearInterval(_this.closeCCTVInterval);
        }, (1000 * 60 * 5));
    };

    openSockets = async (exceptCCTV) => {
        const _this = this;
        this.closeSockets();
        let modalItem = null;

        await this.setState({
            loading: true
        });

        if(exceptCCTV) {
            this.state.cctvs.forEach((cctv) => {
                if (exceptCCTV.port === cctv.port) {
                    modalItem = deepcopy(cctv);
                }
            });
        }

        let promises = [];
        this.state.cctvs.forEach((cctv) => {
            if (!(exceptCCTV && exceptCCTV.port === cctv.port)) {
                promises.push(_this.cctvStart(cctv));
            }
        });
        await Promise.all(promises);

        await this.setState({
            modalItem: modalItem
        });

        await setTimeout(() => {
            _this.setState({loading: false});
        }, 1500);
    };

    cctvStart = cctv => {
        const _this = this;
        return new Promise((resolve, reject) => {
            cctvManager.cctvStart(cctv.id, noSession(_this.props.showDialog)((status, data) => {
                if (status === 200) {
                    const canvas = _this.refs[`canvas${cctv.id}`];
                    const player = new JSMpeg.Player(`ws://${location.hostname}:${cctv.port}/`, {canvas, disableWebAssembly: true, videoBufferSize: 1024 * 1024});
                    _this.players.push({
                        player
                    });
                } else {
                    _this.props.alertError(status, data);
                }
                resolve(true);
            }));
        });
    };

    handleCCTVState = (e, state) => {
        const _this = this;
        if(this.isNoCCTV) {
            return this.props.showDialog({
                alertText: "패널에서는 재생할 수 없습니다."
            });
        }
        if (state === CCTV_STATE_ON) {
            this.setState({
                switch: CCTV_STATE_ON,
                innerSwitch: CCTV_STATE_ON
            }, () => {
                _this.openSockets();
                if(_this.state.is_auto_close) {
                    _this.closeAutomatically();
                }
            });
        } else if (state === CCTV_STATE_OFF) {
            this.setState({
                switch: CCTV_STATE_OFF,
                innerSwitch: CCTV_STATE_OFF
            }, () => {
                _this.closeSockets();
            });
        }
    };

    changeHouseId = (house_id) => {
        const _this = this;
        this.setState({
            switch: CCTV_STATE_OFF,
            innerSwitch: CCTV_STATE_OFF
        }, () => {
            _this.closeSockets(() => {
                _this.setState({house_id}, () => {
                    _this.searchPage(0);
                });
            });
        })
    };

    searchPage = (offset) => {
        const _this = this;
        _this.closeSockets(() => {
            _this.setState({
                offset: offset
            });

            let query = {
                house_id: this.state.house_id,
                offset: offset,
                size: this.state.size
            };

            cctvManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
                if(status === 200) {
                    _this.setState({
                        cctvs: data.data.rows,
                        cctvCount: data.data.count
                    }, () => {
                        if(_this.state.switch === CCTV_STATE_ON) {
                            _this.setState({
                                innerSwitch: CCTV_STATE_OFF
                            }, () => _this.setState({
                                innerSwitch: CCTV_STATE_ON
                            }, () => {
                                _this.openSockets();
                                if(_this.state.is_auto_close) {
                                    _this.closeAutomatically();
                                }
                            }));
                        }
                    });
                } else if (status === 404) {
                    _this.setState({
                        cctvs: [],
                        cctvCount: 0
                    });
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        });
    };

    openCCTVModal = (e, cctv) => {
        const _this = this;

        if(this.state.modalItem) {
            return this.props.showDialog({
                alertText: "열려있는 CCTV창을 닫고 실행해주세요."
            });
        }

        this.setState({loading: true}, () => {
            setTimeout(() => {
                _this.setState({loading: false});
            }, 1000);
        });
        this.props.openCCTVModal(cctv, () => {
            _this.setState({
                modalItem: null
            }, () => {
                _this.setState({loading: true}, () => {
                    setTimeout(() => {
                        _this.setState({loading: false});
                    }, 1000);
                });
                if(this.state.switch === CCTV_STATE_ON) {
                    _this.closeSockets(() => {
                        this.setState({
                            innerSwitch: CCTV_STATE_OFF
                        }, () => _this.setState({
                            innerSwitch: CCTV_STATE_ON,
                        }, () => {
                            _this.openSockets();
                        }));
                    });
                }
            });
        });
        if(this.state.switch === CCTV_STATE_ON) {
            _this.closeSockets(() => {
                this.setState({
                    innerSwitch: CCTV_STATE_OFF
                }, () => _this.setState({
                    innerSwitch: CCTV_STATE_ON,
                }, () => {
                    _this.openSockets(cctv);
                }));
            });
        }
    };

    renderCCTVS = (cctv) => {

        if(this.state.modalItem) {
            if(this.state.modalItem.id === cctv.id) {
                return <div className="no-data-wrap"/>;
            } else {
                return this.state.switch === CCTV_STATE_ON && this.state.innerSwitch === CCTV_STATE_ON ? <canvas ref={`canvas${cctv.id}`}/> : <div className="no-data-wrap"/>;
            }
        } else {
            return this.state.switch === CCTV_STATE_ON && this.state.innerSwitch === CCTV_STATE_ON ? <canvas ref={`canvas${cctv.id}`}/> : <div className="no-data-wrap"/>;
        }
    };

    render() {
        return (
            <Fragment>
                {
                    this.state.cctvs && this.state.cctvs.length ?
                        (
                            <section className="lc-card" id="lcCctvWrap" ref={this.cctvRef}>
                                <div className="lc-card-header">
                                    <h2 className="lc-card-title">CCTV</h2>
                                    <div className="toggle-radio-wrap two-state">
                                        <input type="radio" name="cctv-toggle" value={CCTV_STATE_ON} onChange={e => {}} checked={this.state.switch === CCTV_STATE_ON}/>
                                        <label onClick={e => this.handleCCTVState(e, CCTV_STATE_ON)}>켜기</label>
                                        <input type="radio" name="cctv-toggle" value={CCTV_STATE_OFF} onChange={e => {}} checked={this.state.switch === CCTV_STATE_OFF}/>
                                        <label onClick={e => this.handleCCTVState(e, CCTV_STATE_OFF)}>끄기</label>
                                        <i className="toggle-slider-icon"/>
                                    </div>
                                </div>
                                <article id="lcCctvContentWrap">
                                    <div id="device-wrap">
                                        <Spinner spinning={this.state.loading} size="big" isDark={true}/>
                                        <ul>
                                            {
                                                this.state.cctvs.map((cctv, index) => {
                                                    return (
                                                        <li key={index} className={this.props.isPanel ? 'lc-mode-panel' : ''}>
                                                            <div className="aspect-ratio-wrap">
                                                                <div className="cctv-wrap">
                                                                    {
                                                                        this.renderCCTVS(cctv)
                                                                    }
                                                                </div>
                                                            </div>
                                                            <div className="cctv-info-wrap">
                                                                <span className="cctv-name">{cctv.cctv_name}</span>
                                                                <div>
                                                                    {cctv.type === "ptz" ?
                                                                        <button className="open-ptz-cctv-btn"/> : null
                                                                    }
                                                                    <button className="open-cctv-modal-btn" onClick={e => this.openCCTVModal(e, cctv)}/>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    );
                                                })
                                            }
                                        </ul>
                                    </div>
                                </article>
                                { this.state.cctvCount && (this.state.cctvCount > this.state.size) ?
                                    <Pagination count={this.state.cctvCount}
                                                offset={this.state.offset}
                                                size={this.state.size}
                                                searchPage={this.searchPage}/>
                                    : null
                                }
                            </section>
                        ) : null
                }
            </Fragment>
        );
    }
}

export default useNavScroll(
    ({actions}) => ({
        setCCTVScrollCallback: actions.setCCTVScrollCallback
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useCCTV(
    ({actions}) => ({
        openCCTVModal: actions.open
    })
)(useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId,
        setCCTVCallback: actions.setCCTVCallback
    })
)(Cctv)))));
