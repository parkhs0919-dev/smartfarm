import React, { Component, Fragment } from 'react';
import { useAlert } from "../contexts/alert";
import { useSocket } from "../contexts/socket";
import { useHouse } from "../contexts/house";
import { useSensorAlarmlist } from "../contexts/modals/SensorAlarmlist";
import { useSensorGraph } from "../contexts/modals/sensorGraph";
import { useNavScroll } from "../contexts/navScroll";
import { useSensorMainChart } from "../contexts/modals/sensorMainChart";
import { useSensorTemperature } from "../contexts/modals/sensorTemperature";
import { usesensorhumidityDeficit } from '../contexts/modals/sensorhumidityDeficit';
import { addListener, on } from "../utils/socket";
import { setValue } from "../utils/sensorHandler";
import { sanitize } from "../utils/sensor";
import { useSensorHumi } from "../contexts/modals/SensorHumi"
import { useSensorGroup } from '../contexts/modals/sensorgroup'
import {useLiterAutoReset} from '../contexts/modals/LiterAutoReset'
import { useLiterDetail } from "../contexts/modals/LiterDetail";
import Pagination from '../components/Pagination';
import sensorManager from '../managers/sensor';
import CONSTANT from "../constants/constant";
import alarmManager from "../managers/alarm";
import Spinner from "../components/Spinner";
import LcChart from "../components/Chart";
const INNER_SENSOR_POSITION = "in";
import sensorMainChartManager from '../managers/sensorMainChart';
import { noSession } from "../utils/session";

const TYPE_TEMP = "temperature";
const TYPE_LITER = "liter";
const TYPE_HUMI = "humidity";
const TYPE_HUMIDITYDEFICIT = "humidityDeficit";
const SPECIAL_SENSOR_TYPES = CONSTANT.SPECIAL_SENSOR
const INNER_SENSOR_TYPE = CONSTANT.sensorType.innerSensorType

class InnerSensor extends Component {
    constructor(props) {
        super(props);
       
        this.innerSensorRef = React.createRef();
        this.alarmHash = {};
        this.state = {
            chartData: [],
            sensorMainChart: window.sensorMainChart,
            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            innerSensors: [],
            innerSensorCount: 0,
            innerAlarms: [],
            innerAlarmCount: 0,
            offset: 0,
            size: CONSTANT.defaultLoadSize,
            sensorZoom: true,
            selectedSensorOption: '',
            grade : window.user ? (window.user.grade?window.user.grade:'member') : "member"
        };
        
    }

    componentDidMount() {
        this.searchPage(0);
        this.findSensorCharts();
        const chartInterval = setInterval(() => this.findSensorCharts(), (60 * 1000));
        this.setState({ chartInterval });
        this.props.setInnerSensorCallback(this.changeHouseId);
        
        this.props.setInnerSensorScrollCallback(this.scrollCallback);
        addListener(this, this.listener);
        this.getAlarms();
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('inner-sensor-values');
        socket.removeListener('inner-alarm');
        socket.removeListener('sensors');
        socket.removeListener('sensor-main-chart');
        clearInterval(this.state.chartInterval);
    };

    listener = (socket) => {
        const _this = this;
        on(socket)('inner-sensor-values', ({ innerSensors }) => {
            _this.setState({
                innerSensors: setValue(innerSensors, _this.state.innerSensors, _this.alarmHash)
            })
        });
        on(socket)('sensors', () => {
            _this.searchPage(this.state.offset);
        });
        on(socket)('inner-alarm', () => {
            _this.getAlarms();
        });
        on(socket)('sensor-main-chart', (data) => {
            if (data.house_id === _this.state.house_id) {
                _this.findMainSensorChart();
            }
        });
    };

    changeHouseId = (house_id) => {        
        const _this = this;
     
        this.setState({ house_id }, () => {
            _this.searchPage(0);
            _this.findMainSensorChart();
            _this.getAlarms();
        });
    };

    findMainSensorChart = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId()
        };
        sensorMainChartManager.findMainSensorChart(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                
                _this.setState({
                    sensorMainChart: data.data
                }, () => {
                    _this.findSensorCharts();
                });
            } else if (status === 404) {
                _this.setState({
                    sensorMainChart: null
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    searchPage = (offset) => {
        
        const _this = this;
        this.setState({
            offset: offset
        });

        let query = {
            house_id: this.state.house_id,
            position: INNER_SENSOR_POSITION,
            offset: offset,
            size: this.state.size,
            types:INNER_SENSOR_TYPE
        };

        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    innerSensors: setValue(null, data.data.rows, _this.alarmHash),
                    innerSensorCount: data.data.count
                });
            } else if (status === 404) {
                _this.setState({
                    innerSensors: [],
                    innerSensorCount: 0
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    openSensorAlarmModal = (e) => {
        if (this.state.innerSensorCount === 0) {
            this.props.showDialog({
                alertText: "설정 가능한 센서가 없습니다."
            })
        } else {
            this.props.openSensorAlarmModal({
                key: "inner",
                house_id: this.state.house_id
            });
        }
    };

    openSensorGraphModal = (e, item) => {
        
        if (item.type === TYPE_TEMP) {
            this.props.showDialog({
                alertText: "기능을 선택해주세요.",
                cancelText: "그래프 보기",
                cancelCallback: () => {
                    this.props.openSensorGraphModal(item);
                },
                submitText: "온도상세 보기",
                submitCallback: () => {
                    this.props.openSensorTemperature(item);
                }
            });
        } else if (item.type === TYPE_LITER) {
            let body={item:item,house_id:this.state.house_id}
            // this.props.showDialog({
            //     alertText: "기능을 선택해주세요.",
            //     cancelText: "그래프 보기",
            //     cancelCallback: () => {
            //         this.props.openSensorGraphModal(item);
            //     },
            //     submitText: "유량계 초기화",
            //     submitCallback: () => {
            //         this.clearLiter(item);
            //     }
            // });
            this.props.showDialog({
                alertText: "기능을 선택해주세요.",
                cancelText: "자동 리셋 설정",
                cancelCallback: () => {
                    this.props.openLiterAutoReset(body);
                },
                submitText: "상세 설정",
                submitCallback: () => {
                    this.props.openLiterDetail(body);
                    
                }
            });
        } else if (item.type === TYPE_HUMI) {
            this.props.showDialog({
                alertText: "기능을 선택해주세요.",
                cancelText: "그래프 보기",
                cancelCallback: () => {
                    this.props.openSensorGraphModal(item);
                },
                submitText: "습도상세 보기",
                submitCallback: () => {
                    this.props.openuseSensorHumi(item);
                }
            });
        } else if (item.type === TYPE_HUMIDITYDEFICIT) {
            let check = false;
            for (let i = 0; i < this.state.innerSensors.length; i++) {
                if (item.address == this.state.innerSensors[i].address) {
                    if (this.state.innerSensors[i].type == 'dryTemperature') {
                        check = true;
                        break;
                    }
                }
            }
            if(check==true){
                this.props.showDialog({
                    alertText: "기능을 선택해주세요.",
                    cancelText: "그래프 보기",
                    cancelCallback: () => {
                        this.props.openSensorGraphModal(item);
                    },
                    
                    submitText: "상세 보기",
                    submitCallback: () => {                        
                        this.props.openusesensorhumidityDeficit(item);                       
                    }
                });
            }else{
                this.props.openSensorGraphModal(item);
            }

        } else {
            this.props.openSensorGraphModal(item);
        }

    };

    clearLiter = (item) => {
        const _this = this;
        sensorManager.resetLiter(item.id, (status, data) => {
            if (status === 200) {
                _this.props.showDialog({
                    alertText: "초기화되었습니다."
                })
            } else {
                _this.props.alertError(status, data);
            }
        });
    };

    scrollCallback = () => {
        this.innerSensorRef.current.scrollIntoView({ behavior: "smooth" });
    };

    getAlarms = () => {
        const _this = this;
        let query = {
            house_id: this.state.house_id,
            position: "in"
        };
        alarmManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.generateAlarmsToHash(data.data.rows, (hash) => {
                    let innerSensors = setValue(null, _this.state.innerSensors, hash);
                    _this.setState({
                        innerSensors: innerSensors,
                        innerAlarmCount: data.data.count,
                        innerAlarms: data.data.rows
                    });
                });
            } else if (status === 404) {
                _this.setState({
                    innerAlarmCount: 0,
                    innerAlarms: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    generateAlarmsToHash = (alarms, callback) => {
        let hash = {};
        if (alarms.length) {
            for (let i = 0; i < alarms.length; i++) {
                if (hash[alarms[i].sensor_id] === undefined) {
                    hash[alarms[i].sensor_id] = alarms[i];
                } else {
                    if (Array.isArray(hash[alarms[i].sensor_id])) {
                        hash[alarms[i].sensor_id].push(alarms[i]);
                    } else {
                        let temp = Object.assign({}, hash[alarms[i].sensor_id]);
                        hash[alarms[i].sensor_id] = [];
                        hash[alarms[i].sensor_id].push(temp);
                        hash[alarms[i].sensor_id].push(alarms[i]);
                    }
                }
            }
        }
        this.alarmHash = hash;
        if (callback) callback(hash);
    };

    findSensorCharts = () => {
        const _this = this;
        if (this.state.sensorMainChart) {
            let query = {
                sensor_id: this.state.sensorMainChart.id,
                house_id: this.state.house_id,
                isDate: true
            };

            this.setState({
                chartData: []
            });

            sensorManager.findSensorChart(query, noSession(this.props.showDialog)((status, data) => {
                if (status === 200) {
                    _this.setState({
                        chartData: data.data
                    }, () => {
                        setTimeout(() => {
                            _this.setState({
                                loading: false
                            })
                        }, 1000)
                    });
                } else if (status === 404) {
                    _this.setState({
                        chartData: null
                    })
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        }
    };

    generateSubTitle = () => {
        let str = '';
        if (!SPECIAL_SENSOR_TYPES[this.state.sensorMainChart.type]) {
            str = `(${this.state.sensorMainChart.unit})`;
        }

        str += " - 24시간 기준";
        return str;
    };

    openMainSensorSettingModal = (e) => {
        this.props.openSensorMainChartModal();
    };
    openSensorGroupModal =(e)=>{
        let body = {
            position:'inner',
            house_id:this.state.house_id,
        }
        this.props.openSensorGroupModal(body)
    }
    sensorChartZoom = () => {
        const _this = this;
        
        let query = {
            house_id: this.props.getHouseId(),
            sensor_id: this.state.sensorMainChart.id,
        };
        if(this.state.sensorZoom) {
            this.setState({sensorZoom: false});
            sensorMainChartManager.setMainSensorChart(query, noSession(this.props.showDialog)((status, data) => {
                if(status === 200) {
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        }
        if(!this.state.sensorZoom) {
            this.setState({sensorZoom: true});
            sensorMainChartManager.setMainSensorChart(query, noSession(this.props.showDialog)((status, data) => {
                if(status === 200) {
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        }
    };

    render() {
        return (
            <Fragment>
                {
                    this.state.innerSensors && this.state.innerSensors.length ?
                        (
                            <section className="lc-card" id="lcInnerSensorWrap" ref={this.innerSensorRef}>
                                <div className="lc-card-header">
                                    <h2 className="lc-card-title">내부센서</h2>
                                    
                                    <button className="card-header-button zoom-graph" onClick={() => this.sensorChartZoom()}>{this.state.sensorZoom ? "세로확대보기" : "가로확대보기"}</button>
                                    {this.state.grade=='owner'?<button className="card-header-button" style={{marginRight:'10px'}} onClick={this.openSensorGroupModal}><span>센서그룹 생성</span></button>:null}
                                    {this.state.grade=='owner'?<button className="card-header-button" onClick={this.openSensorAlarmModal}><span>경보설정</span><span className="number-badge">{this.state.innerAlarmCount}</span></button>:null}
                                </div>

                                <div className="lc-card-content-wrap">
                                    <div className="graph-item-wrap">
                                        <Spinner spinning={this.state.loading} />
                                        <button className="theme-text-button" onClick={e => this.openMainSensorSettingModal(e)}>대표센서 설정</button>
                                        {
                                            this.state.chartData && this.state.sensorMainChart ?
                                                (
                                                    <Fragment>
                                                        {
                                                            this.state.chartData.length ? (
                                                                <LcChart chartId="sensorGraphContainer"
                                                                    noChartDetailControl={true}
                                                                    isMain={true}
                                                                    sensorZoom={this.state.sensorZoom}
                                                                    chartSubTitle={this.generateSubTitle()}
                                                                    chartData={this.state.chartData}
                                                                    chartType={this.state.sensorMainChart.type}
                                                                    sensorData={this.state.sensorMainChart} />
                                                            ) : null
                                                        }
                                                    </Fragment>
                                                ) :
                                                (
                                                    <div className="no-chart-data-wrap">
                                                        <p>해당 조건에 그래프 정보가 없습니다.</p>
                                                    </div>
                                                )
                                        }
                                    </div>

                                    {this.state.innerSensors.length ?
                                        <ul className="sensor-list">
                                            {
                                                this.state.innerSensors.map((item, index) => {
                                                    return (
                                                        <li className="sensor-li" key={index}>
                                                            <article className={`lc-content-card lc-sensor-${item.type}`} onClick={e => this.openSensorGraphModal(e, item)}>
                                                                <p className="content-title">{item.sensor_name}</p>
                                                                <p className={item.isWarning ? "content-value lc-warning" : "content-value"}>{sanitize(item)}</p>
                                                            </article>
                                                        </li>
                                                    )
                                                })
                                            }
                                        </ul> : null
                                    }
                                    {!this.state.innerSensors.length ?
                                        <p>데이터가 없습니다.</p> : null
                                    }
                                </div>

                                {this.state.innerSensorCount && (this.state.innerSensorCount > this.state.size) ?
                                    <Pagination count={this.state.innerSensorCount}
                                        offset={this.state.offset}
                                        size={this.state.size}
                                        searchPage={this.searchPage} />
                                    : null
                                }
                            </section>
                        ) : null
                }
            </Fragment>
        )
    }
}

export default useSensorTemperature(
    ({ actions }) => ({
        openSensorTemperature: actions.open
    })
)(useSensorHumi(
    ({ actions }) => ({
        openuseSensorHumi: actions.open
    })
)(usesensorhumidityDeficit(
    ({ actions }) => ({
        openusesensorhumidityDeficit: actions.open
    })
)(useSensorMainChart(
    ({ actions }) => ({
        openSensorMainChartModal: actions.open
    })
)(useNavScroll(
    ({ actions }) => ({
        setInnerSensorScrollCallback: actions.setInnerSensorScrollCallback
    })
)(useSensorGraph(
    ({ actions }) => ({
        openSensorGraphModal: actions.open
    })
)(useSensorAlarmlist(
    ({ actions }) => ({
        openSensorAlarmModal: actions.open
    })
)(useSensorGroup(
    ({ actions }) => ({
        openSensorGroupModal: actions.open
    })
)(useLiterAutoReset(
    ({ actions }) => ({
        openLiterAutoReset: actions.open
    })
)(useLiterDetail(
    ({ actions }) => ({
        openLiterDetail: actions.open
    })
)(useHouse(
    ({ actions }) => ({
        setInnerSensorCallback: actions.setInnerSensorCallback,
        getHouseId: actions.getHouseId,
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSocket(
    ({ state, actions }) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(InnerSensor)))))))))))));
