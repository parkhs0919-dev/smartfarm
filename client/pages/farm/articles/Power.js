import React, {Component, Fragment} from 'react';
import {useSocket} from "../contexts/socket";
import {useHouse} from "../contexts/house";
import {useAlert} from "../contexts/alert";
import {useRemote} from "../contexts/remote";
import {useIndividualControl} from "../contexts/modals/individualControl";
import {useAutoControl} from "../contexts/modals/autoControl";
import {useNavScroll} from "../contexts/navScroll";
import {addListener, on} from "../utils/socket";
import Pagination from '../components/Pagination';
import constant from '../constants/constant';
import Spinner from '../components/Spinner';

import controlManager from '../managers/control';
import modeManager from "../managers/mode";
import autoControlManager from '../managers/autoControl';
import {noSession} from "../utils/session";
import {getQuery} from "../../../utils/route";
import {play} from '../utils/sound';
import controlAlarmManager from "../managers/controlAlarm";
import {useControlGroup} from "../contexts/modals/controlGroup";
import deepcopy from "../../../utils/deepcopy";
import controlGroupManager from "../managers/controlGroup";
import controlGroupActiveManager from "../managers/controlGroupActive";
import Select from "react-select";
import {colourStyles} from "../utils/select";

const TYPE_POWER = 'power';
const MODE_AUTO = constant.MOTOR.modeAuto;
const MODE_MANUAL = constant.MOTOR.modeManual;

class Power extends Component {
    constructor(props) {
        super(props);

        this.moreMenuRefs = new Map();
        this.powerRef = React.createRef();
        this.isOpened = false;
        this.state = {
            remoteState: props.getRemoteState(),
            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            mode: window.mode.mode,
            powers: [],
            powerCount: 0,
            offset: 0,
            size: constant.defaultLoadSize,
            loading: {},
            autoControlCount: 0,
            powerControlGroups : [],
            selectedPowerControlGroup : null,
            powerGroupLoading : false,

        };
    }

    componentDidMount() {
        this.findControlGroups();
        this.searchPage(0);
        this.findAutoControlCount();
        this.props.setPowerCallback(this.changeHouseId);
        this.props.setRemotePowerCallback(this.remotePowerCallback);
        this.props.setPowerScrollCallback(this.scrollCallback);
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('control-state');
        socket.removeListener('power');
        socket.removeListener('powers');
        socket.removeListener('mode');
        socket.removeListener('auto-controls');
        socket.removeListener('control-group');
        socket.removeListener('control-groups');
    }

    shouldComponentUpdate(nextProps, nextState){
        let query = getQuery();
        if(query.modal === "auto-control" && nextProps.autoControlSyncCallback && !this.isOpened) {
            this.isOpened = true;
            this.props.openAutoControlModal({step: 1, house_id: this.state.house_id});
        }
        return true;
    }

    listener = (socket) => {
        const _this = this;
        on(socket)('power', ({power}) => {
            let powers = JSON.parse(JSON.stringify(_this.state.powers));
            for (let i = 0; i < powers.length; i++) {
                if (powers[i].id === power.id) {
                    powers[i] = power;
                    _this.loading(i, false);
                    _this.setState({powers});
                    _this.playSound(power);
                    break;
                }
            }
        });
        on(socket)('powers', (data) => {
            if(data && data.house_id) {
                if(this.state.house_id == data.house_id) {
                    _this.searchPage(_this.state.offset);
                }
            } else {
                _this.searchPage(_this.state.offset);
            }
        });

        on(socket)('mode', ({mode}) => {
            _this.setState({
                mode: mode.mode
            });
        });
        on(socket)('auto-controls', (data) => {
            if(data && data.house_id) {
                if(this.state.house_id == data.house_id) {
                    _this.findAutoControlCount();
                    _this.searchPage(_this.state.offset);
                }
            } else {
                _this.findAutoControlCount();
                _this.searchPage(_this.state.offset);
            }
        });

        on(socket)('control-group', (data) => {
            //상태 변경
            if(data && data.controlGroup.type === 'power') {
                let selectedPowerControlGroup = null;
                if(_this.state.selectedPowerControlGroup) {
                    selectedPowerControlGroup = deepcopy(_this.state.selectedPowerControlGroup);

                    if(selectedPowerControlGroup.value === parseInt(data.controlGroup.id)) {
                        selectedPowerControlGroup.state = data.controlGroup.state;
                        selectedPowerControlGroup.origin.state = data.controlGroup.state;
                        _this.setState({selectedPowerControlGroup});
                    }
                }else {

                }
            }
        });

        on(socket)('control-groups', (data) => {
            //데이터 갱신
            if(data){
                _this.findControlGroups();
            }
        });

        on(socket)('control-group-active-start', (data) => {
            if(data && data.house_id === _this.state.house_id){
                _this.setState({
                    powerGroupLoading : !_this.state.powerGroupLoading
                })
            }
        });

        on(socket)('control-group-active-end', (data) => {
            if(data && data.house_id === _this.state.house_id) {
                _this.setState({
                    powerGroupLoading : !_this.state.powerGroupLoading
                })
            }
        });

    };

    findControlGroups = () => {
        const _this = this;

        let where = {
            house_id : this.props.getHouseId(),
            type : TYPE_POWER,
            on_off_state : 'on'
        };

        controlGroupManager.findAll(where, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    powerControlGroups : _this.generatePowerControlGroups(data.data.rows),
                });
            }else if(status === 404) {
                _this.setState({
                    powerControlGroups : []
                })
            } else {
                _this.props.alertError(status, data);
            }

        }));
    };

    generatePowerControlGroups = (list) => {
        let powerControlGroups = [];
        list.forEach((data, index) => {
            powerControlGroups.push({
                value : data.id,
                label : data.control_group_name,
                state : data.state,
                origin : data,
            });
        });

        return powerControlGroups;
    };

    playSound = (power) => {
        controlAlarmManager.findById(power.id, noSession(this.props.showDialog)((status, data) => {
            if(status === 200){
                const control = data.data;
                if(control && control.controlAlarms.length){
                    control.controlAlarms.forEach((item, index) => {
                        if(power.state == item.state){
                            play(item.text);
                        }
                    });
                }
            }
        }));
    };

    remotePowerCallback = (remoteState) => {
        this.setState({remoteState});
    };

    findAutoControlCount = () => {
        const _this = this;
        let query = {
            house_id: this.state.house_id,
            type: TYPE_POWER,
            state: 'on'
        };
        autoControlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    autoControlCount: data.data.count
                });
            } else if (status === 404) {
                _this.setState({
                    autoControlCount: 0
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    changeHouseId = (house_id) => {
        const _this = this;
        this.setState({house_id}, () => {
            _this.searchPage(0);
            _this.findAutoControlCount();
            _this.findControlGroups();
        });
    };

    scrollCallback = (refName) => {
        // console.log(this[REF_NAMES[refName]]);
        // this[REF_NAMES[refName]].current.scrollIntoView({behavior: 'smooth'});
        this.powerRef.current.scrollIntoView({behavior: "smooth"});
    };

    searchPage = (offset) => {
        const _this = this;
        this.setState({
            offset: offset
        });

        let query = {
            house_id: this.state.house_id,
            type: TYPE_POWER,
            offset: offset,
            size: this.state.size
        };

        this.setState({
            powers: [],
            powerCount: 0
        });

        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    loading: {},
                    powers: data.data.rows,
                    powerCount: data.data.count
                });
            } else if (status === 404) {
                _this.setState({
                    loading: {},
                    powers: [],
                    powerCount: 0
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    clickLabel = (e, val, power, index) => {
        e.preventDefault();
        const _this = this;

        let currentState = power.state;
        if (currentState === val) {
            return false;
        }

        let body = {
            id: power.id,
            state: val,
        };
        this.loading(index, true);
        controlManager.active(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                _this.loading(index, false);
                _this.props.alertError(status, data);
            }
        }));
    };

    clickLabelGroup = (e, val, power) => {
        if(power){
            e.preventDefault();
            const _this = this;
            let currentState = power.state;

            if (currentState === val) {
                return false;
            }

            let body = {
                id: power.value,
                state: val,
            };

            controlGroupActiveManager.active(body, noSession(this.props.showDialog)((status, data) => {
                if (status !== 200) {
                    // _this.loading(index, false);
                    _this.props.alertError(status, data);
                }
            }));
        }
    };

    releaseLoadingByTimeout = (key) => {
        let temp = Object.assign({}, this.state.loading);
        const _this = this;
        if (temp[key]) {
            setTimeout(() => {
                temp[key] = false;
                _this.setState({
                    loading: temp
                });
            }, 2500);
        }
    };

    loading = (index, isLoading) => {
        let loading = Object.assign({}, this.state.loading);
        if (isLoading) {
            loading[index] = true;
            this.releaseLoadingByTimeout(index);
        } else {
            delete loading[index];
        }
        this.setState({loading});
    };

    calculatePercentage = (power) => {
        return parseInt(power.current / power.range * 100);
    };

    openIndividualControlModal = (e, item, index) => {
        // const ref = this.moreMenuRefs.get(index);
        // ref.blur();
        this.props.openIndividualControlModal(item, this.individualSettingCallback);
    };

    individualSettingCallback = () => {
        console.log("설정하고나서 조히다시해서 카운트바꾸어? 수동이면 그럴필요없나?");
    };

    returnModeString = (mode) => {
        if (mode === MODE_AUTO) {
            return "자동";
        } else if (mode === MODE_MANUAL) {
            return "수동";
        }
    };

    returnBooleanByModeState = (mode) => {
        if (mode === MODE_AUTO) {
            return false;
        } else if (mode === MODE_MANUAL) {
            return true;
        }
    };

    returnPowerOnOffMode = (power) => {
        if (power.mode === MODE_AUTO) {
            return MODE_MANUAL;
        } else if (power.mode === MODE_MANUAL) {
            return MODE_AUTO;
        }
    };

    openAutoControlModal = (step, e, item) => {
        const _this = this;

        if(step === 1) {
            return _this.props.openAutoControlModal({step: step, type: TYPE_POWER, house_id: this.state.house_id});
        } else if (step === 2) {
            if(item !== undefined) {
                return _this.props.openAutoControlModal({
                    step: step,
                    control_id: item.id
                });
            }
        }
        return _this.props.openAutoControlModal({step: step});
    };

    clickPowerStateLabel = (e, index) => {
        e.preventDefault();
        const _this = this;

        let powers = JSON.parse(JSON.stringify(this.state.powers));
        let power = powers[index];
        let mode = this.returnPowerOnOffMode(power);
        let body = {};
        body.id = power.id;
        body.mode = mode;
        // if(mode === "auto" && this.state.remoteState === "field") {
        //     return this.props.showDialog({
        //         alertText: "자동제어는 원격모드 '원격' 에서만 가능합니다."
        //     });
        // }
        modeManager.controlMode(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                _this.props.alertError(status, data);
            }
        }));
    };

    openControlGroupModal = (e) => {
        const _this = this;
        return _this.props.openControlGroupModal({type: TYPE_POWER, house_id: this.state.house_id});
    };

    handleControlGroup = (value, key) => {
        const _this = this;
        _this.setState({selectedPowerControlGroup : value});
    };

    render() {
        return (
            <Fragment>
                {
                    this.state.powerControlGroups && this.state.powerControlGroups.length ?
                        <section className="lc-card" id="lcControlMotorWrap">
                            <Spinner spinning={this.state.powerGroupLoading}/>
                            <div className="lc-card-header">
                                <h2 className="lc-card-title">전원그룹제어</h2>
                            </div>
                            <div className="lc-card-content-wrap">
                                <article className="lc-control-group">
                                    <div className="select-module-wrap">
                                        <Select
                                            value={this.state.selectedPowerControlGroup}
                                            placeholder="전원그룹 선택"
                                            onChange={value => this.handleControlGroup(value, 'selectedPowerControlGroup')}
                                            isSearchable={false}
                                            options={this.state.powerControlGroups}
                                            styles={colourStyles}/>
                                    </div>

                                    <div className="absolute-item-wrap">
                                        <div
                                            className={"toggle-radio-wrap two-state lc-fat" + (!this.state.selectedPowerControlGroup ? " lc-disabled-all" : "")}>
                                            <div className="disabled-wrap"/>
                                            <input type="radio" name={"control-group-power-toggle-option"}
                                                   id={"controlGroupToggleFirst"}
                                                   onChange={e => {}}
                                                   checked={this.state.selectedPowerControlGroup && this.state.selectedPowerControlGroup.state === 'on' || false}/>

                                            <input type="radio" name={"control-group-power-toggle-option"}
                                                   id={"controlGroupToggleSecond"}
                                                   onChange={e => {}}
                                                   checked={(!this.state.selectedPowerControlGroup ||  this.state.selectedPowerControlGroup && this.state.selectedPowerControlGroup.state === 'off') || false}/>
                                            <label
                                                onClick={(e) => this.clickLabelGroup(e, 'on', this.state.selectedPowerControlGroup)}>운전</label>
                                            <label
                                                onClick={(e) => this.clickLabelGroup(e, 'off', this.state.selectedPowerControlGroup)}>정지</label>
                                            <i className="toggle-slider-icon"/>
                                        </div>
                                    </div>
                                </article>
                            </div>

                        </section> : null
                }
                {
                    this.state.powers && this.state.powers.length ?
                        (
                            <section className="lc-card" id="lcPowerWrap" ref={this.powerRef}>
                                <div className="lc-card-header">
                                    <h2 className="lc-card-title">전원제어</h2>
                                    <button className="card-header-button group-btn" onClick={e => this.openControlGroupModal(e)}>전원 그룹제어 설정</button>
                                    <button className="card-header-button" onClick={e => this.openAutoControlModal(1, e)}><span>전원 자동제어</span><span className="number-badge">{this.state.autoControlCount}</span></button>
                                </div>

                                <div className="lc-card-content-wrap">
                                    {this.state.powers.length ?
                                        <ul className="lc-power-list">
                                            {this.state.powers.map((power, index) => {
                                                return (
                                                    <li key={index}>
                                                        <article className="lc-content-card">
                                                            <Spinner spinning={this.state.loading[index]}/>

                                                            <div className="content-position-wrap">

                                                                <div className={"power-content-wrap" + (this.state.mode !== "individual" ? " lc-non-individual-mode" : "")}>
                                                                    <div className="power-text-count-wrap" onClick={e => this.openAutoControlModal(2, e, power)}>
                                                                        <p className="power-text">{power.control_name}</p>
                                                                        <span className="power-count">{power.autoControlItemCount + power.autoControlStepCount}</span>
                                                                    </div>

                                                                    <div className="flex-line-break"/>

                                                                    <div className="power-dialog-percentage-wrap">
                                                                        <div className="lc-toggle-wrapper">

                                                                            <label className="switch"
                                                                                   onClick={e => this.clickPowerStateLabel(e, index)}>
                                                                                <input type="checkbox"
                                                                                       checked={this.returnBooleanByModeState(power.mode)}
                                                                                       onChange={() => {
                                                                                       }}/>
                                                                                <span className="slider round"/>
                                                                            </label>

                                                                            <span
                                                                                className="switch-text">{this.returnModeString(power.mode)}</span>
                                                                        </div>
                                                                        {
                                                                            this.calculatePercentage(power) > 0 &&
                                                                            (
                                                                                <span
                                                                                    className={"percentage-state" + (power.state === "stop" ? " lc-warning-color" : "")}>{this.calculatePercentage(power)}%</span>
                                                                            )
                                                                        }
                                                                    </div>
                                                                </div>


                                                                <div className="absolute-item-wrap">
                                                                    <div className={"toggle-radio-wrap two-state lc-fat" + (power.mode === "auto" ? " lc-disabled-all" : "")}>
                                                                        <div className="disabled-wrap"/>
                                                                        <input type="radio" name={"power-toggle-option" + power.id}
                                                                               id={"powerToggleOption" + power.id + "_1"}
                                                                               onChange={e => {
                                                                               }}
                                                                               checked={power.state === "on"}/>

                                                                        <input type="radio" name={"power-toggle-option" + power.id}
                                                                               id={"powerToggleOption" + power.id + "_2"}
                                                                               onChange={e => {
                                                                               }}
                                                                               checked={power.state === "off"}/>

                                                                        <label
                                                                            onClick={e => this.clickLabel(e, "on", power, index)}>{power.button_name_1 ? power.button_name_1 : '운전'}</label>
                                                                        <label
                                                                            onClick={e => this.clickLabel(e, "off", power, index)}>{power.button_name_2 ? power.button_name_2 : '정지'}</label>
                                                                        <i className="toggle-slider-icon"/>
                                                                    </div>
                                                                </div>
                                                                {
                                                                    power.mode !== "auto" && (
                                                                        <div className="three-dot-menu" onClick={e => this.openIndividualControlModal(e, power, index)}>
                                                                            <div className="icon-wrap">
                                                                                <i/><i/><i/>
                                                                            </div>
                                                                            {/*<div className="three-dot-view-more-menu">*/}
                                                                            {/*<ul>*/}
                                                                            {/*<li onClick={e => this.openIndividualControlModal(e, power, index)}>*/}
                                                                            {/*<span>개별제어실행</span></li>*/}
                                                                            {/*</ul>*/}
                                                                            {/*</div>*/}
                                                                        </div>
                                                                    )
                                                                }
                                                            </div>

                                                        </article>
                                                    </li>
                                                )
                                            })
                                            }
                                        </ul> : null
                                    }
                                    {!this.state.powers.length ?
                                        <p>데이터가 없습니다.</p> : null
                                    }
                                </div>

                                {this.state.powerCount && (this.state.powerCount > this.state.size) ?
                                    <Pagination count={this.state.powerCount}
                                                offset={this.state.offset}
                                                size={this.state.size}
                                                searchPage={this.searchPage}/>
                                    : null
                                }
                            </section>
                        ) : null
                }
            </Fragment>
        );
    }
}

export default useNavScroll(
    ({actions}) => ({
        setPowerScrollCallback: actions.setPowerScrollCallback
    })
)(useRemote(
    ({actions}) => ({
        getRemoteState: actions.getRemoteState,
        setRemotePowerCallback: actions.setRemotePowerCallback
    })
)(useAutoControl(
    ({state, actions}) => ({
        autoControlSyncCallback: state.syncCallback,
        openAutoControlModal: actions.open
    })
)(useControlGroup(
    ({state, actions}) => ({
        openControlGroupModal: actions.open
    })
)(useIndividualControl(
    ({actions}) => ({
        openIndividualControlModal: actions.open
    })
)(useHouse(
    ({actions}) => ({
        setPowerCallback: actions.setPowerCallback,
        getHouseId: actions.getHouseId,
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(Power))))))));
