import React, {Component, Fragment} from 'react';
import {addListener, on} from "../utils/socket";
import {useSocket} from "../contexts/socket";
import {useHouse} from "../contexts/house";
import {useAlert} from "../contexts/alert";
import {useRemote} from "../contexts/remote";
import Spinner from '../components/Spinner';
import modeManager from '../managers/mode';
import controlManager from '../managers/control';
import {noSession} from "../utils/session";

class AutoControl extends Component {
    constructor(props) {
        super(props);

        this.instance = null;
        this.state = {
            remoteState: props.getRemoteState(),
            currentAutoCompleteTime: window.currentAutoCompleteTime,
            mode: window.mode,
            loading: {}
        };
    }

    componentDidMount() {
        const _this = this;
        this.findAllControls();
        this.findMode();
        this.state.remoteState === "field" && this.autoControlManualCallback();
        this.props.setModeCallback(this.changeHouseId);
        this.props.setRemoteAutoControlCallback(this.remoteAutoControlCallback, this.autoControlManualCallback);
        addListener(this, this.listener);
        this.instance = setInterval(() => {
            if (_this.state.currentAutoCompleteTime > 0) {
                _this.setState({
                    currentAutoCompleteTime: _this.state.currentAutoCompleteTime - 1
                });
            }
        }, 1000);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('mode');
        socket.removeListener('auto-control-trigger');
        clearInterval(this.instance);
    }

    listener = (socket) => {
        const _this = this;
        on(socket)('mode', ({mode}) => {
            console.log("mode check", mode);
            if (_this.state.mode.house_id == mode.house_id) {
                _this.loading("mode", false);
                _this.setState({mode});
            }
        });
        on(socket)('auto-control-trigger', ({house_id}) => {
            if (_this.state.mode.house_id == house_id) {
                _this.setState({
                    currentAutoCompleteTime: _this.state.mode.period
                });
            }
        });
    };

    findMode = () => {
        const _this = this;
        modeManager.findMode(this.props.getHouseId(), (status, data) => {
            if(status === 200) {
                _this.setState({
                    mode: data.data
                });
            } else {
                _this.props.alertError(status, data);
            }
        });
    };

    findAllControls = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            types: 'motor,power'
        };
        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    controlCount: data.data.count
                });
            } else if (status === 404) {
                _this.setState({
                    controlCount: 0
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    remoteAutoControlCallback = (remoteState) => {
        this.setState({remoteState});
    };

    autoControlManualCallback = () => {
        // this.clickLabel(null, 'manual');
    };

    changeHouseId = (house_id) => {
        let temp = Object.assign({}, this.state.mode);
        temp.house_id = house_id;
        this.setState({
            mode: temp
        }, () => {
            this.findAllControls();
            this.findMode();
        });
    };

    clickLabel = (e, val) => {
        e && e.preventDefault();

        // if(val === "auto" && this.state.remoteState === "field") {
        //     return this.props.showDialog({
        //         alertText: "자동제어는 원격모드 '원격' 에서만 가능합니다."
        //     });
        // }

        if(this.state.mode.mode === val) {
            return false;
        }

        const _this = this;
        let body = {
            id: this.state.mode.house_id,
            mode: val
        };
        this.loading("mode", true);
        modeManager.changeMode(body, noSession(this.props.showDialog)((status, data) => {
            if(status !== 200) {
                _this.loading("mode", false);
                _this.props.alertError(status, data);
            }
        }));
    };

    releaseLoadingByTimeout = (key) => {
        let temp = Object.assign({}, this.state.loading);
        const _this = this;
        if (temp[key]) {
            setTimeout(() => {
                temp[key] = false;
                _this.setState({
                    loading: temp
                });
            }, 2500);
        }
    };

    loading = (index, isLoading) => {
        let loading = Object.assign({}, this.state.loading);
        if (isLoading) {
            loading[index] = true;
            this.releaseLoadingByTimeout(index);
        } else {
            delete loading[index];
        }
        this.setState({loading});
    };

    render() {
        return (
            <Fragment>
                {
                    this.state.controlCount ?
                        (
                            <section className="lc-card" id="lcAutoControlWrap">
                                <Spinner spinning={this.state.loading.mode}/>
                                <div className="lc-card-header">
                                    <h2 className="lc-card-title">자동제어</h2>
                                    <div className="auto-control-header-wrap">
                                        <div className="time-state"><span>{this.state.currentAutoCompleteTime}초</span></div>
                                        <div className="toggle-radio-wrap three-state">
                                            <div className="disabled-wrap"/>
                                            <input type="radio" name="auto-control-toggle" value={'auto'} id="autoControlToggleFirst"
                                                   onChange={e => {}} checked={this.state.mode.mode === 'auto'}/>

                                            <input type="radio" name="auto-control-toggle" value={'individual'} id="autoControlToggleSecond"
                                                   onChange={e => {}} checked={this.state.mode.mode === 'individual'}/>

                                            <input type="radio" name="auto-control-toggle" value={'manual'} id="autoControlToggleThird"
                                                   onChange={e => {}} checked={this.state.mode.mode === 'manual'}/>

                                            <label onClick={e => this.clickLabel(e, 'auto')}>자동</label>
                                            <label onClick={e => this.clickLabel(e, 'individual')}>개별</label>
                                            <label onClick={e => this.clickLabel(e, 'manual')}>수동</label>
                                            <i className="toggle-slider-icon"/>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        ) : null
                }
            </Fragment>
        )
    }
}

export default useRemote(
    ({actions}) => ({
        getRemoteState: actions.getRemoteState,
        setRemoteAutoControlCallback: actions.setRemoteAutoControlCallback
    })
)(useHouse(
    ({actions}) => ({
        setModeCallback: actions.setModeCallback,
        getHouseId: actions.getHouseId,
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(AutoControl))));
