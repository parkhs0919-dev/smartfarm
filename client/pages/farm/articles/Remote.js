import React, {Component, Fragment} from 'react';
import controlManager from "../managers/control";
import {useSocket} from "../contexts/socket";
import {useHouse} from "../contexts/house";
import {useAlert} from "../contexts/alert";
import {useRemote} from "../contexts/remote";
import {addListener, on} from "../utils/socket";
import Spinner from "../components/Spinner";
import {noSession} from "../utils/session";

const TYPE_POWER = 'control';
const ENUM_REMOTE_NAME = "원격모드";
const ENUM_REMOTE_STATES = {
    on: "remote",
    off: "field"
};

class Remote extends Component {
    constructor(props) {
        super(props);

        this.state = {
            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            controls: window.controls,
            controlCount: 0,
            loading: {}
        };
    }

    componentDidMount() {
        this.findAllControls();
        this.props.setControlCallback(this.changeHouseId);
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('control');
    }

    listener = (socket) => {
        const _this = this;
        on(socket)('control', ({control}) => {
            let controls = JSON.parse(JSON.stringify(_this.state.controls));
            for(let i=0; i<controls.length; i++) {
                if(controls[i].id === control.id) {
                    controls[i] = control;
                    _this.loading(i, false);
                    _this.setState({controls});
                    break;
                }
            }
        });
    };

    clickLabel = (e, val, control, index) => {
        e.preventDefault();
        const _this = this;

        if(control.state === val) {
            return false;
        }

        let body = {
            id: control.id,
            state: val,
        };
        this.loading(index, true);

        controlManager.active(body, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                if(control.control_name === ENUM_REMOTE_NAME) {
                    _this.props.changeRemoteState(ENUM_REMOTE_STATES[val]);
                }
            } else {
                _this.loading(index, false);
                _this.props.alertError(status, data);
            }
        }));
    };

    changeHouseId = (house_id) => {
        const _this = this;
        this.setState({house_id}, () => {
            _this.findAllControls();
        });
    };
    findAllControls = () => {
        const _this = this;
        let query = {
            house_id: this.state.house_id,
            type: TYPE_POWER
        };

        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                let temp = {
                    controls: data.data.rows,
                    controlCount: data.data.count
                };
                _this.setState(temp);
            } else if (status === 404) {
                let state = {
                    controls: [],
                    controlCount: 0
                };

                _this.setState(state);
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    releaseLoadingByTimeout = (key) => {
        let temp = Object.assign({}, this.state.loading);
        const _this = this;
        if (temp[key]) {
            setTimeout(() => {
                temp[key] = false;
                _this.setState({
                    loading: temp
                });
            }, 2500);
        }
    };

    loading = (index, isLoading) => {
        let loading = Object.assign({}, this.state.loading);
        if (isLoading) {
            loading[index] = true;
            this.releaseLoadingByTimeout(index);
        } else {
            delete loading[index];
        }
        this.setState({loading});
    };

    render() {
        return (
            <Fragment>
                {
                    this.state.controls.map((control, index) => {
                        return (
                            <section className="lc-card" key={index}>
                                <Spinner spinning={this.state.loading[index]}/>
                                <div className="lc-card-header">
                                    <h2 className="lc-card-title">{control.control_name}</h2>
                                    <div className="toggle-radio-wrap two-state">
                                        <input type="radio" name={`control-toggle-${control.id}`} onChange={e => {}}
                                               checked={control.state === "on"} id={`remoteToggleFirst-${control.id}`}/>
                                        <input type="radio" name={`control-toggle-${control.id}`} onChange={e => {}}
                                               checked={control.state === "off"} id={`remoteToggleSecond-${control.id}`} />

                                        <label onClick={e => this.clickLabel(e, "on", control, index)}>{control.button_name_1 ? control.button_name_1 : 'ON'}</label>
                                        <label onClick={e => this.clickLabel(e, "off", control, index)}>{control.button_name_2 ? control.button_name_2 : 'OFF'}</label>
                                        <i className="toggle-slider-icon"/>
                                    </div>
                                </div>
                            </section>
                        );
                    })
                }
            </Fragment>
        )
    }
}

export default useRemote(
    ({actions}) => ({
        changeRemoteState: actions.changeRemoteState,
    })
)(useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId,
        setControlCallback: actions.setControlCallback
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(Remote))));