import React, {Component, Fragment} from 'react';
import {useAlert} from "../contexts/alert";
import {useHouse} from "../contexts/house";
import {useWeather} from "../contexts/modals/weather";
import {date, attachZero} from "../../../utils/filter";
import {colourStyles} from "../utils/select";
import Select from 'react-select';
import weatherManager from '../managers/weather';
import pingManager from '../managers/ping';
import sunDateManager from '../managers/sunDate';

import CONSTANT from '../constants/constant';
import Spinner from "../components/Spinner";
import {noSession} from "../utils/session";
import deepcopy from "../../../utils/deepcopy";
import {useSunDate} from "../contexts/sunDate";
const DATE_FILTER_FORMAT = "yyyy-MM-dd";

let houseOptions = [];
if(window.houses && window.houses.length) {
    window.houses.forEach(house => {
        let obj = {};
        obj.value = house.id;
        obj.label = house.house_name;
        houseOptions.push(obj);
    });
}

class HouseInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            houses: [],
            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            selectedHouse: this.findSelectedHouse(props.getHouseId()),
            currentTime: new Date(),
            weather: null,
            weatherText: '날씨정보를 불러오지 못했습니다.',
            initialWeatherLoading: true,
            loading: true,
        };
    }

    findSelectedHouse = (house_id) => {
        for(let i=0; i<houseOptions.length; i++) {
            if(houseOptions[i].value === house_id) {
                return houseOptions[i];
            }
        }
    };

    componentDidMount() {
        const dateInterval = setInterval(() => this.generateDate(), 1000);
        const pingInterval = setInterval(() => this.checkPing(), (1000 * 60 * 5));
        let intervals = {
            dateInterval: dateInterval,
            pingInterval: pingInterval
        };
        this.checkPing();
        this.getSunDates();
        if (this.props.isPanel) {
            intervals.weatherInterval = setInterval(() => this.findWeathers(), (1000 * 60 * 60 * 24));
        }
        this.findWeathers();
        this.setState(intervals);
    }

    componentWillUnmount() {
        clearInterval(this.state.dateInterval);
        clearInterval(this.state.pingInterval);
        if(this.state.weatherInterval) {
            clearInterval(this.state.weatherInterval);
        }
    }

    openHouseTab = (e) => {
        this.setState({
            isHouseTabOpen: !this.state.isHouseTabOpen
        })
    };

    generateDate = () => {
        let beforeTime = new Date(deepcopy(this.state.currentTime));
        let now = new Date();

        //00:00마다 1번씩 실행
        if(beforeTime.getDate() - now.getDate() > 0 || (now.getHours() === 0 && now.getMinutes() === 0 && now.getSeconds() === 0)) {
            this.getSunDates();
        }

        this.setState({
            currentTime: now
        });
    };

    getSunDates = () => {
        const _this = this;
        sunDateManager.findAll(noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                // let riseTime = data.data.rise_hour + ':' + data.data.rise_minute;
                // let setTime = data.data.set_hour + ':' + data.data.set_minute;
                // let state = {
                //     riseTime: riseTime,
                //     setTime: setTime
                // };
                // _this.setState(state);
                _this.props.setSunDate(data.data);
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    checkPing = () => {
        const _this = this;
        let query = { platform : window.platform };
        pingManager.checkPing(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    pingLevel: data.data.level
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findWeathers = () => {
        const _this = this;
        let query = { platform : window.platform };
        weatherManager.findWeather(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                let today = date(new Date(), DATE_FILTER_FORMAT);
                let state = {
                    weather: data.data[today],
                    initialWeatherLoading: false,
                    loading: false
                };
                _this.setState(state);
            } else if (status === 404) {
                _this.setState({
                    weather: null,
                    initialWeatherLoading: false,
                    loading: false
                })
            } else {
                _this.setState({
                    initialWeatherLoading: false,
                    loading: false
                });
            }
        }));
    };

    getAMPM = (currentTime) => {
        return (new Date(currentTime).getHours()) >= 12 ? "오후" : "오전";
    };

    handleChange = (selectedHouse) => {
        this.props.changeHouse(selectedHouse.value);
        this.setState({selectedHouse});
    };

    openWeatherModal = () => {
        this.props.openWeatherModal(null, this.findWeathers);
    };

    render() {
        return (
            <section className="lc-card no-padding" id="lcHouseInfoWrap" style={{zIndex: 2}}>
                <div className="lc-card-top">
                    { this.state.initialWeatherLoading ?
                        <Spinner spinning={this.state.loading}/> : null
                    }

                    <div id="internetSignal">
                        <img src="/public/images/farm/alarm-icon@2x.png" alt="signal"/>
                        <i id="signalBall" className={`lc-level-${this.state.pingLevel}`}/>
                    </div>
                    <div className="time-info-wrap">
                        <p className="date">{date(this.state.currentTime, DATE_FILTER_FORMAT)}</p>
                        <p className="time">{this.getAMPM(this.state.currentTime)} {date(this.state.currentTime, 'hh:mm')}</p>
                    </div>
                    { !this.state.loading ?
                        <Fragment>
                            { this.state.weather ?
                                <div className="weather-info-wrap" style={{backgroundImage: 'url('+CONSTANT.weatherURL+this.state.weather.icon+ '.png)'}} onClick={this.openWeatherModal}>
                                    <p className="temp">{this.state.weather.temperature} ℃</p>
                                    <p className="state">{this.state.weather.summary}</p>
                                </div> : null
                            }
                            { !this.state.weather ?
                                <p id="weatherWarn">{this.state.weatherText}</p> : null
                            }
                        </Fragment> : null
                    }
                </div>
                {
                    this.props.rise_hour ?
                        (
                            <div className="lc-card-center">
                                <p>일출시: {attachZero(parseInt(this.props.rise_hour))}:{attachZero(parseInt(this.props.rise_minute))}</p>
                                <p>일몰시: {attachZero(parseInt(this.props.set_hour))}:{attachZero(parseInt(this.props.set_minute))}</p>
                            </div>
                        ) : null
                }
                <div className="lc-card-bottom" onClick={this.openHouseTab}>
                    <label id="houseSelectLabel">하우스 선택</label>
                    <div className="content-item-wrap select-module-wrap">
                        <Select value={this.state.selectedHouse}
                                onChange={this.handleChange}
                                isSearchable={ false }
                                inputProps={{readOnly:true}}
                                options={houseOptions}
                                styles={colourStyles}/>
                    </div>
                </div>
            </section>
        )
    }
}

export default useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useWeather(
    ({actions}) => ({
        openWeatherModal: actions.open
    })
)(useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId,
        changeHouse: actions.changeHouse,
    })
)(useSunDate(
    ({state, actions}) => ({
        rise_hour: state.rise_hour,
        rise_minute: state.rise_minute,
        set_hour: state.set_hour,
        set_minute: state.set_minute,
        setSunDate: actions.setSunDate
    })
)(HouseInfo))));
