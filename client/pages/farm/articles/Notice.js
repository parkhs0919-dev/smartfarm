import React, {Component, Fragment} from 'react';
import noticeManager from '../managers/notice';
import {useAlert} from "../contexts/alert";
import {useNoticeDetail} from "../contexts/modals/noticeDetail";
import {noSession} from "../utils/session";

class Notice extends Component {
    constructor(props) {
        super(props);

        this.state = {
            notice: null
        };
    }

    componentDidMount() {
        const noticeInterval = setInterval(() => this.findNotice(), (1000 * 60 * 60 * 24));
        this.setState({noticeInterval});
        this.findNotice();
    }

    componentWillUnmount() {
        clearInterval(this.state.noticeInterval);
    }

    findNotice = () => {
        const _this = this;
        let query = {
            platform: window.platform
        };
        noticeManager.findNotice(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    notice: data.data
                });
            } else if (status === 404) {
                _this.setState({
                    notice: null
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    openNoticeDetail = () => {
        this.props.openNoticeDetail(Object.assign({}, this.state.notice));
    };

    render() {
        return (
            <Fragment>
                { this.state.notice ?
                    <section className="lc-card" id="lcNoticeWrap" onClick={this.openNoticeDetail}>
                        <div className="lc-card-header">
                            <p>[공지사항] {this.state.notice.rows[0].title}</p>
                        </div>
                    </section> : null
                }
            </Fragment>
        )
    }
}

export default useNoticeDetail(
    ({actions}) => ({
        openNoticeDetail: actions.open
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(Notice));
