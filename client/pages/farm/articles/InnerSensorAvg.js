import React, {Component,Fragment} from 'react';
import constants from '../constants/constant'
import { sanitize2 } from "../utils/sensor";
import {useAlert} from "../contexts/alert";
import {useSocket} from "../contexts/socket";
import {useHouse} from "../contexts/house";
import deepcopy from "../../../utils/deepcopy";
import {addListener, on} from "../utils/socket";
import sensorGroupmanager from '../managers/sensorgroup'
import {noSession} from "../utils/session";

class InSensorGroup extends Component {
    constructor(props) {
        super(props);   
        this.state = {
            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            groups:[],
        };
    }

    componentDidMount() {
        this.getGroups();
        addListener(this, this.listener);
    }
    getGroups = () => {
        const _this = this;
        
        let query = {
            house_id: this.state.house_id,
            position: 'in',
            is_use:1
        };
        
        sensorGroupmanager.findAll(query, noSession(this.props.showDialog)((status, data) => {                   
            if (status === 200) {                
                _this.setState({
                    groups: data.data
                },(e)=>{this.makeunit(e)});
            } else if (status === 404) {
              
                _this.setState({
                    groups: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('sensor-avg-values');
        socket.removeListener('sensor_avg');
    }
    makeunit = (e) =>{
        let defaulttype = constants.sensorGroup.Type
        let groups = deepcopy(this.state.groups)
        
        for(let i=0; i<this.state.groups.length; i++){
            for(let j=0; j<defaulttype.length; j++){
                if(this.state.groups[i].type==defaulttype[j].value){
                    groups[i].unit=defaulttype[j].unit
                    break;
                }
            }
        }
        this.setState({groups})
    }
    listener = (socket) => {
                
        on(socket)('sensor-avg-values', (data) => {
            this.groupsvalue(data);
        });

        on(socket)('sensor_avg', (house_id) => {
            if(house_id==this.state.house_id)
            {                                
                this.getGroups();
            }
        });
    };

    groupsvalue = (data) =>{
        let groups=[];
        let defaulttype = constants.sensorGroup.Type
        if(data)
        {    
            for(let i=0; i<this.state.groups.length; i++)
            {
                
                for(let j=0; j<data.sensorsAvg.length; j++)
                {                 
                    if(this.state.groups[i].id==data.sensorsAvg[j].id)
                    {                        
                        groups.push(data.sensorsAvg[j])
                        break;
                    }
                }
            }

            for(let a=0; a<groups.length; a++){
                for(let b=0; b<defaulttype.length; b++){
                    if(this.state.groups[a].type==defaulttype[b].value){
                        groups[a].unit=defaulttype[b].unit
                        break;
                    }
                }
            }
            this.setState({groups})
        }
    }

    //todo 시작일은 아무때나가능, 종료일은 시작일기준 max 90일이지만 오늘을 넘기면안됨.

    render() {            
        return (
            <Fragment>
            {
                this.state.groups && this.state.groups.length ?
                    (
                        <section className="lc-card position-relative" id="lcSensorGroupWrap" ref={this.outerSensorRef}>

                            <div className="lc-card-header">
                                <h2 className="lc-card-title">내부센서 그룹</h2>                               
                            </div>

                            <div className="lc-card-content-wrap">
                            {
                                this.state.groups && this.state.groups.length ?
                                    (
                                        <React.Fragment>

                                            <ul id="sensorGroup_List">
                                                {
                                                    this.state.groups.map((item, index) => {
                                                 
                                                        return (
                                                            <li className="sensorGroup-list-item" key={index} style={{ zIndex: this.state.groups.length - index }}>
                                                                <article className="lc-content-card">
                                                                    <div className="lc-sensorGroup-name-wrap">
                                                                        <p className="lc-sensorGroup-name-wrap">                                                                            
                                                                            <span className="lc-control-name">{item.name}</span>
                                                                        </p>
                                                                    </div>
                                                                    <div className="lc-sensorGroup-wrap">
                                                                        {
                                                                            <div className="ordinary-menu-wrap">
                                                                                
                                                                                <span style={{color:'#04B45F'}}>{sanitize2(item)}</span>
                                                                            </div>
                                                                        }
                                                                        
                                                                    </div>
                                                                </article>
                                                                
                                                            </li>
                                                        )

                                                    })
                                                }
                                            </ul>
                                        </React.Fragment>
                                    ) :

                                    <article className="lc-content-card">

                                        <div className="lc-sensorGroup-name-wrap">
                                            <p className="lc-sensorGroup-name-wrap2">
                                                그룹 설정이 없습니다.
                                            </p>


                                        </div>
                                    </article>

                            }
                                { !this.state.groups.length ?
                                    <p>데이터가 없습니다.</p> : null
                                }
                            </div>
                            
                        </section>
                    ) : null
             }
        </Fragment>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId,
        setExcelCallback: actions.setExcelCallback,
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(InSensorGroup)));