import React, {Component, Fragment} from 'react';
import {useSocket} from "../contexts/socket";
import {useHouse} from "../contexts/house";
import {useAlert} from "../contexts/alert";
import {useIndividualControl} from "../contexts/modals/individualControl";
import {useAutoControl} from "../contexts/modals/autoControl";
import {useRemote} from "../contexts/remote";
import {useNavScroll} from "../contexts/navScroll";
import {useControlGroup} from "../contexts/modals/controlGroup";
import {addListener, on} from "../utils/socket";
import Pagination from '../components/Pagination';
import constant from '../constants/constant';
import Spinner from '../components/Spinner';
import deepcopy from "../../../utils/deepcopy";

import controlManager from '../managers/control';
import modeManager from '../managers/mode';
import autoControlManager from '../managers/autoControl';
import {noSession} from "../utils/session";
import {getQuery} from "../../../utils/route";
import controlAlarmManager from "../managers/controlAlarm";
import {play} from "../utils/sound";
import nativeBridge from "../../../utils/nativeBridge";
import controlGroupManager from "../managers/controlGroup";
import controlGroupActiveManager from "../managers/controlGroupActive";
import {attachZero} from "../../../utils/filter";
import {colourStyles} from "../utils/select";
import Select from "react-select";

const TYPE_MOTOR = 'motor';
const TYPE_WINDDIRECTION = 'windDirection';
const MODE_AUTO = constant.MOTOR.modeAuto;
const MODE_MANUAL = constant.MOTOR.modeManual;

class Motor extends Component {
    constructor(props) {
        super(props);

        this.isOpened = false;
        this.moreMenuRefs = new Map();
        this.motorRef = React.createRef();
        this.nativeBridge = null;
        this.state = {
            remoteState: props.getRemoteState(),
            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            mode: window.mode.mode,
            motors: [],
            motorCount: 0,
            offset: 0,
            size: constant.defaultLoadSize,
            loading: {},
            autoControlCount: 0,
            motorControlGroups : [],
            selectedMotorControlGroup : null,
            motorGroupLoading : false,

        }
    }

    componentDidMount() {
        this.findMotorControlGroups();
        this.nativeBridge = nativeBridge('tts');
        this.searchPage(0);
        this.findAutoControlCount();
        this.props.setMotorCallback(this.changeHouseId);
        this.props.setRemoteMotorCallback(this.remoteMotorCallback);
        this.props.setMotorScrollCallback(this.scrollCallback);
        addListener(this, this.listener);

    }

    componentWillUnmount() {
        this.nativeBridge = null;
        const socket = this.props.socket;
        socket.removeListener('motor');
        socket.removeListener('motors');
        socket.removeListener('mode');
        socket.removeListener('auto-controls');
        socket.removeListener('control-group');
        socket.removeListener('control-groups');
    }

    shouldComponentUpdate(nextProps, nextState){
        let query = getQuery();
        if(query.modal === "auto-control" && nextProps.autoControlSyncCallback && !this.isOpened) {
            this.isOpened = true;
            this.props.openAutoControlModal({step: 1, house_id: this.state.house_id});
        }
        return true;
    }

    listener = (socket) => {
        const _this = this;
        on(socket)('motor', (data) => {
            let motor = data.motor;
            if (motor) {
                let motors = JSON.parse(JSON.stringify(_this.state.motors));
                for (let i = 0; i < motors.length; i++) {
                    if (motors[i].id === motor.id) {
                        motors[i] = motor;
                        _this.loading(i, false);
                        _this.setState({motors});

                        break;
                    }
                }
            }
        });

        on(socket)('control-state', (data) => {
           if(data){
               _this.playSound(data);
           }
        });

        on(socket)('motors', (data) => {
            if(data && data.house_id) {
                if(this.state.house_id == data.house_id) {
                    _this.searchPage(_this.state.offset);
                }
            } else {
                _this.searchPage(_this.state.offset);
            }
        });

        on(socket)('mode', ({mode}) => {
            _this.setState({
                mode: mode.mode
            });
        });

        on(socket)('auto-controls', (data) => {
            if(data && data.house_id) {
                if(this.state.house_id == data.house_id) {
                    _this.findAutoControlCount();
                    _this.searchPage(_this.state.offset);
                }
            } else {
                _this.findAutoControlCount();
                _this.searchPage(_this.state.offset);
            }
        });

        on(socket)('control-group', (data) => {
            //상태 변경
            if(data && data.controlGroup.type === 'motor') {

                let selectedMotorControlGroup = deepcopy(_this.state.selectedMotorControlGroup);
                if(selectedMotorControlGroup.value === parseInt(data.controlGroup.id)) {
                    selectedMotorControlGroup.state = data.controlGroup.state;
                    selectedMotorControlGroup.origin.state = data.controlGroup.state;
                    _this.setState({selectedMotorControlGroup});
                }
            }
        });

        on(socket)('control-groups', (data) => {
            //데이터 갱신
            if(data){
                _this.findMotorControlGroups();
            }
        });

        on(socket)('control-group-active-start', (data) => {
            if(data && data.house_id === _this.state.house_id){
                _this.setState({
                    motorGroupLoading : !_this.state.motorGroupLoading
                })
            }
        });

        on(socket)('control-group-active-end', (data) => {
            if(data && data.house_id === _this.state.house_id) {
                _this.setState({
                    motorGroupLoading : !_this.state.motorGroupLoading
                })
            }
        });

    };

    findMotorControlGroups = () => {
        const _this = this;

        let where = {
            house_id : this.props.getHouseId(),
            type : TYPE_MOTOR,
            on_off_state : 'on'
        };

        controlGroupManager.findAll(where, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    motorControlGroups : _this.generateMotorControlGroups(data.data.rows),
                });
            }else if(status === 404) {
                _this.setState({
                    motorControlGroups : []
                })
            } else {
                _this.props.alertError(status, data);
            }

        }));
    };

    generateMotorControlGroups = (list) => {
        let motorControlGroups = [];
        list.forEach((data, index) => {
            motorControlGroups.push({
                value : data.id,
                label : data.control_group_name,
                state : data.state,
                origin : data,
            });
        });

        return motorControlGroups;
    };

    playSound = (motor) => {
        const _this = this;
        controlAlarmManager.findById(motor.id, noSession(this.props.showDialog)((status, data) => {
            if(status === 200){
                const control = data.data;
                if(control && control.controlAlarms.length){
                    control.controlAlarms.forEach((item, index) => {
                        if(motor.state == item.state){
                            // play(item.text);
                            _this.nativeBridge.send(item.text);
                        }
                    });
                }
            }
        }));
    };

    findAutoControlCount = () => {
        const _this = this;
        let query = {
            house_id: this.state.house_id,
            type: TYPE_MOTOR,
            state: 'on'
        };
        autoControlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    autoControlCount: data.data.count
                });
            } else if (status === 404) {
                _this.setState({
                    autoControlCount: 0
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    remoteMotorCallback = (remoteState) => {
        this.setState({remoteState});
    };

    changeHouseId = (house_id) => {
        const _this = this;
        this.setState({house_id}, () => {
            _this.searchPage(0);
            _this.findAutoControlCount();
            _this.findMotorControlGroups();
        });
    };

    scrollCallback = () => {
        this.motorRef.current.scrollIntoView({behavior: "smooth"});
    };

    searchPage = (offset) => {
        const _this = this;
        this.setState({
            offset: offset
        });

        let query = {
            house_id: this.state.house_id,
            type: TYPE_MOTOR,
            offset: offset,
            size: this.state.size
        };

        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    loading: {},
                    motors: data.data.rows,
                    motorCount: data.data.count
                })

            } else if (status === 404) {
                _this.setState({
                    loading: {},
                    motors: [],
                    motorCount: 0
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    clickLabel = (e, val, motor, index) => {
        e.preventDefault();
        const _this = this;
        let currentState = motor.state;

        if (currentState === val) {
            return false;
        }

        if ((currentState === "open" && val === "close") || (currentState === "close" && val === "open")) {
            return false;
        }

        let body = {
            id: motor.id,
            state: val,
        };
        this.loading(index, true);
        controlManager.active(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                _this.loading(index, false);
                _this.props.alertError(status, data);
            }
        }));
    };

    clickMotorLabelGroup = (e, val, motor) => {
        if(motor){
            e.preventDefault();
            const _this = this;
            let currentState = motor.state;

            if (currentState === val) {
                return false;
            }

            if ((currentState === "open" && val === "close") || (currentState === "close" && val === "open")) {
                return false;
            }

            let body = {
                id: motor.value,
                state: val,
            };

            controlGroupActiveManager.active(body, noSession(this.props.showDialog)((status, data) => {
                if (status !== 200) {
                    // _this.loading(index, false);
                    _this.props.alertError(status, data);
                }
            }));
        }
    };

    releaseLoadingByTimeout = (key) => {
        let temp = Object.assign({}, this.state.loading);
        const _this = this;
        if (temp[key]) {
            setTimeout(() => {
                temp[key] = false;
                _this.setState({
                    loading: temp
                });
            }, 2500);
        }
    };

    loading = (index, isLoading) => {
        let loading = Object.assign({}, this.state.loading);
        if (isLoading) {
            loading[index] = true;
            this.releaseLoadingByTimeout(index);
        } else {
            delete loading[index];
        }
        this.setState({loading});
    };

    calculatePercentage = (motor) => {
        return parseInt(motor.current / motor.range * 100);
    };

    openIndividualControlModal = (e, item, index) => {
        // const ref = this.moreMenuRefs.get(index);
        // ref.blur();
        this.props.openIndividualControlModal(item, this.individualSettingCallback);
    };

    individualSettingCallback = () => {
        console.log("설정하고나서 조히다시해서 카운트바꾸어? 수동이면 그럴필요없나?");
    };

    clickMotorStateLabel = (e, index) => {
        e.preventDefault();
        const _this = this;

        let motors = JSON.parse(JSON.stringify(this.state.motors));
        let motor = motors[index];
        let mode = this.returnMotorOnOffMode(motor);
        let body = {};
        body.id = motor.id;
        body.mode = mode;
        // if(mode === "auto" && this.state.remoteState === "field") {
        //     return this.props.showDialog({
        //         alertText: "자동제어는 원격모드 '원격' 에서만 가능합니다."
        //     });
        // }
        modeManager.controlMode(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                _this.props.alertError(status, data);
            }
        }));
    };

    returnMotorOnOffMode = (motor) => {
        if (motor.mode === MODE_AUTO) {
            return MODE_MANUAL;
        } else if (motor.mode === MODE_MANUAL) {
            return MODE_AUTO;
        }
    };

    openAutoControlModal = (step, e, item) => {
        const _this = this;

        if(step === 1) {
            return _this.props.openAutoControlModal({step: step, type: TYPE_MOTOR, house_id: this.state.house_id});
        } else if(step === 2) {
            if (item !== undefined) {
                return _this.props.openAutoControlModal({
                    step: step,
                    control_id: item.id
                });
            }
        }
        return _this.props.openAutoControlModal({step: step});

        // autoControlManager.checkCanAutoControl(this.state.house_id, (status, data) => {
        //     if(status === 200) {
        //
        //     } else if (status === 400) {
        //         _this.props.showDialog({
        //             alertText: "자동제어를 추가할 수 없는 상태입니다."
        //         })
        //     }
        // });
    };

    returnModeString = (mode) => {
        if (mode === MODE_AUTO) {
            return "자동";
        } else if (mode === MODE_MANUAL) {
            return "수동";
        }
    };

    returnBooleanByModeState = (mode) => {
        if (mode === MODE_AUTO) {
            return false;
        } else if (mode === MODE_MANUAL) {
            return true;
        }
    };

    openMotorControlGroupModal = (e) => {
        const _this = this;
        return _this.props.openMotorControlGroupModal({type: TYPE_MOTOR, house_id: this.state.house_id});
    };

    handleMotorControlGroup = (value, key) => {
        const _this = this;
        _this.setState({selectedMotorControlGroup : value});
    };

    render() {
        return (
            <Fragment>
                {
                    this.state.motorControlGroups && this.state.motorControlGroups.length ?
                        <section className="lc-card" id="lcControlMotorWrap">
                            <Spinner spinning={this.state.motorGroupLoading}/>
                            <div className="lc-card-header">
                                <h2 className="lc-card-title">모터그룹제어</h2>
                            </div>
                            <div className="lc-card-content-wrap">
                                <article className="lc-control-group">
                                    <div className="select-module-wrap">
                                        <Select
                                            value={this.state.selectedMotorControlGroup}
                                            placeholder="모터그룹 선택"
                                            onChange={value => this.handleMotorControlGroup(value, 'selectedMotorControlGroup')}
                                            isSearchable={false}
                                            options={this.state.motorControlGroups}
                                            styles={colourStyles}/>
                                    </div>
                                    <div className="absolute-item-wrap">
                                        <div
                                            className={"toggle-radio-wrap three-state lc-fat" + (!this.state.selectedMotorControlGroup ? " lc-disabled-all" : "")}>
                                            <div className="disabled-wrap"/>
                                            <input type="radio" name={"control-group-toggle-option"}
                                                   id={"motorControlGroupToggleFirst"}
                                                   onChange={e => {}}
                                                   disabled={this.state.selectedMotorControlGroup && this.state.selectedMotorControlGroup.state === "close" || false}
                                                   checked={this.state.selectedMotorControlGroup && this.state.selectedMotorControlGroup.state === 'open' || false}/>

                                            <input type="radio" name={"control-group-toggle-option"}
                                                   id={"motorControlGroupToggleSecond"}
                                                   onChange={e => {}}
                                                   checked={!this.state.selectedMotorControlGroup  || (this.state.selectedMotorControlGroup && this.state.selectedMotorControlGroup.state === 'stop') || false}/>

                                            <input type="radio" name={"control-group-toggle-option"}
                                                   id={"motorControlGroupToggleThird"}
                                                   onChange={e => {}}
                                                   disabled={this.state.selectedMotorControlGroup && this.state.selectedMotorControlGroup.state === "open" || false}
                                                   checked={this.state.selectedMotorControlGroup && this.state.selectedMotorControlGroup.state === 'close' || false}/>
                                            <label
                                                onClick={(e) => this.clickMotorLabelGroup(e, 'open', this.state.selectedMotorControlGroup)}
                                                className={this.state.selectedMotorControlGroup && this.state.selectedMotorControlGroup.state === "close" ? "lc-disabled" : ""}>열림</label>
                                            <label
                                                onClick={(e) => this.clickMotorLabelGroup(e, 'stop', this.state.selectedMotorControlGroup)}>정지</label>
                                            <label
                                                onClick={(e) => this.clickMotorLabelGroup(e, 'close', this.state.selectedMotorControlGroup)}
                                                className={this.state.selectedMotorControlGroup && this.state.selectedMotorControlGroup.state === "open" ? "lc-disabled" : ""}>닫힘</label>
                                            <i className="toggle-slider-icon"/>
                                        </div>
                                    </div>
                                </article>
                            </div>

                        </section> : null
                }
                {
                    this.state.motors && this.state.motors.length ?
                        (
                            <section className="lc-card" id="lcMotorWrap" ref={this.motorRef}>
                                <div className="lc-card-header">
                                    <h2 className="lc-card-title">모터제어</h2>
                                    <button className="card-header-button group-btn" onClick={e => this.openMotorControlGroupModal(e)}>모터 그룹제어 설정</button>
                                    <button className="card-header-button" onClick={e => this.openAutoControlModal(1, e)}>
                                        <span>모터 자동제어</span><span className="number-badge">{this.state.autoControlCount}</span>
                                    </button>
                                </div>
                                <div className="lc-card-content-wrap">
                                    {this.state.motors.length ?
                                        <ul className="lc-motor-list">
                                            {
                                                this.state.motors.map((motor, index) => {
                                                    return (
                                                        <li key={index}>
                                                            <article className="lc-content-card">
                                                                <Spinner spinning={this.state.loading[index]}/>
                                                                <div className="content-position-wrap">

                                                                    <div className={"motor-content-wrap" + (this.state.mode !== "individual" ? " lc-non-individual-mode" : "")}>
                                                                        <div className="motor-text-count-wrap" onClick={e => this.openAutoControlModal(2, e, motor)}>
                                                                            <p className="motor-text">{motor.control_name}</p>
                                                                            <span className="motor-count">{motor.autoControlItemCount + motor.autoControlStepCount}</span>
                                                                        </div>

                                                                        <div className="flex-line-break"/>

                                                                        <div className="motor-dialog-percentage-wrap">
                                                                            <div className="lc-toggle-wrapper">

                                                                                <label className="switch" onClick={e => this.clickMotorStateLabel(e, index)}>
                                                                                    <input type="checkbox"
                                                                                           checked={this.returnBooleanByModeState(motor.mode)}
                                                                                           onChange={() => {
                                                                                           }}/>
                                                                                    <span className="slider round"/>
                                                                                </label>

                                                                                <span className="switch-text">{this.returnModeString(motor.mode)}</span>
                                                                            </div>
                                                                            {
                                                                                this.calculatePercentage(motor) > 0 &&
                                                                                (
                                                                                    <span
                                                                                        className={"percentage-state" + (motor.state === "stop" ? " lc-warning-color" : "")}>{this.calculatePercentage(motor)}%</span>
                                                                                )
                                                                            }
                                                                        </div>
                                                                    </div>


                                                                    <div className="absolute-item-wrap">
                                                                        <div
                                                                            className={"toggle-radio-wrap three-state lc-fat" + (motor.mode === "auto" ? " lc-disabled-all" : "")}>
                                                                            <div className="disabled-wrap"/>
                                                                            <input type="radio" name={"toggle-option" + motor.id}
                                                                                   id={"toggleFirst" + motor.id}
                                                                                   value="open"
                                                                                   onChange={e => {
                                                                                   }}
                                                                                   disabled={motor.state === "close"}
                                                                                   checked={motor.state === 'open'}/>

                                                                            <input type="radio" name={"toggle-option" + motor.id}
                                                                                   id={"toggleSecond" + motor.id}
                                                                                   value="stop"
                                                                                   onChange={e => {
                                                                                   }}
                                                                                   checked={motor.state === 'stop'}/>

                                                                            <input type="radio" name={"toggle-option" + motor.id}
                                                                                   id={"toggleThird" + motor.id}
                                                                                   value="close"
                                                                                   onChange={e => {
                                                                                   }}
                                                                                   disabled={motor.state === "open"}
                                                                                   checked={motor.state === 'close'}/>
                                                                            <label
                                                                                onClick={(e) => this.clickLabel(e, 'open', motor, index)}
                                                                                className={motor.state === "close" ? "lc-disabled" : ""}>{motor.button_name_1 ? motor.button_name_1 : '열림'}</label>
                                                                            <label
                                                                                onClick={(e) => this.clickLabel(e, 'stop', motor, index)}>{motor.button_name_2 ? motor.button_name_2 : '정지'}</label>
                                                                            <label
                                                                                onClick={(e) => this.clickLabel(e, 'close', motor, index)}
                                                                                className={motor.state === "open" ? "lc-disabled" : ""}>{motor.button_name_3 ? motor.button_name_3 : '닫힘'}</label>
                                                                            <i className="toggle-slider-icon"/>
                                                                        </div>
                                                                    </div>
                                                                    {
                                                                        motor.mode !== "auto" &&
                                                                        (
                                                                            <div className="three-dot-menu" onClick={e => this.openIndividualControlModal(e, motor, index)}>
                                                                                <div className="icon-wrap">
                                                                                    <i/><i/><i/>
                                                                                </div>
                                                                                {/*<div className="three-dot-view-more-menu">*/}
                                                                                    {/*<ul>*/}
                                                                                        {/*<li onClick={e => this.openIndividualControlModal(e, motor, index)}>*/}
                                                                                            {/*<span>개별제어실행</span></li>*/}
                                                                                    {/*</ul>*/}
                                                                                {/*</div>*/}
                                                                            </div>
                                                                        )
                                                                    }
                                                                </div>
                                                            </article>
                                                        </li>
                                                    )
                                                })
                                            }
                                        </ul> : null
                                    }
                                    {!this.state.motors.length ?
                                        <p>데이터가 없습니다.</p> : null
                                    }
                                </div>

                                {this.state.motorCount && (this.state.motorCount > this.state.size) ?
                                    <Pagination count={this.state.motorCount}
                                                offset={this.state.offset}
                                                size={this.state.size}
                                                searchPage={this.searchPage}/>
                                    : null
                                }
                            </section>
                        ) : null
                }
            </Fragment>
        );
    }
}

export default useNavScroll(
    ({actions}) => ({
        setMotorScrollCallback: actions.setMotorScrollCallback
    })
)(useRemote(
    ({actions}) => ({
        getRemoteState: actions.getRemoteState,
        setRemoteMotorCallback: actions.setRemoteMotorCallback
    })
)(useAutoControl(
    ({state, actions}) => ({
        autoControlSyncCallback: state.syncCallback,
        openAutoControlModal: actions.open
    })
)(useControlGroup(
    ({state, actions}) => ({
        openMotorControlGroupModal: actions.open
    })
)(useIndividualControl(
    ({actions}) => ({
        openIndividualControlModal: actions.open
    })
)(useHouse(
    ({actions}) => ({
        setMotorCallback: actions.setMotorCallback,
        getHouseId: actions.getHouseId,
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(Motor))))))));
