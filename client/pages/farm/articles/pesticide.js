import React, { Component, Fragment } from 'react';
import { useSocket } from "../contexts/socket";
import { useHouse } from "../contexts/house";
import { useAlert } from "../contexts/alert";


import { useNavScroll } from "../contexts/navScroll";
import { usePesticide } from "../contexts/modals/pesticide";
import { usePesticideautomake } from "../contexts/modals/pesticideautomake"
import { usepesticideDeatail } from "../contexts/modals/pesticideDeatail"
import { addListener, on } from "../utils/socket";
import Pagination from '../components/Pagination';
import constant from '../constants/constant';
import Spinner from '../components/Spinner';
import pesticideManger from '../managers/pesticide'

import { noSession } from "../utils/session";
import deepcopy from '../../../utils/deepcopy';


const MODE_AUTO = true;
const MODE_MANUAL = false;
const ERROR = {code:273,
data:null,
message:'방제 동작 정지'}
class Pesticide extends Component {
    constructor(props) {
        super(props);
        
        this.moreMenuRefs = new Map();
        this.pesticideRef = React.createRef();
        this.isOpened = false;
        this.state = {

            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            mode: window.mode.mode,
            pesticide: [],
            pesticideCount: 0,
            offset: 0,
            size: constant.defaultLoadSize,
            loading: {},
            autoControlCount: 0,

        };        
    }

    componentDidMount() {                              
        this.findpesticideManger();
        this.searchPage(0)        
        this.props.setPowerScrollCallback(this.scrollCallback);         
        addListener(this, this.listener);        
    }

    componentWillUnmount() {
 
        const socket = this.props.socket;
        socket.removeListener('pest-control');
        socket.removeListener('pest-control-list');
        socket.removeListener('pest-control-fail');
    }



    removepesticide = (e, index, item) => {
        const ref = this.moreMenuRefs.get(index);
        const _this = this;
        ref.blur();

        this.props.showDialog({
            alertText: "방제기를 삭제하시겠습니까?",
            cancelText: "취소",
            submitText: "확인",
            submitCallback: () => {
                pesticideManger.remove(item.id, noSession(this.props.showDialog)((status, data) => {
                    if (status === 200) {
                        _this.props.showDialog({
                            alertText: "삭제되었습니다.",
                        });
                    } else {
                        _this.props.alertError(status, data);
                    }
                }));
            }
        })
    };

    listener = (socket) => {
        on(socket)('pest-control', (data) => {
            let pesticide = deepcopy(this.state.pesticide)
            if (typeof (data.is_auto) == 'undefined') {                
               
                this.state.pesticide.forEach((item, index) => {
                    if (item.id === data.key) {
                        pesticide[index].state = data.state
                        this.setState({ pesticide })
                        return;
                    }
                })
            } else {
                this.state.pesticide.forEach((item, index) => {
                    if (item.id === data.key) {
                        pesticide[index].is_auto = data.is_auto
                        this.setState({ pesticide })
                        return;
                    }

                })
            }
        });

        on(socket)('pest-control-list', (data) => {
            this.findpesticideManger();
        })
        on(socket)('pest-control-fail', (data) => {
            this.props.alertError(400,ERROR);
          
        });
    };

    findpesticideManger = () => {
        const _this = this;
        let query = {
            house_id: this.state.house_id,
        };
        pesticideManger.findAll(query, noSession(this.props.showDialog)((status, data) => {    
            if (status === 200) {
                _this.setState({
                    pesticide: data.data.rows,
                    pesticideCount: data.data.count
                });
            } else if (status === 404) {
                if(data.code=='Not Found'){
                    return;
                }
                this.setState({
                    pesticide: [],
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    }


    changeHouseId = (house_id) => {
        const _this = this;
        this.setState({ house_id }, () => {
            _this.searchPage(0);

        });
    };

    scrollCallback = (refName) => {
        // console.log(this[REF_NAMES[refName]]);
        // this[REF_NAMES[refName]].current.scrollIntoView({behavior: 'smooth'});
        this.pesticideRef.current.scrollIntoView({ behavior: "smooth" });
    };

    searchPage = (offset) => {
        const _this = this;

        this.setState({
            offset: offset
        });

        let query = {
            house_id: this.state.house_id,
            offset: offset,
            size: this.state.size
        };

        pesticideManger.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    loading: {},
                    pesticide: data.data.rows,
                    pesticideCount: data.data.count
                });
            } else if (status === 404) {
                if(data.code=='Not Found'){
                    return;
                }
                _this.setState({
                    loading: {},
                    pesticide: [],
                    pesticideCount: 0
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    clickLabel = (e, val, pesticide, index) => {
        e.preventDefault();
        const _this = this;

        let currentState = pesticide.state;
        if (currentState === val) {
            return false;
        }

        let body = {
            id: pesticide.id,
            state: val,
            index: index,
            key: pesticide.id,
        };

        this.loading(index, true);
        setTimeout(()=>{
            this.loading(index, false);
        },5000)
        pesticideManger.active(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                _this.props.alertError(status, data);
            }
        
        }));
    };

    releaseLoadingByTimeout = (key) => {
        let temp = Object.assign({}, this.state.loading);
        const _this = this;
        if (temp[key]) {
            setTimeout(() => {
                temp[key] = false;
                _this.setState({
                    loading: temp
                });
            }, 2500);
        }
    };

    loading = (index, isLoading) => {
        let loading = Object.assign({}, this.state.loading);
        for(let i=0; i<this.state.pesticide.length; i++)
        {
            if (isLoading) {
                loading[i] = true;
                this.releaseLoadingByTimeout(index);
            } else {
                delete loading[i];
            }
        }
        this.setState({ loading });
    };
    returnModeString = (mode) => {
        if (mode === MODE_AUTO) {
            return "자동";
        } else if (mode === MODE_MANUAL) {
            return "수동";
        }
    };

    returnBooleanByModeState = (mode) => {
        if (mode === MODE_AUTO) {
            return false;
        } else if (mode === MODE_MANUAL) {
            return true;
        }
    };



    openPesticide = (e) => {
        const _this = this;
        return _this.props.openPesticide(this.state.house_id)
    };

    openPesticidemake = (e, index) => {
        const ref = this.moreMenuRefs.get(index);
        document.removeEventListener('keyup', this.closeOnEscape);
        let pesticide = deepcopy(this.state.pesticide[index])

        this.props.openPesticidemake(pesticide)
        ref.blur();
    };
    openpesticideDeatail = (e, index) => {
        const ref = this.moreMenuRefs.get(index);
        document.removeEventListener('keyup', this.closeOnEscape);
        let pesticide = deepcopy(this.state.pesticide[index])

        this.props.openPesticideDeatail(pesticide)
        ref.blur();

    };
    clickAutoStateLabel = (e, index, mode) => {
        e.preventDefault();
        const _this = this;
        let body = {};
        let pesticides = JSON.parse(JSON.stringify(this.state.pesticide));
        let pesticide = pesticides[index];

        body.id = pesticide.id;
        body.is_auto = !mode;
        body.key = pesticide.id;

        // if(mode === "auto" && this.state.remoteState === "field") {
        //     return this.props.showDialog({
        //         alertText: "자동제어는 원격모드 '원격' 에서만 가능합니다."
        //     });
        // }
        pesticideManger.modeupdate(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                _this.props.alertError(status, data);
            }

        }));
    };

    render() {

        return (
            <Fragment>
                {

                    <section className="lc-card" id="lcpesticideWrap" ref={this.pesticideRef} style={{ zIndex: 10 }}>
                        <div className="lc-card-header">
                            <h2 className="lc-card-title">방제기</h2>
                            <button className="card-header-button" onClick={e => this.openPesticide()}><span>설정</span></button>
                        </div>

                        <div className="lc-card-content-wrap">
                            {this.state.pesticide.length ?
                                <ul className="lc-pesticide-list">
                                    {this.state.pesticide.map((rows, index) => {
                                        return (
                                            <li key={index}>
                                                <article className="lc-content-card">
                                                    <Spinner spinning={this.state.loading[index]} />

                                                    <div className="content-position-wrap">
                                                        <div className={"pesticide-content-wrap" + (rows.is_frost != true ? " lc-non-individual-mode" : "")}>
                                                            <div className="pesticide-text-count-wrap">
                                                                <p className="pesticide-text">{rows.pest_control_name}</p>

                                                            </div>

                                                            <div className="flex-line-break" />

                                                            <div className="pesticide-dialog-percentage-wrap">
                                                                <div className="lc-toggle-wrapper">

                                                                    <label className="switch"
                                                                        onClick={e => this.clickAutoStateLabel(e, index, rows.is_auto)}>
                                                                        <input type="checkbox"
                                                                            checked={this.returnBooleanByModeState(rows.is_auto)}
                                                                            onChange={() => {
                                                                            }} />
                                                                        <span className="slider round" />
                                                                    </label>

                                                                    <span
                                                                        className="switch-text">{this.returnModeString(rows.is_auto)}</span>
                                                                </div>

                                                            </div>
                                                        </div>


                                                        <div className="absolute-item-wrap">
                                                            <div className={"toggle-radio-wrap two-state lc-fat"}>
                                                                <div className="disabled-wrap" />
                                                                <input type="radio" name={"pesticide-toggle-option" + rows.id}
                                                                    id={"pesticideToggleOption" + rows.id + "_1"}
                                                                    onChange={e => {
                                                                    }}
                                                                    checked={rows.state === "run"} />

                                                                <input type="radio" name={"pesticide-toggle-option" + rows.id}
                                                                    id={"pesticideToggleOption" + rows.id + "_2"}
                                                                    onChange={e => {
                                                                    }}
                                                                    checked={rows.state === "stop"} />

                                                                <label
                                                                    onClick={e => this.clickLabel(e, "run", rows, index)}>운전</label>
                                                                <label
                                                                    onClick={e => this.clickLabel(e, "stop", rows, index)}>정지</label>
                                                                <i className="toggle-slider-icon" />
                                                            </div>
                                                        </div>
                                                        {
                                                            rows.is_auto !== "auto" && (
                                                                <div className="three-dot-menu" tabIndex="0" ref={c => this.moreMenuRefs.set(index, c)} >
                                                                    <div className="icon-wrap">
                                                                        <i /><i /><i />
                                                                    </div>
                                                                    <div className="three-dot-view-more-menu">
                                                                        <ul>
                                                                            <li><span onClick={(e) => this.openPesticidemake(e, index)}>자동 옵션 설정</span></li>
                                                                            <li><span onClick={(e) => this.openpesticideDeatail(e, index)}>수정</span></li>
                                                                            <li><span onClick={(e) => this.removepesticide(e, index, rows)} >삭제</span></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            )
                                                        }
                                                    </div>

                                                </article>
                                            </li>
                                        )
                                    })
                                    }
                                </ul> : null
                            }
                            {!this.state.pesticide.length ?
                                <p>데이터가 없습니다.</p> : null
                            }
                        </div>
                        {this.state.pesticideCount && (this.state.pesticideCount > this.state.size) ?
                            <Pagination count={this.state.pesticideCount}
                                offset={this.state.offset}
                                size={this.state.size}
                                searchPage={this.searchPage} />
                            : null
                        }

                    </section>

                }
            </Fragment>
        );
    }
}

export default useNavScroll(
    ({ actions }) => ({
        setPowerScrollCallback: actions.setPowerScrollCallback
    })
)(useHouse(
    ({ actions }) => ({
        getHouseId: actions.getHouseId,
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(usePesticide(
    ({ actions }) => ({
        openPesticide: actions.open
    })
)(usePesticideautomake(
    ({ actions }) => ({
        openPesticidemake: actions.open
    })
)(usepesticideDeatail(
    ({ actions }) => ({
        openPesticideDeatail: actions.open
    })
)(useSocket(
    ({ state, actions }) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(Pesticide)))))));
