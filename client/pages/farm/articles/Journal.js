import React, {Component} from 'react';
import {useJournal} from "../contexts/modals/journal";
import journalManager from '../managers/journal';
import constant from "../constants/constant";
import {useAlert} from "../contexts/alert";
import {useSocket} from "../contexts/socket";
import {useHouse} from "../contexts/house";
import {useNavScroll} from "../contexts/navScroll";
import Pagination from "../components/Pagination";
import {addListener, on} from "../utils/socket";
import {date} from "../../../utils/filter";
import {colourStyles} from "../utils/select";
import Select from 'react-select';

const DATE_FILTER_FORMAT = 'yyyy.MM.dd HH:mm:ss';
const TYPE_REPORT = 'report';
const JOURNAL_TYPES = [
    { value: 'motor,power,control,warning,error,auto,report,noti', label: '전체'},
    { value: 'motor,power,control,warning,error,auto,noti', label: '자동기록'},
    { value: 'report', label: '수기작성'},
    { value: 'warning', label: '알람'}
];

import TRANSLATE from '../constants/translate';
import {noSession} from "../utils/session";

class Journal extends Component {
    constructor(props) {
        super(props);

        this.reportRef = React.createRef();
        this.instance = null;
        this.state = {
            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            journals: [],
            journalCount: 0,
            offset: 0,
            size: constant.defaultLoadSize,
            journalType: JOURNAL_TYPES[0],
            grade : window.user ? (window.user.grade?window.user.grade:'member') : "member"
        };
    }

    componentDidMount() {
        this.searchPage(0);
        this.props.setJournalCallback(this.changeHouseId);
        this.props.setReportScrollCallback(this.scrollCallback);
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('report');
    };

    openCreateJournal = () => {
        this.props.openJournalModal({
            house_id: this.state.house_id
        }, this.searchPage);
    };

    listener = (socket) => {
        const _this = this;
        on(socket)('report', () => {
            if (_this.instance) {
                clearTimeout(_this.instance);
            }
            _this.instance = setTimeout(() => _this.searchPage(_this.state.offset), 300);
        });
    };

    changeHouseId = (house_id) => {
        const _this = this;
        this.setState({house_id}, () => {
            _this.searchPage(0);
        });
    };

    scrollCallback = () => {
        this.reportRef.current.scrollIntoView({behavior: "smooth"});
    };

    searchPage = (offset, journalType) => {
        const _this = this;
        this.setState({
            offset: offset
        });

        let query = {
            house_id: this.state.house_id,
            offset: offset,
            size: this.state.size
        };

        if(journalType) {
            query.type = journalType.value
        } else {
            query.type = _this.state.journalType.value
        }

        journalManager.findAll(query, noSession(this.props.showDialog)((status, {data}) => {
            if(status === 200) {
                _this.setState({
                    journals: data.rows,
                    journalCount: data.count
                });
            } else if (status === 404) {
                _this.setState({
                    journals: [],
                    journalCount: 0
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    removeJournal = (e, item) => {
        const _this = this;
        _this.props.showDialog({
            alertText: "작성한 일지를 삭제하시겠습니까?",
            cancelText: "취소",
            submitText: "확인",
            submitCallback: () => {
                journalManager.remove(item.id, noSession(this.props.showDialog)((status, data) => {
                    if(status === 200) {
                        _this.props.showDialog({
                            alertText: "삭제되었습니다.",
                        });
                    } else {
                        _this.props.alertError(status, data);
                    }
                }));
            }
        })
    };

    handleSelect = (journalType) => {
        this.setState({journalType});
        this.searchPage(0, journalType);
    };

    render() {
        return (
            <section className="lc-card" id="lcJournalWrap" ref={this.reportRef}>
                <div className="lc-card-header">
                    <h2 className="lc-card-title">영농일지</h2>
                    {this.state.grade=='owner'?<button className="card-header-button" onClick={this.openCreateJournal}><span>일지 작성</span></button>:null}
                </div>

                <div className="lc-card-content-wrap select-module-wrap">
                    {this.state.grade=='owner'?
                    <Select value={this.state.journalType}
                            onChange={this.handleSelect}
                            isSearchable={ false }
                            options={JOURNAL_TYPES}
                            styles={colourStyles}/>:null}
                    { this.state.journals.length ?
                        <ul className="lc-journal-list">
                            {
                                this.state.journals.map((item, index) => {
                                    return (
                                        <li key={index}>
                                            <article className="lc-content-card">
                                                {
                                                    item.type === TYPE_REPORT ?
                                                        <button type="button" className="lc-card-close-btn" onClick={e => this.removeJournal(e, item)}/> : null
                                                }
                                                <p><strong className={item.type === "error" ? "lc-red" : ""}>{TRANSLATE.JOURNAL[item.type]}</strong> <span>{date(item.created_at, DATE_FILTER_FORMAT)}</span></p>
                                                <p className="journal-content">{item.contents}</p>
                                            </article>
                                        </li>
                                    )
                                })
                            }
                        </ul> : null
                    }
                    { !this.state.journals.length ?
                        <p>{this.state.journalType.label} 데이터가 없습니다.</p> : null
                    }
                </div>

                { this.state.journalCount && (this.state.journalCount > this.state.size) ?
                    <Pagination count={this.state.journalCount}
                                offset={this.state.offset}
                                size={this.state.size}
                                searchPage={this.searchPage}/>
                    : null
                }
            </section>
        )
    }
}

export default useNavScroll(
    ({actions}) => ({
        setReportScrollCallback: actions.setReportScrollCallback
    })
)(useHouse(
    ({actions}) => ({
        setJournalCallback: actions.setJournalCallback,
        getHouseId: actions.getHouseId,
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useJournal(
    ({actions}) => ({
        openJournalModal: actions.open,
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(Journal)))));
