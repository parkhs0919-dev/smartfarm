import React, {Component} from 'react';
import {useSensorGather} from "../contexts/modals/sensorGather";
import {useIntegratedGraph} from "../contexts/modals/integratedGraph";
import {getQuery} from "../../../utils/route";

class TotalGraph extends Component {
    constructor(props) {
        super(props);

        this.state = {};
        this.isOpened = false;
    }

    componentDidMount() {
    }

    shouldComponentUpdate(nextProps, nextState){
        let query = getQuery();
        if(query.modal === "chart" && nextProps.IntegratedGraphSyncCallback && !this.isOpened) {
            this.isOpened = true;
            this.props.openIntegratedGraphModal();
        }
        return true;
    }

    render() {
        return (
            <section className="lc-card" id="lcTotalGraphWrap">
                <div className="lc-card-header">
                    <h2 className="lc-card-title">통합 그래프</h2>
                    <div className="total-graph-header-wrap">
                        <button className="card-header-button" onClick={this.props.openSensorGatherModal}><span>센서 모아보기</span></button>
                        <button className="card-header-button" onClick={this.props.openIntegratedGraphModal}><span>분석</span></button>
                    </div>
                </div>
            </section>
        )
    }
}

export default useIntegratedGraph(
    ({state, actions}) => ({
        IntegratedGraphSyncCallback: state.syncCallback,
        openIntegratedGraphModal: actions.open
    })
)(useSensorGather(
    ({actions}) => ({
        openSensorGatherModal: actions.open
    })
)(TotalGraph));