import React, { Component, Fragment } from 'react';

import { useSocket } from "../contexts/socket";
import { addListener, on } from "../utils/socket";
import href from '../../../utils/href';

function setSize (e) {
   document.getElementById('lcFluidIframe').height = e.data.height;
}

class FluidIframe extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fluidHost: 'http://' + window.fluidHost,
        };
    }

    componentDidMount() {
        addListener(this, this.listener);
        window.addEventListener('message', e => {
            if(e.data.height){
                setSize(e);
            }
            if(e.data.host){
                href(this.state.fluidHost + e.data.host);
            }
        });
    }

    componentWillUnmount() {
        window.removeEventListener('message', setSize);
    }

    listener = () => {

    };

    render() {

        return (
            <Fragment>
                {

                    <section className="lc-card" id="lcFluidIframeWrap">
                        <iframe
                            id="lcFluidIframe"
                            src={this.state.fluidHost + '/fluid-iframe'}
                            width="100%"
                            frameBorder="0"
                            scrolling="no"
                        />
                    </section>

                }
            </Fragment>
        );
    }
}

export default useSocket(
    ({ state, actions }) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(FluidIframe);
