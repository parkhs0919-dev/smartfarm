import React, {Component, Fragment} from 'react';
import {useAlert} from "../contexts/alert";
import {useSocket} from "../contexts/socket";
import {useHouse} from "../contexts/house";
import {useSensorAlarmlist} from "../contexts/modals/SensorAlarmlist";
import {useSensorGraph} from "../contexts/modals/sensorGraph";
import { useSensorGroup } from '../contexts/modals/sensorgroup'
import {useNavScroll} from "../contexts/navScroll";
import {addListener, on} from "../utils/socket";
import {setValue, compareWithAlarms} from "../utils/sensorHandler";
import {useLiterAutoReset} from '../contexts/modals/LiterAutoReset'
import { useLiterDetail } from "../contexts/modals/LiterDetail";
import Pagination from '../components/Pagination';
import sensorManager from '../managers/sensor';
import constant from '../constants/constant';
import alarmManager from "../managers/alarm";
import {sanitize} from "../utils/sensor";
import {noSession} from "../utils/session";
import {getQuery} from "../../../utils/route";
const OUTER_SENSOR_POSITION = "out";
const TYPE_LITER = "liter";
const OUTER_SENSOR_TYPE = constant.sensorType.outerSensorType
const KORINSRAINFALL = 'korinsCumulativeRainFall';
const SENSOR_ALARM_SOUND = constant.SENSOR_ALARM_SOUND;
class OuterSensor extends Component {
    constructor(props) {
        super(props);

        this.audio = null;
        this.isOpened = false;
        this.outerSensorRef = React.createRef();
        this.alarmHash = {};
        this.state = {
            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            outerSensors: [],
            outerSensorCount: 0,
            outerAlarms: [],
            outerAlarmCount: 0,
            offset: 0,
            size: constant.defaultLoadSize,
            grade : window.user ? (window.user.grade?window.user.grade:'member') : "member",
            delay : 0,
            is_use_alarm : false,
        };
    }

    componentDidMount() {
        this.searchPage(0);
        this.props.setOuterSensorCallback(this.changeHouseId);
        this.props.setOuterSensorScrollCallback(this.scrollCallback);
        addListener(this, this.listener);
        this.getAlarms();
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('outer-sensor-values');
        socket.removeListener('outer-alarm');
        socket.removeListener('sensors');
        socket.removeListener('alarm-sound');

        if(this.audio) {
            this.audio.pause();
            this.audio.currentTime = 0;
        }
    }

    shouldComponentUpdate(nextProps, nextState){
        let query = getQuery();
        if(query.modal === "alarm" && nextProps.alarmSyncCallback && !this.isOpened) {
            this.isOpened = true;
            this.props.openSensorAlarmModal({
                key: "outer",
                house_id: this.state.house_id
            });
        }
        return true;
    }

    listener = (socket) => {
        const _this = this;
        on(socket)('outer-sensor-values', ({outerSensors}) => {
            _this.setState({
                outerSensors: setValue(outerSensors, _this.state.outerSensors, _this.alarmHash)
            })
        });
        on(socket)('sensors', () => {
            _this.searchPage(this.state.offset);
        });
        on(socket)('outer-alarm', () => {
            _this.getAlarms();
        });

        on(socket)('alarm-sound', (data) => {
            _this.playSound(data);
        });

        on(socket)('house', (data) => {
            if(data.house_id === _this.state.house_id){
                _this.setState({is_use_alarm : data.is_use_alarm});
            }
        });
    };



    playSound = (data) => {
        const alarm = data.alarm;
        const sensor = alarm.sensor;
        if(sensor.type === 'windDirection' || sensor.type === 'power' || sensor.type ==='water' || sensor.type === 'window' || sensor.type === 'fire' || sensor.type === 'door' || sensor.type === 'korinsWindDirection'){
            if (!this.audio || this.audio.ended) {
                this.audio = new Audio(SENSOR_ALARM_SOUND['etc']);
                this.audio.play();
            }
        }else if(sensor.type === 'rain'){
            if(alarm.value == '0') {
                if (!this.audio || this.audio.ended) {
                    this.audio = new Audio(SENSOR_ALARM_SOUND['rainon']);
                    this.audio.play();
                }
            } else {
                if (!this.audio || this.audio.ended) {
                    this.audio = new Audio(SENSOR_ALARM_SOUND['rainoff']);
                    this.audio.play();
                }
            }
        }else{
            if(alarm.condition === 'over'){
                if (!this.audio || this.audio.ended) {
                    this.audio = new Audio(SENSOR_ALARM_SOUND['over']);
                    this.audio.play();
                }
            }else if(alarm.condition === 'under'){
                if (!this.audio || this.audio.ended) {
                    this.audio = new Audio(SENSOR_ALARM_SOUND['under']);
                    this.audio.play();
                }
            }
        }
    };

    changeHouseId = (house_id) => {
        const _this = this;
        this.setState({house_id}, () => {
            _this.searchPage(0);
            _this.getAlarms();
        });
    };

    searchPage = (offset) => {
        const _this = this;
        this.setState({
            offset: offset
        });

        let query = {
            house_id: this.state.house_id,
            position: OUTER_SENSOR_POSITION,
            offset: offset,
            size: this.state.size,
            types:OUTER_SENSOR_TYPE
        };

        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    outerSensors: setValue(null, data.data.rows, _this.alarmHash),
                    outerSensorCount: data.data.count
                });
            } else if (status === 404) {
                _this.setState({
                    outerSensors: [],
                    outerSensorCount: 0
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    openSensorAlarmModal = (e) => {
        if(this.state.outerSensorCount === 0) {
            this.props.showDialog({
                alertText: "설정 가능한 센서가 없습니다."
            })
        } else {
            this.props.openSensorAlarmModal({
                key: "outer",
                house_id: this.state.house_id
            });
        }
    };

    openSensorGroupModal =(e)=>{
        let body = {
            position:'outer',
            house_id:this.state.house_id,
        }
        this.props.openSensorGroupModal(body)
    }

    openSensorGraphModal = (e, item) => {     
        if(item.type === TYPE_LITER) {
            let body={item:item,house_id:this.state.house_id}
            this.props.showDialog({
                alertText: "기능을 선택해주세요.",
                cancelText: "자동 리셋 설정",
                cancelCallback: () => {
                    this.props.openLiterAutoReset(body);
                },
                submitText: "상세 설정",
                submitCallback: () => {
                    this.props.openLiterDetail(body);
                    
                }
            });
            // this.props.showDialog({
            //     alertText: "기능을 선택해주세요.",
            //     cancelText: "그래프 보기",
            //     cancelCallback: () => {
            //         this.props.openSensorGraphModal(item);
            //     },
            //     submitText: "유량계 초기화",
            //     submitCallback: () => {
            //         this.clearLiter(item);
            //     }
            // });
        } else if(item.type ==KORINSRAINFALL){
            this.props.showDialog({
                alertText: "기능을 선택해주세요.",
                cancelText: "그래프 보기",
                cancelCallback: () => {
                    this.props.openSensorGraphModal(item);
                },
                submitText: "강우량 초기화",
                submitCallback: () => {
                    this.props.showDialog({
                        alertText: "강우량을 초기화 하시겠습니까?.",
                        cancelText: "확인",
                        cancelCallback: () => {
                            this.clearRain(item);
                        },
                        submitText: "취소",
                        submitCallback: () => {
                        }
                    })
                   
                }
            });
        } else  {
            this.props.openSensorGraphModal(item);
        }
    };

    clearLiter = (item) => {
        const _this = this;
        sensorManager.resetLiter(item.id, (status, data) => {
            if(status === 200) {
                _this.props.showDialog({
                    alertText: "초기화되었습니다."
                })
            } else {
                _this.props.alertError(status, data);
            }
        });
    };

    clearRain = (item) => {
        const _this = this;
        
        sensorManager.resetRain(item.id, (status, data) => {            
            if(status === 200) {
                _this.props.showDialog({
                    alertText: "초기화되었습니다."
                })
            } else {
                _this.props.alertError(status, data);
            }
        });
    };

    scrollCallback = () => {
        this.outerSensorRef.current.scrollIntoView({behavior: "smooth"});
    };

    getAlarms = () => {
        const _this = this;
        let query = {
            house_id: this.state.house_id,
            position: "out"
        };

        alarmManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.generateAlarmsToHash(data.data.rows, (hash) => {
                    let outerSensors = setValue(null, _this.state.outerSensors, hash);
                    _this.setState({
                        outerSensors: outerSensors,
                        outerAlarmCount: data.data.count,
                        outerAlarms: data.data.rows
                    });
                });
            } else if (status === 404) {
                _this.setState({
                    outerAlarmCount: 0,
                    outerAlarms: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    generateAlarmsToHash = (alarms, callback) => {
        let hash = {};
        if(alarms.length) {
            for(let i=0; i<alarms.length; i++) {
                if(hash[alarms[i].sensor_id] === undefined) {
                    hash[alarms[i].sensor_id] = alarms[i];
                } else {
                    if(Array.isArray(hash[alarms[i].sensor_id])) {
                        hash[alarms[i].sensor_id].push(alarms[i]);
                    } else {
                        let temp = Object.assign({}, hash[alarms[i].sensor_id]);
                        hash[alarms[i].sensor_id] = [];
                        hash[alarms[i].sensor_id].push(temp);
                        hash[alarms[i].sensor_id].push(alarms[i]);
                    }
                }
            }
        }
        this.alarmHash = hash;
        if(callback) callback(hash);
    };

    render() {
        return (
            <Fragment>
                {
                    this.state.outerSensors && this.state.outerSensors.length ?
                        (
                            <section className="lc-card position-relative" id="lcOuterSensorWrap" ref={this.outerSensorRef}>

                                <div className="lc-card-header">
                                    <h2 className="lc-card-title">외부센서</h2>
                                    {this.state.grade=='owner'?<button className="card-header-button zoom-graph" onClick={this.openSensorGroupModal}><span>센서그룹 생성</span></button>:null}
                                    {this.state.grade=='owner'?<button className="card-header-button" onClick={this.openSensorAlarmModal}><span>경보설정</span><span className="number-badge">{this.state.outerAlarmCount}</span></button>:null}
                                </div>

                                <div className="lc-card-content-wrap">

                                    { this.state.outerSensors.length ?
                                        <ul className="sensor-list">
                                            {
                                                this.state.outerSensors.map((item, index) => {
                                                    return (
                                                        <li className="sensor-li" key={index}>
                                                            <article className={`lc-content-card lc-sensor-${item.type}`} onClick={e => this.openSensorGraphModal(e, item)}>
                                                                <p className="content-title">{item.sensor_name}</p>
                                                                <p className={item.isWarning ? "content-value lc-warning" : "content-value"}>{sanitize(item)}</p>
                                                            </article>
                                                        </li>
                                                    )
                                                })
                                            }
                                        </ul> : null
                                    }
                                    { !this.state.outerSensors.length ?
                                        <p>데이터가 없습니다.</p> : null
                                    }
                                </div>
                                { this.state.outerSensorCount && (this.state.outerSensorCount > this.state.size) ?
                                    <Pagination count={this.state.outerSensorCount}
                                                offset={this.state.offset}
                                                size={this.state.size}
                                                searchPage={this.searchPage}/>
                                    : null
                                }
                            </section>
                        ) : null
                }
            </Fragment>
        )
    }
}

export default useNavScroll(
    ({actions}) => ({
        setOuterSensorScrollCallback: actions.setOuterSensorScrollCallback
    })
)(useSensorGraph(
    ({actions}) => ({
        openSensorGraphModal: actions.open
    })
)(useSensorAlarmlist(
    ({state, actions}) => ({
        alarmSyncCallback: state.syncCallback,
        openSensorAlarmModal: actions.open
    })
)(useSensorGroup(
    ({ actions }) => ({
        openSensorGroupModal: actions.open
    })
)(useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId,
        setOuterSensorCallback: actions.setOuterSensorCallback,
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useLiterAutoReset(
    ({ actions }) => ({
        openLiterAutoReset: actions.open
    })
)(useLiterDetail(
    ({ actions }) => ({
        openLiterDetail: actions.open
    })
)(OuterSensor)))))))));
