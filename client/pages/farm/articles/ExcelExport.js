import React, {Component} from 'react';
import {colourStyles} from "../utils/select";

import {useAlert} from "../contexts/alert";
import {useSocket} from "../contexts/socket";
import {useHouse} from "../contexts/house";

import moment from 'moment';
import {date} from "../../../utils/filter";
import DatePicker from 'react-datepicker';
import Select from 'react-select';
import deepcopy from "../../../utils/deepcopy";
import {addListener, on} from "../utils/socket";
import {generateQuery} from "../../../utils/http";
import {setValue} from "../utils/sensorHandler";
import sensorManager from '../managers/sensor';
import exportManager from '../managers/export';

import Spinner from "../components/Spinner";

import resource from '../constants/resource';
import {noSession} from "../utils/session";

const DATE_DISPLAY_FORMAT = "yyyy-MM-dd";

const selectOptionExample = [
    { value: 'report', label: '영농일지'},
    { value: 'sensor', label: '센서', color: '#83c772'}
];

const INNER_SENSOR_POSITION = "in";
const OUTER_SENSOR_POSITION = "out";

class ExcelExport extends Component {
    constructor(props) {
        super(props);

        this.state = {
            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            startDate: moment(date(new Date(), DATE_DISPLAY_FORMAT)),
            endDate: moment(date(new Date(), DATE_DISPLAY_FORMAT)),
            selectedOption: null,
            excelSelectOptions: [],
            datePickerOpenState: {
                startDate: false,
                endDate: false
            }
        };
    }

    componentDidMount() {
        this.props.setExcelCallback(this.changeHouseId);
        this.findAllSensors();
        addListener(this, this.listener);
    }

    changeHouseId = (house_id) => {
        const _this = this;
        this.setState({house_id}, () => {
            _this.findAllSensors();
        });
    };

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('sensors');
    }

    listener = (socket) => {
        const _this = this;
        on(socket)('sensors', () => {
            // _this.searchPage(this.state.offset);
        });
    };

    handleChange = (selectedOption) => {
        
        this.setState({ selectedOption });
    };

    findAllSensors = () => {
        const _this = this;
        let query = {
            house_id: this.state.house_id,
        };

        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.generateExcelSelectOption(data.data.rows);
            } else if (status === 404) {
                _this.generateExcelSelectOption(null);
            } else {

            }
        }));
    };

    generateExcelSelectOption = (sensors) => {
        let selectOption = [
            { value: 'report', type: 'report', label: '영농일지'},
            { value: 'warning', type: 'warning', label: '알람'},
        ];
        if(sensors && sensors.length) {
            sensors.forEach((item) => {
                let obj = {
                    value: item.id,
                    type: "log",
                    label: "센서 - "+ item.sensor_name
                };
                selectOption.push(obj);
            });
        }
        this.setState({
            excelSelectOptions: selectOption
        });
    };

    exportExcel = (e) => {
        const _this = this;
        if(!this.state.selectedOption) {
            return _this.props.showDialog({
                alertText: "유형을 선택해주세요."
            });
        }

        if(!this.state.startDate) {
            return _this.props.showDialog({
                alertText: "조회시작일을 선택해주세요."
            });
        }

        if(!this.state.endDate) {
            return _this.props.showDialog({
                alertText: "조회종료일을 선택해주세요."
            });
        }

        if((new Date(this.state.endDate).getTime() - new Date(this.state.startDate).getTime()) > (1000*3600*24*31)){
            return _this.props.showDialog({
                alertText: "최대 31일 까지의 기간만 조회 가능합니다."
            });
        }

        if(new Date(this.state.startDate).getTime() > new Date(this.state.endDate).getTime()){
            return _this.props.showDialog({
                alertText: "조회일을 확인해주세요."
            });
        }

        let query = {
            startDate: date(new Date(this.state.startDate), DATE_DISPLAY_FORMAT),
            endDate: date(new Date(this.state.endDate), DATE_DISPLAY_FORMAT),
            type: this.state.selectedOption.type
        };
        if(this.state.selectedOption.type === "log") {
            query.sensor_id = this.state.selectedOption.value;
        } else if (this.state.selectedOption.type === "report" || this.state.selectedOption.type === "warning") {
            query.house_id = this.props.getHouseId();
        }
        //todo excel export api 연동

        
        this.setState({loading : true});

        exportManager.exportItem(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200){
                window.open(data.data, '_blank');
                this.setState({loading : false});
            }
        }))
        //window.open(`${resource.EXPORT}${generateQuery(query)}`, '_blank');
    };

    handleDate = (val, key) => {
        let state = Object.assign({}, this.state);
        state[key] = moment(date(new Date(val), 'yyyy-MM-dd'));
        this.setState(state);
        this.handleDatePickerState(null, key);
    };

    handleDatePickerState = (e, key) => {
        e && e.preventDefault();
        let datePickerOpenState = Object.assign({}, this.state.datePickerOpenState);
        datePickerOpenState[key] = !datePickerOpenState[key];
        this.setState({datePickerOpenState});
    };

    closeDatePickerOnTouchOutside = (e, key) => {
        let datePickerOpenState = deepcopy(this.state.datePickerOpenState);
        datePickerOpenState[key] = false;
        this.setState({datePickerOpenState});
    };

    //todo 시작일은 아무때나가능, 종료일은 시작일기준 max 90일이지만 오늘을 넘기면안됨.
    returnDatePickerMaxDate = () => {
        if(this.state.startDate) {
            if(new Date().getTime() < new Date(new Date(this.state.startDate).setDate(this.state.startDate.getDate()+31)).getTime()) {
                return moment(date(new Date()), DATE_DISPLAY_FORMAT);
            } else {
                return moment(date(new Date(new Date(this.state.startDate).setDate(new Date(this.state.startDate).getDate()+31))));
            }
        } else {
            return moment(date(new Date()), DATE_DISPLAY_FORMAT);
        }
    };

    render() {
        return (
            <section className="lc-card" id="lcExcelExportWrap">
                <Spinner spinning={this.state.loading}/>
                <div className="lc-card-header">
                    <h2 className="lc-card-title">엑셀 다운로드</h2>
                </div>
                <div className="lc-card-content-wrap">
                    <div className="content-row-wrap">
                        <div className="content-item-wrap">
                            <div className="date-picker-wrap start">
                                {/*<label className="toggle-date-picker-label" onClick={e => this.handleDatePickerState(e, "startDate")}>*/}
                                {/*    <input type="text" readOnly value={this.state.startDate ? date(this.state.startDate, DATE_DISPLAY_FORMAT) : ''}/>*/}
                                {/*</label>*/}
                                {/*{*/}
                                this.state.datePickerOpenState.startDate && (
                                <DatePicker dateFormat={"YYYY-MM-DD"}
                                            onClickOutside={e => this.closeDatePickerOnTouchOutside(e, 'startDate')}
                                            className="lc-farmlabs-datepicker"
                                            popperClassName="lc-farmlabs-datepicker-popper"
                                            selected={this.state.startDate}
                                            onChange={val => this.handleDate(val, "startDate")}/>
                                {/*    )*/}
                                {/*}*/}
                            </div>
                        </div>
                        <div className="content-item-wrap">
                            <div className="date-picker-wrap end">
                                {/*<label className="toggle-date-picker-label" onClick={e => this.handleDatePickerState(e, "endDate")}>*/}
                                {/*    <input type="text" readOnly value={this.state.endDate ? date(this.state.endDate, DATE_DISPLAY_FORMAT) : ''}/>*/}
                                {/*</label>*/}
                                {/*{*/}
                                {/*    this.state.datePickerOpenState.endDate && (*/}
                                <DatePicker dateFormat={"YYYY-MM-DD"}
                                            onClickOutside={e => this.closeDatePickerOnTouchOutside(e, 'endDate')}
                                            className="lc-farmlabs-datepicker"
                                            popperClassName="lc-farmlabs-datepicker-popper right"
                                            selected={this.state.endDate}

                                            onChange={val => this.handleDate(val, "endDate")}/>
                                {/*    )*/}
                                {/*}*/}
                            </div>
                        </div>
                    </div>
                    <div className="content-row-wrap">
                        <div className="content-item-wrap select-module-wrap">
                            <Select value={this.state.selectedOption}
                                    placeholder="다운로드 항목 선택"
                                    onChange={this.handleChange}
                                    isSearchable={ false }
                                    options={this.state.excelSelectOptions}
                                    styles={colourStyles}/>
                        </div>
                        <div className="content-item-wrap">
                            <button type="button" className="download-btn" onClick={this.exportExcel}>다운로드</button>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId,
        setExcelCallback: actions.setExcelCallback,
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(ExcelExport)));