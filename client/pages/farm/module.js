import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';

import App from './App';
import {stop} from "../../utils/preload";


import locale from '../../utils/moment';

locale();

const Root = () => (
    <BrowserRouter>
        <App/>
    </BrowserRouter>
);

ReactDOM.render(<Root/>, document.getElementById('root'), () => stop());



import 'react-datepicker/dist/react-datepicker.css';
import './assets/_farm.scss';
