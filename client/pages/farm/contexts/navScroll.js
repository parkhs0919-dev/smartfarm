import React, {Component, createContext} from 'react';
import {createUseConsumer} from '../../../utils/context';

const Context = createContext();

const {Provider, Consumer: NavScrollConsumer} = Context;

const REF_NAMES = {
    cctv: "cctvRef",
    motor: "motorRef",
    power: "powerRef",
    innerSensor: "innerSensorRef",
    outerSensor: "outerSensorRef",
    report: "reportRef"
};

class NavScrollProvider extends Component {
    state = {};
    actions = {
        triggerScrollCallback: (pageName) => {
            if(pageName === "cctv") {
                this.state.cctvScrollCallback();
            } else if (pageName === "motor") {
                this.state.motorScrollCallback();
            } else if (pageName === "power") {
                this.state.powerScrollCallback();
            } else if (pageName === "innerSensor") {
                this.state.innerSensorScrollCallback();
            } else if (pageName === "outerSensor") {
                this.state.outerSensorScrollCallback();
            } else if (pageName === "report") {
                this.state.reportScrollCallback();
            }
        },
        setCCTVScrollCallback: (cctvScrollCallback) => {
            this.setState({cctvScrollCallback});
        },
        setMotorScrollCallback: (motorScrollCallback) => {
            this.setState({motorScrollCallback});
        },
        setPowerScrollCallback: (powerScrollCallback) => {
            this.setState({powerScrollCallback});
        },
        setInnerSensorScrollCallback: (innerSensorScrollCallback) => {
            this.setState({innerSensorScrollCallback});
        },
        setOuterSensorScrollCallback: (outerSensorScrollCallback) => {
            this.setState({outerSensorScrollCallback});
        },
        setReportScrollCallback: (reportScrollCallback) => {
            this.setState({reportScrollCallback});
        }
    };

    render() {
        const {state, actions} = this;
        const value = {state, actions};
        return (
            <Provider value={value}>
                {this.props.children}
            </Provider>
        );
    }
}

const useNavScroll = createUseConsumer(NavScrollConsumer);

export {
    NavScrollProvider,
    NavScrollConsumer,
    useNavScroll
};
