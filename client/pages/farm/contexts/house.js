import React, {Component, createContext} from 'react';
import {createUseConsumer} from '../../../utils/context';
import CONSTANT from '../constants/constant';

const Context = createContext();

const {Provider, Consumer: HouseConsumer} = Context;

const defaultState = CONSTANT.defaultHouseHandlerState;

class HouseProvider extends Component {
    state = Object.assign({}, defaultState);
    actions = {
        init: () => {
            this.setState(Object.assign({}, defaultState));
        },
        changeHouse: (house_id) => {
            this.setState({
                house_id: house_id
            });
            this.state.innerSensorCallback && this.state.innerSensorCallback(house_id);
            this.state.outerSensorCallback && this.state.outerSensorCallback(house_id);
            this.state.motorCallback && this.state.motorCallback(house_id);
            this.state.powerCallback && this.state.powerCallback(house_id);
            this.state.journalCallback && this.state.journalCallback(house_id);
            this.state.modeCallback && this.state.modeCallback(house_id);
            this.state.controlCallback && this.state.controlCallback(house_id);
            this.state.cctvCallback && this.state.cctvCallback(house_id);
            this.state.excelCallback && this.state.excelCallback(house_id);
            this.state.rootCallback && this.state.rootCallback(house_id);
        },
        getHouseId: () => {
            return this.state.house_id;
        },
        setHouseId: (house_id) => {
            this.setState({house_id});
        },
        setInnerSensorCallback: (innerSensorCallback) => {
            this.setState({innerSensorCallback});
        },
        setOuterSensorCallback: (outerSensorCallback) => {
            this.setState({outerSensorCallback});
        },
        setMotorCallback: (motorCallback) => {
            this.setState({motorCallback});
        },
        setPowerCallback: (powerCallback) => {
            this.setState({powerCallback});
        },
        setJournalCallback: (journalCallback) => {
            this.setState({journalCallback});
        },
        setModeCallback: (modeCallback) => {
            this.setState({modeCallback});
        },
        setControlCallback: (controlCallback) => {
            this.setState({controlCallback});
        },
        setCCTVCallback: (cctvCallback) => {
            this.setState({cctvCallback});
        },
        setExcelCallback: (excelCallback) => {
            this.setState({excelCallback});
        },
        setRootCallback: (rootCallback) => {
            this.setState({rootCallback});
        },
    };

    render() {
        const {state, actions} = this;
        const value = {state, actions};
        return (
            <Provider value={value}>
                {this.props.children}
            </Provider>
        );
    }
}

const useHouse = createUseConsumer(HouseConsumer);

export {
    HouseProvider,
    HouseConsumer,
    useHouse
};
