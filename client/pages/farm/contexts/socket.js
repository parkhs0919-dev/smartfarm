import React, {Component, createContext} from 'react';
import {createUseConsumer} from '../../../utils/context';
import CONSTANT from '../constants/constant';

const Context = createContext();

const {Provider, Consumer: SocketConsumer} = Context;

const defaultState = CONSTANT.defaultSocketState;

class SocketProvider extends Component {
    state = Object.assign({}, defaultState);
    actions = {
        init: () => {
            this.setState(Object.assign({}, defaultState));
        },
        setSocket: (socket) => {
            this.setState({socket});
        }
    };

    render() {
        const {state, actions} = this;
        const value = {state, actions};
        return (
            <Provider value={value}>
                {this.props.children}
            </Provider>
        );
    }
}

const useSocket = createUseConsumer(SocketConsumer);

export {
    SocketProvider,
    SocketConsumer,
    useSocket
};
