import React, {Component, createContext} from 'react';
import {createUseConsumer} from '../../../utils/context';

const Context = createContext();

const {Provider, Consumer: CheckPanelConsumer} = Context;

class CheckPanelProvider extends Component {
    state = {};
    actions = {
        setIsPanel: (isPanel) => {
            this.setState({
                isPanel: isPanel
            });
        },
    };

    render() {
        const {state, actions} = this;
        const value = {state, actions};
        return (
            <Provider value={value}>
                {this.props.children}
            </Provider>
        );
    }
}

const useCheckPanel = createUseConsumer(CheckPanelConsumer);

export {
    CheckPanelProvider,
    CheckPanelConsumer,
    useCheckPanel
};
