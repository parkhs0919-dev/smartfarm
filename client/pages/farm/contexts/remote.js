import React, {Component, createContext} from 'react';
import {createUseConsumer} from '../../../utils/context';
import CONSTANT from '../constants/constant';
import autoControl from "../managers/autoControl";

const Context = createContext();

const ENUM_REMOTE_NAME = "원격모드";
const ENUM_REMOTE_STATES = {
    on: "remote",
    off: "field"
};

const {Provider, Consumer: RemoteConsumer} = Context;

const defaultState = CONSTANT.defaultRemoteHandlerState;

class RemoteProvider extends Component {
    state = Object.assign({}, defaultState);
    actions = {
        init: () => {
            this.setState(Object.assign({}, defaultState));
        },
        changeRemoteState: (remoteState) => {
            this.setState({
                remoteState: remoteState
            });
            if(remoteState === 'field') this.state.autoControlManualCallback();
            this.state.powerCallback(remoteState);
            this.state.motorCallback(remoteState);
            this.state.autoControlCallback(remoteState);
        },
        getRemoteState: () => {
            return this.state.remoteState ? this.state.remoteState : this.actions.findRemoteState()
        },
        setRemoteMotorCallback: (motorCallback) => {
            this.setState({motorCallback});
        },
        setRemotePowerCallback: (powerCallback) => {
            this.setState({powerCallback});
        },
        setRemoteAutoControlCallback: (autoControlCallback, autoControlManualCallback) => {
            this.setState({
                autoControlCallback: autoControlCallback,
                autoControlManualCallback: autoControlManualCallback
            });
        },
        findRemoteState: () => {
            let rows = window.controls;
            for(let i=0; i<rows.length; i++) {
                if(rows[i].control_name === ENUM_REMOTE_NAME) {
                    this.setState({
                        remoteState: ENUM_REMOTE_STATES[rows[i].state]
                    });
                    return ENUM_REMOTE_STATES[rows[i].state];
                }
            }
        }
    };

    render() {
        const {state, actions} = this;
        const value = {state, actions};
        return (
            <Provider value={value}>
                {this.props.children}
            </Provider>
        );
    }
}

const useRemote = createUseConsumer(RemoteConsumer);

export {
    RemoteProvider,
    RemoteConsumer,
    useRemote
};
