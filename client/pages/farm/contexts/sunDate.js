import React, {Component, createContext} from 'react';
import {createUseConsumer} from '../../../utils/context';

const Context = createContext();

const {Provider, Consumer: SunDateConsumer} = Context;

const defaultState = {
    rise_hour: '',
    rise_minute: '',
    set_hour: '',
    set_minute: '',
};

class SunDateProvider extends Component {
    constructor(props) {
        super(props);
        this.state = Object.assign({}, defaultState);
    }
    actions = {
        setSunDate: (sunDate) => {
            this.setState(sunDate);
        }
    };

    render() {
        const {state, actions} = this;
        const value = {state, actions};
        return (
            <Provider value={value}>
                {this.props.children}
            </Provider>
        );
    }
}

const useSunDate = createUseConsumer(SunDateConsumer);

export {
    SunDateProvider,
    SunDateConsumer,
    useSunDate
}
