import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.CONTROL);
    return req.gets(query).then(process(callback));
};

const findById = (id, callback) => {
    const req = http(resource.CONTROL);
    return req.get(id).then(process(callback));
};

const update = (data, callback) => {
    validator(data, [
        'id',
        'control_name',
        'button_name_1',
        'button_name_2',
        'button_name_3',
    ], [
        'id',
        'control_name'
    ], (body) => {
        const req = http(resource.CONTROL);
        return req.put(data).then(process(callback));
    }, callback);
};

const updateAll = (data, callback) => {
    validator(data, [
        'motors',
        'powers'
    ], [], (body) => {
        const req = http(resource.CONTROL);
        return req.put(body).then(process(callback));
    }, callback);
};

const active = (data, callback) => {
    validator(data, [
        'id',
        'state',
        'percentage',
        'time'
    ], [
        'id',
        'state'
    ], (body) => {
        const req = http(resource.CONTROL_ACTIVE);
        // if(body.percentage === undefined) body.percentage = null;
        // if(body.time === undefined) body.time = null;
        req.put(body).then(process(callback));
    }, callback);
};

const findControlChart = (data, callback) => {
    validator(data, [
        'control_id',
        'startDate',
        'endDate',
        'isDate',
        'isWeek',
        'isMonth',
        'startHour',
        'startMinute',
        'endHour',
        'endMinute'
    ], [
        'control_id'
    ], (body) => {
        let control_id = body.control_id;
        delete body.control_id;
        const req = http(resource.CONTROL_CHART);
        req.getsWithId(control_id, body).then(process(callback));
    }, callback);
};

export {
    update,
    updateAll,
    findAll,
    findById,
    active,
    findControlChart
};

export default {
    update,
    updateAll,
    findAll: findAll,
    findById :findById,
    active: active,
    findControlChart : findControlChart,
};