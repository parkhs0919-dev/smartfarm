import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const check = (query, callback) => {
    const req = http(resource.CHECK_REFRESH);
    return req.gets(query).then(process(callback));
};

export {
    check
};

export default {
    check
};
