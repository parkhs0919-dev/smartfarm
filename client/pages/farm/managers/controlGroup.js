import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.CONTROL_GROUPS);
    return req.gets(query).then(process(callback));
};

const findById = (id, callback) => {
    const req = http(resource.CONTROL_GROUPS);
    req.get(id).then(process(callback));
};

const getList = (query, callback) => {
    const req = http(resource.CONTROL_GROUP_LIST);
    return req.gets(query).then(process(callback));
};

const create = (data, callback) => {
    validator(data, [
        'house_id',
        'type',
        'control_group_name',
        'direction',
        'state',
        'window_position',
        'on_off_state',
        'controlGroupItems'
    ], [
        'house_id',
        'type',
    ], (body) => {
        const req = http(resource.CONTROL_GROUPS);
        req.post(body).then(process(callback));
    }, callback);
};

const remove = (id, callback) => {
    const req = http(resource.CONTROL_GROUPS);
    req.delete({
        id: id
    }).then(process(callback));
};

const update = (data, callback) => {
    validator(data, [
        'id',
        'house_id',
        'type',
        'control_group_name',
        'direction',
        'state',
        'on_off_state',
        'window_position',
        'controlGroupItems',
        'on_off_state',
    ], [
        'id',
        'house_id',
        'type',
    ], (body) => {
        const req = http(resource.CONTROL_GROUPS);
        req.put(body).then(process(callback));
    }, callback);
};

export {
    findAll,
    findById,
    getList,
    create,
    remove,
    update,
}

export default {
    findAll: findAll,
    findById :findById,
    create: create,
    remove: remove,
    update : update,
    getList : getList,
}
