import resource from '../constants/resource';
import http, {validator, process} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.CONTROL_MIN_MAX_RANGES);
    req.gets(query).then(process(callback));
};

const create = (data, callback) => {
    validator(data, [
        'house_id',
        'sensor_id',
        'name',
        'condition',
        'value'
    ], [
        'house_id',
        'sensor_id',
        'name',
        'condition',
        'value'
    ], body => {
        const req = http(resource.CONTROL_MIN_MAX_RANGES);
        req.post(body).then(process(callback));
    }, callback);
};

const update = (data, callback) => {
    validator(data, [
        'id',
        'sensor_id',
        'name',
        'value',
        'condition',
        'state'
    ], [], body => {
        const req = http(resource.CONTROL_MIN_MAX_RANGES);
        req.put(body).then(process(callback));
    }, callback);
};

const remove = (data, callback) => {
    const req = http(resource.CONTROL_MIN_MAX_RANGES);
    req.delete(data).then(process(callback));
};

export {
    findAll,
    create,
    update,
    remove
}

export default {
    findAll,
    create,
    update,
    remove
}
