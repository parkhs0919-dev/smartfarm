import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.P_BAND);
    return req.gets(query).then(process(callback));
};

const findById = (id, callback) => {
    const req = http(resource.P_BAND);
    return req.get(id).then(process(callback));
};

const create = (data, callback) => {
    validator(data, [
        'house_id',
        'p_band_name',
        'in_temperature_sensor_id',
        'out_temperature_sensor_id',
        'wind_speed_sensor_id',
        'is_min_max_p',
        'wind_direction_forward_min_p',
        'wind_direction_forward_max_p',
        'wind_direction_backward_min_p',
        'wind_direction_backward_max_p',
    ], [
        'house_id',
        'p_band_name',
        'in_temperature_sensor_id',
        'out_temperature_sensor_id',
        'wind_speed_sensor_id',
        'is_min_max_p',
    ], body => {
        const req = http(resource.P_BAND);
        req.post(body).then(process(callback));
    }, callback);
};

const update = (data, callback) => {
    validator(data, [
        'id',
        'p_band_name',
        'in_temperature_sensor_id',
        'out_temperature_sensor_id',
        'wind_speed_sensor_id',
        'is_min_max_p',
        'wind_direction_forward_min_p',
        'wind_direction_forward_max_p',
        'wind_direction_backward_min_p',
        'wind_direction_backward_max_p',
    ], [
        'p_band_name',
        'in_temperature_sensor_id',
        'out_temperature_sensor_id',
        'wind_speed_sensor_id',
        'is_min_max_p',
    ], body => {
        const req = http(resource.P_BAND);
        req.put(body).then(process(callback));
    }, callback);
};

const remove = (data, callback) => {
    const req = http(resource.P_BAND);
    req.delete(data).then(process(callback));
};

export {
    findAll,
    findById,
    create,
    update,
    remove,
};

export default {
    findAll,
    findById,
    create,
    update,
    remove,
};
