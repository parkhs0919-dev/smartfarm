import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findHouseDirection = (query, callback) => {
    validator(query, [
        'offset',
        'size'
    ], [], (data) => {
        const req = http(resource.HOUSE_DIRECTION);
        req.gets(data).then(process(callback));
    })
};

const findById = (data, callback) => {
    validator(data, [
        'id',
    ], [
        'id'
    ], (body) => {
        const req = http(resource.HOUSE_DIRECTION);
        req.get(body.id).then(process(callback));
    }, callback);
};

const update = (data, callback) => {
    validator(data, [
        'direction',
        'id',
        'sensor_id',
        'is_use_alarm'
    ], [
        'direction',
        'id',
    ], (body) => {
        const req = http(resource.HOUSE_DIRECTION);
        req.put(body).then(process(callback));
    }, callback);
};

const updateAlarmOption = (data, callback) => {
    validator(data, [
        'id',
        'is_use_alarm'
    ], [
        'is_use_alarm',
        'id',
    ], (body) => {
        const req = http(resource.HOUSE_DIRECTION);
        req.put(body).then(process(callback));
    }, callback);
};

export {
    findHouseDirection,
    findById,
    update,
    updateAlarmOption,
};

export default {
    findHouseDirection,
    findById,
    update,
    updateAlarmOption
};