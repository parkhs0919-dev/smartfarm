import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.CCTV);
    return req.gets(query).then(process(callback));
};

const findCCTVSetting = (callback) => {
    const req = http(resource.CCTV_SETTING);
    return req.gets().then(process(callback));
};

const setCCTVSetting = (data, callback) => {
    validator(data, [
        "is_auto_close"
    ], [
        "is_auto_close"
    ], (body) => {
        const req = http(resource.CCTV_SETTING);
        return req.put(body).then(process(callback));
    }, callback);
};

const renameCCTVs = (data, callback) => {
    validator(data, [
        "cctvs"
    ], [
        "cctvs"
    ], (body) => {
        const req = http(resource.CCTV);
        return req.put(body).then(process(callback));
    }, callback);
};

const cctvStart = (id, callback) => {
    const req = http(resource.CCTV_START);
    return req.postWithId({id}).then(process(callback));
};

const cctvMove = (data, callback) => {
    validator(data,
        [
            'id',
            'isHome',
            'isActive',
            'x',
            'y',
            'z',
            'timeout'
        ],
        [],
        (body) => {
            const req = http(resource.CCTV_MOVE);
            req.postWithId(body).then(process(callback));
        }, callback);
};

const cctvSnapshot = id => {
    window.open(resource.CCTV_SNAPSHOT + '/' + id, '_blank');
};

export {
    findAll,
    findCCTVSetting,
    setCCTVSetting,
    renameCCTVs,
    cctvStart,
    cctvMove,
    cctvSnapshot,
};

export default {
    findAll,
    findCCTVSetting,
    setCCTVSetting,
    renameCCTVs,
    cctvStart,
    cctvMove,
    cctvSnapshot,
};
