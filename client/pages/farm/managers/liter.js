import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.FLOW_METER);
    return req.getsWithId(query.id,query).then(process(callback));
};

const Detailfind = (query, callback) => {
    const req = http(resource.FLOW_METER_ROUND);    
    req.gets(query).then(process(callback));
};

const create = (data, callback) => {
    validator(data, [
        'flow_meter_id',
      
    ], [
        'flow_meter_id',
      
    ], (body) => {
        const req = http(resource.FLOW_METER_ROUND);
        req.post(body).then(process(callback));
    }, callback);
};

const remove = (id, callback) => {
    const req = http(resource.FLOW_METER_ROUND);
    req.delete({
        id: id
    }).then(process(callback));
};

const update = (data, callback) => {
    validator(data, [
        'id',
        'end_hour_list',
        'id_list',
        'is_auto_reset',
        'start_hour_list',
        'house_id'
    ], [
        'id',
        'is_auto_reset',
        'house_id'
    ], (body) => {

        const req = http(resource.FLOW_METER_ROUND);
        req.put(body).then(process(callback));
    }, callback);
};

export {
    findAll,
    Detailfind,
    create,
    remove,
    update,
}

export default {
    findAll: findAll,
    create: create,
    remove: remove,
    Detailfind,
    update,
}
