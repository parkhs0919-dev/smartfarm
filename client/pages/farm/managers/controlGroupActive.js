import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";


const active = (data, callback) => {
    validator(data, [
        'id',
        'state',
        'percentage',
        'time'
    ], [
        'state',
        'id',
    ], (body) => {
        const req = http(resource.CONTROL_GROUP_ACTIVE);
        req.put(body).then(process(callback));
    }, callback);
};

export {
    active
}

export default {
    active: active,
}
