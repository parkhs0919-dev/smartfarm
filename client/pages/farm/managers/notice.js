import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findNotice = (query, callback) => {
    const req = http(resource.NOTICE);
    return req.gets(query).then(process(callback));
};

export {
    findNotice
}

export default {
    findNotice: findNotice
}