import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (callback) => {
    let query = {
        platform: 'farm'
    };
    const req = http(resource.SUN_DATE);
    return req.gets(query).then(process(callback));
};

export {
    findAll
}

export default {
    findAll: findAll,
}