import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAccumulatedTemperatureSensors = (query, callback) => {
    validator(query, [
        'house_id',
        'position'
    ], [], (data) => {
        const req = http(resource.ACCUMULATED_TEMPERATURE);
        req.gets(data).then(process(callback));
    })
};

const findById = (data, callback) => {
    validator(data, [
        'sensor_id',
    ], [
        'sensor_id'
    ], (body) => {
        const req = http(resource.ACCUMULATED_TEMPERATURE);
        req.get(body.sensor_id).then(process(callback));
    }, callback);
};

const setAccumulatedTemperature = (data, callback) => {
    validator(data, [
        'min_value',
        'start_date',
        'end_date',
        "id",
    ], [
        'start_date',
        'end_date',
        "id",
    ], (body) => {
        const req = http(resource.ACCUMULATED_TEMPERATURE);
        return req.postWithId(body).then(process(callback));
    })
};

export {
    findAccumulatedTemperatureSensors,
    findById,
    setAccumulatedTemperature,
};

export default {
    findAccumulatedTemperatureSensors,
    findById,
    setAccumulatedTemperature,
};