import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findMode = (house_id, callback) => {
    const req = http(resource.MODE);
    req.get(house_id).then(process(callback));
};

const changeMode = (data, callback) => {
    validator(data, [
        'id',
        'mode',
    ], [
        'id',
        'mode'
    ], (body) => {
        const req = http(resource.MODE);
        req.put(body).then(process(callback));
    }, callback);
};

const controlMode = (data, callback) => {
    validator(data, [
        'id',
        'mode',
    ], [
        'id',
        'mode'
    ], (body) => {
        const req = http(resource.CONTROL_MODE);
        req.put(body).then(process(callback));
    }, callback);
};

export {
    findMode,
    changeMode,
    controlMode
};

export default {
    findMode,
    changeMode: changeMode,
    controlMode: controlMode,
};