import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findChart = (data, callback) => {
    const req = http(resource.CHART);
    req.get(data.house_id).then(process(callback));
};

const setChart = (data, callback) => {
    validator(data, [
        'sensor_id',
        'control_id',
        'type',
        'startHour',
        'startMinute',
        'endHour',
        'endMinute',
        'isDate',
        'isWeek',
        'isMonth',
    ], [
    ], (body) => {
        const req = http(resource.CHART);
        return req.gets(body).then(process(callback));
    }, callback);
};


export {
    findChart,
    setChart,
};

export default {
    findChart,
    setChart,
};