import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const exportItem = (query, callback) => {
    validator(query, [
        'type',
        'sensor_id',
        'house_id',
        'startDate',
        'endDate'
    ], [
        'type',
        'startDate',
        'endDate'
    ], (data) => {
        const req = http(resource.EXPORT);
        return req.gets(data).then(process(callback));
    }, callback);
};

export {
    exportItem
}

export default {
    exportItem: exportItem
}