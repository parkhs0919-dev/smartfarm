import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findMainSensorChart = (data, callback) => {
    const req = http(resource.SENSOR_MAIN_CHART);
    req.get(data.house_id).then(process(callback));
};

const setMainSensorChart = (data, callback) => {
    validator(data, [
        'house_id',
        'sensor_id'
    ], [
        'house_id',
        'sensor_id'
    ], (body) => {
        const req = http(resource.SENSOR_MAIN_CHART);
        req.post(body).then(process(callback));
    }, callback);
};


export {
    findMainSensorChart,
    setMainSensorChart,
};

export default {
    findMainSensorChart,
    setMainSensorChart,
};