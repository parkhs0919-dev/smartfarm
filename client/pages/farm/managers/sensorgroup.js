import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.SENSOR_GROUP);
    return req.gets(query).then(process(callback));
};

const create = (data, callback) => {
    validator(data, [
        'type',
        'name',
        'items',       
        'house_id',
        'position',
    ], [
        'type',
        'name',
        'items',
        'house_id',
        'position',
    ], (body) => {
        const req = http(resource.SENSOR_GROUP);
        req.post(body).then(process(callback));
    }, callback);
};

const remove = (data, callback) => {
    const req = http(resource.SENSOR_GROUP);
    req.delete({
        id: data.id,
        house_id: data.house_id,
    }).then(process(callback));
};

const update = (data, callback) => {
    validator(data, [
        'id',
        'name',
        'house_id',
        'items',
    ], [
        'id',        
    ], (body) => {
        const req = http(resource.SENSOR_GROUP);
        req.put(body).then(process(callback));
    }, callback);
};

const activeupdate = (data, callback) => {
    validator(data, [
        'id',
        'is_use',
        'house_id'
    ], [
        'id',
        'is_use',
        'house_id'
    ], (body) => {        
        const req = http(resource.SENSOR_GROUP);
        req.put(body).then(process(callback));
    }, callback);
};

const getsensortype = (data,callback)=> {    
    validator(data, [
        'id',
        'type',
        'house_id',
        'position'
    ], [
        'id',
        'type',
        'house_id'
    ], (body) => {
        const req = http(resource.SENSOR_GROUP);
        req.getsWithId(body.id,body).then(process(callback));
    }, callback);
};

const gettype = (data,callback)=> {    
    validator(data, [        
        'type',
        'house_id',
        'position'
    ], [        
        'type',
        'house_id'
    ], (body) => {
        const req = http(resource.SENSOR_GROUP_TYPE);
        req.gets(body).then(process(callback));
    }, callback);
};


export {
    findAll,
    create,
    remove,
    update,
    getsensortype,
    activeupdate,
    gettype,
}

export default {
    findAll: findAll,
    create: create,
    remove: remove,
    update,
    getsensortype,
    activeupdate,
    gettype,
}
