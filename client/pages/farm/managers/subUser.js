import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const create = (data, callback) => {
    validator(data, [
        'platform',
        'uid',
        'name',
        'password',
    ], [
        'platform',
        'uid',
        'name',
        'password',
    ],(body) => {
        const req = http(resource.SUB_USER);
        req.post(body).then(process(callback));
    }, callback);
};

export {
    create,
}

export default {
    create,
}