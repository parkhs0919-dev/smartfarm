import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findTemperature = (data, callback) => {
    validator(data, [
        'id',
        'platform'
    ], [
        'id',
        'platform'
    ], (body) => {
        const req = http(resource.TEMPERATURE);
        req.get(body.id).then(process(callback));
    }, callback);
};


export {
    findTemperature
};

export default {
    findTemperature
};