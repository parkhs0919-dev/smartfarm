import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.CONTROL_ALARM);
    return req.gets(query).then(process(callback));
};

const findById = (id, callback) => {
    const req = http(resource.CONTROL_ALARM);
    return req.get(id).then(process(callback));
};

const create = (data, callback) => {
    validator(data, [
        'control_id',
        'state',
        'text',
    ], [
        'control_id',
        'state',
        'text',
    ], (body) => {

        const req = http(resource.CONTROL_ALARM);
        req.post(body).then(process(callback));
    }, callback);
};

const remove = (id, callback) => {
    const req = http(resource.CONTROL_ALARM);
    req.delete({
        id: id
    }).then(process(callback));
};

const update = (data, callback) => {
    validator(data, [
        'id',
        'control_id',
        'state',
        'text',
    ], [
        'id',
    ], (body) => {
        const req = http(resource.CONTROL_ALARM);
        req.put(body).then(process(callback));
    }, callback);
};

export {
    findAll,
    findById,
    create,
    remove,
    update,
}

export default {
    findAll: findAll,
    findById : findById,
    create: create,
    remove: remove,
    update : update,
}
