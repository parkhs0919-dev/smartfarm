import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAutoControlRecommend = (body, callback) => {
    const req = http(resource.AUTO_CONTROL_RECOMMEND);
    return req.gets(body).then(process(callback));
};

const findAutoControlRecommendCrops = (body, callback) => {
    const req = http(resource.AUTO_CONTROL_RECOMMEND_CROP);
    return req.gets(body).then(process(callback));
};

const checkCanAutoControl = (house_id, callback) => {
    const req = http(resource.AUTO_CONTROL_CHECK);
    return req.get(house_id).then(process(callback));
};

const findAll = (query, callback) => {
    const req = http(resource.AUTO_CONTROL);
    return req.gets(query).then(process(callback));
};

const create = (data, callback) => {
    validator(data, [
        'house_id',
        'type',
        'auto_control_name',
        'date_type',
        'per_date',
        'is_sun',
        'is_mon',
        'is_tue',
        'is_wed',
        'is_thur',
        'is_fri',
        'is_sat',
        'step_delay',
        'start_date',
        'end_date',
        'autoControlItems',
        'autoControlSteps',
    ], [
        'house_id',
        'type',
        'auto_control_name',
        'autoControlItems',
    ], (body) => {
        if(!body.start_date) body.start_date = null;
        if(!body.end_date) body.end_date = null;
        const req = http(resource.AUTO_CONTROL);
        req.post(body).then(process(callback));
    }, callback);
};

const remove = (id, callback) => {
    const req = http(resource.AUTO_CONTROL);
    req.delete({
        id: id
    }).then(process(callback));
};

const update = (data, callback) => {
    validator(data, [
        'id',
        'house_id',
        'type',
        'date_type',
        'per_date',
        'auto_control_name',
        'is_sun',
        'is_mon',
        'is_tue',
        'is_wed',
        'is_thur',
        'is_fri',
        'is_sat',
        'step_delay',
        'start_date',
        'end_date',
        'autoControlItems',
        'autoControlSteps',
    ], [
        'id',
        'house_id',
        'type',
        'auto_control_name',
        'autoControlItems',
    ], (body) => {
        if(!body.start_date) body.start_date = null;
        if(!body.end_date) body.end_date = null;
        const req = http(resource.AUTO_CONTROL);
        req.put(body).then(process(callback));
    }, callback);
};

const uploadJSON = (param, data, callback) => {
    const req = http(resource.AUTO_CONTROL_UPLOAD);
    req.postFormData(data, param).then(process(callback));
};

const getWindDirectionCount = (house_id, callback) => {
    const req = http(resource.AUTO_CONTROL_WIND_DIRECTION_COUNT);
    req.get(house_id).then(process(callback));
};

export {
    findAll,
    create,
    remove,
    update,
    checkCanAutoControl,
    uploadJSON,
    getWindDirectionCount,
    findAutoControlRecommend,
    findAutoControlRecommendCrops

}

export default {
    findAll: findAll,
    create: create,
    remove: remove,
    update: update,
    checkCanAutoControl,
    uploadJSON,
    getWindDirectionCount,
    findAutoControlRecommend,
    findAutoControlRecommendCrops,
}
