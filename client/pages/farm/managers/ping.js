import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const checkPing = (query, callback) => {
    const req = http(resource.PING);
    return req.gets(query).then(process(callback));
};

export {
    checkPing
}

export default {
    checkPing: checkPing,
}