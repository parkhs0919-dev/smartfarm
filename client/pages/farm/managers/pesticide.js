import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.PEST_CONTROL);
    return req.gets(query).then(process(callback));
};

const findarea = (data,callback) =>{
    validator(data, [
        'id',
    ], [
        'id'
    ], (body) => {
        const req = http(resource.PEST_CONTROL);
        req.get(body.id).then(process(callback));
    }, callback);
}
const create = (data, callback) => {
    validator(data, [
        'pest_control_name',
        'zone_time',
        'medicine_time',
        'compressor_time',
        'zone_list',
        'house_id',
        'zone_use_list',

    ], [
        'pest_control_name',
        'zone_time',
        'medicine_time',
        'compressor_time',
        'house_id',
        'zone_use_list',
    ], (body) => {
        
        const req = http(resource.PEST_CONTROL);
        req.post(body).then(process(callback));
    }, callback);
};

const remove = (id, callback) => {
    const req = http(resource.PEST_CONTROL);
    req.delete({
        id: id
    }).then(process(callback));
};

const setautopesticideupdate = (data, callback) => {
    validator(data, [
        'is_frost',
        'max_temp',
        'midnight_temp',
        'current_temp',
        'delay',
        'id',
    ], [
        
        'id',
    ], (body) => {
        const req = http(resource.PEST_CONTROL_FROST);
        req.put(body).then(process(callback));
    }, callback);
};
const modeupdate = (data, callback) => {
    validator(data, [
        'id',
        'is_auto',
        'key'
    ], [
        'id',
        'is_auto',
        'key'
    ], (body) => {

        const req = http(resource.PEST_CONTROL_ACTIVE);
        req.put(body).then(process(callback));
    }, callback);
};

const active = (data,callback) => { 
    validator(data, [
        'id',
        'index',
        'state',
        'key',
    ], [
        'id',
    
    ], (body) => {

        const req = http(resource.PEST_CONTROL_ACTIVE);
        req.put(body).then(process(callback));
    }, callback);
};

const update = (data, callback) => {
    validator(data, [
        'pest_control_name',
        'zone_time',
        'medicine_time',
        'compressor_time',
        'zone_list',
        'zone_use_list',
        'id',
    ], [
        'id',
    ], (body) => {

        const req = http(resource.PEST_CONTROL);
        req.put(body).then(process(callback));
    }, callback);
};

export {
    findAll,
    findarea,
    create,
    remove,
    setautopesticideupdate,
    modeupdate,
    update,
    active,
}

export default {
    findAll: findAll,
    create: create,
    remove: remove,
    setautopesticideupdate,
    modeupdate,
    update,
    findarea,
    active,
}