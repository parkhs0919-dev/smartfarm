import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findWeather = (query, callback) => {
    const req = http(resource.WEATHER);
    return req.gets(query).then(process(callback));
};

export {
    findWeather
}

export default {
    findWeather: findWeather
}