import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const update = (data, callback) => {
    validator(data, [
        'id',
        'delay',
        'recount'
    ], [
        'id'
    ], (body) => {
        const req = http(resource.AUTO_CONTROL_DELAY_RECOUNT);
        req.put(body).then(process(callback));
    }, callback);
};

export {
    update
}

export default {
    update
}