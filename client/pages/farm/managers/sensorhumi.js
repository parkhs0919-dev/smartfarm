import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findhumi = (data, callback) => {
    
    validator(data, [
        'id',
    ], [
        'id',
    ], (body) => {
        const req = http(resource.SENSOR_HUMI);
        req.get(body.id).then(process(callback));
    }, callback);
};




export {
    findhumi
};

export default {
    findhumi
};