import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

// const update = (data, callback) => {
//     validator(data, [
//         'house_id',
//         'ids'
//     ], [
//         'house_id',
//         'ids'
//     ], (body) => {
//         const req = http(resource.AUTO_CONTROL_ORDER);
//         req.put(body).then(process(callback));
//     }, callback);
// };

const update = (data, callback) => {
    validator(data, [
        'id',
        'order'
    ], [
        'id',
        'order'
    ], (body) => {
        const req = http(resource.AUTO_CONTROL_ORDER);
        req.put(body).then(process(callback));
    }, callback);
};

export {
    update
}

export default {
    update
}