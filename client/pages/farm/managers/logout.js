import resource from '../constants/resource';
import http, {process} from "../../../utils/http";

const logout = (callback) => {
    const req = http(resource.LOGOUT);
    req.del().then(process(callback));
};

export {
    logout
};

export default {
    logout
};
