import resource from '../constants/resource';
import http, {process} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.JOURNAL);
    req.gets(query).then(process(callback));
};

const findById = (id, callback) => {
    const req = http(resource.JOURNAL);
    req.get(id).then(process(callback));
};

const create = (data, callback) => {
    const req = http(resource.JOURNAL);
    req.post(data).then(process(callback));
};

const remove = (id, callback) => {
    const req = http(resource.JOURNAL);
    req.delete({
        id: id
    }).then(process(callback));
};

export {
    findAll,
    findById,
    create,
    remove
}

export default {
    findAll: findAll,
    findById: findById,
    create: create,
    remove: remove,
}