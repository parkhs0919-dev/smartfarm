import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findhumidityDeficit = (data, callback) => {
    validator(data, [
        'id',
    ], [
        'id',
    ], (body) => {
        const req = http(resource.SENSOR_HIMIDITYDEFICIT);
        req.get(body.id).then(process(callback));
    }, callback);
};




export {
    findhumidityDeficit
};

export default {
    findhumidityDeficit
};