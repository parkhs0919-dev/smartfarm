import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.USER);
    return req.gets(query).then(process(callback));
};

const Controlfind = (query, callback) => {
    const req = http(resource.USER_CONTROL_REL);
    return req.gets(query).then(process(callback));
};

const detailcontrolfind = (query, callback) => {
    const req = http(resource.USER);
    return   req.getsWithId(query.id,query).then(process(callback));
};
const create = (data, callback) => {
    validator(data, [
        'id',
        'is_use',
        'motor_list',
        'name',
        'password',
        'platform',
        'power_list',
    ], [
        'id',
        'name',
        'password'
    ], (body) => {

        const req = http(resource.USER);
        req.post(body).then(process(callback));
    }, callback);
};

const remove = (id, callback) => {
    const req = http(resource.USER);
    req.delete({
        id: id
    }).then(process(callback));
};

const update = (data, callback) => {    
    validator(data, [
        'is_use',
        'motor_list',       
        'platform',
        'power_list',
        'id'
    ], [
        'id',
     
    ], (body) => {
        const req = http(resource.USER);
        req.put(body).then(process(callback));
    }, callback);    
}


const pwupdate = (data, callback) => {    
    validator(data, [  
        'newPassword',
        'oldPassword',
        'id'
    ], [
        'id',
     
    ], (body) => {
        const req = http(resource.USER+'/pw');
        req.put(body).then(process(callback));
    }, callback);    
}

const opverlabCheck =(data, callback) =>{
    validator(data, [
        'uid',
        'platform'
    ], [
        'uid',
        
    ], (body) => {
        
        const req = http(resource.USER_CONTROL_REL);
        req.post(body).then(process(callback));
    }, callback);
}

export {
    findAll,
    Controlfind,
    detailcontrolfind,
    create,
    remove,
    update,    
    opverlabCheck,
    pwupdate,
}

export default {
    findAll,
    Controlfind,
    detailcontrolfind,
    create,
    remove,
    update,
    opverlabCheck,
    pwupdate,
}
