import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.ALARM);
    return req.gets(query).then(process(callback));
};

const create = (data, callback) => {
    validator(data, [
        'sensor_id',
        'condition',
        'is_no',
        'is_use',
        'name',
        'no_end_hour',
        'no_start_hour',
        'type',
        'use_end_hour',
        'use_start_hour',
        'is_bar',
        'is_image',
        'is_sound',
        'cycle',
        'value',
        'repeat'

    ], [
        'sensor_id',
        'condition',
        'value'
    ], (body) => {

        const req = http(resource.ALARM);
        req.post(body).then(process(callback));
    }, callback);
};

const remove = (id, callback) => {
    const req = http(resource.ALARM);
    req.delete({
        id: id
    }).then(process(callback));
};

const setAlarmWay = (data, callback) => {
    validator(data, [
        'id',
        'sensor_id',
        'condition',
        'is_no',
        'is_use',
        'name',
        'no_end_hour',
        'no_start_hour',
        'type',
        'use_end_hour',
        'use_start_hour',
        'is_bar',
        'is_image',
        'is_sound',
        'cycle',
        'value',
        'repeat'
    ], [
        'id',
    ], (body) => {
        const req = http(resource.ALARM);
        req.put(body).then(process(callback));
    }, callback);
};

const update = (data, callback) => {
    validator(data, [
        'id',
        'state'
    ], [
        'id',
        'state'
    ], (body) => {

        const req = http(resource.ALARM);
        req.put(body).then(process(callback));
    }, callback);
};

export {
    findAll,
    create,
    remove,
    setAlarmWay,
    update,
}

export default {
    findAll: findAll,
    create: create,
    remove: remove,
    setAlarmWay,
    update,
}
