import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.SENSOR);
    return req.gets(query).then(process(callback));
};

const findSensorChart = (data, callback) => {
    validator(data, [
        'sensor_id',
        'startDate',
        'endDate',
        'isDate',
        'isWeek',
        'isMonth',
        'startHour',
        'startMinute',
        'endHour',
        'endMinute'
    ], [
        'sensor_id'
    ], (body) => {
        let sensor_id = body.sensor_id;
        delete body.sensor_id;
        const req = http(resource.SENSOR_CHART);
        req.getsWithId(sensor_id, body).then(process(callback));
    }, callback);
};


const findPanelSound = (data, callback) => {
    validator(data, [
        'id',
        'sensor_id'
    ], [
        'id',
        'sensor_id'
    ], (body) => {
        const req = http(resource.SENSOR_PANEL_ALARM);
        return req.gets(data).then(process(callback));
    }, callback);

}

const updateAll = (data, callback) => {
    validator(data, [
        'sensors'
    ], [

    ], (body) => {
        const req = http(resource.SENSOR);
        return req.put(body).then(process(callback));
    })
};

const resetLiter = (id, callback) => {
    const req = http(resource.LITER_RESET);
    return req.put({id: id}).then(process(callback));
};
const resetRain = (id, callback) => {    
    const req = http(resource.RESET_RAIN);
    return req.put({id: id}).then(process(callback));
};

export {
    findAll,
    findSensorChart,
    updateAll,
    resetLiter,
    resetRain,
    findPanelSound
};

export default {
    findAll,
    findSensorChart,
    updateAll,
    resetLiter,
    resetRain,
    findPanelSound
};
