import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const getDefault = (query, callback) => {
    const req = http(resource.SUN_DATE_DEFAULT);
    return req.gets(query).then(process(callback));
};

const updateDefault = (body, callback) => {
    const req = http(resource.SUN_DATE_DEFAULT);
    return req.put(body).then(process(callback));
};

export {
    getDefault,
    updateDefault,
}

export default {
    getDefault,
    updateDefault,
}