import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const findAll = (query, callback) => {
    const req = http(resource.SCREEN);
    return req.gets(query).then(process(callback));
};

const updateScreenOrder = (data, callback) => {
    const req = http(resource.SCREEN);
    return req.put(data).then(process(callback));
};

export {
    findAll,
    updateScreenOrder
};

export default {
    findAll,
    updateScreenOrder
};
