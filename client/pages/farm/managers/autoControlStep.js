import resource from '../constants/resource';
import http, {process, validator} from "../../../utils/http";

const create = (data, callback) => {
    validator(data, [
        'auto_control_id',
        'control_id',
        'control_group_id',
        'state',
        'time',
        'percentage',
        'delay',
        'recount',
    ], [
        'auto_control_id',
        'state',
    ], (body) => {
        const req = http(resource.AUTO_CONTROL_STEP);
        req.post(body).then(process(callback));
    }, callback);
};

const remove = (id, callback) => {
    const req = http(resource.AUTO_CONTROL_STEP);
    req.delete({
        id: id
    }).then(process(callback));
};


export {
    create,
    remove,
}

export default {
    create: create,
    remove: remove,
}