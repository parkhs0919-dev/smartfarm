import React, {Component, Fragment} from 'react';

import {useWeather} from "../../contexts/modals/weather";
import {useAlert} from "../../contexts/alert";

import weatherManager from '../../managers/weather';
import {date} from "../../../../utils/filter";

import CONSTANT from '../../constants/constant';
import Spinner from "../Spinner";
import {noSession} from "../../utils/session";

class ModalWeather extends Component{
    constructor(props) {
        super(props);

        this.state = {
            weathers: [],
            loading: true,
        };
    }
    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        document.addEventListener('keyup', this.closeOnEscape);
        this.findWeathers();
    };

    close = (e) => {
        if(e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findWeathers = () => {
        const _this = this;
        let query = {
            platform: window.platform
        };
        weatherManager.findWeather(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                let state = {
                    weathers: data.data,
                    loading: false
                };
                _this.setState(state);
            } else if (status === 404) {
                _this.setState({
                    weathers: null,
                    loading: false
                })
            } else {
                _this.props.alertError(status, data);
                _this.setState({
                    loading: false
                });
            }
        }));
    };

    generateRelativeLabel = (index) => {
        return index === 0 ? "어제" : index === 1 ? "오늘" : "내일";
    };

    setDateFormatToDot = (item) => {
        return date(new Date(item), 'MM.dd');
    };

    render() {
        return (
            <article id="lcModalWeatherWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalWeather" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">날씨</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <Spinner spinning={this.state.loading}/>
                                    { !this.state.loading ?
                                        <Fragment>
                                            <section id="relativeWeatherInfoWrap">
                                                <ul id="relativeDateWeatherInfo">
                                                    {
                                                        Object.keys(this.state.weathers).map((item, index) => {
                                                            const WEATHER = this.state.weathers[item];
                                                            if(index < 3) {
                                                                return (
                                                                    <li key={index}>
                                                                        <article className="weather-info-wrap">
                                                                            <h4>{this.generateRelativeLabel(index)}</h4>
                                                                            <span className="weather-date">{this.setDateFormatToDot(item)}</span>
                                                                            <img className="weather-icon" src={CONSTANT.weatherURL + WEATHER.icon + '.png' } alt="날씨"/>
                                                                            <span className="temperature">{parseFloat(WEATHER.temperature).toFixed(1)} ℃</span>
                                                                            <span className="summary">{WEATHER.summary}</span>
                                                                            <div className="detail-weather-info-wrap">
                                                                                <span className="humidity">습도 {parseInt(WEATHER.humidity * 100)}%</span>
                                                                                <span className="dew-point">이슬점 {parseFloat(WEATHER.dewPoint).toFixed(1)} ℃</span>
                                                                            </div>
                                                                        </article>
                                                                    </li>
                                                                )
                                                            } else {
                                                                return null;
                                                            }
                                                        })
                                                    }
                                                </ul>
                                            </section>
                                            <section id="weeklyWeatherInfoWrap">
                                                <h4>주간예보</h4>
                                                <ul id="weeklyWeatherInfo">
                                                    {
                                                        Object.keys(this.state.weathers).map((item, index) => {
                                                            const WEATHER = this.state.weathers[item];
                                                            if(index > 0 && index < 6) {
                                                                return (
                                                                    <li key={index}>
                                                                        <article className="weather-info-wrap">
                                                                            <span className="weather-date">{this.setDateFormatToDot(item)}</span>
                                                                            <img className="weather-icon" src={CONSTANT.weatherURL + WEATHER.icon + '.png' } alt="날씨"/>
                                                                            <span className="temperature">{parseFloat(WEATHER.temperature).toFixed(1)} ℃</span>
                                                                        </article>
                                                                    </li>
                                                                )
                                                            }
                                                        })
                                                    }
                                                </ul>
                                            </section>
                                        </Fragment> : null
                                    }

                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useWeather(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalWeather));