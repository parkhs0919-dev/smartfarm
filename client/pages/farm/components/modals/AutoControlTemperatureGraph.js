import React, {Component, Fragment} from 'react';
import {useAutoControlTemperatureGraph} from "../../contexts/modals/autoControlTemperatureGraph";
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import deepcopy from "../../../../utils/deepcopy";
import {useSunDate} from "../../contexts/sunDate";
import Spinner from "../Spinner";
import LcChart from "../Chart";
import {attachZero, date} from "../../../../utils/filter";
import Select from "react-select";
import {colourStyles} from "../../utils/select";
import autoControlManager from "../../managers/autoControl";
import {noSession} from "../../utils/session";

const DATE_DISPLAY_FORMAT = "yyyy-MM-dd";
const DEFAULT_STATE = {
    temperature: {},
    chartData: [],
    secondData:[],
    dateRange: {
        isDate: true,
        isWeek: false,
        isMonth: false,
    },
    startDate: null,
    endDate: null,
    loading: true,
    datePickerOpenState: {
        startDate: false,
        endDate: false,
    },
    sensorZoom: true,
    selectedTable : null,
    selectAutoControls: [],
    tableSelectedIsUseTemperature : 'vantilation',
};



class ModalAutoControlTemperatureGraph extends Component {
    constructor(props){
        super(props);
        this.state = deepcopy(DEFAULT_STATE);
    };

    componentDidMount() {
        this.props.sync(this.init);
    }

    componentWillUnmount() {
        if (this.chart) {
            this.chart.dispose();
        }
    }

    componentDidUpdate(oldProps) {
        if (oldProps.paddingRight !== this.props.paddingRight) {
            this.chart.paddingRight = this.props.paddingRight;
        }
    }

    init = (data) => {
        document.addEventListener('keyup', this.closeOnEscape);
        this.setState({data});
        this.getTables(data.tableSelectedIsUseTemperature);
        this.getSunDate();
        this.getTableControls(data);
        this.setState({tableSelectedIsUseTemperature : data.tableSelectedIsUseTemperature});
        this.setState({sensor : {type : data.tableSelectedIsUseTemperature}});
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    getSunDate = () => {

        let rise_time = new Date();
        rise_time.setHours(parseInt(this.props.rise_hour));
        rise_time.setMinutes( parseInt(this.props.rise_minute));

        let set_time = new Date();
        set_time.setHours(parseInt(this.props.set_hour));
        set_time.setMinutes( parseInt(this.props.set_minute));

        let sunDate = {
            rise_time : rise_time,
            set_time : set_time
        };

        this.setState({sunDate});
    };

    getTables = value => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            auto_control_type : 'table',
            temperature_type : value === 'vantilation' ? 'boiler' : 'vantilation'
        };

        autoControlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    selectAutoControls : this.generateSelectData(data.data.rows)
                });
            } else if (status === 404) {
                _this.setState({
                    selectAutoControls: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));

    };

    generateSelectData = (data) => {
        let selectAutoControls = [];
        data.forEach((data, index) => {
            selectAutoControls.push({
                label : data.auto_control_name,
                value : data.id,
                origin : data
            })
        });
        return selectAutoControls;
    };

    handleSelectedTable = async data => {

        await this.setState({chartData : []});
        this.setState({selectedTable : data});
        let autoControl = data.origin;
        let autoControlTables = [];
        let secondData = [];

        const rise = parseInt(this.props.rise_hour) * 60 + parseInt(this.props.rise_minute);
        const set = parseInt(this.props.set_hour) * 60 + parseInt(this.props.set_minute);

        if(autoControl.autoControlItems.length){
            autoControl.autoControlItems.forEach((item, itemIndex) => {
                autoControlTables[item.period] = [];
                autoControlTables[item.period].push(item);
            });
        }

        if(autoControlTables.length){
            Object.keys(autoControlTables).map((key, index) => {
                let autoControlTable = autoControlTables[key][0];

                let start_time = '';
                if(autoControlTable.is_sun_date){
                    start_time = autoControlTable.sun_date_start_time;
                    start_time = (autoControlTable.sun_date_start_type === 'rise' ? rise : set) + parseInt(start_time);
                }else{
                    start_time = parseInt(autoControlTable.start_hour || 0 )* 60 +  parseInt(autoControlTable.start_minute || 0);
                }

                let startTime = Math.abs(start_time);
                let created_at = new Date();
                created_at.setHours(attachZero(parseInt(startTime/60)));
                created_at.setMinutes(parseInt(startTime%60));

                secondData.push({
                    period : autoControlTable.period,
                    start_time : start_time,
                    type : autoControlTable.sun_date_start_type,
                    temperature : autoControlTable.p_band_id ? autoControlTable.p_band_temperature : autoControlTable.expect_temperature,
                    created_at : date(created_at, 'yyyy-MM-dd HH:mm:00')
                });

                if(Object.keys(autoControlTables).length-1 === index){
                    secondData.push({
                        period : 0,
                        start_time : start_time,
                        type : autoControlTable.sun_date_start_type,
                        temperature : autoControlTable.p_band_id ? autoControlTable.p_band_temperature : autoControlTable.expect_temperature,
                        created_at : date(created_at, 'yyyy-MM-dd 23:59:00')
                    });
                }
            });

            if(secondData.length){
                secondData.sort(function (a, b) {
                    return a.created_at < b.created_at ? -1 : a.created_at > b.created_at ? 1 : 0;
                });
            }

            if(secondData[0].created_at !== date(new Date(), 'yyyy-MM-dd 00:00:00')){
                secondData.unshift({
                    period : 0,
                    start_time : 0,
                    type : '',
                    temperature : 0,
                    created_at : date(new Date(), 'yyyy-MM-dd 00:00:00')
                });
            }


            await this.setState({secondData}, () => {
                setTimeout(() => {
                    this.setState({
                        loading: false
                    })
                }, 1000)
            });
            await this.getTableControls(this.state.data);
        }

    }

    getTableControls = (data) => {

        let tableControls = data.tableControls;
        let chartData = [];
        let tempData = [];

        tableControls.forEach((control, index) => {
            if(control.selectedStartHour && control.selectedStartMinute){
                tempData.push(control);
            }
        });

        const rise = parseInt(this.props.rise_hour) * 60 + parseInt(this.props.rise_minute);
        const set = parseInt(this.props.set_hour) * 60 + parseInt(this.props.set_minute);

        if(tempData.length){
            tempData.forEach((control, index) => {
                let start_time = '';
                if(control.selectedStartHour && control.selectedStartMinute){
                    start_time = parseInt(control.selectedStartHour.value || 0) * 60 + parseInt(control.selectedStartMinute.value || 0);
                    if(control.selectedTimeType.value !== 'single') {
                        if (control.selectedTimeCondition.value === 'minus') start_time *= -1;
                        start_time = (control.selectedTimeType.value === 'rise' ? rise : set) + parseInt(start_time);
                    }

                    let startTime = Math.abs(start_time);
                    let created_at = new Date();
                    created_at.setHours(attachZero(parseInt(startTime/60)));
                    created_at.setMinutes(parseInt(startTime%60));

                    chartData.push({
                        period : control.value+1,
                        start_time : start_time,
                        type : control.selectedTimeType.value,
                        temperature : control.expect_temperature,
                        created_at : date(created_at, 'yyyy-MM-dd HH:mm:00')
                    });

                    if(index === tempData.length-1){
                        chartData.push({
                            period : 0,
                            start_time : start_time,
                            type : control.selectedTimeType.value,
                            temperature : control.expect_temperature,
                            created_at : date(created_at, 'yyyy-MM-dd 23:59:00')
                        });
                    }
                }
            });
        }


        if(chartData.length){
            chartData.sort(function (a, b) {
                return a.created_at < b.created_at ? -1 : a.created_at > b.created_at ? 1 : 0;
            });
        }

        if(chartData[0].created_at !== date(new Date(), 'yyyy-MM-dd 00:00:00')){
            chartData.unshift({
                period : 0,
                start_time : 0,
                type : '',
                temperature : 0,
                created_at : date(new Date(), 'yyyy-MM-dd 00:00:00')
            });
        }

        this.setState({chartData}, () => {
            setTimeout(() => {
                this.setState({
                    loading: false
                })
            }, 1000)
        });
    };

    render() {
        return (
            <article id="lcModalAutoControlTemperatureGraphWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap full-screen">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalAutoControlTemperatureGraph" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">{this.state.tableSelectedIsUseTemperature === 'vantilation' ? '환기' : '난방'} 설정 온도값</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body lc-graph-modal">
                                    <section className="graph-wrap">
                                        <div className="graph-item-wrap">
                                            <Spinner spinning={this.state.loading}/>
                                            {
                                                this.state.chartData ?
                                                    (
                                                        <Fragment>
                                                            {
                                                                this.state.chartData.length ? (
                                                                    <LcChart chartId="modalSensorGraphContainer"
                                                                             chartData={this.state.chartData}
                                                                             chartType={this.state.sensor.type}
                                                                             sensorZoom={this.state.sensorZoom}
                                                                             sunDate={this.state.sunDate}
                                                                             secondData={this.state.secondData}
                                                                             modalSlideBar={true}/>
                                                                ) : null
                                                            }
                                                        </Fragment>
                                                    ) :
                                                    (
                                                        <div className="no-chart-data-wrap">
                                                            <p>해당 조건에 그래프 정보가 없습니다.</p>
                                                        </div>
                                                    )
                                            }
                                        </div>
                                    </section>
                                    <section className="lc-graph-bottom-wrap">
                                        <div className="lc-select-chart">
                                            <div className="select-chart-title">
                                                <label>{this.state.tableSelectedIsUseTemperature === 'vantilation' ? '난방' : '환기'}설정 같이보기</label>
                                            </div>
                                            <div className="select-module-wrap">
                                                <Select
                                                    value={this.state.selectedTable}
                                                    placeholder="주기 선택"
                                                    onChange={val => this.handleSelectedTable(val)}
                                                    isSearchable={false}
                                                    options={this.state.selectAutoControls}
                                                    styles={colourStyles}/>
                                            </div>
                                        </div>
                                        {this.state.tableAvgTemeperature ?
                                            <p className="lc-average">{this.state.tableSelectedIsUseTemperature === 'vantilation' ? '환기' : '난방'} 온도 평균값 : {this.state.tableAvgTemeperature}℃</p>
                                            : null
                                        }
                                        <div className="button-wrap">
                                            <button className="theme-button" onClick={this.close}>표 설정으로 돌아가기</button>
                                        </div>
                                    </section>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useSunDate(
    ({state, actions}) => ({
        rise_hour: state.rise_hour,
        rise_minute: state.rise_minute,
        set_hour: state.set_hour,
        set_minute: state.set_minute,
        setSunDate: actions.setSunDate
    })
)(useAutoControlTemperatureGraph(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalAutoControlTemperatureGraph))));