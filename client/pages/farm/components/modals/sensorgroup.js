import React, { Component } from 'react';
import { useSensorGroupAdd } from "../../contexts/modals/sensorgroupAdd";
import { useSensorGroup } from "../../contexts/modals/sensorgroup";
import { useSensorGroupDetail } from "../../contexts/modals/sensorgroupDetail"
import { useSocket } from "../../contexts/socket";
import { useAlert } from "../../contexts/alert";
import CONSTANT from "../../constants/constant";
import TRANSLATE from '../../constants/translate';
import sensorGroupManager from '../../managers/sensorgroup';
import { addListener, on } from "../../utils/socket";
import { noSession } from "../../utils/session";
import deepcopy from '../../../../utils/deepcopy';

const DEFAULT_TYPE = CONSTANT.sensorGroup.Type;
const SENSOR_TYPE_TRANSLATE = TRANSLATE.SENSOR;
const DEFAULT_VALUE = {
    groups: [],
    position: '',            
    condition: null,                        
};




class ModalSensorGroup extends Component {
    constructor(props) {
        super(props);
        
        this.moreMenuRefs = new Map();
        this.state = {
            groups: [],
            position: '',            
            condition: null,                        

        };
    }

    componentDidMount() {
        this.props.sync(this.init);
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('sensor_avg');
        
    }

    listener = (socket) => {
        const _this = this;
        on(socket)('sensor_avg', (house_id) => {
            if(house_id==this.state.house_id)
            {                                
                 _this.getGroups();
            }
        });
     
    };

    init = (data) => {
        
        let state = Object.assign({}, data);
        
        state.position = data.position;
        state.house_id = data.house_id;
        this.setState(state, () => {

            this.getGroups();
        });
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if (e) e.preventDefault();

        this.setState(DEFAULT_VALUE);
        document.removeEventListener('keyup', this.closeOnEscape);

        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };


    getGroups = () => {        
        const _this = this;
        let position='';
        if(this.state.position =='outer'){
            position = 'out';
        }else if (this.state.position =='inner'){
            position ='in';
        }
        let query = {
            house_id: this.state.house_id,
            position: position
        };
        
        sensorGroupManager.findAll(query, noSession(this.props.showDialog)((status, data) => {                   
            if (status === 200) {                
                
                _this.setState({
                    groups: data.data
                });
            } else if (status === 404) {
  
                _this.setState({
                    groups: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    openSensorGroupDetail = (e, index) => {
        let formtype={};
        document.removeEventListener('keyup', this.closeOnEscape);        
        for(let i=0; i<DEFAULT_TYPE.length; i++){
            if(this.state.groups[index].type==DEFAULT_TYPE[i].value){
                formtype=DEFAULT_TYPE[i];
                break;
            }
        }        
        let body = {            
            house_id:this.state.house_id,
            position: this.state.position,
            types:this.state.groups[index],
            formtype:formtype
        };        
        this.props.openSensorGroupDetail(body);
        
    };

    afterAlarmSetting = () => {
        document.addEventListener('keyup', this.closeOnEscape);
    };
    openSensorGroupModal = () => {

        let body = {
            position:this.state.position,
            house_id:this.state.house_id            
        }
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.openSensorGroupAdd(body,this.afterAlarmSetting);

    }
    removeGroup = (e, index, item) => {
        const ref = this.moreMenuRefs.get(index);
        const _this = this;
        ref.blur();        
        let body ={
            id:item.id,
            house_id:item.house_id
        }
        this.props.showDialog({
            alertText: "센서 그룹을 삭제하시겠습니까?",
            cancelText: "취소",
            submitText: "확인",
            submitCallback: () => {
                sensorGroupManager.remove(body, noSession(this.props.showDialog)((status, data) => {
                    if (status === 200) {
                        _this.props.showDialog({
                            alertText: "삭제되었습니다.",
                        });
                    } else {
                        _this.props.alertError(status, data);
                    }
                }));
            }
        })
    };

    toggleDetails = (e, toggleState) => {
        let state = Object.assign({}, this.state);
        let alarms = state.alarms;
        alarms.forEach((item, index) => {
            item.isalarmsTabOpen = toggleState;
        });
        this.setState(state);
    };




    togglealarmWrap = (e, index) => {
        let state = Object.assign({}, this.state);
        let alarms = state.alarms;
        alarms[index].isalarmsTabOpen = !alarms[index].isalarmsTabOpen;
        this.setState(state);
    };

    clickGroupsActivateState = (e, val, item, index) => {
        e.preventDefault();
        const _this = this;
        let groups = deepcopy(this.state.groups)
        if (item.is_use === val) {
            return false;
        }
       

        let body = {
            id: item.id,
            house_id:this.state.house_id,
            is_use: val
        };

        sensorGroupManager.activeupdate(body, noSession(this.props.showDialog)((status, data) => {            
            if (status === 200) {
                groups[index].is_use = val;
                this.setState({groups})     
           
            } else {
                _this.props.alertError(status, data);
            }
        }));

    };

    render() {
        return (
            <article id="lcModalSensorGroupWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalSensorGroup" className="modal-wrap">
                                <section className="modal-header">
                                    {
                                        <h3 className="lc-modal-title">{SENSOR_TYPE_TRANSLATE[this.state.position]}센서 그룹 설정</h3>
                                    }
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>
                                <section className="modal-body">
                                    {
                                        (
                                            <React.Fragment>
                                                <div className="sensorgroup-section-wrap">
                                                    <div className="button-wrap">
                                                        <button className="theme-text-button" onClick={this.openSensorGroupModal}>+그룹 설정 추가</button>
                                                    </div>
                                                </div>
                                                <div className="list-section-wrap">
                                                    {
                                                        this.state.groups && this.state.groups.length ?
                                                            (
                                                                <React.Fragment>

                                                                    <ul id="sensorGroup_List">
                                                                        {
                                                                            this.state.groups.map((item, index) => {
                                                                                return (
                                                                                    <li className="sensorGroup-list-item" key={index} style={{ zIndex: this.state.groups.length - index }}>
                                                                                        <article className="lc-content-card">
                                                                                            <div className="lc-sensorGroup-name-wrap">
                                                                                                <p className="lc-sensorGroup-name-wrap">
                                                                                                    <span className="lc-control-name-index">{(index + 1)}</span>
                                                                                                    <span className="lc-control-name">{item.name}</span>
                                                                                                </p>
                                                                                            </div>
                                                                                            <div className="lc-sensorGroup-wrap">
                                                                                                {
                                                                                                    
                                                                                                            <div className="ordinary-menu-wrap">
                                                                                                                <div className="toggle-radio-wrap two-state">
                                                                                                                    <input type="radio" name={`auto-control-activate-toggle-option-${index}`}
                                                                                                                        onChange={e => { }}
                                                                                                                        checked={item.is_use == true}
                                                                                                                    />

                                                                                                                    <input type="radio" name={`auto-control-activate-toggle-option-${index}`}
                                                                                                                        onChange={e => { }}
                                                                                                                        checked={item.is_use == false}
                                                                                                                    />

                                                                                                                    <label onClick={e => this.clickGroupsActivateState(e, true, item, index)}>ON</label>
                                                                                                                    <label onClick={e => this.clickGroupsActivateState(e, false, item, index)}>OFF</label>
                                                                                                                    <i className="toggle-slider-icon" />
                                                                                                                </div>
                                                                                                            </div>
                                                                                                       

                                                                                                }
                                                                                                <div className="three-dot-menu" tabIndex="0" ref={c => this.moreMenuRefs.set(index, c)} style={{ zIndex: ( this.state.groups.length - index) }}>
                                                                                                    <div className="icon-wrap">
                                                                                                        <i /><i /><i />
                                                                                                    </div>
                                                                                                    <div className="three-dot-view-more-menu">
                                                                                                        <ul>

                                                                                                            <li onClick={e => this.openSensorGroupDetail(e, index)}><span>수정</span></li>
                                                                                                            <li onClick={e => this.removeGroup(e, index, item)}><span>삭제</span></li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </article>
                                                                                        
                                                                                    </li>
                                                                                )

                                                                            })
                                                                        }
                                                                    </ul>
                                                                </React.Fragment>
                                                            ) :

                                                            <article className="lc-content-card">

                                                                <div className="lc-sensorGroup-name-wrap">
                                                                    <p className="lc-sensorGroup-name-wrap2">
                                                                        그룹 설정이 없습니다.
                                                                    </p>


                                                                </div>
                                                            </article>

                                                    }
                                                </div>
                                            </React.Fragment>

                                        )


                                    }
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useSocket(
    ({ state, actions }) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useSensorGroupDetail(
    ({ actions }) => ({
        openSensorGroupDetail: actions.open
    })
)(useSensorGroupAdd(
    ({ actions }) => ({
        openSensorGroupAdd: actions.open
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSensorGroup(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSensorGroup)))));
