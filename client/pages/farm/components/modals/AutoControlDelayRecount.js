import React, {Component} from 'react';
import {useAutoControlDelayRecount} from "../../contexts/modals/autoControlDealyRecount";
import {useAlert} from "../../contexts/alert";
import autoControlDelayRecountManager from '../../managers/autoControlDelayRecount';
import {colourStyles} from "../../utils/select";
import {noSession} from "../../utils/session";

class ModalAutoControlDelayRecount extends Component {
    constructor(props) {
        super(props);

        this.state = {
            autoControl: {
                autoControlItems: [{
                    control: {}
                }]
            },
            delay: '',
            recount: '',
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (autoControl) => {
        let temp = {
            autoControl: autoControl,
            delay: autoControl.delay ? parseInt(autoControl.delay / 60) : '',
            recount: autoControl.recount ? autoControl.recount : ''
        };
        this.setState(temp);
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState({
            autoControl: {
                autoControlItems: [{
                    control: {}
                }]
            },
            delay: '',
            recount: '',
        });
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    handleInput = (e, key) => {
        let temp = {};
        temp[key] = e.target.value;
        this.setState(temp);
    };

    submit = (e) => {
        e && e.preventDefault();
        const _this = this;

        let query = {
            id: this.state.autoControl.id,
            delay: this.state.delay * 60,
            recount: this.state.recount
        };
        autoControlDelayRecountManager.update(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.props.showDialog({
                    alertText: "지연/반복이 등록 되었습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {
        return (
            <article id="lcModalAutoControlDelayRecountWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalAutoControlDelayRecount" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">지연 반복 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <form onSubmit={this.submit}>
                                        <div className="lc-delay-wrap">
                                            <span className="lc-control-fixed-wrap">{this.state.autoControl.autoControlItems[0].control.control_name}</span>

                                            <div className="lc-delay-input-container">
                                                <div className="lc-delay-input-wrap">
                                                    <input type="number" className="lc-farm-input" value={this.state.delay} onChange={e => this.handleInput(e, 'delay')}/>
                                                    <span className="delay-recount-description">분 지연 후</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="lc-recount-input-wrap">
                                            <input type="number" className="lc-farm-input" value={this.state.recount} onChange={e => this.handleInput(e, 'recount')}/>
                                            <span className="delay-recount-description">회 반복</span>
                                        </div>
                                        <button className="theme-button" type="submit">등록</button>
                                    </form>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useAutoControlDelayRecount(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalAutoControlDelayRecount));
