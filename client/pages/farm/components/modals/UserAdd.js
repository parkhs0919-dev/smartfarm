import React, { Component } from 'react';
import { useUserAdd } from "../../contexts/modals/UserAdd";
import { useuser } from '../../contexts/modals/User'
import { useSocket } from "../../contexts/socket";
import { useAlert } from "../../contexts/alert";
import { colourStyles } from "../../../farm/utils/select";
import Select from 'react-select';
import userManager from '../../managers/user';
import deepcopy from "../../../../utils/deepcopy";
import { noSession } from "../../utils/session";

const TYPE_POWER = 'power';
const TYPE_MOTOR = 'motor';
const DEFAULT_VALUE =
{
    house_id: '',
    name: '',
    id: '',
    password: '',
    passwordcheck: '',
    motor: [],
    power: [],
    motoroption: [],
    poweroption: [],
    motoroptions: [],
    poweroptions: [],
    check: false
};
const ERROR = {
    code: 999,
    data: null,
    message: '사용 불가능한 아이디 입니다'
}

const SUCCESS = {
    code: 998,
    data: null,
    message: '사용 가능한 아이디 입니다'
}

class ModalUserAdd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            house_id: '',
            name: '',
            id: '',
            password: '',
            passwordcheck: '',
            motor: [],
            power: [],
            motoroption: [],
            poweroption: [],
            motoroptions: [],
            poweroptions: [],
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        this.setState({ house_id: data },
            () => {
                this.getPower();
                this.getMotor()
            })
        document.addEventListener('keyup', this.closeOnEscape);
    };

    optionmake = (type) => {
        if (type == 'motor') {
            let motoroption = deepcopy(this.state.motoroption)
            for (let i = 0; i < this.state.motoroption.length; i++) {
                motoroption[i] = { value: this.state.motoroption[i].id, label: this.state.motoroption[i].control_name }
            }
            this.setState({ motoroption, motoroptions: motoroption })
        }
        if (type == 'power') {
            let poweroption = deepcopy(this.state.poweroption)
            for (let i = 0; i < this.state.poweroption.length; i++) {
                poweroption[i] = { value: this.state.poweroption[i].id, label: this.state.poweroption[i].control_name }
            }
            this.setState({ poweroption, poweroptions: poweroption })
        }
    }

    getPower = () => {
        let query = {
            house_id: this.state.house_id,
            type: TYPE_POWER,
        };
        userManager.Controlfind(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                this.setState({ poweroption: data.data.rows }, () => { this.optionmake('power') })
            } else if (status === 404) {
                this.setState({
                    poweroption: []
                })
            } else {
                this.props.alertError(status, data);
            }
        }));
    }
    getMotor = () => {
        let query = {
            house_id: this.state.house_id,
            type: TYPE_MOTOR,
        };
        userManager.Controlfind(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                this.setState({ motoroption: data.data.rows }, () => { this.optionmake('motor') })
            } else if (status === 404) {
                this.setState({
                    motoroption: []
                })
            } else {
                this.props.alertError(status, data);
            }
        }));
    }

    close = (e) => {
        if (e) e.preventDefault();
        this.setState(DEFAULT_VALUE)
        document.removeEventListener('keyup', this.closeOnEscape);

        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {
        if (e) e.preventDefault();
        let state = Object.assign({}, this.state);
        let body = {};
        let motor = [], power = [];

        window.platform ? body.platform = window.platform : null
        if (!state.check) {
            return this.props.showDialog({
                alertText: "ID 중복 확인을 클릭해주세요."
            });
        }
        if (!state.name) {
            return this.props.showDialog({
                alertText: "이름을 입력해 주세요."
            });
        }
        if (!state.id) {
            return this.props.showDialog({
                alertText: "ID을 입력해 주세요."
            });
        }

        if (!state.id.length > 4) {
            return this.props.showDialog({
                alertText: "ID 자릿수를 확인해 주세요."
            });
        }

        if (!(state.password.length > 5 || state.passwordcheck.length > 5)) {
            return this.props.showDialog({
                alertText: "비밀번호 자릿수를 확인해 주세요."
            });
        }
        if (state.password != state.passwordcheck) {
            return this.props.showDialog({
                alertText: "비밀번호를 확인해 주세요."
            });
        }
        if (this.state.motor.length > 0) {
            for (let i = 0; i < this.state.motor.length; i++) {
                if (this.state.motor[i].option) {
                    motor.push(this.state.motor[i].option.value)
                } else {
                    return this.props.showDialog({
                        alertText: "모터장치를 확인해 주세요."
                    });
                }
            }
        }
        if (this.state.power.length > 0) {
            for (let i = 0; i < this.state.power.length; i++) {
                if (this.state.power[i].option) {
                    power.push(this.state.power[i].option.value)
                } else {
                    return this.props.showDialog({
                        alertText: "전원장치를 확인해 주세요."
                    });
                }
            }
        }
        body.name = state.name;
        body.id = state.id;
        body.password = state.password;
        body.is_use = 1;
        motor.length > 0 ? body.motor_list = motor.join(',') : null
        power.length > 0 ? body.power_list = power.join(',') : null
        userManager.create(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                this.props.alertError(status, data);
            } else if (status == 200) {
                this.props.changeuser();
                this.props.showDialog({
                    alertText: "사용자가 등록되었습니다.",
                    cancelCallback: () => {
                        this.close();
                    }
                })
            }
        }));
    };
    motorFilter = async () => {
        let motoroptions = deepcopy(this.state.motoroption);
        for (let i = 0; i < this.state.motoroption.length; i++) {
            for (let j = 0; j < this.state.motor.length; j++) {
                if (this.state.motoroption[i].value == this.state.motor[j].option.value) {
                    motoroptions = await motoroptions.filter(data => data.value !== this.state.motor[j].option.value)
                }
            }
        }
        await this.setState({ motoroptions })
    }
    handleSelectMotor = (option, index) => {
        let motor = deepcopy(this.state.motor)
        motor[index].option = option;
        this.setState({ motor }, () => { this.motorFilter(option) })
    }
    handleSelectPower = (option, index) => {
        let power = deepcopy(this.state.power)
        power[index].option = option;
        this.setState({ power }, () => { this.powerFilter(option) })
    }
    powerFilter = async (option) => {
        let poweroptions = deepcopy(this.state.poweroption);
        for (let i = 0; i < this.state.poweroption.length; i++) {
            for (let j = 0; j < this.state.power.length; j++) {
                if (this.state.poweroption[i].value == this.state.power[j].option.value) {
                    poweroptions = await poweroptions.filter(data => data.value !== this.state.power[j].option.value)
                }
            }
        }
        await this.setState({ poweroptions })
    }
    handlename = (e) => {
        const { value } = e.target;
        this.setState({
            name: value
        })
    }

    handleid = (e) => {
        const { value } = e.target;
        this.setState({
            id: value,
            check: false
        })
    }

    handlepassword = (e) => {
        const { value } = e.target;
        this.setState({
            password: value
        })
    }

    handlepasswordcheck = (e) => {
        const { value } = e.target;
        this.setState({
            passwordcheck: value
        })
    }
    motorADD = (e) => {
        e.preventDefault()
        const motor = deepcopy(this.state.motor)
        const length = motor.length;
        let body = { key: length, option: '' }
        motor[length] = body;
        this.setState({ motor })
    }
    motordelete = (e, options) => {
        e.preventDefault()
        this.setState({ motor: this.state.motor.filter(data => data.key !== options) }, (e) => this.motorFilter())
    }

    powerADD = (e) => {
        e.preventDefault()
        const power = deepcopy(this.state.power)
        const length = power.length;
        let body = { key: length, option: '' }
        power[length] = body;
        this.setState({ power })
    }
    powerdelete = (e, options) => {
        e.preventDefault()
        this.setState({ power: this.state.power.filter(data => data.key !== options) }, (e) => this.powerFilter())
    }

    overlapCheck = () => {
        let check = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
        let query = {
            uid: this.state.id,
            platform: window.platform ? window.platform : ''
        }
        if (check.test(this.state.id)) {
            return this.props.showDialog({
                alertText: "ID에 영문과 숫자만 입력해 주세요."
            });
        }
        userManager.opverlabCheck(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                if (data.data.code == '9999') {
                    this.props.alertError(status, ERROR);
                } else if (data.data.code = '0000') {
                    this.setState({ check: true })
                    this.props.alertError(status, SUCCESS);
                }
            } else if (status === 404) {
                this.props.alertError(status, data);

            } else {
                this.props.alertError(status, data);
            }
        }));
    }
    render() {
        return (
            <article id="lcModalUserAddWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalUserAdd" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">사용자 등록</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>
                                <section className="modal-body">
                                    <React.Fragment>
                                        <div className="User-register-wrap">
                                            <ul className="User-control-wrap">
                                                <li className="User-control-item">
                                                    <label className="User-form-label">이름</label>
                                                    <div className="select-module-wrap">
                                                        <input type="text" onChange={this.handlename} value={this.state.name} className="sensor-value-input" placeholder="이름을 입력해주세요" />
                                                    </div>
                                                </li>
                                                <li className="User-control-item">
                                                    <label className="User-form-label">ID</label>
                                                    <div className="select-module-wrap">
                                                        <input type="text" onChange={this.handleid} value={this.state.id} className="sensor-value-input" placeholder="ID을 입력해주세요" />
                                                        <button className="searchbutton" onClick={(e) => this.overlapCheck(e)}>중복체크</button>
                                                    </div>
                                                </li>
                                                <li className="User-control-item">
                                                    <label className="User-form-label" style={{ display: 'inherit' }}>비밀번호</label>
                                                    <span> [6자리 이상 입력해주세요]</span>
                                                    <div className="select-module-wrap">
                                                        <input type="text" onChange={this.handlepassword} value={this.state.password} className="sensor-value-input" placeholder="비밀번호를 입력해주세요" />
                                                    </div>
                                                </li>
                                                <li className="User-control-item">
                                                    <label className="User-form-label">비밀번호 확인</label>
                                                    <div className="select-module-wrap">
                                                        <input type="text" onChange={this.handlepasswordcheck} value={this.state.passwordcheck} className="sensor-value-input" placeholder="비밀번호를 재입력 해주세요" />
                                                    </div>
                                                </li>
                                            </ul>
                                            <div id="UserSettingWrap" className="User-form-section">
                                                <div className="User-setting-form User-condition-wrap">
                                                    <label className="User-form-label">제어 권한 선택</label>
                                                    <article className="lc-content-card">
                                                        <div className='form-label'>
                                                            <label>모터 장치 선택</label>
                                                            <button className='theme-text-button' onClick={(e) => this.motorADD(e)}>제어 추가</button>
                                                        </div>
                                                        <div className="lc-User-name-wrap">
                                                            <ul className="User-control-wrap">
                                                                {
                                                                    this.state.motor.map((item, index) => {
                                                                        return (
                                                                            <React.Fragment key={index}>
                                                                                <li className="User-control-item" style={{ width: '45%' }}>
                                                                                    <div className="select-module-wrap" style={{ zIndex: 11 + this.state.motor.length }}>
                                                                                        <Select value={item.option}
                                                                                            placeholder="모터 선택"
                                                                                            onChange={option => this.handleSelectMotor(option, index)}
                                                                                            isSearchable={false}
                                                                                            options={this.state.motoroptions}
                                                                                            styles={colourStyles} />

                                                                                    </div>
                                                                                </li>
                                                                                <button className='theme-text-button' onClick={(e) => this.motordelete(e, item.key)}>삭제</button>
                                                                            </React.Fragment>
                                                                        )
                                                                    })
                                                                }
                                                            </ul>
                                                        </div>
                                                    </article>
                                                    <article className="lc-content-card">
                                                        <div className='form-label'>
                                                            <label>전원 장치 선택</label>
                                                            <button className='theme-text-button' onClick={(e) => this.powerADD(e)}>제어 추가</button>
                                                        </div>
                                                        <div className="lc-User-name-wrap">
                                                            <ul className="User-control-wrap">
                                                                {
                                                                    this.state.power.map((item, index) => {
                                                                        return (
                                                                            <React.Fragment key={index}>
                                                                                <li className="User-control-item" style={{ width: '45%' }}>
                                                                                    <div className="select-module-wrap" >
                                                                                        <Select value={item.option}
                                                                                            placeholder="전원 선택"
                                                                                            onChange={option => this.handleSelectPower(option, index)}
                                                                                            isSearchable={false}
                                                                                            options={this.state.poweroptions}
                                                                                            styles={colourStyles} />

                                                                                    </div>
                                                                                </li>
                                                                                <button className='theme-text-button' onClick={(e) => this.powerdelete(e, item.key)}>삭제</button>
                                                                            </React.Fragment>
                                                                        )
                                                                    })
                                                                }
                                                            </ul>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                            <div id="autoControlButtonWrap" className="auto-control-form-section">
                                                <div className="button-wrap">
                                                    <button className="card-header-button" onClick={this.close}>취소</button>
                                                </div>
                                                <div className="button-wrap">
                                                    <button className="theme-button" onClick={this.submit}>등록</button>
                                                </div>
                                            </div>
                                        </div>
                                    </React.Fragment>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useSocket(
    ({ state, actions }) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useuser(
    ({ actions }) => ({
        changeuser: actions.changeuser
    })
)(useUserAdd(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalUserAdd))));
