import React, {Component, Fragment} from 'react';
import {useSocket} from "../../contexts/socket";
import {useAutoControlStep} from "../../contexts/modals/autoControlStep";
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import {colourStyles} from "../../utils/select";
import Select from 'react-select';
import TRANSLATE from '../../constants/translate';
import controlManager from "../../managers/control";
import CONSTANT from "../../constants/constant";
import autoControlStepManager from '../../managers/autoControlStep';
import {addListener, on} from "../../utils/socket";
import autoControlManager from "../../managers/autoControl";
import {attachZero} from "../../../../utils/filter";
import {highLightText, boldText} from "../../utils/highlightText";
import {returnTimeText} from "../../utils/timeText";

import deepcopy from '../../../../utils/deepcopy';
import {noSession} from "../../utils/session";

const TYPE_MOTOR = 'motor';
const TYPE_POWER = 'power';

const DAYS = CONSTANT.DAYS;
const AUTO_CONTROL_TYPE_MIX = "mix";
const AUTO_CONTROL_TYPE_SENSOR = "sensor";
const AUTO_CONTROL_TYPE_TIME = "time";
const SPECIEL_SENSOR_TYPE = CONSTANT.SPECIAL_SENSOR;
const CONTROL_STATE_OPTIONS = CONSTANT.CONTROL_OPTIONS.STATE;
const CONTROL_WAY_OPTIONS = CONSTANT.CONTROL_OPTIONS.WAY;
const CONTROL_RETURN_UNIT = CONSTANT.CONTROL_OPTIONS.returnUnit;
const SENSOR_TRANSLATE = TRANSLATE.SENSOR;
const SENSOR_CONDITION_TRANSLATE = TRANSLATE.SENSOR.condition;

const INIT_STATE = {
    settingValue: '',
    delayRecountVisible: false,
    selectedControlOption: null,
    selectedControlWayOption: null,
    controlUnitName: '',
    delay: '',
    recount: '',
    autoControl: {
        autoControlItems: [{
            control: {},
            autoControlSensors: [{
                sensor: {}
            }]
        }],
    }
};

class ModalAutoControlStep extends Component {
    constructor(props) {
        super(props);

        this.state = deepcopy(INIT_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        document.removeEventListener('keyup', this.closeOnEscape);
    }

    listener = (socket) => {
        const _this = this;
        on(socket)('auto-controls', () => {
            if(_this.props.isVisible) _this.findAutoControls();
        });
    };

    findAutoControls = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
        };
        autoControlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                let autoControl = Object.assign({}, _this.state.autoControl);
                let rows = data.data.rows;
                for(let i=0; i<rows.length; i++) {
                    if(rows[i].id === autoControl.id) {
                        autoControl = rows[i];
                        _this.setState({autoControl});
                        break;
                    }
                }
            } else if (status === 404) {
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    init = (autoControl) => {
        this.findControls();
        this.setState({autoControl});
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(Object.assign({}, INIT_STATE));
        this.props.callback && this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {
        e && e.preventDefault();
        const _this = this;

        let body = {
            auto_control_id: this.state.autoControl.id
        };
        if(!this.state.selectedControlOption) {
            return this.props.showDialog({
                alertText: "기기를 선택해주세요."
            });
        }

        if(!this.state.selectedControlStateOption) {
            return this.props.showDialog({
                alertText: "동작을 선택해주세요."
            });
        }


        body.control_id = this.state.selectedControlOption.value;
        if(this.state.selectedControlStateOption.hasValue) {
            if(!this.state.selectedControlWayOption) {
                return this.props.showDialog({
                    alertText: "단위를 선택해주세요."
                });
            }
            if(!this.state.settingValue) {
                return this.props.showDialog({
                    alertText: "값을 입력해주세요."
                });
            }
            //부분열기, 부분닫기, 예약켜짐, 예약꺼짐
            body.state = this.state.selectedControlStateOption.value.split('-partial')[0];
            body[this.state.selectedControlWayOption.value] = this.state.settingValue;

            if(this.state.selectedControlOption.origin.type === TYPE_MOTOR) {
                body[this.state.selectedControlWayOption.value] = this.state.settingValue;
            } else if (this.state.selectedControlOption.origin.type === TYPE_POWER) {
                body.time = this.returnPowerTimeByState(this.state.selectedControlWayOption.value, this.state.settingValue);
            }
        } else {
            //열기, 닫기, 켜짐, 꺼짐
            body.state = this.state.selectedControlStateOption.value;
            if(this.state.selectedControlOption.origin.type === "motor") {
                body.percentage = "200";
            }
        }
        if(this.state.delay) body.delay = this.state.delay * 60;
        body.recount = this.state.recount ? this.state.recount -1 : null;


        // autoControlStepManager.create(body, (status, data) => {
        //     if(status === 200) {
        //         _this.props.showDialog({
        //             alertText: "단계설정이 추가되었습니다.",
        //         })
        //     } else {
        //         _this.props.alertError(status, data);
        //     }
        // })
    };

    returnPowerTimeByState = (state, time) => {
        if(state === "hour") {
            return time * 60 * 60;
        } else if (state === "min") {
            return time * 60;
        }
        return time;
    };

    toggleDelayRecountWrap = () => {
        this.setState({
            delayRecountVisible: !this.state.delayRecountVisible
        })
    };

    returnTime = (type, autoControlItem) => {
        let str = '';
        if(autoControlItem.start_hour !== null && autoControlItem.start_minute !== null) {
            str += highLightText(`${attachZero(autoControlItem.start_hour)}:${attachZero(autoControlItem.start_minute)} `);

            if(autoControlItem.end_hour !== null && autoControlItem.end_minute !== null) {
                str += highLightText(`~ ${attachZero(autoControlItem.end_hour)}:${attachZero(autoControlItem.end_minute)} `);
            }
        }
        return (
            <span dangerouslySetInnerHTML={{__html: str}}/>
        )
    };

    returnCondition = (type, autoControlItem) => {
        let str = '';
        if(type === AUTO_CONTROL_TYPE_MIX) {
            for(let i=0; i<autoControlItem.autoControlSensors.length; i++) {
                let sensor = autoControlItem.autoControlSensors[i];
                if(i > 0) {
                    str += autoControlItem.sensor_operator === "and" ? highLightText(' 그리고 ') : highLightText(' 또는 ')
                }
                if(SPECIEL_SENSOR_TYPE[sensor.sensor.type]) {
                    if(sensor.sensor.type === "windDirection"||sensor.sensor.type === "korinsWindDirection") {
                        str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(sensor.sensor.unit.split(':')[parseInt(sensor.value/45)])}`;
                    } else {
                        str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(sensor.sensor.unit.split(':')[sensor.value])}`;
                    }
                } else {
                    str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(sensor.value)}${highLightText(sensor.sensor.unit)} ${SENSOR_TRANSLATE.condition[sensor.condition]}`
                }
            }
        } else if (type === AUTO_CONTROL_TYPE_SENSOR) {
            let sensor = autoControlItem.autoControlSensors[0];
            if(SPECIEL_SENSOR_TYPE[sensor.sensor.type]) {
                if(sensor.sensor.type === "windDirection"||sensor.sensor.type === "korinsWindDirection") {
                    str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(sensor.sensor.unit.split(':')[parseInt(sensor.value/45)])}`;
                } else {
                    str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(sensor.sensor.unit.split(':')[sensor.value])}`;
                }
            } else {
                str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(sensor.value)}${highLightText(sensor.sensor.unit)} ${SENSOR_TRANSLATE.condition[sensor.condition]}`
            }
        }
        if(str.length) str += '일 때, ';
        return (
            <span dangerouslySetInnerHTML={{__html: str}}/>
        );
    };

    returnControl = (control) => {
        return (
            <span dangerouslySetInnerHTML={{__html: `${boldText(control.control_name)}(을)를 `}}/>
        )
    };

    returnActions = (stepItem) => {
        if(stepItem.control.type === TYPE_MOTOR) {
            if(stepItem.percentage) {
                if(parseInt(stepItem.percentage) === 200) {
                    if(stepItem.state === "open"){
                        return "전체 열기";
                    } else if (stepItem.state === "close") {
                        return "전체 닫기";
                    }
                } else {
                    if(stepItem.state === "open"){
                        return "부분 열기";
                    } else if (stepItem.state === "close") {
                        return "부분 닫기";
                    }
                }
            } else if (stepItem.time) {
                if(stepItem.state === "open"){
                    return "부분 열기";
                } else if (stepItem.state === "close") {
                    return "부분 닫기";
                }
            }
        } else if (stepItem.control.type === TYPE_POWER) {
            if(stepItem.time) {
                if(stepItem.state === "on"){
                    return "켜짐(예약 꺼짐)";
                } else if (stepItem.state === "off") {
                    return "꺼짐(예약 켜짐)";
                }
            }
            if(stepItem.state === "on"){
                return "켜짐";
            } else if (stepItem.state === "off") {
                return "꺼짐";
            }
        }
    };

    returnStepWay = (stepItem) => {
        if(stepItem.percentage) {
            if(parseInt(stepItem.percentage) !== 200) {
                return <span dangerouslySetInnerHTML={{__html: `${highLightText(stepItem.percentage + '%')}로 `}}/>;
            }
        } else if (stepItem.time) {
            return <span dangerouslySetInnerHTML={{__html: `${highLightText(returnTimeText(stepItem.time))} 동안 `}}/>;
        }
    };

    returnDelayRecount = (item) => {
        let str = '';
        if(item.delay) {
            str += '(';
            str += highLightText(returnTimeText(item.delay)) + ' 지연';
        }
        if(item.recount) {
            if(str.length) {
                str += ' - ';
            } else {
                str += '(';
            }
            str += highLightText(item.recount+1 + '회 반복');
        }
        if(str.length) str += ')';
        return (<span dangerouslySetInnerHTML={{__html: str}}/>);
    };

    returnControlCondition = (item) => {
        if(item.percentage) {
            if(parseInt(item.percentage) !== 200) {
                return <span dangerouslySetInnerHTML={{__html: `${highLightText(item.percentage + '%')}로 `}}/>;
            }
        } else if (item.time) {
            return <span dangerouslySetInnerHTML={{__html: `${highLightText(returnTimeText(item.time))} 동안 `}}/>;
        }
    };

    handleSelectControlWay = (selectedControlWayOption) => {
        let temp = {
            selectedControlWayOption: selectedControlWayOption,
            controlUnitName: CONTROL_RETURN_UNIT[selectedControlWayOption.value]
        };
        this.setState(temp);
    };

    handleSelectControlStateOption = (selectedControlStateOption) => {
        this.setState({selectedControlStateOption});
    };

    handleSelectControl = (selectedControlOption) => {
        let temp = {
            selectedControlOption: selectedControlOption,
            selectedControlStateOption: null,
            selectedControlWayOption: null,
        };
        this.setState(temp);
    };

    handleStepInput = (e, key) => {
        let temp = {};
        temp[key] = e.target.value;
        this.setState(temp);
    };

    generateControlOption = (controls) => {
        let selectControlOption = [];
        controls.forEach(item => {
            let obj = {
                value: item.id,
                label: item.control_name,
                origin: item
            };
            selectControlOption.push(obj);
        });
        return selectControlOption;
    };

    removeAutoControlStep = (e, step, index) => {
        const _this = this;
        this.props.showDialog({
            alertText: "해당 단계제어를 삭제하시겠습니까?",
            cancelText: "취소",
            submitText: "확인",
            submitCallback: () => {
                autoControlStepManager.remove(step.id, noSession(this.props.showDialog)((status, data) => {
                    if(status === 200) {
                        _this.props.showDialog({
                            alertText: "삭제되었습니다.",
                            cancelCallback: () => {
                                let temp = Object.assign({}, this.state.autoControl);
                                let autoControlSteps = JSON.parse(JSON.stringify(temp.autoControlSteps));
                                autoControlSteps.splice(index, 1);
                                temp.autoControlSteps = autoControlSteps;
                                _this.setState(temp);
                            }
                        })
                    } else {
                        _this.props.alertError(status, data);
                    }
                }));
            }
        });
    };

    findControls = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            types: 'motor,power'
        };
        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    selectControlOption: _this.generateControlOption(data.data.rows)
                });
            } else if (status === 404) {
                _this.setState({
                    selectControlOption: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    returnAutoControlDays = (item) => {
        let days = {};
        let returnArr = [];
        days.is_fri = item.is_fri;
        days.is_mon = item.is_mon;
        days.is_sat = item.is_sat;
        days.is_sun = item.is_sun;
        days.is_thur = item.is_thur;
        days.is_tue = item.is_tue;
        days.is_wed = item.is_wed;

        for(let key in days) {
            if(days[key] === true) {
                returnArr.push(DAYS[key]);
            }
        }
        if(returnArr.length === 7) {
            return '매일';
        }
        return returnArr.join(',');
    };

    render() {
        return (
            <article id="lcModalAutoControlStepWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalAutoControlStep" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">단계 제어 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                {
                                    this.state.autoControl &&
                                    (
                                        <section className="modal-body">
                                            <div className="basic-setting-wrap">
                                                <label className="modal-section-label">기본설정</label>
                                                <span></span>
                                                <article className={"step-setting-info-wrap" + (this.state.autoControl.autoControlSteps && this.state.autoControl.autoControlSteps.length ? " has-steps" : "")}>
                                                    <p className="step-description">
                                                        <span className="step-index">1</span>
                                                        <span> [{this.returnAutoControlDays(this.state.autoControl)}] </span>
                                                        {this.returnTime(this.state.autoControl.type, this.state.autoControl.autoControlItems[0])}
                                                        {this.returnCondition(this.state.autoControl.type, this.state.autoControl.autoControlItems[0])}
                                                        {this.returnControl(this.state.autoControl.autoControlItems[0].control)}
                                                        {this.returnControlCondition(this.state.autoControl.autoControlItems[0])}
                                                        <span className="lc-theme-color">{this.returnActions(this.state.autoControl.autoControlItems[0])}</span>(을)를 해주세요.
                                                        {this.returnDelayRecount(this.state.autoControl)}
                                                    </p>
                                                </article>
                                                {
                                                    this.state.autoControl.autoControlSteps && this.state.autoControl.autoControlSteps.length ?
                                                        (
                                                            <ol className="added-step-setting-list">
                                                                {
                                                                    this.state.autoControl.autoControlSteps.map((step, index) => {
                                                                        return (
                                                                            <li key={index}>
                                                                                <article className="step-setting-info-wrap">
                                                                                    <p className="step-description">
                                                                                        <span className="step-index">{2+index}</span>
                                                                                        그 후, <span className="lc-bold-text">{step.control.control_name}</span>(을)를 {this.returnStepWay(step)} <span className="lc-theme-color">{this.returnActions(step)}</span> 해주세요. {this.returnDelayRecount(step)}
                                                                                    </p>
                                                                                    <button className="lc-close-btn" onClick={e => this.removeAutoControlStep(e, step, index)}/>
                                                                                </article>
                                                                            </li>
                                                                        )
                                                                    })
                                                                }
                                                            </ol>
                                                        ) : null
                                                }
                                            </div>

                                            <div className="step-setting-wrap">
                                                <label className="modal-section-label">실행</label>
                                                <form onSubmit={this.submit}>
                                                    <div className="select-container">
                                                        <div className="select-module-wrap lc-size-36">
                                                            <Select value={this.state.selectedControlOption}
                                                                    placeholder="기기 선택"
                                                                    onChange={this.handleSelectControl}
                                                                    isSearchable={ false }
                                                                    options={this.state.selectControlOption}
                                                                    styles={colourStyles}/>
                                                        </div>
                                                        {
                                                            this.state.selectedControlOption &&
                                                            (
                                                                <Fragment>
                                                                    <div className="select-module-wrap lc-size-36">
                                                                        <Select value={this.state.selectedControlStateOption}
                                                                                placeholder="동작 선택"
                                                                                onChange={this.handleSelectControlStateOption}
                                                                                isSearchable={ false }
                                                                                options={CONTROL_STATE_OPTIONS[this.state.selectedControlOption.origin.type]}
                                                                                styles={colourStyles}/>
                                                                    </div>

                                                                    {
                                                                        this.state.selectedControlStateOption && this.state.selectedControlStateOption.hasValue && (
                                                                            <div className="form-flex-wrap">
                                                                                <div className="select-and-input-wrap">
                                                                                    <div className="select-and-input">
                                                                                        <div className="select-module-wrap lc-size-36">
                                                                                            <Select value={this.state.selectedControlWayOption}
                                                                                                    placeholder="단위 선택"
                                                                                                    onChange={this.handleSelectControlWay}
                                                                                                    isSearchable={false}
                                                                                                    options={CONTROL_WAY_OPTIONS[this.state.selectedControlOption.origin.type]}
                                                                                                    styles={colourStyles}/>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div className="select-and-input">
                                                                                        <input type="text" className="lc-farm-input" value={this.state.settingValue}
                                                                                               onChange={e => this.handleStepInput(e, 'settingValue')}
                                                                                               placeholder="입력"/>
                                                                                    </div>
                                                                                </div>

                                                                                <span className="lc-modifier">{this.state.controlUnitName}</span>
                                                                            </div>
                                                                        )
                                                                    }
                                                                </Fragment>
                                                            )
                                                        }
                                                    </div>

                                                    <div className="delay-recount-wrap">
                                                        <div className={"delay-recount-opener-wrap" + (this.state.delayRecountVisible ? " lc-open": "")} onClick={this.toggleDelayRecountWrap}>
                                                            <label className="modal-section-label">지연반복(선택항목)</label>
                                                            <i className="opener-btn"/>
                                                        </div>

                                                        {
                                                            this.state.delayRecountVisible &&
                                                            (
                                                                <div className="delay-recount-input-container">
                                                                    <div className="delay-recount-input-wrap">
                                                                        <input type="number" className="lc-farm-input" value={this.state.delay} onChange={e => this.handleStepInput(e, 'delay')}/>
                                                                        <span className="delay-recount-description">분 지연</span>
                                                                    </div>
                                                                    <div className="delay-recount-input-wrap">
                                                                        <input type="number" className="lc-farm-input" value={this.state.recount} onChange={e => this.handleStepInput(e, 'recount')}/>
                                                                        <span className="delay-recount-description">회 반복</span>
                                                                    </div>
                                                                </div>
                                                            )
                                                        }
                                                    </div>

                                                    <button className="theme-button" type="submit">등록</button>
                                                </form>
                                            </div>
                                        </section>
                                    )
                                }
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useAutoControlStep(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalAutoControlStep))));
