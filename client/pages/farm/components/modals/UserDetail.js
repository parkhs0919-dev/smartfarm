import React, { Component } from 'react';
import { useUserDetail } from "../../contexts/modals/Userdetail";
import { useSocket } from "../../contexts/socket";
import { useAlert } from "../../contexts/alert";
import { colourStyles } from "../../../farm/utils/select";
import Select from 'react-select';
import { useuser } from '../../contexts/modals/User'
import userManager from '../../managers/user';
import deepcopy from "../../../../utils/deepcopy";
import { noSession } from "../../utils/session";

const TYPE_POWER = 'power';
const TYPE_MOTOR = 'motor';
const DEFAULT_VALUE =
{
    house_id: '',
    motor: [],
    power: [],
    motoroption: [],
    poweroption: [],
    motoroptions: [],
    poweroptions: [],
    user: {},
};

class ModalUserAdd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            house_id: '',
            motor: [],
            power: [],
            motoroption: [],
            poweroption: [],
            motoroptions: [],
            poweroptions: [],
            user: {},
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        this.setState({ user: data, name: data.name, is_use: data.is_use }, () => {
            this.getPower();
            this.getMotor();
            this.getUserPower();
            this.getUserMotor();
        })
        document.addEventListener('keyup', this.closeOnEscape);
    };
    getUserPower = () => {
        let query = {
            house_id: this.state.house_id,
            type: TYPE_POWER,
            id: this.state.user.user_id,
        };
        userManager.detailcontrolfind(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {                
                this.setState({ power: data.data.userControlData }, () => { this.optionmake('originpower') })
            } else if (status === 404) {
                this.setState({
                    power: []
                })
            } else {
                this.props.alertError(status, data);
            }
        }));
    }
    getUserMotor = () => {

        let query = {
            house_id: this.state.house_id,
            type: TYPE_MOTOR,
            id: this.state.user.user_id,

        };

        userManager.detailcontrolfind(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                this.setState({ motor: data.data.userControlData }, () => { this.optionmake('originmotor') })
            } else if (status === 404) {
                this.setState({
                    motor: []
                })
            } else {
                this.props.alertError(status, data);
            }
        }));
    }

    optionmake = async (type) => {
        if (type == 'motor') {
            let motoroption = deepcopy(this.state.motoroption)
            for (let i = 0; i < this.state.motoroption.length; i++) {
                motoroption[i] = { value: this.state.motoroption[i].id, label: this.state.motoroption[i].control_name }
            }
            this.setState({ motoroption, motoroptions: motoroption }, (e) => this.motorFilter())
        } else if (type == 'originmotor') {
            let motor = deepcopy(this.state.motor)

            for (let i = 0; i < this.state.motor.length; i++) {
                await this.setState({ motoroptions: this.state.motoroptions.filter(data => data.value !== this.state.motor[i].id) })
                motor[i].option = { value: this.state.motor[i].id, label: this.state.motor[i].control_name }
            }
            this.setState({ motor })
        }
        if (type == 'power') {
            let poweroption = deepcopy(this.state.poweroption)
            for (let i = 0; i < this.state.poweroption.length; i++) {
                poweroption[i] = { value: this.state.poweroption[i].id, label: this.state.poweroption[i].control_name }
            }
            this.setState({ poweroption, poweroptions: poweroption }, (e) => this.powerFilter())
        } else if (type == 'originpower') {
            let power = deepcopy(this.state.power)
            for (let i = 0; i < this.state.power.length; i++) {
                await this.setState({ poweroptions: this.state.poweroptions.filter(data => data.value !== this.state.power[i].id) })
                power[i].option = { value: this.state.power[i].id, label: this.state.power[i].control_name }
            }
            this.setState({ power })
        }
    }

    getPower = () => {
        let query = {
            house_id: this.state.house_id,
            type: TYPE_POWER,
        };
        userManager.Controlfind(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                this.setState({ poweroption: data.data.rows }, () => { this.optionmake('power') })
            } else if (status === 404) {
                this.setState({
                    poweroption: []
                })
            } else {
                this.props.alertError(status, data);
            }
        }));
    }
    getMotor = () => {
        let query = {
            house_id: this.state.house_id,
            type: TYPE_MOTOR,
        };
        userManager.Controlfind(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                this.setState({ motoroption: data.data.rows }, () => { this.optionmake('motor') })
            } else if (status === 404) {
                this.setState({
                    motoroption: []
                })
            } else {
                this.props.alertError(status, data);
            }
        }));
    }
    close = (e) => {
        if (e) e.preventDefault();
        this.setState(DEFAULT_VALUE)
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {
        if (e) e.preventDefault();
        let state = Object.assign({}, this.state);
        let body = {};
        let motor = [], power = [];

        window.platform ? body.platform = window.platform : null
        if (!state.name) {
            return this.props.showDialog({
                alertText: "이름을 입력해 주세요."
            });
        }
        if (this.state.motor) {
            if (this.state.motor.length > 0) {
                for (let i = 0; i < this.state.motor.length; i++) {
                    if (this.state.motor[i].option) {
                        motor.push(this.state.motor[i].option.value)
                    } else {
                        return this.props.showDialog({
                            alertText: "모터장치를 확인해 주세요."
                        });
                    }
                }
            }
        }
        if (this.state.power) {
            if (this.state.power.length > 0) {
                for (let i = 0; i < this.state.power.length; i++) {
                    if (this.state.power[i].option) {
                        power.push(this.state.power[i].option.value)
                    } else {
                        return this.props.showDialog({
                            alertText: "전원장치를 확인해 주세요."
                        });
                    }
                }
            }
        }

        body.id = this.state.user.user_id;
        body.is_use = state.user.is_use;
        motor.length > 0 ? body.motor_list = motor.join(',') : null
        power.length > 0 ? body.power_list = power.join(',') : null
        userManager.update(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                this.props.alertError(status, data);
            } else if (status == 200) {
                this.props.changeuser();
                this.props.showDialog({
                    alertText: "사용자가 수정되었습니다.",
                    cancelCallback: () => {
                        this.props.close();
                    },
                })
            }
        }));
    };
    motorFilter = async () => {
        let motoroptions = deepcopy(this.state.motoroption);
        for (let i = 0; i < this.state.motoroption.length; i++) {
            for (let j = 0; j < this.state.motor.length; j++) {
                if (this.state.motoroption[i].value == this.state.motor[j].option.value) {
                    motoroptions = await motoroptions.filter(data => data.value !== this.state.motor[j].option.value)
                }
            }
        }
        await this.setState({ motoroptions })
    }
    handleSelectMotor = (option, index) => {
        let motor = deepcopy(this.state.motor)
        motor[index].option = option;
        this.setState({ motor }, () => { this.motorFilter(option) })
    }
    handleSelectPower = (option, index) => {
        let power = deepcopy(this.state.power)
        power[index].option = option;
        this.setState({ power }, () => { this.powerFilter(option) })
    }
    powerFilter = async () => {
        let poweroptions = deepcopy(this.state.poweroption);
        for (let i = 0; i < this.state.poweroption.length; i++) {
            for (let j = 0; j < this.state.power.length; j++) {
                if (this.state.poweroption[i].value == this.state.power[j].option.value) {
                    poweroptions = await poweroptions.filter(data => data.value !== this.state.power[j].option.value)
                }
            }
        }
        await this.setState({ poweroptions })
    }

    motorADD = (e) => {
        e.preventDefault()
        const motor = deepcopy(this.state.motor)
        const length = motor.length;
        let body = { key: length, option: '' }
        motor[length] = body;
        this.setState({ motor })
    }
    motordelete = (e, options, index) => {
        e.preventDefault()
        this.setState({ motor: this.state.motor.filter(data => data.id !== options) }, (e) => this.motorFilter())
    }
    powerADD = (e) => {
        e.preventDefault()
        const power = deepcopy(this.state.power)
        const length = power.length;
        let body = { key: length, option: '' }
        power[length] = body;
        this.setState({ power })
    }
    powerdelete = (e, options, index) => {
        e.preventDefault()
        this.setState({ power: this.state.power.filter(data => data.id !== options) }, (e) => this.powerFilter())
    }

    clickCheckBox = (e, type) => {
        let user = deepcopy(this.state.user);
        user[type] = !user[type];
        this.setState({ user });
    };
    render() {
        return (
            <article id="lcModalUserAddWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalUserAdd" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">사용자 정보 수정</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>
                                <section className="modal-body">
                                    <React.Fragment>
                                        <div className="User-register-wrap">
                                            <form onSubmit={this.submit}>
                                                <ul className="User-control-wrap">
                                                    <li className="User-control-item">
                                                        <label className="User-form-label">이름</label>
                                                        <div className="select-module-wrap">
                                                            <input type="text" value={this.state.user.name} className="sensor-value-input" placeholder="이름을 입력해주세요" readOnly />
                                                        </div>
                                                    </li>
                                                    <li className="User-control-item">
                                                        <label className="User-form-label">ID</label>
                                                        <div className="select-module-wrap">
                                                            <input type="text" value={this.state.user.id} className="sensor-value-input" placeholder="이름을 입력해주세요" readOnly />
                                                        </div>
                                                    </li>
                                                    <li className="User-control-item">
                                                        <label className="User-form-label">비밀번호</label>
                                                        <div className="select-module-wrap">
                                                            <input type="text" value={this.state.user.password} className="sensor-value-input" placeholder="비밀번호를 입력해주세요" readOnly />
                                                        </div>
                                                    </li>
                                                </ul>
                                                {/* <ul id="screenOrderList">
                                                <span className='user-label'>계정 활성화</span>
                                            <li className="screen-order-list-item">
                                                <article
                                                    className="screen-setting-wrap">
                                                    <div className="screen-setting-left-wrap">
                                                        
                                                        <div className="lc-theme-checkbox-wrap">
                                                            <input id="mixControlCondition1"
                                                                value="time"
                                                                checked={this.state.user.is_use}
                                                                onChange={() => {
                                                                }} type="checkbox"
                                                                name="mix-control-condition" />
                                                            <label
                                                                htmlFor="mixControlCondition1"
                                                                onClick={(e) => this.clickCheckBox(e,'is_use')}
                                                                className="box-shape-label" />
                                                            <label
                                                                htmlFor="mixControlCondition1"
                                                                onClick={(e) => this.clickCheckBox(e,'is_use')}
                                                                className="text-label">계정을 사용합니다.</label>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>    */}
                                                <div id="UserSettingWrap" className="User-form-section">
                                                    <div className="User-setting-form User-condition-wrap">
                                                        <label className="User-form-label">제어 권한 선택</label>
                                                        <article className="lc-content-card">
                                                            <div className='form-label'>
                                                                <label>모터 장치 선택</label>
                                                                <button className='theme-text-button' onClick={(e) => this.motorADD(e)}>제어 추가</button>
                                                            </div>
                                                            <div className="lc-User-name-wrap">
                                                                <ul className="User-control-wrap">
                                                                    {
                                                                        this.state.motor.map((item, index) => {
                                                                            return (
                                                                                <React.Fragment key={index}>
                                                                                    <li className="User-control-item" style={{ width: '45%' }}>
                                                                                        <div className="select-module-wrap" style={{ zIndex: 11 + this.state.motor.length }}>
                                                                                            <Select value={item.option}
                                                                                                placeholder="모터 선택"
                                                                                                onChange={option => this.handleSelectMotor(option, index)}
                                                                                                isSearchable={false}
                                                                                                options={this.state.motoroptions}
                                                                                                styles={colourStyles} />
                                                                                        </div>
                                                                                    </li>
                                                                                    <button className='theme-text-button' onClick={(e) => this.motordelete(e, item.id, index)}>삭제</button>
                                                                                </React.Fragment>
                                                                            )
                                                                        })
                                                                    }
                                                                </ul>
                                                            </div>
                                                        </article>
                                                        <article className="lc-content-card">
                                                            <div className='form-label'>
                                                                <label>전원 장치 선택</label>
                                                                <button className='theme-text-button' onClick={(e) => this.powerADD(e)}>제어 추가</button>
                                                            </div>
                                                            <div className="lc-User-name-wrap">
                                                                <ul className="User-control-wrap">
                                                                    {
                                                                        this.state.power.map((item, index) => {
                                                                            return (
                                                                                <React.Fragment key={index}>
                                                                                    <li className="User-control-item" style={{ width: '45%' }}>
                                                                                        <div className="select-module-wrap" >
                                                                                            <Select value={item.option}
                                                                                                placeholder="전원 선택"
                                                                                                onChange={option => this.handleSelectPower(option, index)}
                                                                                                isSearchable={false}
                                                                                                options={this.state.poweroptions}
                                                                                                styles={colourStyles} />

                                                                                        </div>
                                                                                    </li>
                                                                                    <button className='theme-text-button' onClick={(e) => this.powerdelete(e, item.id, index)}>삭제</button>
                                                                                </React.Fragment>
                                                                            )
                                                                        })
                                                                    }
                                                                </ul>
                                                            </div>
                                                        </article>
                                                    </div>
                                                </div>
                                                <div id="autoControlButtonWrap" className="auto-control-form-section">
                                                    <div className="button-wrap">
                                                        <button className="card-header-button" onClick={this.close}>취소</button>
                                                    </div>
                                                    <div className="button-wrap">
                                                        <button className="theme-button" onClick={this.submit}>등록</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </React.Fragment>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}
export default useSocket(
    ({ state, actions }) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useuser(
    ({ actions }) => ({
        changeuser: actions.changeuser
    })
)(useUserDetail(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalUserAdd))));
