import React, {Component} from 'react';
import {useCCTV} from "../../contexts/modals/cctv";
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import deepcopy from "../../../../utils/deepcopy";
import cctvManager from '../../managers/cctv';
import {noSession} from "../../utils/session";

class ModalCCTV extends Component {
    constructor(props) {
        super(props);
        this.player = null;
        this.state = {
            cctv: {},
            id: '',
            leftMoving: true,
            rightMoving: true,
            topMoving: true,
            bottomMoving: true,
            zoomInMoving: true,
            zoomOutMoving: true,
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (cctv) => {
        const _this = this;

        this.setState({id:cctv.id});
        if (this.player) {
            const player = this.player;
            player.source.socket.close();
            player.destroy();
        }
        this.player = null;
        this.cctvModalRef = React.createRef();
        this.setState({
            cctv: {}
        }, () => _this.setState({cctv}, () => {
            _this.connectCCTV();
        }));

        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        const player = this.player;
        this.player = null;
        if(player) {
            player.source.socket.close();
            player.destroy();
        }
        this.setState({
            cctv: {},
            id : '',
        });
        this.props.callback();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    cctvActionHandler = async cctvCommand => {
        const state = this.state;
        let body = {id:this.state.id};

        if(cctvCommand === "cctvLeft"){
            body.x = parseFloat(-0.3);
            body.y = parseFloat(0);
            body.isActive = true;

            if(!state.topMoving || !state.bottomMoving || !state.zoomOutMoving || !state.zoomInMoving){
                body.isActive = false;
                setTimeout(() => {
                    body.x = parseFloat(-0.3);
                    body.y = parseFloat(0);
                    body.isActive = true;
                    cctvManager.cctvMove(body, noSession(this.props.showDialog)((status, data) => {}));
                },300);
            }

            await this.setState({
                leftMoving: false,
                rightMoving: true,
                topMoving: true,
                bottomMoving: true,
                zoomInMoving: true,
                zoomOutMoving: true,
            });

            if(!state.leftMoving){
                body.isActive = false;
                await this.setState({
                    leftMoving: true,
                    rightMoving: true,
                    topMoving: true,
                    bottomMoving: true,
                    zoomInMoving: true,
                    zoomOutMoving: true,
                });
            }

        }
        if(cctvCommand === "cctvRight"){
            body.x = parseFloat(0.3);
            body.y = parseFloat(0);
            body.isActive = true;

            if(!state.topMoving || !state.bottomMoving || !state.zoomOutMoving || !state.zoomInMoving){
                body.isActive = false;
                setTimeout(() => {
                    body.x = parseFloat(0.3);
                    body.y = parseFloat(0);
                    body.isActive = true;
                    cctvManager.cctvMove(body, noSession(this.props.showDialog)((status, data) => {}));
                },300);
            }

            await this.setState({
                leftMoving: true,
                rightMoving: false,
                topMoving: true,
                bottomMoving: true,
                zoomInMoving: true,
                zoomOutMoving: true,
            });
            if(!state.rightMoving){
                body.isActive = false;
                await this.setState({rightMoving:true});
            }
        }
        if(cctvCommand === "cctvTop"){
            body.x = parseFloat(0);
            body.y = parseFloat(0.3);
            body.isActive = true;

            if(!state.leftMoving || !state.rightMoving || !state.zoomOutMoving || !state.zoomInMoving){
                body.isActive = false;
                setTimeout(() => {
                    body.x = parseFloat(0);
                    body.y = parseFloat(0.3);
                    body.isActive = true;
                    cctvManager.cctvMove(body, noSession(this.props.showDialog)((status, data) => {}));
                },300);
            }

            await this.setState({
                leftMoving: true,
                rightMoving: true,
                topMoving: false,
                bottomMoving: true,
                zoomInMoving: true,
                zoomOutMoving: true,
            });
            if(!state.topMoving){
                body.isActive = false;
                await this.setState({
                    leftMoving: true,
                    rightMoving: true,
                    topMoving: true,
                    bottomMoving: true,
                    zoomInMoving: true,
                    zoomOutMoving: true,
                });
            }
        }
        if(cctvCommand === "cctvBottom"){
            body.x = parseFloat(0);
            body.y = parseFloat(-0.3);
            body.isActive = true;

            if(!state.leftMoving || !state.rightMoving || !state.zoomOutMoving || !state.zoomInMoving){
                body.isActive = false;
                setTimeout(() => {
                    body.x = parseFloat(0);
                    body.y = parseFloat(-0.3);
                    body.isActive = true;
                    cctvManager.cctvMove(body, noSession(this.props.showDialog)((status, data) => {}));
                },300);
            }

            await this.setState({
                leftMoving: true,
                rightMoving: true,
                topMoving: true,
                bottomMoving: false,
                zoomInMoving: true,
                zoomOutMoving: true,
            });
            if(!state.bottomMoving){
                body.isActive = false;
                await this.setState({
                    leftMoving: true,
                    rightMoving: true,
                    topMoving: true,
                    bottomMoving: true,
                    zoomInMoving: true,
                    zoomOutMoving: true,
                });
            }
        }
        if(cctvCommand === "cctvHome"){
            body.isHome = true;
            await this.setState({
                leftMoving: true,
                rightMoving: true,
                topMoving: true,
                bottomMoving: true,
                zoomInMoving: true,
                zoomOutMoving: true,
            });
        }
        if(cctvCommand === "cctvZoomIn"){
            body.z = parseFloat(0.2);
            body.isActive = true;

            if(!state.leftMoving || !state.rightMoving || !state.topMoving || !state.bottomMoving){
                body.isActive = false;
                setTimeout(() => {
                    body.z = parseFloat(0.2);
                    body.isActive = true;
                    cctvManager.cctvMove(body, noSession(this.props.showDialog)((status, data) => {}));
                },300);
            }

            await this.setState({leftMoving: true, rightMoving: true, topMoving: true, bottomMoving: true, zoomInMoving: false, zoomOutMoving: true,});
            if(!state.zoomInMoving){
                body.isActive = false;
                await this.setState({leftMoving: true, rightMoving: true, topMoving: true, bottomMoving: true, zoomInMoving: true, zoomOutMoving: true,});
            }
        }
        if(cctvCommand === "cctvZoomOut"){
            body.z = parseFloat(-0.2);
            body.isActive = true;

            if(!state.leftMoving || !state.rightMoving || !state.topMoving || !state.bottomMoving){
                body.isActive = false;
                setTimeout(() => {
                    body.z = parseFloat(-0.2);
                    body.isActive = true;
                    cctvManager.cctvMove(body, noSession(this.props.showDialog)((status, data) => {}));
                },300);
            }

            await this.setState({
                leftMoving: true,
                rightMoving: true,
                topMoving: true,
                bottomMoving: true,
                zoomInMoving: true,
                zoomOutMoving: false,
            });
            if(!state.zoomOutMoving){
                body.isActive = false;
                await this.setState({
                    leftMoving: true,
                    rightMoving: true,
                    topMoving: true,
                    bottomMoving: true,
                    zoomInMoving: true,
                    zoomOutMoving: true,
                });
            }
        }
        console.log("send ",body);
        cctvManager.cctvMove(body, noSession(this.props.showDialog)((status, data) => {}));
    };

    cctvSnapshot = () => {
        cctvManager.cctvSnapshot(this.state.id);
    };

    connectCCTV = () => {
        const _this = this;
        let cctv = deepcopy(this.state.cctv);
        cctvManager.cctvStart(cctv.id, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                const canvas = this.cctvModalRef.current;
                const player = new JSMpeg.Player(`ws://${location.hostname}:${cctv.port}/`, {canvas, disableWebAssembly: true});
                this.player = player;
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {
        return (
            <article id="lcModalCCTVWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalCCTV" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">CCTV</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div id="modalCCTVWrap">
                                        <canvas ref={this.cctvModalRef} style={{width: '100%'}}/>
                                    </div>
                                </section>
                                {this.state.cctv.type === "ptz" ?
                                    <section className="modal-footer">
                                        <div className="lc-cctv-footer-button-wrap">
                                            <div onClick={() => this.cctvActionHandler("cctvLeft")} className={this.state.leftMoving ? "" : " active-div active-arrow-l"}><span/></div>
                                            <div onClick={() => this.cctvActionHandler("cctvRight")} className={this.state.rightMoving ? "" : " active-div active-arrow-r"}><span/></div>
                                            <div onClick={() => this.cctvActionHandler("cctvTop")} className={this.state.topMoving ? "" : " active-div active-arrow-u"}><span/></div>
                                            <div onClick={() => this.cctvActionHandler("cctvBottom")} className={this.state.bottomMoving ? "" : " active-div active-arrow-d"}><span/></div>
                                            <div onClick={() => this.cctvActionHandler("cctvHome")}><span/></div>
                                            <div onClick={() => this.cctvActionHandler("cctvZoomIn")} className={this.state.zoomInMoving ? "" : " active-div active-zi"}><span/></div>
                                            <div onClick={() => this.cctvActionHandler("cctvZoomOut")} className={this.state.zoomOutMoving ? "" : " active-div active-zo"}><span/></div>
                                            <div onClick={() => this.cctvSnapshot()}><span/></div>
                                        </div>
                                    </section>
                                    : null
                                }
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useCCTV(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalCCTV)));
