import React, {Component} from 'react';
import {useSocket} from "../../contexts/socket";
import {useSensorTemperature} from "../../contexts/modals/sensorTemperature";
import {useAlert} from "../../contexts/alert";
import {addListener, on} from "../../utils/socket";
import deepcopy from "../../../../utils/deepcopy";

import temperatureManager from '../../managers/temperature';
import {noSession} from "../../utils/session";

const DEFAULT_VALUE = {
    sensor: {},
    avg: null,
    sunAvg: null,
    sum: null
};

class ModalSensorTemperature extends Component {
    constructor(props) {
        super(props);

        this.state = deepcopy(DEFAULT_VALUE);
    }

    componentDidMount() {
        this.props.sync(this.init);
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('inner-sensor-values');
    }

    init = (sensor) => {
        const _this = this;
        
        this.setState({sensor}, () => {
            _this.findTemperature();
        });
        document.addEventListener('keyup', this.closeOnEscape);
    };

    listener = (socket) => {
        const _this = this;
        
        on(socket)('inner-sensor-values', ({innerSensors}) => {
            if(_this.props.isVisible) _this.changeTemperature(innerSensors);
        });
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_VALUE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    changeTemperature = (sensors) => {
        for(let i=0; i<sensors.length; i++) {
            if(this.state.sensor.id && this.state.sensor.id === sensors[i].id) {
                let sensor = deepcopy(this.state.sensor);
                sensor.value = sensors[i].value;
                return this.setState({sensor});
            }
        }
    };

    findTemperature = () => {
        const _this = this;
        let body = {
            platform: 'farm',
            id: this.state.sensor.id
        };

        temperatureManager.findTemperature(body, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                
                _this.setState(data.data);
            } else {
               
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {
        return (
            <article id="lcModalSensorTemperatureWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalSensorTemperature" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">{this.state.sensor.sensor_name}</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div className="sensor-value-wrap">
                                        <article className="lc-content-card">
                                            <span className="sensor-name">현재 온도</span>
                                            <span className="sensor-value">{this.state.sensor.value}℃</span>
                                        </article>
                                        <article className="lc-content-card">
                                            <span className="sensor-name">일 평균 온도</span>
                                            {
                                                this.state.avg ?
                                                    (
                                                        <span className="sensor-value">{this.state.avg}℃</span>
                                                    ):
                                                    (
                                                        <span className="not-allowed-text">표시가 불가능한 상태입니다.</span>
                                                    )
                                            }
                                        </article>
                                    </div>
                                    <div className="sensor-value-wrap">
                                        <article className="lc-content-card">
                                            <span className="sensor-name">주간 평균 온도</span>
                                            {
                                                this.state.sunAvg && this.state.sunAvg.day ?
                                                    (
                                                        <span className="sensor-value">{this.state.sunAvg.day}℃</span>
                                                    ) :
                                                    (
                                                        <span className="not-allowed-text">표시가 불가능한 상태입니다.</span>
                                                    )
                                            }
                                        </article>
                                        <article className="lc-content-card">
                                            <span className="sensor-name">야간 평균 온도</span>
                                            {
                                                this.state.sunAvg && this.state.sunAvg.night ?
                                                    (
                                                        <span className="sensor-value">{this.state.sunAvg.night}℃</span>
                                                    ) :
                                                    (
                                                        <span className="not-allowed-text">표시가 불가능한 상태입니다.</span>
                                                    )
                                            }
                                        </article>
                                    </div>

                                    <div className="sensor-value-wrap">
                                        <article className="lc-content-card">
                                            <span className="sensor-name">적산온도</span>
                                            {
                                                this.state.sum ?
                                                    (
                                                        <span className="sensor-value">{this.state.sum}℃</span>
                                                    ) :
                                                    (
                                                        <span className="not-allowed-text">표시가 불가능한 상태입니다.</span>
                                                    )
                                            }
                                        </article>
                                    </div>

                                    <button className="theme-button" onClick={this.close}>확인</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useSensorTemperature(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSensorTemperature)));