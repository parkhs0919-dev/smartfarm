import React, {Component} from 'react';
import {useSocket} from "../../contexts/socket";
import {usesensorhumidityDeficit} from "../../contexts/modals/sensorhumidityDeficit";
import {useAlert} from "../../contexts/alert";
import {addListener, on} from "../../utils/socket";
import deepcopy from "../../../../utils/deepcopy";

import humidityDeficitManager from '../../managers/sensorhumidityDeficit';
import {noSession} from "../../utils/session";

const DEFAULT_VALUE = {
    sensor: {},
    absolute_humidity: null,
    dew_point_temperature: null,
    dryTemperature: null,
    humidity_deficit: null,
    relative_humidity: null,    
    water_vapor_pressure: null,    
    wetTemperature: null,    
};

class ModalsensorhumidityDeficit extends Component {
    constructor(props) {
        super(props);

        this.state = deepcopy(DEFAULT_VALUE);
    }

    componentDidMount() {
        this.props.sync(this.init);
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('inner-sensor-values');
    }

    init = (sensor) => {
        const _this = this;
        
        this.setState({sensor}, () => {
            _this.findhumidityDeficit();
        });
        document.addEventListener('keyup', this.closeOnEscape);
    };

    listener = (socket) => {
        const _this = this;
        on(socket)('inner-sensor-values', ({innerSensors}) => {
            if(_this.props.isVisible) _this.changehumidityDeficit(innerSensors);
        });
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_VALUE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        
        if (e.keyCode === 27) this.close();
    };

    changehumidityDeficit = (sensors) => {
        for(let i=0; i<sensors.length; i++) {
            if(this.state.sensor.id && this.state.sensor.id === sensors[i].id) {
                let sensor = deepcopy(this.state.sensor);
                sensor.value = sensors[i].value;
                return this.setState({sensor});
            }
        }
    };

    findhumidityDeficit = () => {
        const _this = this;
        let body = {
            id: this.state.sensor.id
        };
        humidityDeficitManager.findhumidityDeficit(body, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                
                _this.setState(data.data);
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {
        
        return (
            <article id="lcModalSensorTemperatureWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalSensorTemperature" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">{this.state.sensor.sensor_name}</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div className="sensor-value-wrap">
                                        <article className="lc-content-card">
                                            <span className="sensor-name">건구온도</span>
                                            <span className="sensor-value">{this.state.dryTemperature}℃</span>
                                        </article>
                                        <article className="lc-content-card">
                                            <span className="sensor-name">습구온도</span>
                                            <span className="sensor-value">{this.state.wetTemperature}℃</span>                                          
                                        </article>
                                    </div>

                                    <div className="sensor-value-wrap">
                                        <article className="lc-content-card">
                                            <span className="sensor-name">수증기압</span>
                                            <span className="sensor-value">{this.state.water_vapor_pressure}Pa</span>
                                        </article>
                                        <article className="lc-content-card">
                                            <span className="sensor-name">상대습도</span>
                                            <span className="sensor-value">{this.state.relative_humidity}%</span>
                                        </article>
                                        <article className="lc-content-card">
                                            <span className="sensor-name">절대습도</span>
                                            <span className="sensor-value">{this.state.absolute_humidity}g/m3</span>
                                        </article>
                                        <article className="lc-content-card">
                                            <span className="sensor-name">이슬점온도</span>
                                            <span className="sensor-value">{this.state.dew_point_temperature}℃</span>
                                        </article>
                                        <article className="lc-content-card">
                                            <span className="sensor-name">수분부족량(HD)</span>
                                            <span className="sensor-value">{this.state.humidity_deficit}g/m3</span>
                                        </article>
                                    </div>
                                                                       

                                    <button className="theme-button" onClick={this.close}>확인</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(usesensorhumidityDeficit(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalsensorhumidityDeficit)));