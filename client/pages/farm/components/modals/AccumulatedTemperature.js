import React, {Component} from 'react';
import {useAccumulatedTemperature} from "../../contexts/modals/accumulatedTemperature";
import {useHouse} from "../../contexts/house";
import {useAlert} from "../../contexts/alert";
import {colourStyles} from "../../utils/select";
import Select from 'react-select';

import accumulatedTemperatureManager from '../../managers/accumulatedTemperature';
import DatePicker from "react-datepicker/es";
import moment from "moment";
import {date} from "../../../../utils/filter";
import deepcopy from "../../../../utils/deepcopy";
import {noSession} from "../../utils/session";
const DATE_DISPLAY_FORMAT = "yyyy-MM-dd";

const DEFAULT_STATE = {
    selectSensorOption: [],
    selectedSensorOption: null,
    start_date: null,
    end_date: null,
    min_value: '',
};

class ModalAccumulatedTemperature extends Component {
    constructor(props) {
        super(props);

        this.state = deepcopy(DEFAULT_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        this.findPossibleSensors();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    handleSensorOption = (selectedSensorOption) => {
        this.findAccumulatedTemperatureSetting(selectedSensorOption.value);
        this.setState({selectedSensorOption});
    };

    generateSensorOption = (sensors) => {
        let selectSensorOption = [];
        sensors.forEach(item => {
            let obj = {
                value: item.id,
                label: item.sensor_name,
                origin: item
            };
            selectSensorOption.push(obj);
        });
        return selectSensorOption;
    };

    findAccumulatedTemperatureSetting = (sensor_id) => {
        const _this = this;
        this.setState({
            start_date: null,
            end_date: null,
            min_value: '',
        });
        accumulatedTemperatureManager.findById({sensor_id: sensor_id}, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                if(data.data) {
                    let temp = {};
                    data.data.start_date ? temp.start_date = moment(new Date(data.data.start_date)) : null;
                    data.data.end_date ? temp.end_date = moment(new Date(data.data.end_date)) : null;
                    data.data.min_value ? temp.min_value = data.data.min_value : '';
                    _this.setState(temp);
                }
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findPossibleSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            position: 'in'
        };
        accumulatedTemperatureManager.findAccumulatedTemperatureSensors(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                let selectSensorOption = _this.generateSensorOption(data.data.rows);
                _this.setState({selectSensorOption})
            } else if (status === 404) {
                _this.showDialog({
                    alertText: "설정가능한 센서가 없습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    handleDate = (val, key) => {
        let state = Object.assign({}, this.state);
        state[key] = val === null ? null : moment(date(new Date(val), DATE_DISPLAY_FORMAT));
        this.setState(state);
    };

    handleInput = (e) => {
        let value = e.target.value;
        this.setState({
            min_value: value
        });
    };

    setAccumulatedTemperature = (e) => {
        const _this = this;
        if(!this.state.selectedSensorOption) {
            return this.props.showDialog({
                alertText: "온도센서를 선택해주세요."
            });
        }
        if(!this.state.start_date) {
            return this.props.showDialog({
                alertText: "시작일을 선택해주세요."
            });
        }
        if(!this.state.end_date) {
            return this.props.showDialog({
                alertText: "종료일을 선택해주세요."
            });
        }
        let body = {
            id: this.state.selectedSensorOption.value,
            start_date: date(this.state.start_date, DATE_DISPLAY_FORMAT),
            end_date: date(this.state.end_date, DATE_DISPLAY_FORMAT),
        };
        body.min_value = this.state.min_value ? this.state.min_value : null;

        accumulatedTemperatureManager.setAccumulatedTemperature(body, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.props.showDialog({
                    alertText: "설정되었습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {
        return (
            <article id="lcModalAccumulatedTemperatureWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalAccumulatedTemperature" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">적산온도 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div className="acc-temp-item-wrap">
                                        <label className="lc-acc-temp-label">센서 선택</label>
                                        <div className="select-module-wrap">
                                            <Select value={this.state.selectedSensorOption}
                                                    placeholder="전체보기"
                                                    onChange={this.handleSensorOption}
                                                    isSearchable={ false }
                                                    options={this.state.selectSensorOption}
                                                    styles={colourStyles}/>
                                        </div>
                                    </div>

                                    <div className="acc-temp-item-wrap">
                                        <label className="lc-acc-temp-label">최소온도</label>
                                        <div className="input-unit-wrap">
                                            <input type="number" className="lc-farm-input" value={this.state.min_value}
                                            onChange={e => this.handleInput(e)}/>
                                            <span className="unit-span">℃</span>
                                        </div>
                                    </div>

                                    <div className="acc-temp-item-wrap">
                                        <label className="lc-acc-temp-label">기간설정</label>

                                        <div className="date-picker-container">
                                            <div className="date-picker-wrap">
                                                <DatePicker dateFormat={"YYYY-MM-DD"}
                                                            className="lc-farmlabs-datepicker"
                                                            popperClassName="lc-farmlabs-datepicker-popper"
                                                            selected={this.state.start_date}
                                                            isClearable={true}
                                                            placeholderText="시작일"
                                                            maxDate={this.state.end_date ? this.state.end_date : null}
                                                            onChange={val => this.handleDate(val, "start_date")}/>
                                            </div>

                                            <div className="date-picker-wrap">
                                                <DatePicker dateFormat={"YYYY-MM-DD"}
                                                            className="lc-farmlabs-datepicker"
                                                            popperClassName="lc-farmlabs-datepicker-popper right"
                                                            selected={this.state.end_date}
                                                            isClearable={true}
                                                            minDate={this.state.start_date}
                                                            placeholderText="종료일"
                                                            onChange={val => this.handleDate(val, "end_date")}/>
                                            </div>
                                        </div>
                                    </div>

                                    <button className="theme-button" onClick={e => this.setAccumulatedTemperature(e)}>설정</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useAccumulatedTemperature(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalAccumulatedTemperature)));