import React, {Component} from 'react';
import Select from 'react-select';
import {usePBand} from "../../contexts/modals/pBand";
import {useHouse} from "../../contexts/house";
import {useAlert} from "../../contexts/alert";
// import PBandSettingManager from "../../managers/pBandSensor";
import PBandsManager from "../../managers/pBands";

import deepcopy from "../../../../utils/deepcopy";
import {noSession} from "../../utils/session";
import sensorManager from "../../managers/sensor";
import {returnSensorSelect} from "../../utils/sensor";
import {colourStyles} from "../../utils/select";
import controlMinMaxRangesManager from "../../managers/controlMinMaxRanges";

const DEFAULT_STATE = {
    houseId: '',
    p_band_name : '',
    in_temperature_sensor_id: "",
    out_temperature_sensor_id: "",
    wind_speed_sensor_id: "",
    selectedInTemperature: "",
    selectedOutTemperature: "",
    selectedwindSpeed: "",
    wind_direction_forward_min_p : "",
    wind_direction_forward_max_p : "",
    wind_direction_backward_min_p : "",
    wind_direction_backward_max_p : "",
    inTemperature: [],
    outTemperature: [],
    windSpeed: [],
    is_min_max_p : false,
};


class ModalPBandSetting extends Component {
    constructor(props) {
        super(props);
        this.state = deepcopy(DEFAULT_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        const _this = this;
        this.findInTempSensors();
        this.findOutTempSensors();
        this.findWindSensors();

        if(data) {
            if(data.id){
                _this.setState({id : data.id});
            }

            if(data.p_band_name){
                _this.setState({p_band_name : data.p_band_name});
            }

            if(data.in_temperature_sensor_id){

                _this.setState({
                    selectedInTemperature: {
                        label: data.inTemperatureSensor.sensor_name,
                        value: data.inTemperatureSensor.id
                    },
                    in_temperature_sensor_id: data.inTemperatureSensor.id,
                });
            }

            if(data.out_temperature_sensor_id){
                _this.setState({
                    selectedOutTemperature: {
                        label: data.outTemperatureSensor.sensor_name,
                        value: data.outTemperatureSensor.id
                    },
                    out_temperature_sensor_id : data.outTemperatureSensor.id
                });
            }

            if(data.wind_speed_sensor_id){
                _this.setState({
                    selectedwindSpeed: {
                        label: data.windSpeedSensor.sensor_name,
                        value: data.windSpeedSensor.id
                    },
                    wind_speed_sensor_id : data.windSpeedSensor.id
                });
            }

            if(data.wind_direction_forward_min_p){
                _this.setState({wind_direction_forward_min_p : data.wind_direction_forward_min_p});
            }

            if(data.wind_direction_forward_max_p){
                _this.setState({wind_direction_forward_max_p : data.wind_direction_forward_max_p});
            }

            if(data.wind_direction_backward_min_p){
                _this.setState({wind_direction_backward_min_p : data.wind_direction_backward_min_p});
            }

            if(data.wind_direction_backward_max_p){
                _this.setState({wind_direction_backward_max_p : data.wind_direction_backward_max_p});
            }

            if(data.is_min_max_p){
                _this.setState({is_min_max_p : data.is_min_max_p});
            }
        }

        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if (e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findInTempSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            type: "temperature",
            position: "in",
        };
        sensorManager.findAll(query, noSession(this.props.show)((status, data) => {
            if (status === 200) {
                let inTemperature = [];

                data.data.rows.forEach((item, data) => {
                    inTemperature.push({
                        value: item.id,
                        label: item.sensor_name,
                        origin: item,
                    });
                });
                _this.setState({inTemperature});
            } else if (status === 404) {
                _this.setState({
                    inTemperature: [],
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findOutTempSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            type: "temperature",
            position: "out",
        };
        sensorManager.findAll(query, noSession(this.props.show)((status, data) => {
            if (status === 200) {
                let outTemperature = [];

                data.data.rows.forEach((item, data) => {
                    outTemperature.push({
                        value: item.id,
                        label: item.sensor_name,
                        origin: item,
                    });
                });
                _this.setState({outTemperature});
            } else if (status === 404) {
                _this.setState({
                    outTemperature: [],
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findWindSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            type: "windSpeed",
        };
        sensorManager.findAll(query, noSession(this.props.show)((status, data) => {
            if (status === 200) {
                let windSpeed = [];

                data.data.rows.forEach((item, data) => {
                    windSpeed.push({
                        value: item.id,
                        label: item.sensor_name,
                        origin: item
                    });
                });
                _this.setState({windSpeed});
            } else if (status === 404) {
                _this.setState({
                    windSpeed: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    // findPBand = () => {
    //     const _this = this;
    //     let house_id = this.props.getHouseId();
    //
    //     PBandSettingManager.findByHouseId(house_id, noSession(this.props.show)((status, data) => {
    //         if (status === 200) {
    //             _this.setState({
    //                 selectedInTemperature: {
    //                     label: data.data.inTemperatureSensor.sensor_name,
    //                     value: data.data.inTemperatureSensor.id
    //                 },
    //                 selectedOutTemperature: {
    //                     label: data.data.outTemperatureSensor.sensor_name,
    //                     value: data.data.outTemperatureSensor.id
    //                 },
    //                 selectedwindSpeed: {
    //                     label: data.data.windSpeedSensor.sensor_name,
    //                     value: data.data.windSpeedSensor.id
    //                 },
    //
    //                 wind_direction_forward_min_p : data.data.wind_direction_forward_min_p ? data.data.wind_direction_forward_min_p : '',
    //                 wind_direction_forward_max_p : data.data.wind_direction_forward_max_p? data.data.wind_direction_forward_max_p : '',
    //                 wind_direction_backward_min_p : data.data.wind_direction_backward_min_p? data.data.wind_direction_backward_min_p : '',
    //                 wind_direction_backward_max_p : data.data.wind_direction_backward_max_p? data.data.wind_direction_backward_max_p : '',
    //                 is_min_max_p : data.data.is_min_max_p ? data.data.is_min_max_p : false,
    //
    //             });
    //             if (data.data.inTemperatureSensor.id) {
    //                 _this.setState({in_temperature_sensor_id: data.data.inTemperatureSensor.id,});
    //             }
    //             if (data.data.outTemperatureSensor.id) {
    //                 _this.setState({out_temperature_sensor_id: data.data.outTemperatureSensor.id,});
    //             }
    //             if (data.data.windSpeedSensor.id) {
    //                 _this.setState({wind_speed_sensor_id: data.data.windSpeedSensor.id,});
    //             }
    //             if(data.data.wind_direction_forward_min_p){
    //                 _this.setState({wind_direction_forward_min_p : data.data.wind_direction_forward_min_p,});
    //             }
    //             if(data.data.wind_direction_forward_max_p){
    //                 _this.setState({wind_direction_forward_max_p : data.data.wind_direction_forward_max_p,});
    //             }
    //             if(data.data.wind_direction_backward_min_p){
    //                 _this.setState({wind_direction_backward_min_p : data.data.wind_direction_backward_min_p,});
    //             }
    //             if(data.data.wind_direction_backward_max_p){
    //                 _this.setState({wind_direction_backward_max_p : data.data.wind_direction_backward_max_p,});
    //             }
    //         } else if (status === 404) {
    //             _this.setState({
    //                 selectedInTemperature: {label: '센서를 설정해주세요.', value: ''},
    //                 selectedOutTemperature: {label: '센서를 설정해주세요.', value: ''},
    //                 selectedwindSpeed: {label: '센서를 설정해주세요.', value: ''},
    //                 in_temperature_sensor_id: '',
    //                 out_temperature_sensor_id: '',
    //                 wind_speed_sensor_id: '',
    //                 wind_direction_forward_min_p : '',
    //                 wind_direction_forward_max_p : '',
    //                 wind_direction_backward_min_p : '',
    //                 wind_direction_backward_max_p : '',
    //             });
    //         } else {
    //             _this.props.alertError(status, data);
    //         }
    //     }));
    // };

    setPBand = (e) => {
        e.preventDefault();

        const _this = this;

        let data = {
            house_id: this.props.getHouseId(),
        };

        if(!this.state.p_band_name){
            return this.props.show({
                alertText: 'P-BAND 설정명을\n입력해주세요.',
            })
        }
        if (!this.state.in_temperature_sensor_id) {
            return this.props.show({
                alertText: '내부 온도 센서를\n선택해주세요.',
            })
        }
        if (!this.state.out_temperature_sensor_id) {
            return this.props.show({
                alertText: '외부 온도 센서를\n선택해주세요.',
            })
        }
        if (!this.state.wind_speed_sensor_id) {
            return this.props.show({
                alertText: '풍속 센서를\n선택해주세요.',
            })
        }

        if(this.state.id){
            data.id = this.state.id;
        }

        data.p_band_name = this.state.p_band_name;
        data.in_temperature_sensor_id = this.state.in_temperature_sensor_id;
        data.out_temperature_sensor_id = this.state.out_temperature_sensor_id;
        data.wind_speed_sensor_id = this.state.wind_speed_sensor_id;

        if(this.state.is_min_max_p === true){
            if(!this.state.wind_direction_forward_min_p){
                return this.props.show({
                    alertText: '풍상최소 값을\n입력해주세요.',
                })
            }

            if(!this.state.wind_direction_forward_max_p){
                return this.props.show({
                    alertText: '풍상최대 값을\n입력해주세요.',
                })
            }
            if(!this.state.wind_direction_backward_min_p){
                return this.props.show({
                    alertText: '풍하최소 값을\n입력해주세요.',
                })
            }

            if(!this.state.wind_direction_backward_max_p){
                return this.props.show({
                    alertText: '풍하최대 값을\n입력해주세요.',
                })
            }
        }


        data.wind_direction_forward_min_p = this.state.wind_direction_forward_min_p;
        data.wind_direction_forward_max_p = this.state.wind_direction_forward_max_p;
        data.wind_direction_backward_min_p = this.state.wind_direction_backward_min_p;
        data.wind_direction_backward_max_p = this.state.wind_direction_backward_max_p;

        data.is_min_max_p = this.state.is_min_max_p;

        if(this.state.id){
            PBandsManager.update(data, noSession(this.props.show)((status, data) => {
                if(status === 200){
                    this.props.show({
                        alertText : 'P-밴드가 저장되었습니다.',
                        cancelCallback : () => {
                            this.close();
                        },
                    })
                } else {
                    _this.props.alertError(status, data);
                }
            }))
        }else{
            PBandsManager.create(data, noSession(this.props.show)((status, data) => {
                if(status === 200){
                    this.props.show({
                        alertText : 'P-밴드가 저장되었습니다.',
                        cancelCallback : () => {
                            this.close();
                        }
                    })
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        }

    };

    handleSensor = (e) => {

        if (e.origin.position === "in" && e.origin.type === "temperature") {
            this.setState({
                in_temperature_sensor_id: e.origin.id,
                selectedInTemperature: {label: e.origin.sensor_name, value: e.origin.id},
            });
        }
        if (e.origin.position === "out" && e.origin.type === "temperature") {
            this.setState({
                out_temperature_sensor_id: e.origin.id,
                selectedOutTemperature: {label: e.origin.sensor_name, value: e.origin.id},
            });
        }
        if (e.origin.position === "out" && e.origin.type === "windSpeed") {
            this.setState({
                wind_speed_sensor_id: e.origin.id,
                selectedwindSpeed: {label: e.origin.sensor_name, value: e.origin.id},
            });
        }

    };

    handleInput = (e, key) => {
        if (e.target.validity.valid) {
            let value = e.target.value;
            if (value < 0) value = 0;
            let temp = {};
            temp[key] = value;
            this.setState(temp);
        }
    };

    handleState = (e) => {

        this.setState({
            is_min_max_p : !this.state.is_min_max_p
        });

        e && e.preventDefault();
    };


    render() {
        return (
            <article id="lcModalPBandSettingWrap"
                     className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcPBandSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">P-밴드 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>
                                <section className="modal-body">
                                    <ul>
                                        <li>
                                            <div className="pband-label-wrap">
                                                <label className="pband-label">설정명</label>
                                            </div>
                                            <div className="pband-input-wrap">
                                                <div className="input-wrap">
                                                    <input type="text"
                                                           onChange={e => this.handleInput(e, 'p_band_name')}
                                                           value={this.state.p_band_name}
                                                           className="lc-farm-input"
                                                           placeholder={"설정명을 입력하세요."}/>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="pband-item-wrap">
                                                <div className="pband-label-wrap">
                                                    <label className="pband-label">내부 온도 센서</label>
                                                </div>
                                                <div className="pband-input-wrap">
                                                    <div className="input-wrap">
                                                        <Select value={this.state.selectedInTemperature}
                                                                placeholder="내부 온도 센서 선택"
                                                                onChange={e => this.handleSensor(e)}
                                                                isSearchable={false}
                                                                options={this.state.inTemperature}
                                                                styles={colourStyles}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="pband-item-wrap">
                                                <div className="pband-label-wrap">
                                                    <label className="pband-label">외부 온도 센서</label>
                                                </div>
                                                <div className="pband-input-wrap">
                                                    <div className="input-wrap">
                                                        <Select value={this.state.selectedOutTemperature}
                                                                placeholder="외부 온도 센서 선택"
                                                                onChange={e => this.handleSensor(e)}
                                                                isSearchable={false}
                                                                options={this.state.outTemperature}
                                                                styles={colourStyles}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="pband-item-wrap">
                                                <div className="pband-label-wrap">
                                                    <label className="pband-label">풍속 센서</label>
                                                </div>
                                                <div className="pband-input-wrap">
                                                    <div className="input-wrap">
                                                        <Select value={this.state.selectedwindSpeed}
                                                                placeholder="풍속 센서 선택"
                                                                onChange={e => this.handleSensor(e)}
                                                                isSearchable={false}
                                                                options={this.state.windSpeed}
                                                                styles={colourStyles}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="pband-item-wrap">
                                                <div className="lc-theme-checkbox-wrap">
                                                    <input id="pBandMinMaxP"
                                                           value="time"
                                                           checked={this.state.is_min_max_p}
                                                           onChange={() => {}}
                                                           type="checkbox"
                                                           name="mix-control-condition"/>
                                                    <label htmlFor="pBandMinMaxP"
                                                           onClick={e => this.handleState(e)}
                                                           className="box-shape-label"/>
                                                    <label htmlFor="pBandMinMaxP"
                                                           onClick={e => this.handleState(e)}
                                                           className={"pband-checkbox-label"}>
                                                        풍상/풍하 P-band 최대/최소값 지정
                                                    </label>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="pband-wind-direction-wrap">
                                            <div className="pband-item-wrap">
                                                <div className="pband-label-wrap">
                                                    <label className="pband-label">풍상최대</label>
                                                </div>
                                                <div className="pband-input-wrap">
                                                    <div className="input-wrap">
                                                        <input type="text"
                                                               pattern={"[0-9]*"}
                                                               value={this.state.wind_direction_forward_max_p}
                                                               className="lc-farm-input lc-pband-input"
                                                               onChange={e => this.handleInput(e, "wind_direction_forward_max_p")}
                                                               placeholder={"최대"}/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pband-item-wrap">
                                                <div className="pband-label-wrap">
                                                    <label className="pband-label">풍상최소</label>
                                                </div>
                                                <div className="pband-input-wrap">
                                                    <div className="input-wrap">
                                                        <input type="text"
                                                               pattern={"[0-9]*"}
                                                               value={this.state.wind_direction_forward_min_p}
                                                               className="lc-farm-input lc-pband-input"
                                                               onChange={e => this.handleInput(e, "wind_direction_forward_min_p")}
                                                               placeholder={"최소"}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="pband-wind-direction-wrap">
                                            <div className="pband-item-wrap">
                                                <div className="pband-label-wrap">
                                                    <label className="pband-label">풍하최대</label>
                                                </div>
                                                <div className="pband-input-wrap">
                                                    <div className="input-wrap wind-direction-input-wrap">
                                                        <input type="text"
                                                               pattern={"[0-9]*"}
                                                               value={this.state.wind_direction_backward_max_p}
                                                               className="lc-farm-input lc-pband-input"
                                                               onChange={e => this.handleInput(e, "wind_direction_backward_max_p")}
                                                               placeholder={"최대"}/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="pband-item-wrap">
                                                <div className="pband-label-wrap">
                                                    <label className="pband-label">풍하최소</label>
                                                </div>
                                                <div className="pband-input-wrap">
                                                    <div className="input-wrap wind-direction-input-wrap">
                                                        <input type="text"
                                                               pattern={"[0-9]*"}
                                                               value={this.state.wind_direction_backward_min_p}
                                                               className="lc-farm-input lc-pband-input"
                                                               onChange={e => this.handleInput(e, "wind_direction_backward_min_p")}
                                                               placeholder={"최소"}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <p className={"pband-item-label"}>* 각 모터의 풍상/풍하창 위치 설정은 [모터 설정]에서 지정할 수 있습니다.</p>
                                    <button className="theme-button" onClick={e => this.setPBand(e)}>설정</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        show: actions.show,
        alertError: actions.alertError,
    })
)(usePBand(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalPBandSetting)));
