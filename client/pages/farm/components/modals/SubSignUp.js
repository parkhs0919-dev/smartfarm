import React, {Component} from 'react';
import subUserManager from "../../managers/subUser";
import {useSubSignUp} from "../../contexts/modals/subSignUp";
import {useAlert} from "../../contexts/alert";

import deepcopy from "../../../../utils/deepcopy";
import {noSession} from "../../utils/session";

const DEFAULT_STATE = {
    uid: '',
    name: '',
    password: '',
    rePassword: '',
};

class ModalSubSignUp extends Component {
    constructor(props) {
        super(props);

        this.state = deepcopy(DEFAULT_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    handleInput = (e) => {
        let value = e.target.value;
        this.setState({
            min_value: value
        });
    };

    SubSignUpButton = (e) => {
        const _this = this;
        let body = {platform:"farm"};
        if(!this.state.name) {
            return this.props.show({
                alertText: "이름을 다시한번\n확인해 주세요"
            });
        } else if(!this.state.uid) {
            return this.props.show({
                alertText: "아이디를 다시한번\n확인해 주세요"
            });
        } else if(!this.state.password || !this.state.rePassword || this.state.password !== this.state.rePassword) {
            return this.props.show({
                alertText: "비밀번호를 다시한번\n확인해 주세요"
            });
        } else {
            return this.props.show({
                alertText: "가입하시겠습니까?",
                cancelText: "취소",
                submitText: "가입",
                submitCallback: () => {
                    body.uid = this.state.uid;
                    body.name = this.state.name;
                    body.password = this.state.password;

                    subUserManager.create(body, noSession(this.props.show)((status, data) => {
                        if (status !== 200) {
                            if (status === 409) {
                                _this.props.show({
                                    alertText: "이미 존재하는 아이디입니다.",
                                });
                            } else {
                                _this.props.show({
                                    alertText: "회원가입이 실패했습니다.",
                                });
                            }
                        } else {
                            _this.props.show({
                                alertText: "가입이 완료 되었습니다.",
                                cancelCallback: () => {
                                    this.close();
                                }
                            });
                        }
                    }));
                }
            })
        }




    };

    render() {
        return (
            <article id="lcModalSubSignUpWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalSubSignUp" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">서브 관리자 계정 추가</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">

                                    <div className="sub-sign-up-wrap">
                                         <label className="lc-sub-signup-label">이름</label>
                                         <input type="text" className="lc-farm-input" value={this.state.name} onChange={e => this.setState({name: e.target.value})} placeholder="이름을 입력해 주세요" required/>
                                    </div>

                                    <div className="sub-sign-up-wrap">
                                         <label className="lc-sub-signup-label">아이디</label>
                                         <input type="text" className="lc-farm-input" value={this.state.uid} onChange={e => this.setState({uid: e.target.value})} placeholder="아이디를 입력해 주세요" required/>
                                    </div>

                                    <div className="sub-sign-up-wrap">
                                        <label className="lc-sub-signup-label">비밀번호</label>
                                        <input type="password" className="lc-farm-input" value={this.state.password} onChange={e => this.setState({password: e.target.value})} placeholder="비밀번호를 입력해 주세요" required/>
                                    </div>
                                    <div className="sub-sign-up-wrap">
                                        <label className="lc-sub-signup-label">비밀번호 재확인</label>
                                        <input type="password" className="lc-farm-input" value={this.state.rePassword} onChange={e => this.setState({rePassword: e.target.value})} placeholder="비밀번호를 다시 입력해 주세요" required/>
                                    </div>

                                    <button className="theme-button" onClick={e => this.SubSignUpButton(e)}>가입</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({actions}) => ({
        show: actions.show,
        alertError: actions.alertError,
    })
)(useSubSignUp(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSubSignUp));