import React, {Component, Fragment} from 'react';
import {useSocket} from "../../contexts/socket";
import {useAutoControl} from "../../contexts/modals/autoControl";
import {useAutoControlStep} from "../../contexts/modals/autoControlStep";
import {useAutoControlDelayRecount} from "../../contexts/modals/autoControlDealyRecount";
import {useAutoControlForm} from "../../contexts/modals/autoControlForm";
import {useAutoControlFileUpload} from "../../contexts/modals/autoControlFileUpload";
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import {colourStyles} from "../../utils/select";
import Select from 'react-select';
import {addListener, on} from "../../utils/socket";
import {date, attachZero} from "../../../../utils/filter";
import autoControlManager from '../../managers/autoControl';
import autoControlStateManager from '../../managers/autoControlState';
import autoControlOrderManager from '../../managers/autoControlOrder';
import controlManager from '../../managers/control';
import controlGroupManager from '../../managers/controlGroup';
import CONSTANT from '../../constants/constant';
import TRANSLATE from '../../constants/translate';
import deepcopy from '../../../../utils/deepcopy';
import {highLightText, boldText} from "../../utils/highlightText";
import {sunDateTimeToText, returnTimeText} from "../../utils/timeText";
import {noSession} from "../../utils/session";
import {useSunDate} from "../../contexts/sunDate";
import translate from "../../constants/translate";

const DAYS = CONSTANT.DAYS;
const DATE_DISPLAY_FORMAT = "yy. MM. dd";
const SENSOR_TRANSLATE = TRANSLATE.SENSOR;
const TYPE_MOTOR = 'motor';
const TYPE_POWER = 'power';
const SPECIEL_SENSOR_TYPE=CONSTANT.SPECIAL_SENSOR;
const AUTO_CONTROL_TYPE_MIX = "mix";
const AUTO_CONTROL_TYPE_SENSOR = "sensor";
const AUTO_CONTROL_TYPE_TIME = "time";
const AUTO_CONTROL_TYPE_STEP = "step";
const AUTO_CONTROL_TYPE_CONTROL = "control";


const CONTROL_NAME_TRANSLATE = {
    motor: "모터",
    power: "전원"
};

const MODE_ORDINARY = "ordinary";
const MODE_ORDERING = "order";
const DEFAULT_STATE = {
    step: 0,
    forwardCount: 0,
    backwardCount: 0,
    controls: [],
    control_id: null,
    selectedControl: null,
    selectedOption: null,
    cardMode: MODE_ORDINARY,
    autoControlStepState: false,
    selectedControlOption: null,
    autoControls: [],
    selectControlOption: [],
    isActiveDate: false,
    tempPeriod: 0,
};

class ModalAutoControl extends Component {
    constructor(props) {
        super(props);
        this.moreMenuRefs = new Map();
        this.state = deepcopy(DEFAULT_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
        addListener(this, this.listener);
    }

    listener = (socket) => {
        const _this = this;
        on(socket)('auto-controls', () => {
            if(_this.props.isVisible) {
                _this.findAutoControls();
            }
        });
    };

    init = (data) => {

        let control_id = undefined;
        if(data && data.control_id !== undefined) {
            control_id = data.control_id;
        }

        this.setState(data, () => {
            this.getWindDirectionCount();
            if(data.step === 2) {
                this.findControls(control_id);
                this.findControlGroups();
                this.findAutoControls(undefined);
            } else {
                this.findAllControls();
                this.findControlGroups();
            }
        });
        document.addEventListener('keyup', this.closeOnEscape);
    };

    getWindDirectionCount = () => {
        const _this = this;
        autoControlManager.getWindDirectionCount(this.props.getHouseId(), noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    forwardCount: data.data.windDirectionForwardCount,
                    backwardCount: data.data.windDirectionBackwardCount
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    toggleAutoControlStepWrap = (e, index) => {
        let state = Object.assign({}, this.state);
        let autoControls = state.autoControls;
        autoControls[index].isAutoControlTabOpen = !autoControls[index].isAutoControlTabOpen;
        this.setState(state);
    };

    selectControl = (e, item, type) => {
        if(!item) {
            return this.setState({
                step: 2,
                control_id: null,
                control_group_id : null,
            }, () => {
                this.findAutoControls(undefined);
                this.findControls();
            })
        }

        if(type === 'controlGroup'){
            let control_group_id ={
                wind_direction_type : item.wind_direction_type,
                window_position : item.window_position
            }

            return this.setState({
                step : 2,
                control_id : null,
                control_group_id : control_group_id,
            }, () => {
                this.findAutoControls(null, 'controlGroup');
                this.findControls(null, control_group_id);
                this.findControlGroups(control_group_id);
            })
        }else{
            this.setState({
                control_id: item.id,
                step: 2,
                control_group_id : null,
            }, () => {
                this.findAutoControls(null, 'control');
                this.findControls(item.id);
            });
        }



    };

    generateControlOptionSelect = (control_id, control_group_id) => {

        let arr = [
            {value: '', label: '전체보기'},
        ];
        let temp = {};
        let defaultSelected = null;

        if(this.state.controlGroups){
            this.state.controlGroups.forEach(item => {
                let obj = {};
                let value = translate.WINDOW_POSITION[item.window_position] + ' ' + translate.WIND_DIRECTION[item.wind_direction_type];
                obj.key = 'controlGroup';
                obj.value = {
                    wind_direction_type : item.wind_direction_type,
                    window_position : item.window_position,
                };
                obj.label = value;

                if(control_group_id && control_group_id.wind_direction_type === item.wind_direction_type && control_group_id.window_position == item.window_position) {
                    defaultSelected = obj;
                }

                arr.push(obj);

            })
        }

        this.state.controls.forEach(item => {
           let obj = {};
            obj.key = 'control';
            obj.value = item.id;
            obj.label = item.control_name;
           if(control_id && control_id === item.id) {
               defaultSelected = obj;
           }
           arr.push(obj);
        });

        temp.selectControlOption = arr;


        if (control_id) temp.selectedControlOption = defaultSelected;
        if (control_group_id) temp.selectedControlOption = defaultSelected;

        this.setState(temp);

    };

    findAllControls = () => {
        const _this = this;

        let query = {
            house_id: this.state.house_id
        };
        if(this.state.type) query.type = this.state.type;
        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    controls: data.data.rows
                });
            } else if (status === 404) {
                _this.setState({
                    controls: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findControlGroups = control_group => {
        const _this = this;


        let query = {
            house_id : this.props.getHouseId(),
        };

        if(control_group !== undefined) {
            if(control_group.wind_direction_type) query.wind_direction_type = control_group.wind_direction_type;
            if(control_group.window_position) query.window_position = control_group.window_position;
        };

        controlGroupManager.getList(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
               _this.setState({
                   controlGroups : data.data.rows,
               });

            } else if (status === 404) {
                _this.setState({
                    controlGroups : [],
                })
            } else {
                _this.props.alertError(status, data);
            }
        }))
    }

    findControls = (control_id, control_group_id) => {
        const _this = this;

        let query = {
            house_id: this.props.getHouseId(),
            types: 'motor,power'
        };

        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                this.setState({
                    controls : data.data.rows,
                })
                _this.generateControlOptionSelect(control_id, control_group_id);
            } else if (status === 404) {
                _this.generateControlOptionSelect([]);
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findAutoControls = (control, type) => {

        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            isActiveDate: this.state.isActiveDate,
        };
        this.setState({
            autoControls: []
        });

        if(control && control !== null && control !== undefined) {
            if(control.key === 'controlGroup'){
                query.wind_direction_type = control.value.wind_direction_type;
                query.window_position = control.value.window_position;
            }else if(control.key === 'control'){
                query.control_id = control.value;
            }
        }else if(type){
            if(type === 'controlGroup'){
                query.wind_direction_type = this.state.control_group_id.wind_direction_type;
                query.window_position = this.state.control_group_id.window_position;
            }else{
                query.control_id = this.state.control_id;
            }
        }else if(this.state.selectedControlOption){
            if(this.state.selectedControlOption.key === 'controlGroup'){
                query.wind_direction_type = this.state.selectedControlOption.value.wind_direction_type;
                query.window_position = this.state.selectedControlOption.value.window_position;
            }else{
                query.control_id = this.state.control_id;
            }
        }else if(this.state.control_id){
            query.control_id = this.state.control_id;
        }

        autoControlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    autoControls :data.data.rows
                });
                _this.generateAutoControlTables();
            } else if (status === 404) {
                _this.setState({
                    autoControls: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    generateAutoControlTables = () => {
        const _this = this;
        let autoControls = deepcopy(_this.state.autoControls);
        autoControls.forEach((autoControl, controlIndex) => {
            let autoControlTables = [];

            if(autoControl.type === "table"){
                autoControl.autoControlItems.forEach((item, itemIndex) => {
                    if (!autoControlTables[item.period]) {
                        autoControlTables[item.period] = [];
                    }
                    autoControlTables[item.period].push(item);
                });

                let lastPeriod = Object.keys(autoControlTables)[Object.keys(autoControlTables).length-1];
                if(Object.keys(autoControlTables).length < 6){
                    //저장된 주기가 6보다 작을 때, 6주기까지 임의 생성
                    for(let i=1; i<=6; i++){
                        if(!autoControlTables[i]){
                            autoControlTables[i] = [];
                        }
                    }
                }else{
                    //이어진 주기가 아닌 떨어진 주기를 입력했을 때
                    for(let i=1; i<lastPeriod; i++){
                        if(!autoControlTables[i]){
                            autoControlTables[i] = [];
                        }
                    }
                }

                autoControl.autoControlTables = autoControlTables;
            }
        })

        this.setState({autoControls});
    };

    handleSelectControl = (selectedControlOption) => {
        const _this = this;

        let temp = {
            cardMode: MODE_ORDINARY,
            selectedControlOption: selectedControlOption,
        };

        if(selectedControlOption.key === 'controlGroup'){
            temp.control_group_id = selectedControlOption.value;
        }else{
            temp.control_id = selectedControlOption.value;
        }


        this.setState(temp, () => {
            _this.findAutoControls(selectedControlOption);
        });

    };

    clickAutoControlActivateState = (e, val, item, index) => {
        e.preventDefault();
        const _this = this;

        if(item.state === val) {
            return false;
        }

        this.checkCanAutoControl(() => {
            let body = {
                id: item.id,
                state: val
            };

            let autoControls = JSON.parse(JSON.stringify(_this.state.autoControls));
            autoControlStateManager.update(body, noSession(this.props.showDialog)((status, data) => {
                if(status === 200) {
                    autoControls[index].state = val;
                    _this.setState({
                        autoControls: autoControls
                    });
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        })
    };

    changeOrder = (e, index, direction) => {
        e && e.preventDefault();
        const _this = this;

        let autoControls = JSON.parse(JSON.stringify(this.state.autoControls));
        let targetAutoControl = null;
        if(direction === "down") {
            targetAutoControl = autoControls[index+1];
            // autoControls.splice(index+1, 0, autoControls.splice(index, 1)[0]);
        } else if (direction === "up") {
            targetAutoControl = autoControls[index-1];
            // autoControls.splice(index-1, 0, autoControls.splice(index, 1)[0]);
        }
        let body = {
            id: autoControls[index].id,
            order: targetAutoControl.order
        };

        autoControlOrderManager.update(body, noSession(this.props.showDialog)((status, data) => {
            if(status !== 200) {
                _this.props.alertError(status, data);
            }
        }));
    };

    changeControlOrder = () => {
        this.checkCanAutoControl(() => {
            this.toggleDetails(null, false);
            this.setState({
                cardMode: this.state.cardMode === MODE_ORDINARY ? MODE_ORDERING : MODE_ORDINARY
            })
        });
    };

    checkCanAutoControl = (callback) => {
        const _this = this;
        autoControlManager.checkCanAutoControl(this.props.getHouseId(), noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                callback()
            } else {
                _this.props.showDialog({
                    alertText: "자동제어를 변경할 수 없는<br/>상태입니다."
                })
            }
        }));
    };

    removeAutoControl = (e, index, item) => {
        const ref = this.moreMenuRefs.get(index);
        const _this = this;
        ref.blur();

        this.checkCanAutoControl(() => {
            _this.props.showDialog({
                alertText: "자동제어를 삭제하시겠습니까?",
                cancelText: "취소",
                submitText: "확인",
                submitCallback: () => {
                    autoControlManager.remove(item.id, noSession(this.props.showDialog)((status, data) => {
                        if(status !== 200) {
                            _this.props.alertError(status, data);
                        }
                    }));
                }
            });
        });
    };

    disableMotherModalClose = () => {
        document.removeEventListener('keyup', this.closeOnEscape);
    };

    enableMotherModalClose = () => {
        document.addEventListener('keyup', this.closeOnEscape);
    };

    updateAutoControl = (e, index, item) => {
        const ref = this.moreMenuRefs.get(index);
        ref.blur();
        const _this = this;
        this.disableMotherModalClose();
        this.checkCanAutoControl(() => {
            _this.props.openAutoControlForm({
                isUpdate: true,
                data: item
            }, this.enableMotherModalClose);
        });
    };

    openAutoControlStepModal = (e, index, item) => {
        if(item.type !== "step") {
            return this.props.showDialog({
                alertText: "단계 제어는 단계로 생성된<br>자동제어만 설정 가능합니다."
            })
        }
        const ref = this.moreMenuRefs.get(index);
        ref.blur();
        const _this = this;

        this.disableMotherModalClose();
        if(item.type === "mix" && item.autoControlItems.length > 1) {
            return this.props.showDialog({
                alertText: "복수의 제어가 설정된 자동제어는<br>단계 설정을 할 수 없습니다."
            })
        }
        this.checkCanAutoControl(() => {
            _this.props.openAutoControlStepModal(item, this.enableMotherModalClose);
        });
    };

    openAutoControlDelayRecountModal = (e, index, item) => {
        const ref = this.moreMenuRefs.get(index);
        ref.blur();
        const _this = this;
        this.disableMotherModalClose();
        if(item.type === "mix" && item.autoControlItems.length > 1) {
            return this.props.showDialog({
                alertText: "복수의 제어가 설정된 자동제어는<br>지연/반복 설정을 할 수 없습니다."
            })
        }

        this.checkCanAutoControl(() => {
            _this.props.openAutoControlDelayRecountModal(item, this.enableMotherModalClose);
        });
    };

    openAutoControlFileUpload = () => {
        const _this = this;
        this.disableMotherModalClose();

        this.checkCanAutoControl(() => {
            _this.props.openAutoControlFileUploadModal(null, this.enableMotherModalClose);
        });
    };

    openAutoControlCreate = () => {
        const _this = this;
        this.disableMotherModalClose();
        let options = {
            isUpdate: false
        };

        if(this.state.selectedControlOption && this.state.selectedControlOption.value) {
            options.selectedControlId = this.state.selectedControlOption.value;
            options.selectedControlKey = this.state.selectedControlOption.key;
        }

        this.checkCanAutoControl(() => {
            _this.props.openAutoControlForm(options, this.enableMotherModalClose);
        });
    };

    returnDateType = (item) => {
        let view = '';
        if(item.date_type === "day") {
            if(item.start_date) {
                view += `<span>${date(item.start_date, DATE_DISPLAY_FORMAT)}</span>`;
            }
            if(item.start_date || item.end_date) {
                view += '<span> ~ </span>';
            }
            if(item.end_date) {
                view += `<span>${date(item.end_date, DATE_DISPLAY_FORMAT)} </span>`;
            }
        } else if (item.date_type === "per") {
            if(item.start_date) {
                view += `<span>${date(item.start_date, DATE_DISPLAY_FORMAT)}</span>`;
            }
            if(item.end_date) {
                view += ` ~ <span>${date(item.end_date, DATE_DISPLAY_FORMAT)} </span>`;
            }
            if(item.per_date) {
                view += `<span> 기준 ${item.per_date}일 마다</span>`
            }
        }

        return <span dangerouslySetInnerHTML={{__html: view}}/>;
    };

    returnAutoControlDays = (item) => {
        if(item.date_type === "day") {
            let days = {};
            let returnArr = [];
            days.is_fri = item.is_fri;
            days.is_mon = item.is_mon;
            days.is_sat = item.is_sat;
            days.is_sun = item.is_sun;
            days.is_thur = item.is_thur;
            days.is_tue = item.is_tue;
            days.is_wed = item.is_wed;

            for(let key in days) {
                if(days[key] === true) {
                    returnArr.push(DAYS[key]);
                }
            }
            if(returnArr.length === 7) {
                return '[매일]';
            }
            return `[${returnArr.join(',')}]`;
        }
        return '';
    };

    toggleDetails = (e, toggleState) => {
        let state = Object.assign({}, this.state);
        let autoControls = state.autoControls;
        autoControls.forEach((item, index) => {
            item.isAutoControlTabOpen = toggleState;
        });
        this.setState(state);
    };

    returnTime = (type, autoControlItem) => {
        let str = '';
        if (autoControlItem.is_sun_date) {
            const rise = parseInt(this.props.rise_hour) * 60 + parseInt(this.props.rise_minute);
            const set = parseInt(this.props.set_hour) * 60 + parseInt(this.props.set_minute);
            const sun_date_start_time = autoControlItem.sun_date_start_time || 0;
            const sun_date_end_time = autoControlItem.sun_date_end_time || 0;

            let start = (autoControlItem.sun_date_start_type === 'rise' ? rise : set) + parseInt(sun_date_start_time);
            let end = (autoControlItem.sun_date_end_type === 'rise' ? rise : set) + parseInt(sun_date_end_time);

            start %= 1440;
            if (start < 0) start += 1440;
            end %= 1440;
            if (end < 0) end += 1440;

            str += highLightText(`${attachZero(parseInt(start / 60))}:${attachZero(start % 60)}  ~ ${attachZero(parseInt(end / 60))}:${attachZero(end % 60)}`);

            let startHour = parseInt(Math.abs(sun_date_start_time) / 60);
            let endHour = parseInt(Math.abs(sun_date_end_time) / 60);
            let startMinute = Math.abs(sun_date_start_time) % 60;
            let endMinute = Math.abs(sun_date_end_time) % 60;

            str += ` (${autoControlItem.sun_date_start_type === 'rise' ? '일출' : '일몰'}기준 ${sun_date_start_time < 0 ? '- ' : (sun_date_start_time > 0 ? '+ ' : '')}${startHour ? startHour + '시간 ' : ''}${startMinute ? startMinute + '분 ' : ''}부터 ${autoControlItem.sun_date_end_type === 'rise' ? '일출' : '일몰'}기준 ${sun_date_end_time < 0 ? '- ' : (sun_date_end_time > 0 ? '+ ' : '')}${endHour ? endHour + '시간 ' : ''}${endMinute ? endMinute + '분 ' : ''}까지)`;
        } else {
            if(autoControlItem.start_hour !== null && autoControlItem.start_minute !== null) {
                str += highLightText(`${attachZero(autoControlItem.start_hour)}:${attachZero(autoControlItem.start_minute)}`);

                if(autoControlItem.end_hour !== null && autoControlItem.end_minute !== null) {
                    str += highLightText(` ~ ${attachZero(autoControlItem.end_hour)}:${attachZero(autoControlItem.end_minute)}`);
                }
            }
        }
        return (
            <span dangerouslySetInnerHTML={{__html: str}}/>
        )
    };

    returnConditionControl = (type, autoControlItem) => {
        let str = '';
        if (type === AUTO_CONTROL_TYPE_MIX || type === AUTO_CONTROL_TYPE_STEP) {
            if (autoControlItem.autoControlControls.length) {
                str += '기기 조건이 ';
                for (let i=0; i<autoControlItem.autoControlControls.length; i++) {
                    const autoControlControl = autoControlItem.autoControlControls[i];
                    if (i > 0) {
                        str += autoControlItem.control_operator === "and" ? highLightText(' 그리고 ') : highLightText(' 또는 ');
                    }
                    if (autoControlControl.control.type === 'motor') {
                        str += `${boldText(autoControlControl.control.control_name)}(이)가 ${highLightText(autoControlControl.value + '%')} ${SENSOR_TRANSLATE.condition[autoControlControl.condition]}`;
                    } else {
                        str += `${boldText(autoControlControl.control.control_name)}(이)가 ${highLightText(autoControlControl.value ? '켜짐' : '꺼짐')}`;
                    }
                }
            }
        } else if (type === AUTO_CONTROL_TYPE_CONTROL) {
            if (autoControlItem.autoControlControls.length) {
                str += '기기 조건이 ';
                let autoControlControl = autoControlItem.autoControlControls[0];
                if (autoControlControl.control.type === 'motor') {
                    str += `${boldText(autoControlControl.control.control_name)}(이)가 ${highLightText(autoControlControl.value + '%')} ${SENSOR_TRANSLATE.condition[autoControlControl.condition]}`;
                } else {
                    str += `${boldText(autoControlControl.control.control_name)}(이)가 ${highLightText(autoControlControl.value ? '켜짐' : '꺼짐')}`;
                }
            }
        }
        if(str.length) str += '일 때,<br/>';
        return (
            str ? <span dangerouslySetInnerHTML={{__html: str}}/> : null
        );
    };

    returnCondition = (type, autoControlItem) => {
        let str = '';
        if (type === AUTO_CONTROL_TYPE_MIX || type === AUTO_CONTROL_TYPE_STEP) {
            if (autoControlItem.autoControlSensors.length) {
                str += '센서 조건이 ';
                for(let i=0; i<autoControlItem.autoControlSensors.length; i++) {
                    let sensor = autoControlItem.autoControlSensors[i];
                    if(i > 0) {
                        str += autoControlItem.sensor_operator === "and" ? highLightText(' 그리고 ') : highLightText(' 또는 ');
                    }
                    if(SPECIEL_SENSOR_TYPE[sensor.sensor.type]) {
                        if(sensor.sensor.type === "windDirection"||sensor.sensor.type === "korinsWindDirection") {
                            str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(sensor.sensor.unit.split(':')[parseInt(sensor.value/45)])}`;
                        } else {
                            let units = [];
                            let unit = sensor.unit ? sensor.unit : sensor.sensor.unit;
                            if (unit) {
                                units = unit.split(':');
                            }
                            let condition = sensor.value === 1 ? units[0] : units[1];
                            str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(condition)}`;
                        }
                    } else {
                        str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(sensor.value)}${highLightText(sensor.sensor.unit)} ${SENSOR_TRANSLATE.condition[sensor.condition]}`;
                    }
                }
            }
        } else if (type === AUTO_CONTROL_TYPE_SENSOR) {
            if (autoControlItem.autoControlSensors.length) {
                str += '센서 조건이 ';
                let sensor = autoControlItem.autoControlSensors[0];
                if (SPECIEL_SENSOR_TYPE[sensor.sensor.type]) {
                    if (sensor.sensor.type === "windDirection"||sensor.sensor.type === "korinsWindDirection") {
                        str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(sensor.sensor.unit.split(':')[parseInt(sensor.value/45)])}`;
                    } else {
                        let units = [];
                        let unit = sensor.unit ? sensor.unit : sensor.sensor.unit;
                        if (unit) {
                            units = unit.split(':');
                        }
                        let condition = sensor.value === 1 ? units[0] : units[1];
                        str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(condition)}`;
                    }
                } else {
                    str += `${boldText(sensor.sensor.sensor_name)}(이)가 ${highLightText(sensor.value)}${highLightText(sensor.sensor.unit)} ${SENSOR_TRANSLATE.condition[sensor.condition]}`;
                }
            }
        }
        if(str.length) str += '일 때,<br/>';
        return (
            str ? <span dangerouslySetInnerHTML={{__html: str}}/> : null
        );
    };

    returnOption = (item) => {
        let str = '';
        if(item.autoControlSensors.length){
            for(let i = 0 ; i < item.autoControlSensors.length ; i ++){
                if(item.autoControlSensors[i].is_use_option){
                    str += `(사역값에 의해 최소 ${item.autoControlSensors[i].option_min_value}${item.autoControlSensors[i].sensor.unit} 최대 ${item.autoControlSensors[i].option_max_value}${item.autoControlSensors[i].sensor.unit}<br/>`;
                    if(item.autoControlSensors[i].autoControlSensorOptions){
                        for(let j = 0; j < item.autoControlSensors[i].autoControlSensorOptions.length ; j++){
                            let sensorOption = item.autoControlSensors[i].autoControlSensorOptions[j];
                            str += `${boldText(sensorOption.sensor.sensor_name)}, 최대 ${highLightText(sensorOption.max_value)}${highLightText(sensorOption.sensor.unit)}, 최소 ${highLightText(sensorOption.min_value)}${highLightText(sensorOption.sensor.unit)} 일 때 ${highLightText(sensorOption.percentage)}%`
                            str += j === item.autoControlSensors[i].autoControlSensorOptions.length - 1 ? ')<br/>' :'<br/>';
                        }
                    }
                }
            }
            return  str ? <span dangerouslySetInnerHTML={{__html: str}}/> : null
        }
    }

    returnControl = (item) => {
        let str = '';

        if (item.control) {
            str = item.control.control_name;
        }

        if(item.wind_direction_type !== 'none' && item.window_position !== 'none'){
            str = translate.WINDOW_POSITION[item.window_position] + ' ' + translate.WIND_DIRECTION[item.wind_direction_type];
        }

        return (
            <span dangerouslySetInnerHTML={{__html: `${boldText(str)}(을)를 `}}/>
        )
    };

    returnDelayRecount = (item, autoControlItem) => {
        let str = '';
        if((item.start_hour && item.start_minute) && (!item.end_hour && !item.end_minute)) {
            return;
        }
        if (item.period_type === 'immediately') {
            str += "(즉시 실행";
        } else {
            if(item.delay) {
                str += '(';
                str += highLightText(returnTimeText(item.delay)) + ' 지연';
            } else {
                if(autoControlItem.type !== "step") {
                    str += "(1분 타이머";
                }
            }
        }

        if(autoControlItem.type === "step") {
            if(item.recount !== null) {
                if(str.length) {
                    str += ' - ';
                } else {
                    str += '(';
                }
                str += highLightText(item.recount+1 + '회 반복');
            }
        } else {
            if(item.active_count !== null && item.active_count > 0) {
                if(str.length) {
                    str += ' - ';
                } else {
                    str += '(';
                }
                str += `${highLightText(item.active_count+ '회 실행')}`;
            } else {
                if(str.length) {
                    str += ' - ';
                } else {
                    str += '(';
                }
                str += `${highLightText('계속 실행')}`;
            }
        }
        if(str.length) str += ')';
        return (<span dangerouslySetInnerHTML={{__html: str}}/>);
    };

    returnMinMaxRange = item => {
        if (item.is_min_max_range && item.autoControlItemMinMaxes && item.autoControlItemMinMaxes.length) {
            let str = '';
            item.autoControlItemMinMaxes.forEach(autoControlItemMinMax => {
                if (autoControlItemMinMax.state === 'on' && autoControlItemMinMax.controlMinMaxRange.state === 'on') {
                    str += `<br>${highLightText(autoControlItemMinMax.controlMinMaxRange.name)}일 때, ${item.state === 'open' ? '최대' : '최소'} ${highLightText(autoControlItemMinMax.percentage)}% 까지 동작합니다.`;
                }
            });
            if (str) {
                return (<span dangerouslySetInnerHTML={{__html: str}}/>);
            } else {
                return null;
            }
        } else if (item.is_min_max_range && item.autoControlStepMinMaxes && item.autoControlStepMinMaxes.length) {
            let str = '';
            item.autoControlStepMinMaxes.forEach(autoControlStepMinMax => {
                if (autoControlStepMinMax.state === 'on' && autoControlStepMinMax.controlMinMaxRange.state === 'on') {
                    str += `<br>${highLightText(autoControlStepMinMax.controlMinMaxRange.name)}일 때, ${item.state === 'open' ? '최대' : '최소'} ${highLightText(autoControlStepMinMax.percentage)}% 까지 동작합니다.`;
                }
            });
            if (str) {
                return (<span dangerouslySetInnerHTML={{__html: str}}/>);
            } else {
                return null;
            }
        } else {
            return null;
        }
    };

    returnControlCondition = (item) => {
        if(item.percentage) {
            if(parseInt(item.percentage) !== 200) {
                return <span dangerouslySetInnerHTML={{__html: `${highLightText(item.percentage + '%')}로 `}}/>;
            }
        } else if (item.time) {
            return <span dangerouslySetInnerHTML={{__html: `${highLightText(returnTimeText(item.time))} 동안 `}}/>;
        } else if (item.p_band_integral !== null) {
            let str = `P-BAND 설정 ${highLightText(item.pBand.p_band_name)} 희망온도 ${highLightText(item.p_band_temperature + '℃')}까지 적분 값 ${highLightText(item.p_band_integral + '%')}로 `;
            return <span dangerouslySetInnerHTML={{__html: str}}/>;
        } else if (item.p_band_temperature !== null) {
            return <span dangerouslySetInnerHTML={{__html: `P-BAND 설정 ${highLightText(item.pBand.p_band_name)} 희망온도 ${highLightText(item.p_band_temperature + '℃')}까지 `}}/>;
        }
    };


    returnActions = (item) => {
        if(!item.control || item.control.type === TYPE_MOTOR) {
            if(item.percentage) {
                if(parseInt(item.percentage) === 200) {
                    if(item.state === "open"){
                        return "전체 열기";
                    } else if (item.state === "close") {
                        return "전체 닫기";
                    }
                } else {
                    if(item.state === "open"){
                        return "부분 열기";
                    } else if (item.state === "close") {
                        return "부분 닫기";
                    }
                }
            } else if (item.time !== null) {
                if(item.state === "open"){
                    return "부분 열기";
                } else if (item.state === "close") {
                    return "부분 닫기";
                }
            } else if (item.p_band_temperature !== null) {
                return "부분 열기";
            }
        } else if (item.control.type === TYPE_POWER) {
            if(item.time !== null) {
                if(item.state === "on"){
                    return "켜짐(예약 꺼짐)";
                } else if (item.state === "off") {
                    return "꺼짐(예약 켜짐)";
                }
            }
            if(item.state === "on"){
                return "켜짐";
            } else if (item.state === "off") {
                return "꺼짐";
            }
        }
    };

    returnStepWay = (stepItem) => {
        if(stepItem.percentage) {
            if(parseInt(stepItem.percentage) !== 200) {
                return <span dangerouslySetInnerHTML={{__html: `${highLightText(stepItem.percentage + '%')}로 `}}/>;
            }
        } else if (stepItem.time) {
            return <span dangerouslySetInnerHTML={{__html: `${highLightText(returnTimeText(stepItem.time))} 동안 `}}/>;
        } else if (stepItem.p_band_integral !== null) {
            let str = `P-BAND 설정 ${highLightText(stepItem.pBand.p_band_name)} 희망온도 ${highLightText(stepItem.p_band_temperature + '℃')}까지 적분 값 ${highLightText(stepItem.p_band_integral + '%')}로 `;
            return <span dangerouslySetInnerHTML={{__html: str}}/>;
        } else if (stepItem.p_band_temperature !== null) {
            return <span dangerouslySetInnerHTML={{__html: `P-BAND 설정 ${highLightText(stepItem.pBand.p_band_name)} 희망온도 ${highLightText(stepItem.p_band_temperature + '℃')}까지 `}}/>;
        }
    };

    generateStepBottomString = (item) => {
        let str = '';
        if(item.step_delay) {
            str += '(단계간 지연시간 ';
            str += `${highLightText(returnTimeText(item.step_delay))}`
        }
        if(item.autoControlItems[0].active_count !== null && item.autoControlItems[0].active_count > 0) {
            if(str.length) {
                str += ', ';
            } else {
                str += '(';
            }
            str += `총 세트실행 ${highLightText(item.autoControlItems[0].active_count + '회')}`;
        } else {
            if(str.length) {
                str += ', ';
            } else {
                str += '(';
            }
            str += `총 세트실행 ${highLightText('계속 실행')}`;
        }

        if(str.length) str += ')';
        return <span dangerouslySetInnerHTML={{__html: str}}/>
    };

    clickCheckBox = async () => {
        const {isActiveDate} = this.state;
        await this.setState({
            isActiveDate: !isActiveDate
        });
        this.findAutoControls(undefined);
    };

    render() {
        return (
            <article id="lcModalAutoControlWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalAutoControl" className="modal-wrap">
                                <section className="modal-header">
                                    {
                                        this.state.step === 1 ?
                                            (
                                                <h3 className="lc-modal-title">자동제어 대상 기기 선택 {this.state.type ? `(${CONTROL_NAME_TRANSLATE[this.state.type]})` : ''}</h3>
                                            ) :
                                            (
                                                <h3 className="lc-modal-title">자동제어 설정</h3>
                                            )
                                    }
                                    <button type="button" className="lc-modal-close" onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    {
                                        this.state.step === 1 ?
                                        (
                                            <div id="controlContentWrap">
                                                {
                                                    <ul id="controlList">
                                                        {/*<li className="control-list-item">*/}
                                                        {/*    <button onClick={e => this.selectWindDirection(e, 'forward')} className="card-header-button"><span>풍상창</span><span className="number-badge">{this.state.forwardCount}</span></button>*/}
                                                        {/*</li>*/}
                                                        {/*<li className="control-list-item">*/}
                                                        {/*    <button onClick={e => this.selectWindDirection(e, 'backward')} className="card-header-button"><span>풍하창</span><span className="number-badge">{this.state.backwardCount}</span></button>*/}
                                                        {/*</li>*/}

                                                        {
                                                            this.state.controlGroups && this.state.type === TYPE_MOTOR && this.state.controlGroups.map((item, index) => (
                                                                <li className="control-list-item" key={index}>
                                                                    <button onClick={e => this.selectControl(e, item, 'controlGroup')} className="card-header-button"><span>{translate.WINDOW_POSITION[item.window_position] + ' ' + translate.WIND_DIRECTION[item.wind_direction_type]}</span><span className="number-badge">{item.autoControlCount}</span></button>
                                                                </li>
                                                            ))
                                                        }

                                                        {
                                                            this.state.controls && this.state.controls.map((item, index) => (
                                                                <li className="control-list-item" key={index}>
                                                                    <button onClick={e => this.selectControl(e, item)} className="card-header-button"><span>{item.control_name}</span><span className="number-badge">{item.autoControlItemCount + item.autoControlStepCount}</span></button>
                                                                </li>
                                                            ))
                                                        }
                                                    </ul>
                                                }
                                            </div>
                                        ) :
                                            (
                                                (
                                                    <Fragment>
                                                        <div className="control-section-wrap">
                                                            <div className="button-wrap">
                                                                <button className="theme-text-button" onClick={this.openAutoControlFileUpload}>+ 자동제어 파일 일괄등록</button>
                                                                <button className="theme-text-button" onClick={this.openAutoControlCreate}>+ 자동제어 추가</button>
                                                            </div>

                                                            <div className="select-module-wrap">
                                                                <Select value={this.state.selectedControlOption}
                                                                        placeholder="전체보기"
                                                                        onChange={this.handleSelectControl}
                                                                        isSearchable={ false }
                                                                        options={this.state.selectControlOption}
                                                                        styles={colourStyles}/>
                                                            </div>
                                                        </div>
                                                        <div className="lc-theme-checkbox-wrap">
                                                            <input id="lcIsActiveDate"
                                                                   value="time"
                                                                   checked={this.state.isActiveDate}
                                                                   onChange={() => {
                                                                   }} type="checkbox"
                                                                   name="mix-control-condition"/>
                                                            <label
                                                                htmlFor="lcIsActiveDate"
                                                                onClick={this.clickCheckBox}
                                                                className="box-shape-label"/>
                                                            <label
                                                                htmlFor="lcIsActiveDate"
                                                                onClick={this.clickCheckBox}
                                                                className="text-label">현재 기간만 보기</label>
                                                        </div>
                                                        <div className="list-section-wrap">
                                                            {
                                                                this.state.autoControls && this.state.autoControls.length ?
                                                                    (
                                                                        <Fragment>
                                                                            <div className="item-open-close-button-wrap">
                                                                                <button className="card-header-button" onClick={e => this.toggleDetails(e, true)}>조건펼침</button>
                                                                                <button className="card-header-button" onClick={e => this.toggleDetails(e, false)}>조건접힘</button>
                                                                                <button className="card-header-button" onClick={e => this.changeControlOrder(e)}>{this.state.cardMode === "ordinary" ? "실행 순서 변경" : "순서 변경 닫기"}</button>
                                                                            </div>
                                                                            <ul id="autoControlList">
                                                                                {
                                                                                    this.state.autoControls.map((item, index) => {
                                                                                        return (
                                                                                            <li className="auto-control-list-item" key={index} style={{zIndex: this.state.autoControls.length - index}}>
                                                                                                <article className="lc-content-card">
                                                                                                    <div className="lc-control-name-wrap">
                                                                                                        <p className="lc-control-name-wrap">
                                                                                                            <span className="lc-control-name-index">{(index+1)}</span>
                                                                                                            <span className="lc-control-name">{item.auto_control_name}</span>
                                                                                                        </p>
                                                                                                    </div>
                                                                                                    <div className="lc-control-wrap">
                                                                                                        <span className="auto-control-child-count">{item.autoControlItems.length +item.autoControlSteps.length}</span>
                                                                                                        {
                                                                                                            this.state.cardMode === MODE_ORDINARY ?
                                                                                                                (
                                                                                                                    <div className="ordinary-menu-wrap">
                                                                                                                        <button className={"open-menu-btn" + (item.isAutoControlTabOpen ? ' lc-open' : '')} onClick={e => this.toggleAutoControlStepWrap(e, index)}/>
                                                                                                                        <div className="toggle-radio-wrap two-state">
                                                                                                                            <input type="radio" name={`auto-control-activate-toggle-option-${index}`}
                                                                                                                                   onChange={e => {}}
                                                                                                                                   checked={item.state === "on"}
                                                                                                                            />

                                                                                                                            <input type="radio" name={`auto-control-activate-toggle-option-${index}`}
                                                                                                                                   onChange={e => {}}
                                                                                                                                   checked={item.state === "off"}
                                                                                                                            />

                                                                                                                            <label onClick={e => this.clickAutoControlActivateState(e, "on", item, index)}>ON</label>
                                                                                                                            <label onClick={e => this.clickAutoControlActivateState(e, "off", item, index)}>OFF</label>
                                                                                                                            <i className="toggle-slider-icon"/>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                ) :
                                                                                                                (
                                                                                                                    <div className="ordering-menu-wrap">
                                                                                                                        <i className="preload-img"/>
                                                                                                                        {
                                                                                                                            index !== this.state.autoControls.length-1 &&
                                                                                                                            (
                                                                                                                                <button className="order-btn order-down" onClick={e => this.changeOrder(e, index, 'down')}/>
                                                                                                                            )
                                                                                                                        }
                                                                                                                        {
                                                                                                                            index !== 0 &&
                                                                                                                            (
                                                                                                                                <button className="order-btn order-up" onClick={e => this.changeOrder(e, index, 'up')}/>
                                                                                                                            )
                                                                                                                        }
                                                                                                                    </div>
                                                                                                                )
                                                                                                        }

                                                                                                        <div className="three-dot-menu" tabIndex="0" ref={c => this.moreMenuRefs.set(index, c)} style={{zIndex: (5+ this.state.autoControls.length - index)}}>
                                                                                                            <div className="icon-wrap">
                                                                                                                <i/><i/><i/>
                                                                                                            </div>
                                                                                                            <div className="three-dot-view-more-menu">
                                                                                                                <ul>
                                                                                                                    {/*<li onClick={e => this.openAutoControlStepModal(e, index, item)}><span>단계 제어 설정</span></li>*/}
                                                                                                                    {/*<li onClick={e => this.openAutoControlDelayRecountModal(e, index, item)}><span>지연 반복 설정</span></li>*/}
                                                                                                                    <li onClick={e => this.updateAutoControl(e, index, item)}><span>수정</span></li>
                                                                                                                    <li onClick={e => this.removeAutoControl(e, index, item)}><span>삭제</span></li>
                                                                                                                </ul>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </article>
                                                                                                {
                                                                                                    item.isAutoControlTabOpen &&
                                                                                                    (
                                                                                                        <Fragment>
                                                                                                            {
                                                                                                                item.type === 'table' && Object.keys(item.autoControlTables).length ?
                                                                                                                    (
                                                                                                                        <div className="auto-control-child-wrap">
                                                                                                                            <div className="auto-control-table">
                                                                                                                                {
                                                                                                                                    Object.keys(item.autoControlTables).map((key, index) => {
                                                                                                                                        return (
                                                                                                                                            <Fragment key={index}>
                                                                                                                                                {index === 0 &&
                                                                                                                                                    <p className="auto-control-table-title">
                                                                                                                                                        {item.autoControlTables[key][0] && item.autoControlTables[key][0].p_band_id && item.autoControlTables[key][0].p_band_temperature ? '환기설정' : '난방설정'}
                                                                                                                                                    </p>
                                                                                                                                                }

                                                                                                                                                {index % 6 === 0 &&
                                                                                                                                                <ul className={'auto-control-table-title'}>
                                                                                                                                                    <li><span> </span></li>
                                                                                                                                                    <li><span>사용여부</span></li>
                                                                                                                                                    <li><span>작동기준</span></li>
                                                                                                                                                    <li><span>시작시간</span></li>
                                                                                                                                                    <li><span>희망온도 (℃)</span></li>
                                                                                                                                                    <li><span>온도 상승시간(분)</span></li>
                                                                                                                                                    <li><span>온도 하강시간(분)</span></li>
                                                                                                                                                </ul>
                                                                                                                                                }
                                                                                                                                                <ul className={`auto-control-table-content` + (index !== 0 && index % 5 === 0 ? ` table-border-right` : ``)} >
                                                                                                                                                    <li className="table-content-title"><span>{index + 1}주기</span></li>
                                                                                                                                                    <li><span>{item.autoControlTables[key][0] ? item.autoControlTables[key][0].is_using_period.value ? '사용' : '미사용' : ''}</span></li>


                                                                                                                                                    <li><span>{item.autoControlTables[key][0] ? item.autoControlTables[key][0].sun_date_start_type ? TRANSLATE.TIME_TYPE[item.autoControlTables[key][0].sun_date_start_type] : '고정' : ''}</span></li>
                                                                                                                                                    <li><span>{item.autoControlTables[key][0] ? item.autoControlTables[key][0].is_sun_date ? sunDateTimeToText(item.autoControlTables[key][0].sun_date_start_time) : attachZero(item.autoControlTables[key][0].start_hour) +':' +attachZero(item.autoControlTables[key][0].start_minute) : ''}</span></li>
                                                                                                                                                    <li><span>{item.autoControlTables[key][0] ? item.autoControlTables[key][0].p_band_id && item.autoControlTables[key][0].p_band_temperature ? item.autoControlTables[key][0].p_band_temperature+'℃' : item.autoControlTables[key][0].expect_temperature+'℃': ''}</span></li>
                                                                                                                                                    <li><span>{item.autoControlTables[key][0] ? item.autoControlTables[key][0].rise_time ? item.autoControlTables[key][0].rise_time+'분' : '-': ''}</span></li>
                                                                                                                                                    <li><span>{item.autoControlTables[key][0] ? item.autoControlTables[key][0].drop_time ? item.autoControlTables[key][0].drop_time+'분' : '-': ''}</span></li>
                                                                                                                                                </ul>
                                                                                                                                            </Fragment>
                                                                                                                                        )
                                                                                                                                    })
                                                                                                                                }
                                                                                                                                <p className="table-description-text">* 주기별 자동 조절 값은 수정화면에서 확인하실 수 있습니다.</p>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    ) : null

                                                                                                            }
                                                                                                            {
                                                                                                                item.type !== 'table' ? (
                                                                                                                    item.autoControlItems.length ?
                                                                                                                        (
                                                                                                                            <div className="auto-control-child-wrap">
                                                                                                                                <ul className="control-child-list">
                                                                                                                                    {
                                                                                                                                        item.autoControlItems.map((autoControlItem, index) => {
                                                                                                                                            return (
                                                                                                                                                <li className="control-child-list-item" key={index}>
                                                                                                                                                    <article className="auto-control-child-content-wrap">
                                                                                                                                                        {
                                                                                                                                                            item.autoControlItems.length > 1 ? (<span className="control-child-index">{index+1}</span>) : null
                                                                                                                                                        }
                                                                                                                                                        <div className="partial-control-wrap">
                                                                                                                                                            <p className="control-child-text">
                                                                                                                                                                {this.returnDateType(item)}
                                                                                                                                                                <span> {this.returnAutoControlDays(item)} </span>
                                                                                                                                                                {this.returnTime(item.type, autoControlItem)}
                                                                                                                                                                <br/>
                                                                                                                                                                {this.returnCondition(item.type, autoControlItem)}
                                                                                                                                                                {this.returnConditionControl(item.type, autoControlItem)}
                                                                                                                                                                {
                                                                                                                                                                    item.autoControlItems.length === 1 && item.autoControlSteps.length > 0 ?
                                                                                                                                                                        (
                                                                                                                                                                            <Fragment>
                                                                                                                                                                                <span>[1단계] </span>
                                                                                                                                                                            </Fragment>
                                                                                                                                                                        ) : null
                                                                                                                                                                }
                                                                                                                                                                {this.returnOption(autoControlItem)}
                                                                                                                                                                {this.returnControl(autoControlItem)}
                                                                                                                                                                {this.returnControlCondition(autoControlItem)}
                                                                                                                                                                <span className="lc-theme-color">{this.returnActions(autoControlItem)}</span>(을)를 해주세요.
                                                                                                                                                                {this.returnDelayRecount(autoControlItem, item)}
                                                                                                                                                                {this.returnMinMaxRange(autoControlItem)}
                                                                                                                                                            </p>
                                                                                                                                                            {
                                                                                                                                                                item.autoControlSteps.map((step, index) => {
                                                                                                                                                                    return (
                                                                                                                                                                        <Fragment key={index}>
                                                                                                                                                                            <p className="control-child-text">
                                                                                                                                                                                [{index+2}단계] {this.returnControl(step)}{this.returnStepWay(step)} <span className="lc-theme-color">{this.returnActions(step)}</span>(을)를 해주세요. {this.returnDelayRecount(step, item)}
                                                                                                                                                                                {this.returnMinMaxRange(step)}
                                                                                                                                                                            </p>
                                                                                                                                                                        </Fragment>
                                                                                                                                                                    )
                                                                                                                                                                })
                                                                                                                                                            }
                                                                                                                                                            {
                                                                                                                                                                item.type === "step" ?
                                                                                                                                                                    (
                                                                                                                                                                        <Fragment>{this.generateStepBottomString(item)}</Fragment>
                                                                                                                                                                    ) : null
                                                                                                                                                            }
                                                                                                                                                        </div>
                                                                                                                                                    </article>
                                                                                                                                                </li>
                                                                                                                                            )
                                                                                                                                        })
                                                                                                                                    }
                                                                                                                                </ul>
                                                                                                                            </div>
                                                                                                                        ) :
                                                                                                                        (
                                                                                                                            <div className="auto-control-child-wrap">
                                                                                                                                <p className="no-auto-control-child-p">단계제어가 추가되어 있지 않습니다.</p>
                                                                                                                            </div>
                                                                                                                        )
                                                                                                                ):null
                                                                                                            }
                                                                                                        </Fragment>
                                                                                                    )
                                                                                                }
                                                                                            </li>
                                                                                        )
                                                                                    })
                                                                                }
                                                                            </ul>
                                                                        </Fragment>
                                                                    ) :
                                                                    (
                                                                        <article className="lc-content-card">
                                                                            <p className="no-auto-control-text">해당 기기에 설정된 자동제어가 없습니다.</p>
                                                                        </article>
                                                                    )
                                                            }
                                                        </div>

                                                        <p className="modal-description-text">*기기별 동시 명령이 요청되면 상단에 있는<br className="mobile-display-br"/>설정값이 우선 작동합니다.</p>
                                                    </Fragment>
                                                )
                                            )
                                    }


                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAutoControlFileUpload(
    ({actions}) => ({
        openAutoControlFileUploadModal: actions.open
    })
)(useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAutoControlForm(
    ({actions}) => ({
        openAutoControlForm: actions.open
    })
)(useAutoControlDelayRecount(
    ({actions}) => ({
        openAutoControlDelayRecountModal: actions.open
    })
)(useAutoControlStep(
    ({actions}) => ({
        openAutoControlStepModal: actions.open
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useAutoControl(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(useSunDate(
    ({state}) => ({
        rise_hour: state.rise_hour,
        rise_minute: state.rise_minute,
        set_hour: state.set_hour,
        set_minute: state.set_minute
    })
)(ModalAutoControl)))))))));
