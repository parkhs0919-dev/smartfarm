import React, {Component} from 'react';
import {useHouse} from "../../contexts/house";
import {useSensorSetting} from "../../contexts/modals/sensorSetting";
import {useAlert} from "../../contexts/alert";
import sensorManager from '../../managers/sensor';
import deepcopy from "../../../../utils/deepcopy";
import {noSession} from "../../utils/session";

const OUTER_SENSOR_POSITION = "out";
const INNER_SENSOR_POSITION = "in";

class ModalSensorSetting extends Component {
    constructor(props) {
        super(props);

        this.state = {
            innerSensors: [],
            outerSensors: []
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        this.findAllInnerSensors();
        this.findAllOuterSensors();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findAllInnerSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            position: INNER_SENSOR_POSITION,
        };

        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                let rows = data.data.rows;
                rows.forEach((item) => {
                    item.customRevision = item.revision;
                });
                _this.setState({
                    innerSensors: data.data.rows
                });
            } else if (status === 404) {
                _this.setState({
                    innerSensors: [],
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findAllOuterSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            position: OUTER_SENSOR_POSITION,
        };

        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                let rows = data.data.rows;
                rows.forEach((item) => {
                    item.customRevision = item.revision;
                });
                _this.setState({
                    outerSensors: data.data.rows
                });
            } else if (status === 404) {
                _this.setState({
                    outerSensors: [],
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    handleInput = (e, index, type, key) => {
        let sensors = type === OUTER_SENSOR_POSITION ? deepcopy(this.state.outerSensors) : deepcopy(this.state.innerSensors);
        sensors[index][key] = e.target.validity.valid ? e.target.value : sensors[index][key];
        let temp = {};
        if(type === OUTER_SENSOR_POSITION) {
            temp.outerSensors = sensors;
        } else {
            temp.innerSensors = sensors;
        }
        this.setState(temp);
    };

    updateSensors = () => {
        const _this = this;
        let sensors = [];
        let innerSensors = deepcopy(this.state.innerSensors);
        let outerSensors = deepcopy(this.state.outerSensors);

        innerSensors.forEach((item) => {
            let obj = {
                id: item.id,
                sensor_name: item.sensor_name,
                revision: item.customRevision
            };
            sensors.push(obj);
        });
        outerSensors.forEach((item) => {
            let obj = {
                id: item.id,
                sensor_name: item.sensor_name,
                revision: item.customRevision
            };
            sensors.push(obj);
        });
        sensorManager.updateAll({sensors}, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.props.showDialog({
                    alertText: "센서 보정값이 설정되었습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {
        return (
            <article id="lcModalSensorSettingWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalSensorSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">센서설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    {
                                        this.state.outerSensors && this.state.outerSensors.length ?
                                            (
                                                <section className="sensor-content-wrap">
                                                    <h4 className="sensor-title">외부센서</h4>

                                                    <table className="sensor-table">
                                                        <thead>
                                                        <tr>
                                                            <th>센서명</th>
                                                            <th>현재값</th>
                                                            <th>보정값</th>
                                                            <th>보정치</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {
                                                            this.state.outerSensors.map((item, index) => {
                                                                return (
                                                                    <tr className="sensor-list-item" key={index}>
                                                                        <td><input type="text" className="lc-farm-input"
                                                                                   value={item.sensor_name}
                                                                                   onChange={e => this.handleInput(e, index, OUTER_SENSOR_POSITION, "sensor_name")}/></td>
                                                                        <td><span>{(item.value - item.revision).toFixed(2)}</span></td>
                                                                        <td><span>{(parseFloat(item.value - item.revision) + parseFloat(item.customRevision ? item.customRevision : 0)).toFixed(2)}</span></td>
                                                                        <td><input type="text" pattern="^([-+,0-9.]+)" className="lc-farm-input"
                                                                                   onChange={e => this.handleInput(e, index, OUTER_SENSOR_POSITION, "customRevision")}
                                                                                   value={item.customRevision}/></td>
                                                                    </tr>
                                                                )
                                                            })
                                                        }
                                                        </tbody>
                                                    </table>
                                                </section>
                                            ) : null
                                    }
                                    {
                                        this.state.innerSensors && this.state.innerSensors.length ?
                                            (
                                                <section className="sensor-content-wrap">
                                                    <h4 className="sensor-title">내부센서</h4>

                                                    <table className="sensor-table">
                                                        <thead>
                                                        <tr>
                                                            <th>센서명</th>
                                                            <th>현재값</th>
                                                            <th>보정값</th>
                                                            <th>보정치</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {
                                                            this.state.innerSensors.map((item, index) => {
                                                                return (
                                                                    <tr className="sensor-list-item" key={index}>
                                                                        <td><input type="text" className="lc-farm-input"
                                                                                   value={item.sensor_name}
                                                                                   onChange={e => this.handleInput(e, index, INNER_SENSOR_POSITION, "sensor_name")}/></td>
                                                                        <td><span>{(item.value - item.revision).toFixed(2)}</span></td>
                                                                        <td><span>{(parseFloat(item.value - item.revision) + parseFloat(item.customRevision ? item.customRevision : 0)).toFixed(2)}</span></td>
                                                                        <td><input type="text" pattern="^([-+,0-9.]+)" className="lc-farm-input"
                                                                                   onChange={e => this.handleInput(e, index, INNER_SENSOR_POSITION, "customRevision")}
                                                                                   value={item.customRevision}/></td>
                                                                    </tr>
                                                                )
                                                            })
                                                        }
                                                        </tbody>
                                                    </table>
                                                </section>
                                            ) : null
                                    }

                                    <button className="theme-button" onClick={this.updateSensors}>적용</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useSensorSetting(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSensorSetting)));