import React, {Component} from 'react';
import {useHouse} from "../../contexts/house";
import {useAlert} from "../../contexts/alert";
import {noSession} from "../../utils/session";

import deepcopy from "../../../../utils/deepcopy";
import Select from "react-select";
import {colourStyles} from "../../utils/select";
import {useWindDirectionSetting} from "../../contexts/modals/windDirectionSetting";
import controlManager from "../../managers/control";
import controlGroupManager from"../../managers/controlGroup";
import translate from "../../constants/translate";

const Directions = [
    {label: "우측", value: "right"},
    {label: "좌측", value: "left"},
];

const windowPositions = [
    {label:'1중', value: '1'},
    {label:'2중', value: '2'},
    {label:'3중', value: '3'},
]

const DEFAULT_VALUE = {
    id :null,
    motorList : [],
    selectedControlOption : null,
    selectControlOption : [],
    house_id : null,
    type : 'windDirection',
    control_group_name : '',
    direction : {label: "우측", value: "right"},
    window_position: null,
}

class ModalWindDirectionSetting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id : null,
            motorList : [],
            selectedControlOption : null,
            selectControlOption : [],
            house_id : null,
            type : 'windDirection',
            control_group_name : '',
            direction : {label: "우측", value: "right"},
            window_position: null,
        };

    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = data => {


        const _this = this;

        _this.findControls(this.state.direction.value);

        if(data){
            _this.setData(data);
        }

        document.addEventListener('keyup', this.closeOnEscape);
    };


    close = (e) => {
        if(e) e.preventDefault();
        this.setState(DEFAULT_VALUE);
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    handleInput = (e) => {
        let value = e.target.value;
        this.setState({
            control_group_name: value
        });
    }

    setData = data => {
        if(data.id) this.setState({id : data.id});
        if(data.house_id) this.setState({house_id : data.house_id});
        if(data.type) this.setState({type : data.type});
        if(data.control_group_name) this.setState({control_group_name : data.control_group_name});
        if(data.window_position){
            this.setState({
                window_position : {
                    label : translate.WINDOW_POSITION[data.window_position],
                    value : data.window_position
                }
            })
        }
        if(data.direction){
            this.setState({
                direction : {
                    label : translate.MOTOR_DIRECTION[data.direction],
                    value : data.direction,
                }
            });
        }
        if(data.controlGroupItems && data.controlGroupItems.length > 0){

            let motorList = [];

            data.controlGroupItems.forEach((item, index) => {
                controlManager.findById(item.control_id, (status, data) => {
                    if(status === 200){
                        motorList.push({
                            selectedMotorItem: {
                                value : data.data.id,
                                label : data.data.control_name,
                                origin : data.data
                            }
                        })
                    }
                });
            });

            this.setState({
                motorList : motorList
            });

        }
    }

    selectGroupDirectionHandle = (e) => {

        if(!this.state.motorList.length){
            this.setState({
                direction : {
                    label : e.label,
                    value : e.value
                },
            }, () => this.findControls(e.value));
        }else{
            return this.props.showDialog({
                alertText: "모터장치가 등록된 상태에서는 그룹 방향을 변경할 수 없습니다."
            });
        }
    };


    selectWindowDirectionHandle = e => {
        this.setState({
            window_position : {
                label : e.label,
                value : e.value,
            }
        })
    }

    addMotors = (e) => {
        const _this = this;

        let motorList = deepcopy(this.state.motorList);
        let obj = {
            selectedMotorItem : null,
        };

        motorList.push(obj);

        this.setState({motorList});
    }

    findControls = (val) => {

        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            direction : val,
            types: 'motor,power'
        };

        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    selectControlOption: _this.generateControlOption(data.data.rows)
                });
            } else if (status === 404) {
                _this.setState({
                    selectControlOption: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));

    };

    generateControlOption = (controls) => {
        let selectControlOption = [];
        controls.forEach(item => {
            let obj = {
                value: item.id,
                label: item.control_name,
                origin: item
            };
            if(item.id === this.state.selectedControlId) {
                this.setState({
                    selectedControlOption: obj,
                    controlOptionFromOutside: obj
                });
            }
            selectControlOption.push(obj);
        });

        return selectControlOption;
    };

    handleSelectMotors = (option, index) => {
        let motorList = deepcopy(this.state.motorList);
        motorList[index].selectedMotorItem = option;

        this.setState({motorList});

    };

    removeSelectedMotors = (index) => {
        let motorList = deepcopy(this.state.motorList);
        motorList.splice(index, 1);

        this.setState({motorList});
    }

    submit = e => {
        const _this = this;

        let data = {
            controlGroupItems : [],
        };

        data.house_id = this.props.getHouseId();
        data.type = 'windDirection';
        data.direction = _this.state.direction.value;
        data.window_position = _this.state.window_position.value;

        if(!_this.state.window_position){
            return this.props.showDialog({
                alertText: "창 위치를 선택해주세요."
            });
        }

        if(_this.state.motorList.length){
            _this.state.motorList.forEach((item, index) => {
                data.controlGroupItems.push({
                    control_id : item.selectedMotorItem.value
                })
            });
        }else{
            return this.props.showDialog({
                alertText: "모터장치를 등록해주세요."
            });
        }



        if(data){
            if(_this.state.id){
                data.id = _this.state.id;
                controlGroupManager.update(data, (status, data) => {
                    if(status === 200){
                        _this.props.showDialog({
                            alertText: "풍상/풍하제어 그룹이 \n수정되었습니다.",
                            cancelCallback: () => {
                                _this.close();
                            }
                        })
                    }else{
                        _this.props.alertError(status, data);
                    }
                })
            }else{
                controlGroupManager.create(data, (status, data) => {
                    if(status === 200){
                        _this.props.showDialog({
                            alertText: "풍상/풍하제어 그룹이 \n등록되었습니다.",
                            cancelCallback: () => {
                                _this.close();
                            }
                        })
                    }else{
                        _this.props.alertError(status, data);
                    }
                })
            }


        }
    }

    render() {
        return (
            <article id="lcModalWindDirectionSettingWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalWindDirectionSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">풍상/풍하 그룹 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div className="lc-group-setting">



                                        <div className="lc-group-setting-menu">
                                            <p className="lc-group-setting-name">창 위치</p>
                                            <Select
                                                value={this.state.window_position}
                                                placeholder="창 위치 선택"
                                                onChange={e => this.selectWindowDirectionHandle(e)}
                                                isSearhable={false}
                                                options={windowPositions}
                                                styles={colourStyles}
                                            />
                                        </div>

                                        <div className="lc-group-setting-menu">
                                            <p className="lc-group-setting-name">그룹 방향 설정</p>
                                            <Select
                                                value={this.state.direction}
                                                onChange={e => this.selectGroupDirectionHandle(e)}
                                                isSearchable={ false }
                                                options={Directions}
                                                styles={colourStyles}/>
                                        </div>
                                    </div>
                                    <div className="lc-setting-motor">
                                        <div className='setting-motor-name-wrap'>
                                            <p>모터 장치 선택</p>
                                            <button type="button" onClick={e => this.addMotors(e)}>장치 추가</button>
                                        </div>
                                        <div className="setting-motor-list-wrap">
                                            {
                                                this.state.motorList.length ? this.state.motorList.map((item, index) => (
                                                        <div className="setting-motor-item" key={index}>
                                                            <Select
                                                                className="setting-motor-select"
                                                                value={item.selectedMotorItem}
                                                                placeholder="기기 선택"
                                                                onChange={val => this.handleSelectMotors(val, index)}
                                                                isSearchable={false}
                                                                options={this.state.selectControlOption}
                                                                styles={colourStyles}/>
                                                            <button className="remove-motor-button" onClick={index => this.removeSelectedMotors(index)}>삭제</button>
                                                        </div>
                                                    )) :
                                                    <p className='not_exists'>선택한 장치가 없습니다. [장치 추가]를 눌러 선택해주세요.</p>
                                            }
                                        </div>
                                        <div className="setting-motor-button-wrap">
                                            <button className="theme-button lc-cancel" onClick={this.close}>취소</button>
                                            <button className="theme-button" onClick={e => this.submit(e)}>{this.state.id ? '수정' : '등록'}</button>
                                        </div>
                                    </div>

                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useWindDirectionSetting(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalWindDirectionSetting)));
