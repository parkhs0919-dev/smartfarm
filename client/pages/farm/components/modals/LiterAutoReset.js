import React, { Component } from 'react';
import { useLiterAutoReset } from "../../contexts/modals/LiterAutoReset";
import { useAlert } from "../../contexts/alert";
import literManager from '../../managers/liter';
import deepcopy from "../../../../utils/deepcopy";
import CONSTANT from "../../constants/constant";
import { noSession } from "../../utils/session";
import Select from 'react-select';
import { colourStyles } from "../../utils/select";
import { isNumber } from 'util';

const time = CONSTANT.Liter.Time



class ModalLiterAutoReset extends Component {
    constructor(props) {
        super(props);

        this.state = {
            check: false,
            control: [],
            Liter:[],
            house_id:'',
            flow_meter:{},
            flow_meter_round:[],            
        }
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {        
        this.setState({Liter:data.item,house_id:data.house_id},(e)=>this.getLiter(e))
        document.addEventListener('keyup', this.closeOnEscape);

    };
    getLiter =  (e) =>{
        let body={
            house_id:this.state.house_id,
            id:this.state.Liter.id
        }        
        literManager.findAll(body, noSession(this.props.showDialog)((status, data) => {            
            if(status === 200) {
                console.log(data)
                this.setState({
                    flow_meter:data.data.flow_meter,
                    flow_meter_round:data.data.flow_meter_round
                });
            } else if (status === 404) {
                this.setState({
                    flow_meter: [],
                    flow_meter_round:[]
                });
            } else {
                this.props.alertError(status, data);
            }
        }));
    }
    close = async (e) => {
        if (e) e.preventDefault();
        
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {
        e && e.preventDefault();            
        let body = {};
        let id_list=[],start_hour_list=[],end_hour_list=[];
        if(this.state.flow_meter_round.length>0)
        {
            for(let i=0; i<this.state.flow_meter_round.length; i++){
                if(this.state.flow_meter_round[i].start_hour==null||this.state.flow_meter_round[i].end_hour==null){
                    return this.props.showDialog({
                        alertText: `${i+1}회차 시간을 확인해 주십시오.`,                   
                    })
                }else{
                    id_list.push(this.state.flow_meter_round[i].id)
                    start_hour_list.push(this.state.flow_meter_round[i].start_hour)
                    end_hour_list.push(this.state.flow_meter_round[i].end_hour)
                }
            }
        }
        body.house_id = this.state.house_id;
        body.is_auto_reset=this.state.flow_meter.is_auto_reset;
        body.id = this.state.flow_meter.id;
        id_list.length >0?body.id_list = id_list.join(','):null
        start_hour_list.length >0?body.start_hour_list = start_hour_list.join(','):null
        end_hour_list.length >0?body.end_hour_list = end_hour_list.join(','):null
        
        literManager.update(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                this.props.alertError(status, data);
            } else if (status == 200) {
                this.props.showDialog({
                    alertText: "유량계 회차 설정이 등록되었습니다.",
                    cancelCallback: () => {

                        this.close();
                    }
                })
            }
        }));
    };

    clickCheckBox = (e, type) => {

        let flow_meter = deepcopy(this.state.flow_meter);
         flow_meter[type] = !flow_meter[type];
        
        this.setState({ flow_meter });
    };

    handleSelect = (option, type, index) => {
        let flow_meter_round = deepcopy(this.state.flow_meter_round);        
        if (type == 'start_hour') {
            if (index == '0') {
                if (flow_meter_round[index].end_hour == null) {
                    flow_meter_round[index].start_hour = option.value
                } else {
                    if (Number(option.value) < Number(flow_meter_round[index].end_hour)) {
                        flow_meter_round[index].start_hour = option.value
                    } else {
                        flow_meter_round[index].start_hour = ''                        
                        this.props.showDialog({
                            alertText: "시간을 확인해 주십시오.",
                        })
                    }
                }
            } else {
                if (flow_meter_round[index].end_hour == null) {
                    flow_meter_round[index].start_hour = option.value
                } else {
                    if (Number(option.value) < Number(flow_meter_round[index].end_hour)) {
                        flow_meter_round[index].start_hour = option.value
                    } else {
                        flow_meter_round[index].start_hour = ''                        
                        this.props.showDialog({
                            alertText: "시간을 확인해 주십시오.",
                        })
                    }
                }
                if (index > 0) {
                    if (Number(flow_meter_round[index].start_hour) > Number(flow_meter_round[index - 1].end_hour)) {
                        flow_meter_round[index].start_hour = option.value
                    } else {
                        flow_meter_round[index].start_hour = ''                        
                        this.props.showDialog({
                            alertText: "시간을 확인해 주십시오.",
                        })
                    }
                }
            }
            

            this.setState({ flow_meter_round })
        } else {
            if (index == '0') {
                if (flow_meter_round[index].start_hour == null) {
                    flow_meter_round[index].end_hour = option.value
                } else {
                    if (Number(option.value) > Number(flow_meter_round[index].start_hour)) {            
                        flow_meter_round[index].end_hour = option.value
                    } else {
                        flow_meter_round[index].end_hour = ''
                        this.setState({ flow_meter_round })                                            
                        return this.props.showDialog({
                            alertText: "시간을 확인해 주십시오.",
                        })
                    }
                }
            } else {
                if (flow_meter_round[index].start_hour == null) {
                    flow_meter_round[index].end_hour = option.value
                } else {
                    if (Number(option.value) > Number(flow_meter_round[index].start_hour)) {
                        flow_meter_round[index].end_hour = option.value
                    } else {
                        flow_meter_round[index].end_hour = ''
                        this.setState({ flow_meter_round })             
                        return this.props.showDialog({
                            alertText: "시간을 확인해 주십시오.",
                        })
                    }
                }                
               
            }
            if (flow_meter_round[index + 1] != undefined) {                            
                if (Number(flow_meter_round[index + 1].start_hour) > Number(flow_meter_round[index].end_hour)||flow_meter_round[index + 1].start_hour==null||flow_meter_round[index + 1].start_hour=="") {
                    flow_meter_round[index].end_hour = option.value
                } else {
            
                    flow_meter_round[index].end_hour = ''
                    this.props.showDialog({
                        alertText: "시간을 확인해 주십시오.",
                    })
                }
            }
            
            this.setState({ flow_meter_round })
            
        }
    };
    delete = (e, item) => {
      
        literManager.remove(item.id,noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                this.props.alertError(status, data);
            } else if (status == 200) {
                this.getLiter(event);
            }
        }));
    }

    flowmeterAdd = (e) => {
        let body ={flow_meter_id:this.state.flow_meter.id}
        literManager.create(body, noSession(this.props.showDialog)((status, data) => {            
            if (status !== 200) {
                this.props.alertError(status, data);
            } else if (status == 200) {
                this.getLiter(event);
            }
        }));
    }
    render() {                
      
        return (

            <article id="lcLiterResetWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalLiterReset" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">유량계 회차 설정</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>

                                <section className="modal-body">
                                    <div className="LiterReset-register-wrap">
                                        <ul id="screenOrderList">
                                            <li className="screen-order-list-item">
                                                <article
                                                    className="screen-setting-wrap">
                                                    <div className="screen-setting-left-wrap">
                                                        <div className="lc-theme-checkbox-wrap">
                                                            <input id="mixControlCondition1"
                                                                value="time"
                                                                checked={this.state.flow_meter.is_auto_reset?this.state.flow_meter.is_auto_reset:false}
                                                                onChange={() => {
                                                                }} type="checkbox"
                                                                name="mix-control-condition" />
                                                            <label
                                                                htmlFor="mixControlCondition1"
                                                                onClick={(e) => this.clickCheckBox(e, 'is_auto_reset')}
                                                                className="box-shape-label" />
                                                            <label
                                                                htmlFor="mixControlCondition1"
                                                                onClick={(e) => this.clickCheckBox(e, 'is_auto_reset')}
                                                                className="text-label">자동초기화 사용 (0시에 자동 초기화)</label>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>
                                    </div>
                                    {
                                        this.state.flow_meter_round.map((item, index) => {
                                            
                                            return(
                                                <div className="mix-control-container" key={index}>
                                                    <span className="title-name">{index+1}회차 제어</span>
                                                    {index!=0?<button className="delete-button" onClick={(e)=>this.delete(e,item)}>회차 삭제</button>:null}
                                                    <div className="mix-control-wrap">
                                                        <div id="autoControlSettingWrap" className="auto-control-form-section">
                                                            <div className="mix-control-header mix-control-item-wrap">
                                                                <span className="mix-control-title">{index+1}회차 조건 설정</span>
                                                            </div>
                                                            <div className="auto-control-setting-form auto-control-condition-wrap">
                                                                <label className="auto-control-form-label">시작시간</label>
                                                                <div className="lc-setting-form-wrap">
                                                                    <div className="form-flex-wrap">
                                                                        <div
                                                                            className="left-item select-module-wrap lc-size-36">
                                                                                
                                                                            <Select
                                                                                value={isNumber(item.start_hour)?{value:String(item.start_hour),label:String(item.start_hour).length!=1?String(item.start_hour):'0'+item.start_hour+'시'}:null}
                                                                                placeholder="시간 선택"
                                                                                onChange={option => this.handleSelect(option, "start_hour",index)}
                                                                                isSearchable={false}
                                                                                options={time}
                                                                                styles={colourStyles} />
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <i className="hr" />
                                                            </div>
                                                            <div className="auto-control-setting-form auto-control-execution-wrap">
                                                                <label className="auto-control-form-label">종료시간</label>
                                                                <div className="lc-setting-form-wrap">
                                                                    <div className="form-flex-wrap">
                                                                        <div className="left-item select-module-wrap lc-size-36">
                                                                            <Select
                                                                                 value={isNumber(item.end_hour)?{value:item.end_hour,label:String(item.end_hour).length!=1?item.end_hour+'시':'0'+item.end_hour+'시'}:null}
                                                                                placeholder="시간 선택"
                                                                                onChange={option => this.handleSelect(option, "end_hour",index)}
                                                                                isSearchable={false}
                                                                                options={time}
                                                                                styles={colourStyles} />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            )
                                        })
                                    }
                               
                                    <div id="autoControlButtonWrap" className="auto-control-form-section">
                                          <div className="button-add">
                                        <button className="card-header-button" onClick={(e)=>this.flowmeterAdd(e)}>+ 제어추가</button>
                                        </div>
                                        <div className="button-wrap">
                                            <button className="card-header-button" onClick={this.close}>취소</button>
                                        </div>
                                        <div className="button-wrap">
                                            <button className="theme-button" onClick={this.submit}>{this.state.isUpdate ? "수정" : "등록"}</button>
                                        </div>
                                    </div>
                                </section>

                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useLiterAutoReset(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalLiterAutoReset));
