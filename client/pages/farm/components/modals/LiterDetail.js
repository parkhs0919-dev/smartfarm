import React, { Component } from 'react';
import { useLiterDetail } from "../../contexts/modals/LiterDetail";
import { useAlert } from "../../contexts/alert";
import literManager from '../../managers/liter';
import deepcopy from "../../../../utils/deepcopy";
import CONSTANT from "../../constants/constant";
import { useSensorGraph } from "../../contexts/modals/sensorGraph";
import { date } from "../../../../utils/filter";
import { noSession } from "../../utils/session";
import Select from 'react-select';
import moment from 'moment';
import sensorManager from '../../managers/sensor';
import { colourStyles } from "../../utils/select";
import DatePicker from 'react-datepicker';
const DATE_DISPLAY_FORMAT = "yyyy-MM-dd";
const Time = CONSTANT.Liter.Time;
const DEFAULT_VALUE = {
    Date: moment(date(new Date(), DATE_DISPLAY_FORMAT)),
    check: false,
    control: [],
    tab: 'translate',
    datePickerOpenState: {
        Date: false,
    },
    startTime: '',
    endTime: '',
    Liter: [],
    house_id: '',
    flow_meter: {},
    translate: [],
    totalvalue: 0,
    time: [],
}
class ModalLiterDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            Date: moment(date(new Date(), DATE_DISPLAY_FORMAT)),
            check: false,
            control: [],
            tab: 'translate',
            datePickerOpenState: {
                Date: false,
            },
            startTime: '',
            endTime: '',
            Liter: [],
            house_id: '',
            flow_meter: {},
            translate: [],
            totalvalue: 0,
            time: [],
        }
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        document.addEventListener('keyup', this.closeOnEscape);

        this.setState({ Liter: data.item, house_id: data.house_id }, (e) => this.getLiter(e))
    };
    getLiter = (e) => {
        let body = {
            house_id: this.state.house_id,
            id: this.state.Liter.id
        }
        literManager.findAll(body, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                this.setState({
                    flow_meter: data.data.flow_meter,
                });
            } else if (status === 404) {
                this.setState({
                    flow_meter: [],
                });
            } else {
                this.props.alertError(status, data);
            }
        }));
    }
    close = async (e) => {
        if (e) e.preventDefault();
        this.setState(DEFAULT_VALUE);
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {
        e && e.preventDefault();
        let body = {};
        let dateformat = this.state.Date.toISOString();
        dateformat = dateformat.split('T');
        let date = dateformat[0].split('-')
        let year = date[0], month = date[1], day = date[2]
        if (this.state.tab == 'translate') {
            body.house_id = this.state.house_id;
            body.year = year;
            body.month = month;
            body.day = day;
            body.id = this.state.flow_meter.id;
            body.type = this.state.tab;
        } else if (this.state.tab == 'time') {
            if (!this.state.startTime) {
                return this.props.showDialog({
                    alertText: "시간을 확인해주세요."
                })
            }
            if (!this.state.endTime) {
                return this.props.showDialog({
                    alertText: "시간을 확인해주세요."
                })
            }
            body.start_date = this.state.startTime.value;
            body.end_date = this.state.endTime.value;
            body.house_id = this.state.house_id;
            body.year = year;
            body.month = month;
            body.day = day;
            body.id = this.state.flow_meter.id;
            body.type = this.state.tab;
        }
        literManager.Detailfind(body, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                console.log(data)
                if (body.type == 'translate') {
                    this.setState({
                        translate: data.data,

                    }, (e) => this.findtotalValue(e, 'translate'))
                } else if (body.type == 'time') {
                    this.setState({
                        time: data.data,

                    }, (e) => this.findtotalValue(e, 'time'))
                }
            } else if (status === 404) {
                this.setState({
                    translate: [],
                })
            } else {
                this.props.alertError(status, data);
            }
        }));
    };
    findtotalValue = (e, type) => {
        let result = 0;
        if (type == 'translate') {
            if (this.state.translate && this.state.translate.length > 0) {
                for (let i = 0; i < this.state.translate.length; i++) {
                    result += Number(this.state.translate[i].result)
                }
                this.setState({ totalvalue: result })
            }
        } else if (type == 'time') {
            this.setState({ totalvalue: this.state.time[0].result })
        }
    }
    tab = (e, type) => {
        if (this.state.type == type) {
            return;
        } else {
            this.setState({ tab: type })
        }

    }

    closeDatePickerOnTouchOutside = (e, key) => {
        let datePickerOpenState = deepcopy(this.state.datePickerOpenState);
        datePickerOpenState[key] = false;
        this.setState({ datePickerOpenState });
    };

    handleDatePickerState = (e, key) => {
        e && e.preventDefault();
        let datePickerOpenState = Object.assign({}, this.state.datePickerOpenState);
        datePickerOpenState[key] = !datePickerOpenState[key];
        this.setState({ datePickerOpenState });
    };
    handleDate = (val, key) => {
        let state = Object.assign({}, this.state);
        state[key] = moment(date(new Date(val), 'yyyy-MM-dd'));
        this.setState(state);
        this.handleDatePickerState(null, key);
    };
    handleSelect_startTime = (option, key) => {
        let startTime = deepcopy(this.state.startTime);
        startTime = option;
        this.setState({ startTime });
    }
    handleSelect_endTime = (option, key) => {
        let endTime = deepcopy(this.state.endTime);
        endTime = option;
        this.setState({ endTime });
    }
    clearLiter = (e) => {

        sensorManager.resetLiter(this.state.Liter.id, (status, data) => {
            if (status === 200) {
                this.props.showDialog({
                    alertText: "초기화되었습니다."
                })
            } else {
                this.props.alertError(status, data);
            }
        });
    };
    openSensorGraphModal = (e) => {
        this.props.openSensorGraphModal(this.state.Liter)
    }
    render() {
        return (

            <article id="lcLiterDetailWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalLiterDetail" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">유량계 상세보기</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>

                                <section className="modal-body">
                                    <div className="LiterDetail-register-wrap">
                                        <ul id="screenOrderList">
                                            <div className="screen-order-list-item-Detail">
                                                <div className="screen-setting-left-wrap-Detail">
                                                    <li className={this.state.tab == 'translate' ? 'tab_on' : ''} onClick={(e) => this.tab(e, 'translate')}>회차 검색</li>
                                                    <li className={this.state.tab == 'time' ? 'tab_on' : ''} onClick={(e) => this.tab(e, 'time')}>시간 검색</li>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                    {
                                        this.state.tab == 'translate' ?

                                            <div className="mix-control-container">
                                                <span className="title-name">날짜 선택</span>

                                                <label className="toggle-date-picker-label" onClick={e => this.handleDatePickerState(e, "Date")}>
                                                    <input type="text" readOnly value={this.state.Date ? date(this.state.Date, DATE_DISPLAY_FORMAT) : ''} />
                                                </label>
                                                {this.state.datePickerOpenState.Date &&
                                                    <DatePicker dateFormat={"YYYY-MM-DD"}
                                                        onClickOutside={e => this.closeDatePickerOnTouchOutside(e, 'Date')}
                                                        className="lc-farmlabs-datepicker"
                                                        popperClassName="lc-farmlabs-datepicker-popper"
                                                        selected={this.state.Date}
                                                        withPortal
                                                        inline
                                                        minDate={new Date(new Date().setDate(new Date().getDate() - 90))}
                                                        maxDate={moment(date(new Date(), DATE_DISPLAY_FORMAT))}
                                                        onChange={val => this.handleDate(val, "Date")} />
                                                }

                                                <div id="LiterDetailButtonWrap" className="liter-Detail-form-section">
                                                    <div className="button-wrap">
                                                        <button className="card-header-button" onClick={this.submit}>조회</button>
                                                    </div>
                                                </div>
                                                {
                                                    this.state.translate && this.state.translate.length ?
                                                        <div className="mix-control-wrap">
                                                            <div id="autoControlSettingWrap" className="auto-control-form-section">
                                                                <div id="LiterDetailButtonWrap" className="liter-Detail-form-section" style={{ backgroundColor: 'white', padding: '5px', marginBottom: '10px' }}>
                                                                    <span className="title-name" style={{ float: 'left', width: 'auto' }}>총 유량값</span>
                                                                    <span className="title-name" style={{ float: 'right', width: 'auto', color: '#58A943' }}>{this.state.totalvalue}L</span>

                                                                </div>
                                                                <table>
                                                                    <thead>
                                                                        <tr className='thead-tr'>
                                                                            <th>회차</th>
                                                                            <th>유량</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {
                                                                            this.state.translate.map((item, index) => {
                                                                                return (
                                                                                    <tr key={index}>
                                                                                        <td>{index + 1}회차({item.start_hour} ~ {item.end_hour})</td>
                                                                                        <td>{item.result}L</td>
                                                                                    </tr>
                                                                                )
                                                                            })
                                                                        }
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div> : null
                                                }
                                                <div id="autoControlButtonWrap" className="auto-control-form-section">
                                                    <div className="button-wrap">
                                                        <button className="card-header-button" onClick={(e) => this.openSensorGraphModal(e)}>그래프 보기</button>
                                                    </div>
                                                    <div className="button-wrap">
                                                        <button className="theme-button" onClick={(e) => this.clearLiter(e)}>유량계 초기화</button>
                                                    </div>
                                                    <button className="card-header-button add-control-btn" onClick={this.close}>확인</button>

                                                </div>

                                            </div>

                                            :
                                            <div className="mix-control-container">
                                                <span className="title-name">날짜 선택</span>

                                                <label className="toggle-date-picker-label" onClick={e => this.handleDatePickerState(e, "Date")}>
                                                    <input type="text" readOnly value={this.state.Date ? date(this.state.Date, DATE_DISPLAY_FORMAT) : ''} />
                                                </label>
                                                {this.state.datePickerOpenState.Date &&
                                                    <DatePicker dateFormat={"YYYY-MM-DD"}
                                                        onClickOutside={e => this.closeDatePickerOnTouchOutside(e, 'Date')}
                                                        className="lc-farmlabs-datepicker"
                                                        popperClassName="lc-farmlabs-datepicker-popper"
                                                        selected={this.state.Date}
                                                        withPortal
                                                        inline
                                                        minDate={new Date(new Date().setDate(new Date().getDate() - 90))}
                                                        maxDate={moment(date(new Date(), DATE_DISPLAY_FORMAT))}
                                                        onChange={val => this.handleDate(val, "Date")} />
                                                }
                                                <span className="title-name" style={{ marginTop: '10px' }}>시간 선택</span>
                                                <div className='select-section'>
                                                    <div className='margin-div'>
                                                        <Select
                                                            placeholder="시간 선택"
                                                            value={this.state.startTime}
                                                            isSearchable={false}
                                                            onChange={e => this.handleSelect_startTime(e, 'startTime')}
                                                            options={Time}
                                                            styles={colourStyles} />
                                                    </div>
                                                    <Select
                                                        placeholder="시간 선택"
                                                        value={this.state.endTime}
                                                        isSearchable={false}
                                                        onChange={e => this.handleSelect_endTime(e, 'endTime')}
                                                        options={Time}
                                                        styles={colourStyles} />
                                                </div>

                                                <div id="LiterDetailButtonWrap" className="liter-Detail-form-section">
                                                    <div className="button-wrap">
                                                        <button className="card-header-button" onClick={this.submit}>조회</button>
                                                    </div>
                                                </div>
                                                <div id="LiterDetailButtonWrap" className="liter-Detail-form-section" style={{ backgroundColor: 'white', padding: '5px' }}>
                                                    <span className="title-name" style={{ float: 'left', width: 'auto' }}>총 유량값</span>
                                                    <span className="title-name" style={{ float: 'right', width: 'auto', color: '#58A943' }}>{this.state.totalvalue}L</span>

                                                </div>
                                                <div id="autoControlButtonWrap" className="auto-control-form-section">
                                                    <div className="button-wrap">
                                                        <button className="card-header-button" onClick={(e) => this.openSensorGraphModal(e)}>그래프 보기</button>
                                                    </div>
                                                    <div className="button-wrap">
                                                        <button className="theme-button" onClick={(e) => this.clearLiter(e)}>유량계 초기화</button>
                                                    </div>
                                                    <button className="card-header-button add-control-btn" onClick={this.close}>확인</button>

                                                </div>
                                            </div>
                                    }


                                </section>

                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useLiterDetail(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(useSensorGraph(
    ({ actions }) => ({
        openSensorGraphModal: actions.open
    })
)(ModalLiterDetail)));
