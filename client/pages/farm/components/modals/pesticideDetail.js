import React, { Component,Fragment } from 'react';
import { usepesticideDeatail } from "../../contexts/modals/pesticideDeatail";
import { useAlert } from "../../contexts/alert";
import pesticideManager from '../../managers/pesticide';
import deepcopy from "../../../../utils/deepcopy";
import { noSession } from "../../utils/session";
import CONSTANT from "../../constants/constant";



const DEFAULT_AREA = CONSTANT.defaultpesticide.area
const CAN_MODIFY_ORDER = CONSTANT.defaultpesticide.canModifyChild;

const DEFAULT_VALUE = {
    pesticide_name: '',
    Section_Time: '',
    area: DEFAULT_AREA,
    FrostControl_Time: '',
    drugControl_Time: '',
    house_id: '',
}

class ModalpesticideDetail extends Component {
    constructor(props) {
        super(props);
        this.audio = null
        this.moreMenuRefs = new Map();
        this.state = {
            pesticide: [],
            pesticide_Zone: [],
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (pesticide) => {
      
        let state = Object.assign({}, pesticide);
        this.getArea(state);
        document.addEventListener('keyup', this.closeOnEscape);

    };

    close = (e) => {
        if (e) e.preventDefault();
        this.setState(DEFAULT_VALUE);
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    getArea = (state) => {
        let query = {
            id: state.id
        }

        pesticideManager.findarea(query, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                this.props.alertError(status, data);
            } else if (status == 200) {
                
                this.setState({
                    pesticide: data.data.pest_control,
                    pesticide_Zone: data.data.pest_control_zone
                })
            }
        }));




    }

    submit = (e) => {
        e.preventDefault();
        const _this = this;
        let state = Object.assign({}, this.state);
        let body = {};
        let order = [];
        let is_use = [];


        if (!state.pesticide.pest_control_name) {
            return this.props.showDialog({
                alertText: "방제명을 입력해 주세요."
            });
        }
        if (!state.pesticide.zone_time) {
            return this.props.showDialog({
                alertText: "구역벨브 시간을 입력해 주세요."
            });
        }
        // if (!state.pesticide.medicine_time) {
        //     return this.props.showDialog({
        //         alertText: "약제방제 모터 동작시간을 입력해 주세요."
        //     });
        // }
        // if (!state.pesticide.compressor_time) {
        //     return this.props.showDialog({
        //         alertText: "서리방제 벨브 개폐시간을 입력해 주세요."
        //     });
        // }
    
        for (let i = 0; i < state.pesticide_Zone.length; i++) {
            order.push(state.pesticide_Zone[i].key)
            is_use.push(state.pesticide_Zone[i].is_use)
        }

        body.pest_control_name = state.pesticide.pest_control_name;
        body.zone_time = Number(state.pesticide.zone_time);
        body.medicine_time = Number(state.pesticide.medicine_time);
        body.compressor_time = Number(state.pesticide.compressor_time);
        body.id = Number(state.pesticide.id);
        body.zone_list = order.join(',');
        body.zone_use_list = is_use.join(',');
        pesticideManager.update(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                
                _this.props.alertError(status, data);
            } else if (status == 200) {
                _this.props.showDialog({
                    alertText: "방제기 설정이 수정되었습니다.",
                    cancelCallback: () => {

                        _this.close();
                    }
                })
            }
        }));
    };

    pesticidename = (e, index) => {
        if (e.target.value.length < 50) {
            let pesticide = deepcopy(this.state.pesticide)

            pesticide.pest_control_name = e.target.value;
            this.setState({ pesticide })
        }
    }
    SectionTime = (e) => {
        if (e.target.value.length < 10) {
            let pesticide = deepcopy(this.state.pesticide)
            pesticide.zone_time = e.target.value;
            this.setState({ pesticide })
        }
    }
    compressortime = (e) => {
        if (e.target.value.length < 10) {
            let pesticide = deepcopy(this.state.pesticide)
            pesticide.compressor_time = e.target.value;
            this.setState({ pesticide })
        }
    }
    drugControlTime = (e) => {
        if (e.target.value.length < 10) {
            let pesticide = deepcopy(this.state.pesticide)
            pesticide.medicine_time = e.target.value;
            this.setState({ pesticide })
        }
    }

    changeOrder = (e, index, direction) => {
        e && e.preventDefault();
        let pesticide_Zone = deepcopy(this.state.pesticide_Zone);
        if (direction === "down") {
            // pesticide_Zone[index].key = pesticide_Zone[index].key + 1;
            // pesticide_Zone[index + 1].key = pesticide_Zone[index + 1].key - 1;
            pesticide_Zone.splice(index + 1, 0, pesticide_Zone.splice(index, 1)[0]);
        } else if (direction === "up") {
            // pesticide_Zone[index].key = pesticide_Zone[index].key - 1;
            // pesticide_Zone[index - 1].key = pesticide_Zone[index - 1].key + 1;
            pesticide_Zone.splice(index - 1, 0, pesticide_Zone.splice(index, 1)[0]);
        }

        this.setState({ pesticide_Zone });
    };
    clickCheckBox = (e,index) => {
        let pesticide_Zone =deepcopy(this.state.pesticide_Zone)
        pesticide_Zone[index].is_use = !pesticide_Zone[index].is_use;
        this.setState({ pesticide_Zone });
    };

    render() {
        return (
            <article id="lcModalpesticideWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalpesticide" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">방제기 설정 수정</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>
                                <section className="modal-body">
                                    <React.Fragment>
                                        <div className="pesticide-register-wrap">
                                            <div id="pesticideNameWrap" className="pesticide-form-section">
                                                <label className="pesticide-form-label">방제명</label>
                                                <input type="text" value={this.state.pesticide.pest_control_name ? this.state.pesticide.pest_control_name : ''}
                                                    onChange={this.pesticidename} className="lc-farm-input"
                                                    placeholder="방제명" />

                                            </div>
                                        </div>
                                        <div className="pesticide-register-wrap">
                                            <label className="pesticide-form-label">구간밸브 동작 시간</label>
                                            <div className="lc-setting-form-wrap">
                                                <div className="form-flex-wrap">
                                                    <div
                                                        className="left-item select-module-wrap lc-size-36">
                                                        <input type="text" value={this.state.pesticide.zone_time ? this.state.pesticide.zone_time : ''}
                                                            onChange={this.SectionTime} className="lc-farm-input"
                                                            placeholder="초" />
                                                    </div>
                                                    <span className='lc-modifier'>초</span>
                                                </div>
                                            </div>
                                            {                                                
                                                this.state.pesticide.is_frost!=true?
                                              <Fragment>
                                            <label className="pesticide-form-label">약재방제 모터 동작시간</label>
                                            <div className="lc-setting-form-wrap">
                                                <div className="form-flex-wrap">
                                                    <div
                                                        className="left-item select-module-wrap lc-size-36">
                                                        <input type="text" value={this.state.pesticide.medicine_time ? this.state.pesticide.medicine_time : ''}
                                                            onChange={this.drugControlTime} className="lc-farm-input"
                                                            placeholder="초" />
                                                    </div>
                                                    <span className='lc-modifier'>초</span>
                                                </div>
                                            </div>        
                                            </Fragment>
                                            :null
                                            }                                   
                                            <label className="pesticide-form-label">서리방제 벨브 개폐시간</label>
                                            <div className="lc-setting-form-wrap">
                                                <div className="form-flex-wrap">
                                                    <div
                                                        className="left-item select-module-wrap lc-size-36">
                                                        <input type="text" value={this.state.pesticide.compressor_time ? this.state.pesticide.compressor_time : ''}
                                                            onChange={this.compressortime} className="lc-farm-input"
                                                            placeholder="초" />

                                                    </div>
                                                    <span className='lc-modifier'>초</span>
                                                </div>
                                            </div>
                                            
                                            <ul id="screenOrderList">
                                                <div className="pesticide-screen-section">
                                                    <label className="pesticide-form-label">구역설정</label>
                                                </div>
                                                {
                                                    this.state.pesticide_Zone.map((item, index) => {

                                                        return (
                                                            <li className="screen-order-list-item" key={index}>
                                                                <article
                                                                    className={"screen-setting-wrap" + (item.isTabOpen ? ' lc-open' : '')}>
                                                                    <div className="screen-setting-left-wrap">
                                                                        <div className="lc-theme-checkbox-wrap">
                                                                        <input id="mixControlCondition1"
                                                                                value="time"
                                                                                checked={item.is_use}
                                                                                onChange={() => {
                                                                                }} type="checkbox"
                                                                                name="mix-control-condition" />
                                                                            <label
                                                                                htmlFor="mixControlCondition1"
                                                                                onClick={(e) => this.clickCheckBox(e,index)}
                                                                                className="box-shape-label" />
                                                                            <label
                                                                                htmlFor="mixControlCondition1"
                                                                                onClick={(e) => this.clickCheckBox(e,index)}
                                                                                className="text-label">{item.pest_control_zone_name}</label>

                                                                        </div>

                                                                        {
                                                                            CAN_MODIFY_ORDER[item.type] ?
                                                                                (
                                                                                    <button
                                                                                        className={"open-menu-btn" + (item.isTabOpen ? ' lc-open' : '')}
                                                                                        onClick={e => this.toggleMenu(e, item, index)} />
                                                                                ) : null
                                                                        }
                                                                    </div>

                                                                    <div className="screen-setting-right-wrap">
                                                                        {
                                                                            index !== this.state.pesticide_Zone.length - 1 &&
                                                                            (
                                                                                <button className="order-btn order-down"
                                                                                    onClick={e => this.changeOrder(e, index, 'down')} />
                                                                            )
                                                                        }
                                                                        {
                                                                            index !== 0 &&
                                                                            (
                                                                                <button className="order-btn order-up"
                                                                                    onClick={e => this.changeOrder(e, index, 'up')} />
                                                                            )
                                                                        }
                                                                    </div>
                                                                </article>
                                                                {
                                                                    CAN_MODIFY_ORDER[item.type] && item.isTabOpen ?
                                                                        (
                                                                            <article className="lc-screen-menu-child-wrap">
                                                                                {this.generateChildContent(item.type)}
                                                                            </article>
                                                                        ) : null
                                                                }
                                                            </li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                            <div className="delay-recount-wrap">
                                                <button className="theme-button" onClick={e => this.submit(e)}>등록</button>
                                            </div>

                                        </div>



                                    </React.Fragment>

                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }

}

export default useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(usepesticideDeatail(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalpesticideDetail));
