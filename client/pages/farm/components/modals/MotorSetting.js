import React, {Component} from 'react';
import Select from 'react-select';
import {useMotorSetting} from "../../contexts/modals/motorSetting";
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import controlManager from '../../managers/control';
import deepcopy from "../../../../utils/deepcopy";
import {noSession} from "../../utils/session";
import {colourStyles} from "../../utils/select";
import translate from "../../constants/translate";
const TYPE_MOTOR = "motor";

const Directions = [
    {label: "선택 없음", value: "none"},
    {label: "좌측", value: "left"},
    {label: "우측", value: "right"}
];

const DEFAULT_STATE = {
    motors: [],
    arr: ["left","left","left","left","left"],
};

class ModalMotorSetting extends Component {
    constructor(props) {
        super(props);

        this.state = deepcopy(DEFAULT_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        this.findAllMotors();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findAllMotors = () => {
        const _this = this;
        let query = {
            type: TYPE_MOTOR,
            house_id: this.props.getHouseId()
        };

        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    motors: data.data.rows,
                });
            } else if (status === 404) {
                _this.setState({
                    motors: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    updateMotors = () => {
        const _this = this;
        let motors = deepcopy(this.state.motors);
        controlManager.updateAll({motors}, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.props.showDialog({
                    alertText: "모터 설정이 변경되었습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    handleInput = (e, index, key) => {
        let value = e.target.value;
        if(e.target.type === "number") {
            if(value <= 0) {
                value = 1;
            }
        }
        let motors = deepcopy(this.state.motors);
        motors[index][key] = value;
        this.setState({motors});
    };

    selectHandle = (e, index, key) => {
        let value = e.value;
        let motors = deepcopy(this.state.motors);
        motors[index][key] = value;
        this.setState({motors});
    };

    render() {
        return (
            <article id="lcModalMotorSettingWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalMotorSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">모터설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <ul id="motorSettingList">
                                        {
                                            this.state.motors.map((item, index) => {
                                                return (
                                                    <li key={index}>
                                                        <div className="motors-item-wrap">
                                                            <div className="motors-label-wrap">
                                                                <label className="motors-label">기기명</label>
                                                                <label className="motors-label">방향 설정</label>
                                                                <label className="motors-label">범위값</label>
                                                            </div>
                                                            <div className="motors-input-wrap">
                                                                <div className="input-wrap">
                                                                    <input className="lc-farm-input" type="text" value={item.control_name} onChange={e => this.handleInput(e, index, "control_name")}/>
                                                                </div>
                                                                <div className="input-wrap">
                                                                    <Select
                                                                        value={this.state.arr[index]}
                                                                        placeholder={translate.MOTOR_DIRECTION[item.direction]}
                                                                        onChange={e => this.selectHandle(e, index, "direction")}
                                                                        isSearchable={ false }
                                                                        options={Directions}
                                                                        styles={colourStyles}/>
                                                                </div>
                                                                <div className="input-wrap">
                                                                    <input className="lc-farm-input" type="number" value={item.range} onChange={e => this.handleInput(e, index, "range")}/>
                                                                    <span>초</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                )
                                            })
                                        }
                                    </ul>

                                    <button className="theme-button" onClick={this.updateMotors}>적용</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId,
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useMotorSetting(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalMotorSetting)));