import React, {Component} from 'react';
import {useHouse} from "../../contexts/house";
import {useAlert} from "../../contexts/alert";
import {usePBandSensorSetting} from "../../contexts/modals/pBandSensorSetting";
import {usePBand} from "../../contexts/modals/pBand";
import pBandsManager from "../../managers/pBands";
import {noSession} from "../../utils/session";
import {on} from "../../utils/socket";
import alarmManager from "../../managers/alarm";

class ModalPbandSensorSetting extends Component {

    constructor(props) {
        super(props);

        this.state = {
            pBands : [],
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        this.findPBands();
        document.addEventListener('keyup', this.closeOnEscape);
    }

    findPBands = () => {
        return new Promise(async (resolve, reject) => {
            try {
                const _this = this;
                pBandsManager.findAll({
                    house_id: this.props.getHouseId()
                }, noSession(this.props.showDialog)((status, data) => {
                    if (status === 200) {
                        _this.setState({pBands : data.data.rows}, () => resolve(true));
                    } else if (status === 404) {
                        _this.setState({
                            pBands: []
                        }, () => resolve(true));
                    } else {
                        _this.props.alertError(status, data);
                        resolve(true);
                    }
                }));
            } catch (e) {
                reject(e);
            }
        });
    }

    close = (e) => {
        if (e) e.preventDefault();
        this.props.close();
    }

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    }

    openPBandSetting = (e, index) => {
        document.removeEventListener('keyup', this.closeOnEscape);
        let item = this.state.pBands[index];
        this.props.openPbandModal(item, this.afterPBandSetting);
    }

    afterPBandSetting = () => {
        this.findPBands();
        document.addEventListener('keyup', this.closeOnEscape);
    }

    removePBand = (e, index) => {

        const _this = this;
        const item = this.state.pBands[index];

        this.props.showDialog({
            alertText: "P-BAND를 삭제하시겠습니까?",
            cancelText: "취소",
            submitText: "확인",
            submitCallback: () => {
                pBandsManager.remove(item, noSession(this.props.showDialog)((status, data) => {
                    if(status === 200) {
                        _this.props.showDialog({
                            alertText: "삭제되었습니다.",
                        });

                        this.findPBands();
                    } else {
                        _this.props.alertError(status, data);
                    }
                }));
            }
        })

    }

    render() {
        return (
            <article id="lcModalPbandSensorSettingWrap"
                     className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcPBandSensorSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">P-밴드 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>
                                <section className="modal-body">
                                    <div className="sensor-setting-button-wrap">
                                        <button className="theme-button" onClick={e => this.openPBandSetting(e)}>설정 등록</button>
                                    </div>
                                    <div className="sensor-setting-list-wrap">
                                        <ul className="sensor-setting-list">

                                            {this.state.pBands.map((item, index) => {
                                                return (
                                                    <li key={index}>
                                                        <p className="lc-pband-name-wrap">
                                                            <span className="lc-pband-name-index">{index + 1}</span>
                                                            <span className="lc-pband-name">{item.p_band_name}</span>
                                                        </p>
                                                        <div className="three-dot-menu" tabIndex="0"  style={{zIndex: (5+ this.state.pBands.length - index)}}>
                                                            <div className="icon-wrap">
                                                                <i/><i/><i/>
                                                            </div>
                                                            <div className="three-dot-view-more-menu">
                                                                <ul>
                                                                    <li onClick={e => this.openPBandSetting(e, index)}><span>수정</span></li>
                                                                    <li onClick={e => this.removePBand(e, index)}><span>삭제</span></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </li>
                                                )
                                            })
                                            }

                                        </ul>
                                    </div>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>

            </article>
        )
    }


}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(usePBand(
    ({actions}) => ({
        openPbandModal : actions.open,
    })
)(usePBandSensorSetting(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalPbandSensorSetting))));
