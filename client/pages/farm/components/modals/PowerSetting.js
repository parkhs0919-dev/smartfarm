import React, {Component} from 'react';
import {usePowerSetting} from "../../contexts/modals/powerSetting";
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import controlManager from '../../managers/control';
import deepcopy from "../../../../utils/deepcopy";
import {noSession} from "../../utils/session";
const TYPE_POWER = "power";

const DEFAULT_STATE = {
    powers: []
};

class ModalPowerSetting extends Component {
    constructor(props) {
        super(props);

        this.state = deepcopy(DEFAULT_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        this.findAllPowers();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findAllPowers = () => {
        const _this = this;
        let query = {
            type: TYPE_POWER,
            house_id: this.props.getHouseId()
        };

        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    powers: data.data.rows
                });
            } else if (status === 404) {
                _this.setState({
                    powers: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    updatePowers = () => {
        const _this = this;
        let powers = deepcopy(this.state.powers);
        controlManager.updateAll({powers}, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.props.showDialog({
                    alertText: "전원 설정이 변경되었습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    handleInput = (e, index, key) => {
        let powers = deepcopy(this.state.powers);
        powers[index][key] = e.target.value;
        this.setState({powers});
    };

    render() {
        return (
            <article id="lcModalPowerSettingWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalPowerSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">전원 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <ul id="powerSettingList">
                                        {
                                            this.state.powers.map((item, index) => {
                                                return (
                                                    <li key={index}>
                                                        <input className="lc-farm-input" type="text" value={item.control_name} onChange={e => this.handleInput(e, index, "control_name")}/>
                                                    </li>
                                                )
                                            })
                                        }
                                    </ul>
                                    <button className="theme-button" onClick={this.updatePowers}>등록</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId,
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(usePowerSetting(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalPowerSetting)));