import React, { Component } from 'react';
import { useUserAdd } from "../../contexts/modals/UserAdd";
import { useuser } from '../../contexts/modals/User'
import { useHouse } from "../../contexts/house";
import { useSocket } from "../../contexts/socket";
import { useAlert } from "../../contexts/alert";
import CONSTANT from "../../constants/constant";
import userManager from '../../managers/user';
import Pagination from '../Pagination';
import deepcopy from "../../../../utils/deepcopy";
import { noSession } from "../../utils/session";
import { useUserDetail } from '../../contexts/modals/Userdetail'

class ModalUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            house_id: props.getHouseId() ? props.getHouseId() : window.houses[0].id,
            offset: 0,
            size: CONSTANT.defaultLoadSize,
            usercount: 0,
            user: [],
            searchname: ''
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
        this.props.setuserCallback(this.getuser);
    }

    init = (data) => {
        this.getuser(0);
        document.addEventListener('keyup', this.closeOnEscape);
    };
    getuser = (offset) => {

        this.setState({ offset: offset })
        let query = {
            house_id: this.state.house_id,
            size: this.state.size,
            offset: offset
        }
        userManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                this.setState({
                    user: data.data.rows,
                    usercount: data.data.count
                })
            } else if (status === 404) {
                this.setState({
                    user: []
                })
            } else {
                this.props.alertError(status, data);
            }
        }))
    }

    close = (e) => {
        if (e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    removeUser = (e, item) => {
        if (e) e.preventDefault();
        this.props.showDialog({
            alertText: "사용자를 삭제하시겠습니까?",
            cancelText: "취소",
            submitText: "확인",
            submitCallback: () => {
                userManager.remove(item.user_id, noSession(this.props.showDialog)((status, data) => {
                    if (status === 200) {
                        this.props.showDialog({
                            alertText: "삭제되었습니다.",
                        });
                        this.getuser(0);
                    } else {
                        this.props.alertError(status, data);
                    }
                }));
            }
        })
    };
    openuseradd = (e) => {
        if (e) e.preventDefault();
        this.props.UserAddOpen(this.state.house_id)
    }
    search = (offset) => {
        this.setState({ offset: offset })
        let query = {
            house_id: this.state.house_id,
            size: this.state.size,
            offset: offset,
            name: this.state.searchname
        }
        userManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                this.setState({
                    user: data.data.rows,
                    usercount: data.data.count
                })
            } else if (status === 404) {

                this.setState({
                    user: []
                })
            } else {
                this.props.alertError(status, data);
            }
        }))
    }
    handlesearch = (e) => {
        const { value } = e.target;
        this.setState({
            searchname: value
        })
    }

    UserDetailOpen = (e, index) => {
        if (e) e.preventDefault();
        let user = deepcopy(this.state.user[index])
        this.props.UserDetailOpen(user)
    }
    render() {
        return (
            <article id="lcModalUserWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalUser" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">사용자 관리</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>
                                <section className="modal-body">
                                    <React.Fragment>
                                        <div className="User-register-wrap">
                                            <div id="UserNameWrap" className="User-form-section">
                                                <input type="text" value={this.state.searchname}
                                                    onChange={this.handlesearch} className="searchInput"
                                                    placeholder="검색어를 입력하세요(이름)" />
                                                <button className="searchbutton" onClick={(e) => this.search(0)}>조회</button>
                                                <button className='addbutton' onClick={(e) => this.openuseradd(e)}>사용자 추가</button>
                                            </div>
                                        </div>
                                        <div className="User-register-wrap">
                                            <form onSubmit={this.submit}>
                                                <div id="UserSettingWrap" className="User-form-section">
                                                    <div className="User-setting-form User-condition-wrap">
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <th style={{ width: '20%' }}>NO.</th>
                                                                    <th style={{ width: '20%' }}>이름</th>
                                                                    <th style={{ width: '20%' }}>활성화</th>
                                                                    <th style={{ width: '40%' }}>기능</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {
                                                                    this.state.user && this.state.user.length ?
                                                                        this.state.user.map((item, index) => {
                                                                            return (
                                                                                <tr key={index}>
                                                                                    <td>{item.no}</td>
                                                                                    <td>{item.name}</td>
                                                                                    <td>{item.is_use == true ? 'O' : 'X'}</td>
                                                                                    <td><button onClick={(e) => this.UserDetailOpen(e, index)}>수정</button><button onClick={(e) => this.removeUser(e, item)}>삭제</button></td>
                                                                                </tr>
                                                                            )
                                                                        }) : <tr>
                                                                            <td></td><td></td>
                                                                            <td>데이터가 없습니다</td></tr>
                                                                }
                                                            </tbody>
                                                        </table>
                                                        {this.state.usercount && (this.state.usercount > this.state.size) ?
                                                            <Pagination count={this.state.usercount}
                                                                offset={this.state.offset}
                                                                size={this.state.size}
                                                                searchPage={this.getuser} />
                                                            : null
                                                        }
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </React.Fragment>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useSocket(
    ({ state, actions }) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useuser(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close,
        setuserCallback: actions.setuserCallback,
    })
)(useUserAdd(
    ({ actions }) => ({
        UserAddOpen: actions.open
    })
)(useHouse(
    ({ actions }) => ({
        getHouseId: actions.getHouseId,
    })
)(useUserDetail(
    ({ actions }) => ({
        UserDetailOpen: actions.open,
    })
)(ModalUser))))));
