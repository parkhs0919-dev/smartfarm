import React, {Component} from 'react';
import {useHouse} from "../../contexts/house";
import {useAlert} from "../../contexts/alert";
import {usePanelSoundSetting} from "../../contexts/modals/panelSoundSetting";
import deepcopy from "../../../../utils/deepcopy";
import Select from "react-select";
import {colourStyles} from "../../utils/select";
import controlManager from "../../managers/control";
import controlAlarmManager from "../../managers/controlAlarm";
import {noSession} from "../../utils/session";
import CONSTANT from "../../constants/constant";

const CONTROL_STATE_OPTIONS = CONSTANT.CONTROL_OPTIONS.ALARM_STATE;
const RETURN_ALARM_STATE = CONSTANT.CONTROL_OPTIONS.RETURN_ALARM_STATE;
const DEFAULT_STATE = {
    id : '',
    control :null,
    type : null,
    text : '',
    is_update : false,
};

class ModalPanelSoundSetting extends Component {

    constructor(props) {
        super(props);
        this.state = deepcopy(DEFAULT_STATE);
    };

    componentDidMount() {
        this.props.sync(this.init);
    };

    init = (data) => {
        this.findAllControls();
        document.addEventListener('keyup', this.closeOnEscape);

        if(data) {
            let obj = {
                id : data.id,
                control : {
                    value : data.control_id,
                    label : data.control.control_name,
                    origin : data.control
                },
                type : {
                    value : data.state,
                    label : RETURN_ALARM_STATE[data.state]
                },
                text : data.text,
                is_update : true
            }

            this.setState(obj);
        }
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(DEFAULT_STATE);
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findAllControls = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            types: 'motor,power'
        };
        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    controls: this.generateControlOptions(data.data.rows)
                });
            } else if (status === 404) {
                _this.setState({
                    controls: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    generateControlOptions = (data) => {
        let controls = [];
        data.forEach((item, index) => {
            let obj = {};
            obj.label = item.control_name;
            obj.value = item.id;
            obj.origin = item;
            controls.push(obj);
        })
        return controls;
    };

    handleSelectControls = (value, type) => {
        if(type === 'control'){
            this.setState({type : null});
            this.setState({control : value})
        }else if(type === 'type'){
            this.setState({type : value})
        }
    };

    handleInput = (e, type) => {
        this.setState({text: e.target.value});
    };

    submit = () => {
        const _this = this;

        if(!this.state.control) {
            return _this.props.showDialog({
                alertText: '장치를 선택해주세요.'
            });
        }

        if(!this.state.type) {
            return _this.props.showDialog({
                alertText: '동작을 선택해주세요.'
            });
        }

        if(!this.state.text) {
            return _this.props.showDialog({
                alertText: '음성 텍스트를 입력해주세요.'
            });
        }

        this.dupleCheck();

        let query = {
            control_id : this.state.control.value,
            state : this.state.type.value,
            text : this.state.text
        };

        if(this.state.is_update){
            query.id = this.state.id;
            controlAlarmManager.update(query, noSession(this.props.showDialog)((status, data) => {
                if (status === 200) {
                    _this.props.showDialog({
                        alertText: "장치 제어 알람이<br/>수정되었습니다.",
                        cancelCallback: () => {
                            _this.close();
                        }
                    })
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        }else{
            controlAlarmManager.create(query, noSession(this.props.showDialog)((status, data) => {
                if (status === 200) {
                    _this.props.showDialog({
                        alertText: "장치 제어 알람이<br/>등록되었습니다.",
                        cancelCallback: () => {
                            _this.close();
                        }
                    })
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        }
    };

    dupleCheck = () => {
        const _this = this;
        let query = {
            control_id : this.state.control.value,
            state : this.state.type.value
        };

        controlAlarmManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200){
                return _this.props.showDialog({
                    alertText: '중복된 데이터는<br/>등록할 수 없습니다.'
                });
            }else if(status === 404){
                return true;
            }else{
                _this.props.alertError(status, data);
            }
        }));
    }

    render() {
        return (
            <article id="lcModalPanelSoundSettingModalWrap"
                     className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcPanelSoundSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">장치제어 알람 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>
                                <section className="modal-body">
                                    <div className="lc-group-setting">
                                        <div className="lc-group-setting-menu">
                                            <p className="lc-group-setting-name">장치 선택</p>
                                            <Select
                                                value={this.state.control}
                                                placeholder="장치 선택"
                                                onChange={value => this.handleSelectControls(value, 'control')}
                                                isSearhable={false}
                                                options={this.state.controls}
                                                styles={colourStyles}
                                            />
                                        </div>
                                        <div className="lc-group-setting-menu">
                                            <p className="lc-group-setting-name">동작 선택</p>
                                            <Select
                                                value={this.state.type}
                                                onChange={value => this.handleSelectControls(value, 'type')}
                                                isSearchable={ false }
                                                options={this.state.control ? CONTROL_STATE_OPTIONS[this.state.control.origin.type] : []}
                                                styles={colourStyles}/>
                                        </div>

                                    </div>
                                    <div className="lc-group-setting-detail">
                                        <p className="lc-group-setting-name">음성 텍스트 설정(최대 20자)</p>
                                        <div className="input-wrap">
                                            <input type="text"
                                                   value={this.state.text}
                                                   className="lc-farm-input lc-pband-input"
                                                   maxLength={20}
                                                   onChange={e => this.handleInput(e, "text")}/>
                                        </div>
                                    </div>
                                    <div className="button-wrap">
                                        <button className="theme-button" onClick={e =>this.submit()}>{this.state.is_update ? '수정' : '등록'}</button>
                                    </div>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>

            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(usePanelSoundSetting(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalPanelSoundSetting)));
