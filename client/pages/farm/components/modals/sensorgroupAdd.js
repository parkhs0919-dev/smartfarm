import React, { Component } from 'react';
import { useSensorGroupAdd } from "../../contexts/modals/sensorgroupAdd";
import { useAlert } from "../../contexts/alert";
import CONSTANT from "../../constants/constant";
import TRANSLATE from '../../constants/translate';
import { colourStyles } from "../../../farm/utils/select";
import Select from 'react-select';
import sensorGroupmanager from '../../managers/sensorgroup'
import deepcopy from "../../../../utils/deepcopy";
import { noSession } from "../../utils/session";

const TYPE = CONSTANT.sensorGroup.Type;
const SENSOR_TYPE_TRANSLATE = TRANSLATE.SENSOR;

const DEFAULT_VALUE = {
    types: [],
    GroupName: '',
    type: { value: 'temperature', label: '온도' }

}

class ModalSensorGroupAdd extends Component {
    constructor(props) {
        super(props);
        this.audio = null
        this.moreMenuRefs = new Map();
        this.state = {
            types: [],
            GroupName: '',
            type: { value: 'temperature', label: '온도' }
        };
    }

    componentDidMount() {
        this.props.sync(this.init);

    }




    init = (data) => {
        let state = Object.assign({}, data);
        
        state.position = data.position;
        state.house_id = data.house_id;

        this.setState(state)

        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if (e) e.preventDefault();
        this.setState(DEFAULT_VALUE);
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {
        e.preventDefault();
        const _this = this;
        let state = Object.assign({}, this.state);
        let body = {};
        let items = [];
        let position='';
        if (!state.GroupName) {
            return this.props.showDialog({
                alertText: "그룹명을 입력해 주세요."
            });
        }

        for (let i = 0; i < state.types.length; i++) {
            if (state.types[i].check) {
                items.push(state.types[i].id)
            }
        }
        if (items.length == 0) {
            return this.props.showDialog({
                alertText: "센서를 선택해 주세요."
            });
        }
       
        if(this.state.position =='outer'){
            position = 'out';
        }else if (this.state.position =='inner'){
            position ='in';
        }
        body.position = position;
        body.type = state.type.value;        
        body.name = state.GroupName;
        body.items = items.join(',');
        body.house_id = state.house_id;
        
        sensorGroupmanager.create(body, noSession(this.props.showDialog)((status, data) => {            
            if (status === 200) {
                _this.props.showDialog({
                    alertText: "센서 그룹 설정이 등록되었습니다.",
                    cancelCallback: () => {

                        _this.close();
                    }
                })
            } else {
                this.props.alertError(status, data);
            }
        }));
    };

    handleSelect = (option, key) => {

        let temp = Object.assign({}, this.state);
        temp[key] = option;
        this.setState(temp);
    };

    handleGroupName = (e) => {
        this.setState({
            GroupName: e.target.value
        });
    }

    afterAlarmSetting = () => {
        document.addEventListener('keyup', this.closeOnEscape);
    };

    checksensorDetail = (e, index) => {
        let types = deepcopy(this.state.types);
        if (types[index].check == undefined) {
            types[index].check = true
            this.setState({ types })
        } else {
            types[index].check = !types[index].check
            this.setState({ types });
        }
    };

    typesubmit = (e) => {
        let position = '';
        if (!this.state.type) {
            return this.props.showDialog({
                alertText: "타입을 선택해 주세요."
            });
        }
        if (this.state.position) {
            if (this.state.position == 'outer') {
                position = 'out';
            } else if (this.state.position == 'inner') {
                position = 'in';
            }
        }
        let body = {            
            house_id: this.state.house_id,
            type: this.state.type.value,
            position:position
        }
        sensorGroupmanager.gettype(body, noSession(this.props.showDialog)((status, data) => {
            
            if (status === 200) {                
                if (data.data.length > 0) {
                    this.setState({
                        types: data.data
                    })
                } else {
                    this.setState({
                        types: []
                    })
                }
            } else {
                this.props.alertError(status, data);
            }
        }));
    }
    render() {
        return (
            <article id="lcModalSensorAlarmWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalSensorGroup" className="modal-wrap">
                                <section className="modal-header">

                                    <h3 className="lc-modal-title">{SENSOR_TYPE_TRANSLATE[this.state.position]}그룹 설정 추가</h3>

                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>

                                <section className="modal-body">

                                    <React.Fragment>
                                        <div className="sensorGroup-register-wrap">
                                            <div id="sensorGroupWrap" className="sensorGroup-section">
                                                <label className="sensorGroup-form-label">센서 타입</label>
                                                <ul className="sensorGroup-wrap">

                                                    <li className="sensorGroup-item">
                                                        <div className="select-module-wrap">
                                                            <Select
                                                                value={this.state.type}
                                                                placeholder="센서 선택"
                                                                onChange={option => this.handleSelect(option, "type")}
                                                                isSearchable={false}
                                                                options={TYPE}
                                                                styles={colourStyles} />
                                                        </div>
                                                    </li>
                                                    <li className="sensorGroup-item">
                                                        <div className="select-module-wrap">
                                                            <button className="theme-button" onClick={e => this.typesubmit(e)}>검색</button>
                                                        </div>
                                                    </li>


                                                </ul>

                                            </div>
                                        </div>
                                        <div className="sensorGroup-register-wrap">
                                            <form onSubmit={this.submit}>

                                                <div id="sensorGroupSettingWrap" className="sensorGroup-section">
                                                    <div className="sensorGroup-setting-form">
                                                        <label className="sensorGroup-form-label">그룹명</label>
                                                        <div className="lc-setting-form-wrap">
                                                            <div className="form-flex-wrap">
                                                                <div className="left-item select-module-wrap lc-size-36">
                                                                    <input type="text" value={this.state.GroupName}
                                                                        onChange={(e) => this.handleGroupName(e)} className="lc-farm-input"
                                                                        placeholder="그룹명" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="delay-recount-wrap">
                                                            <label className="sensorGroup-form-label">센서 선택</label>
                                                        </div>
                                                        <div className="delay-recount-wrap">
                                                            <ul id="sensorGroupDetailList">
                                                                {
                                                                    this.state.types && this.state.types.length ?
                                                                        this.state.types.map((rows, index) => {
                                                                            return (
                                                                                <li className="sensorGroup-detail-list-item" key={index}>
                                                                                    <article className="check-wrap">
                                                                                        <div className="lc-theme-checkbox-wrap">
                                                                                            <input
                                                                                                checked={rows.check ? rows.check : false}
                                                                                                id={rows.id}
                                                                                                onChange={() => { }} type="checkbox"
                                                                                                name="groupDetailWay" />
                                                                                            <label htmlFor="isBar"
                                                                                                onClick={(e) => this.checksensorDetail(e, index)}
                                                                                                className="box-shape-label" />
                                                                                            <label htmlFor="isBar"
                                                                                                onClick={(e) => this.checksensorDetail(e, index)}
                                                                                                className="text-label">{rows.sensor_name}</label>
                                                                                        </div>
                                                                                    </article>
                                                                                </li>
                                                                            )
                                                                        }) : <p>그룹 설정이 가능한 센서가 없습니다.</p>
                                                                }
                                                            </ul>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div className="delay-recount-wrap">
                                                    <button className="theme-button" type="submit">등록</button>
                                                </div>
                                            </form>
                                        </div>
                                    </React.Fragment>

                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }

}

export default useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSensorGroupAdd(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSensorGroupAdd));
