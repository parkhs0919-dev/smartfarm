import React, {Component} from 'react';
import {useHouse} from "../../contexts/house";
import {useAlert} from "../../contexts/alert";
import {usePanelSound} from "../../contexts/modals/panelSound";
import {usePanelSoundSetting} from "../../contexts/modals/panelSoundSetting";
import houseManager from "../../managers/houses";
import deepcopy from "../../../../utils/deepcopy";
import {noSession} from "../../utils/session";
import controlAlarmManager from "../../managers/controlAlarm";
import CONSTANT from "../../constants/constant";

const RETURN_ALARM_STATE = CONSTANT.CONTROL_OPTIONS.RETURN_ALARM_STATE;

const DEFAULT_STATE = {
    is_use_alarm : false,
    controlAlarms : [],
};

class ModalPanelSound extends Component {

    constructor(props) {
        super(props);
        this.state = deepcopy(DEFAULT_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        document.addEventListener('keyup', this.closeOnEscape);
        this.getControlAlarms();
        this.getHouses();
    };

    close = (e) => {
        if (e) e.preventDefault();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    clickOption = (e, value) => {
        const _this = this;

        let body = {
            id : this.props.getHouseId(),
            is_use_alarm : value
        };

        houseManager.updateAlarmOption(body, noSession(this.props.show)((status, data) => {
            if(status === 200){
                _this.props.showDialog({
                    alertText: "변경이 완료 되었습니다."
                });
                this.setState({is_use_alarm : value});
            } else {
                _this.props.showDialog({
                    alertText: "변경이 실패하였습니다.",
                });
            }
        }));
    };

    openPanelSoundSettingModal = (e, index) => {
        this.props.openPanelSoundSettingModal(this.state.controlAlarms[index], this.setPanelSoundSetting);
    };

    removePanelSoundSetting = (e, index) => {
        const _this = this;
        const controlAlarm = deepcopy(this.state.controlAlarms[index]);
        this.props.showDialog({
            alertText: "장치 제어 알람을 삭제하시겠습니까?",
            cancelText: "취소",
            submitText: "확인",
            submitCallback: () => {
                controlAlarmManager.remove(controlAlarm.id, noSession(this.props.showDialog)((status, data) => {
                    if(status === 200) {
                        _this.props.showDialog({
                            alertText: "삭제되었습니다.",
                        });
                        _this.getControlAlarms();
                    } else {
                        _this.props.alertError(status, data);
                    }
                }));
            }
        })
    };

    getControlAlarms = () => {
        const _this = this;
        controlAlarmManager.findAll({}, noSession(this.props.showDialog)((status, data) => {
            if(status === 200){
                _this.setState({controlAlarms : data.data.rows});
            }else if(status === 404){
                _this.setState({controlAlarms : []});
            }else{
                _this.props.alertError(status, data);

            }
        }));
    };

    setPanelSoundSetting = () => {
        this.getControlAlarms();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    getHouses = () => {
        const _this = this;
        houseManager.findById({id :this.props.getHouseId()}, noSession(this.props.show)((status, data) => {
            if(status === 200){
                this.setState({is_use_alarm : data.data.is_use_alarm});
            } else {
                _this.props.alertError(status, data);
            }
        }));
    }

    render() {
        return (
            <article id="lcModalPanelSoundModalWrap"
                     className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcPanelSound" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">패널 소리 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>
                                <section className="modal-body">
                                    <div className="use-panel-sound-wrap">
                                        <label className="use-panel-sound-title">센서 경보 알람 소리 설정</label>

                                        <div className="toggle-radio-wrap two-state">
                                            <input type="radio" name={`control-toggle`} onChange={e => {}}
                                                   checked={this.state.is_use_alarm === true} id={`remoteToggleFirst`}/>
                                            <input type="radio" name={`control-toggle`} onChange={e => {}}
                                                   checked={this.state.is_use_alarm === false} id={`remoteToggleSecond`} />

                                            <label onClick={e => this.clickOption(e, true)}>ON</label>
                                            <label onClick={e => this.clickOption(e, false)}>OFF</label>
                                            <i className="toggle-slider-icon"/>
                                        </div>
                                    </div>
                                    <div className="panel-sound-wrap">
                                        <div className="button-wrap">
                                            <button className="theme-button" onClick={e => this.openPanelSoundSettingModal(e)}>장치제어 알람 설정</button>
                                        </div>
                                        <div className="panel-sound-setting-list">
                                            <ul>
                                                {this.state.controlAlarms.length ?
                                                    this.state.controlAlarms.map((data, index) => {
                                                        return (
                                                            <li key={index}>
                                                            <p className="lc-wind-direction-name-wrap">
                                                                <span className="lc-wind-direction-name-index">{index+1}</span>
                                                                <span className="lc-wind-direction-name">{`${data.control.control_name} / ${RETURN_ALARM_STATE[data.state]}`}</span>
                                                            </p>
                                                            <div className="three-dot-menu" tabIndex="0">
                                                                <div className="icon-wrap">
                                                                    <i/><i/><i/>
                                                                </div>
                                                                <div className="three-dot-view-more-menu">
                                                                    <ul>
                                                                        <li onClick={e => this.openPanelSoundSettingModal(e, index)}><span>수정</span></li>
                                                                        <li onClick={e => this.removePanelSoundSetting(e, index)}><span>삭제</span></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        )
                                                    })
                                                    :  <li className="empty-list-item">등록된 장치제어 알람이 없습니다.</li>
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>

            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(usePanelSound(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(usePanelSoundSetting(
    ({actions}) => ({
        openPanelSoundSettingModal: actions.open
    })
)(ModalPanelSound))));
