import React, { Component } from 'react';
import { usePesticide } from "../../contexts/modals/pesticide";
import { useSocket } from "../../contexts/socket";
import { useAlert } from "../../contexts/alert";
import CONSTANT from "../../constants/constant";
import pesticideManager from '../../managers/pesticide';
import deepcopy from "../../../../utils/deepcopy";
import { noSession } from "../../utils/session";

const DEFAULT_AREA = CONSTANT.defaultpesticide.area
const CAN_MODIFY_ORDER = CONSTANT.defaultpesticide.canModifyChild;

const DEFAULT_VALUE = {
    pesticide_name: '',
    Section_Time: '',
    area: DEFAULT_AREA,
    FrostControl_Time: '',
    drugControl_Time: '',
    house_id: '',
}

class Modalpesticide extends Component {
    constructor(props) {
        super(props);
        this.audio = null
        this.moreMenuRefs = new Map();
        this.state = {
            pesticide_name: '',
            Section_Time: '',
            area: DEFAULT_AREA,
            FrostControl_Time: '',
            drugControl_Time: '',
            house_id: '',
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        this.setState({ house_id: data })
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if (e) e.preventDefault();
        this.setState(DEFAULT_VALUE);
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {
        e.preventDefault();
        const _this = this;
        let state = Object.assign({}, this.state);
        let body = {};
        let order = [];
        let is_visible =[];
        if (!state.pesticide_name) {
            return this.props.showDialog({
                alertText: "방제명을 입력해 주세요."
            });
        }
        if (!state.Section_Time) {
            return this.props.showDialog({
                alertText: "구역벨브 시간을 입력해 주세요."
            });
        }
        if (!state.drugControl_Time) {
            return this.props.showDialog({
                alertText: "약제방제 모터 동작시간을 입력해 주세요."
            });
        }
        if (!state.FrostControl_Time) {
            return this.props.showDialog({
                alertText: "서리방제 벨브 개폐시간을 입력해 주세요."
            });
        }
        if (!state.house_id) {
            return this.props.showDialog({
                alertText: "하우스 정보를 확인해 주세요."
            });
        }
        
        for (let i = 0; i < state.area.length; i++) {
            order.push(state.area[i].order)
            is_visible.push(state.area[i].is_visible)
            
        }


        body.pest_control_name = state.pesticide_name;
        body.zone_time = Number(state.Section_Time);
        body.medicine_time = Number(state.drugControl_Time);
        body.compressor_time = Number(state.FrostControl_Time);
        body.house_id = state.house_id;
        body.zone_list = order.join(',');
        body.zone_use_list = is_visible.join(',');
        
        pesticideManager.create(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                _this.props.alertError(status, data);
            } else if (status == 200) {
                _this.props.showDialog({
                    alertText: "방제기 설정이 등록되었습니다.",
                    cancelCallback: () => {

                        _this.close();
                    }
                })
            }
        }));
    };

    pesticidename = (e) => {
        if (e.target.value.length < 50) {
            this.setState({
                pesticide_name: e.target.value
            });
        }
    }
    SectionTime = (e) => {
        if (e.target.value.length < 10) {
            this.setState({
                Section_Time: e.target.value
            });
        }
    }
    FrostControlTime = (e) => {
        if (e.target.value.length < 10) {
            this.setState({
                FrostControl_Time: e.target.value
            });
        }
    }
    drugControlTime = (e) => {
        if (e.target.value.length < 10) {
            this.setState({
                drugControl_Time: e.target.value
            });
        }
    }

    afterAlarmSetting = () => {
        document.addEventListener('keyup', this.closeOnEscape);
    };

    changeOrder = (e, index, direction) => {
        e && e.preventDefault();
        let area = deepcopy(this.state.area);
        if (direction === "down") {
            // area[index].order = area[index].order + 1;
            // area[index + 1].order = area[index + 1].order - 1;
            area.splice(index + 1, 0, area.splice(index, 1)[0]);

        } else if (direction === "up") {
            // area[index].order = area[index].order - 1;
            // area[index - 1].order = area[index - 1].order + 1;
            area.splice(index - 1, 0, area.splice(index, 1)[0]);
        }

        this.setState({ area });
    };
    clickCheckBox = (e, item, index) => {
        let area = deepcopy(this.state.area);
        area[index].is_visible = !area[index].is_visible;
        this.setState({area});
    };
    render() {
        return (
            <article id="lcModalpesticideWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalpesticide" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">방제기 설정 등록</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>
                                <section className="modal-body">
                                    <React.Fragment>
                                        <div className="pesticide-register-wrap">
                                            <div id="pesticideNameWrap" className="pesticide-form-section">
                                                <label className="pesticide-form-label">방제명</label>
                                                <input type="text" value={this.state.pesticide_name}
                                                    onChange={this.pesticidename} className="lc-farm-input"
                                                    placeholder="방제명" />

                                            </div>
                                        </div>
                                        <div className="pesticide-register-wrap">
                                            <label className="pesticide-form-label">구간밸브 동작 시간</label>
                                            <div className="lc-setting-form-wrap">
                                                <div className="form-flex-wrap">
                                                    <div
                                                        className="left-item select-module-wrap lc-size-36">
                                                        <input type="text" value={this.state.Section_Time}
                                                            onChange={this.SectionTime} className="lc-farm-input"
                                                            placeholder="초" />
                                                    </div>
                                                    <span className='lc-modifier'>초</span>
                                                </div>
                                            </div>
                                            <label className="pesticide-form-label">약재방제 모터 동작시간</label>
                                            <div className="lc-setting-form-wrap">
                                                <div className="form-flex-wrap">
                                                    <div
                                                        className="left-item select-module-wrap lc-size-36">
                                                        <input type="text" value={this.state.drugControl_Time}
                                                            onChange={this.drugControlTime} className="lc-farm-input"
                                                            placeholder="초" />
                                                    </div>
                                                    <span className='lc-modifier'>초</span>
                                                </div>
                                            </div>
                                            <label className="pesticide-form-label">서리방제 벨브 개폐시간</label>
                                            <div className="lc-setting-form-wrap">
                                                <div className="form-flex-wrap">
                                                    <div
                                                        className="left-item select-module-wrap lc-size-36">
                                                        <input type="text" value={this.state.FrostControl_Time}
                                                            onChange={this.FrostControlTime} className="lc-farm-input"
                                                            placeholder="초" />

                                                    </div>
                                                    <span className='lc-modifier'>초</span>
                                                </div>
                                            </div>
                                            <ul id="screenOrderList">
                                                <div className="pesticide-screen-section">
                                                    <label className="pesticide-form-label">구역설정</label>
                                                </div>
                                                {
                                                    this.state.area.map((item, index) => {

                                                        return (
                                                            <li className="screen-order-list-item" key={index}>
                                                                <article
                                                                    className={"screen-setting-wrap" + (item.isTabOpen ? ' lc-open' : '')}>
                                                                    <div className="screen-setting-left-wrap">
                                                                        <div className="lc-theme-checkbox-wrap">
                                                                        <input id="mixControlCondition1"
                                                                           value="time"
                                                                           checked={item.is_visible}
                                                                           onChange={() => {
                                                                           }} type="checkbox"
                                                                           name="mix-control-condition"/>
                                                                    <label
                                                                        htmlFor="mixControlCondition1"
                                                                        onClick={(e) => this.clickCheckBox(e, item, index)}
                                                                        className="box-shape-label"/>
                                                                    <label
                                                                        htmlFor="mixControlCondition1"
                                                                        onClick={(e) => this.clickCheckBox(e, item, index)}
                                                                        className="text-label">{item.label}</label>
                                                                        </div>
                                                                        {
                                                                            CAN_MODIFY_ORDER[item.type] ?
                                                                                (
                                                                                    <button
                                                                                        className={"open-menu-btn" + (item.isTabOpen ? ' lc-open' : '')}
                                                                                        onClick={e => this.toggleMenu(e, item, index)} />
                                                                                ) : null
                                                                        }
                                                                    </div>

                                                                    <div className="screen-setting-right-wrap">
                                                                        {
                                                                            index !== this.state.area.length - 1 &&
                                                                            (
                                                                                <button className="order-btn order-down"
                                                                                    onClick={e => this.changeOrder(e, index, 'down')} />
                                                                            )
                                                                        }
                                                                        {
                                                                            index !== 0 &&
                                                                            (
                                                                                <button className="order-btn order-up"
                                                                                    onClick={e => this.changeOrder(e, index, 'up')} />
                                                                            )
                                                                        }
                                                                    </div>
                                                                </article>
                                                                {
                                                                    CAN_MODIFY_ORDER[item.type] && item.isTabOpen ?
                                                                        (
                                                                            <article className="lc-screen-menu-child-wrap">
                                                                                {this.generateChildContent(item.type)}
                                                                            </article>
                                                                        ) : null
                                                                }
                                                            </li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                            <div className="delay-recount-wrap">
                                                <button className="theme-button" onClick={e => this.submit(e)}>등록</button>
                                            </div>

                                        </div>



                                    </React.Fragment>

                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }

}

export default useSocket(
    ({ state, actions }) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(usePesticide(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(Modalpesticide)));
