import React, {Component} from 'react';

import {useSunDateDefaultSetting} from "../../contexts/modals/sunDateDefaultSetting";
import {useSunDate} from "../../contexts/sunDate";
import {useAlert} from "../../contexts/alert";
import {attachZero} from "../../../../utils/filter";
import sunDateManager from '../../managers/sunDate';
import sunDateDefaultSettingManager from '../../managers/sunDateDefault';
import {noSession} from "../../utils/session";

class ModalSunDateDefaultSetting extends Component {
    constructor(props) {
        super(props);
        this.sunDateInterval = null;
        this.state = {
            rise_hour: '',
            rise_minute: '',
            set_hour: '',
            set_minute: '',
        };
    }

    componentDidMount() {
        this.findSunDate();
        this.sunDateInterval = setInterval(() => this.findSunDate(), (60 * 60 * 1000));
        this.props.sync(this.init);
    }

    componentWillUnmount() {
        clearInterval(this.sunDateInterval);
    }

    findSunDate = () => {
        const _this = this;
        sunDateManager.findAll(noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.props.setSunDate(data.data);
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    init = () => {
        this.findSunDateDefault();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {
        e.preventDefault();
        const _this = this;

        let body = {
            rise_hour: this.state.rise_hour,
            rise_minute: this.state.rise_minute,
            set_hour: this.state.set_hour,
            set_minute: this.state.set_minute,
        };

        sunDateDefaultSettingManager.updateDefault(body, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.findSunDate();
                _this.props.showDialog({
                    alertText: "설정되었습니다.",
                    cancelCallback: () => {
                        _this.close();
                    },
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    handleInput = (e, key) => {
        let temp = {};
        temp[key] = e.target.value;
        this.setState(temp);
    };

    findSunDateDefault = () => {
        const _this = this;
        let query = {};
        sunDateDefaultSettingManager.getDefault(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState(data.data);
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {
        return (
            <article id="lcModalSunDateDefaultSettingWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalSunDateDefaultSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">기본 일출/일몰 시간설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div className="setting-wrap">
                                        <span className="setting-title">기본 일출 시간</span>
                                        <div className="setting-row">
                                            <input type="number" value={this.state.rise_hour} onChange={e => this.handleInput(e, "rise_hour")} className="lc-farm-input"/>
                                        </div>
                                    </div>
                                    <div className="setting-wrap">
                                        <span className="setting-title">기본 일출 분</span>
                                        <div className="setting-row">
                                            <input type="number" value={this.state.rise_minute} onChange={e => this.handleInput(e, "rise_minute")} className="lc-farm-input"/>
                                        </div>
                                    </div>
                                    <div className="setting-wrap">
                                        <span className="setting-title">기본 일몰 시간</span>
                                        <div className="setting-row">
                                            <input type="number" value={this.state.set_hour} onChange={e => this.handleInput(e, "set_hour")} className="lc-farm-input"/>
                                        </div>
                                    </div>
                                    <div className="setting-wrap">
                                        <span className="setting-title">기본 일몰 분</span>
                                        <div className="setting-row">
                                            <input type="number" value={this.state.set_minute} onChange={e => this.handleInput(e, "set_minute")} className="lc-farm-input"/>
                                        </div>
                                    </div>

                                    {/*<p>현재 설정된 일출 기본 설정값은 {attachZero(this.state.rise_hour)}시 {attachZero(this.state.rise_minute)}분 입니다.</p>*/}
                                    {/*<p>현재 설정된 일몰 기본 설정값은 {attachZero(this.state.set_hour)}시 {attachZero(this.state.set_minute)}분 입니다.</p>*/}
                                    <p>일출, 일몰 시간은 지역별 기상청 일출, 일몰 데이터를 기준으로 하며,</p>
                                    <p>통신 장애가 있을 시 위에 입력된 기본 설정값을 적용합니다.</p>

                                    <button className="theme-button" onClick={this.submit}>적용</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useSunDate(
    ({actions}) => ({
        setSunDate: actions.setSunDate,
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSunDateDefaultSetting(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSunDateDefaultSetting)));
