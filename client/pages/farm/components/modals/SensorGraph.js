import React, {Component, Fragment} from 'react';
import {useSensorGraph} from "../../contexts/modals/sensorGraph";
import {useSocket} from "../../contexts/socket";
import {useAlert} from "../../contexts/alert";
import { useHouse } from "../../contexts/house";

import Spinner from '../../components/Spinner';
import LcChart from '../Chart';
import sensorManager from '../../managers/sensor';
import DatePicker from 'react-datepicker';
import moment from "moment";
import {date} from "../../../../utils/filter";
import deepcopy from '../../../../utils/deepcopy';
import {noSession} from "../../utils/session";
import sensorMainChartManager from "../../managers/sensorMainChart";

const DATE_DISPLAY_FORMAT = "yyyy-MM-dd";
const NO_DATE_RANGE = {
    isDate: false,
    isWeek: false,
    isMonth: false,
};
const DEFAULT_STATE = {
    sensor: {},
    chartData: [],
    dateRange: {
        isDate: true,
        isWeek: false,
        isMonth: false,
    },
    startDate: null,
    endDate: null,
    loading: true,
    datePickerOpenState: {
        startDate: false,
        endDate: false,
    },
    sensorZoom: true,
};

class ModalSensorGraph extends Component {
    constructor(props) {
        super(props);

        this.state = deepcopy(DEFAULT_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    componentWillUnmount() {
        if (this.chart) {
            this.chart.dispose();
        }
    }

    init = (data) => {
        const _this = this;
        this.setState({
            sensor: data
        }, () => {
            _this.findSensorCharts();
        });
        document.addEventListener('keyup', this.closeOnEscape);
    };

    findSensorCharts = () => {
        const _this = this;
        let query = {
            sensor_id: this.state.sensor.id,
        };
        let dateRange = deepcopy(this.state.dateRange);
        for(let range in dateRange) {
            if(dateRange[range]) {
                query[range] = true;
            }
        }

        if(this.state.startDate && this.state.endDate) {
            query.startDate = date(new Date(this.state.startDate), DATE_DISPLAY_FORMAT);
            query.endDate = date(new Date(this.state.endDate), DATE_DISPLAY_FORMAT);
        }

        this.setState({
            chartData: []
        });


        sensorManager.findSensorChart(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    chartData: data.data
                }, () => {
                    setTimeout(() => {
                        _this.setState({
                            loading: false
                        })
                    }, 1000)
                });
            } else if (status === 404) {
                _this.setState({
                    chartData: null
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    handleDate = (val, key) => {
        const _this = this;
        let temp = {};
        let dateRange = deepcopy(NO_DATE_RANGE);
        let selectDate = moment(date(new Date(val), 'yyyy-MM-dd'));
        temp[key] = selectDate;
        temp.dateRange = dateRange;
        this.setState(temp, () => {
            if(this.state.startDate && this.state.endDate) {
                _this.findSensorCharts();
            }
        });
        this.handleDatePickerState(null, key);
    };

    handleDatePickerState = (e, key) => {
        e && e.preventDefault();
        let datePickerOpenState = deepcopy(this.state.datePickerOpenState);
        datePickerOpenState[key] = !datePickerOpenState[key];
        this.setState({datePickerOpenState});
    };

    closeDatePickerOnTouchOutside = (e, key) => {
        let datePickerOpenState = deepcopy(this.state.datePickerOpenState);
        datePickerOpenState[key] = false;
        this.setState({datePickerOpenState});
    };

    selectDateRange = (e, key) => {
        const _this = this;
        let temp = {
            startDate: null,
            endDate: null
        };
        let dateRange = deepcopy(this.state.dateRange);
        for(let range in dateRange) {
            if(range === key) {
                if(dateRange[range] === true) {
                    return;
                } else {
                    dateRange[range] = true;
                }
            } else {
                dateRange[range] = false;
            }
        }
        temp.dateRange = dateRange;
        this.setState(temp, () => {
            _this.findSensorCharts();
        });
    };

    sensorChartZoom = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            sensor_id: this.state.sensor.id,
        };
        if(this.state.sensorZoom) {
            this.setState({sensorZoom: false});
            this.findSensorCharts();
        }
        if(!this.state.sensorZoom) {
            this.setState({sensorZoom: true});
            this.findSensorCharts();
        }
    };

    render() {
        return (
            <article id="lcModalSensorGraphWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalSensorGraph" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">{this.state.sensor.sensor_name} 그래프</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body lc-graph-modal">
                                    <section className="options-wrap">
                                        <div className="date-range-button-wrap">
                                            <button className={"date-range-button" + (this.state.dateRange.isDate ? ' lc-active' : '')} onClick={e => this.selectDateRange(e, "isDate")}>1일</button>
                                            <button className={"date-range-button" + (this.state.dateRange.isWeek ? ' lc-active' : '')} onClick={e => this.selectDateRange(e, "isWeek")}>1주</button>
                                            <button className={"date-range-button" + (this.state.dateRange.isMonth ? ' lc-active' : '')} onClick={e => this.selectDateRange(e, "isMonth")}>1개월</button>
                                        </div>
                                    </section>
                                    <section className="graph-wrap">
                                        <div className="graph-wrap-header">
                                            <span className="date-range-label">기간 선택</span>
                                            <div className="date-picker-wrap start">

                                                <label className="toggle-date-picker-label" onClick={e => this.handleDatePickerState(e, "startDate")}>
                                                    <input type="text" readOnly value={this.state.startDate ? date(this.state.startDate, DATE_DISPLAY_FORMAT) : ''}/>
                                                </label>
                                                {
                                                    this.state.datePickerOpenState.startDate && (
                                                        <DatePicker dateFormat={"YYYY-MM-DD"}
                                                                    className="lc-farmlabs-datepicker"
                                                                    popperClassName="lc-farmlabs-datepicker-popper"
                                                                    onClickOutside={e => this.closeDatePickerOnTouchOutside(e, 'startDate')}
                                                                    selected={this.state.startDate}
                                                                    maxDate={this.state.endDate ? moment(date(new Date(this.state.endDate), DATE_DISPLAY_FORMAT)) : undefined}
                                                                    withPortal
                                                                    inline
                                                                    onChange={val => this.handleDate(val, "startDate")}/>
                                                    )
                                                }
                                            </div>

                                            <div className="date-picker-wrap end">
                                                <label className="toggle-date-picker-label" onClick={e => this.handleDatePickerState(e, "endDate")}>
                                                    <input type="text" readOnly value={this.state.endDate ? date(this.state.endDate, DATE_DISPLAY_FORMAT) : ''}/>
                                                </label>
                                                {
                                                    this.state.datePickerOpenState.endDate && (
                                                        <DatePicker dateFormat={"YYYY-MM-DD"}
                                                                    onClickOutside={e => this.closeDatePickerOnTouchOutside(e, 'endDate')}
                                                                    className="lc-farmlabs-datepicker"
                                                                    popperClassName="lc-farmlabs-datepicker-popper right"
                                                                    selected={this.state.endDate}
                                                                    minDate={this.state.startDate ? moment(date(new Date(this.state.startDate), DATE_DISPLAY_FORMAT)) : undefined}
                                                                    withPortal
                                                                    inline
                                                                    onChange={val => this.handleDate(val, "endDate")}/>
                                                    )
                                                }
                                            </div>
                                        </div>
                                        <button className="card-header-button zoom-graph" onClick={() => this.sensorChartZoom()}>{this.state.sensorZoom ? "세로확대보기" : "가로확대보기"}</button>
                                        <div className="graph-item-wrap">
                                            <Spinner spinning={this.state.loading}/>
                                            {
                                                this.state.chartData ?
                                                (
                                                    <Fragment>
                                                        {
                                                            this.state.chartData.length ? (
                                                                <LcChart chartId="modalSensorGraphContainer"
                                                                         chartData={this.state.chartData}
                                                                         chartType={this.state.sensor.type}
                                                                         sensorZoom={this.state.sensorZoom}
                                                                         modalSlideBar={true}
                                                                         sensorData={this.state.sensor}/>
                                                            ) : null
                                                        }
                                                    </Fragment>
                                                ) :
                                                    (
                                                        <div className="no-chart-data-wrap">
                                                            <p>해당 조건에 그래프 정보가 없습니다.</p>
                                                        </div>
                                                    )
                                            }
                                        </div>
                                    </section>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        setInnerSensorCallback: actions.setInnerSensorCallback,
        getHouseId: actions.getHouseId,
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSensorGraph(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSensorGraph))));