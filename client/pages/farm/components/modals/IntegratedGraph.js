import React, {Component} from 'react';
import {useHouse} from "../../contexts/house";
import {useIntegratedGraph} from "../../contexts/modals/integratedGraph";
import {useAlert} from "../../contexts/alert";
import {attachZero, date} from "../../../../utils/filter";
import DatePicker from "react-datepicker/es";
import moment from "moment";
import deepcopy from "../../../../utils/deepcopy";
import sensorManager from '../../managers/sensor';
import controlManager from '../../managers/control';
import chartManager from "../../managers/chart";
import CONSTANT from '../../constants/constant';
import ButtonDatePicker from '../../components/ButtonDatePicker';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4_lang_ko from '@amcharts/amcharts4/lang/ko_KR';

import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import "core-js/shim";
import sensorMainChartManager from "../../managers/sensorMainChart";
import {noSession} from "../../utils/session";
import {colourStyles} from "../../utils/select";
import Select from "react-select";
import autoControlManager from "../../managers/autoControl";
import {useSunDate} from "../../contexts/sunDate";

const FILTER_UTIL = require('../../../../utils/filter');

am4core.useTheme(am4themes_animated);

const NO_WIND_DIRECTION = CONSTANT.noWindDirectionRequest;
const DATE_DISPLAY_FORMAT = "yyyy-MM-dd";
const NO_DATE_RANGE = {
    isDate: false,
    isWeek: false,
    isMonth: false,
};

const ITEM_TYPE = [
    {label: '선택안함', value: 'none'},
    {label: '센서', value: 'sensor'},
    {label: '모터', value: 'motor'},
    {label: '전원', value: 'power'},
    {label: '설정값', value: 'setting'},

];

let selectHourOptions = [];
let selectMinuteOptions = [];

for (let i = 0; i < 24; i++) {
    let hour = {
        value: i,
        label: `${attachZero(i)}시`
    };
    selectHourOptions.push(hour);
}

for (let i = 0; i < 60; i++) {
    let minute = {
        value: i,
        label: `${attachZero(i)}분`
    };
    selectMinuteOptions.push(minute);
}

const DEFAULT_STATE = {
    sensors: [],
    motors: [],
    powers: [],
    selectTemperatureOptions : [],
    dateRange: {
        isDate: true,
        isWeek: false,
        isMonth: false,
    },
    items: [{
        selectedItem: {label: '선택안함', value: 'none'},
        selectedControl: null,
    }],
    startDate: null,
    endDate: null,
    isGraphRendered: false,
    sensorValueObject: {},
    sensorZoom: true,
    fullScreen: false,
    selectedStartHour : null,
    selectedStartMinute : null,
    selectedEndHour : null,
    selectedEndMinute : null,
};

class ModalIntegratedGraph extends Component {
    constructor(props) {
        super(props);
        this.sensorNameTypes = {};
        this.state = deepcopy(DEFAULT_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        this.findAllSensors();
        this.findAllMotors();
        this.findAllPowers();
        this.getTables();
        this.getSunDate();

        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if (e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findAllSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            types: NO_WIND_DIRECTION,
            isTotal: true
        };

        sensorManager.findAll(query, (status, data) => {
            if (status === 200) {
                _this.setState({
                    sensors: this.generateSelectSensorOption(data.data.rows)
                });
            } else if (status === 404) {
                _this.setState({
                    sensors: []
                })
            } else {
                _this.props.alertError(status, data);
            }
        });
    };

    findAllMotors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            type: 'motor',
        };

        controlManager.findAll(query, (status, data) => {
            if (status === 200) {
                _this.setState({
                    motors: this.generateSelectControlOption(data.data.rows, 'motor')
                });
            } else if (status === 404) {
                _this.setState({
                    motors: []
                })
            } else {
                _this.props.alertError(status, data);
            }
        })
    };

    findAllPowers = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            type: 'power',
        }

        controlManager.findAll(query, (status, data) => {
            if (status === 200) {
                _this.setState({
                    powers: this.generateSelectControlOption(data.data.rows, 'power')
                });
            } else if (status === 404) {
                _this.setState({
                    powers: []
                })
            } else {
                _this.props.alertError(status, data);
            }
        })
    };

    generateSelectSensorOption = (data) => {

        let selectAutoControls = [];
        data.forEach((item, index) => {
            selectAutoControls.push({
                label: item.sensor_name,
                value: item.id,
                origin: item,
                type : 'sensor'
            })
        });
        return selectAutoControls;

    };

    generateSelectControlOption = (data, type) => {

        let selectAutoControls = [];
        data.forEach((item, index) => {
            selectAutoControls.push({
                label: item.control_name,
                value: item.id,
                origin: item,
                type : type
            })
        });
        return selectAutoControls;

    };

    handleDate = (val, key) => {
        let temp = {};
        let dateRange = deepcopy(NO_DATE_RANGE);
        let selectDate = val ? moment(date(new Date(val), 'yyyy-MM-dd')) : null;
        temp[key] = selectDate;
        temp.dateRange = dateRange;
        this.setState(temp);
    };

    selectDateRange = (e, key) => {
        let temp = {
            startDate: null,
            endDate: null
        };
        let dateRange = deepcopy(this.state.dateRange);
        for (let range in dateRange) {
            if (range === key) {
                if (dateRange[range] === true) {
                    return;
                } else {
                    dateRange[range] = true;
                }
            } else {
                dateRange[range] = false;
            }
        }
        temp.dateRange = dateRange;
        this.setState(temp);
    };

    checkSensorItem = (e, index) => {
        let sensors = deepcopy(this.state.sensors);
        sensors[index].isChecked = !sensors[index].isChecked;
        this.setState({sensors});
    };

    getChartData = (e) => {
        const _this = this;
        let query = {};
        let sensorChartDatas = [];
        let chartData = [];
        if (this.state.startDate || this.state.endDate) {
            if (!(this.state.startDate && this.state.endDate)) {
                return this.props.showDialog({
                    alertText: "시작일과 종료일은<br>둘 다 선택해주세요."
                });
            } else {
                query.startDate = date(new Date(this.state.startDate), DATE_DISPLAY_FORMAT);
                query.endDate = date(new Date(this.state.endDate), DATE_DISPLAY_FORMAT);
            }
        } else {
            let dateRange = deepcopy(this.state.dateRange);
            let selectedDateRange = null;
            for (let key in dateRange) {
                if (dateRange[key]) {
                    selectedDateRange = key;
                    query[key] = true;
                }
            }
            if (!selectedDateRange) {
                return this.props.showDialog({
                    alertText: "분석 기간을 선택해주세요."
                });
            }
        }

        if(((this.state.selectedStartHour && !this.state.selectedStartMinute) ||  (!this.state.selectedStartHour && this.state.selectedStartMinute))
            || ((this.state.selectedEndHour && !this.state.selectedEndMinute) ||  (!this.state.selectedEndHour && this.state.selectedEndMinute))) {
            return this.props.showDialog({
                alertText: "시작시간과 종료시간을<br/>둘 다 선택해주세요."
            });
        }else{
            if( this.state.selectedStartHour) query.startHour = this.state.selectedStartHour.value;
            if( this.state.selectedStartMinute) query.startMinute = this.state.selectedStartMinute.value;
            if( this.state.selectedEndHour) query.endHour = this.state.selectedEndHour.value;
            if( this.state.selectedEndMinute) query.endMinute = this.state.selectedEndMinute.value;
        }

        //let sensors = deepcopy(this.state.sensors);
        let sendSensors = [];
        let sendControls = [];
        let sendTemperature = [];
        
        let items = deepcopy(this.state.items);

        if(items.length){
            items.forEach((item) => {
                if(item.selectedControl){
                    let control = item.selectedControl;
                    if (control.type === 'sensor') {
                        let obj = deepcopy(query);
                        obj.sensor_id = control.origin.id;
                        obj.sensor_name = control.origin.sensor_name;
                        obj.type = control.type;
                        obj.unit = control.origin.unit;
                        sendSensors.push(obj);
                    } else if (control.type === 'motor' || control.type === 'power') {
                        let obj = deepcopy(query);
                        obj.control_id = control.origin.id;
                        obj.control_name = control.origin.control_name;
                        obj.type = control.type;
                        obj.unit = control.origin.unit;
                        sendControls.push(obj);
                    } else if (control.type === 'temperature') {
                        let obj = Object.assign( deepcopy(query), control.origin);
                        sendTemperature.push(obj);
                    }
                }
            });
        }

        if (sendSensors.length + sendControls.length + sendTemperature.length === 0) {
            return this.props.showDialog({
                alertText: "조회할 센서를 선택해주세요."
            });
        }

        if (sendSensors.length+sendControls.length+sendTemperature.length > 4) {
            return this.props.showDialog({
                alertText: "통합그래프 센서 조회는<br>최대 4개까지 가능합니다."
            });
        }

        let count = 0;

        if(sendTemperature.length){
            let auto_control_name = '';
            sendTemperature.forEach((autoControl, autoControlIndex) => {
                auto_control_name = autoControl.auto_control_name;

                chartData.push({
                    name : autoControl.auto_control_name,
                    type : 'temperature',
                    unit : '℃',
                    data : null,
                });

                if(chartData.length){
                    chartData[autoControlIndex].data = [];
                    autoControl.autoControlItems.forEach((item, index) =>{
                        let start_time = '';
                        if(item.is_sun_date){
                            start_time = item.sun_date_start_time;
                            start_time = (item.sun_date_start_type === 'rise' ? rise : set) + parseInt(start_time);
                        }else{
                            start_time = parseInt(item.start_hour || 0 )* 60 +  parseInt(item.start_minute || 0);
                        }

                        let startTime = Math.abs(start_time);
                        let created_at = new Date();
                        created_at.setHours(attachZero(parseInt(startTime/60)));
                        created_at.setMinutes(parseInt(startTime%60));

                        chartData[autoControlIndex].data.push({
                            value : item.p_band_temperature ? item.p_band_temperature : item.expect_temperature,
                            created_at : date(created_at, 'yyyy-MM-dd HH:mm:00')
                        });

                        if(index === autoControl.autoControlItems.length-1){
                            chartData[autoControlIndex].data.push({
                                value : item.p_band_temperature ? item.p_band_temperature : item.expect_temperature,
                                created_at : date(created_at, 'yyyy-MM-dd 23:59:59')
                            });
                        }
                    });

                    if(chartData[autoControlIndex].data.length){
                        chartData[autoControlIndex].data.sort(function (a, b) {
                            return a.created_at < b.created_at ? -1 : a.created_at > b.created_at ? 1 : 0;
                        });
                    }

                    if(chartData[autoControlIndex].data[0].created_at !== date(new Date(), 'yyyy-MM-dd 00:00:00')){
                        chartData[autoControlIndex].data.unshift({
                            value : 0,
                            created_at : date(new Date(), 'yyyy-MM-dd 00:00:00')
                        });
                    }
                }

            });
            if(!sendSensors.length && !sendControls.length) this.makeTemperatureChartData(chartData);
        }

        if(sendSensors.length && !sendControls.length){
            sendSensors.forEach((item) => {
                sensorManager.findSensorChart(item, (status, data) => {
                    if (status === 200) {
                        count++;
                        let obj = {
                            id: item.sensor_id,
                            name: item.sensor_name,
                            type: item.type,
                            unit: item.unit,
                            data: data.data
                        };
                        sensorChartDatas.push(obj);
                        if (count === sendSensors.length) {
                            let charts = Array.concat(sensorChartDatas, chartData);
                            this.makeChartData(charts);
                        }
                    } else {
                        _this.props.alertError(status, data);
                    }
                });
            });
        } else if(!sendSensors.length && sendControls.length){
          sendControls.forEach((item) => {
              controlManager.findControlChart(item, (status, data) => {
                  if (status === 200) {
                      count++;
                      let obj = {
                          id: item.control_id,
                          name: item.control_name,
                          type: item.type,
                          data: data.data
                      };
                      sensorChartDatas.push(obj);
                      if (count === sendControls.length) {
                          let charts = Array.concat(sensorChartDatas, chartData);
                          this.makeChartData(charts);
                      }
                  } else {
                      _this.props.alertError(status, data);
                  }
              })
          })
        } else if(sendSensors.length && sendControls.length){
            sendSensors.forEach((sensor, sensorIndex) => {
                sendControls.forEach((control, controlIndex) => {
                    let obj = deepcopy(query);
                    obj.sensor_id = sensor.sensor_id;
                    obj.control_id = control.control_id;
                    chartManager.setChart(obj, (status, data) => {
                        if(status === 200){
                            count++;
                            let obj = {
                                sensor_id: sensor.sensor_id,
                                control_id : control.control_id,
                                sensor_name : sensor.sensor_name,
                                control_name: control.control_name,
                                sensor_type: sensor.type,
                                control_type: control.type,
                                unit: sensor.unit,
                                data: data.data
                            };

                            sensorChartDatas.push(obj);
                        if (count === sendSensors.length*sendControls.length) {
                            let charts = Array.concat(sensorChartDatas, chartData);
                            this.makeIntegratedChartData(charts);
                        }

                        }else{
                            _this.props.alertError(status, data);
                        }
                    })

                })
            })
        }
    };

    getTables = () => {
        //환기 난방설정
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            auto_control_type : 'table',
        };

        autoControlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    selectTemperatureOptions : this.generateSelectData(data.data.rows)
                });
            } else if (status === 404) {
                _this.setState({
                    selectAutoControls: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));

    };

    generateSelectData = (data) => {
        let selectTemperatureOptions = [];
        data.forEach((data, index) => {
            selectTemperatureOptions.push({
                label : data.auto_control_name,
                value : data.id,
                type : 'temperature',
                origin : data
            })
        });
        return selectTemperatureOptions;
    };

    makeIntegratedChartData = (chartDatas) => {
        this.setState({isGraphRendered : true}, () => {
            let chart = am4core.create("multiChartDiv", am4charts.XYChart);
            chart.language.locale = am4_lang_ko;
            chart.colors.step = 5;
            this.sensorNameTypes = this.generateSensorNameType(chartDatas);
            chart.data = this.generateIntegratedChartData(chartDatas);

            let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
            dateAxis.renderer.minGridDistance = 50;

            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.min = -1;
            valueAxis.max = 2;
            valueAxis.strictMinMax = true;

            let series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "value";
            series.dataFields.dateX = "date";
            series.tooltipText = "{tooltipText}";
            series.propertyFields.stroke = "color";

            let yAxesSide = true;
            for (let key in chart.data[0]) {
                if (key !== 'date') {
                    yAxesSide = !yAxesSide;
                    this.createAxisAndSeries(chart, key, key, yAxesSide);
                }
            }
            chart.legend = new am4charts.Legend();
            chart.cursor = new am4charts.XYCursor();

            if(this.state.sunDate){
                let range = dateAxis.createSeriesRange(series);
                range.date = this.state.sunDate.rise_time;
                range.endDate = this.state.sunDate.set_time;
                range.axisFill.fill = am4core.color("#396478");
                range.axisFill.fillOpacity = 0.3;
            }

            if (this.state.sensorZoom) {
                chart.scrollbarX = new am4core.Scrollbar();
                chart.scrollbarX.marginLeft = 0;
                chart.cursor.behavior = "zoomX";
                chart.cursor.lineY.disabled = true;
            }
            if (!this.state.sensorZoom) {
                chart.scrollbarY = new am4core.Scrollbar();
                chart.scrollbarY.marginLeft = 0;
                chart.cursor.behavior = "zoomY";
                chart.cursor.lineX.disabled = true;
            }

        })
    };

    makeChartData = (sensorChartDatas) => {
        this.setState({isGraphRendered: true}, () => {
            let chart = am4core.create("multiChartDiv", am4charts.XYChart);
            chart.language.locale = am4_lang_ko;
            chart.colors.step = 5;
            this.sensorNameTypes = this.generateSensorNameType(sensorChartDatas);
            chart.data = this.generateChartData(sensorChartDatas);

            console.log(chart.data);

            let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
            dateAxis.renderer.minGridDistance = 50;

            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.min = -1;
            valueAxis.max = 2;
            valueAxis.strictMinMax = true;

            let series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "value";
            series.dataFields.dateX = "date";
            series.tooltipText = "{tooltipText}";
            series.propertyFields.stroke = "color";

            let yAxesSide = true;
            for (let key in chart.data[0]) {
                if (key !== 'date') {
                    yAxesSide = !yAxesSide;
                    this.createAxisAndSeries(chart, key, key, yAxesSide);
                }
            }
            chart.legend = new am4charts.Legend();
            chart.cursor = new am4charts.XYCursor();

            if(this.state.sunDate){
                let range = dateAxis.createSeriesRange(series);
                range.date = this.state.sunDate.rise_time;
                range.endDate = this.state.sunDate.set_time;
                range.axisFill.fill = am4core.color("#396478");
                range.axisFill.fillOpacity = 0.3;
            }

            if (this.state.sensorZoom) {
                chart.scrollbarX = new am4core.Scrollbar();
                chart.scrollbarX.marginLeft = 0;
                chart.cursor.behavior = "zoomX";
                chart.cursor.lineY.disabled = true;
            }
            if (!this.state.sensorZoom) {
                chart.scrollbarY = new am4core.Scrollbar();
                chart.scrollbarY.marginLeft = 0;
                chart.cursor.behavior = "zoomY";
                chart.cursor.lineX.disabled = true;
            }
        });
    };

    makeTemperatureChartData = (chartData) => {
        this.setState({isGraphRendered: true}, () => {
            let chart = am4core.create("multiChartDiv", am4charts.XYChart);
            chart.language.locale = am4_lang_ko;
            chart.colors.step = 5;
            this.sensorNameTypes = this.generateSensorNameType(chartData);
            chart.data = this.generateChartData(chartData);
            let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
            dateAxis.renderer.minGridDistance = 60;
            dateAxis.baseInterval = {
                "timeUnit": "minute",
                "count": 1
            };

            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.grid.template.disabled = true;
            valueAxis.min = -1;
            valueAxis.max = 2;
            valueAxis.strictMinMax = true;

            let series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = "value";
            series.dataFields.dateX = "date";
            series.tooltipText = "{tooltipText}";
            series.propertyFields.stroke = "color";

            let yAxesSide = true;
            for (let key in chart.data[0]) {
                if (key !== 'date') {
                    yAxesSide = !yAxesSide;
                    this.createAxisAndSeries(chart, key, key, yAxesSide);
                }
            }
            chart.legend = new am4charts.Legend();
            chart.cursor = new am4charts.XYCursor();

            if(this.state.sunDate){
                let range = dateAxis.createSeriesRange(series);
                range.date = this.state.sunDate.rise_time;
                range.endDate = this.state.sunDate.set_time;
                range.axisFill.fill = am4core.color("#396478");
                range.axisFill.fillOpacity = 0.3;
            }


            chart.legend = new am4charts.Legend();
            chart.cursor = new am4charts.XYCursor();

            if (this.state.sensorZoom) {
                chart.scrollbarX = new am4core.Scrollbar();
                chart.scrollbarX.marginLeft = 0;
                chart.cursor.behavior = "zoomX";
                chart.cursor.lineY.disabled = true;
            }
            if (!this.state.sensorZoom) {
                chart.scrollbarY = new am4core.Scrollbar();
                chart.scrollbarY.marginLeft = 0;
                chart.cursor.behavior = "zoomY";
                chart.cursor.lineX.disabled = true;
            }
        });
    }

    createAxisAndSeries = (chart, field, name, opposite) => {
        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        let series = chart.series.push(new am4charts.LineSeries());

        series.dataFields.valueY = field;
        series.dataFields.dateX = "date";
        series.strokeWidth = 2;
        series.yAxis = valueAxis;
        series.name = name;
        series.tooltipText = "{name}: [bold]{valueY}[/]";
        series.tensionX = 0.8;

        if (this.sensorNameTypes[field].type === "rain" || this.sensorNameTypes[field].type === "water" || this.sensorNameTypes[field].type === "power" || this.sensorNameTypes[field].type === "window" || this.sensorNameTypes[field].type === "door" || this.sensorNameTypes[field].type === "fire") {
            valueAxis.min = -1;
            valueAxis.max = 2;
            valueAxis.strictMinMax = true;
            let units = this.sensorNameTypes[field].unit.split(":");
            valueAxis.renderer.labels.template.adapter.add("text", function (text) {
                if (text === "0") {
                    return units[1];
                } else if (text === "1") {
                    return units[0];
                } else {
                    return '';
                }
            });
            valueAxis.renderer.line.strokeOpacity = 1;
            valueAxis.renderer.line.strokeWidth = 2;
            valueAxis.renderer.line.stroke = series.stroke;
            valueAxis.renderer.labels.template.fill = series.stroke;
            valueAxis.renderer.opposite = opposite;
            valueAxis.renderer.grid.template.disabled = true;
        } else if(this.sensorNameTypes[field].type === 'motor' || this.sensorNameTypes[field].type === 'power'){
            valueAxis.renderer.line.strokeOpacity = 1;
            valueAxis.renderer.line.strokeWidth = 2;
            valueAxis.renderer.line.stroke = series.stroke;
            valueAxis.renderer.labels.template.fill = series.stroke;
            valueAxis.renderer.opposite = opposite;
            valueAxis.renderer.grid.template.disabled = true;
        } else {
            valueAxis.renderer.line.strokeOpacity = 1;
            valueAxis.renderer.line.strokeWidth = 2;
            valueAxis.renderer.line.stroke = series.stroke;
            valueAxis.renderer.labels.template.fill = series.stroke;
            valueAxis.renderer.opposite = opposite;
            valueAxis.renderer.grid.template.disabled = true;
        }
    };

    generateChartData = (sensorChartDatas) => {
        let chartData = [];
        let sensorValueObject = {};
        let temperatureValue = 0;

        for (let i = 0; i < sensorChartDatas.length; i++) {
            sensorValueObject[sensorChartDatas[i].name] = {avg: 0};
        }
        for (let i = 0; i < sensorChartDatas[0].data.length; i++) {
            let obj = {};
            let dateValue = new Date(sensorChartDatas[0].data[i].created_at);
            let timeValue = new Date(dateValue).getHours() * 60 + new Date(dateValue).getMinutes() ;
            obj.date = dateValue;


            for (let j = 0; j < sensorChartDatas.length; j++) {
                if (sensorChartDatas[j].type === 'temperature') {
                    if (!sensorChartDatas[j].data[i]) {
                        sensorChartDatas[j].data[i] = {
                            value: temperatureValue
                        };

                        obj[sensorChartDatas[j].name] = temperatureValue;
                    } else {
                        let temperatureTime = new Date(sensorChartDatas[j].data[i].created_at).getHours() * 60 + new Date(sensorChartDatas[j].data[i].created_at).getMinutes();
                        let prevTime = sensorChartDatas[j].data[i-1] ? new Date(sensorChartDatas[j].data[i-1].created_at).getHours() * 60 + new Date(sensorChartDatas[j].data[i-1].created_at).getMinutes() : temperatureTime;

                        if (temperatureTime === timeValue) {
                            temperatureValue = sensorChartDatas[j].data[i].value;
                            obj[sensorChartDatas[j].name] = temperatureValue;

                        } else if( temperatureTime > timeValue && prevTime < timeValue) {
                            temperatureValue = sensorChartDatas[j].data[i].value;
                            obj[sensorChartDatas[j].name] = temperatureValue;

                        }else{
                            temperatureValue = sensorChartDatas[j].data[i].value;
                            obj[sensorChartDatas[j].name] = temperatureValue;
                        }
                    }
                } else {
                    obj[sensorChartDatas[j].name] = sensorChartDatas[j].data[i].value;
                }
            }

            for (let j = 0; j < sensorChartDatas.length; j++) {
                if (i === 0) {
                    sensorValueObject[sensorChartDatas[j].name].min = sensorChartDatas[j].data[i].value ;
                    sensorValueObject[sensorChartDatas[j].name].max = sensorChartDatas[j].data[i].value;
                }
                if (sensorValueObject[sensorChartDatas[j].name].min > sensorChartDatas[j].data[i].value) {
                    sensorValueObject[sensorChartDatas[j].name].min = sensorChartDatas[j].data[i].value;
                }
                if (sensorValueObject[sensorChartDatas[j].name].max < sensorChartDatas[j].data[i].value) {
                    sensorValueObject[sensorChartDatas[j].name].max = sensorChartDatas[j].data[i].value;
                }
                sensorValueObject[sensorChartDatas[j].name].avg += (sensorChartDatas[j].data[i].value / sensorChartDatas[0].data.length);

            }
            chartData.push(obj);
        }

        for (let key in sensorValueObject) {
            sensorValueObject[key].avg = (sensorValueObject[key].avg).toFixed(2);
        }
        this.setState({
            sensorValueObject: sensorValueObject
        });
        return chartData;
    };

    generateIntegratedChartData = (chartDatas) => {
        let chartData = [];
        let valueObject = {};
        let temperatureValue = 0;

        for (let i=0; i <chartDatas.length; i ++){
            if(chartDatas[i].control_name) valueObject[chartDatas[i].control_name] = {avg:0};
            if(chartDatas[i].sensor_name) valueObject[chartDatas[i].sensor_name] = {avg:0};
            if(chartDatas[i].name) valueObject[chartDatas[i].name] = {avg:0};
        }

        for (let i=0; i<chartDatas[0].data.length; i++){
            let obj = {};
            let dateValue = new Date(chartDatas[0].data[i].created_at);
            let timeValue = new Date(dateValue).getHours() * 60 + new Date(dateValue).getMinutes() ;
            obj.date = dateValue;

            for (let j = 0; j < chartDatas.length; j++) {
                if (chartDatas[j].type === 'temperature') {
                    if (!chartDatas[j].data[i]) {
                        chartDatas[j].data[i] = {
                            value: temperatureValue
                        };

                        obj[chartDatas[j].name] = temperatureValue;
                    } else {
                        let temperatureTime = new Date(chartDatas[j].data[i].created_at).getHours() * 60 + new Date(chartDatas[j].data[i].created_at).getMinutes();
                        let prevTime = chartDatas[j].data[i-1] ? new Date(chartDatas[j].data[i-1].created_at).getHours() * 60 + new Date(chartDatas[j].data[i-1].created_at).getMinutes() : temperatureTime;

                        if (temperatureTime === timeValue) {
                            temperatureValue = chartDatas[j].data[i].value;
                            obj[chartDatas[j].name] = temperatureValue;

                        } else if( temperatureTime > timeValue && prevTime < timeValue) {
                            temperatureValue = chartDatas[j].data[i].value;
                            obj[chartDatas[j].name] = temperatureValue;

                        }else{
                            temperatureValue = chartDatas[j].data[i].value;
                            obj[chartDatas[j].name] = temperatureValue;
                        }


                    }
                }
            }

            for (let j = 0; j < chartDatas.length; j++) {

                if(chartDatas[j].sensor_name) obj[chartDatas[j].sensor_name] = chartDatas[j].data[i].value;
                if(chartDatas[j].control_name){
                    if (chartDatas[j].control_type === 'motor') {
                        obj[chartDatas[j].control_name] = chartDatas[j].data[i].current;
                    } else if(chartDatas[j].control_type === 'power') {
                        obj[chartDatas[j].control_name] = chartDatas[j].data[i].state;
                    }
                }

                if (i === 0) {
                    if(chartDatas[j].sensor_name){
                        valueObject[chartDatas[j].sensor_name].min = chartDatas[j].data[i].value;
                        valueObject[chartDatas[j].sensor_name].max = chartDatas[j].data[i].value;

                    }

                    if(chartDatas[j].control_name){
                        if (chartDatas[j].control_type === 'motor') {
                            valueObject[chartDatas[j].control_name].min = chartDatas[j].data[i].current;
                            valueObject[chartDatas[j].control_name].max = chartDatas[j].data[i].current;
                        } else if(chartDatas[j].control_type === 'power') {
                            if(chartDatas[j].data[i].state === 0){
                                valueObject[chartDatas[j].control_name].min = chartDatas[j].data[i].state;
                                valueObject[chartDatas[j].control_name].max = chartDatas[j].data[i].state;
                            }
                        }
                    }

                }
                if(chartDatas[j].sensor_name){
                    if (valueObject[chartDatas[j].sensor_name].min > chartDatas[j].data[i].value) {
                        valueObject[chartDatas[j].sensor_name].min = chartDatas[j].data[i].value;
                    }
                    if (valueObject[chartDatas[j].sensor_name].max < chartDatas[j].data[i].value) {
                        valueObject[chartDatas[j].sensor_name].max = chartDatas[j].data[i].value;
                    }
                }

                if(chartDatas[j].control_name){
                    if (chartDatas[j].control_type === 'motor'){
                        if (valueObject[chartDatas[j].control_name].min > chartDatas[j].data[i].current) {
                            valueObject[chartDatas[j].control_name].min = chartDatas[j].data[i].current;
                        }
                        if (valueObject[chartDatas[j].control_name].max < chartDatas[j].data[i].current) {
                            valueObject[chartDatas[j].control_name].max = chartDatas[j].data[i].current;
                        }
                    } else if (chartDatas[j].control_type === 'power'){
                        if (valueObject[chartDatas[j].control_name].min > chartDatas[j].data[i].state) {
                            valueObject[chartDatas[j].control_name].min = chartDatas[j].data[i].state;
                        }
                        if (valueObject[chartDatas[j].control_name].max < chartDatas[j].data[i].state) {
                            valueObject[chartDatas[j].control_name].max = chartDatas[j].data[i].state;
                        }
                    }
                }

                if(chartDatas[j].sensor_name){
                    valueObject[chartDatas[j].sensor_name].avg += (chartDatas[j].data[i].value / chartDatas[0].data.length);
                }

                if(chartDatas[j].control_name){
                    if(chartDatas[j].control_type === 'motor'){
                        valueObject[chartDatas[j].control_name].avg += (chartDatas[j].data[i].current / chartDatas[0].data.length);
                    } else if(chartDatas[j].control_type === 'power'){
                        valueObject[chartDatas[j].control_name].avg += (chartDatas[j].data[i].state / chartDatas[0].data.length);
                    }
                }
            }

            chartData.push(obj);

        }

        for (let key in valueObject) {
            valueObject[key].avg = (valueObject[key].avg).toFixed(2);
        }
        this.setState({
            sensorValueObject: valueObject
        });
        return chartData;
    };

    generateSensorNameType = (sensorChartDatas) => {
        let obj = {};
        sensorChartDatas.forEach((item) => {
            if(item.control_name && item.sensor_name){
                obj[item.control_name] = {};
                obj[item.control_name].type = item.control_type;
                if(item.control_type === 'motor') obj[item.control_name].unit = '%';
                if(item.control_type === 'power') obj[item.control_name].unit = 'ON:OFF';

                obj[item.sensor_name] = {};
                obj[item.sensor_name].type = item.sensor_type;
                obj[item.sensor_name].unit = item.unit;
            }else{
                obj[item.name] = {};
                obj[item.name].type = item.type;
                obj[item.name].unit = item.unit;
            }

            if(item.type === 'temperature'){
                obj[item.name] = {};
                obj[item.name].type = item.type;
                obj[item.name].unit = item.unit;
            }
        });

        return obj;
    };

    sensorChartZoom = () => {
        if (this.state.sensorZoom) {
            this.setState({sensorZoom: false});
            this.getChartData();
        }
        if (!this.state.sensorZoom) {
            this.setState({sensorZoom: true});
            this.getChartData();
        }
    };

    selectFullScreenMode = () => {
        const _this = this;
        _this.setState({fullScreen: !_this.state.fullScreen});
        this.getChartData();
    };

    handleItem = (value, index, type) => {
        let items = deepcopy(this.state.items);
        if (type === 'item') {
            items[index].selectedItem = value;
        } else if (type === 'control') {
            items[index].selectedControl = value;
        }

        this.setState({items});
    };

    getOptions = (type) => {
        if (type.value === 'sensor') {
            return this.state.sensors;
        } else if (type.value === 'motor') {
            return this.state.motors;
        } else if (type.value === 'power') {
            return this.state.powers;
        } else if (type.value === 'setting') {
            return this.state.selectTemperatureOptions;
        }
    };

    addItems = (e) => {
        if(this.state.items.length > 3){
            return this.props.showDialog({
                alertText: "그래프 분석 항목은 최대 4개까지 추가할 수 있습니다."
            });
        }
        let items = deepcopy(this.state.items);

        items.push({
            selectedItem: {label: '선택안함', value: 'none'},
            selectedControl: null,
        });

        this.setState({items});
    };

    removeItem = (e, index) => {
      let items = deepcopy(this.state.items);
      items.splice(index, 1);
      this.setState({items});
    };

    handleTime = (val, key) => {
        let state = Object.assign({}, this.state);
        state[key] = val;
        this.setState(state);
    };

    getSunDate = () => {

        let rise_time = new Date();
        rise_time.setHours(parseInt(this.props.rise_hour));
        rise_time.setMinutes( parseInt(this.props.rise_minute));

        let set_time = new Date();
        set_time.setHours(parseInt(this.props.set_hour));
        set_time.setMinutes( parseInt(this.props.set_minute));

        let sunDate = {
            rise_time : rise_time,
            set_time : set_time
        };

        this.setState({sunDate});
    };

    render() {
        return (
            <article id="lcModalIntegratedGraphWrap"
                     className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className={'vertical-align-wrap ' + (this.state.fullScreen ? ' full-screen' : '')}>
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalIntegratedGraph" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">통합 그래프</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body lc-graph-modal">
                                    {this.state.fullScreen === false ? (
                                        <div className="lc-integrated-graph-top-wrap">
                                            <section className="integrated-graph-item-wrap">
                                                <label className="integrated-graph-modal-label">분석 기간</label>
                                                <section className="options-wrap">
                                                    <div className="date-range-button-wrap">
                                                        <button
                                                            className={"date-range-button" + (this.state.dateRange.isDate ? ' lc-active' : '')}
                                                            onClick={e => this.selectDateRange(e, "isDate")}>1일
                                                        </button>
                                                        <button
                                                            className={"date-range-button" + (this.state.dateRange.isWeek ? ' lc-active' : '')}
                                                            onClick={e => this.selectDateRange(e, "isWeek")}>1주
                                                        </button>
                                                        <button
                                                            className={"date-range-button" + (this.state.dateRange.isMonth ? ' lc-active' : '')}
                                                            onClick={e => this.selectDateRange(e, "isMonth")}>1개월
                                                        </button>
                                                    </div>
                                                </section>
                                                <section className="graph-wrap">
                                                    <div className="graph-wrap-header">
                                                        <span className="date-range-label">기간 선택</span>
                                                        <div className="date-picker-wrap">
                                                            <DatePicker dateFormat={"YYYY-MM-DD"}
                                                                        className="lc-farmlabs-datepicker"
                                                                        placeholderText="시작일"
                                                                        popperClassName="lc-farmlabs-datepicker-popper"
                                                                        isClearable={true}
                                                                        selected={this.state.startDate}
                                                                        customInput={<ButtonDatePicker
                                                                            placeholderText="시작일"/>}
                                                                        maxDate={this.state.endDate ? moment(date(new Date(this.state.endDate), DATE_DISPLAY_FORMAT)) : undefined}
                                                                        onChange={val => this.handleDate(val, "startDate")}/>
                                                        </div>

                                                        <div className="date-picker-wrap">
                                                            <DatePicker dateFormat={"YYYY-MM-DD"}
                                                                        className="lc-farmlabs-datepicker"
                                                                        placeholderText="종료일"
                                                                        popperClassName="lc-farmlabs-datepicker-popper right"
                                                                        isClearable={true}
                                                                        selected={this.state.endDate}
                                                                        customInput={<ButtonDatePicker
                                                                            placeholderText="종료일"/>}
                                                                        minDate={this.state.startDate ? moment(date(new Date(this.state.startDate), DATE_DISPLAY_FORMAT)) : undefined}
                                                                        onChange={val => this.handleDate(val, "endDate")}/>
                                                        </div>
                                                    </div>
                                                    <div className="graph-wrap-header">
                                                        <span className="date-range-label">시간 선택</span>
                                                        <div className="time-wrap">
                                                            <div className="time-item select-module-wrap ">
                                                                <Select
                                                                    value={this.state.selectedStartHour}
                                                                    placeholder="시작시간"
                                                                    onChange={e => this.handleTime(e, 'selectedStartHour')}
                                                                    isSearchable={false}
                                                                    options={selectHourOptions}
                                                                    styles={colourStyles}/>
                                                            </div>
                                                            <div
                                                                className="time-item select-module-wrap ">
                                                                <Select
                                                                    value={this.state.selectedStartMinute}
                                                                    placeholder="시작 분"
                                                                    onChange={e => this.handleTime(e, 'selectedStartMinute')}
                                                                    isSearchable={false}
                                                                    options={selectMinuteOptions}
                                                                    styles={colourStyles}/>
                                                            </div>
                                                        </div>
                                                        <span className="time-span">~</span>
                                                        <div className="time-wrap">
                                                            <div
                                                                className="time-item select-module-wrap ">
                                                                <Select
                                                                    value={this.state.selectedEndHour}
                                                                    placeholder="종료시간"
                                                                    onChange={e => this.handleTime(e, 'selectedEndHour')}
                                                                    isSearchable={false}
                                                                    options={selectHourOptions}
                                                                    styles={colourStyles}/>
                                                            </div>
                                                            <div
                                                                className="time-item select-module-wrap">
                                                                <Select
                                                                    value={this.state.selectedEndMinute}
                                                                    placeholder="종료 분"
                                                                    onChange={e => this.handleTime(e, 'selectedEndMinute')}
                                                                    isSearchable={false}
                                                                    options={selectMinuteOptions}
                                                                    styles={colourStyles}/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </section>
                                            </section>
                                            <section className="integrated-graph-item-wrap">
                                                <label className="integrated-graph-modal-label">그래프 분석 항목</label>
                                                <div className="button-wrap">
                                                    <button className="theme-text-button" onClick={e => this.addItems(e)}>+ 추가</button>
                                                </div>
                                                <ul id="integratedGraphSensorList">
                                                    <li>
                                                        <label className="control-list-label">구분</label>
                                                        <label className="control-list-label">선택</label>
                                                    </li>
                                                    {
                                                        this.state.items.map((item, index) => {
                                                            return (
                                                                <li key={index}>
                                                                    <div className="select-module-wrap">
                                                                        <Select
                                                                            value={item.selectedItem}
                                                                            placeholder="구분 선택"
                                                                            onChange={value => this.handleItem(value, index, 'item')}
                                                                            isSearchable={false}
                                                                            options={ITEM_TYPE}
                                                                            styles={colourStyles}/>
                                                                    </div>
                                                                    <div className="select-module-wrap">
                                                                        <Select
                                                                            value={item.selectedControl}
                                                                            placeholder="분석항목 선택"
                                                                            onChange={e => this.handleItem(e, index, 'control')}
                                                                            isSearchable={false}
                                                                            options={this.getOptions(item.selectedItem)}
                                                                            styles={colourStyles}/>
                                                                    </div>

                                                                    {index > 0 ?
                                                                        <button className="theme-text-button" onClick={e=> this.removeItem(e, index)}>삭제</button>
                                                                        : null
                                                                    }
                                                                </li>
                                                            )
                                                        })
                                                    }
                                                </ul>
                                            </section>
                                            <button className="theme-button" onClick={this.getChartData}>분석</button>
                                        </div>
                                    ) : null}


                                    {
                                        this.state.isGraphRendered ?
                                            (
                                                <div className="lc-integrated-graph-bottom-wrap">
                                                    {this.state.fullScreen === false ? (
                                                        <table id="lcSensorValueTable">
                                                            <thead>
                                                            <tr>
                                                                <th/>
                                                                <th>최소값</th>
                                                                <th>최대값</th>
                                                                <th>평균값</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            {
                                                                Object.keys(this.state.sensorValueObject).map((key, index) => {
                                                                    return (
                                                                        <tr key={key}>
                                                                            <td>{key}</td>
                                                                            <td>{this.state.sensorValueObject[key].min}</td>
                                                                            <td>{this.state.sensorValueObject[key].max}</td>
                                                                            <td>{this.state.sensorValueObject[key].avg}</td>
                                                                        </tr>
                                                                    )
                                                                })
                                                            }
                                                            </tbody>
                                                        </table>
                                                    ) : null
                                                    }
                                                    <button className="card-header-button zoom-graph"
                                                            onClick={() => this.sensorChartZoom()}>{this.state.sensorZoom ? "세로확대보기" : "가로확대보기"}</button>
                                                    <button
                                                        className={'card-header-button full-screen-button ' + (this.state.fullScreen ? ' full-active' : '')}
                                                        onClick={() => this.selectFullScreenMode()}>{this.state.fullScreen ? "분석보기" : "전체보기"}</button>
                                                    <div id="chartWrapper">
                                                        <div id="multiChartDiv"/>
                                                    </div>
                                                </div>
                                            ) : null
                                    }
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useSunDate(
    ({state, actions}) => ({
        rise_hour: state.rise_hour,
        rise_minute: state.rise_minute,
        set_hour: state.set_hour,
        set_minute: state.set_minute,
        setSunDate: actions.setSunDate
    })
)(useIntegratedGraph(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalIntegratedGraph))));