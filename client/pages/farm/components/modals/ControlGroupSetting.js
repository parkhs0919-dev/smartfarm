import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import {colourStyles} from "../../utils/select";
import {useControlGroupSetting} from "../../contexts/modals/controlGroupSetting";
import controlManager from "../../managers/control";
import {noSession} from "../../utils/session";
import deepcopy from "../../../../utils/deepcopy";
import controlGroupManager from "../../managers/controlGroup";

const TYPE_MOTOR = 'motor';
const TYPE_POWER = 'power';

const DEFAULT_STATE = {
    id : '',
    house_id : '',
    controls : [],
    control_group_name : '',
    isUpdate : false,
    controlGroupItems : [],
}

class ControlGroupSetting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id : '',
            house_id : this.props.getHouseId(),
            controls : [],
            control_group_name : '',
            isUpdate: false,
            controlGroupItems : [],
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        document.addEventListener('keyup', this.closeOnEscape);
        if(data) {
            this.getControlGroups(data.type);
            this.setState(data);
        }else{
            this.setState({type : TYPE_MOTOR});
            this.getControlGroups(TYPE_MOTOR);
        }

    };

    getControlGroups = (type) => {
        const _this = this;
        let query = {
            type: type,
            house_id: _this.state.house_id,
        };

        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    controls: _this.generateRows(data.data.rows),
                });
            } else if (status === 404) {
                _this.setState({
                    controls: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));

        this.setState({controls});
    };

    generateRows = (controls) => {
        const _this = this;

        if(_this.state.controlGroupItems && _this.state.controlGroupItems.length) {
            let controlGroupItems = deepcopy(_this.state.controlGroupItems);
            controls.forEach((data, index) => {
               data.isUse = false;
               controlGroupItems.forEach((item, index) => {
                   if(data.id === item.control_id) {
                       data.isUse = true;
                   }
               })
            })
        }else {
            controls.forEach((data, index) => {
                data.isUse = false;
            });
        }

        return controls;
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    handleControlGroupItems = (e, key, index) => {
        let controls = deepcopy(this.state.controls);
        controls[index][key] = !controls[index][key];
        this.setState({controls});
    };

    handleInputControlGroupName = (e, key) => {
        let control_group_name = e.target.value;
        this.setState({control_group_name});
    };

    submit = () => {
        const _this = this;

        let data = {
            controlGroupItems : [],
        };

        data.house_id = _this.props.getHouseId();
        data.type = _this.state.type;
        data.state = _this.state.type === TYPE_MOTOR ? 'stop' : 'off';

        if(!_this.state.control_group_name){
            return this.props.showDialog({
                alertText: "그룹제어명을 입력해주세요."
            });
        }else{
            data.control_group_name = _this.state.control_group_name;
        }

        if(_this.state.controls.length){
            _this.state.controls.forEach((item, index) => {
                if(item.isUse) {
                    data.controlGroupItems.push({
                        control_id : item.id
                    })
                }
            });
        }

        if(!data.controlGroupItems.length) {
            return this.props.showDialog({
                alertText: "그룹제어 장치를 선택해주세요."
            });
        }else {
            if(data.controlGroupItems.length < 2) {
                return this.props.showDialog({
                    alertText: `그룹제어 장치를 <br/>두개 이상 선택해주세요.`
                });
            }
        }


        if(data){
            if(_this.state.id){
                data.id = _this.state.id;
                controlGroupManager.update(data, (status, data) => {
                    if(status === 200){
                        _this.props.showDialog({
                            alertText: "그룹제어 설정이 \n수정되었습니다.",
                            cancelCallback: () => {
                                _this.close();
                            }
                        })
                    }else{
                        _this.props.alertError(status, data);
                    }
                })
            }else{
                data.on_off_state = 'on';

                controlGroupManager.create(data, (status, data) => {
                    if(status === 200){
                        _this.props.showDialog({
                            alertText: "그룹제어 설정이 \n등록되었습니다.",
                            cancelCallback: () => {
                                _this.close();
                            }
                        })
                    }else{
                        _this.props.alertError(status, data);
                    }
                })
            }
        }
    };
    render() {
        return (
            <article id="lcModalControlGroupSettingWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalControlGroupSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">그룹제어 설정 추가</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>
                                <section className="modal-body">
                                    <div className="control-group-name-wrap">
                                        <h3 className="form-label">그룹제어명</h3>
                                        <input type="text" value={this.state.control_group_name}
                                               onChange={e => this.handleInputControlGroupName(e)} className="lc-farm-input"
                                               placeholder="그룹제어명" />
                                    </div>
                                    <div className="control-list-wrap">
                                        <div className="control-list">
                                            <div className="control-list-title">
                                                <h3>그룹제어 장치 선택</h3>
                                            </div>
                                            <ul className="control-list-items">
                                                {this.state.controls.length ?
                                                    this.state.controls.map((data, index) => {
                                                        return (
                                                            <li key={index}>
                                                                <div className="lc-theme-checkbox-wrap">
                                                                    <input value="state"
                                                                           id={`checkControl${index}`}
                                                                           checked={data.isUse === true}
                                                                           onChange={() => {}} type="checkbox"
                                                                           name="control-group-item"/>
                                                                    <label htmlFor={`checkControl${index}`}
                                                                           onClick={(e) => this.handleControlGroupItems(e, 'isUse', index)}
                                                                           className="box-shape-label"/>
                                                                    <label htmlFor={`checkControl${index}`}
                                                                           onClick={(e) => this.handleControlGroupItems(e, 'isUse', index)}
                                                                           className="text-label">{data.control_name}</label>
                                                                </div>
                                                            </li>
                                                        )
                                                    }) : null

                                                }
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="button-wrap">
                                        <button className="theme-button" onClick={this.submit}>등록</button>
                                    </div>
                                </section>
                            </div> : null
                        }
                    </div>
                </div>
            </article>
        );
    }
}

export default useControlGroupSetting(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        show: actions.show,
        alertError: actions.alertError
    })
)(useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(ControlGroupSetting)));
