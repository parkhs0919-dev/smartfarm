import React, {Component, Fragment} from 'react';
import SunDate from "../../components/autoControl/SunDate";
import ControlOne from "../../components/autoControl/ControlOne";
import ControlTable from "../../components/autoControl/ControlTable";
import CheckCondition from "../autoControl/CheckCondition";
import SensorCondition from "../autoControl/SensorCondition";
import ControlCondition from "../autoControl/ControlCondition";
import GetRecommend from "../autoControl/GetRecommend";
import StepDelayByStep from "../autoControl/StepDelayByStep";
import ControlPBand from "../autoControl/ControlPBand";
import ControlTemperatureOption from "../autoControl/ControlTemperatureOption";
import ControlMinMaxRange from "../autoControl/ControlMinMaxRange";
import {useAutoControlForm} from "../../contexts/modals/autoControlForm";
import {useAutoControlTemperatureGraph} from "../../contexts/modals/autoControlTemperatureGraph";
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import Select from 'react-select';
import {colourStyles} from "../../utils/select";
import moment from 'moment';
import CONSTANT from '../../constants/constant';
import {date, attachZero, isEmpty} from "../../../../utils/filter";
import {returnSensorSelect, returnSensorOption} from "../../utils/sensor";
import DatePicker from "react-datepicker/es";
import autoControlManager from '../../managers/autoControl';
import controlMinMaxRangesManager from "../../managers/controlMinMaxRanges"
import sensorManager from '../../managers/sensor';
import controlManager from '../../managers/control';
import controlGroupManager from '../../managers/controlGroup';
import pBandsManager from "../../managers/pBands";
import {noSession} from "../../utils/session";
import {useSunDate} from "../../contexts/sunDate";
import deepcopy from "../../../../utils/deepcopy";

import handler from "./AutoControlForm/handler";
import initHandler from "./AutoControlForm/initHandler";

const SPECIEL_SENSOR_TYPE = CONSTANT.SPECIAL_SENSOR;

const TYPE_MOTOR = 'motor';
const TYPE_POWER = 'power';
const TYPE_WINDDIRECTION = 'windDirection';

const CONTROL_STATE_OPTIONS = CONSTANT.CONTROL_OPTIONS.STATE;
const CONTROL_UNIT_OPTIONS = CONSTANT.CONTROL_OPTIONS.UNIT;
const DATE_DISPLAY_FORMAT = "yyyy-MM-dd";
const TAB_SENSOR = "sensor";
const TAB_TIME = "time";
const TAB_MIX = "mix";
const TAB_STEP = "step";
const TAB_CONTROL = "control";
const TAB_STEP_STEP = "step_step";
const TAB_TABLE = "table";

let selectHourOptions = [];
let selectMinuteOptions = [];

for (let i = 0; i < 24; i++) {
    let hour = {
        value: i,
        label: `${attachZero(i)}시`
    };
    selectHourOptions.push(hour);
}

for (let i = 0; i < 60; i++) {
    let minute = {
        value: i,
        label: `${attachZero(i)}분`
    };
    selectMinuteOptions.push(minute);
}

const INIT_STATE = {
    recommendList : [],
    recommendCrops : [],
    selectedCrop : null,
    is_use_recommend : false,
    recommendSensorTxt : '',
    recommendControl : '',
    recommendControlControls : null,
    selectedRecommendId : null,
    recommendItem : {},
    recommendTxt : {},
    recommendSensor : null,
    recommendMinMax : [],
    is_use_option : false,
    option_max_value : '',
    option_min_value :'',
    tableAvgTemeperature : '',
    autoControlSensorOptions : [{
        selectedAutoControlSensor : null,
        sensor : null,
        sensor_id : '',
        sensorUnit : '',
        max_value : '',
        min_value : '',
        percentage : '',
    }],
    is_temperature_control_option : false,
    rise_time : '',
    drop_time : '',
    expect_temperature : '',
    expect_temperature_sensor_id : '',
    selectedTemperatureSensor : null,
    TemperatureSensor : [],
    pBands: [],
    selectedPBand: null,
    pBandTemperature: '',
    pBandIntegral: '',
    per_date: '',
    selectedDateOption: [],
    currentTab: TAB_SENSOR,
    dayType: "day",
    selectedDayType: null,
    datePickerOpenState: {
        start_date: false,
        end_date: false,
    },
    selectedControlId: null,
    selectedControlKey : null,
    controlOptionFromOutside: null,
    auto_control_name: '',
    step_delay: '',
    setCount: 0,
    start_date: null,
    end_date: null,
    selectedActiveCount: {value: 'infinite', label: "계속 실행"},
    selectedDelayWay: CONSTANT.delayWayOptions[0],
    selectedSensorOption: null,
    selectedControlCondition: null,
    selectedControlConditionOption: null,
    controlConditionValue: '',
    selectedControlOption: null,
    selectedControlStateOption: null,
    selectedControlWayOption: null,
    selectedRangeConditionOption: null,
    selectedTimeWay: null,
    selectedStartHour: null,
    selectedStartMinute: null,
    selectedEndHour: null,
    selectedEndMinute: null,
    selectedSunDateStartType: null,
    selectedSunDateEndType: null,
    selectedSunDateStartCondition: null,
    selectedSunDateEndCondition: null,
    selectedSunDateStartHour: '',
    selectedSunDateStartMinute: '',
    selectedSunDateEndHour: '',
    selectedSunDateEndMinute: '',
    sensorValue: '',
    delay: '',
    recount: '',
    active_count: 0,
    settingValue: '',
    is_mon: true,
    is_tue: true,
    is_wed: true,
    is_thur: true,
    is_fri: true,
    is_sat: true,
    is_sun: true,
    controlUnitName: '',
    is_min_max_range: false,
    minMaxRanges: [],
    selectMotorOption : [],
    selectPowerOption : [],
    tableSelectedIsUseTemperature : "vantilation",
    tableSelectedPband: null,
    tableSelectedSensor: null,
    tableSelectedControls: [{
        selectedMotorOption : null,
        selectedPowerOption : null,
    }],
    tableSelectedPeriod : null,
    tableControls : [{
        value : '',
        label : '',
        selectedPeriod : null,
        is_using_period : {label : '미사용', value : false},
        selectedTimeType : {value : "single", label : "고정"},
        selectedTimeCondition : {value : "plus", label : "+"},
        selectedStartHour : null,
        selectedStartMinute : null,
        isUseExpectTemperatureControl : false,
        expect_temperature : '',
        rise_time: '',
        drop_time: '',
        expect_temperature_max_value : '',
        expect_temperature_min_value : '',
        tableSensors: [{
            selectedAutoControlSensor : null,
            sensor : null,
            sensor_id : '',
            sensorUnit : '',
            max_value : '',
            min_value : '',
            percentage : '',
        }],
    }],
    mixControls: [{
        selectedPBand: null,
        pBandTemperature: '',
        pBandIntegral: '',
        settingValue: '',
        delay: '',
        recount: '',
        active_count: 0,
        start_time: null,
        end_time: null,
        selectedTimeWay: null,
        selectedControlOption: null,
        selectedControlStateOption: null,
        selectedControlWayOption: null,
        selectedSunDateStartType: null,
        selectedSunDateEndType: null,
        selectedSunDateStartCondition: null,
        selectedSunDateEndCondition: null,
        selectedSunDateStartHour: '',
        selectedSunDateStartMinute: '',
        selectedSunDateEndHour: '',
        selectedSunDateEndMinute: '',
        selectedActiveCount: {value: 'infinite', label: "계속 실행"},
        selectedDelayWay: CONSTANT.delayWayOptions[0],
        sensor_operator: 'and',
        control_operator: 'and',
        is_min_max_range: false,
        is_temperature_control_option : false,
        rise_time : '',
        drop_time : '',
        expect_temperature : '',
        expect_temperature_sensor_id : '',
        selectedTemperatureSensor : null,
        conditional: {
            time: true,
            sensor: false,
            control: false,
        },
        mixSensors: [{
            selectedRangeConditionOption : null,
            selectedSensorOption : null,
            sensorValue: '',
            is_use_option : false,
            option_max_value : '',
            option_min_value :'',
            autoControlSensorOptions : [{
                selectedAutoControlSensor : null,
                sensor : null,
                sensor_id : '',
                sensorUnit : '',
                max_value : '',
                min_value : '',
                percentage : '',
            }],
        }],
        mixControls: [{
            controlConditionValue: ''
        }],
        minMaxRanges: [],
        recommendSensorTxt : '',
        recommendControl : '',
        recommendControlControls : null,
        selectedRecommendId : null,
        recommendItem : {},
        recommendTxt : {},
        recommendSensor : null,
        recommendMinMax : [],

    }],
    stepControls: [{
        selectedPBand: null,
        pBandTemperature: '',
        pBandIntegral: '',
        settingValue: '',
        delay: '',
        recount: '',
        start_time: null,
        end_time: null,
        selectedTimeWay: null,
        selectedControlOption: null,
        selectedControlStateOption: null,
        selectedControlWayOption: null,
        selectedSunDateStartType: null,
        selectedSunDateEndType: null,
        selectedSunDateStartCondition: null,
        selectedSunDateEndCondition: null,
        selectedSunDateStartHour: '',
        selectedSunDateStartMinute: '',
        selectedSunDateEndHour: '',
        selectedSunDateEndMinute: '',
        sensor_operator: 'and',
        control_operator: 'and',
        is_min_max_range: false,
        is_temperature_control_option : false,
        rise_time : '',
        drop_time : '',
        expect_temperature : '',
        expect_temperature_sensor_id : '',
        selectedTemperatureSensor : null,
        conditional: {
            time: true,
            sensor: false,
            control: false,
        },
        stepSensors: [{
            selectedRangeConditionOption : null,
            selectedSensorOption : null,
            sensorValue: '',
            is_use_option : false,
            option_max_value : '',
            option_min_value :'',
            autoControlSensorOptions : [{
                selectedAutoControlSensor : null,
                sensor : null,
                sensor_id : '',
                sensorUnit : '',
                max_value : '',
                min_value : '',
                percentage : '',
            }],
        }],
        stepControls: [{
            controlConditionValue: ''
        }],
        minMaxRanges: [],
        recommendSensorTxt : '',
        recommendControl : '',
        recommendControlControls : null,
        selectedRecommendId : null,
        recommendItem : {},
        recommendTxt : {},
        recommendSensor : null,
        recommendMinMax : [],
    }],
    autoControlSteps: [{
        is_use_option : false,
        selectedPBand: null,
        pBandTemperature: '',
        pBandIntegral: '',
        selectedControlOption: null,
        selectedControlStateOption: null,
        selectedControlWay: null,
        settingValue: '',
        recount: '',
        delay: '',
        controlGroups : [],
        is_min_max_range: false,
        minMaxRanges: [],
        recommendSensorTxt : '',
        recommendControl : '',
        recommendControlControls : null,
        selectedRecommendId : null,
        recommendItem : {},
        recommendTxt : {},
        recommendSensor : null,
        recommendMinMax : [],
    }],
};

const TAB_INIT_OPTION = {
    recommendList : [],
    recommendCrops : [],
    selectedCrop : null,
    is_use_recommend : false,
    recommendSensorTxt : '',
    recommendControl : '',
    recommendControlControls : null,
    selectedRecommendId : null,
    recommendItem : {},
    recommendTxt : {},
    recommendSensor : null,
    recommendMinMax : [],
    is_use_option : false,
    option_max_value : '',
    option_min_value :'',
    tableAvgTemeperature : '',
    autoControlSensorOptions : [{
        selectedAutoControlSensor : null,
        sensor : null,
        sensor_id : '',
        sensorUnit : '',
        max_value : '',
        min_value : '',
        percentage : '',
    }],
    selectedPBand: null,
    pBandTemperature: '',
    pBandIntegral: '',
    selectedActiveCount: {value: 'infinite', label: "계속 실행"},
    selectedDelayWay: CONSTANT.delayWayOptions[0],
    selectedSensorOption: null,
    selectedControlCondition: null,
    selectedControlConditionOption: null,
    controlConditionValue: '',
    selectedControlOption: null,
    selectedControlStateOption: null,
    selectedRangeConditionOption: null,
    selectedControlWayOption: null,
    selectedTimeWay: null,
    selectedStartHour: null,
    selectedStartMinute: null,
    selectedEndHour: null,
    selectedEndMinute: null,
    selectedSunDateStartType: null,
    selectedSunDateEndType: null,
    selectedSunDateStartCondition: null,
    selectedSunDateEndCondition: null,
    selectedSunDateStartHour: '',
    selectedSunDateStartMinute: '',
    selectedSunDateEndHour: '',
    selectedSunDateEndMinute: '',
    delay: '',
    recount: '',
    sensorValue: '',
    active_count: 0,
    settingValue: '',
    controlUnitName: '',
    setCount: 0,
    is_min_max_range: false,
    minMaxRanges: [],
    is_temperature_control_option : false,
    rise_time : '',
    drop_time : '',
    expect_temperature : '',
    expect_temperature_sensor_id : '',
    selectedTemperatureSensor : null,
    tableSelectedIsUseTemperature : "vantilation",
    tableSelectedPband: null,
    tableSelectedSensor: null,
    tableSelectedControls: [{
        selectedMotorOption : null,
        selectedPowerOption : null,
    }],
    tableSelectedPeriod : null,
    tableControls : [],
    mixControls: [{
        selectedPBand: null,
        pBandTemperature: '',
        pBandIntegral: '',
        settingValue: '',
        delay: '',
        recount: '',
        active_count: 0,
        start_time: null,
        end_time: null,
        selectedActiveCount: {value: 'infinite', label: "계속 실행"},
        selectedDelayWay: CONSTANT.delayWayOptions[0],
        selectedTimeWay: null,
        selectedControlOption: null,
        selectedControlStateOption: null,
        selectedControlWayOption: null,
        selectedSunDateStartType: null,
        selectedSunDateEndType: null,
        selectedSunDateStartCondition: null,
        selectedSunDateEndCondition: null,
        selectedSunDateStartHour: '',
        selectedSunDateStartMinute: '',
        selectedSunDateEndHour: '',
        selectedSunDateEndMinute: '',
        sensor_operator: 'and',
        control_operator: 'and',
        conditional: {
            time: true,
            sensor: false,
            control: false,
        },
        mixSensors: [{
            selectedRangeConditionOption : null,
            selectedSensorOption : null,
            sensorValue: '',
            is_use_option : false,
            option_max_value : '',
            option_min_value :'',
            autoControlSensorOptions : [{
                selectedAutoControlSensor : null,
                sensor : null,
                sensor_id : '',
                sensorUnit : '',
                max_value : '',
                min_value : '',
                percentage : '',
            }],
        }],
        mixControls: [{
            controlConditionValue: ''
        }],
        is_min_max_range: false,
        minMaxRanges: [],
        is_temperature_control_option : false,
        rise_time : '',
        drop_time : '',
        expect_temperature : '',
        expect_temperature_sensor_id : '',
        selectedTemperatureSensor : null,
        recommendSensorTxt : '',
        recommendControl : '',
        recommendControlControls : null,
        selectedRecommendId : null,
        recommendItem : {},
        recommendTxt : {},
        recommendSensor : null,
        recommendMinMax : [],
    }],
    stepControls: [{
        selectedPBand: null,
        pBandTemperature: '',
        pBandIntegral: '',
        settingValue: '',
        delay: '',
        recount: '',
        start_time: null,
        end_time: null,
        selectedTimeWay: null,
        selectedControlOption: null,
        selectedControlStateOption: null,
        selectedControlWayOption: null,
        selectedSunDateStartType: null,
        selectedSunDateEndType: null,
        selectedSunDateStartCondition: null,
        selectedSunDateEndCondition: null,
        selectedSunDateStartHour: '',
        selectedSunDateStartMinute: '',
        selectedSunDateEndHour: '',
        selectedSunDateEndMinute: '',
        sensor_operator: 'and',
        control_operator: 'and',
        conditional: {
            time: true,
            sensor: false,
            control: false,
        },
        stepSensors: [{
            selectedRangeConditionOption : null,
            selectedSensorOption : null,
            sensorValue: '',
            is_use_option : false,
            option_max_value : '',
            option_min_value :'',
            autoControlSensorOptions : [{
                selectedAutoControlSensor : null,
                sensor : null,
                sensor_id : '',
                sensorUnit : '',
                max_value : '',
                min_value : '',
                percentage : '',
            }],
        }],
        stepControls: [{
            controlConditionValue: ''
        }],
        is_min_max_range: false,
        minMaxRanges: [],
        is_temperature_control_option : false,
        rise_time : '',
        drop_time : '',
        expect_temperature : '',
        expect_temperature_sensor_id : '',
        selectedTemperatureSensor : null,
        recommendSensorTxt : '',
        recommendControl : '',
        recommendControlControls : null,
        selectedRecommendId : null,
        recommendItem : {},
        recommendTxt : {},
        recommendSensor : null,
        recommendMinMax : [],
    }],
    autoControlSteps: [],
};

class ModalAutoControlForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recommendList : [],
            recommendCrops : [],
            selectedCrop : null,
            is_use_recommend : false,
            recommendSensorTxt : '',
            recommendControl : '',
            recommendControlControls : null,
            selectedRecommendId : null,
            recommendItem : {},
            recommendTxt : {},
            recommendSensor : null,
            recommendMinMax : [],
            pBands: [],
            is_use_option : false,
            option_max_value : '',
            option_min_value :'',
            tableAvgTemeperature : '',
            autoControlSensorOptions : [{
                selectedAutoControlSensor : null,
                sensor : null,
                sensor_id : '',
                sensorUnit : '',
                max_value : '',
                min_value : '',
                percentage : '',
            }],
            selectedPBand: null,
            pBandTemperature: '',
            pBandIntegral: '',
            selectedDayType: null,
            per_date: '',
            dayType: "day",
            house_id: props.getHouseId(),
            auto_control_name: '',
            step_delay: '',
            active_count: 0,
            setCount: 0,
            start_date: null,
            end_date: null,
            currentTab: TAB_SENSOR,
            controlMinMaxRanges: [],
            selectSensorOption: [],
            selectNoSelctSensorOption:[],
            selectAutoControlSensorOption : [],
            selectControlOption: [],
            selectControlGroupOption: [],
            controls : [],
            controlGroups : [],
            selectedDateOption: [],
            selectedActiveCount: {value: 'infinite', label: "계속 실행"},
            selectedDelayWay: CONSTANT.delayWayOptions[0],
            selectedSensorOption: null,
            selectedControlCondition: null,
            selectedControlConditionOption: null,
            controlConditionValue: '',
            selectedControlOption: null,
            selectedControlStateOption: null,
            selectedRangeConditionOption: null,
            selectedControlWayOption: null,
            selectedTimeWay: null,
            selectedSunDateStartType: null,
            selectedSunDateEndType: null,
            selectedSunDateStartCondition: null,
            selectedSunDateEndCondition: null,
            selectedSunDateStartHour: '',
            selectedSunDateStartMinute: '',
            selectedSunDateEndHour: '',
            selectedSunDateEndMinute: '',
            is_mon: true,
            is_tue: true,
            is_wed: true,
            is_thur: true,
            is_fri: true,
            is_sat: true,
            is_sun: true,
            isUpdate: false,
            sensorValue: '',
            delay: '',
            recount: '',
            settingValue: '',
            is_min_max_range: false,
            minMaxRanges: [],
            is_temperature_control_option : false,
            rise_time : '',
            drop_time : '',
            expect_temperature : '',
            expect_temperature_sensor_id : '',
            selectedTemperatureSensor : null,
            TemperatureSensor : [],
            datePickerOpenState: {
                start_date: false,
                end_date: false,
                start_time: false,
                end_time: false,
            },
            mixControls: [{
                selectedPBand: null,
                pBandTemperature: '',
                pBandIntegral: '',
                active_count: 0,
                settingValue: '',
                delay: '',
                recount: '',
                selectedTimeWay: null,
                selectedControlOption: null,
                selectedControlStateOption: null,
                selectedControlWayOption: null,
                selectedSunDateStartType: null,
                selectedSunDateEndType: null,
                selectedSunDateStartCondition: null,
                selectedSunDateEndCondition: null,
                selectedSunDateStartHour: '',
                selectedSunDateStartMinute: '',
                selectedSunDateEndHour: '',
                selectedSunDateEndMinute: '',
                selectedActiveCount: {value: 'infinite', label: "계속 실행"},
                selectedDelayWay: CONSTANT.delayWayOptions[0],
                sensor_operator: 'and',
                control_operator: 'and',
                conditional: {
                    time: true,
                    sensor: false,
                    control: false,
                },
                mixSensors: [{
                    selectedRangeConditionOption : null,
                    selectedSensorOption : null,
                    sensorValue: '',
                    is_use_option : false,
                    option_max_value : '',
                    option_min_value :'',
                    autoControlSensorOptions : [{
                        selectedAutoControlSensor : null,
                        sensor : null,
                        sensor_id:'',
                        sensorUnit : '',
                        max_value : '',
                        min_value : '',
                        percentage : '',
                    }],
                }],
                mixControls: [{
                    controlConditionValue: ''
                }],

                is_min_max_range: false,
                minMaxRanges: [],
                is_temperature_control_option : false,
                rise_time : '',
                drop_time : '',
                expect_temperature : '',
                expect_temperature_sensor_id : '',
                selectedTemperatureSensor : null,
                recommendSensorTxt : '',
                recommendControl : '',
                recommendControlControls : null,
                selectedRecommendId : null,
                recommendItem : {},
                recommendTxt : {},
                recommendSensor : null,
                recommendMinMax : [],
            }],
            stepControls: [{
                selectedPBand: null,
                pBandTemperature: '',
                pBandIntegral: '',
                settingValue: '',
                delay: '',
                recount: '',
                selectedTimeWay: null,
                selectedControlOption: null,
                selectedControlStateOption: null,
                selectedControlWayOption: null,
                selectedSunDateStartType: null,
                selectedSunDateEndType: null,
                selectedSunDateStartCondition: null,
                selectedSunDateEndCondition: null,
                selectedSunDateStartHour: '',
                selectedSunDateStartMinute: '',
                selectedSunDateEndHour: '',
                selectedSunDateEndMinute: '',
                sensor_operator: 'and',
                control_operator: 'and',
                conditional: {
                    time: true,
                    sensor: false,
                    control: false
                },
                stepSensors: [{
                    selectedRangeConditionOption : null,
                    selectedSensorOption : null,
                    sensorValue: '',
                    is_use_option : false,
                    option_max_value : '',
                    option_min_value :'',
                    autoControlSensorOptions : [{
                        selectedAutoControlSensor : null,
                        sensor : null,
                        sensor_id : '',
                        sensorUnit : '',
                        max_value : '',
                        min_value : '',
                        percentage : '',
                    }],
                }],
                stepControls: [{
                    controlConditionValue: ''
                }],

                is_min_max_range: false,
                minMaxRanges: [],
                is_temperature_control_option : false,
                rise_time : '',
                drop_time : '',
                expect_temperature : '',
                expect_temperature_sensor_id : '',
                selectedTemperatureSensor : null,
                recommendSensorTxt : '',
                recommendControl : '',
                recommendControlControls : null,
                selectedRecommendId : null,
                recommendItem : {},
                recommendTxt : {},
                recommendSensor : null,
                recommendMinMax : [],
            }],
            tableSelectedIsUseTemperature : "vantilation",
            tableSelectedPband: null,
            tableSelectedSensor: null,
            tableSelectedControls: [{
                selectedMotorOption : null,
                selectedPowerOption : null,
            }],
            tableSelectedPeriod : null,
            tableControls : [{
                value : '',
                label : '',
                selectedPBand: null,
                pBandTemperature: '',
                selectedPeriod : null,
                period : '',
                selectedUsingPeriod: null,
                is_using_period : {label : '미사용', value : false},
                selectedStartType : null,
                selectedStartCondition : null,
                selectedStartHour : null,
                selectedStartMinute : null,
                isUseExpectTemperatureControl : false,
                expect_temperature : '',
                rise_time: '',
                drop_time: '',
                expect_temperature_max_value : '',
                expect_temperature_min_value : '',
                tableSensors: [{
                    selectedAutoControlSensor : null,
                    sensor : null,
                    sensor_id : '',
                    sensorUnit : '',
                    max_value : '',
                    min_value : '',
                    percentage : '',
                }],
            }],
            autoControlSteps: []
        };

        Object.keys(handler).forEach(func => {
            this[func] = handler[func].bind(this);
        });

        Object.keys(initHandler).forEach(func => {
            this[func] = initHandler[func].bind(this);
        });
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    componentWillUnmount() {
        document.removeEventListener('keyup', this.closeOnEscape);
    }

    init = async (data) => {
        this.setState({
            selectedDateOption: JSON.parse(JSON.stringify(CONSTANT.selectDateOption))
        });
        let temp = {
            isUpdate: data.isUpdate
        };

        if(data.selectedControlId && data.selectedControlKey){
            temp.selectedControlId = data.selectedControlId;
            temp.selectedControlKey = data.selectedControlKey;

            if(temp.selectedControlId.window_position){
                temp.window_position = temp.selectedControlId.window_position;
            }

            if(temp.selectedControlId.wind_direction_type){
                temp.wind_direction = temp.selectedControlId.wind_direction;
            }
        }

        await this.setState(temp);
        await this.findControlMinMaxRanges();
        await this.findPBands();

        this.findInnerTempSensor();
        this.findSensors();
        this.findControlGroups();
        this.findMotors();
        this.findPowers();
        this.findAutoControlSensors();
        this.findControls();
        this.findNoWindDirectionSensors();
        this.setState({selectHourOptions});
        this.setState({selectMinuteOptions});

        if (data.isUpdate === true) {
            this.setState({
                autoControlId: data.data.id
            });
            this.initializeDataToForm(data.data);
        }
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        e && e.preventDefault();
        this.setState(Object.assign({}, INIT_STATE));
        this.props.callback && this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findInnerTempSensor = () => {
        return new Promise(async (resolve, reject) => {
            const _this = this;
            let query = {
                house_id: this.props.getHouseId(),
                types:['temperature', 'korinsTemperature'].join(','),
                position : 'in'
            };
            sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
                if (status === 200) {
                    _this.setState({
                        TemperatureSensor: _this.generateSensorOption(data.data.rows)
                    });
                } else if (status === 404) {
                    _this.setState({
                        TemperatureSensor: []
                    });
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        });
    };

    findPBands = () => {
        return new Promise(async (resolve, reject) => {
            try {
                const _this = this;
                pBandsManager.findAll({
                    house_id: this.props.getHouseId()
                }, noSession(this.props.showDialog)((status, data) => {
                    if (status === 200) {
                        let pBands = [];
                        data.data.rows.forEach(pBand => {
                            pBands.push({
                                label: pBand.p_band_name,
                                value: pBand.id
                            });
                        });
                        _this.setState({pBands}, () => resolve(true));
                    } else if (status === 404) {
                        _this.setState({
                            pBands: []
                        }, () => resolve(true));
                    } else {
                        _this.props.alertError(status, data);
                        resolve(true);
                    }
                }));
            } catch (e) {
                reject(e);
            }
        });
    };

    findControlMinMaxRanges = () => {
        return new Promise(async (resolve, reject) => {
            try {
                const _this = this;
                let query = {
                    house_id: this.props.getHouseId(),
                    // state: 'on'
                };
                controlMinMaxRangesManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
                    if (status === 200) {
                        _this.setState({
                            controlMinMaxRanges: data.data.rows
                        }, () => resolve(true));
                    } else if (status === 404) {
                        _this.setState({
                            controlMinMaxRanges: []
                        }, () => resolve(true));
                    } else {
                        _this.props.alertError(status, data);
                        reject(e);
                    }
                }));
            } catch (e) {
                reject(e);
            }
        });
    };

    findSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            // types: noWindDirectionRequest.join(',')
        };
        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    selectSensorOption: _this.generateSensorOption(data.data.rows)
                });
            } else if (status === 404) {
                _this.setState({
                    selectSensorOption: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findNoWindDirectionSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            types: CONSTANT.noSelectReuquest.join(',')
        };
        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    selectNoSelctSensorOption: _this.generateSensorOption(data.data.rows)
                });
            } else if (status === 404) {
                _this.setState({
                    selectNoSelctSensorOption: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findAutoControlSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            types: CONSTANT.noSelectReuquest.join(',')
        };
        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    selectAutoControlSensorOption: _this.generateSensorOption(data.data.rows)
                });
            } else if (status === 404) {
                _this.setState({
                    selectAutoControlSensorOption: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findControls = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            types: 'motor,power'
        };
        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    selectControlOption: _this.generateControlOption(data.data.rows, 'control')
                });

                _this.generateControlConditionOption(data.data.rows);

            } else if (status === 404) {
                _this.setState({
                    selectControlOption: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findMotors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            type: 'motor'
        };
        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    selectMotorOption: _this.generateControlOption(data.data.rows, 'control')
                });
            } else if (status === 404) {
                _this.setState({
                    selectMotorOption: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findControlGroups = () => {
        const _this = this;
        let query = {
            house_id  : this.props.getHouseId(),
        };

        controlGroupManager.getList(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    selectControlGroupOption: _this.generateControlGroupOption(data.data.rows, 'controlGroup')
                });
            } else if (status === 404) {
                _this.setState({
                    selectControlGroupOption: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }))
    };

    findPowers = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            type: 'power'
        };
        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    selectPowerOption: _this.generateControlOption(data.data.rows, 'power')
                });
            } else if (status === 404) {
                _this.setState({
                    selectPowerOption: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    initializeDataToForm = (item) => {
        this.initCommonForm(item);
        this.initDetailForm(item);
    };

    selectTab = (e, type) => {
        if (this.state.isUpdate) {
            return this.props.showDialog({
                alertText: "자동제어 타입은 수정할 수 없습니다."
            });
        }
        let obj = Object.assign({}, TAB_INIT_OPTION);
        obj.currentTab = type;
        if(this.state.controlOptionFromOutside) {
            obj.selectedControlOption = this.state.controlOptionFromOutside;
            obj.mixControls[0].selectedControlOption = this.state.controlOptionFromOutside;
            obj.stepControls[0].selectedControlOption = this.state.controlOptionFromOutside;


            if(type === TAB_TABLE){
                let selectedControlOption = this.state.controlOptionFromOutside;
                obj.tableSelectedControls = [{
                    selectedPowerOption: null,
                    selectedMotorOption: null,
                }];
                if(selectedControlOption.key === 'control'){
                    if(selectedControlOption.origin.type === 'power'){
                        obj.tableSelectedIsUseTemperature = 'boiler';
                        obj.tableSelectedControls[0].selectedPowerOption = selectedControlOption;
                    }else if(selectedControlOption.origin.type === 'motor'){
                        obj.tableSelectedControls[0].selectedMotorOption = selectedControlOption;
                    }
                }else if(selectedControlOption.key === 'controlGroup'){
                    obj.tableSelectedControls[0].selectedMotorOption = selectedControlOption;
                }
            }

        }
        this.setState(obj);


        if(type === TAB_TABLE){
            this.setTableControls();
        }

    };


    renderFormBySensor = () => {
        const SENSOR = this.state.selectedSensorOption ? this.state.selectedSensorOption.origin : null;
        const SENSOR_TYPE = SENSOR ? (SENSOR.type ? SENSOR.type : SENSOR.sensor.type) : null;
        const SENSOR_UNIT = SENSOR ? (SENSOR.unit ? SENSOR.unit : (SENSOR.sensor ?SENSOR.sensor.unit:'')) : null;

        return SENSOR || this.state.recommendSensor ?
            <Fragment>
                {SENSOR_TYPE && !(SPECIEL_SENSOR_TYPE[SENSOR_TYPE]) ?
                    (
                        <div className="form-flex-wrap">
                            <div className="left-item">
                                <input type="text" className="lc-farm-input" value={this.state.sensorValue}
                                       onChange={e => this.handleInput(e, 'sensorValue')}
                                       placeholder="센서 값"/>
                            </div>
                            <span className="lc-modifier">{SENSOR_UNIT ? SENSOR_UNIT : this.state.recommendSensor && this.state.recommendSensor.unit ? this.state.recommendSensor.unit : null}</span>
                        </div>
                    ) : (this.state.recommendSensor && !(SPECIEL_SENSOR_TYPE[this.state.recommendSensor && this.state.recommendSensor.type ? this.state.recommendSensor.type : null])) ?
                        <div className="form-flex-wrap">
                            <div className="left-item">
                                <input type="text" className="lc-farm-input" value={this.state.sensorValue}
                                       onChange={e => this.handleInput(e, 'sensorValue')}
                                       placeholder="센서 값"/>
                            </div>
                            <span className="lc-modifier">{SENSOR_UNIT ? SENSOR_UNIT : this.state.recommendSensor && this.state.recommendSensor.unit ? this.state.recommendSensor.unit : null}</span>
                        </div>
                        : null
                }

                <div className="form-flex-wrap">
                    <div className="left-item select-module-wrap lc-size-36">
                        <Select value={this.state.selectedRangeConditionOption}
                                placeholder="범위 선택"
                                onChange={this.handleSelectSensorRange}
                                isSearchable={false}
                                options={SENSOR ? returnSensorSelect(SENSOR) : (this.state.recommendSensor ? returnSensorSelect(this.state.recommendSensor) : [])}
                                styles={colourStyles}/>
                    </div>
                    <span className="lc-modifier">이 되면</span>
                </div>
                {
                    SENSOR_TYPE && !(SPECIEL_SENSOR_TYPE[SENSOR_TYPE]) ?
                    (
                        <div className="form-flex-wrap">
                            <div className="lc-theme-checkbox-wrap">
                                <input id="checkAutoControlSensor"
                                       checked={this.state.is_use_option}
                                       onChange={() => {}}
                                       type="checkbox"
                                       name="mix-control-condition"/>
                                <label htmlFor='checkAutoControlSensor'
                                       onClick={e => this.handleAutoControlSensor(e, null, null, TAB_SENSOR)}
                                       className="box-shape-label lc-not-allow"/>
                                <label htmlFor="checkAutoControlSensor"
                                       onClick={e => this.handleAutoControlSensor(e, null, null, TAB_SENSOR)}
                                       className="text-label">자동조절값 설정</label>
                            </div>
                        </div>
                    ) : this.state.recommendSensor && !(SPECIEL_SENSOR_TYPE[this.state.recommendSensor.type && this.state.recommendSensor ? this.state.recommendSensor.type : null]) ?
                        (
                            <div className="form-flex-wrap">
                                <div className="lc-theme-checkbox-wrap">
                                    <input id="checkAutoControlSensor"
                                           checked={this.state.is_use_option}
                                           onChange={() => {}}
                                           type="checkbox"
                                           name="mix-control-condition"/>
                                    <label htmlFor='checkAutoControlSensor'
                                           onClick={e => this.handleAutoControlSensor(e, null, null, TAB_SENSOR)}
                                           className="box-shape-label lc-not-allow"/>
                                    <label htmlFor="checkAutoControlSensor"
                                           onClick={e => this.handleAutoControlSensor(e, null, null, TAB_SENSOR)}
                                           className="text-label">자동조절값 설정</label>
                                </div>
                            </div>
                        ) : null
                }

                {
                    this.state.is_use_option && (
                        <Fragment >
                            <div className="form-flex-wrap auto-control-sensor-setting">
                                <div className="left-item">
                                    <p className="form-flex-label">최대 ({SENSOR_UNIT ? SENSOR_UNIT : (this.state.recommendSensor && this.state.recommendSensor.sensor.unit ? this.state.recommendSensor.sensor.unit : null)})</p>
                                    <input type="text" className="lc-farm-input" value={this.state.option_max_value}
                                           onChange={e => this.handleInput(e, 'option_max_value')}
                                           placeholder="최대 값"/>
                                </div>
                                <div className="left-item">
                                    <p className="form-flex-label">최소 ({SENSOR_UNIT ? SENSOR_UNIT : (this.state.recommendSensor && this.state.recommendSensor.sensor.unit ? this.state.recommendSensor.sensor.unit : null)})</p>
                                    <input type="text" className="lc-farm-input" value={this.state.option_min_value}
                                           onChange={e => this.handleInput(e, 'option_min_value')}
                                           placeholder="최소 값"/>
                                </div>
                            </div>

                            <ul className="auto-control-sensors">
                                {
                                    this.state.autoControlSensorOptions.map((info, index) => {
                                        return (
                                            <li key={index}>

                                                <div className="auto-control-sensor">
                                                    <div className="form-flex-wrap">
                                                        <div className="left-item">
                                                            <p className="form-flex-label">자동조절 기준 센서 {index+1}</p>
                                                            <Select value={this.state.autoControlSensorOptions[index].sensor}
                                                                    onChange={val => this.handleSelectAutoControlStandardSensor(val, null, null, index, TAB_SENSOR)}
                                                                    isSearchable={false}
                                                                    placeholder="센서 선택"
                                                                    options={this.state.selectAutoControlSensorOption}
                                                                    styles={colourStyles}/>
                                                        </div>
                                                        <div className="left-item">
                                                            <div className="remove-button-wrap">
                                                                <button className="theme-text-button" onClick={e => this.removeAutoControlStandardSensors(e, null, null, index, TAB_SENSOR)}>삭제</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {
                                                        this.state.recommendSensor && this.state.recommendSensor.autoControlSensorOptions ?
                                                            <div className={"recommend-wrap"}>
                                                                <p className="recommendTxt">(추천센서 : {this.state.recommendSensor.autoControlSensorOptions[index].sensor_id})</p>
                                                            </div>
                                                            : null
                                                    }
                                                    <div className="form-flex-wrap">
                                                        <div className="left-item">
                                                            <p className="form-flex-label">
                                                                최대 ({this.state.autoControlSensorOptions[index].sensorUnit ? this.state.autoControlSensorOptions[index].sensorUnit : (this.state.recommendSensor && this.state.recommendSensor.autoControlSensorOptions ? this.state.recommendSensor.autoControlSensorOptions[index].sensor.unit : null)})
                                                            </p>
                                                            <input type="text" className="lc-farm-input" value={this.state.autoControlSensorOptions[index].max_value}
                                                                   onChange={e => this.handleStandardSensorValue(e, 'max_value',null, null, index, TAB_SENSOR)}
                                                                   placeholder="최대 값"/>
                                                        </div>
                                                        <div className="left-item">
                                                            <p className="form-flex-label">
                                                                최소 ({this.state.autoControlSensorOptions[index].sensorUnit ? this.state.autoControlSensorOptions[index].sensorUnit : (this.state.recommendSensor && this.state.recommendSensor.autoControlSensorOptions ? this.state.recommendSensor.autoControlSensorOptions[index].sensor.unit : null)})
                                                            </p>
                                                            <input type="text" className="lc-farm-input" value={this.state.autoControlSensorOptions[index].min_value}
                                                                   onChange={e => this.handleStandardSensorValue(e,  'min_value',null, null, index, TAB_SENSOR)}
                                                                   placeholder="최소 값"/>
                                                        </div>
                                                    </div>
                                                    <div className="form-flex-wrap">
                                                        <div className="half-item">
                                                            <p className="form-flex-label">센서{index + 1} 실행 비율(%)</p>
                                                            <input type="text" className="lc-farm-input" value={this.state.autoControlSensorOptions[index].percentage}
                                                                   onChange={e => this.handleStandardSensorValue(e,  'percentage',null, null, index, TAB_SENSOR)}
                                                                   placeholder="실행비율"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                            <div className='button-wrap'>
                                <button className='theme-text-button' onClick={e => this.addAutoControlSensorValue(e, null, null, TAB_SENSOR)}>+ 자동조절 값 추가</button>
                            </div>
                        </Fragment>
                    )
                }
                {
                    this.state.recommendSensorTxt ?
                        <div className={"recommend-wrap"}>
                            <p className="recommendTxt" dangerouslySetInnerHTML={{__html: this.state.recommendSensorTxt}}/>
                        </div>
                        : null
                }
            </Fragment>
         : null
    };

    renderMixFormBySensor = (sensorIndex, originIndex) => {
        let mixControls = JSON.parse(JSON.stringify(this.state.mixControls));
        let mixSensors = mixControls[originIndex].mixSensors;
        let mixSensor = mixSensors[sensorIndex].selectedSensorOption ? mixSensors[sensorIndex].selectedSensorOption.origin : null;
        let sensorType = null;
        let sensorUnit = null;

        if(mixSensor) {
            if(mixSensor.type) {
                sensorType = mixSensor.type;
            } else if (mixSensor.sensor && mixSensor.sensor.type) {
                sensorType = mixSensor.sensor.type;
            }

            if(mixSensor.sensor){
                let unit = mixSensor.sensor.unit.split(':');

                if(unit.length === 1){
                    sensorUnit = mixSensor.sensor.unit;
                }
            }
        }

        return mixSensor ?
            <Fragment>
                {
                    !(SPECIEL_SENSOR_TYPE[mixSensor.type]) &&
                    (
                        <div className="form-flex-wrap">
                            <div className="left-item">
                                <input type="text" className="lc-farm-input"
                                       value={mixSensors[sensorIndex].sensorValue}
                                       onChange={e => this.handleMixSensorValue(e, sensorIndex, originIndex, TAB_MIX)}
                                       placeholder="센서 값"/>
                            </div>
                            <span className="lc-modifier">{sensorUnit}</span>
                        </div>
                    )
                }

                <div className="form-flex-wrap">
                    <div
                        className="left-item select-module-wrap lc-size-36">
                        <Select value={mixSensors[sensorIndex].selectedRangeConditionOption}
                                placeholder="범위 선택"
                                onChange={val => this.handleSelectMixSensorRange(val, sensorIndex, originIndex, TAB_MIX)}
                                isSearchable={false}
                                options={returnSensorSelect(mixSensor)}
                                styles={colourStyles}/>
                    </div>
                    <span className="lc-modifier">이 되면</span>
                </div>
                {
                    !(SPECIEL_SENSOR_TYPE[mixSensor.type]) &&
                    (
                        <div className="form-flex-wrap">
                            <div className="lc-theme-checkbox-wrap">
                                <input id="checkAutoControlSensor"
                                       checked={mixSensors[sensorIndex].is_use_option}
                                       onChange={() => {}}
                                       type="checkbox"
                                       name="mix-control-condition"/>
                                <label htmlFor='checkAutoControlSensor'
                                       onClick={e => this.handleAutoControlSensor(e, sensorIndex, originIndex, TAB_MIX)}
                                       className="box-shape-label lc-not-allow"/>
                                <label htmlFor="checkAutoControlSensor"
                                       onClick={e => this.handleAutoControlSensor(e, sensorIndex, originIndex, TAB_MIX)}
                                       className="text-label">자동조절값 설정</label>
                            </div>
                        </div>
                    )
                }

                {
                    mixSensors[sensorIndex].is_use_option && (
                        <Fragment>
                            <div className="form-flex-wrap auto-control-sensor-setting">
                                <div className="left-item">
                                    <p className="form-flex-label">최대 {sensorUnit ? '(' + sensorUnit + ')' : null}</p>
                                    <input type="text" className="lc-farm-input" value={mixSensors[sensorIndex].option_max_value}
                                           onChange={e => this.handleStandardSensor(e,  'option_max_value',originIndex, sensorIndex, TAB_MIX)}
                                    />
                                </div>
                                <div className="left-item">
                                    <p className="form-flex-label">최소 {sensorUnit ? '(' + sensorUnit + ')' : null}</p>
                                    <input type="text" className="lc-farm-input" value={mixSensors[sensorIndex].option_min_value}
                                           onChange={e => this.handleStandardSensor(e,  'option_min_value',originIndex, sensorIndex, TAB_MIX)}
                                    />
                                </div>
                            </div>

                            <ul className="auto-control-sensors">
                                {
                                    mixSensors[sensorIndex].autoControlSensorOptions.map((info, index) => {
                                        return (
                                            <li key={index}>
                                                <div className="auto-control-sensor">
                                                    <div className="form-flex-wrap">
                                                        <div className="left-item">
                                                            <p className="form-flex-label">자동조절 기준 센서 {index+1}</p>
                                                            <Select value={mixSensors[sensorIndex].autoControlSensorOptions[index].sensor}
                                                                    onChange={val => this.handleSelectAutoControlStandardSensor(val, originIndex, sensorIndex, index, TAB_MIX)}
                                                                    isSearchable={false}
                                                                    placeholder="센서 선택"
                                                                    options={this.state.selectAutoControlSensorOption}
                                                                    styles={colourStyles}/>
                                                        </div>
                                                        <div className="left-item">
                                                            <div className="remove-button-wrap">
                                                                <button className="theme-text-button" onClick={e => this.removeAutoControlStandardSensors(e, originIndex, sensorIndex, index, TAB_MIX)}>삭제</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="form-flex-wrap">
                                                        <div className="left-item">
                                                            <p className="form-flex-label">최대 {mixSensors[sensorIndex].autoControlSensorOptions[index].sensorUnit ?  '(' + mixSensors[sensorIndex].autoControlSensorOptions[index].sensorUnit + ')' : null}</p>
                                                            <input type="text" className="lc-farm-input" value={mixSensors[sensorIndex].autoControlSensorOptions[index].max_value}
                                                                   onChange={e => this.handleStandardSensorValue(e,  'max_value',originIndex, sensorIndex, index, TAB_MIX)}
                                                                   placeholder="최대 값"
                                                            />
                                                        </div>
                                                        <div className="left-item">
                                                            <p className="form-flex-label">최소 {mixSensors[sensorIndex].autoControlSensorOptions[index].sensorUnit ?  '(' + mixSensors[sensorIndex].autoControlSensorOptions[index].sensorUnit + ')' : null}</p>
                                                            <input type="text" className="lc-farm-input" value={mixSensors[sensorIndex].autoControlSensorOptions[index].min_value}
                                                                   onChange={e => this.handleStandardSensorValue(e,  'min_value',originIndex, sensorIndex, index, TAB_MIX)}
                                                                   placeholder="최소 값"/>
                                                        </div>
                                                    </div>
                                                    <div className="form-flex-wrap">
                                                        <div className="half-item">
                                                            <p className="form-flex-label">센서{index + 1} 실행 비율(%)</p>
                                                            <input type="text" className="lc-farm-input" value={mixSensors[sensorIndex].autoControlSensorOptions[index].percentage}
                                                                   onChange={e => this.handleStandardSensorValue(e,  'percentage',originIndex, sensorIndex, index, TAB_MIX)}
                                                                   placeholder="실행비율"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                            <div className='button-wrap'>
                                <button className='theme-text-button' onClick={e => this.addAutoControlSensorValue(e, originIndex, sensorIndex, TAB_MIX)}>+ 자동조절 값 추가</button>
                            </div>
                        </Fragment>
                    )
                }
            </Fragment>
            : null
    };

    renderStepFormBySensor = (sensorIndex, originIndex) => {
        let stepControls = JSON.parse(JSON.stringify(this.state.stepControls));
        let stepSensors = stepControls[originIndex].stepSensors;
        let stepSensor = stepSensors[sensorIndex].selectedSensorOption ? stepSensors[sensorIndex].selectedSensorOption.origin : null;
        let sensorType = null;
        let sensorUnit = null;
        if(stepSensor) {
            if(stepSensor.type) {
                sensorType = stepSensor.type;
            } else if (stepSensor.sensor && stepSensor.sensor.type) {
                sensorType = stepSensor.sensor.type;
            }

            if(stepSensor.sensor){

                let unit = stepSensor.sensor.unit.split(':');

                if(unit.length === 1){
                    sensorUnit = stepSensor.sensor.unit;
                }
            }
        }

        return stepSensor ?
            <Fragment>
                {
                    !(SPECIEL_SENSOR_TYPE[stepSensor.type]) &&
                    (
                        <div className="form-flex-wrap">
                            <div className="left-item">
                                <input type="text" className="lc-farm-input"
                                       value={stepSensors[sensorIndex].sensorValue}
                                       onChange={e => this.handleMixSensorValue(e, sensorIndex, originIndex, TAB_STEP)}
                                       placeholder="센서 값"/>
                            </div>
                            <span className="lc-modifier">{sensorUnit}</span>
                        </div>
                    )
                }

                <div className="form-flex-wrap">
                    <div className="left-item select-module-wrap lc-size-36">
                        <Select value={stepSensors[sensorIndex].selectedRangeConditionOption}
                                placeholder="범위 선택"
                                onChange={val => this.handleSelectMixSensorRange(val, sensorIndex, originIndex, TAB_STEP)}
                                isSearchable={false}
                                options={returnSensorSelect(stepSensor)}
                                styles={colourStyles}/>
                    </div>
                    <span className="lc-modifier">이 되면</span>
                </div>
                {
                    !(SPECIEL_SENSOR_TYPE[sensorType]) &&
                    (
                        <div className="form-flex-wrap">
                            <div className="lc-theme-checkbox-wrap">
                                <input id="checkAutoControlSensor"
                                       checked={stepSensors[sensorIndex].is_use_option}
                                       onChange={() => {}}
                                       type="checkbox"
                                       name="mix-control-condition"/>
                                <label htmlFor='checkAutoControlSensor'
                                       onClick={e => this.handleAutoControlSensor(e, sensorIndex, originIndex, TAB_STEP)}
                                       className="box-shape-label lc-not-allow"/>
                                <label htmlFor="checkAutoControlSensor"
                                       onClick={e => this.handleAutoControlSensor(e, sensorIndex, originIndex, TAB_STEP)}
                                       className="text-label">자동조절값 설정</label>
                            </div>
                        </div>
                    )
                }
                {
                    stepSensors[sensorIndex].is_use_option && (
                        <Fragment >
                            <div className="form-flex-wrap auto-control-sensor-setting">
                                <div className="left-item">
                                    <p className="form-flex-label">최대 {sensorUnit ? '(' + sensorUnit + ')' : null}</p>
                                    <input type="text" className="lc-farm-input" value={stepSensors[sensorIndex].option_max_value}
                                           onChange={e => this.handleStandardSensor(e,  'option_max_value',originIndex, sensorIndex, TAB_STEP)}/>
                                </div>
                                <div className="left-item">
                                    <p className="form-flex-label">최소 {sensorUnit ? '(' + sensorUnit + ')' : null}</p>
                                    <input type="text" className="lc-farm-input" value={stepSensors[sensorIndex].option_min_value}
                                           onChange={e => this.handleStandardSensor(e,  'option_min_value',originIndex, sensorIndex, TAB_STEP)}
                                    />
                                </div>
                            </div>

                            <ul className="auto-control-sensors">
                                {
                                    stepSensors[sensorIndex].autoControlSensorOptions.map((info, index) => {
                                        return (
                                            <li key={index}>

                                                <div className="auto-control-sensor">
                                                    <div className="form-flex-wrap">
                                                        <div className="left-item">
                                                            <p className="form-flex-label">자동조절 기준 센서 {index+1}</p>
                                                            <Select value={stepSensors[sensorIndex].autoControlSensorOptions[index].sensor}
                                                                    onChange={val => this.handleSelectAutoControlStandardSensor(val, originIndex, sensorIndex, index, TAB_STEP)}
                                                                    isSearchable={false}
                                                                    placeholder="센서 선택"
                                                                    options={this.state.selectAutoControlSensorOption}
                                                                    styles={colourStyles}/>
                                                        </div>
                                                        <div className="left-item">
                                                            <div className="remove-button-wrap">
                                                                <button className="theme-text-button" onClick={e => this.removeAutoControlStandardSensors(e, originIndex, sensorIndex, index, TAB_STEP)}>삭제</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="form-flex-wrap">
                                                        <div className="left-item">
                                                            <p className="form-flex-label">최대 {stepSensors[sensorIndex].autoControlSensorOptions[index].sensorUnit ?  '(' + stepSensors[sensorIndex].autoControlSensorOptions[index].sensorUnit + ')' : null}</p>
                                                            <input type="text" className="lc-farm-input" value={stepSensors[sensorIndex].autoControlSensorOptions[index].max_value}
                                                                   onChange={e => this.handleStandardSensorValue(e,  'max_value',originIndex, sensorIndex, index, TAB_STEP)}
                                                                   placeholder="센서 값"/>
                                                        </div>
                                                        <div className="left-item">
                                                            <p className="form-flex-label">최소 {stepSensors[sensorIndex].autoControlSensorOptions[index].sensorUnit ?  '(' + stepSensors[sensorIndex].autoControlSensorOptions[index].sensorUnit + ')' : null}</p>
                                                            <input type="text" className="lc-farm-input" value={stepSensors[sensorIndex].autoControlSensorOptions[index].min_value}
                                                                   onChange={e => this.handleStandardSensorValue(e,  'min_value',originIndex, sensorIndex, index, TAB_STEP)}
                                                                   placeholder="센서 값"/>
                                                        </div>
                                                    </div>
                                                    <div className="form-flex-wrap">
                                                        <div className="half-item">
                                                            <p className="form-flex-label">센서{index + 1} 실행 비율(%)</p>
                                                            <input type="text" className="lc-farm-input" value={stepSensors[sensorIndex].autoControlSensorOptions[index].percentage}
                                                                   onChange={e => this.handleStandardSensorValue(e,  'percentage',originIndex, sensorIndex, index, TAB_STEP)}
                                                                   placeholder="센서 값"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                            <div className='button-wrap'>
                                <button className='theme-text-button' onClick={e => this.addAutoControlSensorValue(e, originIndex, sensorIndex, TAB_STEP)}>+ 자동조절 값 추가</button>

                            </div>
                        </Fragment>
                    )
                }
            </Fragment>
            : null
    };

    submit = (e) => {
        e && e.preventDefault();
        const _this = this;
        if (!this.state.auto_control_name) {
            return this.props.showDialog({
                alertText: "제어명을 입력해주세요."
            });
        }

        if(this.state.selectedDayType === "day") {
            if (this.state.selectedDateOption.length === 0) {
                return this.props.showDialog({
                    alertText: "날짜를 선택해주세요."
                });
            }
        }

        if(!this.state.selectedDayType) {
            return this.props.showDialog({
                alertText: "기간 유형을 선택해주세요."
            });
        }

        let form = {
            house_id: this.props.getHouseId(),
            type: this.state.currentTab,
            auto_control_name: this.state.auto_control_name,
        };

        if(this.state.selectedDayType.value === "day") {
            form.is_sun = this.state.is_sun;
            form.is_mon = this.state.is_mon;
            form.is_tue = this.state.is_tue;
            form.is_wed = this.state.is_wed;
            form.is_thur = this.state.is_thur;
            form.is_fri = this.state.is_fri;
            form.is_sat = this.state.is_sat;
            form.start_date = this.state.start_date === null ? null : date(new Date(this.state.start_date), DATE_DISPLAY_FORMAT);
            form.end_date = this.state.end_date === null ? null : date(new Date(this.state.end_date), DATE_DISPLAY_FORMAT);
            form.date_type = "day";
        } else if (this.state.selectedDayType.value === "per") {
            if(!this.state.start_date) {
                return this.props.showDialog({
                    alertText: "시작일을 선택해주세요."
                });
            }
            if(!this.state.per_date) {
                return this.props.showDialog({
                    alertText: "기간 주기를 입력해주세요."
                });
            }
            form.start_date = date(new Date(this.state.start_date), DATE_DISPLAY_FORMAT);
            form.end_date = this.state.end_date === null ? null : date(new Date(this.state.end_date), DATE_DISPLAY_FORMAT);
            form.per_date = parseInt(this.state.per_date);
            form.date_type = "per";
        }

        if (this.state.currentTab === TAB_SENSOR) {
            if (!this.state.selectedControlOption) {
                return _this.props.showDialog({
                    alertText: "기기를 선택해주세요."
                });
            }
            if (!this.state.selectedControlStateOption) {
                return _this.props.showDialog({
                    alertText: "동작을 선택해주세요."
                });
            }
            if (this.state.selectedControlStateOption.hasValue) {
                if (!this.state.selectedControlWayOption) {
                    return _this.props.showDialog({
                        alertText: "동작에 대한 단위를 선택해주세요."
                    });
                }
                let selectedControlWayOptionValue = this.state.selectedControlWayOption.value;
                if (selectedControlWayOptionValue === 'percentage' || selectedControlWayOptionValue === 'time') {
                    if (this.state.settingValue === null || this.state.settingValue === '') {
                        return _this.props.showDialog({
                            alertText: "동작 값을 입력해주세요."
                        });
                    }
                } else if (selectedControlWayOptionValue === 'pBand') {
                    if (!this.state.selectedPBand) {
                        return _this.props.showDialog({
                            alertText: 'P밴드를 선택해주세요.'
                        });
                    }
                    if (!this.state.pBandTemperature) {
                        return _this.props.showDialog({
                            alertText: '희망 온도를 입력해주세요.'
                        });
                    }
                } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                    if (!this.state.selectedPBand) {
                        return _this.props.showDialog({
                            alertText: 'P밴드를 선택해주세요.'
                        });
                    }
                    if (!this.state.pBandTemperature) {
                        return _this.props.showDialog({
                            alertText: '희망 온도를 입력해주세요.'
                        });
                    } else if (!this.state.pBandIntegral) {
                        return _this.props.showDialog({
                            alertText: '적분 %값을 입력해주세요.'
                        });
                    }
                }
            }

            if(this.state.is_use_option){
                if(!this.state.option_max_value){
                    return _this.props.showDialog({
                        alertText: '자동조절 최대값을 입력해주세요.'
                    });
                }

                if(!this.state.option_min_value){
                    return _this.props.showDialog({
                        alertText: '자동조절 최소값을 입력해주세요.'
                    });
                }
                if(!this.state.autoControlSensorOptions.length){
                    return _this.props.showDialog({
                        alertText: '자동조절 기준 센서를 선택해주세요.'
                    });
                }else{
                    let maxPercentage = 0;
                    this.state.autoControlSensorOptions.forEach((data, index) => {
                        if(!data.sensor){
                            return _this.props.showDialog({
                                alertText: `${index + 1} 번 자동조절 기준 센서를 선택해주세요.`
                            });
                        }

                        if(!data.min_value){
                            return _this.props.showDialog({
                                alertText: `${index + 1} 번 자동조절 최소값을 입력해주세요.`
                            });
                        }

                        if(!data.max_value){
                            return _this.props.showDialog({
                                alertText: `${index + 1} 번 자동조절 최대값을 입력해주세요.`
                            });
                        }

                        if(!data.percentage){
                            return _this.props.showDialog({
                                alertText: `${index + 1} 번 실행비율을 입력해주세요.`
                            });
                        }else{
                            maxPercentage += parseInt(data.percentage);
                        }

                    });

                    if(maxPercentage !== 100){
                        return _this.props.showDialog({
                            alertText: '총 실행 비율의 합이 100이 되어야 합니다.'
                        });
                    }
                }
            }

            if(this.state.is_temperature_control_option){
                if(!this.state.rise_time){
                    return _this.props.showDialog({
                        alertText: '상승시간을 입력해주세요.'
                    });
                }

                if(!this.state.drop_time){
                    return _this.props.showDialog({
                        alertText: '하강시간을 입력해주세요.'
                    });
                }

                if(this.state.selectedControlOption && this.state.selectedControlOption.origin.type === 'power' && !this.state.expect_temperature){
                    return _this.props.showDialog({
                        alertText: '희망온도 입력해주세요.'
                    });
                }

                if(this.state.selectedControlOption && this.state.selectedControlOption.origin.type === 'power' && !this.state.selectedTemperatureSensor){
                    return _this.props.showDialog({
                        alertText: '온도센서를 선택해주세요.'
                    });
                }
            }

            if (!this.state.selectedSensorOption) {
                return _this.props.showDialog({
                    alertText: "센서를 선택해주세요."
                });
            }

            if (!SPECIEL_SENSOR_TYPE[this.state.selectedSensorOption.origin.type]) {
                if (this.state.sensorValue === null) {
                    return _this.props.showDialog({
                        alertText: "센서조건 값을 입력해주세요."
                    });
                }

                if (!this.state.selectedRangeConditionOption) {
                    return _this.props.showDialog({
                        alertText: "범위를 선택해주세요."
                    });
                }
            }

            let autoControlItems = [];
            let autoControlSensor = {
                sensor_id: this.state.selectedSensorOption.value,
                is_use_option : this.state.is_use_option,
            };

            if(this.state.option_max_value) autoControlSensor.option_max_value =  this.state.option_max_value;
            if(this.state.option_min_value) autoControlSensor.option_min_value = this.state.option_min_value;

            if(this.state.autoControlSensorOptions.length){
                let autoControlSensorOptions = [];

                this.state.autoControlSensorOptions.forEach((data, index) => {
                    if(data.sensor && data.min_value && data.max_value && data.percentage){
                        autoControlSensorOptions.push({
                            sensor_id : data.sensor.value,
                            min_value : data.min_value,
                            max_value : data.max_value,
                            percentage : data.percentage
                        })
                    }
                });

                autoControlSensor.autoControlSensorOptions = autoControlSensorOptions;
            }

            let the_type = this.state.selectedSensorOption.origin.type ? this.state.selectedSensorOption.origin.type : this.state.selectedSensorOption.origin.sensor.type;

            if (SPECIEL_SENSOR_TYPE[the_type]) {
                autoControlSensor.value = this.state.selectedRangeConditionOption.value;
                autoControlSensor.condition = 'equal';

            } else {
                autoControlSensor.value = this.state.sensorValue;
                autoControlSensor.condition = this.state.selectedRangeConditionOption.value;
            }

            let autoControlItem = {
                autoControlSensors: [autoControlSensor],
            };
            const selectedControlOption = this.state.selectedControlOption;
            const selectedControlOptionValue = this.state.selectedControlOption.value;

            if(selectedControlOption.key === 'controlGroup'){
                autoControlItem.wind_direction_type = selectedControlOptionValue.wind_direction_type;
                autoControlItem.window_position = selectedControlOptionValue.window_position;
            } else {
                autoControlItem.control_id = selectedControlOptionValue;
            }

            if (this.state.selectedDelayWay.value === CONSTANT.delayWayOptions[2].value) {
                autoControlItem.period_type = CONSTANT.delayWayOptions[2].value;
                autoControlItem.delay = null;
            } else {
                autoControlItem.period_type = 'oneMinute';
                if (this.state.delay !== null) autoControlItem.delay = this.state.delay * 60;
            }
            if (this.state.active_count !== null) autoControlItem.active_count = this.state.active_count;

            //환기/난방설정
            autoControlItem.is_temperature_control_option = this.state.is_temperature_control_option;
            if (this.state.rise_time !== null && this.state.rise_time !== '') autoControlItem.rise_time = this.state.rise_time;
            if (this.state.drop_time !== null && this.state.drop_time !== '') autoControlItem.drop_time = this.state.drop_time;
            if (this.state.expect_temperature !== null && this.state.expect_temperature !== '') autoControlItem.expect_temperature = this.state.expect_temperature;
            if (this.state.selectedTemperatureSensor) autoControlItem.expect_temperature_sensor_id = this.state.selectedTemperatureSensor.value;

            if (this.state.selectedControlStateOption.hasValue) {
                //부분열기, 부분닫기, 예약켜짐, 예약꺼짐
                autoControlItem.state = this.state.selectedControlStateOption.value.split('-partial')[0];

                if (this.state.selectedControlOption.origin.type === TYPE_MOTOR || this.state.selectedControlOption.origin.type === TYPE_WINDDIRECTION) {
                    let selectedControlWayOptionValue = this.state.selectedControlWayOption.value;
                    if (selectedControlWayOptionValue === 'time' || selectedControlWayOptionValue === 'percentage') {
                        autoControlItem[selectedControlWayOptionValue] = this.state.settingValue;
                    } else if (selectedControlWayOptionValue === 'pBand') {
                        autoControlItem.p_band_id = this.state.selectedPBand.value;
                        autoControlItem.p_band_temperature = this.state.pBandTemperature;
                    } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                        autoControlItem.p_band_id = this.state.selectedPBand.value;
                        autoControlItem.p_band_temperature = this.state.pBandTemperature;
                        autoControlItem.p_band_integral = this.state.pBandIntegral;
                    }
                } else if (this.state.selectedControlOption.origin.type === TYPE_POWER) {
                    autoControlItem.time = this.returnPowerTimeByState(this.state.selectedControlWayOption.value, this.state.settingValue);

                }
            } else {
                //열기, 닫기, 켜짐, 꺼짐
                autoControlItem.state = this.state.selectedControlStateOption.value;
                if (this.state.selectedControlOption.origin.type === TYPE_MOTOR || this.state.selectedControlOption.origin.type === TYPE_WINDDIRECTION) {
                    autoControlItem.percentage = "200";
                }
            }

            autoControlItem.is_min_max_range = this.state.is_min_max_range;
            if (this.state.minMaxRanges.length) {
                let autoControlItemMinMaxes = [];
                this.state.minMaxRanges.forEach(minMaxRange => {
                    if (minMaxRange.percentage) {
                        autoControlItemMinMaxes.push({
                            control_min_max_range_id: minMaxRange.id,
                            state: minMaxRange.useState,
                            percentage: minMaxRange.percentage
                        });
                    }
                });
                if (autoControlItemMinMaxes.length) {
                    autoControlItem.autoControlItemMinMaxes = autoControlItemMinMaxes;
                }
            }

            autoControlItems.push(autoControlItem);
            form.autoControlItems = autoControlItems;

        }

        else if (this.state.currentTab === TAB_CONTROL) {
            if (!this.state.selectedControlOption) {
                return _this.props.showDialog({
                    alertText: "기기를 선택해주세요."
                });
            }
            if (!this.state.selectedControlStateOption) {
                return _this.props.showDialog({
                    alertText: "동작을 선택해주세요."
                });
            }
            if (this.state.selectedControlStateOption.hasValue) {
                if (!this.state.selectedControlWayOption) {
                    return _this.props.showDialog({
                        alertText: "동작에 대한 단위를 선택해주세요."
                    });
                }
                let selectedControlWayOptionValue = this.state.selectedControlWayOption.value;
                if (selectedControlWayOptionValue === 'percentage' || selectedControlWayOptionValue === 'time') {
                    if (this.state.settingValue === null || this.state.settingValue === '') {
                        return _this.props.showDialog({
                            alertText: "동작 값을 입력해주세요."
                        });
                    }
                } else if (selectedControlWayOptionValue === 'pBand') {
                    if (!this.state.selectedPBand) {
                        return _this.props.showDialog({
                            alertText: 'P밴드를 선택해주세요.'
                        });
                    }
                    if (!this.state.pBandTemperature) {
                        return _this.props.showDialog({
                            alertText: '희망 온도를 입력해주세요.'
                        });
                    }
                } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                    if (!this.state.selectedPBand) {
                        return _this.props.showDialog({
                            alertText: 'P밴드를 선택해주세요.'
                        });
                    }
                    if (!this.state.pBandTemperature) {
                        return _this.props.showDialog({
                            alertText: '희망 온도를 입력해주세요.'
                        });
                    } else if (!this.state.pBandIntegral) {
                        return _this.props.showDialog({
                            alertText: '적분 %값을 입력해주세요.'
                        });
                    }
                }
            }

            if (!this.state.selectedControlCondition) {
                return _this.props.showDialog({
                    alertText: "제어를 선택해주세요."
                });
            }

            if (!this.state.selectedControlConditionOption) {
                return _this.props.showDialog({
                    alertText: "기기조건을 선택해주세요."
                });
            }

            if(this.state.is_temperature_control_option){
                if(!this.state.rise_time){
                    return _this.props.showDialog({
                        alertText: '상승시간을 입력해주세요.'
                    });
                }

                if(!this.state.drop_time){
                    return _this.props.showDialog({
                        alertText: '하강시간을 입력해주세요.'
                    });
                }

                if(this.state.selectedControlOption && this.state.selectedControlOption.origin.type === 'power' && !this.state.expect_temperature){
                    return _this.props.showDialog({
                        alertText: '희망온도 입력해주세요.'
                    });
                }

                if(this.state.selectedControlOption && this.state.selectedControlOption.origin.type === 'power' && !this.state.selectedTemperatureSensor){
                    return _this.props.showDialog({
                        alertText: '온도센서를 선택해주세요.'
                    });
                }
            }

            let autoControlItems = [];
            let autoControlControl = {
                control_id: this.state.selectedControlCondition.value,
            };
            if (this.state.selectedControlCondition.origin.type === TYPE_MOTOR || this.state.selectedControlCondition.origin.type === TYPE_WINDDIRECTION) {
                if (!this.state.controlConditionValue) {
                    return _this.props.showDialog({
                        alertText: "기기조건 %값을 입력해주세요."
                    });
                }
                autoControlControl.value = this.state.controlConditionValue;
                autoControlControl.condition = this.state.selectedControlConditionOption.value;
            } else {
                autoControlControl.value = this.state.selectedControlConditionOption.value;
                autoControlControl.condition = 'equal';
            }

            let autoControlItem = {
                autoControlControls: [autoControlControl]
            };
            const selectedControlOption = this.state.selectedControlOption;
            const selectedControlOptionValue = this.state.selectedControlOption.value;

            if(selectedControlOption.key === 'controlGroup'){
                autoControlItem.wind_direction_type = selectedControlOptionValue.wind_direction_type;
                autoControlItem.window_position = selectedControlOptionValue.window_position;
            } else {
                autoControlItem.control_id = selectedControlOptionValue;
            }
            if (this.state.selectedDelayWay.value === CONSTANT.delayWayOptions[2].value) {
                autoControlItem.period_type = CONSTANT.delayWayOptions[2].value;
                autoControlItem.delay = null;
            } else {
                autoControlItem.period_type = 'oneMinute';
                if (this.state.delay !== null) autoControlItem.delay = this.state.delay * 60;
            }

            if (this.state.active_count !== null) autoControlItem.active_count = this.state.active_count;

            //환기/난방설정
            autoControlItem.is_temperature_control_option = this.state.is_temperature_control_option;
            if (this.state.rise_time !== null && this.state.rise_time !== '') autoControlItem.rise_time = this.state.rise_time;
            if (this.state.drop_time !== null && this.state.drop_time !== '') autoControlItem.drop_time = this.state.drop_time;
            if (this.state.expect_temperature !== null && this.state.expect_temperature !== '') autoControlItem.expect_temperature = this.state.expect_temperature;
            if (this.state.selectedTemperatureSensor) autoControlItem.expect_temperature_sensor_id = this.state.selectedTemperatureSensor.value;

            if (this.state.selectedControlStateOption.hasValue) {
                //부분열기, 부분닫기, 예약켜짐, 예약꺼짐
                autoControlItem.state = this.state.selectedControlStateOption.value.split('-partial')[0];

                if (this.state.selectedControlOption.origin.type === TYPE_MOTOR || this.state.selectedControlOption.origin.type === TYPE_WINDDIRECTION) {
                    let selectedControlWayOptionValue = this.state.selectedControlWayOption.value;
                    if (selectedControlWayOptionValue === 'time' || selectedControlWayOptionValue === 'percentage') {
                        autoControlItem[selectedControlWayOptionValue] = this.state.settingValue;
                    } else if (selectedControlWayOptionValue === 'pBand') {
                        autoControlItem.p_band_id = this.state.selectedPBand.value;
                        autoControlItem.p_band_temperature = this.state.pBandTemperature;
                    } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                        autoControlItem.p_band_id = this.state.selectedPBand.value;
                        autoControlItem.p_band_temperature = this.state.pBandTemperature;
                        autoControlItem.p_band_integral = this.state.pBandIntegral;
                    }
                } else if (this.state.selectedControlOption.origin.type === TYPE_POWER) {
                    autoControlItem.time = this.returnPowerTimeByState(this.state.selectedControlWayOption.value, this.state.settingValue);
                }
            } else {
                //열기, 닫기, 켜짐, 꺼짐
                autoControlItem.state = this.state.selectedControlStateOption.value;
                if (this.state.selectedControlOption.origin.type === "motor") {
                    autoControlItem.percentage = "200";
                }
            }

            autoControlItem.is_min_max_range = this.state.is_min_max_range;
            if (this.state.minMaxRanges.length) {
                let autoControlItemMinMaxes = [];
                this.state.minMaxRanges.forEach(minMaxRange => {
                    if (minMaxRange.percentage) {
                        autoControlItemMinMaxes.push({
                            control_min_max_range_id: minMaxRange.id,
                            state: minMaxRange.useState,
                            percentage: minMaxRange.percentage
                        });
                    }
                });
                if (autoControlItemMinMaxes.length) {
                    autoControlItem.autoControlItemMinMaxes = autoControlItemMinMaxes;
                }
            }

            autoControlItems.push(autoControlItem);
            form.autoControlItems = autoControlItems;
        }

        else if (this.state.currentTab === TAB_TIME) {
            if (!this.state.selectedControlOption) {
                return _this.props.showDialog({
                    alertText: "기기를 선택해주세요."
                });
            }
            if (!this.state.selectedControlStateOption) {
                return _this.props.showDialog({
                    alertText: "동작을 선택해주세요."
                });
            }
            if (this.state.selectedControlStateOption.hasValue) {
                if (!this.state.selectedControlWayOption) {
                    return _this.props.showDialog({
                        alertText: "동작에 대한 단위를 선택해주세요."
                    });
                }
                let selectedControlWayOptionValue = this.state.selectedControlWayOption.value;
                if (selectedControlWayOptionValue === 'percentage' || selectedControlWayOptionValue === 'time') {
                    if (this.state.settingValue === null || this.state.settingValue === '') {
                        return _this.props.showDialog({
                            alertText: "동작 값을 입력해주세요."
                        });
                    }
                } else if (selectedControlWayOptionValue === 'pBand') {
                    if (!this.state.selectedPBand) {
                        return _this.props.showDialog({
                            alertText: 'P밴드를 선택해주세요.'
                        });
                    }
                    if (!this.state.pBandTemperature) {
                        return _this.props.showDialog({
                            alertText: '희망 온도를 입력해주세요.'
                        });
                    }
                } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                    if (!this.state.selectedPBand) {
                        return _this.props.showDialog({
                            alertText: 'P밴드를 선택해주세요.'
                        });
                    }
                    if (!this.state.pBandTemperature) {
                        return _this.props.showDialog({
                            alertText: '희망 온도를 입력해주세요.'
                        });
                    } else if (!this.state.pBandIntegral) {
                        return _this.props.showDialog({
                            alertText: '적분 %값을 입력해주세요.'
                        });
                    }
                }
            }

            if(this.state.is_temperature_control_option){
                if(!this.state.rise_time){
                    return _this.props.showDialog({
                        alertText: '상승시간을 입력해주세요.'
                    });
                }

                if(!this.state.drop_time){
                    return _this.props.showDialog({
                        alertText: '하강시간을 입력해주세요.'
                    });
                }

                if(this.state.selectedControlOption && this.state.selectedControlOption.origin.type === 'power' && !this.state.expect_temperature){
                    return _this.props.showDialog({
                        alertText: '희망온도 입력해주세요.'
                    });
                }

                if(this.state.selectedControlOption && this.state.selectedControlOption.origin.type === 'power' && !this.state.selectedTemperatureSensor){
                    return _this.props.showDialog({
                        alertText: '온도센서를 선택해주세요.'
                    });
                }
            }

            let autoControlItems = [];
            let autoControlItem = {};
            const selectedControlOption = this.state.selectedControlOption;
            const selectedControlOptionValue = this.state.selectedControlOption.value;

            if(selectedControlOption.key === 'controlGroup'){
                autoControlItem.wind_direction_type = selectedControlOptionValue.wind_direction_type;
                autoControlItem.window_position = selectedControlOptionValue.window_position;
            }else {
                autoControlItem.control_id = selectedControlOptionValue;
            }

            if (this.state.selectedDelayWay.value === CONSTANT.delayWayOptions[2].value) {
                autoControlItem.period_type = CONSTANT.delayWayOptions[2].value;
                autoControlItem.delay = null;
            } else {
                autoControlItem.period_type = 'oneMinute';
                if (this.state.delay !== null) autoControlItem.delay = this.state.delay * 60;
            }
            if (this.state.active_count !== null && (!this.state.selectedTimeWay || this.state.selectedTimeWay.value !== "single")) autoControlItem.active_count = this.state.active_count;

            //환기/난방설정
            autoControlItem.is_temperature_control_option = this.state.is_temperature_control_option;
            if (this.state.rise_time !== null && this.state.rise_time !== '') autoControlItem.rise_time = this.state.rise_time;
            if (this.state.drop_time !== null && this.state.drop_time !== '') autoControlItem.drop_time = this.state.drop_time;
            if (this.state.expect_temperature !== null && this.state.expect_temperature !== '') autoControlItem.expect_temperature = this.state.expect_temperature;
            if (this.state.selectedTemperatureSensor) autoControlItem.expect_temperature_sensor_id = this.state.selectedTemperatureSensor.value;

            if (this.state.selectedControlStateOption.hasValue) {
                //부분열기, 부분닫기, 예약켜짐, 예약꺼짐
                autoControlItem.state = this.state.selectedControlStateOption.value.split('-partial')[0];

                if (this.state.selectedControlOption.origin.type === TYPE_MOTOR || this.state.selectedControlOption.origin.type === TYPE_WINDDIRECTION) {
                    let selectedControlWayOptionValue = this.state.selectedControlWayOption.value;
                    if (selectedControlWayOptionValue === 'time' || selectedControlWayOptionValue === 'percentage') {
                        autoControlItem[selectedControlWayOptionValue] = this.state.settingValue;
                    } else if (selectedControlWayOptionValue === 'pBand') {
                        autoControlItem.p_band_id = this.state.selectedPBand.value;
                        autoControlItem.p_band_temperature = this.state.pBandTemperature;
                    } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                        autoControlItem.p_band_id = this.state.selectedPBand.value;
                        autoControlItem.p_band_temperature = this.state.pBandTemperature;
                        autoControlItem.p_band_integral = this.state.pBandIntegral;
                    }
                } else if (this.state.selectedControlOption.origin.type === TYPE_POWER) {
                    autoControlItem.time = this.returnPowerTimeByState(this.state.selectedControlWayOption.value, this.state.settingValue);
                }
            } else {
                //열기, 닫기, 켜짐, 꺼짐
                autoControlItem.state = this.state.selectedControlStateOption.value;
                if (this.state.selectedControlOption.origin.type === "motor" || this.state.selectedControlOption.origin.type === 'windDirection') {
                    autoControlItem.percentage = "200";
                }
            }

            if (this.state.selectedTimeWay.value !== "noSelect" && this.state.selectedTimeWay.value !== "sunDate") {
                autoControlItem.is_sun_date = false;
                if (!this.state.selectedStartHour) {
                    return _this.props.showDialog({
                        alertText: "시작 시간을 선택해주세요."
                    });
                }
                if (!this.state.selectedStartMinute) {
                    return _this.props.showDialog({
                        alertText: "시작 분을 선택해주세요."
                    });
                }
                autoControlItem.start_hour = this.state.selectedStartHour.value;
                autoControlItem.start_minute = this.state.selectedStartMinute.value;

                if (this.state.selectedTimeWay.value === "range") {
                    if (!this.state.selectedEndHour) {
                        return _this.props.showDialog({
                            alertText: "종료 시간을 선택해주세요."
                        });
                    }
                    if (!this.state.selectedEndMinute) {
                        return _this.props.showDialog({
                            alertText: "종료 분을 선택해주세요."
                        });
                    }
                    autoControlItem.end_hour = this.state.selectedEndHour.value;
                    autoControlItem.end_minute = this.state.selectedEndMinute.value;
                }
            } else if (this.state.selectedTimeWay.value === "sunDate") {
                if (!this.state.selectedSunDateStartType) {
                    return _this.props.showDialog({
                        alertText: "시작 기준 설정을 선택해주세요."
                    });
                }
                if (!this.state.selectedSunDateEndType) {
                    return _this.props.showDialog({
                        alertText: "종료 기준 설정을 선택해주세요."
                    });
                }
                if (!this.state.selectedSunDateStartCondition) {
                    return _this.props.showDialog({
                        alertText: "시작 기준 + - 를 선택해주세요."
                    });
                }
                if (!this.state.selectedSunDateEndCondition) {
                    return _this.props.showDialog({
                        alertText: "종료 기준 + - 를 선택해주세요."
                    });
                }
                autoControlItem.is_sun_date = true;
                autoControlItem.sun_date_start_type = this.state.selectedSunDateStartType.value;
                autoControlItem.sun_date_end_type = this.state.selectedSunDateEndType.value;
                autoControlItem.sun_date_start_time = parseInt(this.state.selectedSunDateStartHour || 0) * 60 + parseInt(this.state.selectedSunDateStartMinute || 0);
                autoControlItem.sun_date_end_time = parseInt(this.state.selectedSunDateEndHour || 0) * 60 + parseInt(this.state.selectedSunDateEndMinute || 0);
                if (this.state.selectedSunDateStartCondition.value === 'minus') autoControlItem.sun_date_start_time *= -1;
                if (this.state.selectedSunDateEndCondition.value === 'minus') autoControlItem.sun_date_end_time *= -1;
            }
            if (this.state.selectedControlStateOption.hasValue) {
                //부분열기, 부분닫기, 예약켜짐, 예약꺼짐
                autoControlItem.state = this.state.selectedControlStateOption.value.split('-partial')[0];
                //autoControlItem[this.state.selectedControlWayOption.value] = this.state.settingValue;

                if (this.state.selectedControlOption.origin.type === TYPE_MOTOR || this.state.selectedControlOption.origin.type === TYPE_WINDDIRECTION) {
                    let selectedControlWayOptionValue = this.state.selectedControlWayOption.value;
                    if (selectedControlWayOptionValue === 'time' || selectedControlWayOptionValue === 'percentage') {
                        autoControlItem[selectedControlWayOptionValue] = this.state.settingValue;
                    } else if (selectedControlWayOptionValue === 'pBand') {
                        autoControlItem.p_band_id = this.state.selectedPBand.value;
                        autoControlItem.p_band_temperature = this.state.pBandTemperature;
                    } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                        autoControlItem.p_band_id = this.state.selectedPBand.value;
                        autoControlItem.p_band_temperature = this.state.pBandTemperature;
                        autoControlItem.p_band_integral = this.state.pBandIntegral;
                    }
                } else if (this.state.selectedControlOption.origin.type === TYPE_POWER) {
                    autoControlItem.time = this.returnPowerTimeByState(this.state.selectedControlWayOption.value, this.state.settingValue);
                }
            } else {
                //열기, 닫기, 켜짐, 꺼짐
                autoControlItem.state = this.state.selectedControlStateOption.value;
                if (this.state.selectedControlOption.origin.type === "motor" || this.state.selectedControlOption.origin.type === "windDirection") {
                    autoControlItem.percentage = "200";
                }
            }

            autoControlItem.is_min_max_range = this.state.is_min_max_range;
            if (this.state.minMaxRanges.length) {
                let autoControlItemMinMaxes = [];
                this.state.minMaxRanges.forEach(minMaxRange => {
                    if (minMaxRange.percentage) {
                        autoControlItemMinMaxes.push({
                            control_min_max_range_id: minMaxRange.id,
                            state: minMaxRange.useState,
                            percentage: minMaxRange.percentage
                        });
                    }
                });
                if (autoControlItemMinMaxes.length) {
                    autoControlItem.autoControlItemMinMaxes = autoControlItemMinMaxes;
                }
            }

            autoControlItems.push(autoControlItem);
            form.autoControlItems = autoControlItems;
        }
        else if (this.state.currentTab === TAB_MIX) {
            let autoControlItems = [];
            let mixControls = deepcopy(this.state.mixControls);

            for (let index = 0; index < mixControls.length; index++) {
                let item = mixControls[index];
                if (!item.conditional.time && !item.conditional.sensor && !item.conditional.control) {
                    return _this.props.showDialog({
                        alertText: `제어 ${index + 1}번 시간 혹은 센서조건 중 최소 1개를 선택해주세요.`
                    });
                }
                if (!item.selectedControlOption) {
                    return _this.props.showDialog({
                        alertText: `제어 ${index + 1}번 기기를 선택해주세요.`
                    });
                }
                if (!item.selectedControlStateOption) {
                    return _this.props.showDialog({
                        alertText: `제어 ${index + 1}번 동작을 선택해주세요.`
                    });
                }
                if (item.selectedControlStateOption.hasValue) {
                    if (!item.selectedControlWayOption) {
                        return _this.props.showDialog({
                            alertText: `제어 ${index + 1}번 동작에 대한 단위를 선택해주세요.`
                        });
                    }
                    if(item.is_temperature_control_option){
                        if(!item.rise_time){
                            return _this.props.showDialog({
                                alertText: '상승시간을 입력해주세요.'
                            });
                        }

                        if(!item.drop_time){
                            return _this.props.showDialog({
                                alertText: '하강시간을 입력해주세요.'
                            });
                        }

                        if(item.selectedControlOption && item.selectedControlOption.origin.type === 'power' && !item.expect_temperature){
                            return _this.props.showDialog({
                                alertText: '희망온도 입력해주세요.'
                            });
                        }

                        if(item.selectedControlOption && item.selectedControlOption.origin.type === 'power' && !item.selectedTemperatureSensor){
                            return _this.props.showDialog({
                                alertText: '온도센서를 선택해주세요.'
                            });
                        }
                    }

                    let selectedControlWayOptionValue = item.selectedControlWayOption.value;
                    if (selectedControlWayOptionValue === 'percentage' || selectedControlWayOptionValue === 'time') {
                        if (item.settingValue === null || item.settingValue === '') {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 동작 값을 입력해주세요.`
                            });
                        }
                    } else if (selectedControlWayOptionValue === 'pBand') {
                        if (!item.selectedPBand) {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 P밴드를 선택해주세요.`
                            });
                        }
                        if (!item.pBandTemperature) {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 희망 온도를 입력해주세요.`
                            });
                        }
                    } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                        if (!item.selectedPBand) {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 P밴드를 선택해주세요.`
                            });
                        }
                        if (!item.pBandTemperature) {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 희망 온도를 입력해주세요.`
                            });
                        } else if (!item.pBandIntegral) {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 적분 %값을 입력해주세요.`
                            });
                        }
                    }
                }


                if (item.conditional.sensor) {
                    for (let i=0; i<item.mixSensors.length; i++) {
                        const sensorItem = item.mixSensors[i];
                        if (!sensorItem.selectedSensorOption) {
                            return _this.props.showDialog({
                                alertText: `제어 ${i + 1}번 센서를 선택해주세요.`
                            });
                        }
                        if (!SPECIEL_SENSOR_TYPE[sensorItem.selectedSensorOption.origin.type]) {
                            if (sensorItem.sensorValue === null || sensorItem.sensorValue === '') {
                                return _this.props.showDialog({
                                    alertText: `제어 ${i + 1}번 센서조건 값을 입력해주세요.`
                                });
                            }

                            if (!sensorItem.selectedRangeConditionOption) {
                                return _this.props.showDialog({
                                    alertText: `제어 ${i + 1}번 범위를 선택해주세요.`
                                });
                            }
                        }

                        if(sensorItem.is_use_option){
                            if(!sensorItem.option_max_value){
                                return _this.props.showDialog({
                                    alertText: '자동조절 최대값을 입력해주세요.'
                                });
                            }

                            if(!sensorItem.option_min_value){
                                return _this.props.showDialog({
                                    alertText: '자동조절 최소값을 입력해주세요.'
                                });
                            }
                            if(!sensorItem.autoControlSensorOptions.length){
                                return _this.props.showDialog({
                                    alertText: '자동조절 기준 센서를 선택해주세요.'
                                });
                            }else{
                                let maxPercentage = 0;
                                sensorItem.autoControlSensorOptions.forEach((data, index) => {
                                    if(!data.sensor){
                                        return _this.props.showDialog({
                                            alertText: `${index + 1} 번 자동조절 기준 센서를 선택해주세요.`
                                        });
                                    }

                                    if(!data.min_value){
                                        return _this.props.showDialog({
                                            alertText: `${index + 1} 번 자동조절 최소값을 입력해주세요.`
                                        });
                                    }

                                    if(!data.max_value){
                                        return _this.props.showDialog({
                                            alertText: `${index + 1} 번 자동조절 최대값을 입력해주세요.`
                                        });
                                    }

                                    if(!data.percentage){
                                        return _this.props.showDialog({
                                            alertText: `${index + 1} 번 실행비율을 입력해주세요.`
                                        });
                                    }else{
                                        maxPercentage += parseInt(data.percentage);
                                    }
                                })

                                if(maxPercentage !== 100){
                                    return _this.props.showDialog({
                                        alertText: '총 실행 비율의 합이 100이 되어야 합니다.'
                                    });
                                }
                            }
                        }

                    }
                }
                if (item.conditional.control) {
                    for (let i=0; i<item.mixControls.length; i++) {
                        const controlItem = item.mixControls[i];
                        if (!controlItem.selectedControlCondition) {
                            return _this.props.showDialog({
                                alertText: `제어 ${i + 1}번 제어를 선택해주세요.`
                            });
                        } else if (controlItem.selectedControlCondition.origin.type === 'motor' || controlItem.selectedControlCondition.origin.type === 'windDirection' ) {
                            if (!controlItem.controlConditionValue) {
                                return _this.props.showDialog({
                                    alertText: `제어 ${i + 1}번 기기조건 %값을 입력해주세요.`
                                });
                            }
                        }
                        if (!controlItem.selectedControlConditionOption) {
                            return _this.props.showDialog({
                                alertText: `제어 ${i + 1}번 기기조건을 입력해주세요.`
                            });
                        }
                    }
                }
                let obj = {
                    state: item.selectedControlStateOption.value,
                };
                const selectedControlOption = item.selectedControlOption;
                const selectedControlOptionValue = item.selectedControlOption.value;

                if(selectedControlOption.key === 'controlGroup'){
                    obj.wind_direction_type = selectedControlOptionValue.wind_direction_type;
                    obj.window_position = selectedControlOptionValue.window_position;
                }else {
                    obj.control_id = selectedControlOptionValue;
                }

                if (item.selectedDelayWay.value === CONSTANT.delayWayOptions[2].value) {
                    obj.period_type = CONSTANT.delayWayOptions[2].value;
                    obj.delay = null;
                } else {
                    obj.period_type = 'oneMinute';
                    if (item.delay !== null) obj.delay = item.delay * 60;
                }
                if (item.active_count !== null && (!item.selectedTimeWay || item.selectedTimeWay.value !== "single")) obj.active_count = item.active_count;

                //환기/난방설정
                obj.is_temperature_control_option = item.is_temperature_control_option;
                if (item.rise_time !== null && item.rise_time !== '') obj.rise_time = item.rise_time;
                if (item.drop_time !== null && item.drop_time !== '') obj.drop_time = item.drop_time;
                if (item.expect_temperature !== null && item.expect_temperature !== '') obj.expect_temperature = item.expect_temperature;
                if (item.selectedTemperatureSensor) obj.expect_temperature_sensor_id = item.selectedTemperatureSensor.value;

                if (item.sensor_operator) obj.sensor_operator = item.sensor_operator;
                if (item.control_operator) obj.control_operator = item.control_operator;
                if (item.selectedControlStateOption.hasValue) {
                    //부분열기, 부분닫기, 예약켜짐, 예약꺼짐
                    obj.state = item.selectedControlStateOption.value.split('-partial')[0];

                    if (item.selectedControlOption.origin.type === TYPE_MOTOR || item.selectedControlOption.origin.type === TYPE_WINDDIRECTION) {
                        let selectedControlWayOptionValue = item.selectedControlWayOption.value;
                        if (selectedControlWayOptionValue === 'time' || selectedControlWayOptionValue === 'percentage') {
                            obj[selectedControlWayOptionValue] = item.settingValue;
                        } else if (selectedControlWayOptionValue === 'pBand') {
                            obj.p_band_id = item.selectedPBand.value;
                            obj.p_band_temperature = item.pBandTemperature;
                        } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                            obj.p_band_id = item.selectedPBand.value;
                            obj.p_band_temperature = item.pBandTemperature;
                            obj.p_band_integral = item.pBandIntegral;
                        }
                    } else if (item.selectedControlOption.origin.type === TYPE_POWER) {
                        obj.time = this.returnPowerTimeByState(item.selectedControlWayOption.value, item.settingValue);
                    }
                } else {
                    //열기, 닫기, 켜짐, 꺼짐
                    obj.state = item.selectedControlStateOption.value;
                    if (item.selectedControlOption.origin.type === "motor" || item.selectedControlOption.origin.type === "windDirection") {
                        obj.percentage = "200";
                    }
                }

                if (item.conditional.time) {
                    if (!item.selectedTimeWay) {
                        return _this.props.showDialog({
                            alertText: `제어 ${index + 1}번 시간조건을 선택해주세요.`
                        });
                    }
                    if (item.selectedTimeWay.value !== "noSelect" && item.selectedTimeWay.value !== "sunDate") {
                        obj.is_sun_date = false;
                        if (!item.selectedStartHour && !item.selectedStartMinute) {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 시작시간을 선택해주세요.`
                            })
                        }
                        if ((item.selectedStartHour && !item.selectedStartMinute) || (!item.selectedStartHour && item.selectedStartMinute)) {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 시작시간을 전부 선택해주세요.`
                            });
                        }
                        obj.start_hour = item.selectedStartHour.value;
                        obj.start_minute = item.selectedStartMinute.value;
                        if (item.selectedTimeWay.value === "range") {
                            if (!item.selectedEndHour && !item.selectedEndMinute) {
                                return _this.props.showDialog({
                                    alertText: `제어 ${index + 1}번 종료시간을 선택해주세요.`
                                });
                            }
                            if ((item.selectedEndHour && !item.selectedEndMinute) || (!item.selectedEndHour && item.selectedEndMinute)) {
                                return _this.props.showDialog({
                                    alertText: `제어 ${index + 1}번 종료시간을 전부 선택해주세요.`
                                });
                            }
                            obj.end_hour = item.selectedEndHour.value;
                            obj.end_minute = item.selectedEndMinute.value;
                        }
                    } else if (item.selectedTimeWay.value === "sunDate") {
                        if (!item.selectedSunDateStartType) {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 시작 기준 설정을 선택해주세요.`
                            });
                        }
                        if (!item.selectedSunDateEndType) {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 종료 기준 설정을 선택해주세요.`
                            });
                        }
                        if (!item.selectedSunDateStartCondition) {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 시작 기준 + - 를 선택해주세요.`
                            });
                        }
                        if (!item.selectedSunDateEndCondition) {
                            return _this.props.showDialog({
                                alertText: `제어 ${index + 1}번 종료 기준 + - 를 선택해주세요.`
                            });
                        }
                        obj.is_sun_date = true;
                        obj.sun_date_start_type = item.selectedSunDateStartType.value;
                        obj.sun_date_end_type = item.selectedSunDateEndType.value;
                        obj.sun_date_start_time = parseInt(item.selectedSunDateStartHour || 0) * 60 + parseInt(item.selectedSunDateStartMinute || 0);
                        obj.sun_date_end_time = parseInt(item.selectedSunDateEndHour || 0) * 60 + parseInt(item.selectedSunDateEndMinute || 0);
                        if (item.selectedSunDateStartCondition.value === 'minus') obj.sun_date_start_time *= -1;
                        if (item.selectedSunDateEndCondition.value === 'minus') obj.sun_date_end_time *= -1;
                    }
                }
                if (item.conditional.sensor) {
                    let sensorArr = [];
                    item.mixSensors.forEach((sensorItem) => {
                        let sensor = {
                            sensor_id: sensorItem.selectedSensorOption.value,
                            is_use_option : sensorItem.is_use_option,
                        };

                        if(sensorItem.option_min_value) sensor.option_min_value = sensorItem.option_min_value;
                        if(sensorItem.option_max_value) sensor.option_max_value = sensorItem.option_max_value;

                        if(sensorItem.autoControlSensorOptions && sensorItem.autoControlSensorOptions.length){
                            let autoControlSensorOptions = [];
                            sensorItem.autoControlSensorOptions.forEach((data, index) => {
                                if(data.sensor && data.min_value && data.max_value && data.percentage){

                                    autoControlSensorOptions.push({
                                        sensor_id : data.sensor.value,
                                        min_value : data.min_value,
                                        max_value : data.max_value,
                                        percentage : data.percentage
                                    })
                                }
                            });
                            if(autoControlSensorOptions) sensor.autoControlSensorOptions = autoControlSensorOptions;
                        }

                        let the_type = sensorItem.selectedSensorOption.origin.type ? sensorItem.selectedSensorOption.origin.type : sensorItem.selectedSensorOption.origin.sensor.type;
                        if (SPECIEL_SENSOR_TYPE[the_type]) {
                            sensor.value = sensorItem.selectedRangeConditionOption.value;
                            sensor.condition = 'equal';
                        } else {
                            sensor.value = sensorItem.sensorValue;
                            sensor.condition = sensorItem.selectedRangeConditionOption.value;
                        }

                        sensorArr.push(sensor);
                    });
                    obj.autoControlSensors = sensorArr;
                }
                if (item.conditional.control) {
                    obj.autoControlControls = [];
                    item.mixControls.forEach(controlItem => {
                        let autoControlControl = {
                            control_id: controlItem.selectedControlCondition.value
                        };
                        if (controlItem.selectedControlCondition.origin.type === 'motor' || controlItem.selectedControlCondition.origin.type === 'windDirection') {
                            autoControlControl.value = controlItem.controlConditionValue;
                            autoControlControl.condition = controlItem.selectedControlConditionOption.value;
                        } else {
                            autoControlControl.value = controlItem.selectedControlConditionOption.value;
                            autoControlControl.condition = 'equal';
                        }
                        obj.autoControlControls.push(autoControlControl);
                    });
                }

                obj.is_min_max_range = item.is_min_max_range;
                if (item.minMaxRanges.length) {
                    let autoControlItemMinMaxes = [];
                    item.minMaxRanges.forEach(minMaxRange => {
                        if (minMaxRange.percentage) {
                            autoControlItemMinMaxes.push({
                                control_min_max_range_id: minMaxRange.id,
                                state: minMaxRange.useState,
                                percentage: minMaxRange.percentage
                            });
                        }
                    });
                    if (autoControlItemMinMaxes.length) {
                        obj.autoControlItemMinMaxes = autoControlItemMinMaxes;
                    }
                }

                autoControlItems.push(obj);
            }

            form.autoControlItems = autoControlItems;
        }

        else if (this.state.currentTab === TAB_STEP) {
            let autoControlItems = [];
            let autoControlSteps = [];
            let stepControls = deepcopy(this.state.stepControls);
            let stateAutoControlSteps = deepcopy(this.state.autoControlSteps);

            if (stateAutoControlSteps.length > 0) {
                for (let index = 0; index < stateAutoControlSteps.length; index++) {
                    let item = stateAutoControlSteps[index];

                    if (item.delay !== null || item.recount !== null) {
                        if (item.delay === null || item.recount === null) {
                            return _this.props.showDialog({
                                alertText: `${(index + 2)}단계 지연과 반복은 하나만<br>설정할 수 없습니다.`
                            });
                        }
                    }

                    if (!item.selectedControlOption) {
                        return _this.props.showDialog({
                            alertText: `${(index + 2)}단계 기기를 선택해주세요.`
                        });
                    }
                    if (!item.selectedControlStateOption) {
                        return _this.props.showDialog({
                            alertText: `${(index + 2)}단계 동작을 선택해주세요.`
                        });
                    }
                    if (item.selectedControlStateOption.hasValue) {
                        if (!item.selectedControlWayOption) {
                            return _this.props.showDialog({
                                alertText: `${(index + 2)}단계 동작에 대한 단위를 선택해주세요.`
                            });
                        }

                        if (item.is_temperature_control_option) {
                            if (!item.rise_time) {
                                return _this.props.showDialog({
                                    alertText: '상승시간을 입력해주세요.'
                                });
                            }

                            if (!item.drop_time) {
                                return _this.props.showDialog({
                                    alertText: '하강시간을 입력해주세요.'
                                });
                            }

                            if (item.selectedControlOption && item.selectedControlOption.origin.type === 'power' && !item.expect_temperature) {
                                return _this.props.showDialog({
                                    alertText: '희망온도 입력해주세요.'
                                });
                            }

                            if (item.selectedControlOption && item.selectedControlOption.origin.type === 'power' && !item.selectedTemperatureSensor) {
                                return _this.props.showDialog({
                                    alertText: '온도센서를 선택해주세요.'
                                });
                            }
                        }

                        let selectedControlWayOptionValue = item.selectedControlWayOption.value;
                        if (selectedControlWayOptionValue === 'percentage' || selectedControlWayOptionValue === 'time') {
                            if (item.settingValue === null || item.settingValue === '') {
                                return _this.props.showDialog({
                                    alertText: `${index + 2}단계 동작 값을 입력해주세요.`
                                });
                            }
                        } else if (selectedControlWayOptionValue === 'pBand') {
                            if (!item.selectedPBand) {
                                return _this.props.showDialog({
                                    alertText: `${index + 2}단계 P밴드를 선택해주세요.`
                                });
                            }
                            if (!item.pBandTemperature) {
                                return _this.props.showDialog({
                                    alertText: `${index + 2}단계 희망 온도를 입력해주세요.`
                                });
                            }
                        } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                            if (!item.selectedPBand) {
                                return _this.props.showDialog({
                                    alertText: `${index + 2}단계 P밴드를 선택해주세요.`
                                });
                            }
                            if (!item.pBandTemperature) {
                                return _this.props.showDialog({
                                    alertText: `${index + 2}단계 희망 온도를 입력해주세요.`
                                });
                            } else if (!item.pBandIntegral) {
                                return _this.props.showDialog({
                                    alertText: `${index + 2}단계 적분 %값을 입력해주세요.`
                                });
                            }
                        }
                    }


                    let obj = {
                        state: item.selectedControlStateOption.value,
                    };
                    const selectedControlOption = item.selectedControlOption;
                    const selectedControlOptionValue = item.selectedControlOption.value;

                    if (selectedControlOption.key === 'controlGroup') {
                        obj.wind_direction_type = selectedControlOptionValue.wind_direction_type;
                        obj.window_position = selectedControlOptionValue.window_position;
                    } else {
                        obj.control_id = selectedControlOptionValue;
                    }

                    if (item.delay !== null) obj.delay = item.delay * 60;
                    if (item.recount) obj.recount = item.recount - 1;

                    //환기/난방설정
                    obj.is_temperature_control_option = item.is_temperature_control_option;
                    if (item.rise_time !== null && item.rise_time !== '') obj.rise_time = item.rise_time;
                    if (item.drop_time !== null && item.drop_time !== '') obj.drop_time = item.drop_time;
                    if (item.expect_temperature !== null && item.expect_temperature !== '') obj.expect_temperature = item.expect_temperature;
                    if (item.selectedTemperatureSensor) obj.expect_temperature_sensor_id = item.selectedTemperatureSensor.value;

                    if (item.selectedControlStateOption.hasValue) {
                        //부분열기, 부분닫기, 예약켜짐, 예약꺼짐
                        obj.state = item.selectedControlStateOption.value.split('-partial')[0];

                        if (item.selectedControlOption.origin.type === TYPE_MOTOR || item.selectedControlOption.origin.type === TYPE_WINDDIRECTION) {
                            let selectedControlWayOptionValue = item.selectedControlWayOption.value;
                            if (selectedControlWayOptionValue === 'time' || selectedControlWayOptionValue === 'percentage') {
                                obj[selectedControlWayOptionValue] = item.settingValue;
                            } else if (selectedControlWayOptionValue === 'pBand') {
                                obj.p_band_id = item.selectedPBand.value;
                                obj.p_band_temperature = item.pBandTemperature;
                            } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                                obj.p_band_id = item.selectedPBand.value;
                                obj.p_band_temperature = item.pBandTemperature;
                                obj.p_band_integral = item.pBandIntegral;
                            }
                        } else if (item.selectedControlOption.origin.type === TYPE_POWER) {
                            obj.time = this.returnPowerTimeByState(item.selectedControlWayOption.value, item.settingValue);
                        }
                    } else {
                        //열기, 닫기, 켜짐, 꺼짐
                        obj.state = item.selectedControlStateOption.value;
                        if (item.selectedControlOption.origin.type === "motor" || item.selectedControlOption.origin.type === "windDirection") {
                            obj.percentage = "200";
                        }
                    }

                    obj.is_min_max_range = item.is_min_max_range;
                    if (item.minMaxRanges.length) {
                        let autoControlStepMinMaxes = [];
                        item.minMaxRanges.forEach(minMaxRange => {
                            if (minMaxRange.percentage) {
                                autoControlStepMinMaxes.push({
                                    control_min_max_range_id: minMaxRange.id,
                                    state: minMaxRange.useState,
                                    percentage: minMaxRange.percentage
                                });
                            }
                        });
                        if (autoControlStepMinMaxes.length) {
                            obj.autoControlStepMinMaxes = autoControlStepMinMaxes;
                        }
                    }

                    autoControlSteps.push(obj);
                }
            }

            for (let index = 0; index < stepControls.length; index++) {
                let item = stepControls[index];

                if (item.delay !== null || item.recount !== null) {
                    if (item.delay === null || item.recount === null) {
                        return _this.props.showDialog({
                            alertText: `지연과 반복은 하나만<br>설정할 수 없습니다.`
                        });
                    }
                }

                if (!item.conditional.time && !item.conditional.sensor && !item.conditional.control) {
                    return _this.props.showDialog({
                        alertText: `시간 혹은 센서조건 중 최소 1개를 선택해주세요.`
                    });
                }
                if (!item.selectedControlOption) {
                    return _this.props.showDialog({
                        alertText: `기기를 선택해주세요.`
                    });
                }
                if (!item.selectedControlStateOption) {
                    return _this.props.showDialog({
                        alertText: `동작을 선택해주세요.`
                    });
                }
                if (item.selectedControlStateOption.hasValue) {
                    if (!item.selectedControlWayOption) {
                        return _this.props.showDialog({
                            alertText: `동작에 대한 단위를 선택해주세요.`
                        });
                    }
                    let selectedControlWayOptionValue = item.selectedControlWayOption.value;
                    if (selectedControlWayOptionValue === 'percentage' || selectedControlWayOptionValue === 'time') {
                        if (item.settingValue === null || item.settingValue === '') {
                            return _this.props.showDialog({
                                alertText: `동작 값을 입력해주세요.`
                            });
                        }
                    } else if (selectedControlWayOptionValue === 'pBand') {
                        if (!item.selectedPBand) {
                            return _this.props.showDialog({
                                alertText: `P밴드를 선택해주세요.`
                            });
                        }
                        if (!item.pBandTemperature) {
                            return _this.props.showDialog({
                                alertText: `희망 온도를 입력해주세요.`
                            });
                        }
                    } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                        if (!item.selectedPBand) {
                            return _this.props.showDialog({
                                alertText: `P밴드를 선택해주세요.`
                            });
                        }
                        if (!item.pBandTemperature) {
                            return _this.props.showDialog({
                                alertText: `희망 온도를 입력해주세요.`
                            });
                        } else if (!item.pBandIntegral) {
                            return _this.props.showDialog({
                                alertText: `적분 %값을 입력해주세요.`
                            });
                        }
                    }

                    if (item.is_temperature_control_option) {
                        if (!item.rise_time) {
                            return _this.props.showDialog({
                                alertText: '상승시간을 입력해주세요.'
                            });
                        }

                        if (!item.drop_time) {
                            return _this.props.showDialog({
                                alertText: '하강시간을 입력해주세요.'
                            });
                        }

                        if (item.selectedControlOption && item.selectedControlOption.origin.type === 'power' && !item.expect_temperature) {
                            return _this.props.showDialog({
                                alertText: '희망온도 입력해주세요.'
                            });
                        }

                        if (item.selectedControlOption && item.selectedControlOption.origin.type === 'power' && !item.selectedTemperatureSensor) {
                            return _this.props.showDialog({
                                alertText: '온도센서를 선택해주세요.'
                            });
                        }
                    }
                }
                if (item.conditional.sensor) {
                    for (let i = 0; i < item.stepSensors.length; i++) {
                        const sensorItem = item.stepSensors[i];
                        if (!sensorItem.selectedSensorOption) {
                            return _this.props.showDialog({
                                alertText: `센서를 선택해주세요.`
                            });
                        }
                        if (!SPECIEL_SENSOR_TYPE[sensorItem.selectedSensorOption.origin.type]) {
                            if (sensorItem.sensorValue === '' || sensorItem.sensorValue === null) {
                                return _this.props.showDialog({
                                    alertText: `센서조건 값을 입력해주세요.`
                                });
                            }

                            if (!sensorItem.selectedRangeConditionOption) {
                                return _this.props.showDialog({
                                    alertText: `범위를 선택해주세요.`
                                });
                            }
                        }

                        if (sensorItem.is_use_option) {
                            if (!sensorItem.option_max_value) {
                                return _this.props.showDialog({
                                    alertText: '자동조절 최대값을 입력해주세요.'
                                });
                            }

                            if (!sensorItem.option_min_value) {
                                return _this.props.showDialog({
                                    alertText: '자동조절 최소값을 입력해주세요.'
                                });
                            }
                            if (!sensorItem.autoControlSensorOptions.length) {
                                return _this.props.showDialog({
                                    alertText: '자동조절 기준 센서를 선택해주세요.'
                                });
                            } else {
                                let maxPercentage = 0;
                                sensorItem.autoControlSensorOptions.forEach((data, index) => {
                                    if (!data.sensor) {
                                        return _this.props.showDialog({
                                            alertText: `${index + 1} 번 자동조절 기준 센서를 선택해주세요.`
                                        });
                                    }
                                    if (!data.min_value) {
                                        return _this.props.showDialog({
                                            alertText: `${index + 1} 번 자동조절 최소값을 입력해주세요.`
                                        });
                                    }

                                    if (!data.max_value) {
                                        return _this.props.showDialog({
                                            alertText: `${index + 1} 번 자동조절 최대값을 입력해주세요.`
                                        });
                                    }
                                    if (!data.percentage) {
                                        return _this.props.showDialog({
                                            alertText: `${index + 1} 번 실행비율을 입력해주세요.`
                                        });
                                    } else {
                                        maxPercentage += parseInt(data.percentage);
                                    }

                                })

                                if (maxPercentage !== 100) {
                                    return _this.props.showDialog({
                                        alertText: '총 실행 비율의 합이 100이 되어야 합니다.'
                                    });
                                }
                            }
                        }
                    }
                }
                if (item.conditional.control) {
                    for (let i = 0; i < item.stepControls.length; i++) {
                        const controlItem = item.stepControls[i];
                        if (!controlItem.selectedControlCondition) {
                            return _this.props.showDialog({
                                alertText: `제어를 선택해주세요.`
                            });
                        } else if (controlItem.selectedControlCondition.origin.type === 'motor' || controlItem.selectedControlCondition.origin.type === 'windDirection') {
                            if (!controlItem.controlConditionValue) {
                                return _this.props.showDialog({
                                    alertText: `기기조건 %값을 입력해주세요.`
                                });
                            }
                        }
                        if (!controlItem.selectedControlConditionOption) {
                            return _this.props.showDialog({
                                alertText: `기기조건을 입력해주세요.`
                            });
                        }
                    }
                }

                let obj = {
                    state: item.selectedControlStateOption.value,
                };
                const selectedControlOption = item.selectedControlOption;
                const selectedControlOptionValue = item.selectedControlOption.value;
                if (selectedControlOption.key === 'controlGroup') {
                    obj.wind_direction_type = selectedControlOptionValue.wind_direction_type;
                    obj.window_position = selectedControlOptionValue.window_position;
                } else {
                    obj.control_id = selectedControlOptionValue;
                }
                if (item.selectedTimeWay) {
                    if (item.selectedTimeWay.value !== "single") {
                        form.step_delay = this.state.step_delay * 60;
                        obj.active_count = this.state.setCount;
                    } else {
                        form.step_delay = 0;
                        obj.active_count = 0;
                    }
                } else {
                    form.step_delay = this.state.step_delay * 60;
                    obj.active_count = this.state.setCount;
                }
                if (item.delay !== null) obj.delay = item.delay * 60;
                if (item.recount) obj.recount = item.recount - 1;
                if (item.sensor_operator) obj.sensor_operator = item.sensor_operator;
                if (item.control_operator) obj.control_operator = item.control_operator;

                //환기/난방설정
                obj.is_temperature_control_option = item.is_temperature_control_option;
                if (item.rise_time !== null && item.rise_time !== '') obj.rise_time = item.rise_time;
                if (item.drop_time !== null && item.drop_time !== '') obj.drop_time = item.drop_time;
                if (item.expect_temperature !== null && item.expect_temperature !== '') obj.expect_temperature = item.expect_temperature;
                if (item.selectedTemperatureSensor) obj.expect_temperature_sensor_id = item.selectedTemperatureSensor.value;

                if (item.selectedControlStateOption.hasValue) {
                    //부분열기, 부분닫기, 예약켜짐, 예약꺼짐
                    obj.state = item.selectedControlStateOption.value.split('-partial')[0];

                    if (item.selectedControlOption.origin.type === TYPE_MOTOR || item.selectedControlOption.origin.type === TYPE_WINDDIRECTION) {
                        let selectedControlWayOptionValue = item.selectedControlWayOption.value;
                        if (selectedControlWayOptionValue === 'percentage' || selectedControlWayOptionValue === 'time') {
                            obj[selectedControlWayOptionValue] = item.settingValue;
                        } else if (selectedControlWayOptionValue === 'pBand') {
                            obj.p_band_id = item.selectedPBand.value;
                            obj.p_band_temperature = item.pBandTemperature;
                        } else if (selectedControlWayOptionValue === 'pBandIntegral') {
                            obj.p_band_id = item.selectedPBand.value;
                            obj.p_band_temperature = item.pBandTemperature;
                            obj.p_band_integral = item.pBandIntegral;
                        }
                    } else if (item.selectedControlOption.origin.type === TYPE_POWER) {
                        obj.time = this.returnPowerTimeByState(item.selectedControlWayOption.value, item.settingValue);
                    }
                } else {
                    //열기, 닫기, 켜짐, 꺼짐
                    obj.state = item.selectedControlStateOption.value;
                    if (item.selectedControlOption.origin.type === "motor" || item.selectedControlOption.origin.type === "windDirection") {
                        obj.percentage = "200";
                    }
                }

                if (item.conditional.time) {
                    if (!item.selectedTimeWay) {
                        return _this.props.showDialog({
                            alertText: `시간조건을 선택해주세요.`
                        });
                    }
                    if (item.selectedTimeWay.value !== "noSelect" && item.selectedTimeWay.value !== "sunDate") {
                        obj.is_sun_date = false;
                        if (!item.selectedStartHour && !item.selectedStartMinute) {
                            return _this.props.showDialog({
                                alertText: `시작시간을 선택해주세요.`
                            })
                        }
                        if ((item.selectedStartHour && !item.selectedStartMinute) || (!item.selectedStartHour && item.selectedStartMinute)) {
                            return _this.props.showDialog({
                                alertText: `시작시간을 전부 선택해주세요.`
                            });
                        }
                        obj.start_hour = item.selectedStartHour.value;
                        obj.start_minute = item.selectedStartMinute.value;
                        if (item.selectedTimeWay.value === "range") {
                            if (!item.selectedEndHour && !item.selectedEndMinute) {
                                return _this.props.showDialog({
                                    alertText: `종료시간을 선택해주세요.`
                                });
                            }
                            if ((item.selectedEndHour && !item.selectedEndMinute) || (!item.selectedEndHour && item.selectedEndMinute)) {
                                return _this.props.showDialog({
                                    alertText: `종료시간을 전부 선택해주세요.`
                                });
                            }
                            obj.end_hour = item.selectedEndHour.value;
                            obj.end_minute = item.selectedEndMinute.value;
                        }
                    } else if (item.selectedTimeWay.value === "sunDate") {
                        if (!item.selectedSunDateStartType) {
                            return _this.props.showDialog({
                                alertText: `시작 기준 설정을 선택해주세요.`
                            });
                        }
                        if (!item.selectedSunDateEndType) {
                            return _this.props.showDialog({
                                alertText: `종료 기준 설정을 선택해주세요.`
                            });
                        }
                        if (!item.selectedSunDateStartCondition) {
                            return _this.props.showDialog({
                                alertText: `시작 기준 + - 를 선택해주세요.`
                            });
                        }
                        if (!item.selectedSunDateEndCondition) {
                            return _this.props.showDialog({
                                alertText: `종료 기준 + - 를 선택해주세요.`
                            });
                        }
                        obj.is_sun_date = true;
                        obj.sun_date_start_type = item.selectedSunDateStartType.value;
                        obj.sun_date_end_type = item.selectedSunDateEndType.value;
                        obj.sun_date_start_time = parseInt(item.selectedSunDateStartHour || 0) * 60 + parseInt(item.selectedSunDateStartMinute || 0);
                        obj.sun_date_end_time = parseInt(item.selectedSunDateEndHour || 0) * 60 + parseInt(item.selectedSunDateEndMinute || 0);
                        if (item.selectedSunDateStartCondition.value === 'minus') obj.sun_date_start_time *= -1;
                        if (item.selectedSunDateEndCondition.value === 'minus') obj.sun_date_end_time *= -1;
                    }
                }
                if (item.conditional.sensor) {
                    let sensorArr = [];
                    item.stepSensors.forEach((sensorItem) => {

                        let sensor = {
                            sensor_id: sensorItem.selectedSensorOption.value,
                            is_use_option: sensorItem.is_use_option,
                        };

                        if (sensorItem.option_min_value) sensor.option_min_value = sensorItem.option_min_value;
                        if (sensorItem.option_max_value) sensor.option_max_value = sensorItem.option_max_value;

                        if (sensorItem.autoControlSensorOptions && sensorItem.autoControlSensorOptions.length) {
                            let autoControlSensorOptions = [];
                            sensorItem.autoControlSensorOptions.forEach((data, index) => {
                                if (data.sensor && data.min_value && data.max_value && data.percentage) {
                                    autoControlSensorOptions.push({
                                        sensor_id: data.sensor.value,
                                        min_value: data.min_value,
                                        max_value: data.max_value,
                                        percentage: data.percentage
                                    })
                                }
                            });
                            if (autoControlSensorOptions) sensor.autoControlSensorOptions = autoControlSensorOptions;
                        }

                        let the_type = sensorItem.selectedSensorOption.origin.type ? sensorItem.selectedSensorOption.origin.type : sensorItem.selectedSensorOption.origin.sensor.type;
                        if (SPECIEL_SENSOR_TYPE[the_type]) {
                            sensor.value = sensorItem.selectedRangeConditionOption.value;
                            sensor.condition = 'equal';
                        } else {
                            sensor.value = sensorItem.sensorValue;
                            sensor.condition = sensorItem.selectedRangeConditionOption.value;
                        }

                        sensorArr.push(sensor);
                    });
                    obj.autoControlSensors = sensorArr;
                }
                if (item.conditional.control) {
                    obj.autoControlControls = [];
                    item.stepControls.forEach(controlItem => {
                        let autoControlControl = {
                            control_id: controlItem.selectedControlCondition.value
                        };
                        if (controlItem.selectedControlCondition.origin.type === 'motor' || controlItem.selectedControlCondition.origin.type === 'windDirection') {
                            autoControlControl.value = controlItem.controlConditionValue;
                            autoControlControl.condition = controlItem.selectedControlConditionOption.value;
                        } else {
                            autoControlControl.value = controlItem.selectedControlConditionOption.value;
                            autoControlControl.condition = 'equal';
                        }
                        obj.autoControlControls.push(autoControlControl);
                    });
                }

                obj.is_min_max_range = item.is_min_max_range;
                if (item.minMaxRanges.length) {
                    let autoControlItemMinMaxes = [];
                    item.minMaxRanges.forEach(minMaxRange => {
                        if (minMaxRange.percentage) {
                            autoControlItemMinMaxes.push({
                                control_min_max_range_id: minMaxRange.id,
                                state: minMaxRange.useState,
                                percentage: minMaxRange.percentage
                            });
                        }
                    });
                    if (autoControlItemMinMaxes.length) {
                        obj.autoControlItemMinMaxes = autoControlItemMinMaxes;
                    }
                }

                autoControlItems.push(obj);
            }

            if (autoControlSteps.length) form.autoControlSteps = autoControlSteps;
            form.autoControlItems = autoControlItems;
        }else if (this.state.currentTab === TAB_TABLE) {
            if (!this.state.tableSelectedIsUseTemperature) {
                return _this.props.showDialog({
                    alertText: `환기/난방 설정을 선택해주세요.`
                });
            };

            if (this.state.tableSelectedIsUseTemperature === 'vantilation' && !this.state.tableSelectedPband) {
                return _this.props.showDialog({
                    alertText: `P밴드 기준을 선택해주세요.`
                });
            };

            if (this.state.tableSelectedIsUseTemperature === 'boiler' && !this.state.tableSelectedSensor) {
                return _this.props.showDialog({
                    alertText: `센서 기준을 선택해주세요.`
                });
            };
            if (this.state.tableSelectedControls) {
                let tableSelectedControls = this.state.tableSelectedControls;
                tableSelectedControls.forEach((data, index) => {
                    if (!(data.selectedMotorOption || data.selectedPowerOption)) {
                        return _this.props.showDialog({
                            alertText: `${index + 1} 번째 제어장치를 선택해주세요.`
                        });
                    }
                });
            }

            if(!this.state.tableControls){
                return _this.props.showDialog({
                    alertText: `주기 설정 저장을 해주세요.`
                });
            }

            if (this.state.tableControls) {
                let tableControls = this.state.tableControls;
                let tableSelectedControls = this.state.tableSelectedControls;
                let autoControlItems = [];

                for(let i=0; i<tableControls.length; i++){
                    let control = tableControls[i];
                    let beforeControl = tableControls[i-1];

                    if(control.is_using_period.value){
                        if (!control.selectedTimeType){
                            return _this.props.showDialog({
                                alertText: `${i + 1}주기의 작동기준을 선택해주세요.`
                            });
                        }

                        if (control.selectedTimeType.value !== "single" && !control.selectedTimeCondition) {
                            return _this.props.showDialog({
                                alertText: `${i + 1}주기의 기준상태를 선택해주세요.`
                            });
                        }

                        if (control.selectedStartHour === null || control.selectedStartHour === undefined) {
                            return _this.props.showDialog({
                                alertText: `${i + 1}주기의 시작시간을 선택해주세요.`
                            });
                        }

                        if (!control.selectedStartMinute === null || control.selectedStartMinute === undefined) {
                            return _this.props.showDialog({
                                alertText: `${i + 1}주기의 시작분을 선택해주세요.`
                            });
                        }

                        if (!control.expect_temperature) {
                            return _this.props.showDialog({
                                alertText: `${i + 1}주기의 희망온도를 입력해주세요.`
                            });
                        }

                        if (!control.rise_time) {
                            return _this.props.showDialog({
                                alertText: `${i + 1}주기의 상승시간을 입력해주세요.`
                            });
                        }

                        if (!control.drop_time) {
                            return _this.props.showDialog({
                                alertText: `${i + 1}주기의 하강시간을 입력해주세요.`
                            });
                        }

                        if (control.isUseExpectTemperatureControl) {
                            if (!control.expect_temperature_max_value) {
                                return _this.props.showDialog({
                                    alertText: `${i + 1}주기의 희망온도 자동조절 최대값을 입력해주세요.`
                                });
                            }

                            if (!control.expect_temperature_min_value) {
                                return _this.props.showDialog({
                                    alertText: `${i + 1}주기의 희망온도 자동조절 최소값을 입력해주세요.`
                                });
                            }
                            if (control.tableSensors && control.tableSensors.length) {
                                let maxPercentage = 0;
                                control.tableSensors.forEach((sensor, sensorIndex) => {
                                    if (!sensor.selectedAutoControlSensor) {
                                        return _this.props.showDialog({
                                            alertText: `${i + 1}주기의  자동조절 기준 센서 ${sensorIndex + 1}번을 선택해주세요.`
                                        });
                                    }

                                    if (!sensor.max_value) {
                                        return _this.props.showDialog({
                                            alertText: `${i + 1}주기의  자동조절 기준 센서 ${sensorIndex + 1}번의 최대값을 입력해주세요.`
                                        });
                                    }

                                    if (!sensor.min_value ) {
                                        return _this.props.showDialog({
                                            alertText: `${i + 1}주기의  자동조절 기준 센서 ${sensorIndex + 1}번의 최값을 입력해주세요.`
                                        });
                                    }

                                    if (!sensor.percentage) {
                                        return _this.props.showDialog({
                                            alertText: `${i + 1}주기의  자동조절 기준 센서 ${sensorIndex + 1}번의 실행비율을 입력해주세요.`
                                        });
                                    } else {
                                        maxPercentage += parseInt(sensor.percentage);
                                    }

                                    if (sensorIndex === control.tableSensors.length - 1) {
                                        if (maxPercentage !== 100) {
                                            return _this.props.showDialog({
                                                alertText: '총 실행 비율의 합이 100이 되어야 합니다.'
                                            })
                                        }
                                    }
                                });
                            }
                        }
                    }

                    if(control.selectedTimeType && control.expect_temperature && control.rise_time && control.drop_time){
                        if(i > 0 && beforeControl && (!beforeControl.selectedTimeType || !beforeControl.expect_temperature || !beforeControl.rise_time || !beforeControl.drop_time) ){
                            return _this.props.showDialog({
                                alertText: `${beforeControl.label}의 값을 확인해주세요.`
                            });
                        }
                    }

                    tableSelectedControls.forEach((motor, motorIndex) => {
                        if(control.rise_time && control.drop_time){
                            let autoControlItem = {
                                is_temperature_control_option: true,
                                rise_time : control.rise_time,
                                drop_time : control.drop_time,
                                is_using_period : control.is_using_period.value,
                                period : i+1,
                            };

                            if(motor.selectedMotorOption){
                                if(motor.selectedMotorOption.value.wind_direction_type && motor.selectedMotorOption.value.window_position){
                                    autoControlItem.wind_direction_type = motor.selectedMotorOption.value.wind_direction_type;
                                    autoControlItem.window_position = motor.selectedMotorOption.value.window_position;
                                }else{
                                    autoControlItem.control_id = motor.selectedMotorOption.value
                                }
                            }else if(motor.selectedPowerOption){
                                autoControlItem.control_id = motor.selectedPowerOption.value
                            }

                            if(control.selectedTimeType){
                                if(control.selectedTimeType.value !== "single"){
                                    autoControlItem.is_sun_date = true;
                                    autoControlItem.sun_date_start_type = control.selectedTimeType.value;
                                    autoControlItem.sun_date_start_time = parseInt(control.selectedStartHour.value || 0) * 60 + parseInt(control.selectedStartMinute.value || 0);
                                    if (control.selectedTimeCondition.value === 'minus') autoControlItem.sun_date_start_time *= -1;
                                }else{
                                    autoControlItem.start_hour = control.selectedStartHour.value;
                                    autoControlItem.start_minute = control.selectedStartMinute.value;
                                }
                            }

                            if(this.state.tableSelectedIsUseTemperature === 'vantilation'){
                                autoControlItem.state = 'open';
                                autoControlItem.p_band_id = this.state.tableSelectedPband.value;
                                autoControlItem.p_band_temperature = control.expect_temperature;
                            }else{
                                autoControlItem.state = 'on';
                                autoControlItem.expect_temperature = control.expect_temperature;
                                autoControlItem.expect_temperature_sensor_id = this.state.tableSelectedSensor.value;
                            }

                            autoControlItem.is_expect_temperature_option = control.isUseExpectTemperatureControl;
                            if(control.expect_temperature_max_value) autoControlItem.expect_temperature_max_value = control.expect_temperature_max_value;
                            if(control.expect_temperature_min_value) autoControlItem.expect_temperature_min_value = control.expect_temperature_min_value;

                            if(control.tableSensors && control.tableSensors.length){
                                let autoControlExpectTemperatureSensors = [];
                                control.tableSensors.forEach((data, index) => {
                                    if(data.selectedAutoControlSensor && data.min_value && data.max_value && data.percentage){
                                        autoControlExpectTemperatureSensors.push({
                                            sensor_id : data.selectedAutoControlSensor.value,
                                            min_value : data.min_value,
                                            max_value : data.max_value,
                                            percentage : data.percentage
                                        })
                                    }
                                });
                                if(autoControlExpectTemperatureSensors) autoControlItem.autoControlExpectTemperatureSensors = autoControlExpectTemperatureSensors;
                            }

                            autoControlItems.push(autoControlItem);
                        }
                    });
                }

                form.autoControlItems = autoControlItems;
            }
        }
        if (this.state.isUpdate) {
            form.id = this.state.autoControlId;
            autoControlManager.update(form, noSession(this.props.showDialog)((status, data) => {
                if (status === 200) {
                    _this.props.showDialog({
                        alertText: "자동제어가 수정되었습니다.",
                        cancelCallback: () => {
                            _this.close();
                        }
                    })
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        } else {
            autoControlManager.create(form, noSession(this.props.showDialog)((status, data) => {
                if (status === 200) {
                    _this.props.showDialog({
                        alertText: "자동제어가 등록되었습니다.",
                        cancelCallback: () => {
                            _this.close();
                        }
                    })
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        }
    };
    render() {
        return (
            <article id="lcModalAutoControlFormWrap"
                     className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalAutoControlForm" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">자동제어 추가</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">

                                    <div id="autoControlTypeTab" className="auto-control-form-section">
                                        <label className="auto-control-form-label">제어 기준</label>
                                        <div className="type-tab-wrap">
                                            <button
                                                className={"type-tab" + (this.state.currentTab === TAB_SENSOR ? " lc-active" : "")}
                                                onClick={e => this.selectTab(e, TAB_SENSOR)}>센서
                                            </button>
                                            <button
                                                className={"type-tab" + (this.state.currentTab === TAB_TIME ? " lc-active" : "")}
                                                onClick={e => this.selectTab(e, TAB_TIME)}>시간
                                            </button>
                                            <button
                                                className={"type-tab" + (this.state.currentTab === TAB_CONTROL ? " lc-active" : "")}
                                                onClick={e => this.selectTab(e, TAB_CONTROL)}>기기
                                            </button>
                                            <button
                                                className={"type-tab" + (this.state.currentTab === TAB_MIX ? " lc-active" : "")}
                                                onClick={e => this.selectTab(e, TAB_MIX)}>복합
                                            </button>
                                            <button
                                                className={"type-tab" + (this.state.currentTab === TAB_STEP ? " lc-active" : "")}
                                                onClick={e => this.selectTab(e, TAB_STEP)}>단계
                                            </button>
                                            <button
                                                className={"type-tab" + (this.state.currentTab === TAB_TABLE ? " lc-active" : "")}
                                                onClick={e => this.selectTab(e, TAB_TABLE)}>표 설정
                                            </button>
                                        </div>
                                    </div>
                                    <div id="autoControlRecommendWrap" className="auto-control-form-section">
                                        <label className="auto-control-form-label">추천설정</label>
                                        {GetRecommend(this.state, this)}
                                    </div>
                                    <div id="autoControlNameWrap" className="auto-control-form-section">
                                        <label className="auto-control-form-label">제어명</label>
                                        <input type="text" value={this.state.auto_control_name}
                                               onChange={this.handleAutoControlName} className="lc-farm-input"
                                               placeholder="제어명"/>
                                        {
                                            this.state.currentTab === TAB_STEP ? StepDelayByStep(this.state, this) : null
                                        }
                                    </div>

                                    <div id="autoControlDateWrap" className="auto-control-form-section">
                                        <label className="auto-control-form-label">기간 설정</label>
                                        {/*<div className="day-label-radio-wrap">*/}
                                        {/*<label className="auto-control-form-label">기간(선택사항)</label>*/}
                                        {/*<div className="lc-theme-radio-wrap">*/}
                                        {/*<input id='day' value="day"*/}
                                        {/*checked={this.state.dayType === 'day'}*/}
                                        {/*onChange={() => {}}*/}
                                        {/*type="radio" name={`day`}/>*/}
                                        {/*<label onClick={e => this.clickDayType(e, 'day')}*/}
                                        {/*htmlFor={`day`} className="box-shape-label"/>*/}
                                        {/*<label*/}
                                        {/*onClick={e => this.clickDayType(e, 'day')}*/}
                                        {/*htmlFor={`day`} className="text-label">요일</label>*/}
                                        {/*</div>*/}
                                        {/*<div className="lc-theme-radio-wrap">*/}
                                        {/*<div className="lc-theme-radio-wrap">*/}
                                        {/*<input id='per' value="per"*/}
                                        {/*checked={this.state.dayType === 'per'}*/}
                                        {/*onChange={() => {}}*/}
                                        {/*type="radio" name={`per`}/>*/}
                                        {/*<label onClick={e => this.clickDayType(e, 'per')}*/}
                                        {/*htmlFor={`per`} className="box-shape-label"/>*/}
                                        {/*<label*/}
                                        {/*onClick={e => this.clickDayType(e, 'per')}*/}
                                        {/*htmlFor={`per`} className="text-label">per</label>*/}
                                        {/*</div>*/}
                                        {/*</div>*/}
                                        <div>
                                            <div className="select-module-wrap lc-size-36">
                                                <Select value={this.state.selectedDayType}
                                                        placeholder="유형 선택"
                                                        onChange={this.handleDayType}
                                                        isSearchable={false}
                                                        options={CONSTANT.selectDayTypeOption}
                                                        styles={colourStyles}/>
                                            </div>
                                        </div>

                                        {
                                            this.state.selectedDayType ?
                                                <Fragment>
                                                    {
                                                        this.state.selectedDayType.value === "day" ?
                                                            <Fragment>
                                                                <button className="theme-text-button" onClick={this.checkEveryday}>매일</button>
                                                                <div className="select-module-wrap lc-size-36 type-multi">
                                                                    <Select value={this.state.selectedDateOption}
                                                                            closeMenuOnSelect={false}
                                                                            placeholder="요일 선택"
                                                                            isMulti
                                                                            onChange={this.handleSelectDate}
                                                                            isSearchable={false}
                                                                            options={CONSTANT.selectDateOption}
                                                                            styles={colourStyles}/>
                                                                </div>

                                                                <div className="date-picker-container">
                                                                    <div className="date-picker-wrap">
                                                                        <DatePicker dateFormat={"YYYY-MM-DD"}
                                                                                    className="lc-farmlabs-datepicker"
                                                                                    onClickOutside={e => this.closeDatePickerOnTouchOutside(e, 'start_date')}
                                                                                    popperClassName="lc-farmlabs-datepicker-popper"
                                                                                    selected={this.state.start_date}
                                                                                    isClearable={true}
                                                                                    placeholderText="시작일"
                                                                                    onChange={val => this.handleDate(val, "start_date")}/>
                                                                    </div>

                                                                    <div className="date-picker-wrap">
                                                                        <DatePicker dateFormat={"YYYY-MM-DD"}
                                                                                    className="lc-farmlabs-datepicker"
                                                                                    onClickOutside={e => this.closeDatePickerOnTouchOutside(e, 'end_date')}
                                                                                    popperClassName="lc-farmlabs-datepicker-popper right"
                                                                                    selected={this.state.end_date}
                                                                                    isClearable={true}
                                                                                    minDate={this.state.start_date}
                                                                                    placeholderText="종료일"
                                                                                    onChange={val => this.handleDate(val, "end_date")}/>
                                                                    </div>
                                                                </div>
                                                            </Fragment>
                                                            :
                                                            <Fragment>
                                                                <div className="date-picker-container">
                                                                    <div className="date-picker-wrap">
                                                                        <DatePicker dateFormat={"YYYY-MM-DD"}
                                                                                    className="lc-farmlabs-datepicker"
                                                                                    onClickOutside={e => this.closeDatePickerOnTouchOutside(e, 'start_date')}
                                                                                    popperClassName="lc-farmlabs-datepicker-popper"
                                                                                    selected={this.state.start_date}
                                                                                    placeholderText="시작일"
                                                                                    onChange={val => this.handleDate(val, "start_date")}/>
                                                                    </div>

                                                                    <div className="input-wrap">
                                                                        <input type="text" pattern="[0-9]*" onChange={e =>{
                                                                            this.setState({per_date: e.target.validity.valid ? e.target.value : this.state.per_date})}
                                                                        } value={this.state.per_date} className="lc-farm-input" placeholder="간격"/>
                                                                        <span>일 마다</span>
                                                                    </div>
                                                                </div>
                                                                <div className="date-picker-container" style={{marginTop: "8px"}}>
                                                                    <div className="date-picker-wrap">
                                                                        <DatePicker dateFormat={"YYYY-MM-DD"}
                                                                                    className="lc-farmlabs-datepicker"
                                                                                    onClickOutside={e => this.closeDatePickerOnTouchOutside(e, 'end_date')}
                                                                                    popperClassName="lc-farmlabs-datepicker-popper"
                                                                                    selected={this.state.end_date}
                                                                                    isClearable={true}
                                                                                    minDate={this.state.start_date}
                                                                                    placeholderText="종료일"
                                                                                    onChange={val => this.handleDate(val, "end_date")}/>
                                                                    </div>
                                                                </div>
                                                            </Fragment>
                                                    }
                                                </Fragment> : null
                                        }
                                    </div>

                                    {this.state.currentTab !== TAB_TABLE && (

                                        <div id="autoControlSettingWrap" className="auto-control-form-section">

                                            {
                                                this.state.currentTab === TAB_SENSOR &&
                                                (
                                                    <Fragment>
                                                        <div
                                                            className="auto-control-setting-form auto-control-condition-wrap">
                                                            <label className="auto-control-form-label">센서조건</label>
                                                            <div className="lc-setting-form-wrap">
                                                                <div className="form-flex-wrap">
                                                                    <div
                                                                        className="left-item select-module-wrap lc-size-36">
                                                                        <Select value={this.state.selectedSensorOption}
                                                                                placeholder="센서 선택"
                                                                                onChange={this.handleSelectSensor}
                                                                                isSearchable={false}
                                                                                options={this.state.selectSensorOption}
                                                                                styles={colourStyles}/>
                                                                    </div>
                                                                    <span className="lc-modifier">가</span>
                                                                </div>

                                                                {
                                                                    this.renderFormBySensor()
                                                                }

                                                            </div>
                                                        </div>

                                                        <i className="hr"/>

                                                        {ControlOne(this.state, this)}
                                                    </Fragment>
                                                )
                                            }

                                            {
                                                this.state.currentTab === TAB_CONTROL &&
                                                (
                                                    <Fragment>
                                                        <div className="auto-control-setting-form auto-control-condition-wrap">
                                                            <label className="auto-control-form-label">기기조건</label>
                                                            <div className="lc-setting-form-wrap">
                                                                <div className="form-flex-wrap">
                                                                    <div className="left-item select-module-wrap lc-size-36">
                                                                        <Select value={this.state.selectedControlCondition}
                                                                                placeholder="기기 선택"
                                                                                onChange={this.handleSelectControlCondition}
                                                                                isSearchable={false}
                                                                                options={this.state.selectControlConditionOption}
                                                                                styles={colourStyles}/>
                                                                    </div>
                                                                    <span className="lc-modifier">가</span>
                                                                </div>
                                                                {
                                                                    this.state.recommendControlControls ?
                                                                        <div className={"recommend-wrap"}>
                                                                            <p className="recommendTxt">(추천기기 : {this.state.recommendControlControls.control_id})</p>
                                                                        </div>
                                                                        : null
                                                                }
                                                                {
                                                                    this.state.selectedControlCondition || this.state.recommendControlControls ?
                                                                        <Fragment>
                                                                            {
                                                                                ( this.state.selectedControlCondition && this.state.selectedControlCondition.origin.type === 'motor' )|| this.state.recommendControlControls.control.type === 'motor' ? (
                                                                                    <div className="form-flex-wrap">
                                                                                        <div className="left-item">
                                                                                            <input type="text" className="lc-farm-input"
                                                                                                   value={this.state.controlConditionValue}
                                                                                                   onChange={this.handleControlConditionValue}
                                                                                                   placeholder="% 값"/>
                                                                                        </div>
                                                                                        <span className="lc-modifier">%</span>
                                                                                    </div>
                                                                                ) : null
                                                                            }
                                                                            <div className="form-flex-wrap">
                                                                                <div className="left-item select-module-wrap lc-size-36">
                                                                                    <Select value={this.state.selectedControlConditionOption}
                                                                                            placeholder="조건 선택"
                                                                                            onChange={this.handleSelectControlConditionOption}
                                                                                            isSearchable={false}
                                                                                            options={this.state.selectedControlCondition ? CONSTANT.selectControlConditions[this.state.selectedControlCondition.origin.type] : CONSTANT.selectControlConditions[this.state.recommendControlControls.control.type]}
                                                                                            styles={colourStyles}/>
                                                                                </div>
                                                                                <span className="lc-modifier">이 되면</span>
                                                                            </div>
                                                                        </Fragment>
                                                                    : null
                                                                }

                                                            </div>
                                                        </div>

                                                        <i className="hr"/>

                                                        {ControlOne(this.state, this)}
                                                    </Fragment>
                                                )
                                            }

                                            {
                                                this.state.currentTab === TAB_TIME &&
                                                (
                                                    <Fragment>
                                                        <div
                                                            className="auto-control-setting-form auto-control-condition-wrap">
                                                            <label className="auto-control-form-label">시간조건</label>
                                                            <div className="lc-setting-form-wrap">

                                                                <div className="time-form-flex-wrap">
                                                                    <div
                                                                        className="left-item select-module-wrap lc-size-36">
                                                                        <Select value={this.state.selectedTimeWay}
                                                                                placeholder="시간조건 선택"
                                                                                onChange={this.handleSelectTimeWay}
                                                                                isSearchable={false}
                                                                                options={CONSTANT.selectTimeWay}
                                                                                styles={colourStyles}/>
                                                                    </div>
                                                                </div>

                                                                {
                                                                    this.state.selectedTimeWay && this.state.selectedTimeWay.value !== "noSelect" && this.state.selectedTimeWay.value !== "sunDate" && (
                                                                        <div className="lc-time-select-container">
                                                                            {
                                                                                <Fragment>
                                                                                    <div className="lc-time-form-wrap">
                                                                                        <label
                                                                                            className="auto-control-form-label">시작시간</label>
                                                                                        <div className="lc-hour-and-minute-wrap">
                                                                                            <div className="time-item select-module-wrap lc-size-36">
                                                                                                <Select
                                                                                                    value={this.state.selectedStartHour}
                                                                                                    placeholder="시간 선택"
                                                                                                    onChange={e => this.handleTime(e, 'selectedStartHour')}
                                                                                                    isSearchable={false}
                                                                                                    options={selectHourOptions}
                                                                                                    styles={colourStyles}/>
                                                                                            </div>
                                                                                            <div
                                                                                                className="time-item select-module-wrap lc-size-36">
                                                                                                <Select
                                                                                                    value={this.state.selectedStartMinute}
                                                                                                    placeholder="분 선택"
                                                                                                    onChange={e => this.handleTime(e, 'selectedStartMinute')}
                                                                                                    isSearchable={false}
                                                                                                    options={selectMinuteOptions}
                                                                                                    styles={colourStyles}/>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    {
                                                                                        this.state.selectedTimeWay.value === "range" &&
                                                                                        (
                                                                                            <div
                                                                                                className="lc-time-form-wrap">
                                                                                                <label
                                                                                                    className="auto-control-form-label">종료시간</label>
                                                                                                <div className="lc-hour-and-minute-wrap">
                                                                                                    <div
                                                                                                        className="time-item select-module-wrap lc-size-36">
                                                                                                        <Select
                                                                                                            value={this.state.selectedEndHour}
                                                                                                            placeholder="시간 선택"
                                                                                                            onChange={e => this.handleTime(e, 'selectedEndHour')}
                                                                                                            isSearchable={false}
                                                                                                            options={selectHourOptions}
                                                                                                            styles={colourStyles}/>
                                                                                                    </div>
                                                                                                    <div
                                                                                                        className="time-item select-module-wrap lc-size-36">
                                                                                                        <Select
                                                                                                            value={this.state.selectedEndMinute}
                                                                                                            placeholder="분 선택"
                                                                                                            onChange={e => this.handleTime(e, 'selectedEndMinute')}
                                                                                                            isSearchable={false}
                                                                                                            options={selectMinuteOptions}
                                                                                                            styles={colourStyles}/>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        )
                                                                                    }
                                                                                </Fragment>
                                                                            }
                                                                        </div>
                                                                    )
                                                                }

                                                                {
                                                                    this.state.selectedTimeWay && this.state.selectedTimeWay.value === "sunDate" && SunDate({
                                                                        index: null, type: TAB_TIME,

                                                                        ...this.props,
                                                                        ...this.state,

                                                                        handleTime: this.handleSunDateTime,
                                                                        handleInput: this.handleSunDateInput,
                                                                        returnSunDateCalculate: this.returnSunDateCalculate
                                                                    })
                                                                }
                                                            </div>
                                                        </div>

                                                        <i className="hr"/>

                                                        {ControlOne(this.state, this)}
                                                    </Fragment>
                                                )
                                            }

                                            {
                                                this.state.currentTab === TAB_MIX &&
                                                (
                                                    <Fragment>
                                                        <div className="mix-control-container">
                                                            {
                                                                this.state.mixControls.map((item, index) => {
                                                                    return (
                                                                        <div className="mix-control-wrap" key={index}>
                                                                            <div className="mix-control-header mix-control-item-wrap">
                                                                                <span className="mix-control-title">제어 {index + 1}</span>
                                                                                <button className="card-header-button" onClick={e => this.copyThisControl(e, index)}>조건복사</button>
                                                                                {
                                                                                    index > 0 &&
                                                                                    (
                                                                                        <button className="card-header-button" onClick={e => this.removeMixControlWrap(e, index)}>삭제</button>
                                                                                    )
                                                                                }
                                                                            </div>

                                                                            {CheckCondition({
                                                                                type: TAB_MIX,
                                                                                index,
                                                                                item
                                                                            }, {
                                                                                ...this
                                                                            })}

                                                                            {
                                                                                item.conditional.time && (item.conditional.sensor || item.conditional.control) &&
                                                                                (
                                                                                    <div
                                                                                        className="mix-control-item-wrap mix-control-time-condition-wrap">
                                                                                        <label
                                                                                            className="auto-control-form-label">시간조건</label>
                                                                                        <div
                                                                                            className="select-module-wrap lc-size-36">
                                                                                            <Select
                                                                                                value={item.selectedTimeWay}
                                                                                                placeholder="시간조건 선택"
                                                                                                onChange={val => this.handleMixSelectedTimeWay(val, index, TAB_MIX)}
                                                                                                isSearchable={false}
                                                                                                options={CONSTANT.selectTimeWay}
                                                                                                styles={colourStyles}/>
                                                                                        </div>

                                                                                        <div
                                                                                            className="lc-mix-control-time-container">
                                                                                            {
                                                                                                item.selectedTimeWay && item.selectedTimeWay.value !== "noSelect" && item.selectedTimeWay.value !== "sunDate" && (
                                                                                                    <div
                                                                                                        className="lc-time-select-container">
                                                                                                        {
                                                                                                            <Fragment>
                                                                                                                <div
                                                                                                                    className="lc-time-form-wrap">
                                                                                                                    <label
                                                                                                                        className="auto-control-form-label">시작시간</label>
                                                                                                                    <div
                                                                                                                        className="lc-hour-and-minute-wrap">
                                                                                                                        <div
                                                                                                                            className="time-item select-module-wrap lc-size-36">
                                                                                                                            <Select
                                                                                                                                value={item.selectedStartHour}
                                                                                                                                placeholder="시간 선택"
                                                                                                                                onChange={e => this.handleMixTime(e, 'selectedStartHour', index, TAB_MIX)}
                                                                                                                                isSearchable={false}
                                                                                                                                options={selectHourOptions}
                                                                                                                                styles={colourStyles}/>
                                                                                                                        </div>
                                                                                                                        <div
                                                                                                                            className="time-item select-module-wrap lc-size-36">
                                                                                                                            <Select
                                                                                                                                value={item.selectedStartMinute}
                                                                                                                                placeholder="분 선택"
                                                                                                                                onChange={e => this.handleMixTime(e, 'selectedStartMinute', index, TAB_MIX)}
                                                                                                                                isSearchable={false}
                                                                                                                                options={selectMinuteOptions}
                                                                                                                                styles={colourStyles}/>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                {
                                                                                                                    item.selectedTimeWay.value === "range" &&
                                                                                                                    (
                                                                                                                        <div
                                                                                                                            className="lc-time-form-wrap">
                                                                                                                            <label
                                                                                                                                className="auto-control-form-label">종료시간</label>
                                                                                                                            <div
                                                                                                                                className="lc-hour-and-minute-wrap">
                                                                                                                                <div
                                                                                                                                    className="time-item select-module-wrap lc-size-36">
                                                                                                                                    <Select
                                                                                                                                        value={item.selectedEndHour}
                                                                                                                                        placeholder="시간 선택"
                                                                                                                                        onChange={e => this.handleMixTime(e, 'selectedEndHour', index, TAB_MIX)}
                                                                                                                                        isSearchable={false}
                                                                                                                                        options={selectHourOptions}
                                                                                                                                        styles={colourStyles}/>
                                                                                                                                </div>
                                                                                                                                <div
                                                                                                                                    className="time-item select-module-wrap lc-size-36">
                                                                                                                                    <Select
                                                                                                                                        value={item.selectedEndMinute}
                                                                                                                                        placeholder="분 선택"
                                                                                                                                        onChange={e => this.handleMixTime(e, 'selectedEndMinute', index, TAB_MIX)}
                                                                                                                                        isSearchable={false}
                                                                                                                                        options={selectMinuteOptions}
                                                                                                                                        styles={colourStyles}/>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    )
                                                                                                                }
                                                                                                            </Fragment>
                                                                                                        }
                                                                                                    </div>
                                                                                                )
                                                                                            }

                                                                                            {
                                                                                                item.selectedTimeWay && item.selectedTimeWay.value === "sunDate" && SunDate({
                                                                                                    index, type: TAB_MIX,

                                                                                                    ...this.props,
                                                                                                    ...item,

                                                                                                    handleTime: this.handleSunDateTime,
                                                                                                    handleInput: this.handleSunDateInput,
                                                                                                    returnSunDateCalculate: this.returnSunDateCalculate
                                                                                                })
                                                                                            }
                                                                                        </div>
                                                                                    </div>
                                                                                )
                                                                            }

                                                                            {
                                                                                (!item.conditional.time && !item.conditional.sensor && !item.conditional.control) ?
                                                                                    (
                                                                                        <div
                                                                                            className="not-allowed-to-create-wrap">
                                                                                            <p>시간 혹은 센서조건 중 하나를 선택해주세요.</p>
                                                                                        </div>
                                                                                    ) :
                                                                                    (
                                                                                        <div
                                                                                            className="mix-control-item-wrap mix-control-sensor-wrap">
                                                                                            {
                                                                                                item.conditional.time && !item.conditional.sensor && !item.conditional.control &&
                                                                                                (
                                                                                                    <div
                                                                                                        className="auto-control-setting-form auto-control-condition-wrap"
                                                                                                        style={{paddingLeft: 0}}>
                                                                                                        <label
                                                                                                            className="auto-control-form-label">시간조건</label>
                                                                                                        <div
                                                                                                            className="lc-setting-form-wrap">

                                                                                                            <div
                                                                                                                className="time-form-flex-wrap">
                                                                                                                <div
                                                                                                                    className="left-item select-module-wrap lc-size-36">
                                                                                                                    <Select
                                                                                                                        value={item.selectedTimeWay}
                                                                                                                        placeholder="시간조건 선택"
                                                                                                                        onChange={e => this.handleMixSelectedTimeWay(e, index, TAB_MIX)}
                                                                                                                        isSearchable={false}
                                                                                                                        options={CONSTANT.selectTimeWay}
                                                                                                                        styles={colourStyles}/>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            {
                                                                                                                item.selectedTimeWay && item.selectedTimeWay.value !== "noSelect" && item.selectedTimeWay.value !== "sunDate" && (
                                                                                                                    <div
                                                                                                                        className="lc-time-select-container">
                                                                                                                        {
                                                                                                                            <Fragment>
                                                                                                                                <div
                                                                                                                                    className="lc-time-form-wrap">
                                                                                                                                    <label
                                                                                                                                        className="auto-control-form-label">시작시간</label>
                                                                                                                                    <div
                                                                                                                                        className="lc-hour-and-minute-wrap">
                                                                                                                                        <div
                                                                                                                                            className="time-item select-module-wrap lc-size-36">
                                                                                                                                            <Select
                                                                                                                                                value={item.selectedStartHour}
                                                                                                                                                placeholder="시간 선택"
                                                                                                                                                onChange={e => this.handleMixTime(e, 'selectedStartHour', index, TAB_MIX)}
                                                                                                                                                isSearchable={false}
                                                                                                                                                options={selectHourOptions}
                                                                                                                                                styles={colourStyles}/>
                                                                                                                                        </div>
                                                                                                                                        <div
                                                                                                                                            className="time-item select-module-wrap lc-size-36">
                                                                                                                                            <Select
                                                                                                                                                value={item.selectedStartMinute}
                                                                                                                                                placeholder="분 선택"
                                                                                                                                                onChange={e => this.handleMixTime(e, 'selectedStartMinute', index, TAB_MIX)}
                                                                                                                                                isSearchable={false}
                                                                                                                                                options={selectMinuteOptions}
                                                                                                                                                styles={colourStyles}/>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                {
                                                                                                                                    item.selectedTimeWay.value === "range" &&
                                                                                                                                    (
                                                                                                                                        <div
                                                                                                                                            className="lc-time-form-wrap">
                                                                                                                                            <label
                                                                                                                                                className="auto-control-form-label">종료시간</label>
                                                                                                                                            <div
                                                                                                                                                className="lc-hour-and-minute-wrap">
                                                                                                                                                <div
                                                                                                                                                    className="time-item select-module-wrap lc-size-36">
                                                                                                                                                    <Select
                                                                                                                                                        value={item.selectedEndHour}
                                                                                                                                                        placeholder="시간 선택"
                                                                                                                                                        onChange={e => this.handleMixTime(e, 'selectedEndHour', index, TAB_MIX)}
                                                                                                                                                        isSearchable={false}
                                                                                                                                                        options={selectHourOptions}
                                                                                                                                                        styles={colourStyles}/>
                                                                                                                                                </div>
                                                                                                                                                <div
                                                                                                                                                    className="time-item select-module-wrap lc-size-36">
                                                                                                                                                    <Select
                                                                                                                                                        value={item.selectedEndMinute}
                                                                                                                                                        placeholder="분 선택"
                                                                                                                                                        onChange={e => this.handleMixTime(e, 'selectedEndMinute', index, TAB_MIX)}
                                                                                                                                                        isSearchable={false}
                                                                                                                                                        options={selectMinuteOptions}
                                                                                                                                                        styles={colourStyles}/>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    )
                                                                                                                                }
                                                                                                                            </Fragment>
                                                                                                                        }
                                                                                                                    </div>
                                                                                                                )
                                                                                                            }

                                                                                                            {
                                                                                                                item.selectedTimeWay && item.selectedTimeWay.value === "sunDate" && SunDate({
                                                                                                                    index, type: TAB_MIX,

                                                                                                                    ...this.props,
                                                                                                                    ...item,

                                                                                                                    handleTime: this.handleSunDateTime,
                                                                                                                    handleInput: this.handleSunDateInput,
                                                                                                                    returnSunDateCalculate: this.returnSunDateCalculate
                                                                                                                })
                                                                                                            }

                                                                                                        </div>
                                                                                                    </div>
                                                                                                )
                                                                                            }



                                                                                            {
                                                                                                (item.conditional.sensor || item.conditional.control) &&
                                                                                                (
                                                                                                    <div className="lc-mix-sensor-container">
                                                                                                        {
                                                                                                            item.conditional.sensor && SensorCondition({
                                                                                                                type: TAB_MIX,
                                                                                                                index,
                                                                                                                item,
                                                                                                                selectSensorOption: this.state.selectSensorOption
                                                                                                            }, {
                                                                                                                ...this
                                                                                                            })
                                                                                                        }

                                                                                                        {item.conditional.control && item.conditional.sensor && (<hr className="hr"/>)}

                                                                                                        {
                                                                                                            item.conditional.control && ControlCondition({
                                                                                                                type: TAB_MIX,
                                                                                                                index,
                                                                                                                item,
                                                                                                                selectControlConditionOption: this.state.selectControlConditionOption,

                                                                                                            }, {
                                                                                                                ...this
                                                                                                            })
                                                                                                        }
                                                                                                    </div>
                                                                                                )
                                                                                            }

                                                                                            <i className="hr"/>

                                                                                            <div className="auto-control-setting-form auto-control-execution-wrap">
                                                                                                <label className="auto-control-form-label">실행부</label>
                                                                                                <div className="lc-setting-form-wrap">
                                                                                                    <div className="form-flex-wrap">
                                                                                                        <div className="left-item select-module-wrap lc-size-36">
                                                                                                            <Select
                                                                                                                value={item.selectedControlOption}
                                                                                                                placeholder="기기 선택"
                                                                                                                onChange={val => this.handleMixSelectControl(val, index, TAB_MIX)}
                                                                                                                isSearchable={false}
                                                                                                                options={this.state.selectControlOption}
                                                                                                                styles={colourStyles}/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    {
                                                                                                        item.recommendControl ?
                                                                                                            <div className={"recommend-wrap"}>
                                                                                                                <p className="recommendTxt">(추천기기 : {item.recommendControl.control_id})</p>
                                                                                                            </div>
                                                                                                            : null
                                                                                                    }
                                                                                                    {
                                                                                                        item.selectedControlOption || item.recommendControl ? (
                                                                                                            <Fragment>
                                                                                                                <div className="form-flex-wrap">
                                                                                                                    <div className="left-item select-module-wrap lc-size-36">
                                                                                                                        <Select
                                                                                                                            value={item.selectedControlStateOption}
                                                                                                                            placeholder="동작 선택"
                                                                                                                            onChange={val => this.handleMixSelectControlStateOption(val, index, TAB_MIX)}
                                                                                                                            isSearchable={false}
                                                                                                                            options={item.selectedControlOption && item.selectedControlOption.origin.type ? CONTROL_STATE_OPTIONS[item.selectedControlOption.origin.type] : (item.recommendControl.control.type ? CONTROL_STATE_OPTIONS[item.recommendControl.control.type]:[])}
                                                                                                                            styles={colourStyles}/>
                                                                                                                    </div>
                                                                                                                    <span className="lc-modifier">를</span>
                                                                                                                </div>
                                                                                                                {
                                                                                                                    item.selectedControlStateOption && item.selectedControlStateOption.hasValue && (
                                                                                                                        <div className="form-flex-wrap">
                                                                                                                            <div className="left-item select-and-input-wrap">
                                                                                                                                <div className="select-and-input">
                                                                                                                                    <div className="select-module-wrap lc-size-36">
                                                                                                                                        <Select value={item.selectedControlWayOption}
                                                                                                                                                placeholder="단위 선택"
                                                                                                                                                onChange={val => this.handleMixSelectControlWay(val, index, TAB_MIX)}
                                                                                                                                                isSearchable={false}
                                                                                                                                                options={this.state.pBands.length && item.selectedControlOption && (item.selectedControlOption.origin.type === 'motor' || item.selectedControlOption.origin.type === 'windDirection') && item.selectedControlStateOption.value === 'open-partial' ? [
                                                                                                                                                    ...CONTROL_UNIT_OPTIONS.motor,
                                                                                                                                                    ...CONTROL_UNIT_OPTIONS.pBand
                                                                                                                                                ] : item.selectedControlOption && CONTROL_UNIT_OPTIONS[item.selectedControlOption.origin.type] ? CONTROL_UNIT_OPTIONS[item.selectedControlOption.origin.type] : (CONTROL_UNIT_OPTIONS[item.recommendControl.control.type])}
                                                                                                                                                styles={colourStyles}/>
                                                                                                                                    </div>
                                                                                                                                </div>

                                                                                                                                <div className="select-and-input input-auto-control">
                                                                                                                                    {
                                                                                                                                        !item.selectedControlWayOption || (item.selectedControlWayOption.value !== 'pBand' && item.selectedControlWayOption.value !== 'pBandIntegral') && (
                                                                                                                                            <input type="number"
                                                                                                                                                   className="lc-farm-input"
                                                                                                                                                   value={item.settingValue}
                                                                                                                                                   onChange={e => this.handleMixSettingValue(e, index, TAB_MIX)}
                                                                                                                                                   placeholder="입력"/>
                                                                                                                                        )
                                                                                                                                    }
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            {
                                                                                                                                item.selectedControlWayOption && (item.selectedControlWayOption.value !== 'pBand' && item.selectedControlWayOption.value !== 'pBandIntegral') &&
                                                                                                                                (
                                                                                                                                    <span
                                                                                                                                        className="lc-modifier">{item.controlUnitName}</span>
                                                                                                                                )
                                                                                                                            }
                                                                                                                        </div>
                                                                                                                    )
                                                                                                                }

                                                                                                                {
                                                                                                                    item.selectedControlWayOption && (item.selectedControlWayOption.value === 'pBand' || item.selectedControlWayOption.value === 'pBandIntegral') ?
                                                                                                                        ControlPBand({
                                                                                                                            type: TAB_MIX,
                                                                                                                            index,
                                                                                                                            item,
                                                                                                                            pBands : this.state.pBands,
                                                                                                                    }, {
                                                                                                                        ...this
                                                                                                                    }) : null
                                                                                                                }
                                                                                                            </Fragment>
                                                                                                        ) : null
                                                                                                    }

                                                                                                    {
                                                                                                        item.selectedTimeWay && item.selectedTimeWay.value === "single" ?
                                                                                                            null :
                                                                                                            (
                                                                                                                <div className="delay-recount-wrap">
                                                                                                                    <label className="auto-control-form-label">지연 및 실행횟수</label>

                                                                                                                    <div className="delay-recount-input-container">
                                                                                                                        <div className="delay-recount-input-wrap">
                                                                                                                            <div className="select-module-wrap lc-size-36">
                                                                                                                                <Select value={item.selectedDelayWay}
                                                                                                                                        placeholder="실행 타이머"
                                                                                                                                        onChange={val => this.handleMixSelectDelayWay(val, index)}
                                                                                                                                        isSearchable={false}
                                                                                                                                        options={CONSTANT.delayWayOptions}
                                                                                                                                        styles={colourStyles}/>
                                                                                                                            </div>
                                                                                                                        </div>

                                                                                                                        <div className="delay-recount-input-wrap">
                                                                                                                            <div className="select-module-wrap lc-size-36">
                                                                                                                                <Select value={item.selectedActiveCount}
                                                                                                                                        placeholder="실행횟수 선택"
                                                                                                                                        onChange={val => this.handleMixSelectActiveCount(val, index)}
                                                                                                                                        isSearchable={false}
                                                                                                                                        options={CONSTANT.actionCountOptions}
                                                                                                                                        styles={colourStyles}/>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>

                                                                                                                    {
                                                                                                                        item.selectedActiveCount || item.selectedDelayWay ?
                                                                                                                            (
                                                                                                                                <div className="delay-recount-input-container">
                                                                                                                                    {
                                                                                                                                        item.selectedDelayWay && item.selectedDelayWay.value === "delay" ?
                                                                                                                                            (
                                                                                                                                                <div className="delay-recount-input-wrap">
                                                                                                                                                    <input type="number" className="lc-farm-input" value={item.delay}
                                                                                                                                                           onChange={e => this.toggleMixDelayRecountInput(e, index, 'delay', TAB_MIX)}/>
                                                                                                                                                    <span className="delay-recount-description">분 지연</span>
                                                                                                                                                </div>
                                                                                                                                            ) : null
                                                                                                                                    }
                                                                                                                                    {
                                                                                                                                        item.selectedActiveCount && item.selectedActiveCount.value === "infinite" ?
                                                                                                                                            null :
                                                                                                                                            (
                                                                                                                                                <div className="delay-recount-input-wrap">
                                                                                                                                                    <input type="number" className="lc-farm-input" value={item.active_count}
                                                                                                                                                           onChange={e => this.toggleMixDelayRecountInput(e, index, 'active_count', TAB_MIX)}/>
                                                                                                                                                    <span className="delay-recount-description">회</span>
                                                                                                                                                </div>
                                                                                                                                            )
                                                                                                                                    }
                                                                                                                                </div>
                                                                                                                            ) : null
                                                                                                                    }
                                                                                                                </div>
                                                                                                            )
                                                                                                    }
                                                                                                    {
                                                                                                        (item.selectedControlWayOption && item.selectedControlWayOption.value === 'pBand') || (item.selectedControlOption && item.selectedControlOption.origin.type === 'power') ?
                                                                                                            ControlTemperatureOption({
                                                                                                                type: TAB_MIX,
                                                                                                                index,
                                                                                                                item,
                                                                                                                TemperatureSensor: this.state.TemperatureSensor,
                                                                                                            }, {
                                                                                                                ...this
                                                                                                            }) : null
                                                                                                    }
                                                                                                    <p className="execution-ment">실행해주세요</p>
                                                                                                    {
                                                                                                        item.selectedControlOption && item.selectedControlOption.origin.type === 'motor' ?
                                                                                                            ControlMinMaxRange({
                                                                                                                type: TAB_MIX,
                                                                                                                index,
                                                                                                                item,
                                                                                                                TemperatureSensor: this.state.TemperatureSensor,
                                                                                                            }, {
                                                                                                                ...this
                                                                                                            })
                                                                                                            : null
                                                                                                    }
                                                                                                </div>

                                                                                                {item.recommendMinMax && item.recommendMinMax.length ?
                                                                                                    <Fragment>
                                                                                                        <div className={"recommend-wrap"}>
                                                                                                            <p className="recommendTxt">추천 최소/최대 개폐범위</p>
                                                                                                            <ul>
                                                                                                                {
                                                                                                                    item.recommendMinMax.map((data, index) => (
                                                                                                                        <li key={index}><p className="recommendTxt">{`${index+1} : ${data.name} / ${data.percentage}%`}</p></li>
                                                                                                                    ))
                                                                                                                }
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    </Fragment>
                                                                                                    : null
                                                                                                }
                                                                                            </div>
                                                                                        </div>
                                                                                    )
                                                                            }

                                                                        </div>
                                                                    )
                                                                })
                                                            }

                                                            <button className="card-header-button add-control-btn" onClick={this.addControl}>+ 제어추가</button>
                                                        </div>
                                                    </Fragment>
                                                )
                                            }



                                            {
                                                this.state.currentTab === TAB_STEP &&
                                                (
                                                    <Fragment>
                                                        <div className="mix-control-container">
                                                            {
                                                                this.state.stepControls.map((item, index) => {
                                                                    return (
                                                                        <div className="mix-control-wrap" key={index}>
                                                                            <div className="mix-control-header mix-control-item-wrap">
                                                                                <span className="mix-control-title">단계제어</span>
                                                                            </div>

                                                                            {CheckCondition({
                                                                                type: TAB_STEP,
                                                                                index,
                                                                                item
                                                                            }, {
                                                                                ...this
                                                                            })}

                                                                            {
                                                                                item.conditional.time && (item.conditional.sensor || item.conditional.control) &&
                                                                                (
                                                                                    <div
                                                                                        className="mix-control-item-wrap mix-control-time-condition-wrap">
                                                                                        <label
                                                                                            className="auto-control-form-label">시간조건</label>
                                                                                        <div
                                                                                            className="select-module-wrap lc-size-36">
                                                                                            <Select
                                                                                                value={item.selectedTimeWay}
                                                                                                placeholder="시간조건 선택"
                                                                                                onChange={val => this.handleMixSelectedTimeWay(val, index, TAB_STEP)}
                                                                                                isSearchable={false}
                                                                                                options={CONSTANT.selectTimeWay}
                                                                                                styles={colourStyles}/>
                                                                                        </div>

                                                                                        <div
                                                                                            className="lc-mix-control-time-container">
                                                                                            {
                                                                                                item.selectedTimeWay && item.selectedTimeWay.value !== "noSelect" && item.selectedTimeWay.value !== "sunDate" && (
                                                                                                    <div
                                                                                                        className="lc-time-select-container">
                                                                                                        {
                                                                                                            <Fragment>
                                                                                                                <div className="lc-time-form-wrap">
                                                                                                                    <label className="auto-control-form-label">시작시간</label>
                                                                                                                    <div className="lc-hour-and-minute-wrap">
                                                                                                                        <div className="time-item select-module-wrap lc-size-36">
                                                                                                                            <Select
                                                                                                                                value={item.selectedStartHour}
                                                                                                                                placeholder="시간 선택"
                                                                                                                                onChange={e => this.handleMixTime(e, 'selectedStartHour', index, TAB_STEP)}
                                                                                                                                isSearchable={false}
                                                                                                                                options={selectHourOptions}
                                                                                                                                styles={colourStyles}/>
                                                                                                                        </div>
                                                                                                                        <div
                                                                                                                            className="time-item select-module-wrap lc-size-36">
                                                                                                                            <Select
                                                                                                                                value={item.selectedStartMinute}
                                                                                                                                placeholder="분 선택"
                                                                                                                                onChange={e => this.handleMixTime(e, 'selectedStartMinute', index, TAB_STEP)}
                                                                                                                                isSearchable={false}
                                                                                                                                options={selectMinuteOptions}
                                                                                                                                styles={colourStyles}/>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                {
                                                                                                                    item.selectedTimeWay.value === "range" &&
                                                                                                                    (
                                                                                                                        <div
                                                                                                                            className="lc-time-form-wrap">
                                                                                                                            <label
                                                                                                                                className="auto-control-form-label">종료시간</label>
                                                                                                                            <div
                                                                                                                                className="lc-hour-and-minute-wrap">
                                                                                                                                <div
                                                                                                                                    className="time-item select-module-wrap lc-size-36">
                                                                                                                                    <Select
                                                                                                                                        value={item.selectedEndHour}
                                                                                                                                        placeholder="시간 선택"
                                                                                                                                        onChange={e => this.handleMixTime(e, 'selectedEndHour', index, TAB_STEP)}
                                                                                                                                        isSearchable={false}
                                                                                                                                        options={selectHourOptions}
                                                                                                                                        styles={colourStyles}/>
                                                                                                                                </div>
                                                                                                                                <div
                                                                                                                                    className="time-item select-module-wrap lc-size-36">
                                                                                                                                    <Select
                                                                                                                                        value={item.selectedEndMinute}
                                                                                                                                        placeholder="분 선택"
                                                                                                                                        onChange={e => this.handleMixTime(e, 'selectedEndMinute', index, TAB_STEP)}
                                                                                                                                        isSearchable={false}
                                                                                                                                        options={selectMinuteOptions}
                                                                                                                                        styles={colourStyles}/>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    )
                                                                                                                }
                                                                                                            </Fragment>
                                                                                                        }
                                                                                                    </div>
                                                                                                )
                                                                                            }

                                                                                            {
                                                                                                item.selectedTimeWay && item.selectedTimeWay.value === "sunDate" && SunDate({
                                                                                                    index, type: TAB_STEP,

                                                                                                    ...this.props,
                                                                                                    ...item,

                                                                                                    handleTime: this.handleSunDateTime,
                                                                                                    handleInput: this.handleSunDateInput,
                                                                                                    returnSunDateCalculate: this.returnSunDateCalculate
                                                                                                })
                                                                                            }
                                                                                        </div>
                                                                                    </div>
                                                                                )
                                                                            }

                                                                            {
                                                                                (!item.conditional.time && !item.conditional.sensor && !item.conditional.control) ?
                                                                                    (
                                                                                        <div className="not-allowed-to-create-wrap">
                                                                                            <p>시간 혹은 센서조건 중 하나를 선택해주세요.</p>
                                                                                        </div>
                                                                                    ) :
                                                                                    (
                                                                                        <div
                                                                                            className="mix-control-item-wrap mix-control-sensor-wrap">
                                                                                            {
                                                                                                item.conditional.time && !item.conditional.sensor && !item.conditional.control &&
                                                                                                (
                                                                                                    <div className="auto-control-setting-form auto-control-condition-wrap" style={{paddingLeft: 0}}>
                                                                                                        <label className="auto-control-form-label">시간조건</label>
                                                                                                        <div className="lc-setting-form-wrap">
                                                                                                            <div className="time-form-flex-wrap">
                                                                                                                <div
                                                                                                                    className="left-item select-module-wrap lc-size-36">
                                                                                                                    <Select
                                                                                                                        value={item.selectedTimeWay}
                                                                                                                        placeholder="시간조건 선택"
                                                                                                                        onChange={e => this.handleMixSelectedTimeWay(e, index, TAB_STEP)}
                                                                                                                        isSearchable={false}
                                                                                                                        options={CONSTANT.selectTimeWay}
                                                                                                                        styles={colourStyles}/>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            {
                                                                                                                item.selectedTimeWay && item.selectedTimeWay.value !== "noSelect" && item.selectedTimeWay.value !== "sunDate" && (
                                                                                                                    <div className="lc-time-select-container">
                                                                                                                        {
                                                                                                                            <Fragment>
                                                                                                                                <div className="lc-time-form-wrap">
                                                                                                                                    <label className="auto-control-form-label">시작시간</label>
                                                                                                                                    <div className="lc-hour-and-minute-wrap">
                                                                                                                                        <div className="time-item select-module-wrap lc-size-36">
                                                                                                                                            <Select
                                                                                                                                                value={item.selectedStartHour}
                                                                                                                                                placeholder="시간 선택"
                                                                                                                                                onChange={e => this.handleMixTime(e, 'selectedStartHour', index, TAB_STEP)}
                                                                                                                                                isSearchable={false}
                                                                                                                                                options={selectHourOptions}
                                                                                                                                                styles={colourStyles}/>
                                                                                                                                        </div>
                                                                                                                                        <div
                                                                                                                                            className="time-item select-module-wrap lc-size-36">
                                                                                                                                            <Select
                                                                                                                                                value={item.selectedStartMinute}
                                                                                                                                                placeholder="분 선택"
                                                                                                                                                onChange={e => this.handleMixTime(e, 'selectedStartMinute', index, TAB_STEP)}
                                                                                                                                                isSearchable={false}
                                                                                                                                                options={selectMinuteOptions}
                                                                                                                                                styles={colourStyles}/>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                {
                                                                                                                                    item.selectedTimeWay.value === "range" &&
                                                                                                                                    (
                                                                                                                                        <div className="lc-time-form-wrap">
                                                                                                                                            <label className="auto-control-form-label">종료시간</label>
                                                                                                                                            <div className="lc-hour-and-minute-wrap">
                                                                                                                                                <div className="time-item select-module-wrap lc-size-36">
                                                                                                                                                    <Select
                                                                                                                                                        value={item.selectedEndHour}
                                                                                                                                                        placeholder="시간 선택"
                                                                                                                                                        onChange={e => this.handleMixTime(e, 'selectedEndHour', index, TAB_STEP)}
                                                                                                                                                        isSearchable={false}
                                                                                                                                                        options={selectHourOptions}
                                                                                                                                                        styles={colourStyles}/>
                                                                                                                                                </div>
                                                                                                                                                <div className="time-item select-module-wrap lc-size-36">
                                                                                                                                                    <Select
                                                                                                                                                        value={item.selectedEndMinute}
                                                                                                                                                        placeholder="분 선택"
                                                                                                                                                        onChange={e => this.handleMixTime(e, 'selectedEndMinute', index, TAB_STEP)}
                                                                                                                                                        isSearchable={false}
                                                                                                                                                        options={selectMinuteOptions}
                                                                                                                                                        styles={colourStyles}/>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    )
                                                                                                                                }
                                                                                                                            </Fragment>
                                                                                                                        }
                                                                                                                    </div>
                                                                                                                )
                                                                                                            }

                                                                                                            {
                                                                                                                item.selectedTimeWay && item.selectedTimeWay.value === "sunDate" && SunDate({
                                                                                                                    index, type: TAB_STEP,

                                                                                                                    ...this.props,
                                                                                                                    ...item,

                                                                                                                    handleTime: this.handleSunDateTime,
                                                                                                                    handleInput: this.handleSunDateInput,
                                                                                                                    returnSunDateCalculate: this.returnSunDateCalculate
                                                                                                                })
                                                                                                            }

                                                                                                        </div>
                                                                                                    </div>
                                                                                                )
                                                                                            }


                                                                                            {
                                                                                                (item.conditional.sensor || item.conditional.control) &&
                                                                                                (
                                                                                                    <div className="lc-mix-sensor-container">
                                                                                                        {
                                                                                                            item.conditional.sensor && SensorCondition({
                                                                                                                type: TAB_STEP,
                                                                                                                index,
                                                                                                                item,
                                                                                                                selectSensorOption: this.state.selectSensorOption
                                                                                                            }, {
                                                                                                                ...this
                                                                                                            })
                                                                                                        }

                                                                                                        {item.conditional.control && item.conditional.sensor && (<hr className="hr"/>)}

                                                                                                        {
                                                                                                            item.conditional.control && ControlCondition({
                                                                                                                type: TAB_STEP,
                                                                                                                index,
                                                                                                                item,
                                                                                                                selectControlConditionOption: this.state.selectControlConditionOption
                                                                                                            }, {
                                                                                                                ...this
                                                                                                            })
                                                                                                        }
                                                                                                    </div>
                                                                                                )
                                                                                            }

                                                                                            <i className="hr"/>

                                                                                            <div className="auto-control-setting-form auto-control-execution-wrap">
                                                                                                <label className="auto-control-form-label">실행부</label>
                                                                                                <div className="lc-setting-form-wrap">
                                                                                                    <div className="form-flex-wrap">
                                                                                                        <div className="left-item select-module-wrap lc-size-36">
                                                                                                            <Select
                                                                                                                value={item.selectedControlOption}
                                                                                                                placeholder="기기 선택"
                                                                                                                onChange={val => this.handleMixSelectControl(val, index, TAB_STEP)}
                                                                                                                isSearchable={false}
                                                                                                                options={this.state.selectControlOption}
                                                                                                                styles={colourStyles}/>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    {
                                                                                                        item.recommendControl ?
                                                                                                            <div className={"recommend-wrap"}>
                                                                                                                <p className="recommendTxt">(추천기기 : {item.recommendControl.control_id})</p>
                                                                                                            </div>
                                                                                                            : null
                                                                                                    }
                                                                                                    {
                                                                                                        item.selectedControlOption || item.recommendControl ? (
                                                                                                            <Fragment>
                                                                                                                <div className="form-flex-wrap">
                                                                                                                    <div className="left-item select-module-wrap lc-size-36">
                                                                                                                        <Select
                                                                                                                            value={item.selectedControlStateOption}
                                                                                                                            placeholder="동작 선택"
                                                                                                                            onChange={val => this.handleMixSelectControlStateOption(val, index, TAB_STEP)}
                                                                                                                            isSearchable={false}
                                                                                                                            options={item.selectedControlOption && item.selectedControlOption.origin.type ? CONTROL_STATE_OPTIONS[item.selectedControlOption.origin.type] : (item.recommendControl.control.type ? CONTROL_STATE_OPTIONS[item.recommendControl.control.type]:[])}
                                                                                                                            styles={colourStyles}/>
                                                                                                                    </div>
                                                                                                                    <span className="lc-modifier">를</span>
                                                                                                                </div>
                                                                                                                {
                                                                                                                    item.selectedControlStateOption && item.selectedControlStateOption.hasValue && (
                                                                                                                        <div className="form-flex-wrap">
                                                                                                                            <div className="left-item select-and-input-wrap">
                                                                                                                                <div className="select-and-input">
                                                                                                                                    <div className="select-module-wrap lc-size-36">
                                                                                                                                        <Select value={item.selectedControlWayOption}
                                                                                                                                                placeholder="단위 선택"
                                                                                                                                                onChange={val => this.handleMixSelectControlWay(val, index, TAB_STEP)}
                                                                                                                                                isSearchable={false}
                                                                                                                                                options={this.state.pBands.length && item.selectedControlOption && (item.selectedControlOption.origin.type === 'motor' || item.selectedControlOption.origin.type === 'windDirection') && item.selectedControlStateOption.value === 'open-partial' ? [
                                                                                                                                                    ...CONTROL_UNIT_OPTIONS.motor,
                                                                                                                                                    ...CONTROL_UNIT_OPTIONS.pBand
                                                                                                                                                ] : item.selectedControlOption && CONTROL_UNIT_OPTIONS[item.selectedControlOption.origin.type] ? CONTROL_UNIT_OPTIONS[item.selectedControlOption.origin.type] : (CONTROL_UNIT_OPTIONS[item.recommendControl.control.type])}
                                                                                                                                                styles={colourStyles}/>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div className="select-and-input input-auto-control">
                                                                                                                                    {
                                                                                                                                        !item.selectedControlWayOption || (item.selectedControlWayOption.value !== 'pBand' && item.selectedControlWayOption.value !== 'pBandIntegral') && (
                                                                                                                                            <input type="number"
                                                                                                                                                   className="lc-farm-input"
                                                                                                                                                   value={item.settingValue}
                                                                                                                                                   onChange={e => this.handleMixSettingValue(e, index, TAB_STEP)}
                                                                                                                                                   placeholder="입력"/>
                                                                                                                                        )
                                                                                                                                    }
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            {
                                                                                                                                item.selectedControlWayOption && (item.selectedControlWayOption.value !== 'pBand' && item.selectedControlWayOption.value !== 'pBandIntegral') &&
                                                                                                                                (
                                                                                                                                    <span
                                                                                                                                        className="lc-modifier">{item.controlUnitName}</span>
                                                                                                                                )
                                                                                                                            }
                                                                                                                        </div>
                                                                                                                    )
                                                                                                                }
                                                                                                                {
                                                                                                                    item.selectedControlWayOption && (item.selectedControlWayOption.value === 'pBand' || item.selectedControlWayOption.value === 'pBandIntegral') ?
                                                                                                                        ControlPBand({
                                                                                                                            type: TAB_STEP,
                                                                                                                            index,
                                                                                                                            item,
                                                                                                                            pBands : this.state.pBands,
                                                                                                                        }, {
                                                                                                                            ...this
                                                                                                                        }) : null
                                                                                                                }
                                                                                                            </Fragment>
                                                                                                        ) : null
                                                                                                    }
                                                                                                    {
                                                                                                        item.selectedTimeWay && item.selectedTimeWay.value === "single" ?
                                                                                                            null :
                                                                                                            (
                                                                                                                <div className="delay-recount-wrap">
                                                                                                                    <label className="auto-control-form-label">지연/반복(선택사항)</label>
                                                                                                                    <div className="delay-recount-input-container">
                                                                                                                        <div className="delay-recount-input-wrap">
                                                                                                                            <input
                                                                                                                                type="number"
                                                                                                                                className="lc-farm-input"
                                                                                                                                value={item.delay}
                                                                                                                                onChange={e => this.toggleMixDelayRecountInput(e, index, 'delay', TAB_STEP)}/>
                                                                                                                            <span className="delay-recount-description">분 지연</span>
                                                                                                                        </div>
                                                                                                                        <div className="delay-recount-input-wrap">
                                                                                                                            <input
                                                                                                                                type="number"
                                                                                                                                className="lc-farm-input"
                                                                                                                                value={item.recount}
                                                                                                                                onChange={e => this.toggleMixDelayRecountInput(e, index, 'recount', TAB_STEP)}/>
                                                                                                                            <span className="delay-recount-description">회 반복</span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            )
                                                                                                    }
                                                                                                    {
                                                                                                        (item.selectedControlWayOption && item.selectedControlWayOption.value === 'pBand') || (item.selectedControlOption && item.selectedControlOption.origin.type === 'power') ?
                                                                                                            ControlTemperatureOption({
                                                                                                                type: TAB_STEP,
                                                                                                                index,
                                                                                                                item,
                                                                                                                TemperatureSensor: this.state.TemperatureSensor,
                                                                                                            }, {
                                                                                                                ...this
                                                                                                            }) : null
                                                                                                    }
                                                                                                    <p className="execution-ment">실행해주세요</p>
                                                                                                    {
                                                                                                        item.selectedControlOption && item.selectedControlOption.origin.type === 'motor' ?
                                                                                                            ControlMinMaxRange({
                                                                                                                type: TAB_STEP,
                                                                                                                index,
                                                                                                                item,
                                                                                                            }, {
                                                                                                                ...this
                                                                                                            })
                                                                                                            : null
                                                                                                    }
                                                                                                </div>
                                                                                                {item.recommendMinMax && item.recommendMinMax.length ?
                                                                                                    <Fragment>
                                                                                                        <div className={"recommend-wrap"}>
                                                                                                            <p className="recommendTxt">추천 최소/최대 개폐범위</p>
                                                                                                            <ul>
                                                                                                                {
                                                                                                                    item.recommendMinMax.map((data, index) => (
                                                                                                                        <li key={index}><p className="recommendTxt">{`${index+1} : ${data.name} / ${data.percentage}%`}</p></li>
                                                                                                                    ))
                                                                                                                }
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    </Fragment>
                                                                                                    : null
                                                                                                }
                                                                                            </div>
                                                                                        </div>
                                                                                    )
                                                                            }
                                                                        </div>
                                                                    )
                                                                })
                                                            }
                                                            {
                                                                this.state.autoControlSteps.length ? this.state.autoControlSteps.map((item, index) => {
                                                                    return (
                                                                        <div className="mix-control-wrap" key={index}>
                                                                            <div className="mix-control-header mix-control-item-wrap">
                                                                                <span className="mix-control-title">{index+2}단계</span>
                                                                            </div>

                                                                            <div className="mix-control-item-wrap mix-control-sensor-wrap">
                                                                                <div className="auto-control-setting-form auto-control-condition-wrap" style={{paddingLeft: 0}}>
                                                                                    <label className="auto-control-form-label">실행</label>
                                                                                    <div className="lc-setting-form-wrap">
                                                                                        <div className="form-flex-wrap">
                                                                                            <div className="left-item select-module-wrap lc-size-36">
                                                                                                <Select
                                                                                                    value={item.selectedControlOption}
                                                                                                    placeholder="기기 선택"
                                                                                                    onChange={val => this.handleMixSelectControl(val, index, TAB_STEP_STEP)}
                                                                                                    isSearchable={false}
                                                                                                    options={this.state.selectControlOption}
                                                                                                    styles={colourStyles}/>
                                                                                            </div>
                                                                                        </div>
                                                                                        {
                                                                                            item.recommendControl ?
                                                                                                <div className={"recommend-wrap"}>
                                                                                                    <p className="recommendTxt">(추천기기 : {item.recommendControl.control_id})</p>
                                                                                                </div>
                                                                                                : null
                                                                                        }
                                                                                        {
                                                                                            item.selectedControlOption || item.recommendControl ? (
                                                                                                <Fragment>
                                                                                                    <div className="form-flex-wrap">
                                                                                                        <div className="left-item select-module-wrap lc-size-36">
                                                                                                            <Select
                                                                                                                value={item.selectedControlStateOption}
                                                                                                                placeholder="동작 선택"
                                                                                                                onChange={val => this.handleMixSelectControlStateOption(val, index, TAB_STEP_STEP)}
                                                                                                                isSearchable={false}
                                                                                                                options={item.selectedControlOption && item.selectedControlOption.origin.type ? CONTROL_STATE_OPTIONS[item.selectedControlOption.origin.type] : (item.recommendControl.control.type ? CONTROL_STATE_OPTIONS[item.recommendControl.control.type]:[])}
                                                                                                                styles={colourStyles}/>
                                                                                                        </div>
                                                                                                        <span className="lc-modifier">를</span>
                                                                                                    </div>
                                                                                                    {
                                                                                                        item.selectedControlStateOption && item.selectedControlStateOption.hasValue && (
                                                                                                            <div className="form-flex-wrap">
                                                                                                                <div className="left-item select-and-input-wrap">
                                                                                                                    <div className="select-and-input">
                                                                                                                        <div className="select-module-wrap lc-size-36">
                                                                                                                            <Select value={item.selectedControlWayOption}
                                                                                                                                    placeholder="단위 선택"
                                                                                                                                    onChange={val => this.handleMixSelectControlWay(val, index, TAB_STEP_STEP)}
                                                                                                                                    isSearchable={false}
                                                                                                                                    options={this.state.pBands.length && item.selectedControlOption && (item.selectedControlOption.origin.type === 'motor' || item.selectedControlOption.origin.type === 'windDirection') && item.selectedControlStateOption.value === 'open-partial' ? [
                                                                                                                                        ...CONTROL_UNIT_OPTIONS.motor,
                                                                                                                                        ...CONTROL_UNIT_OPTIONS.pBand
                                                                                                                                    ] : item.selectedControlOption && CONTROL_UNIT_OPTIONS[item.selectedControlOption.origin.type] ? CONTROL_UNIT_OPTIONS[item.selectedControlOption.origin.type] : (CONTROL_UNIT_OPTIONS[item.recommendControl.control.type])}
                                                                                                                                    styles={colourStyles}/>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div className="select-and-input input-auto-control">
                                                                                                                        {
                                                                                                                            !item.selectedControlWayOption || (item.selectedControlWayOption.value !== 'pBand' && item.selectedControlWayOption.value !== 'pBandIntegral') && (
                                                                                                                                <input type="number"
                                                                                                                                       className="lc-farm-input"
                                                                                                                                       value={item.settingValue}
                                                                                                                                       onChange={e => this.handleMixSettingValue(e, index, TAB_STEP_STEP)}
                                                                                                                                       placeholder="입력"/>
                                                                                                                            )
                                                                                                                        }
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                {
                                                                                                                    item.selectedControlWayOption && ( item.selectedControlWayOption.value !== 'pBand' && item.selectedControlWayOption.value !== 'pBandIntegral') &&
                                                                                                                    (
                                                                                                                        <span className="lc-modifier">{item.controlUnitName}</span>
                                                                                                                    )
                                                                                                                }
                                                                                                            </div>
                                                                                                        )
                                                                                                    }
                                                                                                    {
                                                                                                        item.selectedControlWayOption && (item.selectedControlWayOption.value === 'pBand' || item.selectedControlWayOption.value === 'pBandIntegral') ?
                                                                                                            ControlPBand({
                                                                                                                type: TAB_STEP_STEP,
                                                                                                                index,
                                                                                                                item,
                                                                                                                pBands : this.state.pBands,
                                                                                                            }, {
                                                                                                                ...this
                                                                                                            }) : null
                                                                                                    }
                                                                                                </Fragment>
                                                                                            ) : null
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                                <div className="auto-control-setting-form auto-control-execution-wrap">
                                                                                    <div className="delay-recount-wrap" style={{marginTop: 0}}>
                                                                                        <label className="auto-control-form-label">지연/반복(선택사항)</label>
                                                                                        <div className="delay-recount-input-container">
                                                                                            <div className="delay-recount-input-wrap">
                                                                                                <input
                                                                                                    type="number"
                                                                                                    className="lc-farm-input"
                                                                                                    value={item.delay}
                                                                                                    onChange={e => this.toggleMixDelayRecountInput(e, index, 'delay', TAB_STEP_STEP)}/>
                                                                                                <span className="delay-recount-description">분 지연</span>
                                                                                            </div>
                                                                                            <div className="delay-recount-input-wrap">
                                                                                                <input
                                                                                                    type="number"
                                                                                                    className="lc-farm-input"
                                                                                                    value={item.recount}
                                                                                                    onChange={e => this.toggleMixDelayRecountInput(e, index, 'recount', TAB_STEP_STEP)}/>
                                                                                                <span className="delay-recount-description">회 반복</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    {
                                                                                        (item.selectedControlWayOption && item.selectedControlWayOption.value === 'pBand') || (item.selectedControlOption && item.selectedControlOption.origin.type === 'power') ?
                                                                                            ControlTemperatureOption({
                                                                                                type: TAB_STEP_STEP,
                                                                                                index,
                                                                                                item,
                                                                                                TemperatureSensor: this.state.TemperatureSensor,
                                                                                            }, {
                                                                                                ...this
                                                                                            }) : null
                                                                                    }
                                                                                    <p className="execution-ment">실행해주세요</p>
                                                                                    {
                                                                                        item.selectedControlOption && item.selectedControlOption.origin.type === 'motor' ?
                                                                                            ControlMinMaxRange({
                                                                                                type: TAB_STEP_STEP,
                                                                                                index,
                                                                                                item,
                                                                                            }, {
                                                                                                ...this
                                                                                            })
                                                                                         : null
                                                                                    }
                                                                                </div>
                                                                                {item.recommendMinMax && item.recommendMinMax.length ?
                                                                                    <Fragment>
                                                                                        <div className={"recommend-wrap"}>
                                                                                            <p className="recommendTxt">추천 최소/최대 개폐범위</p>
                                                                                            <ul>
                                                                                                {
                                                                                                    item.recommendMinMax.map((data, index) => (
                                                                                                        <li key={index}><p className="recommendTxt">{`${index+1} : ${data.name} / ${data.percentage}%`}</p></li>
                                                                                                    ))
                                                                                                }
                                                                                            </ul>
                                                                                        </div>
                                                                                    </Fragment>
                                                                                    : null
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                }) : null
                                                            }
                                                            <button className="card-header-button add-control-btn" onClick={this.addStep}>+ 단계추가</button>
                                                        </div>
                                                    </Fragment>
                                                )
                                            }
                                        </div>
                                    )}
                                    {
                                        this.state.currentTab === TAB_TABLE &&
                                        (
                                            <Fragment>
                                                {ControlTable(this.state, this)}
                                            </Fragment>
                                        )
                                    }
                                    <div id="autoControlButtonWrap" className="auto-control-form-section">
                                        <div className="button-wrap">
                                            <button className="card-header-button" onClick={this.close}>취소</button>
                                        </div>
                                        <div className="button-wrap">
                                            <button className="theme-button" onClick={this.submit}>{this.state.isUpdate ? "수정" : "등록"}</button>
                                        </div>
                                    </div>

                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useAutoControlForm(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(useSunDate(
    ({state}) => ({
        rise_hour: state.rise_hour,
        rise_minute: state.rise_minute,
        set_hour: state.set_hour,
        set_minute: state.set_minute
    })
)(useAutoControlTemperatureGraph(
    ({state, actions}) => ({
        openAutoControlTemperatureModal: actions.open
    })
)(ModalAutoControlForm)))));


