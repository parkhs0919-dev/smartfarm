import React, { Component } from 'react';
import { useuserPasswd } from "../../contexts/modals/UserPasswd";
import { useSocket } from "../../contexts/socket";
import { useAlert } from "../../contexts/alert";
import { useuser } from '../../contexts/modals/User'
import userManager from '../../managers/user';
import deepcopy from "../../../../utils/deepcopy";
import { noSession } from "../../utils/session";

const DEFAULT_VALUE =
{
    user: [],
    oldpassword: '',
    newpassword: '',
    newpasswdch: '',
};

class ModalUserPasswd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: [],
            oldpassword: '',
            newpassword: '',
            newpasswdch: '',
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if (e) e.preventDefault();
        this.setState(DEFAULT_VALUE)
        document.removeEventListener('keyup', this.closeOnEscape);

        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {
        if (e) e.preventDefault();
        let state = Object.assign({}, this.state);
        let body = {};

        if (!state.oldpassword) {
            return this.props.showDialog({
                alertText: "기존 비밀번호를 확인해 주세요."
            });
        }
        if (!state.newpassword) {
            return this.props.showDialog({
                alertText: "새 비밀번호를 확인해 주세요."
            });
        }
        if (String(state.newpassword) != String(state.newpasswdch)) {
            return this.props.showDialog({
                alertText: "새 비밀번호와 새 비밀번호 확인이 일치하지 않습니다."
            });
        }
        body.id = window.user.id;
        body.oldPassword = state.oldpassword;
        body.newPassword = state.newpassword;        
        userManager.pwupdate(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                this.props.alertError(status, data);
            } else if (status == 200) {                
                this.props.showDialog({
                    alertText: "사용자가 수정되었습니다.",
                    cancelCallback: () => {
                        this.close(event);
                    },
                })
            }
        }));
    };

    oldpassword = (e) => {
        e.preventDefault();
        const { value } = e.target;
        this.setState({ oldpassword: value })
    }

    newpassword = (e) => {
        e.preventDefault();
        const { value } = e.target;
        this.setState({ newpassword: value })
    }

    newpasswdch = (e) => {
        e.preventDefault();
        const { value } = e.target;
        this.setState({ newpasswdch: value })
    }
    render() {
        return (
            <article id="lcLiterDetailWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalLiterDetail" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">비밀번호 변경</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>
                                <section className="modal-body">
                                    <div className="mix-control-container">
                                        <div className="LiterDetail-register-wrap">
                                            <span className="title-name">기존 비밀번호</span>
                                            <label className="toggle-date-picker-label">
                                                <input className='inputtext' type="text" onChange={e => this.oldpassword(e)} />
                                            </label>
                                        </div>
                                        <div className="LiterDetail-register-wrap">
                                            <span className="title-name">새 비밀번호</span>
                                            <label className="toggle-date-picker-label">
                                                <input className='inputtext' type="text" onChange={e => this.newpassword(e)} />
                                            </label>
                                        </div>
                                        <div className="LiterDetail-register-wrap">
                                            <span className="title-name">새 비밀번호 확인</span>
                                            <label className="toggle-date-picker-label">
                                                <input className='inputtext' type="text" onChange={e => this.newpasswdch(e)} />
                                            </label>
                                        </div>
                                        <div id="autoControlButtonWrap" className="auto-control-form-section">
                                            <button className="card-header-button add-control-btn" onClick={this.submit}>변경</button>
                                        </div>
                                    </div>
                                </section>
                            </div> : null

                        }
                    </div>
                </div>
            </article>
        )
    }
}
export default useSocket(
    ({ state, actions }) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useuserPasswd(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalUserPasswd)));
