import React, {Component} from 'react';
import {useSensorAlarm} from "../../contexts/modals/sensorAlarm";
import {useSensorAlarmlist} from "../../contexts/modals/SensorAlarmlist";
import {useSensorAlarmDetail} from "../../contexts/modals/sensorAlarmDetail"
import {useSocket} from "../../contexts/socket";
import {useAlert} from "../../contexts/alert";
import CONSTANT from "../../constants/constant";
import TRANSLATE from '../../constants/translate';

import alarmManager from '../../managers/alarm';
import {addListener, on} from "../../utils/socket";
import { returnSensorSelect,sanitizeText} from "../../utils/sensor";

import deepcopy from "../../../../utils/deepcopy";
import {noSession} from "../../utils/session";


const SENSOR_TYPE = CONSTANT.sensorType;
const SENSOR_TYPE_TRANSLATE = TRANSLATE.SENSOR;
const ALARM_CONDITION = TRANSLATE.SENSOR.condition;
const DEFAULT_VALUE = CONSTANT.DEFAULT_VALUES.SENSOR_ALARMS;
const MODE_ORDINARY = "ordinary";
const MODE_ORDERING = "order";
const SPECIAL_SENSOR_TYPES =CONSTANT.SPECIAL_SENSOR;


class ModalInnerSensorAlarmlist extends Component {
    constructor(props) {
        super(props);
        this.audio = null
        this.moreMenuRefs = new Map();
        this.state = {
            form: {
                value: '',
            },
            alarm: {},
            alarms: [],

            sensor_id: null,
            condition: null,
            cycle:{value:'5',label:'05분'},
            alarm_name:'',
            cardMode:MODE_ORDINARY,

        };
    }

    componentDidMount() {
        this.props.sync(this.init);
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('outer-alarm');
        socket.removeListener('inner-alarm');
    }

    listener = (socket) => {
        const _this = this;
        on(socket)('inner-alarm', (data) => {
            if(_this.props.isVisible) _this.getAlarms(_this.state.form.sensor_id);
        });
        on(socket)('outer-alarm', (data) => {
            if(_this.props.isVisible) _this.getAlarms(_this.state.form.sensor_id);
        });
    };

    init = (data) => {

        let state = Object.assign({}, data);
        state.position = SENSOR_TYPE[data.key];

        this.setState(state, () => {

            this.getAlarms();
        });
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        if(this.audio) {
            this.audio.pause();
            this.audio.currentTime = 0;
        }
        this.setState(DEFAULT_VALUE);
        document.removeEventListener('keyup', this.closeOnEscape);

        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };








    getAlarms = () => {
        const _this = this;

        let query = {
            house_id: this.state.house_id,
            position: this.state.position
        };
                alarmManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    alarms: data.data.rows
                });
            } else if (status === 404) {
                _this.setState({
                    alarms: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    openAlarmSelectModal = (e, index) => {
        const ref = this.moreMenuRefs.get(index);
        document.removeEventListener('keyup', this.closeOnEscape);
        let alarm = deepcopy(this.state.alarms[index]);

        this.props.openSensorAlarmDetail(alarm, this.afterAlarmSetting);
        ref.blur();
    };

    afterAlarmSetting = () => {
        document.addEventListener('keyup', this.closeOnEscape);
    };
    openAlarmModal =()=>{

        let alarm ={house_id:this.state.house_id,key:this.state.key}
         document.removeEventListener('keyup', this.closeOnEscape);
         this.props.openSensorAlarm(alarm, this.afterAlarmSetting);

    }
    removeAlarm = (e, index, item) => {
        const ref = this.moreMenuRefs.get(index);
        const _this = this;
        ref.blur();

        this.props.showDialog({
            alertText: "경보를 삭제하시겠습니까?",
            cancelText: "취소",
            submitText: "확인",
            submitCallback: () => {
                alarmManager.remove(item.id, noSession(this.props.showDialog)((status, data) => {
                    if(status === 200) {
                        _this.props.showDialog({
                            alertText: "삭제되었습니다.",
                        });
                    } else {
                        _this.props.alertError(status, data);
                    }
                }));
            }
        })
    };

    toggleDetails = (e, toggleState) => {
        let state = Object.assign({}, this.state);
        let alarms = state.alarms;
        alarms.forEach((item, index) => {
            item.isalarmsTabOpen = toggleState;
        });
        this.setState(state);
    };

    generateAlarmText = (alarm) => {              
        return `${alarm.is_use?alarm.use_start_hour +'시 ~':''}  ${alarm.is_use?alarm.use_end_hour + '시':''} ${alarm.sensor.sensor_name}(이)가 ${ SPECIAL_SENSOR_TYPES[alarm.sensor.type] ? sanitizeText(alarm) : ''} ${alarm.condition=='equal'?'':alarm.value + ' ' + alarm.sensor.unit} ${ SPECIAL_SENSOR_TYPES[alarm.sensor.type] ? '' : ALARM_CONDITION[alarm.condition]}일 때 알려주세요.
        `;
    };

    generateAlarmDetail = (alarm) => {

        if(alarm.is_no||alarm.is_bar || alarm.is_image || alarm.is_sound) {
            let str ='('
            if(alarm.is_no=='1')
            {
                str += `${alarm.cycle}분 간격, ${alarm.is_no?alarm.no_start_hour+'시 ~':''} ${alarm.is_no?alarm.no_end_hour + '시 방해금지':''} `
            }
            if(alarm.is_bar) {
                str += '알림바';
            }
            if(alarm.is_image) {
                if(str.length > 1) str += ',';
                str += '이미지';
            }
            if(alarm.is_sound) {
                if(str.length > 1) str += ',';
                str += '사운드';
            }
            str += ')';
            return str;
        }

        return '';
    };




    togglealarmWrap = (e, index) => {
        let alarms = deepcopy(this.state.alarms);
        alarms[index].isalarmsTabOpen = !alarms[index].isalarmsTabOpen;
        this.setState({alarms});
    };

    clickalamrsActivateState = (e, val, item, index) => {
        e.preventDefault();
        const _this = this;

        if(item.state === val) {
            return false;
        }
            let body = {
                id: item.id,
                state: val
            };

            let alarms = JSON.parse(JSON.stringify(_this.state.alarms));

            alarmManager.update(body, noSession(this.props.showDialog)((status, data) => {
                if(status === 200) {
                    alarms[index].state = val;
                    _this.setState({
                        alarms: alarms
                    });
                } else {
                    _this.props.alertError(status, data);
                }
            }));

    };

    render() {

        return (
            <article id="lcModalSensorAlarmWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalSensorAlarm" className="modal-wrap">
                                <section className="modal-header">
                                    {
                                            <h3 className="lc-modal-title">{SENSOR_TYPE_TRANSLATE[this.state.key]} 경보설정</h3>
                                    }
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>
                                <section className="modal-body">
                                {
                                        (
                                            <React.Fragment>
                                            <div className="alarm-register-wrap">
                                                <div id="alarmNameWrap" className="alarm-form-section">
                                                <button className="theme-button" onClick={this.openAlarmModal}>경보 설정 등록</button>
                                                </div>
                                            </div>
                                            <div className="list-section-wrap">
                                            {
                                                this.state.alarms && this.state.alarms.length ?
                                                    (
                                                        <React.Fragment>
                                                            <div className="item-open-close-button-wrap">
                                                                <button className="card-header-button" onClick={e => this.toggleDetails(e, true)}>조건펼침</button>
                                                                <button className="card-header-button" onClick={e => this.toggleDetails(e, false)}>조건접힘</button>

                                                            </div>
                                                            <ul id="alarm_List">
                                                            {
                                                                this.state.alarms.map((item, index) => {
                                                                    return (
                                                                        <li className="alarm-list-item" key={index} style={{zIndex: this.state.alarms.length - index}}>
                                                                        <article className="lc-content-card">
                                                                            <div className="lc-alarm-name-wrap">
                                                                                <p className="lc-alarm-name-wrap">
                                                                                    <span className="lc-control-name-index">{(index+1)}</span>
                                                                                    <span className="lc-control-name">{item.name}</span>
                                                                                </p>
                                                                            </div>
                                                                            <div className="lc-alarm-wrap">
                                                                            {
                                                                                this.state.cardMode === MODE_ORDINARY ?
                                                                                (
                                                                                    <div className="ordinary-menu-wrap">
                                                                                        <button className={"open-menu-btn" + (item.isalarmsTabOpen ? ' lc-open' : '')} onClick={e => this.togglealarmWrap(e, index)} />
                                                                                        <div className="toggle-radio-wrap two-state">
                                                                                            <input type="radio" name={`auto-control-activate-toggle-option-${index}`}
                                                                                                    onChange={e => {}}
                                                                                                    checked={item.state === "use"}
                                                                                            />

                                                                                            <input type="radio" name={`auto-control-activate-toggle-option-${index}`}
                                                                                                    onChange={e => {}}
                                                                                                    checked={item.state === "unuse"}
                                                                                            />

                                                                                            <label onClick={e => this.clickalamrsActivateState(e, "use", item, index)}>ON</label>
                                                                                            <label onClick={e => this.clickalamrsActivateState(e, "unuse", item, index)}>OFF</label>
                                                                                            <i className="toggle-slider-icon"/>
                                                                                        </div>
                                                                                    </div>
                                                                                ) : null

                                                                            }
                                                                            <div className="three-dot-menu" tabIndex="0" ref={c => this.moreMenuRefs.set(index, c)} style={{zIndex: (5+ this.state.alarms.length - index)}}>
                                                                                    <div className="icon-wrap">
                                                                                        <i/><i/><i/>
                                                                                    </div>
                                                                                    <div className="three-dot-view-more-menu">
                                                                                        <ul>

                                                                                            <li onClick={e => this.openAlarmSelectModal(e, index, item)}><span>수정</span></li>
                                                                                            <li onClick={e => this.removeAlarm(e, index, item)}><span>삭제</span></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </article>
                                                                        {
                                                                            item.isalarmsTabOpen &&
                                                                            (
                                                                                <React.Fragment>
                                                                                {

                                                                                        <div className="alarm-child-wrap">
                                                                                            <ul className="alarm-child-list">
                                                                                            {

                                                                                                        <li className="alarm-child-list-item" key={index}>

                                                                                                            <article className="alarm-child-content-wrap">
                                                                                                            {
                                                                                                                item.sensor.length > 1 ? (<span className="alarm-child-index">{index+1}</span>) : null
                                                                                                            }
                                                                                                                <div className="partial-alarm-wrap">
                                                                                                                    <p className="alarm-child-text">
                                                                                                                        {this.generateAlarmText(item)}
                                                                                                                        <br/>
                                                                                                                        {this.generateAlarmDetail(item)}
                                                                                                                    </p>
                                                                                                                </div>
                                                                                                            </article>
                                                                                                        </li>

                                                                                            }
                                                                                            </ul>
                                                                                        </div>

                                                                                }
                                                                                </React.Fragment>
                                                                            )

                                                                        }
                                                                        </li>
                                                                    )

                                                                })
                                                            }
                                                            </ul>
                                                        </React.Fragment>
                                                    ) :

                                                    <article className="lc-content-card">

                                                        <div className="lc-alarm-name-wrap">
                                                            <p className="lc-alarm-name-wrap2">
                                                            해당 기기에 설정된 경보가 없습니다.

                                                            </p>


                                                        </div>
                                                    </article>

                                            }
                                            </div>
                                            </React.Fragment>

                                        )


                                }
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useSensorAlarmDetail(
    ({actions}) => ({
        openSensorAlarmDetail: actions.open
    })
)(useSensorAlarm(
    ({actions}) => ({
        openSensorAlarm: actions.open
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSensorAlarmlist(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalInnerSensorAlarmlist)))));
