import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import {useControlMinMaxRange} from "../../contexts/modals/controlMinMaxRange";
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import {noSession} from "../../utils/session";
import controlMinMaxRangesManager from "../../managers/controlMinMaxRanges";
import sensorManager from "../../managers/sensor";
import deepcopy from "../../../../utils/deepcopy";
import {sanitize, returnSensorSelect} from "../../utils/sensor";
import {colourStyles} from "../../utils/select";

const NOT_RANGE_SENSOR = {
    'windDirection': true,
    'korinsWindDirection': true,
    'rain': true,
    'power': true,
    'water': true,
    'window': true,
    'fire': true,
    'door': true,
};

class ControlMinMaxRange extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sensors: [],
            controlMinMaxRanges: []
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        this.findSensors();
        this.findControlMinMaxRanges();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findSensors = () => {
        const _this = this;
        sensorManager.findAll({
            house_id: this.props.getHouseId(),
        }, noSession(this.props.show)((status, data) => {
            if (status === 200) {
                let sensors = [];
                data.data.rows.forEach(item => {
                    sensors.push({
                        value: item.id,
                        label: item.sensor_name,
                        origin: item
                    });
                });
                _this.setState({sensors});
            } else if (status === 404) {
                _this.setState({
                    sensors: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findControlMinMaxRanges = () => {
        const _this = this;
        controlMinMaxRangesManager.findAll({
            house_id: this.props.getHouseId()
        }, noSession(this.props.show)((status, data) => {
            if (status === 200) {
                let controlMinMaxRanges = [];
                data.data.rows.forEach(item => {
                    const sensor = item.sensor;
                    item.selectedSensor = {
                        value: sensor.id,
                        label: sensor.sensor_name,
                        origin: sensor
                    };
                    if (NOT_RANGE_SENSOR[sensor.type]) {
                        item.selectedCondition = {
                            value: item.value,
                            label: sanitize(sensor, item.value)
                        };
                    } else {
                        item.selectedCondition = {
                            value: item.condition,
                            label: item.condition === 'over' ? '이상' : '미만'
                        };
                    }
                    controlMinMaxRanges.push(item);
                });
                _this.setState({controlMinMaxRanges});
            } else if (status === 404) {
                _this.setState({
                    controlMinMaxRanges: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    addItem = e => {
        e && e.preventDefault();
        let controlMinMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        controlMinMaxRanges.push({
            isCreate: true,
            house_id: this.props.getHouseId(),
            sensor_id: null,
            selectedSensor: null,
            selectedCondition: null,
            name: '',
            value: ''
        });
        this.setState({controlMinMaxRanges});
    };

    handleName = (e, index) => {
        let controlMinMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        controlMinMaxRanges[index].name = e.target.value;
        this.setState({controlMinMaxRanges});
    };

    handleSensor = (val, index) => {
        let controlMinMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        controlMinMaxRanges[index].selectedSensor = val;
        controlMinMaxRanges[index].sensor_id = val.origin.id;
        controlMinMaxRanges[index].selectedCondition = null;
        controlMinMaxRanges[index].value = '';
        this.setState({controlMinMaxRanges});
    };

    handleValue = (e, index) => {
        if (e.target.validity.valid) {
            let controlMinMaxRanges = deepcopy(this.state.controlMinMaxRanges);
            controlMinMaxRanges[index].value = e.target.value;
            this.setState({controlMinMaxRanges});
        }
    };

    handleCondition = (val, index) => {
        let controlMinMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        controlMinMaxRanges[index].selectedCondition = val;
        this.setState({controlMinMaxRanges});
    };

    storeItem = (e, index) => {
        e && e.preventDefault();
        const _this = this;
        let controlMinMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        let controlMinMaxRange = controlMinMaxRanges[index];
        if (!controlMinMaxRange.name) {
            return this.props.show({
                alertText: '명칭을 입력해주세요.'
            });
        }
        if (!controlMinMaxRange.selectedSensor) {
            return this.props.show({
                alertText: '센서를 선택해주세요.'
            });
        }
        if (!NOT_RANGE_SENSOR[controlMinMaxRange.selectedSensor.origin.type] && !controlMinMaxRange.value) {
            return this.props.show({
                alertText: '기준 값을 입력해주세요.'
            });
        }
        if (!controlMinMaxRange.selectedCondition) {
            return this.props.show({
                alertText: '조건을 선택해주세요.'
            });
        }
        let body = {
            house_id: controlMinMaxRange.house_id,
            name: controlMinMaxRange.name,
            sensor_id: controlMinMaxRange.sensor_id,
        };
        if (NOT_RANGE_SENSOR[controlMinMaxRange.selectedSensor.origin.type]) {
            body.value = controlMinMaxRange.selectedCondition.value;
            body.condition = 'equal';
        } else {
            body.value = controlMinMaxRange.value;
            body.condition = controlMinMaxRange.selectedCondition.value;
        }
        if (controlMinMaxRange.isCreate) {
            controlMinMaxRangesManager.create(body, noSession(this.props.show)((status, data) => {
                if (status === 200) {
                    delete controlMinMaxRanges[index].isCreate;
                    controlMinMaxRanges[index].state = 'on';
                    controlMinMaxRanges[index].id = data.data.id;
                    _this.setState({controlMinMaxRanges});
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        } else if (controlMinMaxRange.isUpdate) {
            body.id = controlMinMaxRange.id;
            controlMinMaxRangesManager.update(body, noSession(this.props.show)((status, data) => {
                if (status === 200) {
                    delete controlMinMaxRanges[index].isUpdate;
                    _this.setState({controlMinMaxRanges});
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        }
    };

    removeItem = (e, index) => {
        e && e.preventDefault();
        let controlMinMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        controlMinMaxRanges.splice(index, 1);
        this.setState({controlMinMaxRanges});
    };

    editItem = (e, index) => {
        e && e.preventDefault();
        let controlMinMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        controlMinMaxRanges[index].origin = deepcopy(controlMinMaxRanges[index]);
        controlMinMaxRanges[index].isUpdate = true;
        this.setState({controlMinMaxRanges});
    };

    cancelItem = (e, index) => {
        e && e.preventDefault();
        let controlMinMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        controlMinMaxRanges[index] = controlMinMaxRanges[index].origin;
        delete controlMinMaxRanges[index].origin;
        delete controlMinMaxRanges[index].isUpdate;
        this.setState({controlMinMaxRanges});
    };

    deleteItem = (e, index) => {
        e && e.preventDefault();
        const _this = this;
        _this.props.show({
            alertText: '삭제하시겠습니까?',
            cancelText: '취소',
            submitText: '삭제',
            submitCallback: () => {
                let controlMinMaxRanges = deepcopy(_this.state.controlMinMaxRanges);
                controlMinMaxRangesManager.remove(controlMinMaxRanges[index], noSession(_this.props.show)((status, data) => {
                    if (status === 200) {
                        controlMinMaxRanges.splice(index, 1);
                        _this.setState({controlMinMaxRanges});
                        _this.props.show({
                            alertText: '삭제되었습니다.'
                        });
                    } else {
                        _this.props.alertError(status, data);
                    }
                }));
            }
        });
    };

    handleState = (e, index) => {
        e && e.preventDefault();
        const _this = this;
        let controlMinMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        let controlMinMaxRange = controlMinMaxRanges[index];
        const state = controlMinMaxRange.state === 'on' ? 'off' : 'on';
        controlMinMaxRangesManager.update({
            id: controlMinMaxRange.id,
            state
        }, noSession(this.props.show)((status, data) => {
            if (status === 200) {
                controlMinMaxRanges[index].state = state;
                _this.setState({controlMinMaxRanges});
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {
        return (
            <article id="lcModalControlMinMaxRangeWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalControlMinMaxRange" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">최소/최대 개폐 기준 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <ul>
                                        {
                                            this.state.controlMinMaxRanges.map((item, index) => (
                                                <li className="lc-item" key={index}>
                                                    {
                                                        (item.isCreate || item.isUpdate) && (
                                                            <Fragment>
                                                                <div className="lc-theme-checkbox-wrap">
                                                                    <input id={`lcMinMaxRangeState${index}`}
                                                                           value="time"
                                                                           checked={false}
                                                                           onChange={() => {}}
                                                                           type="checkbox"
                                                                           name="mix-control-condition"/>
                                                                    <label htmlFor={`lcMinMaxRangeState${index}`}
                                                                           className="box-shape-label lc-not-allow"/>
                                                                </div>
                                                                <div className="lc-item">
                                                                    <input type="text"
                                                                           className="lc-farm-input"
                                                                           value={item.name}
                                                                           onChange={e => this.handleName(e, index)}
                                                                           placeholder="명칭"/>
                                                                </div>
                                                                <div className="lc-item select-module-wrap lc-size-36">
                                                                    <Select value={item.selectedSensor}
                                                                            placeholder="센서 선택"
                                                                            onChange={val => this.handleSensor(val, index)}
                                                                            isSearchable={false}
                                                                            options={this.state.sensors}
                                                                            styles={colourStyles}/>
                                                                </div>
                                                                {
                                                                    item.selectedSensor ?
                                                                        <Fragment>
                                                                            <div className="lc-item">
                                                                                {
                                                                                    !NOT_RANGE_SENSOR[item.selectedSensor.origin.type] ?
                                                                                        <input type="text"
                                                                                               pattern={"[0-9]*"}
                                                                                               className="lc-farm-input"
                                                                                               value={item.value}
                                                                                               onChange={e => this.handleValue(e, index)}
                                                                                               placeholder="기준 값"/> :
                                                                                        <input type="text"
                                                                                               className="lc-farm-input"
                                                                                               value={"-"}
                                                                                               readOnly={true}/>
                                                                                }
                                                                            </div>
                                                                            <div className="lc-item select-module-wrap lc-size-36">
                                                                                <Select value={item.selectedCondition}
                                                                                        placeholder="조건 선택"
                                                                                        onChange={val => this.handleCondition(val, index)}
                                                                                        isSearchable={false}
                                                                                        options={returnSensorSelect(item.selectedSensor.origin, true)}
                                                                                        styles={colourStyles}/>
                                                                            </div>
                                                                        </Fragment> :
                                                                        <Fragment>
                                                                            <div className="lc-item"/>
                                                                            <div className="lc-item"/>
                                                                        </Fragment>
                                                                }
                                                                <div className="lc-button-wrap">
                                                                    {
                                                                        item.isCreate ?
                                                                            <button type="button" className="theme-button" onClick={e => this.storeItem(e, index)}>등록</button> :
                                                                            <button type="button" className="theme-button" onClick={e => this.storeItem(e, index)}>저장</button>
                                                                    }
                                                                </div>
                                                                <div className="lc-button-wrap">
                                                                    {
                                                                        item.isCreate ?
                                                                            <button type="button" className="theme-text-button" onClick={e => this.removeItem(e, index)}>취소</button> :
                                                                            <button type="button" className="theme-text-button" onClick={e => this.cancelItem(e, index)}>취소</button>
                                                                    }
                                                                </div>
                                                            </Fragment>
                                                        )
                                                    }
                                                    {
                                                        !item.isUpdate && !item.isCreate && (
                                                            <Fragment>
                                                                <div className="lc-theme-checkbox-wrap">
                                                                    <input id={`lcMinMaxRangeState${index}`}
                                                                           value="time"
                                                                           checked={item.state === 'on'}
                                                                           onChange={() => {}}
                                                                           type="checkbox"
                                                                           name="mix-control-condition"/>
                                                                    <label htmlFor={`lcMinMaxRangeState${index}`}
                                                                           onClick={e => this.handleState(e, index)}
                                                                           className="box-shape-label"/>
                                                                </div>
                                                                <div className="lc-item">
                                                                    <input type="text"
                                                                           className="lc-farm-input"
                                                                           value={item.name}
                                                                           placeholder="명칭"
                                                                           readOnly={true}/>
                                                                </div>
                                                                <div className="lc-item select-module-wrap lc-size-36">
                                                                    <input type="text"
                                                                           className="lc-farm-input"
                                                                           value={item.selectedSensor.origin.sensor_name}
                                                                           readOnly={true}/>
                                                                </div>
                                                                <div className="lc-item">
                                                                    <input type="text"
                                                                           className="lc-farm-input"
                                                                           value={NOT_RANGE_SENSOR[item.selectedSensor.origin.type] ? '-' : item.value}
                                                                           readOnly={true}/>
                                                                </div>
                                                                <div className="lc-item">
                                                                    <input type="text"
                                                                           className="lc-farm-input"
                                                                           value={item.selectedCondition.label}
                                                                           readOnly={true}/>
                                                                </div>
                                                                <div className="lc-button-wrap">
                                                                    <button type="button" className="theme-button" onClick={e => this.editItem(e, index)}>수정</button>
                                                                </div>
                                                                <div className="lc-button-wrap">
                                                                    <button type="button" className="theme-text-button" onClick={e => this.deleteItem(e, index)}>삭제</button>
                                                                </div>
                                                            </Fragment>
                                                        )
                                                    }
                                                </li>
                                            ))
                                        }
                                        <li>
                                            <button type="button" className="theme-button" onClick={this.addItem}>항목 추가</button>
                                        </li>
                                    </ul>
                                </section>
                            </div> :
                            null
                        }
                    </div>
                </div>
            </article>
        );
    }
}

export default useControlMinMaxRange(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(useAlert(
    ({actions}) => ({
        show: actions.show,
        alertError: actions.alertError
    })
)(useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(ControlMinMaxRange)));
