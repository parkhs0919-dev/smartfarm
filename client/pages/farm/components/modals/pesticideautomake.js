import React, { Component } from 'react';
import { usePesticideautomake } from "../../contexts/modals/pesticideautomake";
import { useAlert } from "../../contexts/alert";
import pesticideManager from '../../managers/pesticide';
import deepcopy from "../../../../utils/deepcopy";
import { noSession } from "../../utils/session";






class Modalpesticideautomake extends Component {
    constructor(props) {
        super(props);

        this.audio = null;

        this.state = {
            pesticide:[],
        }
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (pesticide) => {
        document.addEventListener('keyup', this.closeOnEscape);
        this.setState({pesticide})
    };

    close = async (e) => {
        if (e) e.preventDefault();
        this.setState({pesticide:[]})
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {
        e && e.preventDefault();
        const _this = this;
        let body = {};
        let state = deepcopy(this.state.pesticide)

        if (state.is_frost) {
            if (!state.max_temp) {
                return this.props.showDialog({
                    alertText: "전일 최고 온도을 입력해 주세요."
                });
            }
            if (!state.midnight_temp) {
                return this.props.showDialog({
                    alertText: "당일 자정 온도을 입력해 주세요."
                });
            }
            if (!state.current_temp) {
                return this.props.showDialog({
                    alertText: "현재 온도을 입력해 주세요."
                });
            }
            if (!state.delay) {
                return this.props.showDialog({
                    alertText: "지연시간을 입력해 주세요."
                });
            }
        }
        body.id = state.id
        body.is_frost = state.is_frost
        body.max_temp = state.max_temp
        body.midnight_temp = state.midnight_temp
        body.current_temp = state.current_temp
        body.delay = state.delay
        pesticideManager.setautopesticideupdate(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                _this.props.alertError(status, data);
            } else if (status == 200) {
                _this.props.showDialog({
                    alertText: "알림방식이 등록되었습니다.",
                    cancelCallback: () => {

                        _this.close();
                    }
                })
            }
        }));
    };

    clickCheckBox = (e,type) => {
        
        let pesticide = deepcopy(this.state.pesticide);
        pesticide[type]= !pesticide[type];
        
        this.setState({ pesticide });
    };

    maxtemp = (e,type) => {
        if (e.target.value.length < 10) {
            let pesticide = deepcopy(this.state.pesticide);
            pesticide[type] = e.target.value;
            this.setState({ pesticide });
        }
    };
    midnighttemp = (e, type) => {
        if (e.target.value.length < 10) {
            let pesticide = deepcopy(this.state.pesticide);
            pesticide[type] = e.target.value;
            this.setState({ pesticide });
        }
    };
    currenttemp = (e, type) => {
        if (e.target.value.length < 10) {
            let pesticide = deepcopy(this.state.pesticide);
            pesticide[type] = e.target.value;
            this.setState({ pesticide });
        }
    };
    delay = (e, type) => {
        if (e.target.value.length < 10) {
            let pesticide = deepcopy(this.state.pesticide);
            pesticide[type] = e.target.value;
            this.setState({ pesticide });
        }
    };
    render() {
        
        return (

            <article id="lcpesticidemakeWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalpesticidemake" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">방제기 자동 옵션 설정</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>

                                <section className="modal-body">
                                    <div className="pesticidemake-register-wrap">
                                        <ul id="screenOrderList">
                                            <li className="screen-order-list-item">
                                                <article
                                                    className="screen-setting-wrap">
                                                    <div className="screen-setting-left-wrap">
                                                        <div className="lc-theme-checkbox-wrap">
                                                            <input id="mixControlCondition1"
                                                                value="time"
                                                                checked={this.state.pesticide.is_frost}
                                                                onChange={() => {
                                                                }} type="checkbox"
                                                                name="mix-control-condition" />
                                                            <label
                                                                htmlFor="mixControlCondition1"
                                                                onClick={(e) => this.clickCheckBox(e,'is_frost')}
                                                                className="box-shape-label" />
                                                            <label
                                                                htmlFor="mixControlCondition1"
                                                                onClick={(e) => this.clickCheckBox(e,'is_frost')}
                                                                className="text-label">서리 방제 옵션 사용</label>
                                                        </div>
                                                    </div>
                                                </article>
                                            </li>
                                        </ul>

                                        <label className="pesticidemake-form-label">전일 최고 온도</label>
                                        <div className="lc-setting-form-wrap">
                                            <div className="form-flex-wrap">
                                                <div
                                                    className="left-item select-module-wrap lc-size-36">
                                                    <input type="text" value={this.state.pesticide.max_temp?this.state.pesticide.max_temp:(this.state.pesticide.max_temp==0?'0':'')}
                                                        onChange={(e)=>this.maxtemp(e,'max_temp')} className="lc-farm-input"
                                                        placeholder="값을 넣어주세요" />
                                                </div>
                                                <span className='lc-modifier'>℃ 이하</span>
                                            </div>
                                        </div>

                                        <label className="pesticidemake-form-label">당일 자정 온도</label>
                                        <div className="lc-setting-form-wrap">
                                            <div className="form-flex-wrap">
                                                <div
                                                    className="left-item select-module-wrap lc-size-36">
                                                    <input type="text" value={this.state.pesticide.midnight_temp?this.state.pesticide.midnight_temp:(this.state.pesticide.midnight_temp==0?'0':'')}
                                                        onChange={(e)=>this.midnighttemp(e,'midnight_temp')} className="lc-farm-input"
                                                        placeholder="값을 넣어주세요" />
                                                </div>
                                                <span className='lc-modifier'>℃ 이하</span>
                                            </div>
                                        </div>

                                        <label className="pesticidemake-form-label">현재 온도</label>
                                        <div className="lc-setting-form-wrap">
                                            <div className="form-flex-wrap">
                                                <div
                                                    className="left-item select-module-wrap lc-size-36">
                                                    <input type="text" value={this.state.pesticide.current_temp?this.state.pesticide.current_temp:(this.state.pesticide.current_temp==0?'0':'')}
                                                        onChange={(e)=>this.currenttemp(e,'current_temp')} className="lc-farm-input"
                                                        placeholder="값을 넣어주세요" />
                                                </div>
                                                <span className='lc-modifier'>℃ 이하</span>
                                            </div>
                                        </div>
                                        <span className='lc-modifier'>일때 서리 방제 동작을 합니다.</span>
                                    </div>
                                    <div className="pesticidemake-register-wrap">
                                        <label className="pesticidemake-form-label">지연시간</label>
                                        <div className="lc-setting-form-wrap">
                                            <div className="form-flex-wrap">
                                                <div
                                                    className="left-item select-module-wrap lc-size-36">
                                                    <input type="text" value={this.state.pesticide.delay?this.state.pesticide.delay:(this.state.pesticide.delay==0?'0':'')}
                                                        onChange={(e)=>this.delay(e,'delay')} className="lc-farm-input"
                                                        placeholder="분" />
                                                </div>
                                                <span className='lc-modifier'>분</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="pesticidemakeButtonWrap" className="pesticidemake-form-section">
                                        <div className="button-wrap">
                                            <button className="card-header-button" onClick={this.close}>취소</button>
                                        </div>
                                        <div className="button-wrap">
                                            <button className="theme-button" onClick={this.submit}>등록</button>
                                        </div>
                                    </div>
                                </section>

                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(usePesticideautomake(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(Modalpesticideautomake));
