import React, {Component} from 'react';
import Select from 'react-select';
import {useSensorAlarmDetail} from "../../contexts/modals/sensorAlarmDetail";
import {useAlert} from "../../contexts/alert";
import TRANSLATE from '../../constants/translate';
import alarmManager from '../../managers/alarm';
import deepcopy from "../../../../utils/deepcopy";
import {noSession} from "../../utils/session";
import {colourStyles} from "../../../farm/utils/select";
import CONSTANT from "../../constants/constant";
import sensorManager from '../../managers/sensor';
import {returnSensorSelect} from "../../utils/sensor";


const SENSOR_TYPE_TRANSLATE = TRANSLATE.SENSOR;
const alarm =CONSTANT.defaultAlarm.Type;
const REPEAT_TYPE = CONSTANT.defaultAlarm.repeat;
const SPECIAL_SENSOR_TYPES =CONSTANT.SPECIAL_SENSOR;

const time=CONSTANT.defaultAlarm.Time
const cycle=CONSTANT.defaultAlarm.cycle

class ModalInnerSensorAlarmDetail extends Component {
    constructor(props) {
        super(props);
        
        this.audio = null;
        this.state = {
            alarm: {
                condition:''
            },
            sensorOptions: [],
            alarm_name:'',
            sensor_id: null,
        };
    }

    componentDidMount() {
        this.props.sync(this.init);

    }


    getSensors = () => {
        const _this = this;
      
        let query = {
            house_id: this.state.alarm.sensor.houseSensorRels[0].house_id,
            position: this.state.alarm.sensor.position,
          
        };

        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                let sensors = data.data.rows;
                let arr = [];
                sensors.forEach((item) => {
                    let obj = {};
                    obj.value = item.id;
                    obj.label = item.sensor_name;
                    obj.origin = item;
                    arr.push(obj);
                });
                _this.setState({
                    sensorOptions: arr
                });
            } else if (status === 404) {
                _this.props.showDialog({
                    alertText: "설정 가능한 센서가 없습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                })
            } else {
                _this.alertError(status, data);
            }
        }));
    };
    init = (alarm) => {
        
        if(alarm.sensor.position=='in'){
            alarm.position='inner'
        }else if(alarm.sensor.position=='out'){
            alarm.position='outer'
        }

        if(String(alarm.use_end_hour).length==1)
        {
            alarm.use_end_hour = {value:alarm.use_end_hour,label:'0'+alarm.use_end_hour+'시'}
        }else if(String(alarm.use_end_hour).length>1){
            alarm.use_end_hour = {value:alarm.use_end_hour,label:alarm.use_end_hour+'시'}
        }
        if(String(alarm.use_start_hour).length==1)
        {
            alarm.use_start_hour = {value:alarm.use_start_hour,label:'0'+alarm.use_start_hour+'시'}
        }else if(String(alarm.use_start_hour).length>1){
            alarm.use_start_hour = {value:alarm.use_start_hour,label:alarm.use_start_hour+'시'}
        }
        if(String(alarm.no_end_hour).length==1)
        {
            alarm.no_end_hour = {value:alarm.no_end_hour,label:'0'+alarm.no_end_hour+'시'}
        }else if(String(alarm.no_end_hour).length>1){
            alarm.no_end_hour = {value:alarm.no_end_hour,label:alarm.no_end_hour+'시'}
        }
        if(String(alarm.no_start_hour).length==1)
        {
            alarm.no_start_hour = {value:alarm.no_start_hour,label:'0'+alarm.no_start_hour+'시'}
        }else if(String(alarm.no_start_hour).length>1){
            alarm.no_start_hour = {value:alarm.no_start_hour,label:alarm.no_start_hour+'시'}
        }
        if(String(alarm.cycle).length==1)
        {
            alarm.cycle = {value:alarm.cycle,label:'0'+alarm.cycle+'분'}
        }else if(String(alarm.cycle).length>1){
            alarm.cycle = {value:alarm.cycle,label:alarm.cycle+'분'}
        }
        if(alarm.repeat){
            alarm.repeat = {value:alarm.repeat,label:alarm.repeat+'번'}
        }
        this.setState({alarm});

          setTimeout(()=>{
            this.getSensors();
          },500)




        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(this.audio) {
            this.audio.pause();
            this.audio.currentTime = 0;
        }
        if(e) e.preventDefault();
        this.props.callback();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    checkAlarmDetail = (e, key) => {
        let alarm = deepcopy(this.state.alarm);
        alarm[key] = !alarm[key];
        this.setState({alarm});
    };

    handlealarmName = (e, key) => {
        let alarm = deepcopy(this.state.alarm);
        alarm[key] = e.target.value;
        this.setState({alarm});
    };

    repeat = (option,key) =>{
        let alarm = deepcopy(this.state.alarm);
        alarm[key] = option;

        this.setState({alarm});

    }
    handleSelect_alarm=(option,key)=>{
        
        let alarm = deepcopy(this.state.alarm);
        alarm[key] = option.value;

        if(option.value=='range')
        {
            alarm['value']='';
            alarm['condition']='';
        }
        this.setState({alarm});
    };
    handleSelect_hour=(option,key)=>{
        let alarm = deepcopy(this.state.alarm);
        alarm[key] = option;
        this.setState({alarm});
    };
    handleSelect=(option,key)=>{
        let alarm = deepcopy(this.state.alarm);
        alarm[key] = option;

        if(key === "select_sensor") {
            alarm.condition = null;
            alarm.value='';
        }

        this.setState({alarm});

    };
    handleInput = (e,key) => {
        let alarm = deepcopy(this.state.alarm);

        alarm[key] = e.target.value;
        this.setState({alarm});
    };
    handleSelect_condition =(option, key) => {
        let alarm = deepcopy(this.state.alarm);
        alarm[key] = option;

        this.setState({alarm});
    };
    handleSelect_cycle = (option, key) => {
        let alarm = deepcopy(this.state.alarm);
        alarm[key] = option;

        this.setState({alarm});
    };
    submit = (e) => {
        e && e.preventDefault();
        const _this = this;
        let body = {};
        let alarm = deepcopy(this.state.alarm);
        let random = Math.round(Math.random()*999999);

        if(!alarm.name){
            return this.props.showDialog({
                alertText: "알람명을 입력해 주세요."
            });
        }
        if(!alarm.sensor_id) {
            return this.props.showDialog({
                alertText: "센서를 선택해주세요."
            });
        }
        if(!alarm.type){
            return this.props.showDialog({
                alertText: "센서 항목을 선택해 주세요."
            });
        }
        if(!alarm.condition && alarm.type=='range') {
            return this.props.showDialog({
                alertText: "범위를 선택해주세요."
            });
        }
        if(!alarm.type) {
            return this.props.showDialog({
                alertText: "알람 내용을 선택해 주세요."
            });
        }


        if(SPECIAL_SENSOR_TYPES[alarm.select_sensor.origin.type] && alarm.type=='range') {
            body.condition = "equal";
            body.value = alarm.condition.value;
        }else if(SPECIAL_SENSOR_TYPES[alarm.select_sensor.origin.type] && alarm.type=='error'){

            body.condition = 'equal';
            body.value = random;
        }else if(!SPECIAL_SENSOR_TYPES[alarm.select_sensor.origin.type]&&alarm.type=='range'){

            body.condition = alarm.condition.value;
            body.value = alarm.value;
        }else if(!SPECIAL_SENSOR_TYPES[alarm.select_sensor.origin.type]&&alarm.type=='error'){

            body.condition = 'equal';
            body.value = random;
        } else {
            if(alarm.value.length==0 && alarm.alarm_select.value=='range') {
                return this.props.showDialog({
                    alertText: "센서 값을 입력해주세요."
                });
            }
        }

        if(body.value.length==0) {
            return this.props.showDialog({
                alertText: "센서 값을 선택해 주세요."
            });
        }

        if(alarm.is_use)
        {
            if(alarm.hasOwnProperty('use_start_hour')==false)
            {
                return this.props.showDialog({
                    alertText: "알람시간을 입력해 주세요."
                });
            }
            if(alarm.hasOwnProperty('use_end_hour')==false)
            {
                return this.props.showDialog({
                    alertText: "알람시간을 입력해 주세요."
                });
            }
            if(alarm.use_start_hour.value ==alarm.use_end_hour.value)
            {
                return this.props.showDialog({
                    alertText: "알람시간을 확인해 주세요."
                });
            }

            body.is_use = alarm.is_use;
            body.use_start_hour =alarm.use_start_hour.value;
            body.use_end_hour = alarm.use_end_hour.value;
        }else{
            body.is_use = false;
            body.use_start_hour = 0;
            body.use_end_hour = 0;
        }
        if(alarm.is_no)
        {
            if(alarm.hasOwnProperty('no_end_hour')==false)
            {
                return this.props.showDialog({
                    alertText: "방해금지 시간을 입력해 주세요."
                });
            }
            if(alarm.hasOwnProperty('no_start_hour')==false)
            {
                return this.props.showDialog({
                    alertText: "방해금지 시간을 입력해 주세요."
                });
            }
            if(alarm.no_end_hour.value ==alarm.no_start_hour.value)
            {
                return this.props.showDialog({
                    alertText: "방해금지 시간을 확인해 주세요."
                });
            }
            body.is_no = alarm.is_no;
            body.no_start_hour = alarm.no_start_hour.value;
            body.no_end_hour =alarm.no_end_hour.value;
        }else{
            body.is_no=false;
            body.no_start_hour = '0';
            body.no_end_hour = '0';
        }
        if(alarm.is_bar)
        {
            body.is_bar = alarm.is_bar;
        }else{body.is_bar=false}
        if(alarm.is_image)
        {

            body.is_image = alarm.is_image;
        }else{body.is_image=false}
        if(alarm.is_sound)
        {
            body.is_sound = alarm.is_sound;
        }else{body.is_sound=false}
        body.id = alarm.id;
        body.sensor_id = alarm.sensor_id;

        body.name = alarm.name;
        body.type = alarm.type;
        body.cycle = alarm.cycle.value;
        body.repeat = alarm.repeat.value;


        alarmManager.setAlarmWay(body, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.props.showDialog({
                    alertText: "알림방식이 설정되었습니다.",
                    cancelCallback: () => {
                        sensor_id: null,
                        _this.close();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    testSound = (e) => {

        if(this.audio && this.audio.src){
            this.audio.pause();
            this.audio.currentTime = 0;
        }

        if(this.state.alarm.select_sensor.origin.type === "windDirection" || this.state.alarm.select_sensor.origin.type === "power" || this.state.alarm.select_sensor.origin.type === "water" || this.state.alarm.select_sensor.origin.type === "window"||this.state.alarm.select_sensor.origin.type === "fire"||this.state.alarm.select_sensor.origin.type === "door"||this.state.alarm.select_sensor.origin.type === "korinsWindDirection") {
            this.audio = new Audio("/public/sounds/etc.mp3");
            this.audio.play();
        } else if (this.state.alarm.select_sensor.origin.type === "rain") {
            // this.audio = new Audio("/public/sounds/etc.mp3");
            // this.audio.play();
            if(this.state.alarm.condition.value == '0') {
                this.audio = new Audio("/public/sounds/rainon.mp3");
                this.audio.play();
            } else {
                this.audio = new Audio("/public/sounds/rainoff.mp3");
                this.audio.play();
            }
        } else {
            if(this.state.alarm.condition.value === "over") {

                this.audio = new Audio("/public/sounds/over.mp3");
                this.audio.play();
            } else if (this.state.alarm.condition.value === "under") {

                this.audio = new Audio("/public/sounds/under.mp3");
                this.audio.play();
            }
        }
    };

    render() {
        let returnsensor;

        if(this.state.alarm)
        {
            if(this.state.alarm.hasOwnProperty('select_sensor')==false)
            {
                for(let i=0; i<this.state.sensorOptions.length; i++)
                {
                    if(this.state.alarm.sensor_id == this.state.sensorOptions[i].value)
                    {
                            this.state.alarm.select_sensor=this.state.sensorOptions[i]
                    }
                }
            }
        }
        if(this.state.alarm.select_sensor)
        {
            if(this.state.alarm.condition!=null&&!this.state.alarm.condition.label)
            {
                returnsensor =  returnSensorSelect(this.state.alarm.select_sensor.origin, true)
                for(let i=0; i<returnsensor.length; i++)
                {
                    if(SPECIAL_SENSOR_TYPES[this.state.alarm.select_sensor.origin.type])
                    {
                        if(this.state.alarm.value == returnsensor[i].value)
                        {
                            this.state.alarm.condition = returnsensor[i]
                        }
                    }else{
                        if(this.state.alarm.condition == returnsensor[i].value)
                        {
                            this.state.alarm.condition = returnsensor[i]
                        }
                    }
                }
            }
        }
        
        return (

            <article id="lcModalSensorAlarmWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalSensorAlarm" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">{SENSOR_TYPE_TRANSLATE[this.state.alarm.position]} 경보수정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div id="alarmNameWrap" className="alarm-form-section">
                                        <div className="alarm-register-wrap">
                                            <label className="alarm-form-label">알람명</label>
                                            <input type="text" defaultValue={this.state.alarm.name}
                                            onChange={(e) => this.handlealarmName(e, 'name')} className="lc-farm-input"
                                            placeholder="알람명"/>
                                        </div>

                                    </div>
                                    <div className="alarm-register-wrap">
                                        <form onSubmit={this.submit}>
                                            <ul className="alarm-control-wrap">

                                                <li className="alarm-control-item">
                                                    <div className="select-module-wrap">
                                                    <Select value={this.state.alarm.select_sensor?{value:this.state.alarm.select_sensor.value,label:this.state.alarm.select_sensor.label}:null}
                                                                placeholder="센서 선택"
                                                                onChange={option => this.handleSelect(option, "select_sensor")}
                                                                isSearchable={ false }
                                                                options={this.state.sensorOptions}
                                                                styles={colourStyles}/>
                                                    </div>



                                                </li>
                                                <li className="alarm-control-item">
                                                    <div className="select-module-wrap">
                                                        <Select value={this.state.alarm.type=='range'?{value:this.state.alarm.type,label:'센서 범위 설정'}:{value:this.state.alarm.type,label:'센서 이상 동작'}}
                                                                placeholder="경보 항목 선택"
                                                                onChange={option => this.handleSelect_alarm(option, "type")}
                                                                isSearchable={ false }
                                                                options={alarm}
                                                                styles={colourStyles}
                                                                />
                                                    </div>
                                                </li>
                                                {
                                                    this.state.alarm.select_sensor && !SPECIAL_SENSOR_TYPES[this.state.alarm.select_sensor.origin.type] && this.state.alarm.type=='range'?
                                                        (
                                                            <li className="alarm-control-item">
                                                                <input type="number" value={this.state.alarm.value} onChange={e => this.handleInput(e, "value")} className="sensor-value-input" placeholder="센서 값"/>
                                                                <span className="lc-unit">{this.state.alarm.select_sensor.origin.unit}</span>
                                                            </li>
                                                        ) : null
                                                }
                                                {
                                                    this.state.alarm.select_sensor && this.state.alarm.select_sensor.origin  && this.state.alarm.type=='range'?
                                                        (
                                                            <li className="alarm-control-item">
                                                                <div className="select-module-wrap">
                                                                    <Select value={this.state.alarm.condition?{value:this.state.alarm.condition.value,label:this.state.alarm.condition.label}:null}
                                                                            placeholder="범위 선택"
                                                                               onChange={option => this.handleSelect_condition(option, "condition")}
                                                                            isSearchable={ false }
                                                                            options={returnSensorSelect(this.state.alarm.select_sensor.origin, true)}
                                                                            styles={colourStyles}/>
                                                                </div>
                                                            </li>
                                                        ) : null
                                                }
                                                {
                                                    this.state.alarm.select_sensor && this.state.alarm.select_sensor.origin && this.state.alarm.type=='range'?
                                                        (
                                                            <li className="alarm-control-item">
                                                                <p>일 때 알려주세요</p>
                                                            </li>
                                                        ) : null
                                                }

                                            </ul>
                                            <div id="alarmSettingWrap" className="alarm-form-section">
                                                <div className="alarm-setting-form alarm-condition-wrap">
                                                    <label className="alarm-form-label">알람주기</label>
                                                        <div className="lc-setting-form-wrap">
                                                            <div className="form-flex-wrap">
                                                                <div className="left-item select-module-wrap lc-size-36">
                                                                <Select
                                                                    placeholder="주기 선택"
                                                                    value={this.state.alarm.cycle?{value:this.state.alarm.cycle.value,label:this.state.alarm.cycle.label}:null}
                                                                    onChange={option => this.handleSelect_cycle(option, "cycle")}
                                                                    isSearchable={ true }
                                                                    options={cycle}
                                                                    styles={colourStyles}/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="delay-recount-wrap">
                                                            <label className="alarm-form-label">알림방식 선택</label>
                                                        </div>
                                                        <div className="delay-recount-wrap">
                                                            <ul id="alarmDetailList">
                                                                <li className="alarm-detail-list-item">
                                                                    <article className="check-wrap">
                                                                        <div className="lc-theme-checkbox-wrap">
                                                                             <input value="is_bar"
                                                                                id="isBar"
                                                                                checked={this.state.alarm.is_bar === true}
                                                                                onChange={() => {}} type="checkbox"
                                                                                name="alarmDetailWay"/>
                                                                            <label htmlFor="isBar"
                                                                                  onClick={(e) => this.checkAlarmDetail(e, 'is_bar')}
                                                                                className="box-shape-label"/>
                                                                            <label htmlFor="isBar"
                                                                               onClick={(e) => this.checkAlarmDetail(e, 'is_bar')}
                                                                                className="text-label">알림바</label>
                                                                        </div>
                                                                    </article>
                                                                </li>
                                                                <li className="alarm-detail-list-item">
                                                                    <article className="check-wrap">
                                                                        <div className="lc-theme-checkbox-wrap">
                                                                          <input id="isImage"
                                                                                value="is_image"
                                                                                checked={this.state.alarm.is_image === true}
                                                                                onChange={() => {}} type="checkbox"
                                                                                name="alarmDetailWay"/>
                                                                            <label
                                                                                htmlFor="isImage"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'is_image')}
                                                                                className="box-shape-label"/>
                                                                            <label
                                                                                htmlFor="isImage"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'is_image')}
                                                                                className="text-label">이미지</label>
                                                                        </div>
                                                                    </article>
                                                                </li>
                                                                <li className="alarm-detail-list-item">
                                                                    <article className="check-wrap">
                                                                        <div className="lc-theme-checkbox-wrap">
                                                                           <input id="isSound"
                                                                                value="is_sound"
                                                                                checked={this.state.alarm.is_sound === true}
                                                                                onChange={() => {}} type="checkbox"
                                                                                name="alarmDetailWay"/>
                                                                            <label
                                                                                htmlFor="isSound"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'is_sound')}
                                                                                className="box-shape-label"/>
                                                                            <label
                                                                                htmlFor="isSound"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'is_sound')}
                                                                                className="text-label">음성</label>
                                                                        </div>
                                                                    </article>
                                                                    <div className="sound-test-wrap" onClick={e => this.testSound(e)}>
                                                                        <i/>
                                                                        <span>알림 소리 미리듣기</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                    </div>
                                                </div>
                                                <div className="alarm-setting-form2 alarm-execution-wrap">
                                                    <label className="alarm-form-label">알림 시간대</label>
                                                    <div className="delay-recount-wrap">
                                                    <ul id="alarmDetailList2">
                                                        <li className="alarm-detail-list-item">
                                                            <article className="check-wrap">
                                                                <div className="lc-theme-checkbox-wrap">
                                                                    <input value="alarmtime"
                                                                        id="alarmtime"
                                                                        checked={this.state.alarm.is_use === true}
                                                                        onChange={() => {}} type="checkbox"
                                                                        name="alarmDetailWay"/>
                                                                    <label htmlFor="alarmtime"
                                                                            onClick={(e) => this.checkAlarmDetail(e, 'is_use')}
                                                                        className="box-shape-label"/>
                                                                    <label htmlFor="alarmtime"
                                                                        onClick={(e) => this.checkAlarmDetail(e, 'is_use')}
                                                                        className="text-label">사용</label>
                                                                </div>
                                                            </article>

                                                        </li>
                                                        <div className="lc-setting-form-wrap">
                                                        {
                                                            this.state.alarm.is_use ?
                                                            (
                                                                <div className="form-flex-wrap">
                                                                    <div className="left-item2 select-module-wrap lc-size-36">
                                                                    <Select value={this.state.alarm.use_start_hour?{value:this.state.alarm.use_start_hour.value,label:this.state.alarm.use_start_hour.label}:null}
                                                                            placeholder="시간 선택"
                                                                            onChange={option => this.handleSelect_hour(option, "use_start_hour")}
                                                                            isSearchable={ false }
                                                                            options={time}
                                                                            styles={colourStyles}/>
                                                                    </div>
                                                                    <span className="lc-modifier">부터</span>
                                                                    <div className="left-item2 select-module-wrap lc-size-36">
                                                                    <Select value={this.state.alarm.use_end_hour?{value:this.state.alarm.use_end_hour.value,label:this.state.alarm.use_end_hour.label}:null}
                                                                            placeholder="시간 선택"
                                                                            onChange={option => this.handleSelect_hour(option, "use_end_hour")}
                                                                            isSearchable={ false }
                                                                            options={time}
                                                                            styles={colourStyles}/>
                                                                    </div>
                                                                    <span className="lc-modifier">까지</span>
                                                                </div>
                                                            ) : null
                                                        }
                                                        </div>
                                                    </ul>


                                                </div>
                                                <div className="delay-recount-wrap">
                                                    <label className="alarm-form-label">방해금지 시간대</label>
                                                </div>
                                                <div className="delay-recount-wrap">
                                                    <ul id="alarmDetailList2">
                                                        <li className="alarm-detail-list-item">
                                                            <article className="check-wrap">
                                                                <div className="lc-theme-checkbox-wrap">
                                                                    <input value="notalarmtime"
                                                                        id="notalarmtime"
                                                                        checked={this.state.alarm.is_no === true}
                                                                        onChange={() => {}} type="checkbox"
                                                                        name="alarmDetailWay"/>
                                                                    <label htmlFor="notalarmtime"
                                                                            onClick={(e) => this.checkAlarmDetail(e, 'is_no')}
                                                                        className="box-shape-label"/>
                                                                    <label htmlFor="notalarmtime"
                                                                        onClick={(e) => this.checkAlarmDetail(e, 'is_no')}
                                                                        className="text-label">사용</label>
                                                                </div>
                                                            </article>
                                                        </li>
                                                        <div className="lc-setting-form-wrap">
                                                        {
                                                            this.state.alarm.is_no ?
                                                            (
                                                                <div className="form-flex-wrap">
                                                                    <div className="left-item2 select-module-wrap lc-size-36">
                                                                    <Select value={this.state.alarm.no_start_hour?{value:this.state.alarm.no_start_hour.value,label:this.state.alarm.no_start_hour.label}:null}
                                                                            placeholder="시간 선택"
                                                                            onChange={option => this.handleSelect_hour(option, "no_start_hour")}
                                                                            isSearchable={ false }
                                                                            options={time}
                                                                            styles={colourStyles}/>
                                                                    </div>
                                                                    <span className="lc-modifier">부터</span>
                                                                    <div className="left-item2 select-module-wrap lc-size-36">
                                                                    <Select value={this.state.alarm.no_end_hour?{value:this.state.alarm.no_end_hour.value,label:this.state.alarm.no_end_hour.label}:null}
                                                                            placeholder="시간 선택"
                                                                            onChange={option => this.handleSelect_hour(option, "no_end_hour")}
                                                                            isSearchable={ false }
                                                                            options={time}
                                                                            styles={colourStyles}/>
                                                                    </div>
                                                                    <span className="lc-modifier">까지</span>
                                                                </div>
                                                            ) : null
                                                        }
                                                        </div>
                                                        <div className="delay-recount-wrap">
                                                                    <label className="alarm-form-label">반복횟수</label>
                                                                </div>
                                                                <div className="lc-setting-form-wrap">
                                                                    <li className="left-item select-module-wrap lc-size-36">
                                                                        <Select value={this.state.alarm.repeat}
                                                                            placeholder="시간 선택"
                                                                            onChange={option => this.repeat(option, "repeat")}
                                                                            isSearchable={false}
                                                                            options={REPEAT_TYPE}
                                                                            styles={colourStyles} />
                                                                    </li>
                                                                </div>
                                                    </ul>
                                                </div>
                                                </div>

                                            </div>
                                            <div className="delay-recount-wrap">
                                                 <button className="theme-button" type="submit">등록</button>
                                            </div>
                                        </form>
                                    </div>


                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSensorAlarmDetail(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalInnerSensorAlarmDetail));
