import moment from "moment";
import {attachZero, date} from "../../../../../utils/filter";
import deepcopy from "../../../../../utils/deepcopy";
import CONSTANT from "../../../constants/constant";
import autoControlManager from "../../../managers/autoControl";
import React from "react";
import translate from "../../../constants/translate";

const TAB_SENSOR = "sensor";
const TAB_TIME = "time";
const TAB_MIX = "mix";
const TAB_STEP = "step";
const TAB_CONTROL = "control";
const TAB_STEP_STEP = "step_step";
const TAB_TABLE = 'table';
const TYPE_POWER = 'power';
const TYPE_MOTOR = 'motor';

const DATE_OPTIONS = {
    is_mon: false,
    is_tue: false,
    is_wed: false,
    is_thur: false,
    is_fri: false,
    is_sat: false,
    is_sun: false,
};

const delayWayOptions = [
    {value: 'noDelay', label: "1분 타이머"},
    {value: 'delay', label: "지연시간 입력"},
    {value: 'immediately', label: "즉시 실행"},
];


const TABLE_CONTROL_SENSOR = {
    selectedAutoControlSensor : null,
    sensor : null,
    sensor_id : '',
    sensorUnit : '',
    max_value : '',
    min_value : '',
    percentage : '',
};

const selectControlConditions = {
    motor: [
        {value: 'over', label: '이상'},
        {value: 'excess', label: '초과'},
        {value: 'below', label: '이하'},
        {value: 'under', label: '미만'},
        {value: 'equal', label: '같음'},
    ],
    power: [
        {value: 1, label: '켜짐'},
        {value: 0, label: '꺼짐'}
    ]
};
const selectTimeWay = [
    {value: "noSelect", label: "선택 안함"},
    {value: "single", label: "특정 시간"},
    {value: "range", label: "시간 범위"},
    {value: "sunDate", label: "일출 일몰 시간 범위"}
];
const selectSunDateTypes = [
    {value: "rise", label: "일출 기준"},
    {value: "set", label: "일몰 기준"},
];

const selectTableStartTimeTypes = [
    {value: "rise", label: "일출 기준"},
    {value: "set", label: "일몰 기준"},
    {value: "single", label: "고정"}
];

const selectPlusMinus = [
    {value: "plus", label: "+"},
    {value: "minus", label: "-"}
];

const RETURN_CONTROL_OPTION = CONSTANT.RETURN_CONTROL_OPTION;
const CONTROL_RETURN_UNIT = CONSTANT.CONTROL_OPTIONS.returnUnit;

export default {
    handleDayType: function (val) {
        let temp = {
            selectedDayType: val
        };
        if (val.value === "per") {
            temp.start_date = moment(date(new Date(), "yyyy-MM-dd"));
        }
        this.setState(temp);
    },

    handleDate: function (val, key) {
        let state = Object.assign({}, this.state);
        state[key] = val === null ? null : moment(date(new Date(val), 'yyyy-MM-dd'));
        this.setState(state);
        this.handleDatePickerState(null, key);
    },

    handleTime: function (val, key) {
        let state = Object.assign({}, this.state);
        state[key] = val;
        this.setState(state);
    },

    handleDatePickerState: function (e, key) {
        e && e.preventDefault();
        let datePickerOpenState = Object.assign({}, this.state.datePickerOpenState);
        datePickerOpenState[key] = !datePickerOpenState[key];
        this.setState({datePickerOpenState});
    },

    handleSelectDate: function (selectedOption) {
        let dateOptions = Object.assign({}, DATE_OPTIONS);
        selectedOption.forEach(item => {
            dateOptions[item.value] = dateOptions[item.value] !== undefined;
        });
        dateOptions.selectedDateOption = selectedOption;
        this.setState(dateOptions);
    },

    handleSelectTimeWay: function (selectedTimeWay) {
        let temp = {
            selectedTimeWay: selectedTimeWay,
            start_time: moment('00:00', 'HH:mm'),
            end_time: moment('00:00', 'HH:mm'),
        };
        if (selectedTimeWay.value === "single") {
            temp.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
            temp.active_count = 0;
        }
        this.setState(temp);
    },

    handleSelectActiveCount: function (selectedActiveCount) {
        if (this.state.currentTab === TAB_SENSOR || this.state.currentTab === TAB_TIME) {
            if (this.state.selectedTimeWay && this.state.selectedTimeWay.value === "single") {
                return this.props.showDialog({
                    alertText: "특정시간 기준 자동제어는<br>실행횟수를 입력할 수 없습니다.",
                });
            }
        }
        if (this.state.currentTab === TAB_STEP) {
            if (this.state.stepControls[0].selectedTimeWay && this.state.stepControls[0].selectedTimeWay.value === "single") {
                return this.props.showDialog({
                    alertText: "특정시간 기준 자동제어는<br>실행횟수를 입력할 수 없습니다.",
                });
            }
        }
        let temp = {
            selectedActiveCount: selectedActiveCount,
            active_count: 0,
            setCount: 0
        };
        this.setState(temp);
    },

    handleSelectDelayWay: function (selectedDelayWay) {
        let temp = {
            selectedDelayWay: selectedDelayWay,
            delay: 0
        };
        this.setState(temp);
    },

    handleMixSelectedTimeWay: function (selectedTimeWay, originIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].selectedTimeWay = selectedTimeWay;
            mixControls[originIndex].start_time = moment('00:00', 'HH:mm');
            mixControls[originIndex].end_time = moment('00:00', 'HH:mm');
            if (selectedTimeWay.value === "single") {
                mixControls[originIndex].selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                mixControls[originIndex].active_count = 0;
                mixControls[originIndex].selectedDelayWay = delayWayOptions[0];
                mixControls[originIndex].delay = 0;
            }
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let temp = {};
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].selectedTimeWay = selectedTimeWay;
            stepControls[originIndex].start_time = moment('00:00', 'HH:mm');
            stepControls[originIndex].end_time = moment('00:00', 'HH:mm');
            if (selectedTimeWay.value === "single") {
                stepControls[originIndex].delay = '';
                stepControls[originIndex].recount = '';
                temp.step_delay = 0;
                temp.setCount = 0;
                temp.autoControlSteps = [];
                temp.step_delay = 0;
                temp.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
            }
            temp.stepControls = stepControls;
            this.setState(temp);
        }
    },

    handleMixTime: function (val, key, originIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex][key] = val;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex][key] = val;
            this.setState({stepControls});
        }
    },

    handleSunDateTime: function (val, key, index, type) {
        if (type === TAB_TIME) {
            let temp = {};
            temp[key] = val;
            this.setState(temp);
        } else if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index][key] = val;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index][key] = val;
            this.setState({stepControls});
        }
    },

    handleSunDateInput: function (e, key, index, type) {
        if (e.target.validity.valid || !e.target.value) {
            if (type === TAB_TIME) {
                let temp = {};
                temp[key] = e.target.value;
                this.setState(temp);
            } else if (type === TAB_MIX) {
                let mixControls = deepcopy(this.state.mixControls);
                mixControls[index][key] = e.target.value;
                this.setState({mixControls});
            } else if (type === TAB_STEP) {
                let stepControls = deepcopy(this.state.stepControls);
                stepControls[index][key] = e.target.value;
                this.setState({stepControls});
            }
        }
    },

    handleMixSensors: function (val, sensorIndex, originIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixSensors[sensorIndex].selectedSensorOption = val;
            mixControls[originIndex].mixSensors[sensorIndex].sensorValue = '';
            mixControls[originIndex].mixSensors[sensorIndex].selectedRangeConditionOption = null;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepSensors[sensorIndex].selectedSensorOption = val;
            stepControls[originIndex].stepSensors[sensorIndex].sensorValue = '';
            stepControls[originIndex].stepSensors[sensorIndex].selectedRangeConditionOption = null;
            this.setState({stepControls});
        }
    },

    handleMixControlCondition: function (val, controlIndex, originIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixControls[controlIndex].selectedControlCondition = val;
            mixControls[originIndex].mixControls[controlIndex].controlConditionValue = '';
            mixControls[originIndex].mixControls[controlIndex].selectedControlConditionOption = null;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepControls[controlIndex].selectedControlCondition = val;
            stepControls[originIndex].stepControls[controlIndex].controlConditionValue = '';
            stepControls[originIndex].stepControls[controlIndex].selectedControlConditionOption = null;
            this.setState({stepControls});
        }
    },

    handleMixSensorValue: function (e, sensorIndex, originIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixSensors[sensorIndex].sensorValue = e.target.value;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepSensors[sensorIndex].sensorValue = e.target.value;
            this.setState({stepControls});
        }
    },

    handleMixControlConditionValue: function (e, controlIndex, originIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixControls[controlIndex].controlConditionValue = e.target.value;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepControls[controlIndex].controlConditionValue = e.target.value;
            this.setState({stepControls});
        }
    },

    handleSelectMixSensorRange: function (val, sensorIndex, originIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            let mixSensors = mixControls[originIndex].mixSensors;
            let mixSensor = mixSensors[sensorIndex];
            mixSensor.selectedRangeConditionOption = val;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            let stepSensors = stepControls[originIndex].stepSensors;
            let stepSensor = stepSensors[sensorIndex];
            stepSensor.selectedRangeConditionOption = val;
            this.setState({stepControls});
        }
    },

    handleSelectAutoControlStandardSensor: function (val, originIndex, controlIndex, standardIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixSensors[controlIndex].autoControlSensorOptions[standardIndex].sensor = val;
            mixControls[originIndex].mixSensors[controlIndex].autoControlSensorOptions[standardIndex].sensorUnit = val.origin.unit;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepSensors[controlIndex].autoControlSensorOptions[standardIndex].sensor = val;
            stepControls[originIndex].stepSensors[controlIndex].autoControlSensorOptions[standardIndex].sensorUnit = val.origin.unit;
            this.setState({stepControls});
        } else if (type === TAB_SENSOR) {
            let autoControlSensorOptions = deepcopy(this.state.autoControlSensorOptions);
            autoControlSensorOptions[standardIndex].sensor = val;
            autoControlSensorOptions[standardIndex].sensorUnit = val.origin.unit;
            this.setState({autoControlSensorOptions});
        }
    },

    handleMixControlConditionOption: function (val, controlIndex, originIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixControls[controlIndex].selectedControlConditionOption = val;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepControls[controlIndex].selectedControlConditionOption = val;
            this.setState({stepControls});
        }
    },

    handleSelectSensor: function (selectedSensorOption) {

        let temp = {
            selectedSensorOption: selectedSensorOption,
            sensorValue: '',
            selectedRangeConditionOption: null
        };
        this.setState(temp);
    },

    handleSelectControlCondition: function (selectedControlCondition) {
        this.setState({
            selectedControlCondition,
            controlConditionValue: '',
            selectedControlConditionOption: null,
        });
    },

    handleSelectControlConditionOption: function (selectedControlConditionOption) {
        let temp = {
            selectedControlConditionOption,
        };
        this.setState(temp);
    },

    handleMixSelectControl: function (selectedControlOption, index, type) {
        let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        for (let i = 0; i < minMaxRanges.length; i++) {
            minMaxRanges[i].percentage = '';
            minMaxRanges[i].useState = 'off';
        }
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index].selectedControlOption = selectedControlOption;
            mixControls[index].selectedControlStateOption = null;
            mixControls[index].selectedControlWay = null;
            mixControls[index].settingValue = '';
            mixControls[index].minMaxRanges = minMaxRanges;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index].selectedControlOption = selectedControlOption;
            stepControls[index].selectedControlStateOption = null;
            stepControls[index].selectedControlWay = null;
            stepControls[index].settingValue = '';
            stepControls[index].minMaxRanges = minMaxRanges;
            this.setState({stepControls});
        } else if (type === TAB_STEP_STEP) {
            let autoControlSteps = deepcopy(this.state.autoControlSteps);
            autoControlSteps[index].selectedControlOption = selectedControlOption;
            autoControlSteps[index].selectedControlStateOption = null;
            autoControlSteps[index].selectedControlWay = null;
            autoControlSteps[index].settingValue = '';
            autoControlSteps[index].minMaxRanges = minMaxRanges;
            this.setState({autoControlSteps});
        }
    },

    handleMixSelectControlStateOption: function (selectedControlStateOption, index, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index].selectedControlStateOption = selectedControlStateOption;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index].selectedControlStateOption = selectedControlStateOption;
            this.setState({stepControls});
        } else if (type === TAB_STEP_STEP) {
            let autoControlSteps = deepcopy(this.state.autoControlSteps);
            autoControlSteps[index].selectedControlStateOption = selectedControlStateOption;
            this.setState({autoControlSteps});
        }
    },

    handleMixSelectControlWay: function (selectedControlWayOption, index, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index].selectedControlWayOption = selectedControlWayOption;
            mixControls[index].controlUnitName = CONTROL_RETURN_UNIT[selectedControlWayOption.value];
            mixControls[index].settingValue = '';
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index].selectedControlWayOption = selectedControlWayOption;
            stepControls[index].controlUnitName = CONTROL_RETURN_UNIT[selectedControlWayOption.value];
            stepControls[index].settingValue = '';
            this.setState({stepControls});
        } else if (type === TAB_STEP_STEP) {
            let autoControlSteps = deepcopy(this.state.autoControlSteps);
            autoControlSteps[index].selectedControlWayOption = selectedControlWayOption;
            autoControlSteps[index].controlUnitName = CONTROL_RETURN_UNIT[selectedControlWayOption.value];
            autoControlSteps[index].settingValue = '';
            this.setState({autoControlSteps});
        }
    },

    handleMixSettingValue: function (e, index, type) {
        let value = e.target.value;
        if (value < 0) {
            value = 0;
        }
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            if (mixControls[index].selectedControlWayOption && mixControls[index].selectedControlWayOption.value === 'percentage') {
                if (value > 100) {
                    value = 100;
                }
            }
            mixControls[index].settingValue = value;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            if (stepControls[index].selectedControlWayOption && stepControls[index].selectedControlWayOption.value === 'percentage') {
                if (value > 100) {
                    value = 100;
                }
            }
            stepControls[index].settingValue = value;
            this.setState({stepControls});
        } else if (type === TAB_STEP_STEP) {
            let autoControlSteps = deepcopy(this.state.autoControlSteps);
            if (autoControlSteps[index].selectedControlWayOption && autoControlSteps[index].selectedControlWayOption.value === 'percentage') {
                if (value > 100) {
                    value = 100;
                }
            }
            autoControlSteps[index].settingValue = value;
            this.setState({autoControlSteps});
        }
    },

    handleSelectControl: function (selectedControlOption) {
        let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        for (let i = 0; i < minMaxRanges.length; i++) {
            minMaxRanges[i].percentage = '';
            minMaxRanges[i].useState = 'off';
        }
        this.setState({
            selectedControlOption: selectedControlOption,
            selectedControlStateOption: null,
            selectedControlWay: null,
            settingValue: '',
            minMaxRanges,
            is_min_max_range: false
        });
    },

    handleSelectControlWay: function (selectedControlWayOption) {
        let temp = {
            selectedControlWayOption: selectedControlWayOption,
            controlUnitName: CONTROL_RETURN_UNIT[selectedControlWayOption.value],
            settingValue: ''
        };
        this.setState(temp);
    },

    handleMixSelectDelayWay: function (val, index) {
        let mixControls = deepcopy(this.state.mixControls);
        if (mixControls[index].selectedTimeWay && mixControls[index].selectedTimeWay.value === "single") {
            return this.props.showDialog({
                alertText: "특정시간 기준 자동제어는<br>실행횟수를 입력할 수 없습니다.",
            });
        }
        mixControls[index].selectedDelayWay = val;
        mixControls[index].delay = 0;
        this.setState({mixControls});
    },

    handleMixSelectActiveCount: function (val, index) {
        let mixControls = deepcopy(this.state.mixControls);
        if (mixControls[index].selectedTimeWay && mixControls[index].selectedTimeWay.value === "single") {
            return this.props.showDialog({
                alertText: "특정시간 기준 자동제어는<br>실행횟수를 입력할 수 없습니다.",
            });
        }
        mixControls[index].selectedActiveCount = val;
        mixControls[index].active_count = 0;
        this.setState({mixControls});
    },

    handleSelectSensorRange: function (selectedRangeConditionOption) {
        this.setState({selectedRangeConditionOption});
    },

    handleSelectControlStateOption: function (selectedControlStateOption) {
        let temp = {
            selectedControlStateOption: selectedControlStateOption,
            selectedControlWayOption: null,
            settingValue: ''
        };
        this.setState(temp);
    },

    handleInput: function (e, key) {
        let temp = {};
        temp[key] = e.target.value;
        this.setState(temp);
    },

    handleStandardSensor: function (e, key, originIndex, controlIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixSensors[controlIndex][key] = e.target.value;
            this.setState({mixControls});

        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepSensors[controlIndex][key] = e.target.value;
            this.setState({stepControls});

        }
    },

    handleStandardSensorValue: function (e, key, originIndex, controlIndex, standardIndex, type) {
        if (type === TAB_SENSOR) {
            let autoControlSensorOptions = deepcopy(this.state.autoControlSensorOptions);
            autoControlSensorOptions[standardIndex][key] = e.target.value;
            this.setState({autoControlSensorOptions});
        } else if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixSensors[controlIndex].autoControlSensorOptions[standardIndex][key] = e.target.value;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepSensors[controlIndex].autoControlSensorOptions[standardIndex][key] = e.target.value;
            this.setState({stepControls});
        }
    },

    handleControlConditionValue: function (e) {
        let value = e.target.value;
        if (value < 0) {
            value = 0;
        } else if (value > 100) {
            value = 100;
        }
        this.setState({
            controlConditionValue: value
        });
    },

    handleSettingValue: function (e) {
        let value = e.target.value;
        let temp = {};
        if (value < 0) {
            value = 0;
        }
        if (this.state.selectedControlWayOption && this.state.selectedControlWayOption.value === 'percentage') {
            if (value > 100) {
                value = 100;
            }
        }
        temp.settingValue = value;
        this.setState(temp);
    },

    handleAutoControlName: function (e) {
        this.setState({
            auto_control_name: e.target.value
        });
    },

    handleStepDelay: function (e) {
        this.setState({
            step_delay: e.target.value
        });
    },

    handleSetCount: function (e) {
        let temp = {};
        let value = e.target.value;
        if (value < 0) {
            value = 0;
        } else if (value > 100) {
            value = 100;
        }
        temp.setCount = value;
        this.setState(temp);
    },

    handleStepInput: function (e, key) {
        let temp = {};
        // temp[key] = e.target.value;
        // if(e.target.type === "number") {
        //     if(key === "active_count") {
        //         if(e.target.value < 1) {
        //             temp[key] = 1;
        //         } else if (e.target.value > 100) {
        //             temp[key] = 100;
        //         }
        //     } else {
        //         if(e.target.value < 0) {
        //             temp[key] = 0;
        //         } else if (e.target.value > 100) {
        //             temp[key] = 100;
        //         }
        //     }
        // }
        temp[key] = e.target.validity.valid || !e.target.value ? e.target.value : this.state[key];
        this.setState(temp);
    },

    handleTemperatureType: function (e, index, key, type) {

        if (type === TAB_MIX) {
            let mixControls = JSON.parse(JSON.stringify(this.state.mixControls));
            mixControls[index][key] = e.target.value;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = JSON.parse(JSON.stringify(this.state.stepControls));
            stepControls[index][key] = e.target.value;
            this.setState({stepControls});
        } else if (type === TAB_STEP_STEP) {
            let autoControlSteps = deepcopy(this.state.autoControlSteps);
            autoControlSteps[index][key] = e.target.value;
            this.setState({autoControlSteps});
        }

    },
    handleMixInput: function (e, index, key, type) {
        if (e.target.validity.valid || !e.target.value) {
            if (type === TAB_MIX) {
                let mixControls = JSON.parse(JSON.stringify(this.state.mixControls));
                mixControls[index][key] = e.target.value;
                this.setState({mixControls});
            } else if (type === TAB_STEP) {
                let stepControls = JSON.parse(JSON.stringify(this.state.stepControls));
                stepControls[index][key] = e.target.value;
                this.setState({stepControls});
            } else if (type === TAB_STEP_STEP) {
                let autoControlSteps = deepcopy(this.state.autoControlSteps);
                autoControlSteps[index][key] = e.target.value;
                this.setState({autoControlSteps});
            }
        }
    },

    handleIsMinMaxRange: function (e) {
        e && e.preventDefault();
        const is_min_max_range = !this.state.is_min_max_range;

        let temp = {
            is_min_max_range,
        };

        if (this.state.minMaxRanges.length === 0) {
            let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
            for (let i = 0; i < minMaxRanges.length; i++) {
                minMaxRanges[i].percentage = '';
                minMaxRanges[i].useState = 'off';
            }
            temp.minMaxRanges = minMaxRanges;
        }

        this.setState(temp);
    },

    handleMinMaxUseState: function (e, index) {
        let minMaxRanges = deepcopy(this.state.minMaxRanges);
        minMaxRanges[index].useState = minMaxRanges[index].useState === 'on' ? 'off' : 'on';
        this.setState({minMaxRanges});
    },

    handleMinMaxPercentage: function (e, index) {
        if (e.target.validity.valid) {
            let value = e.target.value;
            if (value < 0) {
                value = 0;
            } else if (value > 100) {
                value = 100;
            }
            let minMaxRanges = deepcopy(this.state.minMaxRanges);
            minMaxRanges[index].percentage = value;
            this.setState({minMaxRanges});
        }
    },

    handleAutoControlSensor: function (e, controlIndex, originIndex, type) {
        e && e.preventDefault();

        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            let mixSensors = mixControls[originIndex].mixSensors;
            mixSensors[controlIndex].is_use_option = !mixSensors[controlIndex].is_use_option;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            let stepSensors = stepControls[originIndex].stepSensors;
            stepSensors[controlIndex].is_use_option = !stepSensors[controlIndex].is_use_option;
            this.setState({stepControls});
        } else if (type === TAB_SENSOR) {
            this.setState({
                is_use_option: !this.state.is_use_option
            })
        }

    },

    handleMixIsMinMaxRange: function (e, index, type) {
        e && e.preventDefault();
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index].is_min_max_range = !mixControls[index].is_min_max_range;
            if (mixControls[index].minMaxRanges.length === 0) {
                let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
                for (let i = 0; i < minMaxRanges.length; i++) {
                    minMaxRanges[i].percentage = '';
                    minMaxRanges[i].useState = 'off';
                }
                mixControls[index].minMaxRanges = minMaxRanges;
            }
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index].is_min_max_range = !stepControls[index].is_min_max_range;
            if (stepControls[index].minMaxRanges.length === 0) {
                let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
                for (let i = 0; i < minMaxRanges.length; i++) {
                    minMaxRanges[i].percentage = '';
                    minMaxRanges[i].useState = 'off';
                }
                stepControls[index].minMaxRanges = minMaxRanges;
            }
            this.setState({stepControls});
        } else if (type === TAB_STEP_STEP) {
            let autoControlSteps = deepcopy(this.state.autoControlSteps);
            autoControlSteps[index].is_min_max_range = !autoControlSteps[index].is_min_max_range;
            if (autoControlSteps[index].minMaxRanges.length === 0) {
                let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
                for (let i = 0; i < minMaxRanges.length; i++) {
                    minMaxRanges[i].percentage = '';
                    minMaxRanges[i].useState = 'off';
                }
                autoControlSteps[index].minMaxRanges = minMaxRanges;
            }
            this.setState({autoControlSteps});
        }
    },

    handleMixMinMaxUseState: function (e, index, itemIndex, type) {
        e && e.preventDefault();
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index].minMaxRanges[itemIndex].useState = mixControls[index].minMaxRanges[itemIndex].useState === 'on' ? 'off' : 'on';
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index].minMaxRanges[itemIndex].useState = stepControls[index].minMaxRanges[itemIndex].useState === 'on' ? 'off' : 'on';
            this.setState({stepControls});
        } else if (type === TAB_STEP_STEP) {
            let autoControlSteps = deepcopy(this.state.autoControlSteps);
            autoControlSteps[index].minMaxRanges[itemIndex].useState = autoControlSteps[index].minMaxRanges[itemIndex].useState === 'on' ? 'off' : 'on';
            this.setState({autoControlSteps});
        }
    },

    handleMixMinMaxPercentage: function (e, index, itemIndex, type) {
        if (e.target.validity.valid) {
            let value = e.target.value;
            if (value < 0) {
                value = 0;
            } else if (value > 100) {
                value = 100;
            }
            if (type === TAB_MIX) {
                let mixControls = deepcopy(this.state.mixControls);
                mixControls[index].minMaxRanges[itemIndex].percentage = value;
                this.setState({mixControls});
            } else if (type === TAB_STEP) {
                let stepControls = deepcopy(this.state.stepControls);
                stepControls[index].minMaxRanges[itemIndex].percentage = value;
                this.setState({stepControls});
            } else if (type === TAB_STEP_STEP) {
                let autoControlSteps = deepcopy(this.state.autoControlSteps);
                autoControlSteps[index].minMaxRanges[itemIndex].percentage = value;
                this.setState({autoControlSteps});
            }
        }
    },

    handleSelectPBand: function (selectedPBand) {
        this.setState({selectedPBand});
    },

    handleSelectTemperatureSensor: function (value, index, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index].selectedTemperatureSensor = value;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index].selectedTemperatureSensor = value;
            this.setState({stepControls});
        } else if (type === TAB_STEP_STEP) {
            let autoControlSteps = deepcopy(this.state.autoControlSteps);
            autoControlSteps[index].selectedTemperatureSensor = value;
            this.setState({autoControlSteps});
        } else {
            this.setState({
                selectedTemperatureSensor: value
            })
        }
    },

    handlePBand: function (e, key) {
        if (e.target.validity.valid) {
            let value = e.target.value;
            if (key === 'pBandIntegral') {
                if (value < 0) {
                    value = 0;
                } else if (value > 100) {
                    value = 100;
                }
            }
            let temp = {};
            temp[key] = value;
            this.setState(temp);
        }
    },

    handleTemperature: function (e, key) {
        if (e.target.validity.valid) {
            let value = e.target.value;
            let temp = {};
            temp[key] = value;
            this.setState(temp);
        }
    },
    handleMixSelectPBand: function (selectedPBand, index, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index].selectedPBand = selectedPBand;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index].selectedPBand = selectedPBand;
            this.setState({stepControls});
        } else if (type === TAB_STEP_STEP) {
            let autoControlSteps = deepcopy(this.state.autoControlSteps);
            autoControlSteps[index].selectedPBand = selectedPBand;
            this.setState({autoControlSteps});
        } else if (type === TAB_TABLE) {
            // this.setState({
            //     handleMixSelectPBand : selectedPBand
            // });
        }
    },

    handleMixPBand: function (e, index, key, type) {
        if (e.target.validity.valid) {
            let value = e.target.value;
            if (key === 'pBandIntegral') {
                if (value < 0) {
                    value = 0;
                } else if (value > 100) {
                    value = 100;
                }
            }
            if (type === TAB_MIX) {
                let mixControls = deepcopy(this.state.mixControls);
                mixControls[index][key] = value;
                this.setState({mixControls});
            } else if (type === TAB_STEP) {
                let stepControls = deepcopy(this.state.stepControls);
                stepControls[index][key] = value;
                this.setState({stepControls});
            } else if (type === TAB_STEP_STEP) {
                let autoControlSteps = deepcopy(this.state.autoControlSteps);
                autoControlSteps[index][key] = value;
                this.setState({autoControlSteps});
            }
        }
    },

    handleIsUseTemperatureOption: function () {
        let temp = {
            is_temperature_control_option: !this.state.is_temperature_control_option
        };
        this.setState(temp);
    },

    handleIsUseTemperatureOptionType: function (e, index, key, type) {

        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index][key] = !mixControls[index][key];
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index][key] = !stepControls[index][key];
            this.setState({stepControls});
        } else if (type === TAB_STEP_STEP) {
            let autoControlSteps = deepcopy(this.state.autoControlSteps);
            autoControlSteps[index][key] = !autoControlSteps[index][key];
            this.setState({autoControlSteps});
        }
    },

    handleTableIsUseTemperatureOption: function (e, value) {
        if (this.state.tableSelectedPband || this.state.tableSelectedSensor || this.tableControls) {
            return this.props.showDialog({
                alertText: "제어장치가 추가된 상태에서는<br/>변경할 수 없습니다.",
            });
        }
        this.setState({tableSelectedIsUseTemperature: value});
    },

    handleTableSelectMotors: function (val, index, type) {
        let tableSelectedControls = this.state.tableSelectedControls;
        if (type === 'motor') {
            tableSelectedControls[index].selectedMotorOption = val;
        } else if (type === 'power') {
            tableSelectedControls[index].selectedPowerOption = val;
        }
        this.setState({tableSelectedControls});
    },

    addTableControls: function () {
        let tableSelectedControls = this.state.tableSelectedControls;
        tableSelectedControls.push({
            selectedMotorOption: null
        });

        this.setState({tableSelectedControls});
    },

    handleTableSelectPeriod: function (value, type) {
        let tableSelectedPeriod = null;
        if (!type) {
            tableSelectedPeriod = this.state.tableControls[value];
        } else {
            tableSelectedPeriod = value;
        }
        this.setState({tableSelectedPeriod});
    },

    handleTableIsUseTemperatureControl: function (e, index, key, type) {
        let tempTableControls = deepcopy(this.state.tempTableControls);
        tempTableControls[index][key] = !tempTableControls[index][key];
        this.setState({tempTableControls});
    },

    handleTableSelect: function (value, index, key, type) {
        let tempTableControls = deepcopy(this.state.tempTableControls);
        tempTableControls[index][key] = value;
        this.setState({tempTableControls});
    },

    handleTableSelectSensor: function (value, key) {
        if (key === 'pBand') {
            this.setState({tableSelectedPband: value});
        } else if (key === 'sensor') {
            this.setState({tableSelectedSensor: value});
        }
    },
    handleTableInput: function (e, index, key, type) {
        let tempTableControls = deepcopy(this.state.tempTableControls);
        tempTableControls[index][key] = e.target.value;
        this.setState({tempTableControls});
    },

    handleTableSensorSelect: function (value, index, sensorIndex, key, type) {
        let tempTableControls = deepcopy(this.state.tempTableControls);
        tempTableControls[index].tableSensors[sensorIndex][key] = value;
        this.setState({tempTableControls});
    },

    handleTableSensorInput: function (e, index, sensorIndex, key, type) {
        let tempTableControls = deepcopy(this.state.tempTableControls);
        tempTableControls[index].tableSensors[sensorIndex][key] = e.target.value;
        this.setState({tempTableControls});
    },
    addStep :function (e){
        if(this.state.stepControls[0].selectedTimeWay && this.state.stepControls[0].selectedTimeWay.value === "single") {
            return this.props.showDialog({
                alertText: "특정시간 기준 자동제어는<br>실행횟수를 입력할 수 없습니다.",
            });
        }

        let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        for (let i=0; i<minMaxRanges.length; i++) {
            minMaxRanges[i].useState = 'off';
            minMaxRanges[i].percentage = '';
        }

        let autoControlSteps = JSON.parse(JSON.stringify(this.state.autoControlSteps));
        let obj = {
            selectedPBand: null,
            pBandTemperature: '',
            pBandIntegral: '',
            settingValue: '',
            delay: '',
            recount: '',
            is_min_max_range: false,
            minMaxRanges,
            is_temperature_control_option : false,
            rise_time : '',
            drop_time : '',
            expect_temperature : '',
            expect_temperature_sensor_id : '',
            selectedTemperatureSensor : null,
        };
        if(this.state.controlOptionFromOutside) {
            obj.selectedControlOption = this.state.controlOptionFromOutside;
        }
        autoControlSteps.push(obj);
        this.setState({autoControlSteps});
    },

    addControl :function (e) {
        // if ((this.state.delay !== null && this.state.delay !== undefined) || (this.state.recount !== null && this.state.recount !== undefined)) {
        //     return this.props.showDialog({
        //         alertText: '지연/반복 설정된 자동제어는<br>제어를 추가할 수 없습니다.'
        //     });
        // }
        if (this.state.autoControlSteps && this.state.autoControlSteps.length) {
            return this.props.showDialog({
                alertText: '단계제어가 설정된 자동제어는<br>제어를 추가할 수 없습니다.'
            });
        }

        let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
        for (let i=0; i<minMaxRanges.length; i++) {
            minMaxRanges[i].useState = 'off';
            minMaxRanges[i].percentage = '';
        }

        let mixControls = JSON.parse(JSON.stringify(this.state.mixControls));

        let obj = {
            is_temperature_control_option : false,
            rise_time : '',
            drop_time : '',
            expect_temperature : '',
            expect_temperature_sensor_id : '',
            selectedTemperatureSensor : null,
            datePickerOpenState: {
                start_time: false,
                end_time: false,
            },
            selectedPBand: null,
            pBandTemperature: '',
            pBandIntegral: '',
            settingValue: '',
            delay: '',
            recount: '',
            active_count: 0,
            start_time: null,
            end_time: null,
            selectedTimeWay: null,
            selectedControlOption: null,
            selectedControlStateOption: null,
            selectedControlWayOption: null,
            selectedSunDateStartType: null,
            selectedSunDateEndType: null,
            selectedSunDateStartCondition: null,
            selectedSunDateEndCondition: null,
            selectedSunDateStartHour: '',
            selectedSunDateStartMinute: '',
            selectedSunDateEndHour: '',
            selectedSunDateEndMinute: '',
            selectedActiveCount: {value: 'infinite', label: "계속 실행"},
            selectedDelayWay: delayWayOptions[0],
            sensor_operator: "and",
            control_operator: 'and',
            conditional: {
                time: true,
                sensor: false,
                control: false,
            },
            mixSensors: [{
                sensorValue: '',
                is_use_option : false,
                option_max_value : '',
                option_min_value :'',
                autoControlSensorOptions : [{
                    selectedAutoControlSensor : null,
                    sensor : null,
                    sensor_id : '',
                    sensorUnit : '',
                    max_value : '',
                    min_value : '',
                    percentage : '',
                }],
            }],
            mixControls: [{
                controlConditionValue: ''
            }],

            is_min_max_range: false,
            minMaxRanges,
        };

        if(this.state.controlOptionFromOutside) {
            obj.selectedControlOption = this.state.controlOptionFromOutside;
        }

        mixControls.push(obj);
        this.setState({mixControls});
    },

    setTableControls :function () {
        let temp = [];
        for(let i = 0; i < 6; i ++){
            let table = deepcopy(CONSTANT.TABLE_CONTROL);
            table.value = i;
            table.label = (i+1) + '주기';
            temp.push(table);
        }
        this.setState({tempTableControls : temp});
        this.setState({tableControls : temp});
    },

    addTablePeriod :function(){
        let tableControls = deepcopy(this.state.tableControls);
        tableControls.push(CONSTANT.TABLE_CONTROL);

        tableControls[tableControls.length-1].value = tableControls.length-1;
        tableControls[tableControls.length-1].label = tableControls.length +'주기';

        this.setState({tableControls : tableControls});
    },

    removeTablePeriod :function (e, index) {
        let tableControls = deepcopy(this.state.tableControls);
        tableControls.splice(index, 1);
        this.setState({tableControls});
    },

    generateUpdateControlForm :function (autoControlItem, control) {

        let returnItem = {
            settingValue: '',
            controlUnitName: '',
            selectedPBand: null,
            pBandTemperature: '',
            pBandIntegral: ''
        };
        let selectedControlStateOption = {};
        let selectedControlWayOption = {};
        if (control) {
            returnItem.selectedControlOption = {
                label: control.control_name,
                origin: control,
                value: control.id
            };
        }

        if(autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none' ){
            returnItem.selectedControlOption = {
                key : 'controlGroup',
                value: {
                    wind_direction_type : autoControlItem.wind_direction_type,
                    window_position : autoControlItem.window_position,
                },
                label: translate.WINDOW_POSITION[autoControlItem.window_position] + ' ' + translate.WIND_DIRECTION[autoControlItem.wind_direction_type],
                origin: {type : 'motor'}
            };
        }

        if (control && control.type === TYPE_POWER) {
            if (autoControlItem.time !== null) {
                selectedControlStateOption = RETURN_CONTROL_OPTION.powerPartial[autoControlItem.state];

                if (autoControlItem.time % 3600 === 0) {
                    returnItem.settingValue = parseInt(autoControlItem.time / 3600);
                    selectedControlWayOption = RETURN_CONTROL_OPTION.powerUnitHour;
                    returnItem.controlUnitName = CONTROL_RETURN_UNIT[RETURN_CONTROL_OPTION.powerUnitHour.value];
                } else if (autoControlItem.time % 60 === 0) {
                    returnItem.settingValue = parseInt(autoControlItem.time / 60);
                    selectedControlWayOption = RETURN_CONTROL_OPTION.powerUnitMin;
                    returnItem.controlUnitName = CONTROL_RETURN_UNIT[RETURN_CONTROL_OPTION.powerUnitMin.value];
                } else {
                    returnItem.settingValue = autoControlItem.time;
                    selectedControlWayOption = RETURN_CONTROL_OPTION.powerUnitSec;
                    returnItem.controlUnitName = CONTROL_RETURN_UNIT[RETURN_CONTROL_OPTION.powerUnitSec.value];
                }
            } else {
                selectedControlStateOption = RETURN_CONTROL_OPTION.power[autoControlItem.state];
            }
        } else if (!control || control.type === TYPE_MOTOR) {
            if (autoControlItem.percentage !== null) {
                if (parseInt(autoControlItem.percentage) === 200) {
                    selectedControlStateOption = RETURN_CONTROL_OPTION.motor[autoControlItem.state];
                } else {
                    selectedControlStateOption = RETURN_CONTROL_OPTION.motorPartial[autoControlItem.state];
                    selectedControlWayOption = RETURN_CONTROL_OPTION.motorUnitPercentage;
                    returnItem.settingValue = autoControlItem.percentage;
                    returnItem.controlUnitName = CONTROL_RETURN_UNIT[RETURN_CONTROL_OPTION.motorUnitPercentage.value];
                }
            } else if (autoControlItem.time !== null) {
                selectedControlStateOption = RETURN_CONTROL_OPTION.motorPartial[autoControlItem.state];
                selectedControlWayOption = RETURN_CONTROL_OPTION.motorUnitTime;
                returnItem.settingValue = autoControlItem.time;
                returnItem.controlUnitName = CONTROL_RETURN_UNIT[RETURN_CONTROL_OPTION.motorUnitTime.value];
            } else if (autoControlItem.p_band_temperature !== null && autoControlItem.p_band_integral !== null) {
                selectedControlStateOption = RETURN_CONTROL_OPTION.motorPartial.open;
                selectedControlWayOption = RETURN_CONTROL_OPTION.motorUnitPBandIntegral;
                returnItem.pBandTemperature = autoControlItem.p_band_temperature;
                returnItem.pBandIntegral = autoControlItem.p_band_integral;
                returnItem.selectedPBand = {
                    label: autoControlItem.pBand.p_band_name,
                    value: autoControlItem.p_band_id
                };
            } else if (autoControlItem.p_band_temperature !== null) {
                selectedControlStateOption = RETURN_CONTROL_OPTION.motorPartial.open;
                selectedControlWayOption = RETURN_CONTROL_OPTION.motorUnitPBand;
                returnItem.pBandTemperature = autoControlItem.p_band_temperature;
                returnItem.selectedPBand = {
                    label: autoControlItem.pBand.p_band_name,
                    value: autoControlItem.p_band_id
                };
            }
        }

        returnItem.selectedControlStateOption = selectedControlStateOption;
        returnItem.selectedControlWayOption = selectedControlWayOption;
        return returnItem;
    },

    clickConditionLabel :function (e, index, key, type) {
        if(type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index].conditional[key] = !mixControls[index].conditional[key];
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index].conditional[key] = !stepControls[index].conditional[key];
            this.setState({stepControls});
        }
    },

    clickSensorOperator :function (e, val, index, type) {
        if(type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index].sensor_operator = val;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index].sensor_operator = val;
            this.setState({stepControls});
        }
    },

    clickControlOperator :function (e, val, index, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[index].control_operator = val;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[index].control_operator = val;
            this.setState({stepControls});
        }
    },

    addMixSensors :function (e, originIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixSensors.push({
                selectedRangeConditionOption : null,
                selectedSensorOption : null,
                sensorValue: '',
            });
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepSensors.push({
                selectedRangeConditionOption : null,
                selectedSensorOption : null,
                sensorValue: '',
            });
            this.setState({stepControls});
        }
    },

    addMixControls :function (e, originIndex, type) {
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixControls.push({
                controlConditionValue: ''
            });
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepControls.push({
                controlConditionValue: ''
            });
            this.setState({stepControls});
        }
    },

    removeMixSensor :function (e, sensorIndex, originIndex, type){
        e && e.preventDefault();
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixSensors.splice(sensorIndex, 1);
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepSensors.splice(sensorIndex, 1);
            this.setState({stepControls});
        }
    },

    removeMixControl :function (e, controlIndex, originIndex, type) {
        e && e.preventDefault();
        if (type === TAB_MIX) {
            let mixControls = deepcopy(this.state.mixControls);
            mixControls[originIndex].mixControls.splice(controlIndex, 1);
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = deepcopy(this.state.stepControls);
            stepControls[originIndex].stepControls.splice(controlIndex, 1);
            this.setState({stepControls});
        }
    },

    generateSensorOption :function (sensors) {
        let selectSensorOption = [];
        sensors.forEach(item => {
            let obj = {
                value: item.id,
                label: item.sensor_name,
                origin: item
            };
            selectSensorOption.push(obj);
        });
        return selectSensorOption;
    },

    generateControlOption :function (controls, key){

        let selectControlOption = [];

        if(key !== 'power') {
            selectControlOption = deepcopy(this.state.selectControlGroupOption);
        }

        controls.forEach(item => {
            let obj = {
                key : key,
                value: item.id,
                label: item.control_name,
                origin: item
            };
            if(item.id === this.state.selectedControlId && key === this.state.selectedControlKey) {
                this.setState({
                    selectedControlOption: obj,
                    controlOptionFromOutside: obj
                });
            }
            selectControlOption.push(obj);
        });

        return selectControlOption;
    },

    generateControlConditionOption :function (controls, key) {
        let selectControlConditionOption = [];
        controls.forEach(item => {
            let obj = {
                key : key,
                value: item.id,
                label: item.control_name,
                origin: item
            };

            selectControlConditionOption.push(obj);
        });
        this.setState({selectControlConditionOption});
    },

    generateControlGroupOption :function (controlGroups, key) {
        let selectControlOption = [];

        controlGroups.forEach(item => {
            let obj = {
                key : 'controlGroup',
                value: {
                    wind_direction_type : item.wind_direction_type,
                    window_position : item.window_position,
                },
                label: translate.WINDOW_POSITION[item.window_position] + ' ' + translate.WIND_DIRECTION[item.wind_direction_type],
                origin: item
            };


            if(this.state.selectedControlId && key ===  this.state.selectedControlKey && item.wind_direction_type === this.state.selectedControlId.wind_direction_type && item.window_position === this.state.selectedControlId.window_position) {
                this.setState({
                    selectedControlOption: obj,
                    controlOptionFromOutside: obj
                });
            }
            selectControlOption.push(obj);
        });
        return selectControlOption;
    },

    checkEveryday :function () {
        this.setState({
            is_mon: true,
            is_tue: true,
            is_wed: true,
            is_thur: true,
            is_fri: true,
            is_sat: true,
            is_sun: true,
            selectedDateOption: JSON.parse(JSON.stringify(CONSTANT.selectDateOption))
        });
    },

    addAutoControlSensorValue :function (e, originIndex, controlIndex, type) {

        if(type === TAB_SENSOR){
            let autoControlSensorOptions = deepcopy(this.state.autoControlSensorOptions);
            autoControlSensorOptions.push(
                {
                    sensor : null,
                    sensor_id : '',
                    max_value : '',
                    min_value : '',
                    percentage : '',
                }
            )
            this.setState({autoControlSensorOptions});
        }else if(type === TAB_MIX){
            let mixControls = this.state.mixControls;
            mixControls[originIndex].mixSensors[controlIndex].autoControlSensorOptions.push({
                sensor : null,
                sensor_id : '',
                max_value : '',
                min_value : '',
                percentage : '',
            })
            this.setState({mixControls});
        }else if(type === TAB_STEP){
            let stepControls = this.state.stepControls;
            stepControls[originIndex].stepSensors[controlIndex].autoControlSensorOptions.push({
                sensor : null,
                sensor_id : '',
                max_value : '',
                min_value : '',
                percentage : '',
            })
            this.setState({stepControls});
        }
    },

    removeAutoControlStandardSensors : function(e, originIndex, controlIndex, index, type) {
        if(type === TAB_SENSOR){
            let autoControlSensorOptions = deepcopy(this.state.autoControlSensorOptions);
            autoControlSensorOptions.splice(index, 1);
            if(autoControlSensorOptions.length === 0){
                return this.props.showDialog({
                    alertText: "자동조절 기준 센서는 하나 이상 존재해야 합니다."
                });
            }
            this.setState({autoControlSensorOptions});
        }else if(type === TAB_MIX){
            let mixControls = deepcopy(this.state.mixControls);

            if(mixControls[originIndex].mixSensors[controlIndex].autoControlSensorOptions.length === 1){
                return this.props.showDialog({
                    alertText: "자동조절 기준 센서는 하나 이상 존재해야 합니다."
                });
            }else{
                mixControls[originIndex].mixSensors[controlIndex].autoControlSensorOptions.splice(index, 1);
            }

            this.setState({mixControls});
        }else if(type === TAB_STEP){
            let stepControls = deepcopy(this.state.stepControls);
            if(stepControls[originIndex].stepSensors[controlIndex].autoControlSensorOptions.length === 1){
                return this.props.showDialog({
                    alertText: "자동조절 기준 센서는 하나 이상 존재해야 합니다."
                });
            }else{
                stepControls[originIndex].stepSensors[controlIndex].autoControlSensorOptions.splice(index, 1);
            }
            this.setState({stepControls});
        }
    },

    returnPowerTimeByState :function (state, time) {
        if (state === "hour") {
            return time * 60 * 60;
        } else if (state === "min") {
            return time * 60;
        }
        return time;
    },

    removeMixControlWrap :function (e, index) {
        let mixControls = JSON.parse(JSON.stringify(this.state.mixControls));
        mixControls.splice(index, 1);
        this.setState({mixControls});
    },

    copyThisControl :function (e, index) {
        let mixControls = JSON.parse(JSON.stringify(this.state.mixControls));
        mixControls.splice(index, 0, mixControls[index]);
        this.setState({mixControls});
    },

    closeDatePickerOnTouchOutside :function (e, key) {
        let datePickerOpenState = Object.assign({}, this.state.datePickerOpenState);
        datePickerOpenState[key] = false;
        this.setState({datePickerOpenState});
    },

    toggleMixDelayRecountInput :function (e, index, key, type){
        let value = e.target.value;
        if(type === TAB_MIX) {
            let mixControls = JSON.parse(JSON.stringify(this.state.mixControls));
            mixControls[index][key] = value;
            this.setState({mixControls});
        } else if (type === TAB_STEP) {
            let stepControls = JSON.parse(JSON.stringify(this.state.stepControls));
            stepControls[index][key] = value;
            this.setState({stepControls});
        } else if (type === TAB_STEP_STEP) {
            let autoControlSteps = JSON.parse(JSON.stringify(this.state.autoControlSteps));
            autoControlSteps[index][key] = value;
            this.setState({autoControlSteps});
        }
    },

    returnSunDateCalculate :function (type, condition, hour, minute) {
        if (type && type.value) {
            let result = type.value === 'rise' ? (parseInt(this.props.rise_hour) * 60 + parseInt(this.props.rise_minute)) : (parseInt(this.props.set_hour) * 60 + parseInt(this.props.set_minute));
            let time = parseInt(hour || 0) * 60 + parseInt(minute || 0);
            result += condition && condition.value === 'minus' ? -time : time;
            result %= 1440;
            if (result < 0) result += 1440;
            return attachZero(parseInt(result / 60)) + ':' + attachZero(result % 60);
        } else {
            return '기준을 선택해주세요.';
        }
    },

    addTableControlSensors :function (e, index){
        let tempTableControls = this.state.tempTableControls;
        tempTableControls[index].tableSensors.push(TABLE_CONTROL_SENSOR);
        this.setState({tempTableControls});
    },

    removeTableControlSensors :function (e, index, sensorIndex) {
        let tempTableControls = deepcopy(this.state.tempTableControls);
        if(tempTableControls[index].tableSensors.length === 1){
            return this.props.showDialog({
                alertText: "자동조절 기준 센서는 하나 이상 존재해야 합니다."
            });
        }
        tempTableControls[index].tableSensors.splice(sensorIndex, 1);
        this.setState({tempTableControls});
    },

    removeTableControls :function (e, index) {
        let tableSelectedControls = this.state.tableSelectedControls;
        if(tableSelectedControls.length === 1){
            return this.props.showDialog({
                alertText: "제어장치는 하나 이상 존재해야 합니다."
            });
        }
        tableSelectedControls.splice(index, 1);
        this.setState({tableSelectedControls});
    },

    setUpdateDays :function (item) {
        let body = {};
        let selectedDateOption = [];
        body.is_mon = item.is_mon;
        body.is_tue = item.is_tue;
        body.is_wed = item.is_wed;
        body.is_thur = item.is_thur;
        body.is_fri = item.is_fri;
        body.is_sat = item.is_sat;
        body.is_sun = item.is_sun;

        for (let key in body) {
            if (body[key]) {
                selectedDateOption.push({
                    value: key,
                    label: CONSTANT.const_days[key]
                })
            }
        }
        body.selectedDateOption = selectedDateOption;
        this.setState(body);
    },

    setSensorOperator :function (form, autoControlItem)  {
        form.sensor_operator = autoControlItem.sensor_operator;
    },

    setControlOperator :function (form, autoControlItem) {
        form.control_operator = autoControlItem.control_operator;
    },

    setRecountAndDelay :function (form, autoControlItem) {
        if (autoControlItem.delay !== null) form.delay = parseInt(autoControlItem.delay / 60);
        if (autoControlItem.recount !== null) form.recount = autoControlItem.recount;
    },

    tableCheck :function (controlIndex) {
        const _this = this;
        let control = deepcopy(this.state.tempTableControls[controlIndex]);
        let tableControls = deepcopy(this.state.tableControls);
        let result = false;

        if (!control.selectedTimeType) {
            result = false;
            return _this.props.showDialog({
                alertText: `${controlIndex + 1}주기의 작동기준을 선택해주세요.`
            });
        }else result = true;

        if (control.selectedTimeType.value !== "single" && !control.selectedTimeCondition) {
            result = false;

            return _this.props.showDialog({
                alertText: `${controlIndex + 1}주기의 기준상태를 선택해주세요.`
            });
        }else result = true;

        if (!control.selectedStartHour) {
            result = false;

            return _this.props.showDialog({
                alertText: `${controlIndex + 1}주기의 시작시간을 선택해주세요.`
            });
        }else result = true;

        if (!control.selectedStartMinute) {
            result = false;

            return _this.props.showDialog({
                alertText: `${controlIndex + 1}주기의 시작분을 선택해주세요.`
            });
        }else result = true;

        if (!control.expect_temperature) {
            result = false;

            return _this.props.showDialog({
                alertText: `${controlIndex + 1}주기의 희망온도를 입력해주세요.`
            });
        }else result = true;

        if (!control.rise_time) {
            result = false;

            return _this.props.showDialog({
                alertText: `${controlIndex + 1}주기의 상승시간을 입력해주세요.`
            });
        }else result = true;

        if (!control.drop_time) {
            result = false;

            return _this.props.showDialog({
                alertText: `${controlIndex + 1}주기의 하강시간을 입력해주세요.`
            });
        }else result = true;

        if (control.isUseExpectTemperatureControl) {
            if (!control.expect_temperature_max_value) {
                result = false;

                return _this.props.showDialog({
                    alertText: `${controlIndex + 1}주기의 희망온도 자동조절 최대값을 입력해주세요.`
                });
            }else result = true;

            if (!control.expect_temperature_min_value) {
                result = false;

                return _this.props.showDialog({
                    alertText: `${controlIndex + 1}주기의 희망온도 자동조절 최소값을 입력해주세요.`
                });
            }else result = true;

            if (control.tableSensors && control.tableSensors.length) {
                let maxPercentage = 0;
                control.tableSensors.forEach((sensor, sensorIndex) => {
                    if (!sensor.selectedAutoControlSensor) {
                        result = false;

                        return _this.props.showDialog({
                            alertText: `${controlIndex + 1}주기의  자동조절 기준 센서 ${sensorIndex + 1}번을 선택해주세요.`
                        });
                    }else result = true;

                    if (!sensor.max_value) {
                        result = false;

                        return _this.props.showDialog({
                            alertText: `${controlIndex + 1}주기의  자동조절 기준 센서 ${sensorIndex + 1}번의 최대값을 입력해주세요.`
                        });
                    }else result = true;

                    if (!sensor.min_value) {
                        result = false;

                        return _this.props.showDialog({
                            alertText: `${controlIndex + 1}주기의  자동조절 기준 센서 ${sensorIndex + 1}번의 최값을 입력해주세요.`
                        });
                    }else result = true;

                    if (!sensor.percentage) {
                        result = false;

                        return _this.props.showDialog({
                            alertText: `${controlIndex + 1}주기의  자동조절 기준 센서 ${sensorIndex + 1}번의 실행비율을 입력해주세요.`
                        });
                    }else{
                        maxPercentage += parseInt(sensor.percentage);
                    }

                    if(sensorIndex === control.tableSensors.length -1){
                        if(maxPercentage !== 100){
                            return _this.props.showDialog({
                                alertText: '총 실행 비율의 합이 100이 되어야 합니다.'
                            })
                        }
                    }
                });
            }else{
                return _this.props.showDialog({
                    alertText: `${controlIndex + 1}주기의 자동조절센서 값을 확인해주세요.`
                })
            }
        }

        return result;
    },

    tempSubmit :function (e, controlIndex) {
        const _this = this;
        let control = deepcopy(this.state.tempTableControls[controlIndex]);
        let tableControls = deepcopy(this.state.tableControls);

        if(!this.tableCheck(controlIndex)){
            return _this.props.showDialog({
                alertText: `${controlIndex + 1}주기의 자동조절센서 값을 확인해주세요.`
            })
        }else{
            tableControls[controlIndex] = control;
            this.setState({tableControls});
        }
    },

    showRecommendList:function (option) {
        this.setState({selectedCrop : option});
        this.getAutoControlRecommendList(option);
    },

    getAutoControlRecommendCrops : async function (){
        const _this = this;

        await this.setState({is_use_recommend: !this.state.is_use_recommend});

        if (this.state.is_use_recommend) {
            let body = {
                platform : window.platform,
            };

            await autoControlManager.findAutoControlRecommendCrops(body, (status, data) => {
                if (status === 200) {
                    _this.setState({recommendCrops: this.generateRecommendCrops(data.data.rows)});
                } else if (status === 404) {
                    _this.setState({recommendCrops: []});
                }
            })
        }
    },

    generateRecommendCrops : function (list) {
        const _this = this;
        let recommendCrops = [];
        list.forEach((data, index) => {
            recommendCrops.push({
                label : data.crop_name,
                value : data.crop_name,
            })
        });

        return recommendCrops;
    },

    getAutoControlRecommendList: function (option) {
        const _this = this;
        let body = {
            platform: window.platform,
            crop_type: option.value
        };

        autoControlManager.findAutoControlRecommend(body, (status, data) => {
            if (status === 200) {
                let recommendList = [];
                data.data.rows.forEach((data, index) => {
                    recommendList.push({
                        id : data.id,
                        auto_control_name : data.auto_control_name,
                        type : data.type,
                        crop_type : data.crop_type,
                        control_type : data.control_type,
                        control_name : data.control_name,
                        data : JSON.parse(data.data),
                    })
                });
                _this.setState({recommendList});
            } else if (status === 404) {
                _this.setState({recommendList: []});
            }
        })
    },

    getRecommendData: async function (e, item) {
        const _this = this;
        let recommendItem = await item;

        if (_this.state.isUpdate && item.data.type !== _this.state.currentTab) {
            return this.props.showDialog({
                alertText: "자동제어 타입은 수정할 수 없습니다."
            });
        }

        await _this.setState({
            recommendItem,
            selectedRecommendId: item.id,
        });

        _this.setState({
            currentTab: recommendItem.data.type,
            step_delay: recommendItem.data.step_delay ? recommendItem.data.step_delay / 60 : '',
            selectedDayType: recommendItem.data.date_type && recommendItem.data.date_type === 'day' ? {
                value: 'day',
                label: "요일별"
            } : {value: 'per', label: "기간별"},
            selectedActiveCount : {value: 'infinite', label: "계속 실행"},
            is_fri: recommendItem.data.is_fri ? recommendItem.data.is_fri : false,
            is_mon: recommendItem.data.is_mon ? recommendItem.data.is_mon : false,
            is_sat: recommendItem.data.is_sat ? recommendItem.data.is_sat : false,
            is_sun: recommendItem.data.is_sun ? recommendItem.data.is_sun : false,
            is_thur: recommendItem.data.is_thur ? recommendItem.data.is_thur : false,
            is_tue: recommendItem.data.is_tue ? recommendItem.data.is_tue : false,
            is_wed: recommendItem.data.is_wed ? recommendItem.data.is_wed : false,
            start_date: recommendItem.data.start_date ? moment(date(new Date(recommendItem.data.start_date), DATE_DISPLAY_FORMAT)) : null,
            end_date: recommendItem.data.end_date ? moment(date(new Date(recommendItem.data.end_date), DATE_DISPLAY_FORMAT)) : null,
            per_date: recommendItem.data.per_date ? recommendItem.data.per_date : '',
        });

        if(recommendItem.data.type === TAB_SENSOR) {
            this.setRecommendSensorData(recommendItem);
            this.handleRecommendData(recommendItem);
        }

        if(recommendItem.data.type === TAB_TIME) {
            this.setRecommendTimeData(recommendItem);
            this.handleRecommendData(recommendItem);
        }


        if(recommendItem.data.type === TAB_CONTROL) {
            this.setRecommendControlData(recommendItem);
            this.handleRecommendData(recommendItem);
        }

        if(recommendItem.data.type === TAB_MIX) {
            this.setRecommendMixData(recommendItem);
        }

        if(recommendItem.data.type === TAB_STEP) {
            this.setRecommendStepData(recommendItem);
        }


    },

    setRecommendStepData : function (item) {
        const autoControlItems = item.data.autoControlItems;
        let stepControls = [];
        let temp = {};
        if(item.data.autoControlSteps && item.data.autoControlSteps.length) {
            let autoControlSteps = [];

            item.data.autoControlSteps.forEach((autoControlStep, index) => {
                let stepControl = {};

                if(autoControlStep.autoControlControls){
                    stepControl.conditional.control = true;
                    stepControl.mixControls = [];
                    autoControlStep.autoControlControls.forEach((control, index) =>{
                        let recommendMixControls = {};
                        recommendMixControls.recommendControlControls = control;

                        if(control.control.type === 'motor') {
                            recommendMixControls.controlConditionValue = control.value;
                        }

                        recommendMixControls.selectedControlConditionOption = '';

                        if(control.control.type === 'motor'){

                            switch(control.condition) {
                                case 'over':
                                    recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[0];
                                    break;
                                case 'excess':
                                    recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[1];
                                    break;
                                case 'below':
                                    recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[2];
                                    break;
                                case 'under':
                                    recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[3];
                                    break;
                                case 'equal':
                                    recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[4];
                                    break;
                            }
                        }else{
                            if (control.value) {
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.power[0];
                            } else {
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.power[1];
                            }
                        }

                        stepControl.mixControls.push(recommendMixControls);
                    });

                }
                if (autoControlStep.recount !== null || autoControlStep.recount !== undefined) stepControl.recount = autoControlStep.recount+1;


                if (autoControlStep.period_type === delayWayOptions[2].value) {
                    stepControl.selectedDelayWay = Object.assign({}, delayWayOptions[2]);
                    stepControl.delay = '';
                } else {
                    if(autoControlStep.delay) {
                        stepControl.selectedDelayWay = Object.assign({}, delayWayOptions[1]);
                        stepControl.delay = autoControlStep.delay / 60;
                    } else {
                        stepControl.selectedDelayWay = Object.assign({}, delayWayOptions[0]);
                        stepControl.delay = '';
                    }
                }

                stepControl.recommendControl = autoControlStep;

                if(autoControlStep.control_id) {
                    stepControl.selectedControlStateOption = CONSTANT.CONTROL_OPTIONS.RECOMMEND_CONTROL_STATE_UNIT[autoControlStep.state];
                    if((autoControlStep.state === 'open-partial' || autoControlStep.state === 'close-partial') && autoControlStep.time) {
                        stepControl.selectedControlWayOption = {label: "초 단위", value: "time"};
                        stepControl.settingValue = autoControlStep.time;
                        stepControl.controlUnitName = '초';
                    }else if((autoControlStep.state === 'open-partial' || autoControlStep.state === 'close-partial') && autoControlStep.percentage) {
                        stepControl.selectedControlWayOption = {label: "% 단위", value: "percentage"};
                        stepControl.settingValue = autoControlStep.percentage;
                        stepControl.controlUnitName = '%';
                    }
                }


                if(autoControlStep.active_count !== null) {
                    stepControl.active_count = autoControlStep.active_count;
                    if(autoControlStep.active_count > 0) {
                        stepControl.selectedActiveCount = {value: 'limit', label: "실행횟수 입력"};
                    } else {
                        stepControl.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                    }
                }

                stepControl.is_min_max_range = autoControlStep.is_min_max_range;
                let stepMinMaxRanges = [];
                if(autoControlStep.autoControlItemMinMaxes){
                    autoControlStep.autoControlItemMinMaxes.forEach(autoControlItemMinMax => {
                        stepMinMaxRanges.push({
                            name : autoControlItemMinMax.name,
                            percentage : autoControlItemMinMax.percentage
                        });
                    });
                }

                stepControl.recommendMinMax = stepMinMaxRanges;

                autoControlSteps.push(stepControl);
            });

            temp.autoControlSteps = autoControlSteps;

        }

        autoControlItems.forEach((autoControl,index) => {
            let stepControl = {};

            stepControl.conditional = {
                time: false,
                sensor: false,
                control: false,
            };

            stepControl.sensor_operator = autoControl.sensor_operator;
            stepControl.control_operator = autoControl.control_operator;

            if(autoControl.autoControlSensors) {
                stepControl.conditional.sensor = true;
                let sensorItem = autoControl.autoControlSensors;
                stepControl.stepSensors = [{
                    selectedRangeConditionOption : null,
                    selectedSensorOption : null,
                    sensorValue: '',
                    is_use_option : false,
                    option_max_value : '',
                    option_min_value :'',
                    autoControlSensorOptions : [{
                        selectedAutoControlSensor : null,
                        sensor : null,
                        sensor_id : '',
                        sensorUnit : '',
                        max_value : '',
                        min_value : '',
                        percentage : '',
                    }],
                }];

                sensorItem.forEach((sensor, sensorIndex) => {
                    stepControl.recommendSensorTxt = `센서조건 ${sensorIndex+1}`;

                    let sensorUnit = sensor.sensor.unit.split(':');

                    stepControl.recommendSensorTxt += `<br/>센서 ${sensor.sensor_id}가`;
                    if(sensorUnit.length === 1) {
                        stepControl.recommendSensorUnit = sensorUnit[0];

                        if(sensor.value) {
                            stepControl.recommendSensorTxt += `<br/>${sensor.value}${sensor.sensor.unit}${sensor.textValue}이 되면`;
                        }
                    }else{
                        stepControl.recommendSensorTxt += `<br/>${sensor.textValue}이 되면`;
                    }

                    if(sensor.is_use_option) {
                        stepControl.recommendSensorTxt += `<br/>자동조절값 사용 (최대 : ${sensor.option_max_value}${stepControl.recommendSensorUnit} 최소 : ${sensor.option_min_value}${stepControl.recommendSensorUnit})`;
                        if(sensor.autoControlSensorOptions && sensor.autoControlSensorOptions.length) {
                            sensor.autoControlSensorOptions.forEach((sensorOption, sensorOptionIndex) => {
                                stepControl.recommendSensorTxt += `<br/>자동조절센서${sensorOptionIndex+1} : ${sensorOption.sensor_id} (최대 :${sensorOption.max_value}${stepControl.recommendSensorUnit} 최소 : ${sensorOption.min_value}${stepControl.recommendSensorUnit} 실행비율 : ${sensorOption.percentage}%)`;
                            });
                        }
                    }
                });
            }
            if(autoControl.active_count !== null) {
                stepControl.active_count = autoControl.active_count;
                if(autoControl.active_count > 0) {
                    stepControl.selectedActiveCount = {value: 'limit', label: "실행횟수 입력"};
                } else {
                    stepControl.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                }
            } else {
                stepControl.active_count = '';
            }

            if (autoControl.is_sun_date) {
                stepControl.conditional.time = true;
                stepControl.selectedTimeWay = {value: "sunDate", label: "일출 일몰 시간 범위"};
                if (autoControl.sun_date_start_type === 'rise') {
                    stepControl.selectedSunDateStartType = Object.assign({}, selectSunDateTypes[0]);
                } else {
                    stepControl.selectedSunDateStartType = Object.assign({}, selectSunDateTypes[1]);
                }
                if (autoControl.sun_date_start_time >= 0) {
                    stepControl.selectedSunDateStartCondition = Object.assign({}, selectPlusMinus[0]);
                } else {
                    stepControl.selectedSunDateStartCondition = Object.assign({}, selectPlusMinus[1]);
                }
                if (autoControl.sun_date_end_type === 'rise') {
                    stepControl.selectedSunDateEndType = Object.assign({}, selectSunDateTypes[0]);
                } else {
                    stepControl.selectedSunDateEndType = Object.assign({}, selectSunDateTypes[1]);
                }
                if (autoControl.sun_date_end_time >= 0) {
                    stepControl.selectedSunDateEndCondition = Object.assign({}, selectPlusMinus[0]);
                } else {
                    stepControl.selectedSunDateEndCondition = Object.assign({}, selectPlusMinus[1]);
                }
                let startTime = Math.abs(autoControl.sun_date_start_time);
                stepControl.selectedSunDateStartHour = parseInt(startTime / 60);
                stepControl.selectedSunDateStartMinute = startTime % 60;
                let endTime = Math.abs(autoControl.sun_date_end_time);
                stepControl.selectedSunDateEndHour = parseInt(endTime / 60);
                stepControl.selectedSunDateEndMinute = endTime % 60;

            } else {
                if (autoControl.start_hour === null && autoControl.start_minute === null &&
                    autoControl.end_hour === null && autoControl.end_minute === null) {
                    stepControl.selectedTimeWay = {value: "noSelect", label: "선택 안함"};
                } else {
                    stepControl.conditional.time = true;
                    stepControl.selectedStartHour = {
                        value: autoControl.start_hour,
                        label: `${attachZero(autoControl.start_hour)}시`
                    };
                    stepControl.selectedStartMinute = {
                        value: autoControl.start_minute,
                        label: `${attachZero(autoControl.start_minute)}분`
                    };
                    if (!autoControl.end_hour  && !autoControl.end_minute) {
                        stepControl.selectedTimeWay = {value: "single", label: "특정 시간"};
                    } else {
                        stepControl.selectedTimeWay = {value: "range", label: "시간 범위"};
                        stepControl.selectedEndHour = {
                            value: autoControl.end_hour,
                            label: `${attachZero(autoControl.end_hour)}시`
                        };
                        stepControl.selectedEndMinute = {
                            value: autoControl.end_minute,
                            label: `${attachZero(autoControl.end_minute)}분`
                        };
                    }
                }
            }

            if(autoControl.autoControlControls){
                stepControl.conditional.control = true;
                stepControl.mixControls = [];
                autoControl.autoControlControls.forEach((control, index) =>{
                    let recommendMixControls = {};
                    recommendMixControls.recommendControlControls = control;

                    if(control.control.type === 'motor') {
                        recommendMixControls.controlConditionValue = control.value;
                    }

                    recommendMixControls.selectedControlConditionOption = '';

                    if(control.control.type === 'motor'){

                        switch(control.condition) {
                            case 'over':
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[0];
                                break;
                            case 'excess':
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[1];
                                break;
                            case 'below':
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[2];
                                break;
                            case 'under':
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[3];
                                break;
                            case 'equal':
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[4];
                                break;
                        }
                    }else{
                        if (control.value) {
                            recommendMixControls.selectedControlConditionOption = selectControlConditions.power[0];
                        } else {
                            recommendMixControls.selectedControlConditionOption = selectControlConditions.power[1];
                        }
                    }

                    stepControl.mixControls.push(recommendMixControls);
                });

            }

            if (autoControl.recount) {
                stepControl.recount = autoControl.recount+1
            } else {
                stepControl.recount = '';
            };

            if (autoControl.period_type === delayWayOptions[2].value) {
                stepControl.selectedDelayWay = Object.assign({}, delayWayOptions[2]);
                stepControl.delay = '';
            } else {
                if(autoControl.delay) {
                    stepControl.selectedDelayWay = Object.assign({}, delayWayOptions[1]);
                    stepControl.delay = autoControl.delay / 60;
                } else {
                    stepControl.selectedDelayWay = Object.assign({}, delayWayOptions[0]);
                    stepControl.delay = '';
                }
            }

            stepControl.recommendControl = autoControl;

            if(autoControl.control_id) {
                stepControl.selectedControlStateOption = CONSTANT.CONTROL_OPTIONS.RECOMMEND_CONTROL_STATE_UNIT[autoControl.state];
                if((autoControl.state === 'open-partial' || autoControl.state === 'close-partial') && autoControl.time) {
                    stepControl.selectedControlWayOption = {label: "초 단위", value: "time"};
                    stepControl.settingValue = autoControl.time;
                    stepControl.controlUnitName = '초';
                }else if((autoControl.state === 'open-partial' || autoControl.state === 'close-partial') && autoControl.percentage) {
                    stepControl.selectedControlWayOption = {label: "% 단위", value: "percentage"};
                    stepControl.settingValue = autoControl.percentage;
                    stepControl.controlUnitName = '%';
                }
            }

            if (autoControl.period_type === delayWayOptions[2].value) {
                stepControl.selectedDelayWay = Object.assign({}, delayWayOptions[2]);
            } else {
                if(autoControl.delay) {
                    stepControl.selectedDelayWay = Object.assign({}, delayWayOptions[1]);
                    stepControl.delay = autoControl.delay / 60;
                } else {
                    stepControl.selectedDelayWay = Object.assign({}, delayWayOptions[0]);
                }
            }

            if(autoControl.active_count !== null) {
                stepControl.active_count = autoControl.active_count;
                if(autoControl.active_count > 0) {
                    stepControl.selectedActiveCount = {value: 'limit', label: "실행횟수 입력"};
                } else {
                    stepControl.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                }
            }

            stepControl.is_min_max_range = autoControl.is_min_max_range;
            let minMaxRanges = [];
            if(autoControl.autoControlItemMinMaxes){
                autoControl.autoControlItemMinMaxes.forEach(autoControlItemMinMax => {
                    minMaxRanges.push({
                        name : autoControlItemMinMax.name,
                        percentage : autoControlItemMinMax.percentage
                    });
                });
            }

            stepControl.recommendMinMax = minMaxRanges;

            stepControls.push(stepControl);
        });

        this.setState({stepControls});
        this.setState(temp);
    },

    setRecommendMixData:function(item){
        const autoControlItems = item.data.autoControlItems;
        let mixControls = [];
        autoControlItems.forEach((autoControl,index) => {
            let mixControl = {};

            mixControl.conditional = {
                time: false,
                sensor: false,
                control: false,
            };

            mixControl.sensor_operator = autoControl.sensor_operator;
            mixControl.control_operator = autoControl.control_operator;

            if(autoControl.autoControlSensors) {
                mixControl.conditional.sensor = true;
                let sensorItem = autoControl.autoControlSensors;
                mixControl.recommendSensorTxt = `제어 ${index+1}`;
                mixControl.mixSensors = [{
                    selectedRangeConditionOption : null,
                    selectedSensorOption : null,
                    sensorValue: '',
                    is_use_option : false,
                    option_max_value : '',
                    option_min_value :'',
                    autoControlSensorOptions : [{
                        selectedAutoControlSensor : null,
                        sensor : null,
                        sensor_id : '',
                        sensorUnit : '',
                        max_value : '',
                        min_value : '',
                        percentage : '',
                    }],
                }];



                sensorItem.forEach((sensor, sensorIndex) => {
                    mixControl.recommendSensorTxt += `<br/>센서조건 ${sensorIndex+1}`;

                    let sensorUnit = sensor.sensor.unit.split(':');

                    if(sensorUnit.length === 1) {
                        mixControl.recommendSensorUnit = sensorUnit[0];

                        if(sensor.value) {
                            mixControl.recommendSensorTxt += `<br/>${sensor.value}${sensor.sensor.unit}${sensor.textValue}이 되면`;
                        }
                    }else{
                        mixControl.recommendSensorTxt += `<br/>${sensor.textValue}이 되면`;
                    }

                    if(sensor.is_use_option) {
                        mixControl.recommendSensorTxt += `<br/>자동조절값 사용 (최대 : ${sensor.option_max_value}${mixControl.recommendSensorUnit} 최소 : ${sensor.option_min_value}${mixControl.recommendSensorUnit})`;
                        if(sensor.autoControlSensorOptions && sensor.autoControlSensorOptions.length) {
                            sensor.autoControlSensorOptions.forEach((sensorOption, sensorOptionIndex) => {
                                mixControl.recommendSensorTxt += `<br/>자동조절센서${sensorOptionIndex+1} : ${sensorOption.sensor_id} (최대 :${sensorOption.max_value}${mixControl.recommendSensorUnit} 최소 : ${sensorOption.min_value}${mixControl.recommendSensorUnit} 실행비율 : ${sensorOption.percentage}%)`;
                            });
                        }
                    }
               });
            }
            if(autoControl.active_count !== null) {
                mixControl.active_count = autoControl.active_count;
                if(autoControl.active_count > 0) {
                    mixControl.selectedActiveCount = {value: 'limit', label: "실행횟수 입력"};
                } else {
                    mixControl.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                }
            } else {
                mixControl.active_count = '';
            }
            if (autoControl.is_sun_date) {
                mixControl.conditional.time = true;
                mixControl.selectedTimeWay = {value: "sunDate", label: "일출 일몰 시간 범위"};

                if (autoControl.sun_date_start_type === 'rise') {
                    mixControl.selectedSunDateStartType = Object.assign({}, selectSunDateTypes[0]);
                } else {
                    mixControl.selectedSunDateStartType = Object.assign({}, selectSunDateTypes[1]);
                }
                if (autoControl.sun_date_start_time >= 0) {
                    mixControl.selectedSunDateStartCondition = Object.assign({}, selectPlusMinus[0]);
                } else {
                    mixControl.selectedSunDateStartCondition = Object.assign({}, selectPlusMinus[1]);
                }
                if (autoControl.sun_date_end_type === 'rise') {
                    mixControl.selectedSunDateEndType = Object.assign({}, selectSunDateTypes[0]);
                } else {
                    mixControl.selectedSunDateEndType = Object.assign({}, selectSunDateTypes[1]);
                }
                if (autoControl.sun_date_end_time >= 0) {
                    mixControl.selectedSunDateEndCondition = Object.assign({}, selectPlusMinus[0]);
                } else {
                    mixControl.selectedSunDateEndCondition = Object.assign({}, selectPlusMinus[1]);
                }
                let startTime = Math.abs(autoControl.sun_date_start_time);
                mixControl.selectedSunDateStartHour = parseInt(startTime / 60);
                mixControl.selectedSunDateStartMinute = startTime % 60;
                let endTime = Math.abs(autoControl.sun_date_end_time);
                mixControl.selectedSunDateEndHour = parseInt(endTime / 60);
                mixControl.selectedSunDateEndMinute = endTime % 60;

            } else {
                if (!autoControl.start_hour && !autoControl.start_minute ) {
                    mixControl.selectedTimeWay = {value: "noSelect", label: "선택 안함"};
                } else {
                    mixControl.conditional.time = true;
                    mixControl.selectedStartHour = {
                        value: autoControl.start_hour,
                        label: `${attachZero(autoControl.start_hour)}시`
                    };
                    mixControl.selectedStartMinute = {
                        value: autoControl.start_minute,
                        label: `${attachZero(autoControl.start_minute)}분`
                    };
                    if (!autoControl.end_hour  && !autoControl.end_minute) {
                        mixControl.selectedTimeWay = {value: "single", label: "특정 시간"};
                    } else {
                        mixControl.selectedTimeWay = {value: "range", label: "시간 범위"};
                        mixControl.selectedEndHour = {
                            value: autoControl.end_hour,
                            label: `${attachZero(autoControl.end_hour)}시`
                        };
                        mixControl.selectedEndMinute = {
                            value: autoControl.end_minute,
                            label: `${attachZero(autoControl.end_minute)}분`
                        };
                    }
                }
            }

            if(autoControl.autoControlControls){
                mixControl.conditional.control = true;
                mixControl.mixControls = [];
                autoControl.autoControlControls.forEach((control, index) =>{
                    let recommendMixControls = {};
                    recommendMixControls.recommendControlControls = control;

                    if(control.control.type === 'motor') {
                        recommendMixControls.controlConditionValue = control.value;
                    }

                    recommendMixControls.selectedControlConditionOption = '';

                    if(control.control.type === 'motor'){

                        switch(control.condition) {
                            case 'over':
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[0];
                                break;
                            case 'excess':
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[1];
                                break;
                            case 'below':
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[2];
                                break;
                            case 'under':
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[3];
                                break;
                            case 'equal':
                                recommendMixControls.selectedControlConditionOption = selectControlConditions.motor[4];
                                break;
                        }
                    }else{
                        if (control.value) {
                            recommendMixControls.selectedControlConditionOption = selectControlConditions.power[0];
                        } else {
                            recommendMixControls.selectedControlConditionOption = selectControlConditions.power[1];
                        }
                    }

                    mixControl.mixControls.push(recommendMixControls);
                });

            }

            if (autoControl.recount) {
                mixControl.recount = autoControl.recount+1
            } else {
                mixControl.recount = '';
            };

            if (autoControl.period_type === delayWayOptions[2].value) {
                mixControl.selectedDelayWay = Object.assign({}, delayWayOptions[2]);
                mixControl.delay = '';
            } else {
                if(autoControl.delay) {
                    mixControl.selectedDelayWay = Object.assign({}, delayWayOptions[1]);
                    mixControl.delay = autoControl.delay / 60;
                } else {
                    mixControl.selectedDelayWay = Object.assign({}, delayWayOptions[0]);
                    mixControl.delay = '';
                }
            }

            mixControl.recommendControl = autoControl;

            if(autoControl.control_id) {
                mixControl.selectedControlStateOption = CONSTANT.CONTROL_OPTIONS.RECOMMEND_CONTROL_STATE_UNIT[autoControl.state];
                if((autoControl.state === 'open-partial' || autoControl.state === 'close-partial') && autoControl.time) {
                    mixControl.selectedControlWayOption = {label: "초 단위", value: "time"};
                    mixControl.settingValue = autoControl.time;
                    mixControl.controlUnitName = '초';
                }else if((autoControl.state === 'open-partial' || autoControl.state === 'close-partial') && autoControl.percentage) {
                    mixControl.selectedControlWayOption = {label: "% 단위", value: "percentage"};
                    mixControl.settingValue = autoControl.percentage;
                    mixControl.controlUnitName = '%';
                }
            }

            if (autoControl.period_type === delayWayOptions[2].value) {
                mixControl.selectedDelayWay = Object.assign({}, delayWayOptions[2]);
            } else {
                if(autoControl.delay) {
                    mixControl.selectedDelayWay = Object.assign({}, delayWayOptions[1]);
                    mixControl.delay = autoControl.delay / 60;
                } else {
                    mixControl.selectedDelayWay = Object.assign({}, delayWayOptions[0]);
                }
            }

            if(autoControl.active_count !== null) {
                mixControl.active_count = autoControl.active_count;
                if(autoControl.active_count > 0) {
                    mixControl.selectedActiveCount = {value: 'limit', label: "실행횟수 입력"};
                } else {
                    mixControl.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                }
            }

            mixControl.is_min_max_range = autoControl.is_min_max_range;
            let minMaxRanges = [];
            if(autoControl.autoControlItemMinMaxes){
                autoControl.autoControlItemMinMaxes.forEach(autoControlItemMinMax => {
                    minMaxRanges.push({
                        name : autoControlItemMinMax.name,
                        percentage : autoControlItemMinMax.percentage
                    });
                });
            }

            mixControl.recommendMinMax = minMaxRanges;

            mixControls.push(mixControl);
        });

        this.setState({mixControls})
    },

    setRecommendControlData : function(item) {
        const _this = this;
        let controlOne = item.data.autoControlItems[0];
        let control = controlOne.autoControlControls[0];
        let obj = {};

        obj.recommendControlControls = control;

        if(control.control.type === 'motor') {
            obj.controlConditionValue = control.value;
        }

        obj.selectedControlConditionOption = '';

        if(controlOne.autoControlControls[0].control.type === 'motor'){

            switch(controlOne.autoControlControls[0].condition) {
                case 'over':
                    obj.selectedControlConditionOption = selectControlConditions.motor[0];
                    break;
                case 'excess':
                    obj.selectedControlConditionOption = selectControlConditions.motor[1];
                    break;
                case 'below':
                    obj.selectedControlConditionOption = selectControlConditions.motor[2];
                    break;
                case 'under':
                    obj.selectedControlConditionOption = selectControlConditions.motor[3];
                    break;
                case 'equal':
                    obj.selectedControlConditionOption = selectControlConditions.motor[4];
                    break;
            }
        }else{
            if (controlOne.autoControlControls[0].value) {
                obj.selectedControlConditionOption = selectControlConditions.power[0];
            } else {
                obj.selectedControlConditionOption = selectControlConditions.power[1];

            }
        }

        _this.setState(obj);
    },
    setRecommendSensorData : function(item) {
        const _this = this;
        let controlOne = item.data.autoControlItems[0];
        let sensorTxt = '';
        let obj = {};

        if (_this.state.selectSensorOption && controlOne.autoControlSensors) {

            let sensor = controlOne.autoControlSensors[0];
            obj.recommendSensorTxt = `추천센서 : ${sensor.sensor_id}가 `;

            obj.recommendSensor =sensor;

            let sensorUnit = sensor.sensor.unit.split(':');

            if(sensorUnit.length === 1) {
                obj.recommendSensorUnit = sensorUnit[0];

                if(sensor.value) {
                    obj.recommendSensorTxt += `${sensor.value}${sensor.sensor.unit}${sensor.textValue}이 되면`;
                }
            }else{
                obj.recommendSensorTxt += `${sensor.textValue}이 되면`
            }


            if(sensor.is_use_option) {
              obj.recommendSensorTxt += `<br/>자동조절값 사용 (최대 : ${sensor.option_max_value}${obj.recommendSensorUnit} 최소 : ${sensor.option_min_value}${obj.recommendSensorUnit})`;
                if(sensor.autoControlSensorOptions && sensor.autoControlSensorOptions.length) {
                    sensor.autoControlSensorOptions.forEach((sensorOption, sensorOptionIndex) => {
                        obj.recommendSensorTxt += `<br/>자동조절센서${sensorOptionIndex+1} : ${sensorOption.sensor_id} (최대 :${sensorOption.max_value}${obj.recommendSensorUnit} 최소 : ${sensorOption.min_value}${obj.recommendSensorUnit} 실행비율 : ${sensorOption.percentage}%)`;
                    });
                }
            }

            this.setState(obj);
        }
    },
    setRecommendTimeData : function (item) {
        const _this = this;

        let selectedTimeWay = {};
        let controlOne = item.data.autoControlItems[0];

        if (controlOne.is_sun_date) {
            selectedTimeWay = Object.assign({}, selectTimeWay[3]);
            let selectedSunDateStartType = {};
            let selectedSunDateStartCondition = {};
            let selectedSunDateEndType = {};
            let selectedSunDateEndCondition = {};
            let selectedSunDateStartHour = '';
            let selectedSunDateStartMinute = '';
            let selectedSunDateEndHour = '';
            let selectedSunDateEndMinute = '';
            if (controlOne.sun_date_start_type === 'rise') {
                selectedSunDateStartType = Object.assign({}, selectSunDateTypes[0]);
            } else {
                selectedSunDateStartType = Object.assign({}, selectSunDateTypes[1]);
            }
            if (controlOne.sun_date_start_time >= 0) {
                selectedSunDateStartCondition = Object.assign({}, selectPlusMinus[0]);
            } else {
                selectedSunDateStartCondition = Object.assign({}, selectPlusMinus[1]);
            }
            if (controlOne.sun_date_end_type === 'rise') {
                selectedSunDateEndType = Object.assign({}, selectSunDateTypes[0]);
            } else {
                selectedSunDateEndType = Object.assign({}, selectSunDateTypes[1]);
            }
            if (controlOne.sun_date_end_time >= 0) {
                selectedSunDateEndCondition = Object.assign({}, selectPlusMinus[0]);
            } else {
                selectedSunDateEndCondition = Object.assign({}, selectPlusMinus[1]);
            }
            let startTime = Math.abs(controlOne.sun_date_start_time);
            selectedSunDateStartHour = parseInt(startTime / 60);
            selectedSunDateStartMinute = startTime % 60;
            let endTime = Math.abs(controlOne.sun_date_end_time);
            selectedSunDateEndHour = parseInt(endTime / 60);
            selectedSunDateEndMinute = endTime % 60;

            _this.setState({
                selectedTimeWay,
                selectedSunDateStartType,
                selectedSunDateStartCondition,
                selectedSunDateEndType,
                selectedSunDateEndCondition,
                selectedSunDateStartHour,
                selectedSunDateStartMinute,
                selectedSunDateEndHour,
                selectedSunDateEndMinute
            });
        } else {
            let selectedStartHour = {};
            let selectedStartMinute = {};
            let selectedEndHour = {};
            let selectedEndMinute = {};
            if (controlOne.start_hour === null && controlOne.start_minute === null) {
                selectedTimeWay = {value: "noSelect", label: "선택 안함"};
            } else {
                selectedStartHour = {
                    value: controlOne.start_hour,
                    label: `${attachZero(controlOne.start_hour)}시`
                };
                selectedStartMinute = {
                    value: controlOne.start_minute,
                    label: `${attachZero(controlOne.start_minute)}분`
                };
                if (controlOne.end_hour === null && controlOne.end_minute === null) {
                    selectedTimeWay = {value: "single", label: "특정 시간"};
                } else {
                    selectedTimeWay = {value: "range", label: "시간 범위"};
                    selectedEndHour = {
                        value: controlOne.end_hour,
                        label: `${attachZero(controlOne.end_hour)}시`
                    };
                    selectedEndMinute = {
                        value: controlOne.end_minute,
                        label: `${attachZero(controlOne.end_minute)}분`
                    };
                }
            }

            _this.setState({
                selectedTimeWay,
                selectedStartHour,
                selectedStartMinute,
                selectedEndHour,
                selectedEndMinute
            });
        }
    },
    handleRecommendData: function (item) {
        const _this = this;
        let controlOne = item.data.autoControlItems[0];
        let obj = {};
        obj.recommendControl = controlOne;
        obj.recommendMinMax = [];

        if(controlOne.control_id) {
            obj.selectedControlStateOption = CONSTANT.CONTROL_OPTIONS.RECOMMEND_CONTROL_STATE_UNIT[controlOne.state];
            if((controlOne.state === 'open-partial' || controlOne.state === 'close-partial') && controlOne.time) {
                obj.selectedControlWayOption = {label: "초 단위", value: "time"};
                obj.settingValue = controlOne.time;
                obj.controlUnitName = '초';
            }else if((controlOne.state === 'open-partial' || controlOne.state === 'close-partial') && controlOne.percentage) {
                obj.selectedControlWayOption = {label: "% 단위", value: "percentage"};
                obj.settingValue = controlOne.percentage;
                obj.controlUnitName = '%';
            }
        }

        if (controlOne.period_type === delayWayOptions[2].value) {
            obj.selectedDelayWay = Object.assign({}, delayWayOptions[2]);
        } else {
            if(controlOne.delay) {
                obj.selectedDelayWay = Object.assign({}, delayWayOptions[1]);
                obj.delay = controlOne.delay / 60;
            } else {
                obj.selectedDelayWay = Object.assign({}, delayWayOptions[0]);
            }
        }
        if(controlOne.active_count !== null) {
            obj.active_count = controlOne.active_count;
            if(obj.active_count > 0) {
                obj.selectedActiveCount = {value: 'limit', label: "실행횟수 입력"};
            } else {
                obj.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
            }
        }

        obj.is_min_max_range = controlOne.is_min_max_range;
        let minMaxRanges = [];
        if(controlOne.autoControlItemMinMaxes){
            controlOne.autoControlItemMinMaxes.forEach(autoControlItemMinMax => {
                minMaxRanges.push({
                    name : autoControlItemMinMax.name,
                    percentage : autoControlItemMinMax.percentage
                });
            });
        }

        obj.recommendMinMax = minMaxRanges;

        this.setState(obj);

    },

    openTemperatureGraphModal :function () {
        this.props.openAutoControlTemperatureModal(this.state);
    },

}