import translate from "../../../constants/translate";
import {attachZero, date} from "../../../../../utils/filter";
import CONSTANT from "../../../constants/constant";
import {returnSensorOption} from "../../../utils/sensor";
import deepcopy from "../../../../../utils/deepcopy";
import moment from 'moment';

const TAB_SENSOR = "sensor";
const TAB_TIME = "time";
const TAB_MIX = "mix";
const TAB_STEP = "step";
const TAB_CONTROL = "control";
const TAB_STEP_STEP = "step_step";
const TAB_TABLE = "table";

const DATE_DISPLAY_FORMAT = "yyyy-MM-dd";

export default {
    initCommonForm :function(item){
        this.setUpdateDays(item);
        let form = {
            currentTab: item.type,
            auto_control_name: item.auto_control_name,
            step_delay: parseInt(item.step_delay / 60),
            delay: item.delay,
            recount: item.recount,
            autoControlSteps: []
        };
        if (item.date_type === "per") {
            form.selectedDayType = {value: 'per', label: "기간별"};
            form.per_date = item.per_date;
        } else if (item.date_type === "day") {
            form.selectedDayType = {value: 'day', label: "요일별"};
        }
        if (item.start_date) {
            form.start_date = moment(date(new Date(item.start_date), DATE_DISPLAY_FORMAT));
        }
        if (item.end_date) {
            form.end_date = moment(date(new Date(item.end_date), DATE_DISPLAY_FORMAT));
        }
        this.setState(form);
    },
    initDetailForm : function(item) {
        if (item.type === TAB_TABLE){
            let autoControlTables = item.autoControlTables;
            let tableControls = [];
            let tempTableControls = [];
            const commonData = autoControlTables[1][0];

            let maxTemperature = 0;
            let length = 0;

            if(commonData){
                let tableSelectedIsUseTemperature = commonData.p_band_id && commonData.p_band_temperature ? "vantilation" : "boiler";
                let tableSelectedPband = commonData.pBand && commonData.p_band_id  ? {label : commonData.pBand.p_band_name, value : commonData.pBand.id} : null;
                let tableSelectedSensor = commonData.expect_temperature_sensor_id ? {value : commonData.expectTemperatureSensor.id, label : commonData.expectTemperatureSensor.sensor_name, origin : commonData.expectTemperatureSensor }: null;

                this.setState({tableSelectedIsUseTemperature});
                this.setState({tableSelectedPband});
                this.setState({tableSelectedSensor});
            }

            Object.keys(autoControlTables).map((key, index) => {
                const keyTable = autoControlTables[key];
                let tableSelectedControls =[];

                if(keyTable && keyTable.length){
                    keyTable.forEach((item, itemIndex) => {
                        if(this.state.tableSelectedIsUseTemperature === 'vantilation'){
                            if(item.control_id){
                                tableSelectedControls.push({selectedMotorOption : {label : item.control.control_name, value : item.control.id, key: 'control'}});
                            }else if(item.wind_direction_type && item.window_position){
                                tableSelectedControls.push({selectedMotorOption :{
                                        value :{
                                            wind_direction_type : item.wind_direction_type,
                                            window_position : item.window_position
                                        },
                                        label: translate.WINDOW_POSITION[item.window_position] + ' ' + translate.WIND_DIRECTION[item.wind_direction_type],
                                        key : 'controlGroup'
                                    }
                                });
                            }
                        }else if(this.state.tableSelectedIsUseTemperature === 'boiler'){
                            tableSelectedControls.push({selectedPowerOption : {label : item.control.control_name, value : item.control.id, key: 'control'}});
                        }

                        this.setState({tableSelectedControls});
                    })


                    keyTable.forEach((item, itemIndex) => {
                        let obj = {};
                        obj.label = item.period +'주기';
                        obj.value = item.period-1;
                        length = item.period;

                        if(item.is_using_period){
                            obj.is_using_period = {label : "사용", value : true}
                        }else{
                            obj.is_using_period = {label : "미사용", value : false}
                        }

                        if(item.is_sun_date){
                            if(item.sun_date_start_type === 'rise'){
                                obj.selectedTimeType = Object.assign({}, CONSTANT.selectTableStartTimeTypes[0]);
                            }else if(item.sun_date_start_type ==='set'){
                                obj.selectedTimeType = Object.assign({}, CONSTANT.selectTableStartTimeTypes[1]);
                            }

                            if(item.sun_date_start_time >= 0){
                                obj.selectedTimeCondition = Object.assign({}, CONSTANT.selectPlusMinus[0]);
                            }else{
                                obj.selectedTimeCondition = Object.assign({}, CONSTANT.selectPlusMinus[1]);
                            }
                            let startTime = Math.abs(item.sun_date_start_time);
                            obj.selectedStartHour = {
                                label : attachZero(parseInt(startTime/60))+'시',
                                value : parseInt(startTime/60)
                            };

                            obj.selectedStartMinute = {
                                label : attachZero(startTime % 60)+'분',
                                value : startTime%60,
                            }

                        }else{
                            obj.selectedTimeType = Object.assign({}, CONSTANT.selectTableStartTimeTypes[2]);
                            obj.selectedStartHour = {
                                label : attachZero(item.start_hour)+'시',
                                value : item.start_hour
                            };

                            obj.selectedStartMinute = {
                                label : attachZero(item.start_minute)+'분',
                                value : item.start_minute,
                            };
                        }


                        if(item.p_band_id && item.p_band_temperature){
                            obj.expect_temperature = item.p_band_temperature;
                            maxTemperature += parseInt(item.p_band_temperature);
                        }else{
                            obj.expect_temperature = item.expect_temperature;
                            maxTemperature += parseInt(item.expect_temperature);
                        }

                        obj.rise_time = item.rise_time;
                        obj.drop_time = item.drop_time;

                        obj.isUseExpectTemperatureControl = item.is_expect_temperature_option;
                        obj.expect_temperature_max_value = item.expect_temperature_max_value ? item.expect_temperature_max_value : '';
                        obj.expect_temperature_min_value = item.expect_temperature_min_value ? item.expect_temperature_min_value : '';

                        if(item.autoControlExpectTemperatureSensors.length){
                            const sensors = item.autoControlExpectTemperatureSensors;
                            let tableSensors = [];
                            sensors.forEach((sensor, sensorIndex) => {
                                let tableSensor = {};
                                tableSensor.selectedAutoControlSensor = {
                                    value : sensor.sensor.id,
                                    label : sensor.sensor.sensor_name,
                                    origin : sensor.sensor
                                };

                                tableSensor.sensor_id = sensor.sensor.id;
                                tableSensor.sensorUnit = sensor.sensor.unit;
                                tableSensor.max_value = sensor.max_value;
                                tableSensor.min_value = sensor.min_value;
                                tableSensor.percentage = sensor.percentage;

                                tableSensors.push(tableSensor);
                            });

                            obj.tableSensors = tableSensors;
                        }else{
                            obj.tableSensors = [{
                                selectedAutoControlSensor : null,
                                sensor : null,
                                sensor_id : '',
                                sensorUnit : '',
                                max_value : '',
                                min_value : '',
                                percentage : '',
                            }];
                        }

                        if(itemIndex === 0) {
                            tableControls[index] = obj;
                            tempTableControls[index] = obj;
                        }

                    })

                }else{
                    let selectValue = {
                        label : (index+1)+"주기",
                        value : index
                    };
                    tableControls[index] = Object.assign(selectValue, CONSTANT.TABLE_CONTROL);
                    tempTableControls[index] = Object.assign(selectValue, CONSTANT.TABLE_CONTROL);
                }
            });


            if(this.state.tableSelectedControls.length){
                maxTemperature /= this.state.tableSelectedControls.length;
            }

            this.setState({tableAvgTemeperature : (maxTemperature / length).toFixed(2)});
            this.setState({tableControls});
            this.setState({tempTableControls});
        }else if (item.type === TAB_MIX) {
            let autoControlItems = item.autoControlItems;
            let mixControls = [];
            autoControlItems.forEach((autoControl, index) => {
                let mixControl = this.generateUpdateControlForm(autoControl, autoControl.control);
                this.setRecountAndDelay(mixControl, autoControl);
                this.setSensorOperator(mixControl, autoControl);
                this.setControlOperator(mixControl, autoControl);

                mixControl.conditional = {
                    time: false,
                    sensor: false,
                    control: false,
                };
                mixControl.mixSensors = [{
                    sensorValue: ''
                }];
                mixControl.mixControls = [{
                    controlConditionValue: ''
                }];
                if(!autoControl.control_id && autoControl.wind_direction_type !== 'none' && autoControl.window_position !== 'none'){
                    this.setState({
                        selectedControlOption : {
                            key : 'controlGroup',
                            value: {
                                wind_direction_type : autoControl.wind_direction_type,
                                window_position : autoControl.window_position,
                            },
                            label: translate.WINDOW_POSITION[autoControl.window_position] + ' ' + translate.WIND_DIRECTION[autoControl.wind_direction_type],
                            origin: {
                                type : 'motor'
                            }
                        }
                    })
                }

                mixControl.is_temperature_control_option = autoControl.is_temperature_control_option;
                mixControl.rise_time = autoControl.rise_time ? autoControl.rise_time : '';
                mixControl.drop_time = autoControl.drop_time ? autoControl.drop_time : '';
                mixControl.expect_temperature = autoControl.expect_temperature ? autoControl.expect_temperature : '';
                if(autoControl.expectTemperatureSensor){
                    mixControl.selectedTemperatureSensor = {
                        value : autoControl.expectTemperatureSensor.id,
                        label : autoControl.expectTemperatureSensor.sensor_name,
                        origin : autoControl.expectTemperatureSensor
                    }
                }else{
                    mixControl.selectedTemperatureSensor = null;
                }


                //todo delay 에 따른 셀렉트/인풋 처리
                if (autoControl.period_type === CONSTANT.delayWayOptions[2].value) {
                    mixControl.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[2]);
                    mixControl.delay = '';
                } else {
                    if(autoControl.delay) {
                        mixControl.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[1]);
                        mixControl.delay = autoControl.delay / 60;
                    } else {
                        mixControl.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[0]);
                        mixControl.delay = '';
                    }
                }

                if (autoControl.autoControlControls.length) {
                    mixControl.conditional.control = true;
                    mixControl.mixControls = [];
                    autoControl.autoControlControls.forEach(autoControlControl => {
                        let item = {
                            selectedControlCondition: {
                                value: autoControlControl.control.id,
                                label: autoControlControl.control.control_name,
                                origin: autoControlControl.control
                            }
                        };
                        if (autoControlControl.control.type === 'motor') {
                            item.controlConditionValue = autoControlControl.value;
                            switch(autoControlControl.condition) {
                                case 'over':
                                    item.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[0];
                                    break;
                                case 'excess':
                                    item.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[1];
                                    break;
                                case 'below':
                                    item.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[2];
                                    break;
                                case 'under':
                                    item.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[3];
                                    break;
                                case 'equal':
                                    item.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[4];
                                    break;
                            }
                        } else {
                            if (autoControlControl.value) {
                                item.selectedControlConditionOption = CONSTANT.selectControlConditions.power[0];
                            } else {
                                item.selectedControlConditionOption = CONSTANT.selectControlConditions.power[1];
                            }
                        }
                        mixControl.mixControls.push(item);
                    });
                }

                if (autoControl.autoControlSensors.length) {
                    //센서가 있는경우
                    mixControl.conditional.sensor = true;
                    let mixSensors = [];
                    autoControl.autoControlSensors.forEach((mSensor) => {
                        let mixSensor = {
                            selectedSensorOption: {
                                value: mSensor.sensor.id,
                                label: mSensor.sensor.sensor_name,
                                origin: mSensor
                            },
                            is_use_option : mSensor.is_use_option,
                            option_max_value : mSensor.option_max_value ? mSensor.option_max_value : '',
                            option_min_value : mSensor.option_min_value ? mSensor.option_min_value : '',
                            autoControlSensorOptions : [],
                        };

                        mSensor.autoControlSensorOptions.forEach((option, index) => {
                            mixSensor.autoControlSensorOptions.push({
                                selectedAutoControlSensor : null,
                                sensor : {
                                    value : option.sensor.id,
                                    label : option.sensor.sensor_name,
                                    origin : option.sensor
                                },
                                sensorUnit : option.sensor.unit,
                                sensor_id: option.sensor_id,
                                max_value : option.max_value,
                                min_value : option.min_value,
                                percentage : option.percentage,
                            });
                        });

                        mixSensor.sensorValue = mSensor.value;
                        mixSensor.selectedRangeConditionOption = returnSensorOption(mSensor.sensor.type, mSensor);
                        mixSensors.push(mixSensor);
                    });
                    mixControl.mixSensors = mixSensors;
                }else{
                    mixControl.mixSensors = [{
                        selectedAutoControlSensor : null,
                        sensor : null,
                        sensor_id : '',
                        sensorUnit : '',
                        max_value : '',
                        min_value : '',
                        percentage : '',
                    }]
                }

                if (autoControl.is_sun_date) {
                    mixControl.conditional.time = true;
                    mixControl.selectedTimeWay = Object.assign({}, CONSTANT.selectTimeWay[3]);
                    if (autoControl.sun_date_start_type === 'rise') {
                        mixControl.selectedSunDateStartType = Object.assign({}, CONSTANT.selectSunDateTypes[0]);
                    } else {
                        mixControl.selectedSunDateStartType = Object.assign({}, CONSTANT.selectSunDateTypes[1]);
                    }
                    if (autoControl.sun_date_start_time >= 0) {
                        mixControl.selectedSunDateStartCondition = Object.assign({}, CONSTANT.selectPlusMinus[0]);
                    } else {
                        mixControl.selectedSunDateStartCondition = Object.assign({}, CONSTANT.selectPlusMinus[1]);
                    }
                    if (autoControl.sun_date_end_type === 'rise') {
                        mixControl.selectedSunDateEndType = Object.assign({}, CONSTANT.selectSunDateTypes[0]);
                    } else {
                        mixControl.selectedSunDateEndType = Object.assign({}, CONSTANT.selectSunDateTypes[1]);
                    }
                    if (autoControl.sun_date_end_time >= 0) {
                        mixControl.selectedSunDateEndCondition = Object.assign({}, CONSTANT.selectPlusMinus[0]);
                    } else {
                        mixControl.selectedSunDateEndCondition = Object.assign({}, CONSTANT.selectPlusMinus[1]);
                    }
                    let startTime = Math.abs(autoControl.sun_date_start_time);
                    mixControl.selectedSunDateStartHour = parseInt(startTime / 60);
                    mixControl.selectedSunDateStartMinute = startTime % 60;
                    let endTime = Math.abs(autoControl.sun_date_end_time);
                    mixControl.selectedSunDateEndHour = parseInt(endTime / 60);
                    mixControl.selectedSunDateEndMinute = endTime % 60;
                } else {
                    if (autoControl.start_hour === null && autoControl.start_minute === null &&
                        autoControl.end_hour === null && autoControl.end_minute === null) {
                        if (autoControl.autoControlSensors.length === 0 && autoControl.autoControlControls.length === 0) {
                            //시간인데 시간선택안함인경우
                            mixControl.conditional.time = true;
                            mixControl.selectedTimeWay = {value: "noSelect", label: "선택 안함"};
                        }else{
                            if(autoControl.active_count > 0) {
                                mixControl.selectedActiveCount = {value: 'limit', label: "실행횟수 입력"};
                                mixControl.active_count = autoControl.active_count;
                            }else{
                                mixControl.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                            }
                        }
                    } else {
                        mixControl.conditional.time = true;
                        mixControl.selectedStartHour = {
                            value: autoControl.start_hour,
                            label: `${attachZero(autoControl.start_hour)}시`
                        };
                        mixControl.selectedStartMinute = {
                            value: autoControl.start_minute,
                            label: `${attachZero(autoControl.start_minute)}분`
                        };
                        if (autoControl.end_hour === null && autoControl.end_minute === null) {
                            //특정시간 인경우
                            mixControl.selectedTimeWay = {value: "single", label: "특정 시간"};
                            mixControl.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                            mixControl.active_count = 0;
                        } else {
                            //시간범위 인경우
                            mixControl.selectedTimeWay = {value: "range", label: "시간 범위"};
                            mixControl.selectedEndHour = {
                                value: autoControl.end_hour,
                                label: `${attachZero(autoControl.end_hour)}시`
                            };
                            mixControl.selectedEndMinute = {
                                value: autoControl.end_minute,
                                label: `${attachZero(autoControl.end_minute)}분`
                            };
                        }
                    }
                }

                mixControl.is_min_max_range = autoControl.is_min_max_range;
                let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
                autoControl.autoControlItemMinMaxes.forEach(autoControlItemMinMax => {
                    for (let i=0; i<minMaxRanges.length; i++) {
                        if (minMaxRanges[i].id === autoControlItemMinMax.control_min_max_range_id) {
                            minMaxRanges[i].percentage = autoControlItemMinMax.percentage;
                            minMaxRanges[i].useState = autoControlItemMinMax.state;
                        }
                    }
                });
                for (let i=0; i<minMaxRanges.length; i++) {
                    if (!minMaxRanges[i].useState) {
                        minMaxRanges[i].useState = 'off';
                        minMaxRanges[i].percentage = '';
                    }
                }
                mixControl.minMaxRanges = minMaxRanges;

                mixControls.push(mixControl);
            });
            this.setState({mixControls});
        }

        else if (item.type === TAB_STEP) {
            let temp = {};
            let autoControlItems = item.autoControlItems;
            let stepControls = [];

            if(item.autoControlSteps && item.autoControlSteps.length) {
                let autoControlSteps = [];

                item.autoControlSteps.forEach((autoControlStep, index) => {
                    let stepControl = this.generateUpdateControlForm(autoControlStep, autoControlStep.control);
                    stepControl.recount = autoControlStep.recount !== null ? autoControlStep.recount+1 : '';
                    stepControl.delay = autoControlStep.delay !== null ? autoControlStep.delay / 60 : '';
                    stepControl.is_min_max_range = autoControlStep.is_min_max_range;

                    stepControl.is_temperature_control_option = autoControlStep.is_temperature_control_option;
                    stepControl.rise_time = autoControlStep.rise_time ? autoControlStep.rise_time : '';
                    stepControl.drop_time = autoControlStep.drop_time ? autoControlStep.drop_time : '';
                    stepControl.expect_temperature = autoControlStep.expect_temperature ? autoControlStep.expect_temperature : '';

                    let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
                    autoControlStep.autoControlStepMinMaxes.forEach(autoControlStepMinMax => {
                        for (let i=0; i<minMaxRanges.length; i++) {
                            if (minMaxRanges[i].id === autoControlStepMinMax.control_min_max_range_id) {
                                minMaxRanges[i].percentage = autoControlStepMinMax.percentage;
                                minMaxRanges[i].useState = autoControlStepMinMax.state;
                            }
                        }
                    });
                    for (let i=0; i<minMaxRanges.length; i++) {
                        if (!minMaxRanges[i].useState) {
                            minMaxRanges[i].useState = 'off';
                            minMaxRanges[i].percentage = '';
                        }
                    }
                    stepControl.minMaxRanges = minMaxRanges;

                    autoControlSteps.push(stepControl);
                });

                temp.autoControlSteps = autoControlSteps;
            }

            autoControlItems.forEach((autoControl, index) => {
                let stepControl = this.generateUpdateControlForm(autoControl, autoControl.control);
                this.setRecountAndDelay(stepControl, autoControl);
                this.setSensorOperator(stepControl, autoControl);
                this.setControlOperator(stepControl, autoControl);
                stepControl.conditional = {
                    time: false,
                    sensor: false,
                    control: false,
                };
                stepControl.stepSensors = [{
                    sensorValue: ''
                }];
                stepControl.stepControls = [{
                    controlConditionValue: ''
                }];

                if(autoControl.expectTemperatureSensor){
                    stepControl.selectedTemperatureSensor = {
                        value : autoControl.expectTemperatureSensor.id,
                        label : autoControl.expectTemperatureSensor.sensor_name,
                        origin : autoControl.expectTemperatureSensor
                    }
                }

                if(!autoControl.control_id && autoControl.wind_direction_type !== 'none' && autoControl.window_position !== 'none'){
                    stepControl.selectedControlOption = {
                        key : 'controlGroup',
                        value: {
                            wind_direction_type : autoControl.wind_direction_type,
                            window_position : autoControl.window_position,
                        },
                        label: translate.WINDOW_POSITION[autoControl.window_position] + ' ' + translate.WIND_DIRECTION[autoControl.wind_direction_type],
                        origin: {
                            type : 'motor'
                        }
                    }
                }

                stepControl.delay = autoControl.delay !== null ? autoControl.delay / 60 : '';
                stepControl.recount = autoControl.recount !== null ? autoControl.recount + 1 : '';
                temp.setCount = autoControl.active_count !== null ? autoControl.active_count : 0;

                stepControl.is_temperature_control_option = autoControl.is_temperature_control_option;
                stepControl.rise_time = autoControl.rise_time ? autoControl.rise_time : '';
                stepControl.drop_time = autoControl.drop_time ? autoControl.drop_time : '';
                stepControl.expect_temperature = autoControl.expect_temperature ? autoControl.expect_temperature : '';

                if (autoControl.autoControlControls.length) {
                    stepControl.conditional.control = true;
                    stepControl.stepControls = [];
                    autoControl.autoControlControls.forEach(autoControlControl => {

                        let item = {
                            selectedControlCondition: {
                                value: autoControlControl.control.id,
                                label: autoControlControl.control.control_name,
                                origin: autoControlControl.control
                            }
                        };
                        if (autoControlControl.control.type === 'motor') {
                            item.controlConditionValue = autoControlControl.value;
                            switch(autoControlControl.condition) {
                                case 'over':
                                    item.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[0];
                                    break;
                                case 'excess':
                                    item.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[1];
                                    break;
                                case 'below':
                                    item.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[2];
                                    break;
                                case 'under':
                                    item.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[3];
                                    break;
                                case 'equal':
                                    item.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[4];
                                    break;
                            }
                        } else {
                            if (autoControlControl.value) {
                                item.selectedControlConditionOption = CONSTANT.selectControlConditions.power[0];
                            } else {
                                item.selectedControlConditionOption = CONSTANT.selectControlConditions.power[1];
                            }
                        }
                        stepControl.stepControls.push(item);
                    });
                }

                if (autoControl.autoControlSensors.length) {
                    //센서가 있는경우
                    stepControl.conditional.sensor = true;
                    let stepSensors = [];
                    autoControl.autoControlSensors.forEach((sSensor) => {
                        let stepSensor = {
                            selectedSensorOption: {
                                value: sSensor.sensor.id,
                                label: sSensor.sensor.sensor_name,
                                origin: sSensor
                            },
                            is_use_option : sSensor.is_use_option,
                            option_max_value : sSensor.option_max_value ? sSensor.option_max_value : '',
                            option_min_value : sSensor.option_min_value ? sSensor.option_min_value : '',
                            autoControlSensorOptions : [],
                        };

                        sSensor.autoControlSensorOptions.forEach((option, index) => {
                            stepSensor.autoControlSensorOptions.push({
                                selectedAutoControlSensor : null,
                                sensor : {
                                    value : option.sensor.id,
                                    label : option.sensor.sensor_name,
                                    origin : option.sensor
                                },
                                sensorUnit : option.sensor.unit,
                                sensor_id: option.sensor_id,
                                max_value : option.max_value,
                                min_value : option.min_value,
                                percentage : option.percentage,
                            });
                        });
                        stepSensor.sensorValue = sSensor.value;
                        stepSensor.selectedRangeConditionOption = returnSensorOption(sSensor.sensor.type, sSensor);
                        stepSensors.push(stepSensor);
                    });
                    stepControl.stepSensors = stepSensors;
                }else{
                    stepControl.stepSensors = [{
                        selectedAutoControlSensor : null,
                        sensor : null,
                        sensor_id : '',
                        sensorUnit : '',
                        max_value : '',
                        min_value : '',
                        percentage : '',
                    }]
                }

                if(autoControl.active_count > 0) {
                    temp.selectedActiveCount = {value: 'limit', label: "실행횟수 입력"};
                } else {
                    temp.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                }
                if (autoControl.is_sun_date) {
                    stepControl.conditional.time = true;
                    stepControl.selectedTimeWay = Object.assign({}, CONSTANT.selectTimeWay[3]);
                    if (stepControl.sun_date_start_type === 'rise') {
                        stepControl.selectedSunDateStartType = Object.assign({}, CONSTANT.selectSunDateTypes[0]);
                    } else {
                        stepControl.selectedSunDateStartType = Object.assign({}, CONSTANT.selectSunDateTypes[1]);
                    }
                    if (stepControl.sun_date_start_time >= 0) {
                        stepControl.selectedSunDateStartCondition = Object.assign({}, CONSTANT.selectPlusMinus[0]);
                    } else {
                        stepControl.selectedSunDateStartCondition = Object.assign({}, CONSTANT.selectPlusMinus[1]);
                    }
                    if (stepControl.sun_date_end_type === 'rise') {
                        stepControl.selectedSunDateEndType = Object.assign({}, CONSTANT.selectSunDateTypes[0]);
                    } else {
                        stepControl.selectedSunDateEndType = Object.assign({}, CONSTANT.selectSunDateTypes[1]);
                    }
                    if (stepControl.sun_date_end_time >= 0) {
                        stepControl.selectedSunDateEndCondition = Object.assign({}, CONSTANT.selectPlusMinus[0]);
                    } else {
                        stepControl.selectedSunDateEndCondition = Object.assign({}, CONSTANT.selectPlusMinus[1]);
                    }
                    let startTime = Math.abs(autoControl.sun_date_start_time);
                    stepControl.selectedSunDateStartHour = parseInt(startTime / 60);
                    stepControl.selectedSunDateStartMinute = startTime % 60;
                    let endTime = Math.abs(autoControl.sun_date_end_time);
                    stepControl.selectedSunDateEndHour = parseInt(endTime / 60);
                    stepControl.selectedSunDateEndMinute = endTime % 60;
                } else {
                    if (autoControl.start_hour === null && autoControl.start_minute === null &&
                        autoControl.end_hour === null && autoControl.end_minute === null) {
                        if (autoControl.autoControlSensors.length === 0 && autoControl.autoControlControls.length === 0) {
                            //시간인데 시간선택안함인경우
                            stepControl.conditional.time = true;
                            stepControl.selectedTimeWay = Object.assign({}, CONSTANT.selectTimeWay[0]);
                        }
                    } else {
                        stepControl.conditional.time = true;
                        stepControl.selectedStartHour = {
                            value: autoControl.start_hour,
                            label: `${attachZero(autoControl.start_hour)}시`
                        };
                        stepControl.selectedStartMinute = {
                            value: autoControl.start_minute,
                            label: `${attachZero(autoControl.start_minute)}분`
                        };
                        if (autoControl.end_hour === null && autoControl.end_minute === null) {
                            //특정시간 인경우
                            stepControl.selectedTimeWay = {value: "single", label: "특정 시간"};
                        } else {
                            //시간범위 인경우
                            stepControl.selectedTimeWay = {value: "range", label: "시간 범위"};
                            stepControl.selectedEndHour = {
                                value: autoControl.end_hour,
                                label: `${attachZero(autoControl.end_hour)}시`
                            };
                            stepControl.selectedEndMinute = {
                                value: autoControl.end_minute,
                                label: `${attachZero(autoControl.end_minute)}분`
                            };
                        }
                    }
                }

                stepControl.is_min_max_range = autoControl.is_min_max_range;
                let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
                autoControl.autoControlItemMinMaxes.forEach(autoControlItemMinMax => {
                    for (let i=0; i<minMaxRanges.length; i++) {
                        if (minMaxRanges[i].id === autoControlItemMinMax.control_min_max_range_id) {
                            minMaxRanges[i].percentage = autoControlItemMinMax.percentage;
                            minMaxRanges[i].useState = autoControlItemMinMax.state;
                        }
                    }
                });
                for (let i=0; i<minMaxRanges.length; i++) {
                    if (!minMaxRanges[i].useState) {
                        minMaxRanges[i].useState = 'off';
                        minMaxRanges[i].percentage = '';
                    }
                }
                stepControl.minMaxRanges = minMaxRanges;

                stepControls.push(stepControl);
            });
            temp.stepControls = stepControls;
            this.setState(temp);
        }

        else if (item.type === TAB_SENSOR) {
            let form = this.generateUpdateControlForm(item.autoControlItems[0], item.autoControlItems[0].control);
            this.setRecountAndDelay(form, item.autoControlItems[0]);
            this.setSensorOperator(form, item.autoControlItems[0]);
            this.setControlOperator(form, item.autoControlItems[0]);
            const autoControlItem = item.autoControlItems[0];
            const sensor = autoControlItem.autoControlSensors[0];
            sensor.sensor.sensor = deepcopy(sensor.sensor);
            form.autoControlSensorOptions = [];


            const sensorOption = sensor.autoControlSensorOptions;

            form.option_min_value = sensor.option_min_value ? sensor.option_min_value : '';
            form.option_max_value = sensor.option_max_value ? sensor.option_max_value : '';
            form.is_use_option = sensor.is_use_option;
            form.autoControlSensorOptions = [];

            form.is_temperature_control_option = autoControlItem.is_temperature_control_option;
            form.rise_time = autoControlItem.rise_time ? autoControlItem.rise_time : '';
            form.drop_time = autoControlItem.drop_time ? autoControlItem.drop_time : '';
            form.expect_temperature = autoControlItem.expect_temperature ? autoControlItem.expect_temperature : '';

            if(autoControlItem.expectTemperatureSensor){
                form.selectedTemperatureSensor = {
                    value : autoControlItem.expectTemperatureSensor.id,
                    label : autoControlItem.expectTemperatureSensor.sensor_name,
                    origin : autoControlItem.expectTemperatureSensor
                }
            }else{
                form.selectedTemperatureSensor = null;
            }

            if(sensorOption && sensorOption.length) {
                sensorOption.forEach((data, index) => {
                    form.autoControlSensorOptions.push({
                        id : data.id,
                        max_value : data.max_value,
                        min_value : data.min_value,
                        percentage : data.percentage,
                        sensor_id : data.sensor_id,
                        sensorUnit : data.sensor.unit,
                        sensor : {
                            value : data.sensor.id,
                            label : data.sensor.sensor_name,
                            origin : data.sensor
                        }
                    });
                });
            }else{
                form.autoControlSensorOptions = [{
                    selectedAutoControlSensor : null,
                    sensor : null,
                    sensor_id : '',
                    sensorUnit : '',
                    max_value : '',
                    min_value : '',
                    percentage : '',
                }]
            }

            form.sensorValue = sensor.value;
            form.selectedSensorOption = {
                value : sensor.sensor.id,
                label : sensor.sensor.sensor_name,
                origin : sensor.sensor
            };

            if(!autoControlItem.control_id && autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none'){
                form.selectedControlOption = {
                    key : 'controlGroup',
                    value: {
                        wind_direction_type : autoControlItem.wind_direction_type,
                        window_position : autoControlItem.window_position,
                    },
                    label: translate.WINDOW_POSITION[autoControlItem.window_position] + ' ' + translate.WIND_DIRECTION[autoControlItem.wind_direction_type],
                    origin: {
                        type : 'motor'
                    }
                }
            }

            form.selectedRangeConditionOption = returnSensorOption(sensor.sensor.type, sensor);

            if (autoControlItem.period_type === CONSTANT.delayWayOptions[2].value) {
                form.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[2]);
            } else {
                if(autoControlItem.delay) {
                    form.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[1]);
                    form.delay = autoControlItem.delay / 60;
                } else {
                    form.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[0]);
                }
            }
            if(autoControlItem.active_count !== null) {
                form.active_count = autoControlItem.active_count;
                if(form.active_count > 0) {
                    form.selectedActiveCount = {value: 'limit', label: "실행횟수 입력"};
                } else {
                    form.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                }
            }

            form.is_min_max_range = autoControlItem.is_min_max_range;
            let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
            autoControlItem.autoControlItemMinMaxes.forEach(autoControlItemMinMax => {
                for (let i=0; i<minMaxRanges.length; i++) {
                    if (minMaxRanges[i].id === autoControlItemMinMax.control_min_max_range_id) {
                        minMaxRanges[i].percentage = autoControlItemMinMax.percentage;
                        minMaxRanges[i].useState = autoControlItemMinMax.state;
                    }
                }
            });
            for (let i=0; i<minMaxRanges.length; i++) {
                if (!minMaxRanges[i].useState) {
                    minMaxRanges[i].useState = 'off';
                    minMaxRanges[i].percentage = '';
                }
            }
            form.minMaxRanges = minMaxRanges;

            this.setState(form);
        }

        else if (item.type === TAB_CONTROL) {
            let form = this.generateUpdateControlForm(item.autoControlItems[0], item.autoControlItems[0].control);
            this.setRecountAndDelay(form, item.autoControlItems[0]);
            this.setSensorOperator(form, item.autoControlItems[0]);
            this.setControlOperator(form, item.autoControlItems[0]);
            const autoControlItem = item.autoControlItems[0];
            const autoControlControl = autoControlItem.autoControlControls[0];

            form.selectedControlCondition = {
                value: autoControlControl.control.id,
                label: autoControlControl.control.control_name,
                origin: autoControlControl.control
            };

            form.is_temperature_control_option = autoControlItem.is_temperature_control_option;
            form.rise_time = autoControlItem.rise_time ? autoControlItem.rise_time : '';
            form.drop_time = autoControlItem.drop_time ? autoControlItem.drop_time : '';
            form.expect_temperature = autoControlItem.expect_temperature ? autoControlItem.expect_temperature : '';

            if(autoControlItem.expectTemperatureSensor){
                form.selectedTemperatureSensor = {
                    value : autoControlItem.expectTemperatureSensor.id,
                    label : autoControlItem.expectTemperatureSensor.sensor_name,
                    origin : autoControlItem.expectTemperatureSensor
                }
            }else{
                form.selectedTemperatureSensor = null;
            }

            if(!autoControlItem.control_id && autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none'){
                form.selectedControlOption = {
                    key : 'controlGroup',
                    value: {
                        wind_direction_type : autoControlItem.wind_direction_type,
                        window_position : autoControlItem.window_position,
                    },
                    label: translate.WINDOW_POSITION[autoControlItem.window_position] + ' ' + translate.WIND_DIRECTION[autoControlItem.wind_direction_type],
                    origin: {type : 'motor'}
                }
            }

            if (autoControlControl.control.type === 'motor') {
                form.controlConditionValue = autoControlControl.value;
                switch(autoControlControl.condition) {
                    case 'over':
                        form.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[0];
                        break;
                    case 'excess':
                        form.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[1];
                        break;
                    case 'below':
                        form.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[2];
                        break;
                    case 'under':
                        form.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[3];
                        break;
                    case 'equal':
                        form.selectedControlConditionOption = CONSTANT.selectControlConditions.motor[4];
                        break;
                }
            } else {
                if (autoControlControl.value) {
                    form.selectedControlConditionOption = CONSTANT.selectControlConditions.power[0];
                } else {
                    form.selectedControlConditionOption = CONSTANT.selectControlConditions.power[1];
                }
            }

            if (autoControlItem.period_type === CONSTANT.delayWayOptions[2].value) {
                form.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[2]);
            } else {
                if(autoControlItem.delay) {
                    form.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[1]);
                    form.delay = autoControlItem.delay / 60;
                } else {
                    form.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[0]);
                }
            }
            if(autoControlItem.active_count !== null) {
                form.active_count = autoControlItem.active_count;
                if(form.active_count > 0) {
                    form.selectedActiveCount = {value: 'limit', label: "실행횟수 입력"};
                } else {
                    form.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                }
            }

            form.is_min_max_range = autoControlItem.is_min_max_range;
            let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
            autoControlItem.autoControlItemMinMaxes.forEach(autoControlItemMinMax => {
                for (let i=0; i<minMaxRanges.length; i++) {
                    if (minMaxRanges[i].id === autoControlItemMinMax.control_min_max_range_id) {
                        minMaxRanges[i].percentage = autoControlItemMinMax.percentage;
                        minMaxRanges[i].useState = autoControlItemMinMax.state;
                    }
                }
            });
            for (let i=0; i<minMaxRanges.length; i++) {
                if (!minMaxRanges[i].useState) {
                    minMaxRanges[i].useState = 'off';
                    minMaxRanges[i].percentage = '';
                }
            }
            form.minMaxRanges = minMaxRanges;

            this.setState(form);
        }

        else if (item.type === TAB_TIME) {
            let form = this.generateUpdateControlForm(item.autoControlItems[0], item.autoControlItems[0].control);
            this.setRecountAndDelay(form, item.autoControlItems[0]);
            const autoControlItem = item.autoControlItems[0];
            let selectedTimeWay = {};

            if(!autoControlItem.control_id && autoControlItem.wind_direction_type !== 'none' && autoControlItem.window_position !== 'none'){
                form.selectedControlOption = {
                    key : 'controlGroup',
                    value: {
                        wind_direction_type : autoControlItem.wind_direction_type,
                        window_position : autoControlItem.window_position,
                    },
                    label: translate.WINDOW_POSITION[autoControlItem.window_position] + ' ' + translate.WIND_DIRECTION[autoControlItem.wind_direction_type],
                    origin: {type : 'motor'}
                }
            }

            form.is_temperature_control_option = autoControlItem.is_temperature_control_option;
            form.rise_time = autoControlItem.rise_time ? autoControlItem.rise_time : '';
            form.drop_time = autoControlItem.drop_time ? autoControlItem.drop_time : '';
            form.expect_temperature = autoControlItem.expect_temperature ? autoControlItem.expect_temperature : '';

            if(autoControlItem.expectTemperatureSensor){
                form.selectedTemperatureSensor = {
                    value : autoControlItem.expectTemperatureSensor.id,
                    label : autoControlItem.expectTemperatureSensor.sensor_name,
                    origin : autoControlItem.expectTemperatureSensor
                }
            }else{
                form.selectedTemperatureSensor = null;
            }

            if (autoControlItem.is_sun_date) {
                selectedTimeWay = Object.assign({}, CONSTANT.selectTimeWay[3]);
                if (autoControlItem.sun_date_start_type === 'rise') {
                    form.selectedSunDateStartType = Object.assign({}, CONSTANT.selectSunDateTypes[0]);
                } else {
                    form.selectedSunDateStartType = Object.assign({}, CONSTANT.selectSunDateTypes[1]);
                }
                if (autoControlItem.sun_date_start_time >= 0) {
                    form.selectedSunDateStartCondition = Object.assign({}, CONSTANT.selectPlusMinus[0]);
                } else {
                    form.selectedSunDateStartCondition = Object.assign({}, CONSTANT.selectPlusMinus[1]);
                }
                if (autoControlItem.sun_date_end_type === 'rise') {
                    form.selectedSunDateEndType = Object.assign({}, CONSTANT.selectSunDateTypes[0]);
                } else {
                    form.selectedSunDateEndType = Object.assign({}, CONSTANT.selectSunDateTypes[1]);
                }
                if (autoControlItem.sun_date_end_time >= 0) {
                    form.selectedSunDateEndCondition = Object.assign({}, CONSTANT.selectPlusMinus[0]);
                } else {
                    form.selectedSunDateEndCondition = Object.assign({}, CONSTANT.selectPlusMinus[1]);
                }
                let startTime = Math.abs(autoControlItem.sun_date_start_time);
                form.selectedSunDateStartHour = parseInt(startTime / 60);
                form.selectedSunDateStartMinute = startTime % 60;
                let endTime = Math.abs(autoControlItem.sun_date_end_time);
                form.selectedSunDateEndHour = parseInt(endTime / 60);
                form.selectedSunDateEndMinute = endTime % 60;
            } else {
                if (autoControlItem.start_hour === null && autoControlItem.start_minute === null) {
                    selectedTimeWay = {value: "noSelect", label: "선택 안함"};
                } else {
                    form.selectedStartHour = {
                        value: autoControlItem.start_hour,
                        label: `${attachZero(autoControlItem.start_hour)}시`
                    };
                    form.selectedStartMinute = {
                        value: autoControlItem.start_minute,
                        label: `${attachZero(autoControlItem.start_minute)}분`
                    };
                    if (autoControlItem.end_hour === null && autoControlItem.end_minute === null) {
                        selectedTimeWay = {value: "single", label: "특정 시간"};
                    } else {
                        selectedTimeWay = {value: "range", label: "시간 범위"};
                        form.selectedEndHour = {
                            value: autoControlItem.end_hour,
                            label: `${attachZero(autoControlItem.end_hour)}시`
                        };
                        form.selectedEndMinute = {
                            value: autoControlItem.end_minute,
                            label: `${attachZero(autoControlItem.end_minute)}분`
                        };
                    }
                }
            }

            if (autoControlItem.period_type === CONSTANT.delayWayOptions[2].value) {
                form.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[2]);
            } else {
                if(autoControlItem.delay) {
                    form.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[1]);
                    form.delay = autoControlItem.delay / 60;
                } else {
                    form.selectedDelayWay = Object.assign({}, CONSTANT.delayWayOptions[0]);
                }
            }

            if(autoControlItem.active_count !== null) {
                form.active_count = autoControlItem.active_count;
                if(form.active_count > 0) {
                    form.selectedActiveCount = {value: 'limit', label: "실행횟수 입력"};
                } else {
                    form.selectedActiveCount = {value: 'infinite', label: "계속 실행"};
                }
            }
            form.selectedTimeWay = selectedTimeWay;

            form.is_min_max_range = autoControlItem.is_min_max_range;
            let minMaxRanges = deepcopy(this.state.controlMinMaxRanges);
            autoControlItem.autoControlItemMinMaxes.forEach(autoControlItemMinMax => {
                for (let i=0; i<minMaxRanges.length; i++) {
                    if (minMaxRanges[i].id === autoControlItemMinMax.control_min_max_range_id) {
                        minMaxRanges[i].percentage = autoControlItemMinMax.percentage;
                        minMaxRanges[i].useState = autoControlItemMinMax.state;
                    }
                }
            });
            for (let i=0; i<minMaxRanges.length; i++) {
                if (!minMaxRanges[i].useState) {
                    minMaxRanges[i].useState = 'off';
                    minMaxRanges[i].percentage = '';
                }
            }
            form.minMaxRanges = minMaxRanges;

            this.setState(form);
        }
    },

}