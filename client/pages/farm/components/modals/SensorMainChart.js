import React, {Component} from 'react';
import {useSensorMainChart} from "../../contexts/modals/sensorMainChart";
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import Select from 'react-select';
import {colourStyles} from "../../utils/select";
import sensorManager from '../../managers/sensor';
import sensorMainChartManager from '../../managers/sensorMainChart';
import {noSession} from "../../utils/session";

class ModalSensorMainChart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sensorMainChart: null,
            selectSensorOption: [],
            selectedSensorOption: null
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        this.findMainSensorChart();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findAllInnerSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            position: "in"
        };
        sensorManager.findAll(query, (status, data) => {
            if(status === 200) {
                let selectSensorOption = _this.generateSensorOption(data.data.rows);
                _this.setState({selectSensorOption});
            } else if (status === 404) {
                _this.props.showDialog({
                    alertText: "설정가능한 센서가 없습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        });
    };

    generateSensorOption = (sensors) => {
        let selectSensorOption = [];
        sensors.forEach(item => {
            let obj = {
                value: item.id,
                label: item.sensor_name,
                origin: item
            };
            if(this.state.sensorMainChart) {
                if(this.state.sensorMainChart.id === item.id) {
                    this.setState({
                        selectedSensorOption: obj
                    });
                }
            }
            selectSensorOption.push(obj);
        });
        return selectSensorOption;
    };

    handleSensorOption = (selectedSensorOption) => {
        this.setState({selectedSensorOption});
    };

    findMainSensorChart = (e) => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId()
        };
        sensorMainChartManager.findMainSensorChart(query, (status, data) => {
            if(status === 200) {
                _this.setState({
                    sensorMainChart: data.data
                }, () => {
                    _this.findAllInnerSensors();
                });
            } else if (status === 404) {
                _this.setState({
                    sensorMainChart: null
                }, () => {
                    _this.findAllInnerSensors();
                });
            } else {
                _this.props.alertError(status, data);
            }
        });
    };

    setSensorMainChart = (e) => {
        const _this = this;
        if(!this.state.selectedSensorOption) {
            return this.props.showDialog({
                alertText: "대표 센서를 선택해주세요."
            });
        }
        let query = {
            house_id: this.props.getHouseId(),
            sensor_id: this.state.selectedSensorOption.value
        };

        sensorMainChartManager.setMainSensorChart(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.props.showDialog({
                    alertText: "메인차트 센서가 설정되었습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {
        return (
            <article id="lcModalSensorMainChartWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalSensorMainChart" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">차트 표시 센서 선택</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div className="select-module-wrap">
                                        <Select value={this.state.selectedSensorOption}
                                                placeholder="전체보기"
                                                onChange={this.handleSensorOption}
                                                isSearchable={ false }
                                                options={this.state.selectSensorOption}
                                                styles={colourStyles}/>
                                    </div>

                                    <button className="theme-button" onClick={e => this.setSensorMainChart(e)}>등록</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useSensorMainChart(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSensorMainChart)));