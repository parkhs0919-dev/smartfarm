import React, {Component} from 'react';
import {useAutoControlFileUpload} from "../../contexts/modals/autoControlFileUpload";
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import deepcopy from "../../../../utils/deepcopy";
import {noSession} from "../../utils/session";
import CONSTANT from '../../constants/constant';
const FILE_TYPES = CONSTANT.fileTypes;
import autoControlManager from '../../managers/autoControl';

class ModalAutoControlFileUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uploadedFile: null
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.props.callback();
        this.setState({
            uploadedFile: null
        });
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    fileUpload = (e) => {
        const file = e.target.files[0];
        if(!file) {
            return this.setState({
                uploadedFile: null
            });
        }
        if(file.type !== FILE_TYPES.JSON) {
            return this.props.showDialog({
                alertText: "JSON파일만 업로드 가능합니다."
            });
        }
        this.setState({
            uploadedFile: file
        });
    };

    uploadAutoControlFile = () => {
        const _this = this;
        if(!this.state.uploadedFile) {
            return this.props.showDialog({
                alertText: "JSON파일을 업로드해주세요."
            });
        }

        let form = new FormData();
        form.append('file', this.state.uploadedFile);

        autoControlManager.uploadJSON(this.props.getHouseId(), form, (status, data) => {
            if(status === 200) {
                _this.props.showDialog({
                    alertText: "등록 되었습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        });
    };

    render() {
        return (
            <article id="lcModalAutoControlFileUploadWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalAutoControlFileUpload" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">자동제어 일괄등록</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <label className="title-label">JSON 업로드</label>
                                    <div className="input-file-upload-wrap">
                                        <input id="autoControlFileUpload" type="file" onChange={this.fileUpload}/>
                                        <label htmlFor="autoControlFileUpload" className="input-shape-label lc-farm-input">{this.state.uploadedFile ? this.state.uploadedFile.name : ''}</label>
                                        <label htmlFor="autoControlFileUpload" className="button-shape-label">업로드</label>
                                    </div>
                                    <button className="theme-button" onClick={this.uploadAutoControlFile}>등록</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId,
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useAutoControlFileUpload(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalAutoControlFileUpload)));
