import React, {Component} from 'react';
import {useIndividualControl} from "../../contexts/modals/individualControl";
import CONSTANT from "../../constants/constant";
import Select from 'react-select';
import {useAlert} from "../../contexts/alert";
import {colourStyles} from "../../../farm/utils/select";

import controlManager from '../../managers/control';
import Spinner from "../Spinner";
import {noSession} from "../../utils/session";

const TYPE_MOTOR = "motor";
const TYPE_POWER = "power";
const DEFAULT_INDIVIDUAL_CONTROL_STATE = CONSTANT.defaultIndividualControlState;
const CONTROL_OPTIONS = CONSTANT.CONTROL_OPTIONS;
// const STATE_OPTION = CONTROL_OPTIONS.STATE;
const STATE_OPTION = CONTROL_OPTIONS.INDIVIDUAL_STATE;
const UNIT_OPTION = CONTROL_OPTIONS.UNIT;
const VALUE_OPTION = CONTROL_OPTIONS.VALUE;

class ModalIndividualControl extends Component {
    constructor(props) {
        super(props);
        this.state = Object.assign({}, DEFAULT_INDIVIDUAL_CONTROL_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (control) => {
        this.setState({control});
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        let initial = Object.assign({}, DEFAULT_INDIVIDUAL_CONTROL_STATE);
        initial.form.state = '';
        initial.form.unit = '';
        initial.form.value = '';
        this.setState(initial);
        this.props.close();
    };
    returnPowerTimeByState = (state, time) => {
        if(state === "hour") {
            return time * 60 * 60;
        } else if (state === "min") {
            return time * 60;
        }
        return time;
    };

    submit = (e) => {
        if(e) e.preventDefault();
        const _this = this;
        // this.loading(true);
        let temp = Object.assign({}, this.state.form);
        let form = {
            id: this.state.control.id,
        };

        if(!this.state.state) {
            return this.props.showDialog({
                alertText: "동작을 선택해주세요."
            });
        }

        if(this.state.state.hasValue) {
            if(!this.state.unit) {
                return this.props.showDialog({
                    alertText: "단위를 선택해주세요."
                });
            }
            if(!temp.value) {
                return this.props.showDialog({
                    alertText: "단위값을 입력해주세요."
                });
            }
            //부분열기, 부분닫기, 예약켜짐, 예약꺼짐
            form.state = this.state.state.value.split('-partial')[0];

            if(this.state.control.type === TYPE_MOTOR) {
                form[this.state.unit.value] = temp.value;
            } else if (this.state.control.type === TYPE_POWER) {
                form.time = this.returnPowerTimeByState(this.state.unit.value, temp.value);
            }
        } else {
            //열기, 닫기, 켜짐, 꺼짐
            form.state = this.state.state.value;
            if(this.state.control.type === "motor") {
                form.percentage = "200";
            }
        }

        controlManager.active(form, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.props.showDialog({
                    alertText: "개별제어가 등록되었습니다.",
                    cancelCallback: () => {
                        _this.props.callback();
                        _this.close();
                    }
                })
            } else {
                _this.loading(false);
                _this.props.alertError(status, data);
            }
        }));
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };
    releaseLoadingByTimeout = () => {
        let loading = Object.assign({}, this.state.loading);
        const _this = this;
        if (loading) {
            setTimeout(() => {
                loading = false;
                _this.setState({loading});
            }, 2500);
        }
    };

    loading = (isLoading) => {
        let loading = Object.assign(this.state.loading);
        if (isLoading) {
            loading = true;
            this.releaseLoadingByTimeout();
        } else {
            loading = false;
        }
        this.setState({loading});
    };

    handleSelect = (option, key) => {
        let temp = Object.assign({}, this.state);
        temp[key] = option;
        temp.form[key] = option.value;
        this.setState(temp);
    };

    handleInput = (e) => {
        let temp = Object.assign({}, this.state);
        temp.form.value = e.target.value;
        this.setState(temp);
    };

    render() {
        return (
            <article id="lcModalIndividualControlWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalIndividualControl" className="modal-wrap lc-visible-modal">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">{this.state.control.control_name} 개별제어</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <Spinner spinning={this.state.loading}/>
                                    <form onSubmit={this.submit}>
                                        <div className="control-row-wrap">
                                            <div className="select-module-wrap">
                                                <Select value={this.state.state}
                                                        placeholder="동작 선택"
                                                        onChange={option => this.handleSelect(option, "state")}
                                                        isSearchable={ false }
                                                        options={STATE_OPTION[this.state.control.type]}
                                                        styles={colourStyles}/>
                                            </div>
                                            <span className="lc-just-text">(을)를</span>
                                        </div>

                                        {
                                            this.state.state && this.state.state.hasValue ?
                                                (
                                                    <div className="control-row-wrap">
                                                        <div className="lc-control-input-wrap">
                                                            <div className="item-wrap">
                                                                <div className="select-module-wrap">
                                                                    <Select value={this.state.unit}
                                                                            placeholder="단위 선택"
                                                                            onChange={option => this.handleSelect(option, "unit")}
                                                                            isSearchable={ false }
                                                                            options={UNIT_OPTION[this.state.control.type]}
                                                                            styles={colourStyles}/>
                                                                </div>
                                                            </div>
                                                            <div className="item-wrap">
                                                                <input type="number" value={this.state.form.value}
                                                                       onChange={this.handleInput}
                                                                       maxLength={this.state.control.type === TYPE_MOTOR ? 3 : null}
                                                                       className="control-value-input" placeholder="입력"/>
                                                            </div>
                                                        </div>
                                                        <span className="lc-just-text">{VALUE_OPTION[this.state.form.unit]}</span>
                                                        <div className="line-break"/>
                                                    </div>
                                                ) : null
                                        }
                                        <span className="lc-order-text">실행해주세요</span>
                                        <button className="theme-button" type="submit">실행</button>
                                    </form>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useIndividualControl(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalIndividualControl));
