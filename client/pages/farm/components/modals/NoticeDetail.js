import React, { Component } from 'react';

import { useNoticeDetail } from "../../contexts/modals/noticeDetail";
import { useAlert } from "../../contexts/alert";
import noticeManager from '../../managers/notice';
import Pagination from '../../components/Pagination';

import { date } from "../../../../utils/filter";

class ModalNoticeDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            notices: {},
            notice: '',
            mode: 'listShow',
            offset: 0,
            size: 5,
            searchField: "title",
            searchItem: "",
        };
    }
    componentDidMount() {
        this.searchPage(0);
        this.props.sync(this.init);
    }

    init = (notice) => {
        this.setState({
            mode: 'listShow',
        });

        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(this.state.mode == 'detailShow'){
            this.showNoticeList();
        }else{
            if (e) e.preventDefault();
            document.removeEventListener('keyup', this.closeOnEscape);
            this.props.close();
        }
       
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    showNoticeDetail = (e, notice) => {
        e.preventDefault();

        this.setState({
            notice: notice,
            mode: 'detailShow'
        });
    }

    showNoticeList = (e) => {
        this.setState({
            notice: null,
            mode: 'listShow'
        });
    }

    inputSearchItem = (e) => {
        this.setState({
            searchItem: e.target.value
        });
    };

    onInputKeyUp = (e) => {
        if (e.key === "Enter") {
            this.searchPage(0);
        }
    };
    
    search = (e) => {
        this.searchPage(0);
    };

    searchPage = (offset) => {
        const _this = this;
        this.setState({
            offset: offset
        });

        let query = {
            offset: offset,
            size: this.state.size
        };

        if(_this.state.searchItem) {
            query.searchField = _this.state.searchField;
            query.searchItem = _this.state.searchItem;
        }

        noticeManager.findNotice(query, ((status, data) => {
            if (status === 200) {
                _this.setState({
                    notices: data.data.rows,
                    noticeCount: data.data.count
                })
            } else if (status === 404) {
                _this.setState({
                    notices: [],
                    noticeCount: 0
                })
            } else if (status === 400){
            
            }else {
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {
        return (
            <article id="lcModalNoticeDetailWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalNoticeDetail" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">공지사항</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>

                                <section className="modal-body">
                                    {this.state.mode == "listShow"?
                                        <div className="lc-animal-search">
                                            <input type="text" ref={ref => { this.searchinput = ref }} className="search-box" placeholder="검색"
                                                onKeyUp={e => this.onInputKeyUp(e)}
                                                onChange={e => this.inputSearchItem(e)}
                                            />
                                            <button className="searchBtn" onClick={e => this.search(e)}><img src="/public/images/commons/btn-search@2x.png" /></button>
                                        </div> : null
                                    }
                                    
                                    {this.state.notices ?
                                        <ul className={"lc-notice-list" + (this.state.mode !== "listShow" ? " notice-list-mode" : " ")}>
                                            
                                            {
                                                this.state.notices.map((notice, index) => {
                                                    return (
                                                            <li key={index}>
                                                                <article className="lc-content-card">
                                                                    <div className="content-position-wrap">

                                                                        <div className="notice-content-wrap lc-non-individual-mode" onClick={e => this.showNoticeDetail(e, notice)}>

                                                                            <div className="notice-text-count-wrap">
                                                                                <p className="notice-text">{notice.title}</p>
                                                                            </div>

                                                                            <div className="absolute-item-wrap">
                                                                                <p className="notice-text">{notice.created_at}</p>
                                                                            </div>

                                                                        </div>

                                                                    </div>
                                                                </article>
                                                            </li>
                                                    )
                                                })
                                            }
                                        </ul> : null
                                    }
                                    {!this.state.notices ?
                                        <p>데이터가 없습니다.</p> : null
                                    }

                                    {this.state.noticeCount && this.state.mode == "listShow" && (this.state.noticeCount > this.state.size) ?
                                        <Pagination count={this.state.noticeCount}
                                            offset={this.state.offset}
                                            size={this.state.size}
                                            searchPage={this.searchPage} />
                                        : null
                                    }
                                    {this.state.mode == 'detailShow' ?
                                        <div>
                                            <div className={"lc-textarea-shape" + (this.state.mode !== 'detailShow' ? " notice-detail-mode" : "")} dangerouslySetInnerHTML={{ __html: this.state.notice.contents }} />
                                        </div> : null
                                    }
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useNoticeDetail(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalNoticeDetail));