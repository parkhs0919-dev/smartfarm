import React, {Component} from 'react';
import {useHousePositionSetting} from "../../contexts/modals/housePositionSetting";
import {useHouse} from "../../contexts/house";
import {useAlert} from "../../contexts/alert";
import {noSession} from "../../utils/session";
import housesDirectionManager from "../../managers/houses";
import sensorManager from "../../managers/sensor";
import controlGroupManager from"../../managers/controlGroup";

import deepcopy from "../../../../utils/deepcopy";
import Select from "react-select";
import {colourStyles} from "../../utils/select";
import translate from "../../constants/translate";
import {useWindDirectionSetting} from "../../contexts/modals/windDirectionSetting";
import alarmManager from "../../managers/alarm";

const DEFAULT_STATE = {
    selectDirection: null,
    houseId: '',
    selectedWindDirection: [],
    controlGroups : [],
};

const PositionSelect = [
    {label:"북", value:"N"},
    {label:"북북동", value:"NNE"},
    {label:"북동", value:"NE"},
    {label:"동북동", value:"ENE"},
    {label:"동", value:"E"},
    {label:"동남동", value:"ESE"},
    {label:"남동", value:"SE"},
    {label:"남남동", value:"SSE"},
    {label:"남", value:"S"},
    {label:"남남서", value:"SSW"},
    {label:"남서", value:"SW"},
    {label:"서남서", value:"WSW"},
    {label:"서", value:"W"},
    {label:"서북서", value:"WNW"},
    {label:"북서", value:"NW"},
    {label:"북북서", value:"NNW"},
];

class ModalHousePositionSetting extends Component {
    constructor(props) {
        super(props);

        this.state = deepcopy(DEFAULT_STATE);
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        const _this = this;

        this.findWindDirectionSensors();
        this.findHouseId(function () {
            _this.findHouseDirections();
        });
        this.findControlGroups();

        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    handlePositionOption = (e) => {
        this.setState({selectDirection:{label:e.label,value:e.value},});
    };

    handleSensor = (e) => {
        if(!e.value) {
            return this.setState({
                selectedWindDirection:{
                    label: "선택 안함",
                    value: null
                }
            });
        }
        if(e.origin.position === "out" && e.origin.type === "windDirection"){
            return this.setState({
                selectedWindDirection:{
                    label:e.origin.sensor_name,
                    value:e.origin.id
                }
            });
        }
    };

    findHouseId = (callback) => {
        let body = {
            house_id: this.props.getHouseId(),
        };
        this.setState({
            houseId:body.house_id,
        }, function () {
            callback();
        });
    };
    findControlGroups = () => {

        const _this = this;

        let where = {
            house_id : this.props.getHouseId(),
            type : 'windDirection',
        };

        controlGroupManager.findAll(where, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    controlGroups : _this.generateControlGroup(data.data.rows),
                });
            }else if(status === 404) {
                _this.setState({
                    controlGroups : []
                })
            } else {
                _this.props.alertError(status, data);
            }

        }));

    };

    generateControlGroup = data => {
        let arr = [];

        data.forEach(item => {
            arr.push({
                id : item.id,
                house_id : item.house_id,
                type : item.type,
                control_group_name : translate.WINDOW_POSITION[item.window_position] + ' ' + translate.MOTOR_DIRECTION[item.direction],
                window_position: item.window_position,
                direction : item.direction,
                controlGroupItems : item.controlGroupItems
            })
        })

        return arr;
    }

    findHouseDirections = () => {
        const _this = this;
        let body = {};
        let selectDirection = '';
        let selectedWindDirection = '';

        body.id = this.state.houseId;

        housesDirectionManager.findById(body, noSession(this.props.show)((status, data) => {
            if(status === 200) {
                selectDirection = {label:(data.data.direction ? translate.DIRECTION[data.data.direction] : "하우스 입구의 방위설정"), value:(data.data.direction)};
                selectedWindDirection = data.data.houseWindDirectionSensor ? {label:data.data.houseWindDirectionSensor.sensor.sensor_name, value:data.data.houseWindDirectionSensor.sensor.id} : {label:"선택 없음", value:null};
                _this.setState({
                    selectDirection,
                    selectedWindDirection
                });
            } else if (status === 404) {
                selectDirection = {label:"하우스 입구의 방위설정", value:""};
                _this.setState({selectDirection});
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findWindDirectionSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            type: "windDirection",
        };

        sensorManager.findAll(query, noSession(this.props.show)((status, data) => {
            if(status === 200){
                let windDirections = [];

                data.data.rows.forEach((item, index, array) => {
                    if(index === 0){
                        windDirections.push({
                            value: null,
                            label: "선택 안함",
                        });
                    }
                   windDirections.push({
                       value: item.id,
                       label: item.sensor_name,
                       origin: item,
                   });
                });

                _this.setState({windDirections});
            } else if (status === 404) {
                _this.setState({windDirections: []});
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    setHousePosition = (e) => {
        const _this = this;
        let body = {
            id:this.state.houseId
        };
        if(this.state.selectDirection){
            this.props.show({
                alertText: this.state.selectedWindDirection.value ? "하우스 방위 및 풍향 센서를\n재설정 하시겠습니까?" : "하우스 방위를\n재설정 하시겠습니까?",
                cancelText: "취소",
                submitText: "변경",
                submitCallback: () => {
                    body.direction = this.state.selectDirection.value;
                    if(this.state.selectedWindDirection.value) body.sensor_id = this.state.selectedWindDirection.value;

                    housesDirectionManager.update(body, noSession(this.props.show)((status, data) => {
                        if(status === 200){
                            _this.props.show({
                                alertText: "변경이 완료 되었습니다.",
                                cancelCallback: () => {
                                    this.close();
                                }
                            });
                        } else {
                            _this.props.show({
                                alertText: "변경이 실패하였습니다.",
                            });
                        }
                    }));
                }
            });
        } else {
            this.props.show({
                alertText: "하우스 방위를\n선택 해주세요",
            });
        }
    };

    setWindDirectionGroup = (e, index) => {
        const _this = this;
        document.removeEventListener('keyup', this.closeOnEscape);
        const item = this.state.controlGroups[index];
        controlGroupManager.findById(item.id, (status, data) => {
            if(status === 200){
                if(data.data.count > 0){
                    return _this.props.show({
                        alertText: "자동제어에 등록된 풍상/풍하 그룹은 수정할 수 없습니다.",
                    });
                }else {
                    this.props.openWindDirectionSetting(item, this.afterControlGroupSetting);
                }
            }else if(status === 404){
                return _this.props.show({
                    alertText: "풍상/풍하 그룹을 수정할 수 없습니다.",
                });
            }
        })
    }

    addControlGroup = e => {
        const _this = this;
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.openWindDirectionSetting(null ,this.afterControlGroupSetting);
    }

    removeWindDirectionGroup = (e, index) => {

        let _this = this;
        let item = _this.state.controlGroups[index];

        controlGroupManager.findById(item.id, (status, data) => {
            if(status === 200){
                if(data.data.count > 0){
                    return _this.props.show({
                        alertText: "자동제어에 등록된 풍상/풍하 그룹은 삭제할 수 없습니다.",
                    });
                }else {
                    this.props.show({
                        alertText: "풍상/풍하 그룹을\n삭제 하시겠습니까?",
                        cancelText: "취소",
                        submitText: "삭제",
                        submitCallback: () => {
                            controlGroupManager.remove(item.id, (status, data) => {
                                if(status === 200) {
                                    _this.props.showDialog({
                                        alertText: "삭제되었습니다.",
                                    });
                                    this.findControlGroups();
                                } else {
                                    _this.props.alertError(status, data);
                                }
                            });
                        }
                    });
                }
            }else if(status === 404){
                return _this.props.show({
                    alertText: "풍상/풍하 그룹을 수정할 수 없습니다.",
                });
            }
        })

        document.addEventListener('keyup', this.closeOnEscape);
    }

    afterControlGroupSetting = () => {
        this.findControlGroups();
        document.addEventListener('keyup', this.closeOnEscape);
    }

    render() {
        return (
            <article id="lcModalHousePositionSettingWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalHousePositionSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">풍상/풍하 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div className="lc-setting-house-position">
                                        <p className="lc-modal-body-title">하우스 입구의 방위를 설정해주세요.</p>
                                        <div className="select-module-wrap">
                                            <Select value={this.state.selectDirection}
                                                    placeholder="방위 선택"
                                                    onChange={e => this.handlePositionOption(e)}
                                                    isSearchable={ false }
                                                    options={PositionSelect}
                                                    styles={colourStyles}/>
                                        </div>
                                        <p className="lc-modal-body-title">풍향 센서를 선택해주세요.</p>
                                        <div className="select-module-wrap">
                                            <Select value={this.state.selectedWindDirection}
                                                    placeholder="센서를 선택해주세요"
                                                    onChange={e => this.handleSensor(e)}
                                                    isSearchable={ false }
                                                    options={this.state.windDirections}
                                                    styles={colourStyles}/>
                                        </div>
                                        <button className="theme-button" onClick={e => this.setHousePosition(e)}>설정</button>
                                        <span className="lc-content-span">* 각 모터의 풍상/풍하창 위치 설정은 [모터 설정]에서 지정할 수 있습니다.</span>
                                    </div>
                                    <div className="lc-setting-wind-direction">
                                        <button className="theme-button" onClick={e => this.addControlGroup(e)}>풍상/풍하 그룹 설정 추가</button>

                                        {this.state.controlGroups && this.state.controlGroups.length > 0 ?
                                            <div className='lc-wind-direction-list'>
                                                <ul>
                                                {this.state.controlGroups ? this.state.controlGroups.map((item, index) => {
                                                        return (
                                                            <li key={index}>
                                                                <p className="lc-wind-direction-name-wrap">
                                                                    <span className="lc-wind-direction-name-index">{index + 1}</span>
                                                                    <span className="lc-wind-direction-name">{item.control_group_name}</span>
                                                                </p>
                                                                <div className="three-dot-menu" tabIndex="0"  style={{zIndex: (5+ this.state.controlGroups.length - index)}}>
                                                                    <div className="icon-wrap">
                                                                        <i/><i/><i/>
                                                                    </div>
                                                                    <div className="three-dot-view-more-menu">
                                                                        <ul>
                                                                            <li onClick={e => this.setWindDirectionGroup(e, index)}><span>수정</span></li>
                                                                            <li onClick={e => this.removeWindDirectionGroup(e, index)}><span>삭제</span></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        )
                                                    }) :
                                                    <li className="empty-list-item">등록된 풍상/풍하 그룹이 없습니다.</li>
                                                }
                                                </ul>
                                            </div>
                                            : null
                                        }

                                    </div>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        show: actions.show,
        alertError: actions.alertError,
    })
)(useWindDirectionSetting(
    ({actions}) => ({
        openWindDirectionSetting : actions.open,
    })
)(useHousePositionSetting(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalHousePositionSetting))));
