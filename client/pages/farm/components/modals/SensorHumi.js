import React, {Component} from 'react';
import {useSocket} from "../../contexts/socket";
import {useSensorHumi} from "../../contexts/modals/SensorHumi";
import {useAlert} from "../../contexts/alert";
import {addListener, on} from "../../utils/socket";
import deepcopy from "../../../../utils/deepcopy";
import sensorhumi from '../../managers/sensorhumi';
import {noSession} from "../../utils/session";

const DEFAULT_VALUE = {
    sensor: {},
    value: null,
};
class ModalSensorHumi extends Component {
    constructor(props) {
        super(props);   
        this.state = deepcopy(DEFAULT_VALUE);
    }

    componentDidMount() {
        this.props.sync(this.init);
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('inner-sensor-values');
    }

    init = (sensor) => {
        const _this = this;     
        this.setState({sensor}, () => {
            _this.findHumi();
        });
        document.addEventListener('keyup', this.closeOnEscape);
    };

    listener = (socket) => {
        const _this = this;
        on(socket)('inner-sensor-values', ({innerSensors}) => {
            if(_this.props.isVisible) _this.changeHumi(innerSensors);
        });
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_VALUE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {   
        if (e.keyCode === 27) this.close();
    };

    changeHumi = (sensors) => {
        for(let i=0; i<sensors.length; i++) {
            if(this.state.sensor.id && this.state.sensor.id === sensors[i].id) {
                let sensor = deepcopy(this.state.sensor);
                sensor.value = sensors[i].value;
                return this.setState({sensor});
            }
        }
    };

    findHumi = () => {
        const _this = this;
        let body = {
            id: this.state.sensor.id
        };
        sensorhumi.findhumi(body, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState(data.data);
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {   
        return (
            <article id="lcModalSensorTemperatureWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalSensorTemperature" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">{this.state.sensor.sensor_name}</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>
                                <section className="modal-body">
                                    <div className="sensor-value-wrap">
                                        <article className="lc-content-card">
                                            <span className="sensor-name">현재 습도</span>
                                            <span className="sensor-value">{this.state.sensor.value}%</span>
                                        </article>
                                        <article className="lc-content-card">
                                            <span className="sensor-name">이슬점온도</span>
                                            {
                                                this.state.dew_point_temperature ?
                                                    (
                                                        <span className="sensor-value">{this.state.dew_point_temperature}℃</span>
                                                    ):
                                                    (  
                                                        <span className="not-allowed-text">표시가 불가능한 상태입니다.</span>
                                                    )
                                            }
                                        </article>                                            
                                        <article className="lc-content-card">
                                            <span className="sensor-name">수분부족분 (HD)</span>
                                            {
                                                this.state.humidity_deficit ?
                                                    (
                                                        <span className="sensor-value">{this.state.humidity_deficit}g/m3</span>
                                                    ):
                                                    (  
                                                        <span className="not-allowed-text">표시가 불가능한 상태입니다.</span>
                                                    )
                                            }
                                        </article>
                                    </div>                                                                       
                                    <button className="theme-button" onClick={this.close}>확인</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useSensorHumi(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSensorHumi)));