import React, {Component} from 'react';
import {useJournal} from "../../contexts/modals/journal";
import {useAlert} from "../../contexts/alert";
import journalManager from "../../managers/journal";

import CONSTANT from '../../constants/constant';

class ModalJournal extends Component{
    constructor(props) {
        super(props);

        this.house_id = null;
        this.state = {
            journalContent: ''
        };
    }
    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        let initialState = Object.assign({}, CONSTANT.defaultJournalState);
        this.setState(initialState);
        this.house_id = data.house_id;
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    submit = (e) => {
        if(e) e.preventDefault();
        const _this = this;

        let body = {
            house_id: this.house_id,
            contents: this.state.journalContent
        };

        journalManager.create(body, (status, data) => {
            if(status === 200) {
                _this.props.showDialog({
                    alertText: '일지가 작성되었습니다.',
                    cancelCallback: () => {
                        _this.close();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        });
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    handleInput = e => {
        const val = e.target.value;
        this.setState({
            journalContent: val
        });
    };

    render() {
        return (
            <article id="lcModalJournalWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalJournal" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">영농일지</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <form className="lc-modal-form" onSubmit={this.submit}>
                                        <textarea rows="6" className="lc-textarea" placeholder="일지 내용을 작성해주세요."
                                                  value={this.state.journalContent} onChange={this.handleInput}/>
                                        <button className="theme-button" type="submit">등록</button>
                                    </form>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useJournal(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalJournal));