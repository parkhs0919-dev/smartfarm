import React, {Component} from 'react';
import {useCCTVSetting} from "../../contexts/modals/cctvSetting";
import {useHouse} from "../../contexts/house";
import {useAlert} from "../../contexts/alert";
import cctvManager from '../../managers/cctv';
import Spinner from "../Spinner";
import {noSession} from "../../utils/session";

class ModalCCTVSetting extends Component {
    constructor(props) {
        super(props);

        this.state = {
            is_auto_close: false,
            cctvs: [],
            loading: {
                is_auto_close: false,
                cctv_name: false
            }
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        this.getCCTVs();
        this.getCCTVSetting();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    getCCTVSetting = () => {
        const _this = this;
        cctvManager.findCCTVSetting(noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    is_auto_close: data.data.is_auto_close
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    getCCTVs = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId()
        };
        cctvManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    cctvs: data.data.rows
                })
            } else if (status === 404) {
                _this.props.showDialog({
                    alertText: "설정 가능한 CCTV가 없습니다.",
                    cancelCallback: () => {
                        _this.props.close();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    releaseLoadingByTimeout = (key) => {
        let temp = Object.assign({}, this.state.loading);
        const _this = this;
        if (temp[key]) {
            setTimeout(() => {
                temp[key] = false;
                _this.setState({
                    loading: temp
                });
            }, 2500);
        }
    };

    loading = (key, isLoading) => {
        let loading = Object.assign({}, this.state.loading);
        if (isLoading) {
            loading[key] = true;
            this.releaseLoadingByTimeout(key);
        } else {
            delete loading[key];
        }
        this.setState({loading});
    };

    clickLabel = (e, val) => {
        e.preventDefault();

        let state = {
            is_auto_close: val,
        };
        this.setState(state);
    };

    handleInput = (e, index) => {
        let cctvs = JSON.parse(JSON.stringify(this.state.cctvs));
        cctvs[index].cctv_name = e.target.value;
        this.setState({
            cctvs: cctvs
        });
    };

    setCCTVs = () => {
        this.setState({
            loading: { is_auto_close: true, cctv_name: true }
        });
        const _this = this;
        let cctvs = JSON.parse(JSON.stringify(this.state.cctvs));
        let isAutoClose = this.state.is_auto_close;
        let cctvRename = {};
        let cctvArr = [];

        cctvs.forEach((cctv) => {
            let obj = {};
            obj.id = cctv.id;
            obj.cctv_name = cctv.cctv_name;
            cctvArr.push(obj);
        });
        cctvRename.cctvs = cctvArr;

        cctvManager.setCCTVSetting({is_auto_close: isAutoClose}, noSession(this.props.showDialog)((status, data) => {
            _this.loading("is_auto_close", false);
            if(status === 200) {
                cctvManager.renameCCTVs(cctvRename, noSession(this.props.showDialog)((status, data) => {
                    _this.loading("cctv_name", false);
                    if(status === 200) {
                        _this.props.showDialog({
                            alertText: "설정이 적용되었습니다.",
                            cancelCallback: () => {
                                _this.props.close();
                            }
                        })
                    } else {
                        _this.alertError(status, data);
                    }
                }));
            } else {
                _this.alertError(status, data);
            }
        }));
    };

    render() {
        return (
            <article id="lcModalCCTVSettingWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalCCTVSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">CCTV 설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <Spinner spinning={this.state.loading.is_auto_close || this.state.loading.cctv_name}/>
                                    <section className="cctv-timer-wrap">
                                        <label className="title-label">5분 후 재생 멈춤</label>
                                        <div className="toggle-radio-wrap two-state">
                                            <input type="radio" name="cctv-timer-setting-toggle-option"
                                                   onChange={e => {}}
                                                   checked={this.state.is_auto_close === true}/>

                                            <input type="radio" name="cctv-timer-setting-toggle-option"
                                                   onChange={e => {}}
                                                   checked={this.state.is_auto_close === false}/>

                                            <label onClick={e => this.clickLabel(e, true)}>ON</label>
                                            <label onClick={e => this.clickLabel(e, false)}>OFF</label>
                                            <i className="toggle-slider-icon"/>
                                        </div>
                                    </section>

                                    <section className="cctv-name-change-wrap">
                                        <h4 className="sensor-title">CCTV 명칭 변경</h4>
                                        <ul id="cctvInputList">
                                            {
                                                this.state.cctvs.map((item, index) => {
                                                    return (
                                                        <li key={index}>
                                                            <input className="lc-farm-input" placeholder="CCTV명 설정" type="text" value={item.cctv_name} onChange={e => this.handleInput(e, index)}/>
                                                        </li>
                                                    )
                                                })
                                            }
                                        </ul>
                                        <button className="theme-button" onClick={this.setCCTVs}>적용</button>
                                    </section>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useCCTVSetting(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalCCTVSetting)));