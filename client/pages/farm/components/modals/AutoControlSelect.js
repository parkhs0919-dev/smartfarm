import React, {Component} from 'react';
import {useAutoControlSelect} from "../../contexts/modals/autoControlSelect";
import {useAutoControl} from "../../contexts/modals/autoControl";
import {useAlert} from "../../contexts/alert";
import controlManager from '../../managers/control';
import {noSession} from "../../utils/session";
import controlGroupManager from "../../managers/controlGroup";

const CONTROL_NAME_TRANSLATE = {
    motor: "모터",
    power: "파워"
};

class ModalAutoControlSelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            controls: [],
            controlGroups :[],
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    componentWillUnmount() {
        document.removeEventListener('keyup', this.closeOnEscape);
    }

    init = (data) => {
        this.setState(data, () => {
            this.findControls();
            this.findControlGroups();
        });
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findControls = () => {
        const _this = this;

        let query = {
            type: this.state.type,
            house_id: this.state.house_id
        };
        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    controls: data.data.rows
                });
            } else if (status === 404) {
                _this.setState({
                    controls: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };


    findControlGroups = () => {
        const _this = this;
        let query = {
            house_id : this.state.house_id
        };

        controlGroupManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200){
                _this.setState({
                    controlGroups : data.data.rows
                });
            }else if(status === 404){
                _this.setState({
                    controlGroups : []
                });
            }else{
                _this.props.alertError(status, data);

            }
        }))
    }

    openAutoControlModal = (item) => {
        if ((item.autoControlItemCount + item.autoControlStepCount) === 0) {
            return _this.props.showDialog({
                alertText: "설정된 자동제어가 없습니다."
            });
        }
        return _this.props.openAutoControlModal({
            control_id: item.id
        });
    };

    render() {
        return (
            <article id="lcModalAutoControlSelectWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalAutoControlSelect" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">자동제어 대상 기기 선택 {`${CONTROL_NAME_TRANSLATE[this.state.type]}`}</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div id="controlContentWrap">
                                        <ul id="controlList">
                                            {this.state.controlGroups && this.state.controlGroups.length && (

                                                this.state.controlGroups.map((item, index) => {
                                                    return (
                                                        <li className="control-list-item" key={index}>
                                                            <button className="card-header-button"><span>{item.control_group_name}</span><span className="number-badge">{item.autoControlItemCount + item.autoControlStepCount}</span></button>
                                                        </li>
                                                    )
                                                })

                                            )}

                                            {this.state.controls && this.state.controls.length && (
                                                this.state.controls.map((item, index) => {
                                                    return (
                                                        <li className="control-list-item" key={index}>
                                                            <button className="card-header-button"><span>{item.control_name}</span><span className="number-badge">{item.autoControlItemCount + item.autoControlStepCount}</span></button>
                                                        </li>
                                                    )
                                                })
                                            )}
                                        </ul>
                                    </div>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAutoControl()(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useAutoControlSelect(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalAutoControlSelect)));