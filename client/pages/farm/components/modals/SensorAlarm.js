import React, { Component } from 'react';
import { useSensorAlarm } from "../../contexts/modals/sensorAlarm";

import { useSocket } from "../../contexts/socket";
import { useAlert } from "../../contexts/alert";
import CONSTANT from "../../constants/constant";
import TRANSLATE from '../../constants/translate';
import { colourStyles } from "../../../farm/utils/select";
import Select from 'react-select';
import sensorManager from '../../managers/sensor';
import alarmManager from '../../managers/alarm';
import { sanitize, returnSensorSelect } from "../../utils/sensor";

import deepcopy from "../../../../utils/deepcopy";
import { noSession } from "../../utils/session";

const SENSOR_TYPE = CONSTANT.sensorType;
const SENSOR_TYPE_TRANSLATE = TRANSLATE.SENSOR;
const REPEAT_TYPE = CONSTANT.defaultAlarm.repeat;
const DEFAULT_VALUE = CONSTANT.DEFAULT_VALUES.SENSOR_ALARMS;
const MODE_ORDINARY = "ordinary";

const SPECIAL_SENSOR_TYPES = CONSTANT.SPECIAL_SENSOR;
const alarm = CONSTANT.defaultAlarm.Type;

const time = CONSTANT.defaultAlarm.Time
const cycle = CONSTANT.defaultAlarm.cycle

class ModalInnerSensorAlarm extends Component {
    constructor(props) {
        super(props);
        this.audio = null
        this.moreMenuRefs = new Map();
        this.state = {
            form: {
                value: '',
            },
            alarm: {},
            alarms: [],
            sensorOptions: [],
            sensor_id: null,
            condition: null,
            cycle: { value: '5', label: '05분' },
            alarm_name: '',
            cardMode: MODE_ORDINARY,
            repeat:{value:'1',label:'1번'},

        };
    }

    componentDidMount() {
        this.props.sync(this.init);

    }




    init = (data) => {
        let state = Object.assign({}, data);

        state.position = SENSOR_TYPE[data.key];

        this.setState(state, () => {
            this.getSensors();
            this.getAlarms();
        });
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if (e) e.preventDefault();
        if (this.audio) {
            this.audio.pause();
            this.audio.currentTime = 0;
        }
        this.setState(DEFAULT_VALUE);

        document.removeEventListener('keyup', this.closeOnEscape);

        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {

        e.preventDefault();
        const _this = this;
        let state = Object.assign({}, this.state);
        let body = {};
        let random = Math.round(Math.random() * 999999);

        if (!state.alarm_name) {
            return this.props.showDialog({
                alertText: "알람명을 입력해 주세요."
            });
        }
        if (!state.sensor_id) {
            return this.props.showDialog({
                alertText: "센서를 선택해주세요."
            });
        }
        if (state.hasOwnProperty('alarm_select') == false) {
            return this.props.showDialog({
                alertText: "센서 항목을 선택해 주세요."
            });
        }
        if (!state.condition && state.alarm_select.value == 'range') {
            return this.props.showDialog({
                alertText: "범위를 선택해주세요."
            });
        }
        if (!state.alarm_select) {
            return this.props.showDialog({
                alertText: "알람 내용을 선택해 주세요."
            });
        }

        if (SPECIAL_SENSOR_TYPES[state.sensor_id.origin.type] && state.alarm_select.value == 'range') {

            body.condition = "equal";
            body.value = state.condition.value;
        } else if (SPECIAL_SENSOR_TYPES[state.sensor_id.origin.type] && state.alarm_select.value == 'error') {

            body.condition = 'equal';
            body.value = random;
        } else if (!SPECIAL_SENSOR_TYPES[state.sensor_id.origin.type] && state.alarm_select.value == 'range') {

            body.condition = state.condition.value;
            body.value = state.form.value;
        } else if (!SPECIAL_SENSOR_TYPES[state.sensor_id.origin.type] && state.alarm_select.value == 'error') {

            body.condition = 'equal';
            body.value = random;
        } else {
            if (!state.form.value && state.alarm_select.value == 'range') {
                return this.props.showDialog({
                    alertText: "센서 값을 입력해주세요."
                });
            }
        }

        if (state.alarm.alarmtime) {
            if (state.hasOwnProperty('usefirst') == false) {
                return this.props.showDialog({
                    alertText: "알람시간을 입력해 주세요."
                });
            }
            if (state.hasOwnProperty('useend') == false) {
                return this.props.showDialog({
                    alertText: "알람시간을 입력해 주세요."
                });
            }
            if (state.usefirst.value == state.useend.value) {
                return this.props.showDialog({
                    alertText: "알람시간을 확인해 주세요."
                });
            }
           
            body.is_use = state.alarm.alarmtime;
            body.use_start_hour = state.usefirst.value;
            body.use_end_hour = state.useend.value;
        } else {
            body.is_use = false;
            body.use_start_hour = '0';
            body.use_end_hour = '0';
        }
        if (state.alarm.notalarmtime) {
            if (state.hasOwnProperty('notfirst') == false) {
                return this.props.showDialog({
                    alertText: "방해금지 시간을 입력해 주세요."
                });
            }
            if (state.hasOwnProperty('notend') == false) {
                return this.props.showDialog({
                    alertText: "방해금지 시간을 입력해 주세요."
                });
            }
            if (state.notfirst.value == state.notend.value) {
                return this.props.showDialog({
                    alertText: "방해금지 시간을 확인해 주세요."
                });
            }
            body.is_no = state.alarm.notalarmtime;
            body.no_start_hour = state.notfirst.value;
            body.no_end_hour = state.notend.value;
        } else {
            body.is_no = false;
            body.no_start_hour = '0';
            body.no_end_hour = '0';
        }
        if (state.alarm.is_bar) {
            body.is_bar = state.alarm.is_bar;
        } else { body.is_bar = false }
        if (state.alarm.is_image) {

            body.is_image = state.alarm.is_image;
        } else { body.is_image = false }
        if (state.alarm.is_sound) {
            body.is_sound = state.alarm.is_sound;
        } else { body.is_sound = false }
        body.sensor_id = state.sensor_id.value;
        body.name = this.state.alarm_name;
        body.type = state.alarm_select.value;
        body.cycle = state.cycle.value;
        state.repeat?body.repeat = state.repeat.value:null
        
        alarmManager.create(body, noSession(this.props.showDialog)((status, data) => {
            if (status !== 200) {
                _this.props.alertError(status, data);
            } else if (status == 200) {
                _this.props.showDialog({
                    alertText: "알림방식이 등록되었습니다.",
                    cancelCallback: () => {

                        _this.close();
                    }
                })
            }
        }));
    };

    handleSelect = (option, key) => {

        let temp = Object.assign({}, this.state);
        temp[key] = option;
        temp.form[key] = option.value;

        if (key === "sensor_id") {

            temp.condition = null;
            temp.form.value = '';
        }

        this.setState(temp);

    };
    handleSelect_cycle = (option, key) => {
        let temp = Object.assign({}, this.state);
        temp[key] = option;
        temp.form[key] = option.value;


        this.setState(temp);
    };
    handleSelect_firsttime = (option, key) => {
        let temp = Object.assign({}, this.state);
        temp[key] = option;
        temp.form[key] = option.value;

        this.setState(temp);
    };

    handleSelect_firsttime2 = (option, key) => {
        let temp = Object.assign({}, this.state);
        temp[key] = option;
        temp.form[key] = option.value;

        this.setState(temp);
    };

    handleSelect_endtime = (option, key) => {
        let temp = Object.assign({}, this.state);
        temp[key] = option;
        temp.form[key] = option.value;

        this.setState(temp);
    };

    repeat =(option,key) => {
        let temp = Object.assign({}, this.state);
        temp[key] = option;
        temp.form[key] = option.value;

        this.setState(temp);
    }
    handleSelect_endtime2 = (option, key) => {
        let temp = Object.assign({}, this.state);
        temp[key] = option;
        temp.form[key] = option.value;

        this.setState(temp);
    };

    handleSelect_alarm = (option, key) => {

        let temp = Object.assign({}, this.state);
        temp[key] = option;
        temp.form[key] = option.value;

        this.setState(temp);
    }

    handleInput = (e) => {
        let temp = Object.assign({}, this.state);

        temp.form.value = e.target.value;
        this.setState(temp);
    };
    handlealarmName = (e) => {
        this.setState({
            alarm_name: e.target.value
        });
    }


    getSensors = () => {
        const _this = this;

        let query = {
            house_id: this.state.house_id,
            position: this.state.position

        };

        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {

            if (status === 200) {
                let sensors = data.data.rows;
                let arr = [];
                sensors.forEach((item) => {
                    let obj = {};
                    obj.value = item.id;
                    obj.label = item.sensor_name;
                    obj.origin = item;
                    arr.push(obj);
                });

                _this.setState({
                    sensorOptions: arr
                });
            } else if (status === 404) {
                _this.props.showDialog({
                    alertText: "설정 가능한 센서가 없습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                })
            } else {
                _this.alertError(status, data);
            }
        }));
    };

    getAlarms = () => {
        const _this = this;
        let query = {
            house_id: this.state.house_id,
            position: this.state.position
        };
        alarmManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    alarms: data.data.rows
                });
            } else if (status === 404) {
                _this.setState({
                    alarms: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    openAlarmSelectModal = (e, index) => {
        const ref = this.moreMenuRefs.get(index);
        document.removeEventListener('keyup', this.closeOnEscape);
        let alarm = deepcopy(this.state.alarms[index]);

        this.props.useSensorAlarmDetail(alarm, this.afterAlarmSetting);
        ref.blur();
    };

    afterAlarmSetting = () => {
        document.addEventListener('keyup', this.closeOnEscape);
    };

    removeAlarm = (e, index, item) => {
        const ref = this.moreMenuRefs.get(index);
        const _this = this;
        ref.blur();

        this.props.showDialog({
            alertText: "경보를 삭제하시겠습니까?",
            cancelText: "취소",
            submitText: "확인",
            submitCallback: () => {
                alarmManager.remove(item.id, noSession(this.props.showDialog)((status, data) => {
                    if (status === 200) {
                        _this.props.showDialog({
                            alertText: "삭제되었습니다.",
                        });
                    } else {
                        _this.props.alertError(status, data);
                    }
                }));
            }
        })
    };

    toggleDetails = (e, toggleState) => {
        let state = Object.assign({}, this.state);
        let alarms = state.alarms;
        alarms.forEach((item, index) => {
            item.isalarmsTabOpen = toggleState;
        });
        this.setState(state);
    };


    generateAlarmDetail = (alarm) => {

        if (alarm.is_no || alarm.is_bar || alarm.is_image || alarm.is_sound) {
            let str = '('
            if (alarm.is_no == '1') {
                str += `${alarm.cycle}분 간격, ${alarm.no_start_hour}시 ~ ${alarm.no_end_hour}시 방해금지) `
            }
            if (alarm.is_bar) {
                str += '알림바';
            }
            if (alarm.is_image) {
                if (str.length > 1) str += ',';
                str += '이미지';
            }
            if (alarm.is_sound) {
                if (str.length > 1) str += ',';
                str += '사운드';
            }
            str += ')';
            return str;
        }

        return '';
    };

    checkAlarmDetail = (e, key) => {
        let alarm = deepcopy(this.state.alarm);
        alarm[key] = !alarm[key];
        this.setState({ alarm });

    };

    testSound = (e) => {
        if (!this.state.sensor_id) {
            return this.props.showDialog({
                alertText: "센서를 선택해 주세요."
            });
        }
        if (!this.state.alarm_select) {
            return this.props.showDialog({
                alertText: "센서 범위를 선택해 주세요."
            });
        }
        if (!this.state.condition) {
            return this.props.showDialog({
                alertText: "범위를 선택해 주세요."
            });
        }
        if (this.state.sensor_id.origin.type === "windDirection" || this.state.sensor_id.origin.type === "power" || this.state.sensor_id.origin.type === "water" ||
            this.state.sensor_id.origin.type === "window" || this.state.sensor_id.origin.type === "fire" || this.state.sensor_id.origin.type === "door" || this.state.sensor_id.origin.type === "korinsWindDirection") {
            this.audio = new Audio("/public/sounds/etc.mp3");
            this.audio.play();
        } else if (this.state.sensor_id.origin.type === "rain") {
            // this.audio = new Audio("/public/sounds/etc.mp3");
            // this.audio.play();

            if (this.state.condition.value === 0) {
                this.audio = new Audio("/public/sounds/rainon.mp3");
                this.audio.play();
            } else {
                this.audio = new Audio("/public/sounds/rainoff.mp3");
                this.audio.play();
            }
        } else {
            if (this.state.condition.value === "over") {
                this.audio = new Audio("/public/sounds/over.mp3");
                this.audio.play();
            } else if (this.state.condition.value === "under") {
                this.audio = new Audio("/public/sounds/under.mp3");
                this.audio.play();
            }
        }
    };

    togglealarmWrap = (e, index) => {
        let state = Object.assign({}, this.state);
        let alarms = state.alarms;
        alarms[index].isalarmsTabOpen = !alarms[index].isalarmsTabOpen;
        this.setState(state);
    };

    clickalamrsActivateState = (e, val, item, index) => {
        e.preventDefault();
        const _this = this;

        if (item.state === val) {
            return false;
        }


        let body = {
            id: item.id,
            state: val
        };

        let alarms = JSON.parse(JSON.stringify(_this.state.alarms));

        alarmManager.update(body, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                alarms[index].state = val;
                _this.setState({
                    alarms: alarms
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));

    };

    render() {
        let alarm_check
        if (this.state.alarm_select) {
            alarm_check = Object.values(this.state.alarm_select)[0]

        }

        return (
            <article id="lcModalSensorAlarmWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalSensorAlarm" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">{SENSOR_TYPE_TRANSLATE[this.state.key]} 경보설정 등록</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>

                                <section className="modal-body">

                                    <React.Fragment>
                                        <div className="alarm-register-wrap">
                                            <div id="alarmNameWrap" className="alarm-form-section">
                                                <label className="alarm-form-label">알람명</label>
                                                <input type="text" value={this.state.alarm_name}
                                                    onChange={this.handlealarmName} className="lc-farm-input"
                                                    placeholder="알람명" />

                                            </div>
                                        </div>
                                        <div className="alarm-register-wrap">
                                            <form onSubmit={this.submit}>
                                                <ul className="alarm-control-wrap">

                                                    <li className="alarm-control-item">
                                                        <div className="select-module-wrap">
                                                            <Select value={this.state.sensor_id}
                                                                placeholder="센서 선택"
                                                                onChange={option => this.handleSelect(option, "sensor_id")}
                                                                isSearchable={false}
                                                                options={this.state.sensorOptions}
                                                                styles={colourStyles} />
                                                        </div>



                                                    </li>
                                                    <li className="alarm-control-item">
                                                        <div className="select-module-wrap">
                                                            <Select value={this.state.alarm_select}
                                                                placeholder="경보 항목 선택"
                                                                onChange={option => this.handleSelect_alarm(option, "alarm_select")}
                                                                isSearchable={false}
                                                                options={alarm}
                                                                styles={colourStyles} />
                                                        </div>
                                                    </li>

                                                    {
                                                        this.state.sensor_id && !SPECIAL_SENSOR_TYPES[this.state.sensor_id.origin.type] && alarm_check == 'range' ?
                                                            (
                                                                <li className="alarm-control-item">
                                                                    <input type="number" value={this.state.form.value} onChange={this.handleInput} className="sensor-value-input" placeholder="센서 값" />
                                                                    <span className="lc-unit">{this.state.sensor_id.origin.unit}</span>
                                                                </li>
                                                            ) : null
                                                    }
                                                    {
                                                        this.state.sensor_id && this.state.sensor_id.origin && this.state.alarm_select && alarm_check == 'range' ?
                                                            (
                                                                <li className="alarm-control-item">
                                                                    <div className="select-module-wrap">
                                                                        <Select value={this.state.condition}
                                                                            placeholder="범위 선택"
                                                                            onChange={option => this.handleSelect(option, "condition")}
                                                                            isSearchable={false}
                                                                            options={returnSensorSelect(this.state.sensor_id.origin, true)}
                                                                            styles={colourStyles} />
                                                                    </div>
                                                                </li>
                                                            ) : null
                                                    }
                                                    {
                                                        this.state.sensor_id && this.state.sensor_id.origin ?
                                                            (
                                                                <li className="alarm-control-item">
                                                                    <p>일 때 알려주세요</p>
                                                                </li>
                                                            ) : null
                                                    }
                                                </ul>
                                                <div id="alarmSettingWrap" className="alarm-form-section">
                                                    <div className="alarm-setting-form alarm-condition-wrap">
                                                        <label className="alarm-form-label">알람주기</label>
                                                        <div className="lc-setting-form-wrap">
                                                            <div className="form-flex-wrap">
                                                                <div className="left-item select-module-wrap lc-size-36">
                                                                    <Select
                                                                        placeholder="주기 선택"
                                                                        value={this.state.cycle}
                                                                        onChange={option => this.handleSelect_cycle(option, "cycle")}
                                                                        isSearchable={true}
                                                                        options={cycle}
                                                                        styles={colourStyles} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="delay-recount-wrap">
                                                            <label className="alarm-form-label">알림방식 선택</label>
                                                        </div>
                                                        <div className="delay-recount-wrap">
                                                            <ul id="alarmDetailList">
                                                                <li className="alarm-detail-list-item">
                                                                    <article className="check-wrap">
                                                                        <div className="lc-theme-checkbox-wrap">
                                                                            <input value="is_bar"
                                                                                id="isBar"

                                                                                onChange={() => { }} type="checkbox"
                                                                                name="alarmDetailWay" />
                                                                            <label htmlFor="isBar"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'is_bar')}
                                                                                className="box-shape-label" />
                                                                            <label htmlFor="isBar"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'is_bar')}
                                                                                className="text-label">알림바</label>
                                                                        </div>
                                                                    </article>
                                                                </li>
                                                                <li className="alarm-detail-list-item">
                                                                    <article className="check-wrap">
                                                                        <div className="lc-theme-checkbox-wrap">
                                                                            <input id="isImage"
                                                                                value="is_image"

                                                                                onChange={() => { }} type="checkbox"
                                                                                name="alarmDetailWay" />
                                                                            <label
                                                                                htmlFor="isImage"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'is_image')}
                                                                                className="box-shape-label" />
                                                                            <label
                                                                                htmlFor="isImage"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'is_image')}
                                                                                className="text-label">이미지</label>
                                                                        </div>
                                                                    </article>
                                                                </li>
                                                                <li className="alarm-detail-list-item">
                                                                    <article className="check-wrap">
                                                                        <div className="lc-theme-checkbox-wrap">
                                                                            <input id="isSound"
                                                                                value="is_sound"

                                                                                onChange={() => { }} type="checkbox"
                                                                                name="alarmDetailWay" />
                                                                            <label
                                                                                htmlFor="isSound"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'is_sound')}
                                                                                className="box-shape-label" />
                                                                            <label
                                                                                htmlFor="isSound"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'is_sound')}
                                                                                className="text-label">음성</label>
                                                                        </div>
                                                                    </article>
                                                                    <div className="sound-test-wrap" onClick={e => this.testSound(e)}>
                                                                        <i />
                                                                        <span>알림 소리 미리듣기</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div className="alarm-setting-form2 alarm-execution-wrap">
                                                        <label className="alarm-form-label">알림 시간대</label>
                                                        <div className="delay-recount-wrap">
                                                            <ul id="alarmDetailList2">
                                                                <li className="alarm-detail-list-item">
                                                                    <article className="check-wrap">
                                                                        <div className="lc-theme-checkbox-wrap">
                                                                            <input value="alarmtime"
                                                                                id="alarmtime"
                                                                                checked={this.state.alarm.alarmtime === true}
                                                                                onChange={() => { }} type="checkbox"
                                                                                name="alarmDetailWay" />
                                                                            <label htmlFor="alarmtime"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'alarmtime')}
                                                                                className="box-shape-label" />
                                                                            <label htmlFor="alarmtime"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'alarmtime')}
                                                                                className="text-label">사용</label>
                                                                        </div>
                                                                    </article>

                                                                </li>
                                                                <div className="lc-setting-form-wrap">
                                                                    {
                                                                        this.state.alarm.alarmtime ?
                                                                            (
                                                                                <div className="form-flex-wrap">
                                                                                    <div className="left-item2 select-module-wrap lc-size-36" style={{ zIndex: 11 + this.state.alarms.length }}>
                                                                                        <Select value={this.state.usefirst}
                                                                                            placeholder="시간 선택"
                                                                                            onChange={option => this.handleSelect_firsttime(option, "usefirst")}
                                                                                            isSearchable={false}
                                                                                            options={time}
                                                                                            styles={colourStyles} />
                                                                                    </div>
                                                                                    <span className="lc-modifier">부터</span>
                                                                                    <div className="left-item2 select-module-wrap lc-size-36" style={{ zIndex: 11 + this.state.alarms.length }}>
                                                                                        <Select value={this.state.useend}
                                                                                            placeholder="시간 선택"
                                                                                            onChange={option => this.handleSelect_endtime(option, "useend")}
                                                                                            isSearchable={false}
                                                                                            options={time}
                                                                                            styles={colourStyles} />
                                                                                    </div>
                                                                                    <span className="lc-modifier">까지</span>
                                                                                </div>
                                                                            ) : null
                                                                    }
                                                                </div>
                                                            </ul>


                                                        </div>
                                                        <div className="delay-recount-wrap">
                                                            <label className="alarm-form-label">방해금지 시간대</label>
                                                        </div>
                                                        <div className="delay-recount-wrap">
                                                            <ul id="alarmDetailList2">
                                                                <li className="alarm-detail-list-item">
                                                                    <article className="check-wrap">
                                                                        <div className="lc-theme-checkbox-wrap">
                                                                            <input value="notalarmtime"
                                                                                id="notalarmtime"
                                                                                checked={this.state.alarm.notalarmtime === true}
                                                                                onChange={() => { }} type="checkbox"
                                                                                name="alarmDetailWay" />
                                                                            <label htmlFor="notalarmtime"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'notalarmtime')}
                                                                                className="box-shape-label" />
                                                                            <label htmlFor="notalarmtime"
                                                                                onClick={(e) => this.checkAlarmDetail(e, 'notalarmtime')}
                                                                                className="text-label">사용</label>
                                                                        </div>
                                                                    </article>
                                                                </li>
                                                                <div className="lc-setting-form-wrap">
                                                                    {
                                                                        this.state.alarm.notalarmtime ?
                                                                            (
                                                                                <div className="form-flex-wrap">
                                                                                    <div className="left-item2 select-module-wrap lc-size-36" style={{ zIndex: 10 + this.state.alarms.length }}>
                                                                                        <Select value={this.state.notfirst}
                                                                                            placeholder="시간 선택"
                                                                                            onChange={option => this.handleSelect_firsttime2(option, "notfirst")}
                                                                                            isSearchable={false}
                                                                                            options={time}
                                                                                            styles={colourStyles} />
                                                                                    </div>
                                                                                    <span className="lc-modifier">부터</span>
                                                                                    <div className="left-item2 select-module-wrap lc-size-36" style={{ zIndex: 10 + this.state.alarms.length }}>
                                                                                        <Select value={this.state.notend}
                                                                                            placeholder="시간 선택"
                                                                                            onChange={option => this.handleSelect_endtime2(option, "notend")}
                                                                                            isSearchable={false}
                                                                                            options={time}
                                                                                            styles={colourStyles} />
                                                                                    </div>
                                                                                    <span className="lc-modifier">까지</span>
                                                                                </div>
                                                                            ) : null
                                                                    }
                                                                </div>
                                                                <div className="delay-recount-wrap">
                                                                    <label className="alarm-form-label">반복횟수</label>
                                                                </div>
                                                                <div className="lc-setting-form-wrap">
                                                                    <li className="left-item select-module-wrap lc-size-36">
                                                                        <Select value={this.state.repeat}
                                                                            placeholder="시간 선택"
                                                                            onChange={option => this.repeat(option, "repeat")}
                                                                            isSearchable={false}
                                                                            options={REPEAT_TYPE}
                                                                            styles={colourStyles} />
                                                                    </li>
                                                                </div>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div className="delay-recount-wrap">
                                                    <button className="theme-button" type="submit">등록</button>
                                                </div>
                                            </form>
                                        </div>



                                    </React.Fragment>

                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }

}

export default useSocket(
    ({ state, actions }) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSensorAlarm(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalInnerSensorAlarm)));
