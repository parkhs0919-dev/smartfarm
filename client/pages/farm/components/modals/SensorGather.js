import React, {Component, Fragment} from 'react';
import {useSensorGather} from "../../contexts/modals/sensorGather";
import {useSocket} from "../../contexts/socket";
import {useHouse} from "../../contexts/house";
import {useAlert} from "../../contexts/alert";
import {sanitize} from "../../utils/sensor";
import {setValue} from "../../utils/sensorHandler";

import sensorManager from '../../managers/sensor';
import alarmManager from "../../managers/alarm";
import {addListener, on} from "../../utils/socket";
import {noSession} from "../../utils/session";
import deepcopy from "../../../../utils/deepcopy";


const SENSOR_POSITION_IN = "in";
const SENSOR_POSITION_OUT = "out";

class ModalSensorGather extends Component {
    constructor(props) {
        super(props);

        this.alarmHash = {};
        this.state = {
            outerSensors: [],
            innerSensors: {}
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
        addListener(this, this.listener);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        socket.removeListener('outer-sensor-values');
        socket.removeListener('inner-sensor-values');
        socket.removeListener('outer-alarm');
        socket.removeListener('inner-alarm');
        socket.removeListener('sensors');
    }

    init = () => {
        let innerSensors = deepcopy(this.state.innerSensors);
        for(let i=0; i<window.houses.length; i++) {
            innerSensors[i] = {
                house_name: window.houses[i].house_name
            };
        }
        this.setState({innerSensors}, () => {
            this.findAllSensors();
        });
        document.addEventListener('keyup', this.closeOnEscape);
    };

    listener = (socket) => {
        const _this = this;
        on(socket)('outer-sensor-values', ({outerSensors}) => {
            if (_this.props.isVisible) {
                _this.findAllSensors();
            }
        });
        on(socket)('inner-sensor-values', ({innerSensors}) => {
            if (_this.props.isVisible) _this.findAllSensors();
            // _this.setState({
            //     innerSensors: setValue(innerSensors, _this.state.innerSensors, _this.alarmHash)
            // })
        });
        on(socket)('outer-alarm', () => {
            // if(_this.props.isVisible) _this.getAlarms();
            if (_this.props.isVisible) _this.findAllSensors();
        });
        on(socket)('inner-alarm', () => {
            // if(_this.props.isVisible) _this.getAlarms();
            if (_this.props.isVisible) _this.findAllSensors();
        });
        on(socket)('sensors', (data) => {
            if (_this.props.isVisible) _this.findAllSensors();
        });
    };

    close = (e) => {
        if (e) e.preventDefault();
        this.setState({
            sensors: []
        });
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findOuterSensors = () => {
        const _this = this;
        let query = {
            isTotal: true,
            position: "out"
        };
        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    outerSensors: data.data.rows
                }, () => {
                    _this.getAlarms(_this.state.outerSensors, SENSOR_POSITION_OUT);
                })
            } else if (status === 404) {
                _this.setState({
                    outerSensors: null
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findInnerSensors = () => {
        const _this = this;
        if (window.houses.length) {
            for(let i=0; i<window.houses.length; i++) {
                let query = {
                    house_id: window.houses[i].id,
                    isTotal: true,
                    position: "in"
                };

                sensorManager.findAll(query, noSession(this.props.showDialog) ((status, data) => {
                    if (status === 200) {
                        let innerSensors = deepcopy(this.state.innerSensors);
                        innerSensors[i].data = data.data.rows;
                        _this.setState({innerSensors}, () => {
                            _this.getAlarms(_this.state.innerSensors[i].data, SENSOR_POSITION_IN, i);
                        });
                    } else if (status === 404) {
                        let innerSensors = deepcopy(this.state.innerSensors);
                        innerSensors[i].data = null;
                        _this.setState({innerSensors});
                    } else {
                        _this.props.alertError(status, data);
                    }
                }));
            }
        }
    };

    findAllSensors = () => {
        this.findOuterSensors();
        this.findInnerSensors();
    };

    getAlarms = (sensorsParam, position, key) => {
        const _this = this;
        let query = {};
        alarmManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.generateAlarmsToHash(data.data.rows, (hash) => {
                    let sensors = setValue(null, sensorsParam, hash);
                    if (position === SENSOR_POSITION_OUT) {
                        _this.setState({
                            outerSensors: sensors
                        });
                    } else if (position === SENSOR_POSITION_IN) {
                        let innerSensors = deepcopy(_this.state.innerSensors);
                        innerSensors[key].data = sensors;
                        _this.setState({innerSensors});
                    }
                });
            } else if (status === 404) {

            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    generateAlarmsToHash = (alarms, callback) => {
        let hash = {};
        if (alarms.length) {
            for (let i = 0; i < alarms.length; i++) {
                if (hash[alarms[i].sensor_id] === undefined) {
                    hash[alarms[i].sensor_id] = alarms[i];
                } else {
                    if (Array.isArray(hash[alarms[i].sensor_id])) {
                        hash[alarms[i].sensor_id].push(alarms[i]);
                    } else {
                        let temp = Object.assign({}, hash[alarms[i].sensor_id]);
                        hash[alarms[i].sensor_id] = [];
                        hash[alarms[i].sensor_id].push(temp);
                        hash[alarms[i].sensor_id].push(alarms[i]);
                    }
                }
            }
        }
        this.alarmHash = hash;
        if (callback) callback(hash);
    };

    render() {
        return (
            <article id="lcModalSensorGatherWrap"
                     className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalSensorGather" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">센서 모아보기</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    {
                                        this.state.outerSensors && this.state.outerSensors.length ?
                                            (
                                                <section className="sensors-gather-wrap">
                                                    <h4>외부센서</h4>
                                                    <ul className="sensorGatherList">
                                                        {
                                                            this.state.outerSensors.map((item, index) => {
                                                                return (
                                                                    <li className="sensor-gather-list-item" key={index}>
                                                                        <article className="sensor-info-wrap">
                                                                            <div
                                                                                className="sensor-info sensor-name-wrap">
                                                                                <span>{item.sensor_name}</span></div>
                                                                            <div
                                                                                className="sensor-info sensor-value-wrap">
                                                                            <span
                                                                                className={item.isWarning ? "lc-warning" : ""}>{sanitize(item)}</span>
                                                                            </div>
                                                                        </article>
                                                                    </li>
                                                                )
                                                            })
                                                        }
                                                    </ul>
                                                </section>
                                            ) : null
                                    }
                                    {
                                        this.state.innerSensors && Object.keys(this.state.innerSensors).length ?
                                            (
                                                <Fragment>
                                                    {
                                                        Object.keys(this.state.innerSensors).map((key, index) => {
                                                            return (
                                                                <section className="sensors-gather-wrap" key={key}>
                                                                    <h4>{this.state.innerSensors[key].house_name} 내부센서</h4>
                                                                    {
                                                                        this.state.innerSensors[key].data && this.state.innerSensors[key].data.length ?
                                                                            <ul className="sensorGatherList">
                                                                                {
                                                                                    this.state.innerSensors[key].data.map((item, index) => {
                                                                                        return (
                                                                                            <li className="sensor-gather-list-item" key={index}>
                                                                                                <article className="sensor-info-wrap">
                                                                                                    <div className="sensor-info sensor-name-wrap">
                                                                                                        <span>{item.sensor_name}</span>
                                                                                                    </div>
                                                                                                    <div className="sensor-info sensor-value-wrap">
                                                                                                        <span className={item.isWarning ? "lc-warning" : ""}>{sanitize(item)}</span>
                                                                                                    </div>
                                                                                                </article>
                                                                                            </li>
                                                                                        )
                                                                                    })
                                                                                }
                                                                            </ul> :
                                                                            <p className="no-sensor-p">표시 가능한 센서가 없습니다.</p>
                                                                    }
                                                                </section>
                                                            )
                                                        })
                                                    }
                                                </Fragment>
                                            ) : null
                                    }
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useSensorGather(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSensorGather))));