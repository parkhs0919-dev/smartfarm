import React, {Component} from 'react';
import {useScreenSetting} from "../../contexts/modals/screenSetting";
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import CONSTANT from '../../constants/constant';
import TRANSLATE from '../../constants/translate';
import deepcopy from '../../../../utils/deepcopy';
import {on} from "../../utils/socket";
import screenManager from '../../managers/screen';
import cctvManager from "../../managers/cctv";
import controlManager from "../../managers/control";
import sensorManager from "../../managers/sensor";
import {setValue} from "../../utils/sensorHandler";
import {noSession} from "../../utils/session";

const TYPE_MOTOR = "motor";
const TYPE_POWER = "power";
const OUTER_SENSOR_POSITION = "out";
const INNER_SENSOR_POSITION = "in";
const SCREEN_TRANSLATE = TRANSLATE.SCREEN;
const CAN_MODIFY_ORDER = CONSTANT.canModifyChild;

class ModalScreenSetting extends Component {
    constructor(props) {
        super(props);

        this.state = {
            screens: [],
            cctvs: [],
            innerSensors: [],
            outerSensors: [],
            motors: [],
            powers: [],
            cctvHasChange: false,
            innerSensorHasChange: false,
            outerSensorHasChange: false,
            motorHasChange: false,
            powerHasChange: false
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = () => {
        this.findAllScreens();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    listener = (socket) => {
        const _this = this;
        on(socket)('screens', (data) => {
            if(data && data.house_id && data.house_id === _this.state.house_id) {
                if(_this.props.isVisible) _this.findAllScreens();
            }
            if(!data || data.house_id === undefined) {
                if(_this.props.isVisible) _this.findAllScreens();
            }
        });

        on(socket)('cctvs', () => {
            if(_this.props.isVisible) _this.findAllCCTVs();
        });
        on(socket)('motors', () => {
            if(_this.props.isVisible) _this.findAllMotors();
        });
        on(socket)('powers', () => {
            if(_this.props.isVisible) _this.findAllPowers();
        });
        on(socket)('outer-sensor-values', () => {
            if(_this.props.isVisible) _this.findAllOuterSensors()
        });
        on(socket)('inner-sensor-value', () => {
            if(_this.props.isVisible) _this.findAllInnerSensors()
        });

    };

    close = (e) => {
        if (e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    findAllScreens = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId()
        };
        screenManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    screens: data.data.rows
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    clickCheckBox = (e, item, index) => {
        let screens = deepcopy(this.state.screens);
        screens[index].is_visible = !screens[index].is_visible;
        this.setState({screens});
    };

    changeOrder = (e, index, direction) => {
        e && e.preventDefault();

        let screens = deepcopy(this.state.screens);
        if (direction === "down") {
            screens.splice(index + 1, 0, screens.splice(index, 1)[0]);
        } else if (direction === "up") {
            screens.splice(index - 1, 0, screens.splice(index, 1)[0]);
        }
  
        this.setState({screens});
    };

    updateScreen = async () => {
        const _this = this;

        let promises = [];

        promises.push(new Promise((resolve, reject) => {
            let screens = deepcopy(this.state.screens);
            let body = {
                screens: screens,
                house_id: _this.props.getHouseId(),
            };
            screenManager.updateScreenOrder(body, noSession(this.props.showDialog)((status, data) => {
                if(status === 200) {
                    _this.props.setHouseId(this.props.getHouseId());
                } else {
                    _this.props.alertError(status, data);
                }
                resolve(true);
            }));
        }));

        promises.push(new Promise((resolve, reject) => {
            if (this.state.cctvHasChange) {
                let cctvs = deepcopy(this.state.cctvs);
                cctvManager.renameCCTVs({cctvs: cctvs}, noSession(this.props.showDialog)((status, data) => {
                    _this.setState({
                        cctvHasChange: false
                    });
                    if(status !== 200) {
                        _this.props.alertError(status, data);
                    }
                    resolve(true);
                }));
            } else {
                resolve(true);
            }
        }));

        promises.push(new Promise((resolve, reject) => {
            if (this.state.motorHasChange) {
                let motors = deepcopy(this.state.motors);
                controlManager.updateAll({motors: motors}, noSession(this.props.showDialog)((status, data) => {
                    _this.setState({
                        motorHasChange: false
                    });
                    if(status !== 200) {
                        _this.props.alertError(status, data);
                    }
                    resolve(true);
                }));
            } else {
                resolve(true);
            }
        }));

        promises.push(new Promise((resolve, reject) => {
            if (this.state.powerHasChange) {
                let powers = deepcopy(this.state.powers);
                controlManager.updateAll({powers: powers}, noSession(this.props.showDialog)((status, data) => {
                    _this.setState({
                        powerHasChange: false
                    });
                    if(status !== 200) {
                        _this.props.alertError(status, data);
                    }
                    resolve(true);
                }));
            } else {
                resolve(true);
            }
        }));

        promises.push(new Promise((resolve, reject) => {
            if (this.state.innerSensorHasChange) {
                let sensors = deepcopy(this.state.innerSensors);
                sensorManager.updateAll({sensors}, noSession(this.props.showDialog)((status, data) => {
                    _this.setState({
                        innerSensorHasChange: false
                    });
                    if(status !== 200) {
                        _this.props.alertError(status, data);
                    }
                    resolve(true);
                }));
            } else {
                resolve(true);
            }
        }));

        promises.push(new Promise((resolve, reject) => {
            if (this.state.outerSensorHasChange) {
                let sensors = deepcopy(this.state.outerSensors);
                sensorManager.updateAll({sensors}, noSession(this.props.showDialog)((status, data) => {
                    _this.setState({
                        outerSensorHasChange: false
                    });
                    if(status !== 200) {
                        _this.props.alertError(status, data);
                    }
                    resolve(true);
                }));
            } else {
                resolve(true);
            }
        }));

        await Promise.all(promises).then(() => {
            _this.props.showDialog({
                alertText: '적용됐습니다.'
            });
        });
    };

    toggleMenu = (e, item, index) => {
        let screens = deepcopy(this.state.screens);
        screens[index].isTabOpen = !screens[index].isTabOpen;
        this.setState({
            screens: screens
        });
        if (item.type === "cctv" && screens[index].isTabOpen) {
            this.findAllCCTVs();
        } else if (item.type === "motor" && screens[index].isTabOpen) {
            this.findAllMotors();
        } else if (item.type === "power" && screens[index].isTabOpen) {
            this.findAllPowers();
        } else if (item.type === "outerSensor" && screens[index].isTabOpen) {
            this.findAllOuterSensors();
        } else if (item.type === "innerSensor" && screens[index].isTabOpen) {
            this.findAllInnerSensors();
        }
    };

    changeChildOrder = (e, type, index, direction) => {
        e && e.preventDefault();
        let listItem = null;
        if (type === "cctv") {
            listItem = deepcopy(this.state.cctvs);
        } else if (type === "motor") {
            listItem = deepcopy(this.state.motors);
        } else if (type === "power") {
            listItem = deepcopy(this.state.powers);
        } else if (type === "outerSensor") {
            listItem = deepcopy(this.state.outerSensors);
        } else if (type === "innerSensor") {
            listItem = deepcopy(this.state.innerSensors);
        }
        if (direction === "down") {
            listItem.splice(index + 1, 0, listItem.splice(index, 1)[0]);
        } else if (direction === "up") {
            listItem.splice(index - 1, 0, listItem.splice(index, 1)[0]);
        }

        if (type === "cctv") {
            this.setState({
                cctvs: listItem,
                cctvHasChange: true
            });
        } else if (type === "motor") {
            this.setState({
                motors: listItem,
                motorHasChange: true,
            });
        } else if (type === "power") {
            this.setState({
                powers: listItem,
                powerHasChange: true,
            });
        } else if (type === "outerSensor") {
            this.setState({
                outerSensors: listItem,
                outerSensorHasChange: true,
            });
        } else if (type === "innerSensor") {
            this.setState({
                innerSensors: listItem,
                innerSensorHasChange: true,
            });
        }
    };

    generateChildContent = (type) => {
        let item = null;
        if (type === "cctv") {
            return (
                <ul className="child-content-list">
                    {
                        this.state.cctvs.map((item, index) => {
                            return (
                                <li className="child-content-list-item" key={index}>
                                    <article className="screen-setting-child-wrap">
                                        <div className="screen-setting-left-wrap">
                                            <span>{item.cctv_name}</span>
                                        </div>
                                        <div className="screen-setting-right-wrap">
                                            {
                                                index !== this.state.cctvs.length - 1 &&
                                                (
                                                    <button className="order-btn order-down"
                                                            onClick={e => this.changeChildOrder(e, type, index, 'down')}/>
                                                )
                                            }
                                            {
                                                index !== 0 &&
                                                (
                                                    <button className="order-btn order-up"
                                                            onClick={e => this.changeChildOrder(e, type, index, 'up')}/>
                                                )
                                            }
                                        </div>
                                    </article>
                                </li>
                            )
                        })
                    }
                </ul>
            );
        } else if (type === "motor") {
            return (
                <ul className="child-content-list">
                    {
                        this.state.motors.map((item, index) => {
                            return (
                                <li className="child-content-list-item" key={index}>
                                    <article className="screen-setting-child-wrap">
                                        <div className="screen-setting-left-wrap">
                                            <span>{item.control_name}</span>
                                        </div>
                                        <div className="screen-setting-right-wrap">
                                            {
                                                index !== this.state.motors.length - 1 &&
                                                (
                                                    <button className="order-btn order-down"
                                                            onClick={e => this.changeChildOrder(e, type, index, 'down')}/>
                                                )
                                            }
                                            {
                                                index !== 0 &&
                                                (
                                                    <button className="order-btn order-up"
                                                            onClick={e => this.changeChildOrder(e, type, index, 'up')}/>
                                                )
                                            }
                                        </div>
                                    </article>
                                </li>
                            )
                        })
                    }
                </ul>
            );
        } else if (type === "power") {
            return (
                <ul className="child-content-list">
                    {
                        this.state.powers.map((item, index) => {
                            return (
                                <li className="child-content-list-item" key={index}>
                                    <article className="screen-setting-child-wrap">
                                        <div className="screen-setting-left-wrap">
                                            <span>{item.control_name}</span>
                                        </div>
                                        <div className="screen-setting-right-wrap">
                                            {
                                                index !== this.state.powers.length - 1 &&
                                                (
                                                    <button className="order-btn order-down"
                                                            onClick={e => this.changeChildOrder(e, type, index, 'down')}/>
                                                )
                                            }
                                            {
                                                index !== 0 &&
                                                (
                                                    <button className="order-btn order-up"
                                                            onClick={e => this.changeChildOrder(e, type, index, 'up')}/>
                                                )
                                            }
                                        </div>
                                    </article>
                                </li>
                            )
                        })
                    }
                </ul>
            )
        } else if (type === "outerSensor") {
            return (
                <ul className="child-content-list">
                    {
                        this.state.outerSensors.map((item, index) => {
                            return (
                                <li className="child-content-list-item" key={index}>
                                    <article className="screen-setting-child-wrap">
                                        <div className="screen-setting-left-wrap">
                                            <span>{item.sensor_name}</span>
                                        </div>
                                        <div className="screen-setting-right-wrap">
                                            {
                                                index !== this.state.outerSensors.length - 1 &&
                                                (
                                                    <button className="order-btn order-down"
                                                            onClick={e => this.changeChildOrder(e, type, index, 'down')}/>
                                                )
                                            }
                                            {
                                                index !== 0 &&
                                                (
                                                    <button className="order-btn order-up"
                                                            onClick={e => this.changeChildOrder(e, type, index, 'up')}/>
                                                )
                                            }
                                        </div>
                                    </article>
                                </li>
                            )
                        })
                    }
                </ul>
            )
        } else if (type === "innerSensor") {
            return (
                <ul className="child-content-list">
                    {
                        this.state.innerSensors.map((item, index) => {
                            return (
                                <li className="child-content-list-item" key={index}>
                                    <article className="screen-setting-child-wrap">
                                        <div className="screen-setting-left-wrap">
                                            <span>{item.sensor_name}</span>
                                        </div>
                                        <div className="screen-setting-right-wrap">
                                            {
                                                index !== this.state.innerSensors.length - 1 &&
                                                (
                                                    <button className="order-btn order-down"
                                                            onClick={e => this.changeChildOrder(e, type, index, 'down')}/>
                                                )
                                            }
                                            {
                                                index !== 0 &&
                                                (
                                                    <button className="order-btn order-up"
                                                            onClick={e => this.changeChildOrder(e, type, index, 'up')}/>
                                                )
                                            }
                                        </div>
                                    </article>
                                </li>
                            )
                        })
                    }
                </ul>
            )
        }
    };

    findAllCCTVs = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId()
        };
        cctvManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    cctvs: data.data.rows
                });
            } else if (status === 404) {
                _this.setState({
                    cctvs: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findAllMotors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            type: TYPE_MOTOR,
        };
        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    motors: data.data.rows,
                });
            } else if (status === 404) {
                _this.setState({
                    motors: [],
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findAllPowers = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            type: TYPE_POWER,
        };
        controlManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    powers: data.data.rows,
                });
            } else if (status === 404) {
                _this.setState({
                    powers: [],
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findAllInnerSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            position: INNER_SENSOR_POSITION,
        };

        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    innerSensors: data.data.rows
                });
            } else if (status === 404) {
                _this.setState({
                    innerSensors: [],
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    findAllOuterSensors = () => {
        const _this = this;
        let query = {
            house_id: this.props.getHouseId(),
            position: OUTER_SENSOR_POSITION,
        };

        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
                _this.setState({
                    outerSensors: data.data.rows
                });
            } else if (status === 404) {
                _this.setState({
                    outerSensors: [],
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    render() {
     
        return (
            <article id="lcModalScreenSettingWrap"
                     className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalScreenSetting" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">화면설정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <ul id="screenOrderList">
                                        {
                                            this.state.screens.map((item, index) => {
                                                return (
                                                    <li className="screen-order-list-item" key={index}>
                                                        <article
                                                            className={"screen-setting-wrap" + (item.isTabOpen ? ' lc-open' : '')}>
                                                            <div className="screen-setting-left-wrap">
                                                                <div className="lc-theme-checkbox-wrap">
                                                                    <input id="mixControlCondition1"
                                                                           value="time"
                                                                           checked={item.is_visible}
                                                                           onChange={() => {
                                                                           }} type="checkbox"
                                                                           name="mix-control-condition"/>
                                                                    <label
                                                                        htmlFor="mixControlCondition1"
                                                                        onClick={(e) => this.clickCheckBox(e, item, index)}
                                                                        className="box-shape-label"/>
                                                                    <label
                                                                        htmlFor="mixControlCondition1"
                                                                        onClick={(e) => this.clickCheckBox(e, item, index)}
                                                                        className="text-label">{SCREEN_TRANSLATE[item.type]}</label>
                                                                </div>
                                                                {
                                                                    CAN_MODIFY_ORDER[item.type] ?
                                                                        (
                                                                            <button
                                                                                className={"open-menu-btn" + (item.isTabOpen ? ' lc-open' : '')}
                                                                                onClick={e => this.toggleMenu(e, item, index)}/>
                                                                        ) : null
                                                                }
                                                            </div>

                                                            <div className="screen-setting-right-wrap">
                                                                {
                                                                    index !== this.state.screens.length - 1 &&
                                                                    (
                                                                        <button className="order-btn order-down"
                                                                                onClick={e => this.changeOrder(e, index, 'down')}/>
                                                                    )
                                                                }
                                                                {
                                                                    index !== 0 &&
                                                                    (
                                                                        <button className="order-btn order-up"
                                                                                onClick={e => this.changeOrder(e, index, 'up')}/>
                                                                    )
                                                                }
                                                            </div>
                                                        </article>
                                                        {
                                                            CAN_MODIFY_ORDER[item.type] && item.isTabOpen ?
                                                                (
                                                                    <article className="lc-screen-menu-child-wrap">
                                                                        {this.generateChildContent(item.type)}
                                                                    </article>
                                                                ) : null
                                                        }
                                                    </li>
                                                )
                                            })
                                        }
                                    </ul>

                                    <button className="theme-button" onClick={this.updateScreen}>적용</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        setHouseId: actions.setHouseId,
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useScreenSetting(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalScreenSetting)));
