import React, { Component } from 'react';
import { useSensorGroupDetail } from "../../contexts/modals/sensorgroupDetail";
import { useAlert } from "../../contexts/alert";
import { useSocket } from '../../contexts/socket'
import CONSTANT from "../../constants/constant";
import TRANSLATE from '../../constants/translate';
import { colourStyles } from "../../../farm/utils/select";
import Select from 'react-select';
import sensorGroupmanager from '../../managers/sensorgroup'
import deepcopy from "../../../../utils/deepcopy";
import { noSession } from "../../utils/session";

const TYPE = CONSTANT.sensorGroup.Type;
const SENSOR_TYPE_TRANSLATE = TRANSLATE.SENSOR;
const DEFAULT_VALUE = {
    house_id:'',
    types: {},        
    position:'',
    formtype:{},    
    sensor:[],
};

class ModalSensorGroupAdd extends Component {
    constructor(props) {
        super(props);        
        this.moreMenuRefs = new Map();
        this.state = {
            house_id:'',
            types: {},        
            position:'',
            formtype:{},    
            sensor:[],
        };
    }

    componentDidMount() {
        this.props.sync(this.init);

    }




    init = (data) => {        
        let state = Object.assign({}, data);
       
        state.position = data.position;        
        
        this.setState(state,()=>{
            this.findType();
        })

        document.addEventListener('keyup', this.closeOnEscape);
    };
    findType=()=>{
        let position ='';
        if(this.state.position =='outer'){
            position = 'out';
        }else if (this.state.position =='inner'){
            position ='in';
        }        
        let body = {
            id: this.state.types.id,
            house_id: this.state.house_id,
            type: this.state.formtype.value,
            position:position,
        }        
        
        sensorGroupmanager.getsensortype(body, noSession(this.props.showDialog)((status, data) => {
            
            if (status === 200) {                                
                if (data.data.sensor_avg_items.length > 0) {
                    this.setState({
                        sensor: data.data.sensor_avg_items
                    })
                } else {
        
                    this.setState({
                        sensor: []
                    })
                }
            } else {
                this.props.alertError(status, data);
            }
        }));
    }
    
    close = (e) => {
        if (e) e.preventDefault();
        this.setState(DEFAULT_VALUE);
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    submit = (e) => {
        e.preventDefault();
        const _this = this;
        let state = Object.assign({}, this.state);
        let body = {};
        let items = [];
        if (!state.types.name) {
            return this.props.showDialog({
                alertText: "그룹명을 입력해 주세요."
            });
        }
        
        for (let i = 0; i < state.sensor.length; i++) {
            if (state.sensor[i].avg_sensor_id&&state.sensor[i].avg_sensor_id.length!=0) {                
                items.push(state.sensor[i].id)
            }
        }
        if (items.length == 0) {
            return this.props.showDialog({
                alertText: "센서를 선택해 주세요."
            });
        }
        body.id = state.types.id;
        body.name = state.types.name;
        body.items = items.join(',');
        body.house_id = state.house_id;        
        sensorGroupmanager.update(body, noSession(this.props.showDialog)((status, data) => {
            
            if (status === 200) {
                _this.props.showDialog({
                    alertText: "센서 그룹 설정이 수정되었습니다.",
                    cancelCallback: () => {
                        _this.close();
                    }
                })
            } else {
                this.props.alertError(status, data);
            }
        }));
    };

    handleSelect = (option, key) => {
        let temp = Object.assign({}, this.state);
        temp[key] = option;
        this.setState(temp);
    };

    handleGroupName = (e,key) => {
        let types = Object.assign({}, this.state.types);        
        types[key] = e.target.value;
        this.setState({types})
    }

    afterAlarmSetting = () => {
        document.addEventListener('keyup', this.closeOnEscape);
    };

    checksensorDetail = (e, rows,index) => {
        let sensor = deepcopy(this.state.sensor);
        
        if(sensor[index].avg_sensor_id && sensor[index].avg_sensor_id.length!=0){
            sensor[index].avg_sensor_id =''
        }else{
            sensor[index].avg_sensor_id ='1'
        }
        this.setState({ sensor })
            
    };

    
    render() {  
           
        return (
            <article id="lcModalSensorAlarmWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalSensorGroup" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">{SENSOR_TYPE_TRANSLATE[this.state.position]}그룹 설정 수정</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>

                                <section className="modal-body">

                                    <React.Fragment>
                                        <div className="sensorGroup-register-wrap">
                                            <div id="sensorGroupWrap" className="sensorGroup-section">
                                                <label className="sensorGroup-form-label">센서 타입</label>
                                                <ul className="sensorGroup-wrap">

                                                    <li className="sensorGroup-item">
                                                        <div className="select-module-wrap">
                                                            <Select
                                                                value={this.state.formtype}
                                                                placeholder="센서 선택"
                                                                onChange={option => this.handleSelect(option, "type")}
                                                                isSearchable={false}
                                                                options={TYPE}
                                                                styles={colourStyles} 
                                                                isDisabled={true}/>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div className="sensorGroup-register-wrap">
                                            <form onSubmit={this.submit}>

                                                <div id="sensorGroupSettingWrap" className="sensorGroup-section">
                                                    <div className="sensorGroup-setting-form">
                                                        <label className="sensorGroup-form-label">그룹명</label>
                                                        <div className="lc-setting-form-wrap">
                                                            <div className="form-flex-wrap">
                                                                <div className="left-item select-module-wrap lc-size-36">
                                                                    <input type="text" value={this.state.types.name}
                                                                        onChange={(e) => this.handleGroupName(e,'name')} className="lc-farm-input"
                                                                        placeholder="그룹명" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="delay-recount-wrap">
                                                            <label className="sensorGroup-form-label">센서 선택</label>
                                                        </div>
                                                        <div className="delay-recount-wrap">
                                                            <ul id="sensorGroupDetailList">
                                                                {
                                                                    this.state.sensor && this.state.sensor.length ?
                                                                        this.state.sensor.map((rows, index) => {
                                                                            return (
                                                                                <li className="sensorGroup-detail-list-item" key={index}>
                                                                                    <article className="check-wrap">
                                                                                        <div className="lc-theme-checkbox-wrap">
                                                                                            <input
                                                                                                checked={rows.avg_sensor_id ? true : false}
                                                                                                id={rows.id}
                                                                                                onChange={() => { }} type="checkbox"
                                                                                                name="groupDetailWay" />
                                                                                            <label htmlFor="isBar"
                                                                                                onClick={(e) => this.checksensorDetail(e, rows,index)}
                                                                                                className="box-shape-label" />
                                                                                            <label htmlFor="isBar"
                                                                                                onClick={(e) => this.checksensorDetail(e, rows,index)}
                                                                                                className="text-label">{rows.sensor_name}</label>
                                                                                        </div>
                                                                                    </article>
                                                                                </li>
                                                                            )
                                                                        }) : <p>그룹 설정이 가능한 센서가 없습니다.</p>
                                                                }
                                                            </ul>
                                                        </div>
                                                    </div>


                                                </div>
                                                <div className="delay-recount-wrap">
                                                    <button className="theme-button" type="submit">등록</button>
                                                </div>
                                            </form>
                                        </div>
                                    </React.Fragment>

                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }

}

export default useSocket(
    ({ state, actions }) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSensorGroupDetail(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSensorGroupAdd)));
