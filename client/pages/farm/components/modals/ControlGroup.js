import React, {Component, Fragment} from 'react';
import Select from 'react-select';
import {useAlert} from "../../contexts/alert";
import {useHouse} from "../../contexts/house";
import {useControlGroup} from "../../contexts/modals/controlGroup";
import {useControlGroupSetting} from "../../contexts/modals/controlGroupSetting";
import {colourStyles} from "../../utils/select";
import deepcopy from "../../../../utils/deepcopy";
import controlGroupManager from "../../managers/controlGroup";
import {noSession} from "../../utils/session";

const TYPE_MOTOR = 'motor';
const TYPE_POWER = 'power';

const DEFAULT_STATE = {
    house_id : '',
    controlGroups : [],
};

class ControlGroup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            house_id : this.props.getHouseId(),
            controlGroups : [],
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        document.addEventListener('keyup', this.closeOnEscape);

        if(data) {
            this.setState(data);
            this.getControlGroups(data.type);
        }else{
            this.getControlGroups();
        }


    };

    getControlGroups = async (type) => {
        const _this = this;

        let where = {
            house_id : this.props.getHouseId(),
            type : type ? type : this.state.type,
        };

        await controlGroupManager.findAll(where, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    controlGroups : data.data.rows,
                });
            }else if(status === 404) {
                _this.setState({
                    controlGroups : []
                })
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    close = (e) => {
        if(e) e.preventDefault();
        this.setState(deepcopy(DEFAULT_STATE));
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    openControlGroupSettingModal = (e) => {
        let data = {type : this.state.type};
        this.props.openControlGroupSettingModal(data, this.modalClose);
    };

    updateControlGroupSettingModal = (e, index) => {
        let controlGroups = deepcopy(this.state.controlGroups);
        let data = controlGroups[index];

        this.props.openControlGroupSettingModal(data, this.modalClose);
    };

    removeControlGroupSetting = (e, index) => {
        const _this = this;
        let controlGroups = deepcopy(this.state.controlGroups);
        let controlGroup = controlGroups[index];
        this.props.show({
            alertText: `${this.state.type === TYPE_MOTOR ? '모터' : '전원'} 그룹제어 설정을<br/> 삭제하시겠습니까?`,
            cancelText: "취소",
            submitText: "삭제",
            submitCallback: () => {
                controlGroupManager.remove(controlGroup.id, (status, data) => {
                    if(status === 200) {
                        _this.props.showDialog({
                            alertText: "삭제되었습니다.",
                        });
                        this.getControlGroups();
                    } else {
                        _this.props.alertError(status, data);
                    }
                });
            }
        });
    };

    clickOption = (e, value, index) => {
        const _this = this;
        let controlGroups = deepcopy(this.state.controlGroups);
        controlGroups[index].on_off_state = value;
        let data = {
            id : controlGroups[index].id,
            house_id : controlGroups[index].house_id,
            type : controlGroups[index].type,
            on_off_state : value,
            controlGroupItems : controlGroups[index].controlGroupItems
        };

        controlGroupManager.update(data, (status, data) => {
            if(status !== 200){
                _this.props.alertError(status, data);
            }else{
                _this.setState({controlGroups});
            }
        });
    };

    modalClose = () => {
        this.getControlGroups();
    };

    render() {
        return (
            <article id="lcModalControlGroupWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalControlGroup" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">{`${this.state.type === TYPE_MOTOR ? '모터' : '전원'} 그룹 제어 설정`}</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>
                                <section className="modal-body">
                                    <div className="button-wrap">
                                        <button className='theme-text-button' onClick={e => this.openControlGroupSettingModal(e)}>+ 그룹제어 설정 추가</button>
                                    </div>
                                    <div className="control-group-list-wrap">
                                        <ul>
                                            {this.state.controlGroups.length ?
                                                this.state.controlGroups.map((data, index) => {
                                                    return (
                                                        <li key={index}>
                                                            <p className="lc-control-group-name-wrap">
                                                                <span className="lc-control-group-name-index">{index+1}</span>
                                                                <span className="lc-control-group-name">{data.control_group_name.length > 10 ? `${data.control_group_name.substr(0, 10)}...` : data.control_group_name}</span>
                                                            </p>
                                                            <div className={"control-group-option"}>
                                                                <div className="toggle-radio-wrap two-state">
                                                                    <input type="radio" name={`control-group-toggle-option-${index}`} onChange={e => {}}
                                                                           checked={data.on_off_state ==='on'} id={`remoteToggleFirst`}/>
                                                                    <input type="radio" name={`control-group-toggle-option-${index}`} onChange={e => {}}
                                                                           checked={data.on_off_state==='off'} id={`remoteToggleSecond`} />
                                                                    <label onClick={e => this.clickOption(e, 'on', index)}>ON</label>
                                                                    <label onClick={e => this.clickOption(e, 'off', index)}>OFF</label>
                                                                    <i className="toggle-slider-icon"/>
                                                                </div>
                                                                <div className="three-dot-menu" tabIndex="0">
                                                                    <div className="icon-wrap">
                                                                        <i/><i/><i/>
                                                                    </div>
                                                                    <div className="three-dot-view-more-menu">
                                                                        <ul>
                                                                            <li onClick={e => this.updateControlGroupSettingModal(e, index)}><span>수정</span></li>
                                                                            <li onClick={e => this.removeControlGroupSetting(e, index)}><span>삭제</span></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    )
                                                })
                                                : <div className='empty-item'>
                                                    <p>등록된 그룹설정이 없습니다.</p>
                                                </div>
                                            }
                                        </ul>
                                    </div>
                                </section>
                            </div> : null
                        }
                    </div>
                </div>
            </article>
        );
    }
}

export default useControlGroup(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(useControlGroupSetting(
    ({actions}) => ({
        openControlGroupSettingModal: actions.open
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        show: actions.show,
        alertError: actions.alertError
    })
)(useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(ControlGroup))));
