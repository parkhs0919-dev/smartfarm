import React, {Component} from 'react';

class ButtonDatePicker extends Component {

    componentDidMount() {
    }
    render () {
        return (
            <button
                className="lc-farmlabs-datepicker"
                onClick={this.props.onClick}>
                {this.props.value ? this.props.value : this.props.placeholderText}
            </button>
        )
    }
}

export default ButtonDatePicker;