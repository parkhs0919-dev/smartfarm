import React, {Component, Fragment} from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";

import moment from 'moment';
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4_lang_ko from  '@amcharts/amcharts4/lang/ko_KR';
import "core-js/shim";
import deepcopy from '../../../utils/deepcopy';
import {date} from "../../../utils/filter";

am4core.useTheme(am4themes_animated);

am4_lang_ko.January = "1 /";
am4_lang_ko.February = "2 /";
am4_lang_ko.March = "3 /";
am4_lang_ko.April = "4 /";
am4_lang_ko.May = "5 /";
am4_lang_ko.June = "6 /";
am4_lang_ko.July = "7 /";
am4_lang_ko.August = "8 /";
am4_lang_ko.September = "9 /";
am4_lang_ko.October = "10 /";
am4_lang_ko.November = "11 /";
am4_lang_ko.December = "12 /";

am4_lang_ko.Jan = "1 /";
am4_lang_ko.Feb = "2 /";
am4_lang_ko.Mar = "3 /";
am4_lang_ko.Apr = "4 /";
am4_lang_ko['May(short)'] = "5 /";
am4_lang_ko.Jun = "6 /";
am4_lang_ko.Jul = "7 /";
am4_lang_ko.Aug = "8 /";
am4_lang_ko.Sep = "9 /";
am4_lang_ko.Oct = "10 /";
am4_lang_ko.Nov = "11 /";
am4_lang_ko.Dec = "12 /";

class LcChart extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        this.drawChart();
    }

    drawChart = () => {
        if(this.props.chartType === "windDirection" ||this.props.chartType=='korinsWindDirection') {
            // this.drawWindDirectionChart();
            this.drawStandardChart(0, 360);
        } else if (this.props.chartType === "rain" || this.props.chartType === "water" || this.props.chartType === "power"||this.props.chartType === "window"||this.props.chartType === "fire"||this.props.chartType === "door") {
            if(this.props.sensorZoom){
                this.drawTwoValueChart();
            } else {
                this.drawTwoValueChart();
            }
        }else if(this.props.chartType === 'boiler' || this.props.chartType === 'vantilation'){
            if(this.props.sensorZoom){
                this.drawTemperatureChart();
            } else {
                this.drawTemperatureChart();
            }
        }else {
            if(this.props.sensorZoom){
                this.drawStandardChart();
            } else {
                this.drawStandardChart();
            }
        }
    };

    drawStandardChart = (min, max) => {
        let chart = am4core.create(this.props.chartId, am4charts.XYChart);
        chart.language.locale = am4_lang_ko;
        chart.data = this.generateChartData().generatedData;
        let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.minGridDistance = 60;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.grid.template.disabled = true;
        let series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "value";
        series.dataFields.dateX = "date";
        series.tooltipText = "{value}";
        series.propertyFields.stroke = "color";

        if(min&&max) valueAxis.strictMinMax = true;
        if(min) valueAxis.min = min;
        if(max) valueAxis.max = max;
        chart.cursor = new am4charts.XYCursor();
        // chart.cursor.snapToSeries = series;
        chart.cursor.xAxis = dateAxis;

        if(!this.props.noChartDetailControl) {
            chart.exporting.menu = new am4core.ExportMenu();
            chart.exporting.menu.align = "right";
            chart.exporting.menu.items = [{
                "label": "...",
                "menu": [{
                    "label": "Image",
                    "menu": [
                        { "type": "png", "label": "PNG" },
                        { "type": "jpg", "label": "JPG" },
                    ]
                }]
            }];
            if(this.props.modalSlideBar && this.props.sensorZoom){
                chart.scrollbarX = new am4core.Scrollbar();
                chart.scrollbarX.marginLeft = 0;
            }
            if(this.props.modalSlideBar && !this.props.sensorZoom){
                chart.scrollbarY = new am4core.Scrollbar();
                chart.scrollbarY.marginLeft = 0;
            }
            // chart.scrollbarX = new am4core.Scrollbar();
        }

        if(this.props.sensorZoom){
            chart.cursor.behavior = "zoomX";
            chart.cursor.lineY.disabled = true;
        }
        if(!this.props.sensorZoom){
            chart.cursor.behavior = "zoomY";
            chart.cursor.lineX.disabled = true;
        }

    };

    drawTemperatureChart = () => {
        const _this = this;
        let chart = am4core.create(this.props.chartId, am4charts.XYChart);
        chart.language.locale = am4_lang_ko;
        let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.minGridDistance = 60;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.grid.template.disabled = true;

        var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis2.renderer.opposite = true;

        let series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "value";
        series.dataFields.dateX = "date";
        series.tooltipText = "{tooltipText}";
        series.propertyFields.stroke = "color";
        series.data = this.generateChartData().generatedData;

        var series2 = chart.series.push(new am4charts.LineSeries());
        series2.dataFields.valueY = "value";
        series2.dataFields.dateX = "date";
        series2.tooltipText = "{tooltipText}";
        series2.yAxis = valueAxis2;
        series2.data = this.generateChartData().generatedData2;

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.snapToSeries = series;
        chart.cursor.xAxis = dateAxis;

        if(this.props.chartType === 'boiler' || this.props.chartType === 'vantilation'){
            dateAxis.baseInterval = {
                "timeUnit": "minute",
                "count": 1
            };
        }

        if(this.props.sunDate){
            let range = dateAxis.createSeriesRange(series);
            range.date = this.props.sunDate.rise_time;
            range.endDate = this.props.sunDate.set_time;
            range.axisFill.fill = am4core.color("#396478");
            range.axisFill.fillOpacity = 0.3;
        }

        if(!this.props.noChartDetailControl) {
            chart.exporting.menu = new am4core.ExportMenu();
            chart.exporting.menu.align = "right";
            chart.exporting.menu.items = [{
                "label": "...",
                "menu": [{
                    "label": "Image",
                    "menu": [
                        { "type": "png", "label": "PNG" },
                        { "type": "jpg", "label": "JPG" },
                    ]
                }]
            }];
            chart.scrollbarX = new am4core.Scrollbar();
        }
        if(this.props.sensorZoom){
            chart.cursor.behavior = "zoomX";
            chart.cursor.lineY.disabled = true;
        } else {
            chart.cursor.behavior = "zoomY";
            chart.cursor.lineX.disabled = true;
        }
    }

    drawTwoValueChart = () => {
        const _this = this;
        let chart = am4core.create(this.props.chartId, am4charts.XYChart);
        chart.language.locale = am4_lang_ko;
        chart.data = this.generateChartData().generatedData;
        let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.minGridDistance = 60;
        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.grid.template.disabled = true;
        valueAxis.min = -1;
        valueAxis.max = 2;
        valueAxis.strictMinMax = true;
        let units = _this.props.sensorData.unit.split(":");
        valueAxis.renderer.labels.template.adapter.add("text", function(text) {
            if(text === "0") {
                return units[1];
            } else if(text === "1") {
                return units[0];
            } else {
                return '';
            }
        });

        let series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "value";
        series.dataFields.dateX = "date";
        series.tooltipText = "{tooltipText}";
        series.propertyFields.stroke = "color";

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.snapToSeries = series;
        chart.cursor.xAxis = dateAxis;

        if(!this.props.noChartDetailControl) {
            chart.exporting.menu = new am4core.ExportMenu();
            chart.exporting.menu.align = "right";
            chart.exporting.menu.items = [{
                "label": "...",
                "menu": [{
                    "label": "Image",
                    "menu": [
                        { "type": "png", "label": "PNG" },
                        { "type": "jpg", "label": "JPG" },
                    ]
                }]
            }];
            chart.scrollbarX = new am4core.Scrollbar();
        }
        if(this.props.sensorZoom){
            chart.cursor.behavior = "zoomX";
            chart.cursor.lineY.disabled = true;
        } else {
            chart.cursor.behavior = "zoomY";
            chart.cursor.lineX.disabled = true;
        }
    };

    drawWindDirectionChart = () => {
        const _this = this;
        let chart = am4core.create(this.props.chartId, am4charts.XYChart);
        chart.data = this.generateChartData().generatedData;
        // chart.exporting.menu.verticalAlign = "top";
        let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.minGridDistance = 60;
        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.grid.template.disabled = true;
        valueAxis.min = 0;
        valueAxis.max = 90;
        valueAxis.strictMinMax = true;
        let units = _this.props.sensorData.unit.split(":");
        valueAxis.renderer.labels.template.adapter.add("text", function(text) {
            return text;
            // if (text === "-200") {
            //     return units[0];
            // } else if (text === "-100") {
            //     return units[1];
            // } else if (text === "0") {
            //     return units[2];
            // } else if (text === "100") {
            //     return units[3];
            // } else if (text === "200") {
            //     return units[4];
            // } else if (text === "300") {
            //     return units[5];
            // } else if (text === "400") {
            //     return units[6];
            // } else if (text === "500") {
            //     return units[7];
            // }
        });
        let series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "value";
        series.dataFields.dateX = "date";
        series.tooltipText = "{value}";
        series.propertyFields.stroke = "color";

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.snapToSeries = series;
        chart.cursor.xAxis = dateAxis;
        if(!this.props.noChartDetailControl) {
            chart.exporting.menu = new am4core.ExportMenu();
            chart.exporting.menu.align = "right";
            chart.exporting.menu.items = [{
                "label": "...",
                "menu": [{
                    "label": "Image",
                    "menu": [
                        { "type": "png", "label": "PNG" },
                        { "type": "jpg", "label": "JPG" },
                    ]
                }]
            }];
            chart.scrollbarX = new am4core.Scrollbar();
        }
        if(this.props.sensorZoom){
            chart.cursor.behavior = "zoomX";
            chart.cursor.lineY.disabled = true;
        } else {
            chart.cursor.behavior = "zoomY";
            chart.cursor.lineX.disabled = true;
        }
    };

    generateChartData = () => {
        let generatedData = [];
        let generatedData2 = [];

        if(this.props.chartType === "windDirection" || this.props.chartType ==='korinsWindDirection') {
            let data = deepcopy(this.props.chartData);
            data.forEach(item => {
                let obj = {
                    color: "#f13b3b",
                };
                obj.value = item.value;
                obj.date = new Date(item.created_at);
                generatedData.push(obj);
            });
        } else if (this.props.chartType === "rain" || this.props.chartType === "water" || this.props.chartType === "power"||this.props.chartType === "window"||this.props.chartType === "fire"||this.props.chartType === "door" ) {
            let data = deepcopy(this.props.chartData);
            let units = this.props.sensorData.unit.split(":");
            data.forEach((item, index) => {
                let obj = {
                    color: "#f13b3b",
                };
                obj.value = item.value;
                obj.date = new Date(item.created_at);
                obj.tooltipText = item.value === 0 ? units[1] : units[0];
                generatedData.push(obj);
            });
        } else if(this.props.chartType === 'boiler' || this.props.chartType === 'vantilation'){
            let secondData = [];
            let data = deepcopy(this.props.chartData);
            if(this.props.secondData.length) secondData = deepcopy(this.props.secondData);

            data.forEach((item, index) => {
                let obj = {
                    color: "#f13b3b",
                };
                obj.value = item.temperature;
                obj.date = new Date(item.created_at);
                obj.tooltipText = '℃';
                generatedData.push(obj);
            });

            if(secondData.length){
                secondData.forEach((item, index) => {
                    let obj = {
                        color: "#f13b3b",
                    };
                    obj.value = item.temperature;
                    obj.date = new Date(item.created_at);
                    obj.tooltipText = '℃';
                    generatedData2.push(obj);
                });
            };

        }else {
            let data = deepcopy(this.props.chartData);
            data.forEach((item, index) => {
                let obj = {
                    color: "#f13b3b",
                };
                obj.value = item.value;
                obj.date = new Date(item.created_at);
                generatedData.push(obj);
            });
        }
        return {generatedData, generatedData2};
    };

    render() {
        return (
            <Fragment>
                {
                    <div className={"sensor-chart-container" + (this.props.isMain ? " lc-main": "")}>
                        <span id="sensorTitle">{this.props.sensorData ? this.props.sensorData.sensor_name : ''}{this.props.chartSubTitle ? ` ${this.props.chartSubTitle}` : ''}</span>
                        <div id={this.props.chartId} className="sensor-chart-wrap"/>
                        <div className="lc-white-box"/>
                    </div>
                }
            </Fragment>
        )
    }
}

export default LcChart;
