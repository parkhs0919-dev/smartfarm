import React, {Fragment} from 'react';
import Select from "react-select";
import {colourStyles} from "../../utils/select";

const TAB_MIX = "mix";
const TAB_STEP = "step";

const SensorCondition = ({
    type,
    index,
    item,
    selectSensorOption,
}, {
    clickSensorOperator,
    addMixSensors,
    removeMixSensor,
    handleMixSensors,
    renderMixFormBySensor,
    renderStepFormBySensor,
}) => (
    <Fragment>
        <label className="auto-control-form-label">복수센서 동작조건</label>
        <div className="mix-sensor-radio-wrap">
            <div className="lc-theme-radio-wrap">
                <input
                    id={`mixSensorAnd${index}`}
                    value="and"
                    checked={item.sensor_operator === "and"}
                    onChange={() => {
                    }}
                    type="radio"
                    name={`mixSensorRadio${index}`}/>
                <label
                    onClick={e => clickSensorOperator(e, 'and', index, type)}
                    htmlFor={`mixSensorAnd${index}`}
                    className="box-shape-label"/>
                <label
                    onClick={e => clickSensorOperator(e, 'and', index, type)}
                    htmlFor={`mixSensorAnd${index}`}
                    className="text-label">모두일치</label>
            </div>
            <div
                className="lc-theme-radio-wrap">
                <input
                    id={`mixSensorOr${index}`}
                    value="or"
                    checked={item.sensor_operator === "or"}
                    onChange={() => {
                    }}
                    type="radio"
                    name={`mixSensorRadio${index}`}/>
                <label
                    onClick={e => clickSensorOperator(e, 'or', index, type)}
                    htmlFor={`mixSensorOr${index}`}
                    className="box-shape-label"/>
                <label
                    onClick={e => clickSensorOperator(e, 'or', index, type)}
                    htmlFor={`mixSensorOr${index}`}
                    className="text-label">일부일치</label>
            </div>
        </div>
        {
            item[type === TAB_MIX ? 'mixSensors' : 'stepSensors'] ?
                item[type === TAB_MIX ? 'mixSensors' : 'stepSensors'].map((subItem, sensorIndex) => {
                    return (
                        <div
                            className="auto-control-setting-form auto-control-condition-wrap lc-mix-sensors-wrap"
                            key={sensorIndex}>
                            <label
                                className="auto-control-form-label">센서조건 {sensorIndex + 1}</label>
                            {
                                sensorIndex < 1 ?
                                    (
                                        <button
                                            className="mix-text-btn"
                                            onClick={e => addMixSensors(e, index, type)}>+
                                            조건추가</button>
                                    ) :
                                    (
                                        <button
                                            className="mix-text-btn"
                                            onClick={e => removeMixSensor(e, sensorIndex, index, type)}>삭제</button>
                                    )
                            }
                            <div
                                className="lc-setting-form-wrap">
                                <div
                                    className="form-flex-wrap">
                                    <div
                                        className="left-item select-module-wrap lc-size-36">
                                        <Select
                                            value={subItem.selectedSensorOption}
                                            placeholder="센서 선택"
                                            onChange={val => handleMixSensors(val, sensorIndex, index, type)}
                                            isSearchable={false}
                                            options={selectSensorOption}
                                            styles={colourStyles}/>
                                    </div>
                                    <span
                                        className="lc-modifier">가</span>
                                </div>

                                {
                                    type === TAB_MIX ?
                                        renderMixFormBySensor(sensorIndex, index) :
                                        renderStepFormBySensor(sensorIndex, index)
                                }
                            </div>
                        </div>
                    )
                })
                : null
            }

        {
           item.recommendSensorTxt ?
                <div className={"recommend-wrap"}>
                    <p className="recommendTxt" dangerouslySetInnerHTML={{__html: item.recommendSensorTxt}}/>
                </div>
                : null
        }
    </Fragment>
);

export default SensorCondition;
