import React, {Fragment} from "react";
import Select from "react-select";
import {colourStyles} from "../../utils/select";

const GetRecommend = ({
                              recommendList,
                              is_use_recommend,
                              recommendItem,
                              selectedRecommendId,
                              recommendCrops,
                              selectedCrop,
                          }, {
                                showRecommendList,
                                getRecommendData,
                                getAutoControlRecommendCrops
                          }) => (

                <Fragment>
                    {
                        !is_use_recommend ?
                            <div className={"auto-control-recommend-title"}>
                                <span>작물에 따라 추천 설정을 제공해드립니다.<br/>세부 설정은 농가의 상황에 맞춰 수정해주세요.</span>
                                <div className="button-wrap">
                                    <button className="theme-button" onClick={getAutoControlRecommendCrops}>추천 설정 보기</button>
                                </div>
                            </div>
                         :
                            <Fragment>
                                {
                                    recommendCrops && recommendCrops.length ?
                                        <div className="select-module-wrap " >
                                            <Select value={selectedCrop}
                                                    placeholder="작물 선택"
                                                    onChange={option => showRecommendList(option)}
                                                    isSearchable={ false }
                                                    options={recommendCrops}
                                                    styles={colourStyles}/>
                                        </div>
                                        : null

                                }
                                { recommendList && recommendList.length ?

                                    <div className="auto-control-recommend-list-wrap">
                                        <ul className="auto-control-recommend-list">
                                            {
                                                recommendList.map((item, index) => {
                                                    return (
                                                        <li key={index} onClick={e => getRecommendData(e, item)} className={item.id === selectedRecommendId ? 'is-active' : ''}>{item.auto_control_name}</li>
                                                    )
                                                })
                                            }
                                        </ul>
                                    </div>

                                    : <div className="auto-control-recommend-list-wrap">
                                        <span className="not-exists">추천 가능한 설정이 없습니다.</span>
                                    </div>

                                }
                            </Fragment>
                    }
                </Fragment>

);

export default GetRecommend;
