import React, {Fragment} from 'react';
import Select from "react-select";
import CONSTANT from "../../constants/constant";
import {colourStyles} from "../../utils/select";

const StepDelayByStep = ({
     step_delay,
     stepControls,
     selectedActiveCount,
     setCount,
}, {
    handleSelectActiveCount,
    handleSetCount,
    handleStepDelay
}) => (
    <Fragment>
        <label className="auto-control-form-label">단계지연</label>
        <div className="step-delay-flex-wrap">
            <input type="number" value={step_delay}
                   disabled={stepControls[0].selectedTimeWay && stepControls[0].selectedTimeWay.value === "single"}
                   onChange={handleStepDelay} className="lc-farm-input"
                   placeholder="단계지연"/>
            <span> 분</span>
        </div>

        <label className="auto-control-form-label">세트 실행횟수</label>
        <div className="step-active-count-wrap">

            <div className="step-active-count-item">
                <div className="select-module-wrap lc-size-36">
                    <Select value={selectedActiveCount}
                            placeholder="실행횟수 선택"
                            onChange={handleSelectActiveCount}
                            isSearchable={false}
                            options={CONSTANT.actionCountOptions}
                            styles={colourStyles}/>
                </div>
            </div>

            <div className="step-active-count-item">
                {
                    selectedActiveCount ?
                        (
                            <Fragment>
                                {
                                    selectedActiveCount.value === "infinite" ?
                                        (
                                            <span>세트</span>
                                        ) :
                                        (
                                            <div className="step-delay-flex-wrap">
                                                <input type="number" value={setCount}
                                                       disabled={stepControls[0].selectedTimeWay && stepControls[0].selectedTimeWay.value === "single"}
                                                       onChange={handleSetCount} className="lc-farm-input"
                                                       placeholder="세트 실행횟수"/>
                                                <span> 회</span>
                                            </div>
                                        )
                                }
                            </Fragment>
                        ) : null
                }
            </div>
        </div>
    </Fragment>
);

export default StepDelayByStep;
