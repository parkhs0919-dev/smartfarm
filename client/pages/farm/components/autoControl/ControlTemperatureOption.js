import React from "react";
import Select from "react-select";
import {colourStyles} from "../../utils/select";
const TAB_SENSOR = "sensor";
const TAB_TIME = "time";
const TAB_MIX = "mix";
const TAB_STEP = "step";
const TAB_CONTROL = "control";
const TAB_STEP_STEP = "step_step";
const TAB_TABLE = "table";

const ControlTemperatureOption = ({
    type,
    index,
    item,
    TemperatureSensor
}, {
    handleIsUseTemperatureOptionType,
    handleSelectTemperatureSensor,
    handleTemperatureType,
    handleMixInput
}) => (
    <div className="lc-temperature-wrap lc-temperature-step-wrap">
        <div className="lc-theme-checkbox-wrap">
            <input id="lcIsMinMaxRange"
                   checked={item.is_temperature_control_option}
                   onChange={() => {}}
                   type="checkbox"
                   name="mix-control-condition"/>
            <label htmlFor="lcIsMinMaxRange"
                   onClick={e => handleIsUseTemperatureOptionType(e, index, 'is_temperature_control_option', type)}
                   className="box-shape-label lc-not-allow"/>
            <label htmlFor="lcIsMinMaxRange"
                   onClick={e => handleIsUseTemperatureOptionType(e, index, 'is_temperature_control_option', type)}
                   className="text-label">
                {item.selectedControlOption && (item.selectedControlOption.origin.type === 'motor' || item.selectedControlOption.origin.type === 'windDirection') ? `환기설정` : `난방설정`}
            </label>
        </div>
        {
            item.is_temperature_control_option && item.selectedControlOption && item.selectedControlOption.origin.type === 'power' && (
                <div className="form-flex-wrap lc-expect-temperature-wrap">
                    <div className="left-item temperature-sensor-wrap">
                        <p className="form-flex-label">온도센서</p>
                        <div className="left-item">
                            <div className="select-module-wrap lc-size-36">
                                <Select
                                    value={item.selectedTemperatureSensor}
                                    placeholder="온도센서 선택"
                                    onChange={val => handleSelectTemperatureSensor(val, index, type)}
                                    isSearchable={false}
                                    options={TemperatureSensor}
                                    styles={colourStyles}/>
                            </div>
                        </div>
                    </div>
                    <div className="left-item">
                        <p className="form-flex-label">희망온도</p>
                        <div className="sub-left-item">
                            <input type="text" className="lc-farm-input" value={item.expect_temperature}
                                   pattern={"[0-9]*\\.?[0-9]*"}
                                   onChange={e => handleTemperatureType(e, index,  'expect_temperature', type)}
                                   placeholder="희망온도 값"/>
                            <label className="value-label">℃</label>
                        </div>
                    </div>
                </div>
            )
        }

        {
            item.is_temperature_control_option && (
                <div className="form-flex-wrap lc-temperature-content-wrap">
                    <div className="left-item">
                        <p className="form-flex-label">상승시간</p>
                        <div className="sub-left-item">
                            <input type="text" className="lc-farm-input" value={item.rise_time}
                                   pattern={"[0-9]*"}
                                   onChange={e => handleMixInput(e, index,  'rise_time', type)}
                                   placeholder="상승시간 값"/>
                            <label className="value-label">분/℃</label>
                        </div>
                    </div>
                    <div className="left-item">
                        <p className="form-flex-label">하강시간</p>
                        <div className="sub-left-item">
                            <input type="text" className="lc-farm-input" value={item.drop_time}
                                   pattern={"[0-9]*"}
                                   onChange={e => handleMixInput(e, index,  'drop_time', type)}
                                   placeholder="하강시간 값"/>
                            <label className="value-label">분/℃</label>
                        </div>
                    </div>
                </div>
            )
        }
    </div>
);

export default ControlTemperatureOption;
