import React from "react";
import Select from "react-select";
import {colourStyles} from "../../utils/select";
const TAB_SENSOR = "sensor";
const TAB_TIME = "time";
const TAB_MIX = "mix";
const TAB_STEP = "step";
const TAB_CONTROL = "control";
const TAB_STEP_STEP = "step_step";
const TAB_TABLE = "table";

const ControlMinMaxRange = ({
    type,
    index,
    item
}, {
    handleMixIsMinMaxRange,
    handleMixMinMaxUseState,
    handleMixMinMaxPercentage
}) => (
    <div className="lc-min-max-range-wrap">
        <div className="lc-theme-checkbox-wrap">
            <input id={`lcStepStepIsMinMaxRange${index}`}
                   checked={item.is_min_max_range}
                   onChange={() => {}}
                   type="checkbox"
                   name="mix-control-condition"/>
            <label htmlFor={`lcStepStepIsMinMaxRange${index}`}
                   onClick={e => handleMixIsMinMaxRange(e, index, type)}
                   className="box-shape-label lc-not-allow"/>
            <label htmlFor={`lcStepStepIsMinMaxRange${index}`}
                   onClick={e => handleMixIsMinMaxRange(e, index, type)}
                   className="text-label">최소/최대 개폐 범위 설정</label>
        </div>
        {
            item.is_min_max_range && (
                <ul className="lc-min-max-range-item-wrap">
                    {
                        item.minMaxRanges.map((minMaxRange, minMaxRangeKey) => (
                            minMaxRange.state === 'on' && (
                                <li className="lc-min-max-range-item" key={minMaxRangeKey}>
                                    <div className="lc-item lc-theme-checkbox-wrap">
                                        <input id={`lcMixMinMaxUseState${index}_${minMaxRangeKey}`}
                                               checked={minMaxRange.useState === 'on'}
                                               onChange={() => {}}
                                               type="checkbox"
                                               name="mix-control-condition"/>
                                        <label htmlFor={`lcMixMinMaxUseState${index}_${minMaxRangeKey}`}
                                               onClick={e => handleMixMinMaxUseState(e, index, minMaxRangeKey, type)}
                                               className="box-shape-label lc-not-allow"/>
                                    </div>
                                    <div className="lc-item">
                                        <input type="text"
                                               className="lc-farm-input"
                                               value={minMaxRange.name}
                                               readOnly={true}/>
                                    </div>
                                    <div className="lc-item">
                                        {
                                            item.selectedControlStateOption ?
                                                <input type="text"
                                                       className="lc-farm-input"
                                                       value={`${item.selectedControlStateOption.value === 'open' || item.selectedControlStateOption.value === 'open-partial' ? '최대' : ''}${item.selectedControlStateOption.value === 'close' || item.selectedControlStateOption.value === 'close-partial' ? '최소' : ''}`}
                                                       readOnly={true}/> :
                                                <input type="text" className="lc-farm-input" value={'-'} readOnly={true}/>
                                        }
                                    </div>
                                    <div className="lc-item">
                                        <input type="text"
                                               pattern={"[0-9]*"}
                                               className="lc-farm-input"
                                               value={minMaxRange.percentage}
                                               onChange={e => handleMixMinMaxPercentage(e, index, minMaxRangeKey, type)}
                                               placeholder="% 입력"/>
                                    </div>
                                </li>
                            )
                        ))
                    }
                </ul>
            )
        }
    </div>
);

export default ControlMinMaxRange;
