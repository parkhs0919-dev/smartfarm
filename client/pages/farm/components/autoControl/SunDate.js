import React, {Fragment} from 'react';
import Select from "react-select";
import {attachZero} from "../../../../utils/filter";
import {colourStyles} from "../../utils/select";

const selectSunDateTypes = [
    {value: "rise", label: "일출 기준"},
    {value: "set", label: "일몰 기준"}
];
const selectPlusMinus = [
    {value: "plus", label: "+"},
    {value: "minus", label: "-"}
];

const SunDate = ({
                     index, type,

                     rise_hour, rise_minute, set_hour, set_minute,

                     selectedSunDateStartType, selectedSunDateStartCondition, selectedSunDateStartHour, selectedSunDateStartMinute,
                     selectedSunDateEndType, selectedSunDateEndCondition, selectedSunDateEndHour, selectedSunDateEndMinute,

                     handleTime, handleInput, returnSunDateCalculate
                 }) => (
    <div className="lc-time-select-container">
        <div className="lc-time-form-wrap">
            <div className="lc-sun-date-wrap">
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">일출시간</label>
                    <input type="text"
                           className="lc-farm-input"
                           value={`${attachZero(parseInt(rise_hour))}:${attachZero(parseInt(rise_minute))}`}
                           placeholder={"일출시간"}
                           readOnly={true}/>
                </div>
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">일몰시간</label>
                    <input type="text"
                           className="lc-farm-input"
                           value={`${attachZero(parseInt(set_hour))}:${attachZero(parseInt(set_minute))}`}
                           placeholder={"일몰시간"}
                           readOnly={true}/>
                </div>
            </div>
        </div>
        <div className="lc-time-form-wrap">
            <div className="lc-sun-date-wrap">
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">시작 기준 설정</label>
                    <Select
                        value={selectedSunDateStartType}
                        placeholder="기준 선택"
                        onChange={e => handleTime(e, 'selectedSunDateStartType', index, type)}
                        isSearchable={false}
                        options={selectSunDateTypes}
                        styles={colourStyles}/>
                </div>
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">+ - 설정</label>
                    <Select
                        value={selectedSunDateStartCondition}
                        placeholder="분 선택"
                        onChange={e => handleTime(e, 'selectedSunDateStartCondition', index, type)}
                        isSearchable={false}
                        options={selectPlusMinus}
                        styles={colourStyles}/>
                </div>
            </div>
            <div className="lc-sun-date-wrap">
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">설정기준 시작시간(시간)</label>
                    <input type="text"
                           pattern={"[0-9]*"}
                           className="lc-farm-input"
                           value={selectedSunDateStartHour}
                           onChange={e => handleInput(e, "selectedSunDateStartHour", index, type)}/>
                </div>
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">설정기준 시작시간(분)</label>
                    <input type="text"
                           pattern={"[0-9]*"}
                           className="lc-farm-input"
                           value={selectedSunDateStartMinute}
                           onChange={e => handleInput(e, "selectedSunDateStartMinute", index, type)}/>
                </div>
            </div>
        </div>
        <div className="lc-time-form-wrap">
            <div className="lc-sun-date-wrap">
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">종료 기준 설정</label>
                    <Select
                        value={selectedSunDateEndType}
                        placeholder="기준 선택"
                        onChange={e => handleTime(e, 'selectedSunDateEndType', index, type)}
                        isSearchable={false}
                        options={selectSunDateTypes}
                        styles={colourStyles}/>
                </div>
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">+ - 설정</label>
                    <Select
                        value={selectedSunDateEndCondition}
                        placeholder="분 선택"
                        onChange={e => handleTime(e, 'selectedSunDateEndCondition', index, type)}
                        isSearchable={false}
                        options={selectPlusMinus}
                        styles={colourStyles}/>
                </div>
            </div>
            <div className="lc-sun-date-wrap">
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">설정기준 종료시간(시간)</label>
                    <input type="text"
                           pattern={"[0-9]*"}
                           className="lc-farm-input"
                           value={selectedSunDateEndHour}
                           onChange={e => handleInput(e, "selectedSunDateEndHour", index, type)}/>
                </div>
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">설정기준 종료시간(분)</label>
                    <input type="text"
                           pattern={"[0-9]*"}
                           className="lc-farm-input"
                           value={selectedSunDateEndMinute}
                           onChange={e => handleInput(e, "selectedSunDateEndMinute", index, type)}/>
                </div>
            </div>
        </div>
        <div className="lc-time-form-wrap">
            <div className="lc-sun-date-wrap">
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">일출/일몰 기준 시작시간</label>
                    <input type="text"
                           className="lc-farm-input"
                           value={returnSunDateCalculate(selectedSunDateStartType, selectedSunDateStartCondition, selectedSunDateStartHour, selectedSunDateStartMinute)}
                           placeholder={"일출시간"}
                           readOnly={true}/>
                </div>
                <div className="time-item select-module-wrap lc-size-36">
                    <label className="auto-control-form-label">일출/일몰 기준 종료시간</label>
                    <input type="text"
                           className="lc-farm-input"
                           value={returnSunDateCalculate(selectedSunDateEndType, selectedSunDateEndCondition, selectedSunDateEndHour, selectedSunDateEndMinute)}
                           placeholder={"일몰시간"}
                           readOnly={true}/>
                </div>
            </div>
        </div>
    </div>
);

export default SunDate;
