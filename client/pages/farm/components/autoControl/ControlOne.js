import React, {Fragment} from 'react';
import Select from "react-select";
import {colourStyles} from "../../utils/select";
import {isEmpty, setNewLine} from '../../../../utils/filter';
import CONSTANT from '../../constants/constant';

const CONTROL_STATE_OPTIONS = CONSTANT.CONTROL_OPTIONS.STATE;
const CONTROL_UNIT_OPTIONS = CONSTANT.CONTROL_OPTIONS.UNIT;
const delayWayOptions = [
    {value: 'noDelay', label: "1분 타이머"},
    {value: 'delay', label: "지연시간 입력"},
    {value: 'immediately', label: "즉시 실행"},
];
const actionCountOptions = [
    {value: 'infinite', label: "계속 실행"},
    {value: 'limit', label: "실행횟수 입력"}
];
const TAB_SENSOR = "sensor";
const TAB_TIME = "time";
const TAB_CONTROL = "control";

const ControlOne = ({
                        currentTab,
                        selectedControlOption,
                        selectControlOption,
                        selectedControlStateOption,
                        selectedControlWayOption,
                        selectedTemperatureSensor,
                        controlUnitName,
                        selectedDelayWay,
                        selectedActiveCount,
                        delay,
                        active_count,
                        selectedTimeWay,
                        settingValue,
                        is_min_max_range,
                        minMaxRanges,
                        pBands,
                        selectedPBand,
                        pBandTemperature,
                        pBandIntegral,
                        is_temperature_control_option,
                        rise_time,
                        drop_time,
                        expect_temperature,
                        TemperatureSensor,
                        expect_temperature_sensor_id,
                        recommendItem,
                        recommendTxt,
                        is_use_recommend,
                        recommendControl,
                        recommendMinMax
                    }, {
                        handleInput,
                        handleTemperature,
                        handleSelectControl,
                        handleSelectControlStateOption,
                        handleSelectControlWay,
                        handleSettingValue,
                        handleSelectDelayWay,
                        handleSelectActiveCount,
                        handleStepInput,
                        handleIsMinMaxRange,
                        handleMinMaxUseState,
                        handleMinMaxPercentage,
                        handleSelectPBand,
                        handlePBand,
                        handleIsUseTemperatureOption,
                        handleSelectTemperatureSensor,
                        handleRecommendData
                    }) => (
    <div className="auto-control-setting-form auto-control-execution-wrap">
        <label className="auto-control-form-label">실행부</label>
        <div className="lc-setting-form-wrap">
            <div className="form-flex-wrap">
                <div
                    className="left-item select-module-wrap lc-size-36">
                    <Select value={selectedControlOption}
                            placeholder="기기 선택"
                            onChange={handleSelectControl}
                            isSearchable={false}
                            options={selectControlOption}
                            styles={colourStyles}/>
                </div>
            </div>
            {
                recommendControl ?
                    <div className={"recommend-wrap"}>
                        <p className="recommendTxt">(추천기기 : {recommendControl.control_id})</p>
                    </div>
                    : null
            }
            {
                selectedControlOption ||  recommendControl ?
                (
                    <Fragment>
                        <div className="form-flex-wrap">
                            <div className="left-item select-module-wrap lc-size-36">
                                <Select value={selectedControlStateOption}
                                        placeholder="동작 선택"
                                        onChange={handleSelectControlStateOption}
                                        isSearchable={false}
                                        options={selectedControlOption && selectedControlOption.origin.type ? CONTROL_STATE_OPTIONS[selectedControlOption.origin.type] : (recommendControl.control.type ? CONTROL_STATE_OPTIONS[recommendControl.control.type]:[])}
                                        styles={colourStyles}/>
                            </div>
                            <span className="lc-modifier">를</span>

                        </div>
                        {
                            selectedControlStateOption && selectedControlStateOption.hasValue && (
                                <div className="form-flex-wrap">
                                    <div className="left-item select-and-input-wrap">
                                        <div className="select-and-input">
                                            <div className="select-module-wrap lc-size-36">
                                                <Select
                                                    value={selectedControlWayOption}
                                                    placeholder="단위 선택"
                                                    onChange={handleSelectControlWay}
                                                    isSearchable={false}
                                                    options={pBands.length && (selectedControlOption.origin.type === 'motor' || selectedControlOption.origin.type === 'windDirection') && selectedControlStateOption.value === 'open-partial' ? [
                                                        ...CONTROL_UNIT_OPTIONS.motor,
                                                        ...CONTROL_UNIT_OPTIONS.pBand
                                                    ] : CONTROL_UNIT_OPTIONS[selectedControlOption.origin.type] ? CONTROL_UNIT_OPTIONS[selectedControlOption.origin.type] : (CONTROL_UNIT_OPTIONS[recommendControl.control.type])}
                                                    styles={colourStyles}/>
                                            </div>
                                        </div>

                                        <div className="select-and-input input-auto-control">
                                            {
                                                !selectedControlWayOption || (selectedControlWayOption.value !== 'pBand' && selectedControlWayOption.value !== 'pBandIntegral') && (
                                                    <input type="number"
                                                           className="lc-farm-input"
                                                           value={settingValue}
                                                           onChange={handleSettingValue}
                                                           placeholder="입력"/>
                                                )
                                            }

                                        </div>
                                    </div>
                                    {
                                        selectedControlWayOption && (selectedControlWayOption.value !== 'pBand' && selectedControlWayOption.value !== 'pBandIntegral') &&
                                        (
                                            <span className="lc-modifier">{controlUnitName}</span>
                                        )
                                    }
                                </div>
                            )
                        }

                        {
                            selectedControlWayOption && (selectedControlWayOption.value === 'pBand' || selectedControlWayOption.value === 'pBandIntegral') && (
                                <div className="form-flex-wrap">
                                    <div className="left-item select-and-input-wrap p-band-select-wrap">
                                        <div className="p-band-select select-and-input ">
                                            <div className="select-module-wrap lc-size-36">
                                                <Select
                                                    value={selectedPBand}
                                                    placeholder="P밴드 선택"
                                                    onChange={handleSelectPBand}
                                                    isSearchable={false}
                                                    options={pBands}
                                                    styles={colourStyles}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                        {
                            selectedControlWayOption && (selectedControlWayOption.value === 'pBand' || selectedControlWayOption.value === 'pBandIntegral') && (
                                <div className="form-flex-wrap">
                                    <p className="lc-p-band">희망 온도</p>
                                    <input type={"text"}
                                           pattern={"[0-9]*\\.?[0-9]*"}
                                           className="lc-farm-input lc-p-band"
                                           value={pBandTemperature}
                                           onChange={e => handlePBand(e, 'pBandTemperature')}/>
                                    <p className="lc-p-band">℃</p>
                                </div>
                            )
                        }
                        {
                            selectedControlWayOption && selectedControlWayOption.value === 'pBandIntegral' && (
                                <div className="form-flex-wrap">
                                    <p className="lc-p-band">적분 값</p>
                                    <input type={"text"}
                                           pattern={"[0-9]*"}
                                           className="lc-farm-input lc-p-band"
                                           value={pBandIntegral}
                                           onChange={e => handlePBand(e, 'pBandIntegral')}/>
                                    <p className="lc-p-band">%</p>
                                </div>
                            )
                        }
                    </Fragment>
                ) : null
            }

            {
                currentTab === TAB_TIME && selectedTimeWay && selectedTimeWay.value === "single" ?
                    null :
                    (
                        <div className="delay-recount-wrap">
                            <label className="auto-control-form-label">지연 및 실행횟수</label>
                            <div className="delay-recount-input-container">
                                <div className="delay-recount-input-wrap">
                                    <div className="select-module-wrap lc-size-36">
                                        <Select value={selectedDelayWay}
                                                placeholder="실행 타이머"
                                                onChange={handleSelectDelayWay}
                                                isSearchable={false}
                                                options={delayWayOptions}
                                                styles={colourStyles}/>
                                    </div>
                                </div>
                                <div className="delay-recount-input-wrap">
                                    <div className="select-module-wrap lc-size-36">
                                        <Select value={selectedActiveCount}
                                                placeholder="실행횟수 선택"
                                                onChange={handleSelectActiveCount}
                                                isSearchable={false}
                                                options={actionCountOptions}
                                                styles={colourStyles}/>
                                    </div>
                                </div>
                            </div>

                            {
                                selectedActiveCount || selectedDelayWay ?
                                    (
                                        <div className="delay-recount-input-container">
                                            {
                                                selectedDelayWay.value === "delay" ?
                                                    (
                                                        <div className="delay-recount-input-wrap">
                                                            <input type="number"
                                                                   className="lc-farm-input"
                                                                   value={delay}
                                                                   onChange={e => handleStepInput(e, 'delay')}/>
                                                            <span className="delay-recount-description">분 지연</span>
                                                        </div>
                                                    ) : null
                                            }
                                            {
                                                selectedActiveCount.value === "infinite" ?
                                                    null :
                                                    (
                                                        <div className="delay-recount-input-wrap">
                                                            <input type="number" className="lc-farm-input" value={active_count} onChange={e => handleStepInput(e, 'active_count')}/>
                                                            <span className="delay-recount-description">회</span>
                                                        </div>
                                                    )
                                            }
                                        </div>
                                    ) : null
                            }
                        </div>
                    )
            }
            {
                ( selectedControlWayOption && selectedControlWayOption.value === 'pBand') || (selectedControlOption && selectedControlOption.origin.type === 'power') ?
                    (
                        <div className="lc-temperature-wrap">
                            <div className="lc-theme-checkbox-wrap">
                                <input id="lcIsMinMaxRange"
                                       checked={is_temperature_control_option}
                                       onChange={() => {}}
                                       type="checkbox"
                                       name="mix-control-condition"/>
                                <label htmlFor="lcIsMinMaxRange"
                                       onClick={handleIsUseTemperatureOption}
                                       className="box-shape-label lc-not-allow"/>
                                <label htmlFor="lcIsMinMaxRange"
                                       onClick={handleIsUseTemperatureOption}
                                       className="text-label">
                                    {selectedControlOption && (selectedControlOption.origin.type === 'motor' || selectedControlOption.origin.type === 'windDirection') ? `환기설정` : `난방설정`}
                                </label>
                            </div>
                            {
                                is_temperature_control_option && selectedControlOption && selectedControlOption.origin.type === 'power' && (

                                    <div className="form-flex-wrap lc-temperature-content-wrap">
                                        <div className="left-item">
                                            <p className="form-flex-label">온도센서</p>
                                            <div className="left-item">
                                                <div className="select-module-wrap lc-size-36">
                                                    <Select
                                                        value={selectedTemperatureSensor}
                                                        placeholder="온도센서 선택"
                                                        onChange={handleSelectTemperatureSensor}
                                                        isSearchable={false}
                                                        options={TemperatureSensor}
                                                        styles={colourStyles}/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="left-item">
                                            <p className="form-flex-label">희망온도</p>
                                            <div className="sub-left-item">
                                                <input type="text" className="lc-farm-input" value={expect_temperature}
                                                       pattern={"[0-9]*\\.?[0-9]*"}
                                                       onChange={e => handleTemperature(e, 'expect_temperature')}
                                                       placeholder="센서 값"/>
                                                <label className="value-label">℃</label>
                                            </div>
                                        </div>
                                    </div>
                                )
                            }

                            {
                                is_temperature_control_option && (
                                    <div className="form-flex-wrap lc-temperature-content-wrap">
                                        <div className="left-item">
                                            <p className="form-flex-label">상승시간</p>
                                            <div className="sub-left-item">
                                                <input type="text" className="lc-farm-input" value={rise_time}
                                                       pattern={"[0-9]*"}
                                                       onChange={e => handleInput(e, 'rise_time')}
                                                       placeholder="센서 값"/>
                                                <label className="value-label">분/℃</label>
                                            </div>
                                        </div>
                                        <div className="left-item">
                                            <p className="form-flex-label">하강시간</p>
                                            <div className="sub-left-item">
                                                <input type="text" className="lc-farm-input" value={drop_time}
                                                       pattern={"[0-9]*"}
                                                       onChange={e => handleInput(e, 'drop_time')}
                                                       placeholder="센서 값"/>
                                                <label className="value-label">분/℃</label>
                                            </div>
                                        </div>
                                    </div>
                                )
                            }
                        </div>
                    ): null
            }
            <p className="execution-ment">실행해주세요</p>
            {
                selectedControlOption && (selectedControlOption.origin.type === 'motor'  || selectedControlOption.origin.type === 'windDirection')&& (
                    <div className="lc-min-max-range-wrap">
                        <div className="lc-theme-checkbox-wrap">
                            <input id="lcIsMinMaxRange"
                                   checked={is_min_max_range}
                                   onChange={() => {}}
                                   type="checkbox"
                                   name="mix-control-condition"/>
                            <label htmlFor="lcIsMinMaxRange"
                                   onClick={handleIsMinMaxRange}
                                   className="box-shape-label lc-not-allow"/>
                            <label htmlFor="lcIsMinMaxRange"
                                   onClick={handleIsMinMaxRange}
                                   className="text-label">최소/최대 개폐 범위 설정</label>
                        </div>
                        {
                            is_min_max_range && (
                                <ul className="lc-min-max-range-item-wrap">
                                    {
                                        minMaxRanges.map((minMaxRange, minMaxRangeKey) => (
                                            minMaxRange.state === 'on' && (
                                                <li className="lc-min-max-range-item" key={minMaxRangeKey}>
                                                    <div className="lc-item lc-theme-checkbox-wrap">
                                                        <input id={`lcMinMaxUseState${minMaxRangeKey}`}
                                                               checked={minMaxRange.useState === 'on'}
                                                               onChange={() => {}}
                                                               type="checkbox"
                                                               name="mix-control-condition"/>
                                                        <label htmlFor={`lcMinMaxUseState${minMaxRangeKey}`}
                                                               onClick={e => handleMinMaxUseState(e, minMaxRangeKey)}
                                                               className="box-shape-label lc-not-allow"/>
                                                    </div>
                                                    <div className="lc-item">
                                                        <input type="text"
                                                               className="lc-farm-input"
                                                               value={minMaxRange.name}
                                                               readOnly={true}/>
                                                    </div>
                                                    <div className="lc-item">
                                                        {
                                                            selectedControlStateOption ?
                                                                <input type="text"
                                                                       className="lc-farm-input"
                                                                       value={`${selectedControlStateOption.value === 'open' || selectedControlStateOption.value === 'open-partial' ? '최대' : ''}${selectedControlStateOption.value === 'close' || selectedControlStateOption.value === 'close-partial' ? '최소' : ''}`}
                                                                       readOnly={true}/> :
                                                                <input type="text" className="lc-farm-input" value={'-'} readOnly={true}/>
                                                        }
                                                    </div>
                                                    <div className="lc-item">
                                                        <input type="text"
                                                               pattern={"[0-9]*"}
                                                               className="lc-farm-input"
                                                               value={minMaxRange.percentage}
                                                               onChange={e => handleMinMaxPercentage(e, minMaxRangeKey)}
                                                               placeholder="% 입력"/>
                                                    </div>
                                                </li>
                                            )
                                        ))
                                    }
                                </ul>
                            )
                        }
                    </div>
                )
            }

            {recommendMinMax && recommendMinMax.length ?
                <Fragment>
                    <div className={"recommend-wrap"}>
                        <p className="recommendTxt">추천 최소/최대 개폐범위</p>
                        <ul>
                        {
                            recommendMinMax.map((data, index) => (
                               <li key={index}><p className="recommendTxt">{`${index+1} : ${data.name} / ${data.percentage}%`}</p></li>
                            ))
                        }
                        </ul>
                    </div>
                </Fragment>
                : null
            }

        </div>
    </div>
);

export default ControlOne;
