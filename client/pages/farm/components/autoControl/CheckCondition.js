import React from "react";

const CheckCondition = ({
    type,
    index,
    item,
}, {
    clickConditionLabel
}) => (
    <div className="mix-control-item-wrap mix-control-condition-tab-wrap">
        <label className="auto-control-form-label">조건 선택</label>
        <div className="lc-checkbox-wrap">
            <div className="lc-theme-checkbox-wrap">
                <input id="mixControlCondition1"
                       value="time"
                       checked={item.conditional.time === true}
                       onChange={() => {
                       }} type="checkbox"
                       name="mix-control-condition"/>
                <label htmlFor="mixControlCondition1"
                       onClick={(e) => clickConditionLabel(e, index, 'time', type)}
                       className="box-shape-label"/>
                <label htmlFor="mixControlCondition1"
                       onClick={(e) => clickConditionLabel(e, index, 'time', type)}
                       className="text-label">시간 조건</label>
            </div>
            <div className="lc-theme-checkbox-wrap">
                <input id="mixControlCondition2"
                       value="sensor"
                       checked={item.conditional.sensor === true}
                       onChange={() => {
                       }} type="checkbox"
                       name="mix-control-condition"/>
                <label
                    htmlFor="mixControlCondition2"
                    onClick={(e) => clickConditionLabel(e, index, 'sensor', type)}
                    className="box-shape-label"/>
                <label
                    htmlFor="mixControlCondition2"
                    onClick={(e) => clickConditionLabel(e, index, 'sensor', type)}
                    className="text-label">센서 조건</label>
            </div>
            <div className="lc-theme-checkbox-wrap">
                <input id="mixControlCondition3"
                       value="sensor"
                       checked={item.conditional.control === true}
                       onChange={() => {
                       }} type="checkbox"
                       name="mix-control-condition"/>
                <label
                    htmlFor="mixControlCondition3"
                    onClick={(e) => clickConditionLabel(e, index, 'control', type)}
                    className="box-shape-label"/>
                <label
                    htmlFor="mixControlCondition3"
                    onClick={(e) => clickConditionLabel(e, index, 'control', type)}
                    className="text-label">기기 조건</label>
            </div>
        </div>
    </div>
);

export default CheckCondition;
