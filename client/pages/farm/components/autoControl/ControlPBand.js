import React, {Component, Fragment} from 'react';
import Select from "react-select";
import {colourStyles} from "../../utils/select";
const TAB_SENSOR = "sensor";
const TAB_TIME = "time";
const TAB_MIX = "mix";
const TAB_STEP = "step";
const TAB_CONTROL = "control";
const TAB_STEP_STEP = "step_step";
const TAB_TABLE = "table";

const ControlPBand = ({
    type,
    index,
    item,
    pBands
}, {
    handleMixSelectPBand,
    handleMixPBand,
}) => (
   <Fragment>
       {
           item.selectedControlWayOption && (item.selectedControlWayOption.value === 'pBand' || item.selectedControlWayOption.value === 'pBandIntegral') && (
               <div className="form-flex-wrap">
                   <div className="left-item select-and-input-wrap p-band-select-wrap">
                       <div className="p-band-select select-and-input ">
                           <div className="select-module-wrap lc-size-36">
                               <Select
                                   value={item.selectedPBand}
                                   placeholder="P밴드 선택"
                                   onChange={val => handleMixSelectPBand(val, index, type)}
                                   isSearchable={false}
                                   options={pBands}
                                   styles={colourStyles}/>
                           </div>
                       </div>
                   </div>
               </div>
           )
       }
       {
           item.selectedControlWayOption && (item.selectedControlWayOption.value === 'pBand' || item.selectedControlWayOption.value === 'pBandIntegral') && (
               <div className="form-flex-wrap">
                   <p className="lc-p-band">희망 온도</p>
                   <input type={"text"}
                          pattern={"[0-9]*"}
                          className="lc-farm-input lc-p-band"
                          value={item.pBandTemperature}
                          onChange={e => handleMixPBand(e, index, 'pBandTemperature', type)}/>
                   <p className="lc-p-band">℃</p>
               </div>
           )
       }
       {
           item.selectedControlWayOption && item.selectedControlWayOption.value === 'pBandIntegral' && (
               <div className="form-flex-wrap">
                   <p className="lc-p-band">적분 값</p>
                   <input type={"text"}
                          pattern={"[0-9]*"}
                          className="lc-farm-input lc-p-band"
                          value={item.pBandIntegral}
                          onChange={e => handleMixPBand(e, index, 'pBandIntegral', type)}/>
                   <p className="lc-p-band">%</p>
               </div>
           )
       }
   </Fragment>
);

export default ControlPBand;
