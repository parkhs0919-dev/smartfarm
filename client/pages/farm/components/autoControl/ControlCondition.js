import React, {Fragment} from "react";
import Select from "react-select";
import {colourStyles} from "../../utils/select";
import CONSTANT from '../../constants/constant';

const TAB_MIX = "mix";
const TAB_STEP = "step";

const ControlCondition = ({
    type,
    index,
    item,
    selectControlConditionOption
}, {
    clickControlOperator,
    addMixControls,
    removeMixControl,
    handleMixControlCondition,
    handleMixControlConditionValue,
    handleMixControlConditionOption,
}) => (
    <Fragment>
        <label className="auto-control-form-label">복수기기 동작조건</label>
        <div className="mix-sensor-radio-wrap">
            <div className="lc-theme-radio-wrap">
                <input id={`mixControlAnd${index}`}
                       value="and"
                       checked={item.control_operator === "and"}
                       onChange={() => {}}
                       type="radio"
                       name={`mixControlRadio${index}`}/>
                <label onClick={e => clickControlOperator(e, 'and', index, type)}
                       htmlFor={`mixControlAnd${index}`}
                       className="box-shape-label"/>
                <label onClick={e => clickControlOperator(e, 'and', index, type)}
                       htmlFor={`mixControlAnd${index}`}
                       className="text-label">모두일치</label>
            </div>
            <div className="lc-theme-radio-wrap">
                <input id={`mixControlOr${index}`}
                       value="or"
                       checked={item.control_operator === "or"}
                       onChange={() => {}}
                       type="radio"
                       name={`mixControlRadio${index}`}/>
                <label onClick={e => clickControlOperator(e, 'or', index, type)}
                       htmlFor={`mixControlOr${index}`}
                       className="box-shape-label"/>
                <label onClick={e => clickControlOperator(e, 'or', index, type)}
                       htmlFor={`mixControlOr${index}`}
                       className="text-label">일부일치</label>
            </div>
        </div>
        {
            item[type === TAB_MIX ? 'mixControls' : 'stepControls'] ?
            item[type === TAB_MIX ? 'mixControls' : 'stepControls'].map((subItem, controlIndex) => {
                return (
                    <div className="auto-control-setting-form auto-control-condition-wrap lc-mix-sensors-wrap"
                         key={controlIndex}>
                        <label className="auto-control-form-label">기기조건 {controlIndex + 1}</label>
                        {
                            controlIndex < 1 ?
                                (
                                    <button className="mix-text-btn"
                                            onClick={e => addMixControls(e, index, type)}>+ 조건추가</button>
                                ) :
                                (
                                    <button className="mix-text-btn"
                                            onClick={e => removeMixControl(e, controlIndex, index, type)}>삭제</button>
                                )
                        }
                        <div className="lc-setting-form-wrap">
                            <div className="form-flex-wrap">
                                <div className="left-item select-module-wrap lc-size-36">
                                    <Select value={subItem.selectedControlCondition}
                                            placeholder="기기 선택"
                                            onChange={val => handleMixControlCondition(val, controlIndex, index, type)}
                                            isSearchable={false}
                                            options={selectControlConditionOption}
                                            styles={colourStyles}/>
                                </div>
                                <span className="lc-modifier">가</span>
                            </div>
                            {
                                subItem.recommendControlControls ?
                                    <div className={"recommend-wrap"}>
                                        <p className="recommendTxt">(추천기기 : {subItem.recommendControlControls.control_id})</p>
                                    </div>
                                    : null
                            }

                            {
                                subItem.selectedControlCondition || subItem.recommendControlControls ? (
                                    <Fragment>
                                        {
                                            (subItem.selectedControlCondition && subItem.selectedControlCondition.origin.type === 'motor') || (subItem.recommendControlControls && subItem.recommendControlControls.control && subItem.recommendControlControls.control.type === 'motor')  ? (
                                                <div className="form-flex-wrap">
                                                    <div className="left-item">
                                                        <input type="text"
                                                               className="lc-farm-input"
                                                               value={subItem.controlConditionValue}
                                                               onChange={e => handleMixControlConditionValue(e, controlIndex, index, type)}
                                                               placeholder="% 값"/>
                                                    </div>
                                                    <span className="lc-modifier">%</span>
                                                </div>
                                            ) : null
                                        }

                                        <div className="form-flex-wrap">
                                            <div className="left-item select-module-wrap lc-size-36">
                                                <Select value={subItem.selectedControlConditionOption}
                                                        placeholder="조건 선택"
                                                        onChange={val => handleMixControlConditionOption(val, controlIndex, index, type)}
                                                        isSearchable={false}
                                                        options={subItem.selectedControlCondition ? CONSTANT.selectControlConditions[subItem.selectedControlCondition.origin.type] : (subItem.recommendControlControls && subItem.recommendControlControls.control)  ? CONSTANT.selectControlConditions[subItem.recommendControlControls.control.type] : null}
                                                        styles={colourStyles}/>
                                            </div>
                                            <span className="lc-modifier">이 되면</span>
                                        </div>
                                    </Fragment>
                                ) : null
                            }
                        </div>
                    </div>
                )
            }) : null
        }
    </Fragment>
);

export default ControlCondition;
