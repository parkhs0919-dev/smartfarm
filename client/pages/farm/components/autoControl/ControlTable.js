import React, {Fragment} from 'react';
import Select from "react-select";
import {colourStyles} from "../../utils/select";
import CONSTANT from '../../constants/constant';
import TRANSLATE from '../../constants/translate';
import {attachZero} from "../../../../utils/filter";
import {sunDateTimeToText} from "../../utils/timeText";

const CONTROL_STATE_OPTIONS = CONSTANT.CONTROL_OPTIONS.STATE;
const CONTROL_UNIT_OPTIONS = CONSTANT.CONTROL_OPTIONS.UNIT;

const TAB_TABLE = "table";

const selectPlusMinus = [
    {value: "plus", label: "+"},
    {value: "minus", label: "-"}
];

const selectUsingPeriod = [
    {label: '미사용', value: false},
    {label: '사용', value: true},
]

const selectTableStartTimeTypes = [
    {value: "rise", label: "일출 기준"},
    {value: "set", label: "일몰 기준"},
    {value: "single", label: "고정"}
]

const ControlTable = ({
                          currentTab,
                          pBands,
                          selectMotorOption,
                          selectPowerOption,
                          selectAutoControlSensorOption,
                          selectHourOptions,
                          selectMinuteOptions,
                          selectNoSelctSensorOption,
                          tableSelectedIsUseTemperature,
                          tableSelectedPband,
                          tableSelectedSensor,
                          tableSelectedControls,
                          tableControls,
                          tableSelectedPeriod,
                          tempTableControls,
                          tableAvgTemeperature,
                      }, {
                          handleTableSelectMotors,
                          addTableControls,
                          removeTableControls,
                          handleTableIsUseTemperatureOption,
                          addTablePeriod,
                          handleTableSelectPeriod,
                          removeTablePeriod,
                          handleTableIsUseTemperatureControl,
                          handleTableSelectSensor,
                          handleTableSelect,
                          handleTableInput,
                          handleTableSensorInput,
                          handleTableSensorSelect,
                          addTableControlSensors,
                          removeTableControlSensors,
                          tempSubmit,
                          openTemperatureGraphModal
                      }) => (
    <div id="autoControlTableWrap">
        <div className="auto-control-table-option auto-control-form-section">
            <div className="table-option-wrap left-item">
                <label className="auto-control-form-label">환기/난방 설정</label>
                <div className="table-option">
                    <div className="lc-theme-radio-wrap">
                        <input
                            id="isUseTemperatureVantilation"
                            value="vantilation"
                            checked={tableSelectedIsUseTemperature === 'vantilation'}
                            onChange={() => {
                            }}
                            type="radio"/>
                        <label
                            onClick={e => handleTableIsUseTemperatureOption(e, 'vantilation')}
                            htmlFor="isUseTemperatureVantilation"
                            className="box-shape-label"/>
                        <label
                            onClick={e => handleTableIsUseTemperatureOption(e, 'vantilation')}
                            htmlFor="isUseTemperatureVantilation"
                            className="text-label">환기</label>
                    </div>
                    <div className="lc-theme-radio-wrap">
                        <input
                            id="isUseTemperatureBoiler"
                            value="boiler"
                            checked={tableSelectedIsUseTemperature === 'boiler'}
                            onChange={() => {
                            }}
                            type="radio"/>
                        <label
                            onClick={e => handleTableIsUseTemperatureOption(e, 'boiler')}
                            htmlFor="isUseTemperatureBoiler"
                            className="box-shape-label"/>
                        <label
                            onClick={e => handleTableIsUseTemperatureOption(e, 'boiler')}
                            htmlFor="isUseTemperatureBoiler"
                            className="text-label">난방</label>
                    </div>
                </div>
            </div>
            {tableSelectedIsUseTemperature === 'vantilation' && (
                <div className="table-option-wrap left-item">
                    <label className="auto-control-form-label">P밴드 기준 선택</label>
                    <div className="table-option">
                        <div className="select-module-wrap">
                            <Select
                                value={tableSelectedPband}
                                placeholder="P밴드 선택"
                                onChange={val => handleTableSelectSensor(val, 'pBand')}
                                isSearchable={false}
                                options={pBands}
                                styles={colourStyles}/>
                        </div>
                    </div>

                </div>
            )}
            {tableSelectedIsUseTemperature === 'boiler' && (
                <div className="table-option-wrap left-item">
                    <label className="auto-control-form-label">센서 기준 선택</label>
                    <div className="table-option">
                        <div className="select-module-wrap">
                            <Select
                                value={tableSelectedSensor}
                                placeholder="센서 선택"
                                onChange={val => handleTableSelectSensor(val, 'sensor')}
                                isSearchable={false}
                                options={selectNoSelctSensorOption}
                                styles={colourStyles}/>
                        </div>
                    </div>

                </div>
            )}

        </div>
        <div className="auto-control-form-section">
            <div className="auto-control-form-header">
                <label className="auto-control-form-label">제어 장치</label>
                <button className="theme-text-button" onClick={addTableControls}>장치 추가</button>
            </div>
            <div className="auto-control-list-wrap">
                {tableSelectedControls ? tableSelectedControls.map((data, index) => {
                    return (
                        <div key={index}>
                            {
                                tableSelectedIsUseTemperature === 'vantilation' &&
                                (
                                    <div className="auto-control-item" key={index}>
                                        <div className="select-module-wrap">
                                            <Select
                                                value={data.selectedMotorOption}
                                                placeholder="모터장치 선택"
                                                onChange={val => handleTableSelectMotors(val, index, 'motor')}
                                                isSearchable={false}
                                                options={selectMotorOption}
                                                styles={colourStyles}/>
                                        </div>
                                        <button className="theme-text-button"
                                                onClick={e => removeTableControls(e, index)}>삭제
                                        </button>
                                    </div>
                                )
                            }

                            {
                                tableSelectedIsUseTemperature === 'boiler' &&
                                (
                                    <div className="auto-control-item" key={index}>
                                        <div className="select-module-wrap">
                                            <Select
                                                value={data.selectedPowerOption}
                                                placeholder="전원장치 선택"
                                                onChange={val => handleTableSelectMotors(val, index, 'power')}
                                                isSearchable={false}
                                                options={selectPowerOption}
                                                styles={colourStyles}/>
                                        </div>
                                        <button className="theme-text-button" onClick={e => removeTableControls(e, index)}>삭제</button>
                                    </div>
                                )
                            }
                        </div>

                    )
                }) : null}
            </div>
        </div>
        <div className="auto-control-table-wrap auto-control-form-section">
            <div className="auto-control-form-header">
                <label className="auto-control-form-label">주기 설정 표</label>
                <button className="theme-text-button" onClick={addTablePeriod}>주기 추가</button>
            </div>
            <div className='auto-control-table'>
                {tableControls ? tableControls.map((data, index) => {
                    return (
                        <Fragment key={index}>
                            {index % 6 === 0 &&
                            <ul className={'auto-control-table-title'}>
                                <li><span> </span></li>
                                <li><span>사용여부</span></li>
                                <li><span>작동기준</span></li>
                                <li><span>시작시간</span></li>
                                <li><span>희망온도 (℃)</span></li>
                                <li><span>온도 상승시간(분)</span></li>
                                <li><span>온도 하강시간(분)</span></li>
                                <li><span>주기 설정</span></li>
                                {index > 5 ? <li><span></span></li> : null}
                            </ul>
                            }
                            <ul className="auto-control-table-content" >
                                <li className="table-content-title"><span>{index + 1}주기</span></li>
                                <li><span>{data.is_using_period.value ? '사용' : '미사용'}</span></li>
                                <li><span>{data.selectedTimeType ? TRANSLATE.TIME_TYPE[data.selectedTimeType.value] : '-'}</span></li>
                                <li><span>{data.selectedStartHour && data.selectedStartMinute ? data.selectedTimeType.value !== 'single' && data.selectedTimeCondition ? data.selectedTimeCondition.label +' '+ attachZero(data.selectedStartHour.value) +':'+attachZero(data.selectedStartMinute.value) : attachZero(data.selectedStartHour.value) +':'+attachZero(data.selectedStartMinute.value) :'-'}</span></li>
                                <li><span>{data.expect_temperature ? data.expect_temperature+'℃' : '-'}</span></li>
                                <li><span>{data.rise_time ? data.rise_time+'분' : '-'}</span></li>
                                <li><span>{data.drop_time ? data.drop_time+'분' : '-'}</span></li>
                                <li>
                                    <button className="card-header-button"
                                            onClick={e => handleTableSelectPeriod(index)}>선택
                                    </button>
                                </li>
                                {index > 5 ?
                                    <li>
                                        <button onClick={e => removeTablePeriod(e, index)}
                                                className="card-header-button">삭제
                                        </button>
                                    </li>
                                    : null
                                }
                            </ul>
                        </Fragment>
                    )
                }) : null}
            </div>
        </div>
        {tableSelectedPeriod !== null ?
            <div className="auto-control-table-detail-wrap auto-control-form-section">
                <div className="auto-control-form-header auto-control-detail-header">
                    <label className="auto-control-form-label">주기 설정</label>
                    <div className="select-module-wrap">
                        <Select
                            value={tableSelectedPeriod}
                            placeholder="주기 선택"
                            onChange={val => handleTableSelectPeriod(val, TAB_TABLE)}
                            isSearchable={false}
                            options={tableControls}
                            styles={colourStyles}/>
                    </div>
                </div>
                <div className="auto-control-detail-form">
                    <div className="left-item">
                        <div className="detail-item">
                            <label className="detail-item-label">사용여부</label>
                            <div className="select-module-wrap">
                                <Select
                                    value={tempTableControls[tableSelectedPeriod.value].is_using_period}
                                    placeholder="주기 선택"
                                    onChange={val => handleTableSelect(val, tableSelectedPeriod.value, 'is_using_period', TAB_TABLE)}
                                    isSearchable={false}
                                    options={selectUsingPeriod}
                                    styles={colourStyles}/>
                            </div>
                        </div>
                        <div className="detail-item">
                            <label className="detail-item-label">작동기준</label>
                            <div className="detail-sub-item">
                                <div className="select-module-wrap">
                                    <Select
                                        value={tempTableControls[tableSelectedPeriod.value].selectedTimeType}
                                        placeholder="작동기준 선택"
                                        onChange={val => handleTableSelect(val, tableSelectedPeriod.value, 'selectedTimeType', TAB_TABLE)}
                                        isSearchable={false}
                                        options={selectTableStartTimeTypes}
                                        styles={colourStyles}/>
                                </div>
                                {
                                    tempTableControls[tableSelectedPeriod.value].selectedTimeType.value !== 'single' ?
                                        <div className="select-module-wrap">
                                            <Select
                                                value={tempTableControls[tableSelectedPeriod.value].selectedTimeCondition}
                                                onChange={val => handleTableSelect(val, tableSelectedPeriod.value, 'selectedTimeCondition', TAB_TABLE)}
                                                isSearchable={false}
                                                options={selectPlusMinus}
                                                styles={colourStyles}/>
                                        </div>
                                        : null
                                }

                            </div>

                        </div>
                        <div className="detail-item">
                            <label className="detail-item-label">시작시간</label>
                            <div className="select-module-wrap time-select">
                                <Select
                                    value={tempTableControls[tableSelectedPeriod.value].selectedStartHour}
                                    placeholder="시간 선택"
                                    onChange={val => handleTableSelect(val, tableSelectedPeriod.value, 'selectedStartHour', TAB_TABLE)}
                                    isSearchable={false}
                                    options={selectHourOptions}
                                    styles={colourStyles}/>
                            </div>
                            <div className="select-module-wrap time-select">
                                <Select
                                    value={tempTableControls[tableSelectedPeriod.value].selectedStartMinute}
                                    placeholder="분 선택"
                                    onChange={val => handleTableSelect(val, tableSelectedPeriod.value, 'selectedStartMinute', TAB_TABLE)}
                                    isSearchable={false}
                                    options={selectMinuteOptions}
                                    styles={colourStyles}/>
                            </div>
                        </div>
                        <div className="detail-item">
                            <label className="detail-item-label">희망온도</label>
                            <div className="select-module-wrap">
                                <input type="text" className="lc-farm-input"
                                       value={tempTableControls[tableSelectedPeriod.value].expect_temperature}
                                       onChange={e => handleTableInput(e, tableSelectedPeriod.value, 'expect_temperature', TAB_TABLE)}
                                       placeholder="희망온도 값"/>
                            </div>
                            <span>℃</span>

                        </div>
                        <div className="detail-item">
                            <label className="detail-item-label">상승시간</label>
                            <div className="select-module-wrap">
                                <input type="text" className="lc-farm-input"
                                       value={tempTableControls[tableSelectedPeriod.value].rise_time}
                                       onChange={e => handleTableInput(e, tableSelectedPeriod.value, 'rise_time', TAB_TABLE)}
                                       placeholder="상승시간 값"/>
                            </div>
                            <span>분</span>

                        </div>
                        <div className="detail-item">
                            <label className="detail-item-label">하강시간</label>
                            <div className="select-module-wrap">
                                <input type="text" className="lc-farm-input"
                                       value={tempTableControls[tableSelectedPeriod.value].drop_time}
                                       onChange={e => handleTableInput(e, tableSelectedPeriod.value, 'drop_time', TAB_TABLE)}
                                       placeholder="하강시간 값"/>
                            </div>
                            <span>분</span>

                        </div>
                    </div>
                    {tableSelectedIsUseTemperature === 'vantilation' ?
                    <div className="left-item">
                        <div className="detail-item">
                            <div className="lc-theme-checkbox-wrap">
                                <input id="useExpectTemperatureControl"
                                       checked={tempTableControls[tableSelectedPeriod.value].isUseExpectTemperatureControl}
                                       onChange={() => {
                                       }}
                                       type="checkbox"
                                       name="mix-control-condition"/>
                                <label htmlFor="useExpectTemperatureControl"
                                       onClick={e => handleTableIsUseTemperatureControl(e, tableSelectedPeriod.value, 'isUseExpectTemperatureControl', TAB_TABLE)}
                                       className="box-shape-label lc-not-allow"/>
                                <label htmlFor="useExpectTemperatureControl"
                                       onClick={e => handleTableIsUseTemperatureControl(e, tableSelectedPeriod.value, 'isUseExpectTemperatureControl', TAB_TABLE)}
                                       className="text-label">희망온도 자동조절값 설정</label>
                            </div>
                        </div>
                        {tempTableControls[tableSelectedPeriod.value].isUseExpectTemperatureControl ? (
                            <Fragment>
                                <div className="detail-item option-value-form">
                                    <div className="detail-temperature-item">
                                        <label className="detail-temperature-label">최대(℃)</label>
                                        <div className="select-module-wrap">
                                            <input type="text" className="lc-farm-input"
                                                   value={tempTableControls[tableSelectedPeriod.value].expect_temperature_max_value}
                                                   onChange={e => handleTableInput(e, tableSelectedPeriod.value, 'expect_temperature_max_value', TAB_TABLE)}
                                                   placeholder="최대 값"/>
                                        </div>
                                    </div>
                                    <div className="detail-temperature-item">
                                        <label className="detail-temperature-label">최소(℃)</label>
                                        <div className="select-module-wrap">
                                            <input type="text" className="lc-farm-input"
                                                   value={tempTableControls[tableSelectedPeriod.value].expect_temperature_min_value}
                                                   onChange={e => handleTableInput(e, tableSelectedPeriod.value, 'expect_temperature_min_value', TAB_TABLE)}
                                                   placeholder="최소 값"/>
                                        </div>
                                    </div>
                                    <br/>
                                </div>
                                <ul>
                                {tempTableControls[tableSelectedPeriod.value].tableSensors && tempTableControls[tableSelectedPeriod.value].tableSensors.map((data, index) => {
                                    return (
                                        <li key={index} className="auto-control-sensor-form-wrap">
                                            <div className="auto-control-sensor-button-wrap">
                                                <button className="theme-text-button" onClick={e => removeTableControlSensors(e, tableSelectedPeriod.value, index)}>삭제</button>
                                            </div>
                                            <div className="detail-item">
                                                <div className="detail-temperature-item">
                                                    <label className="detail-temperature-label">자동조절 기준 센서 {index + 1}</label>
                                                    <div className="select-module-wrap">
                                                        <Select
                                                            value={data.selectedAutoControlSensor}
                                                            placeholder="센서 선택"
                                                            onChange={e => handleTableSensorSelect(e, tableSelectedPeriod.value, index, 'selectedAutoControlSensor', TAB_TABLE)}
                                                            isSearchable={false}
                                                            options={selectAutoControlSensorOption}
                                                            styles={colourStyles}/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="detail-item">
                                                <div className="detail-temperature-item">
                                                    <label className="detail-temperature-label">최대{data.selectedAutoControlSensor && data.selectedAutoControlSensor.origin.unit ? '('+ data.selectedAutoControlSensor.origin.unit +')' : null}</label>
                                                    <div className="select-module-wrap">
                                                        <input type="text" className="lc-farm-input"
                                                               value={data.max_value}
                                                               onChange={e => handleTableSensorInput(e, tableSelectedPeriod.value, index, 'max_value', TAB_TABLE)}
                                                               placeholder="최대 값"/>
                                                    </div>
                                                </div>
                                                <div className="detail-temperature-item">
                                                    <label className="detail-temperature-label">최소{data.selectedAutoControlSensor && data.selectedAutoControlSensor.origin.unit ? '('+ data.selectedAutoControlSensor.origin.unit +')' : null}</label>
                                                    <div className="select-module-wrap">
                                                        <input type="text" className="lc-farm-input"
                                                               value={data.min_value}
                                                               onChange={e => handleTableSensorInput(e, tableSelectedPeriod.value, index,  'min_value', TAB_TABLE)}
                                                               placeholder="최소 값"/>
                                                    </div>
                                                </div>
                                                <br/>
                                            </div>
                                            <div className="detail-item">
                                                <div className="detail-temperature-item">
                                                    <label className="detail-temperature-label">실행비율(%)</label>
                                                    <div className="select-module-wrap">
                                                        <input type="text" className="lc-farm-input"
                                                               value={data.percentage}
                                                               onChange={e => handleTableSensorInput(e, tableSelectedPeriod.value, index,  'percentage', TAB_TABLE)}
                                                               placeholder="실행비율 값"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    )
                                })}
                                </ul>

                                <div className="auto-control-sensor-button-wrap">
                                    <button className="theme-text-button" onClick={e => addTableControlSensors(e, tableSelectedPeriod.value)}>+ 자동조절 값 추가</button>
                                </div>
                            </Fragment>
                        ) : null
                        }
                    </div>
                        : null}
                </div>
                <div className="button-wrap">
                    <button className="theme-button" onClick={e => tempSubmit(e, tableSelectedPeriod.value)}>주기 설정 저장</button>
                </div>
            </div>
            : null
        }
        <div className="temperature-option-graph-wrap">
            <div className="temperature-option-graph-header">
                {tableAvgTemeperature ?
                    <p className="lc-average">{tableSelectedIsUseTemperature === 'vantilation' ? '환기' : '난방'} 온도 평균값 : {tableAvgTemeperature}℃</p>
                    : null
                }
            </div>
            <div className="button-wrap">
                <button className="theme-button" onClick={openTemperatureGraphModal}>설정 온도 그래프 보기</button>
            </div>
        </div>

    </div>
);

export default ControlTable;
