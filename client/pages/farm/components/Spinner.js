import React, {Component, Fragment} from 'react';

class Spinner extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {

    }

    render() {
        return (
            <Fragment>
                {
                    this.props.spinning ?
                        (
                            <div className={"spinner-overlay" + (this.props.isDark ? " is-dark" : "")}>
                                <div className={"lds-spinner" + (this.props.size === "big" ? " lc-big" : "")}>
                                    <div/>
                                    <div/>
                                    <div/>
                                    <div/>
                                    <div/>
                                    <div/>
                                    <div/>
                                    <div/>
                                    <div/>
                                    <div/>
                                    <div/>
                                    <div/>
                                </div>
                                {
                                    this.props.isDark ?
                                        <div className="spinner-logo">
                                            <img src="/public/images/header/logo@2x.png" alt="farmlabs logo"/>
                                        </div>
                                        : null
                                }
                            </div>
                        ) : null
                }
            </Fragment>
        )
    }
}

export default Spinner;