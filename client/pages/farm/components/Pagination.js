import React, {Component} from 'react';
import constant from '../constants/constant';

const NUM_LENGTH = constant.defaultNumberPageLength;
const DEFAULT_SIZE = constant.defaultLoadSize;

class Pagination extends Component {
    createNumberPage = (START_NUM, CURRENT_NUM, END_NUM) => {
        let render = [];
        for (let i = 0; i <= END_NUM - START_NUM; i++) {
            render.push(<li className={START_NUM + i === CURRENT_NUM ? 'lc-active' : null} key={i}>
                <button type="button" className="lc-pagination-number-btn"
                        onClick={(e) => this.goPage(e, START_NUM + i)}><span>{START_NUM + i}</span></button>
            </li>);
        }
        return render;
    };

    goPage = (e, page) => {
        e.stopPropagation();
        const SIZE = this.props.size ? this.props.size : DEFAULT_SIZE;
        if ((this.props.offset / SIZE + 1) !== page) {
            this.props.searchPage((page - 1) * SIZE);
        }
    };

    render() {
        const COUNT = this.props.count;
        const SIZE = this.props.size ? this.props.size : DEFAULT_SIZE;

        const CURRENT_NUM = this.props.offset / SIZE + 1;
        
        const START_NUM = Math.floor((CURRENT_NUM - 1) / NUM_LENGTH) * NUM_LENGTH + 1;

        const END_PAGE_NUM = START_NUM + NUM_LENGTH - 1;
        const END_COUNT_NUM = (Math.floor(COUNT / SIZE) + (COUNT % SIZE ? 1 : 0)) || 1;

        const END_NUM = END_PAGE_NUM < END_COUNT_NUM ? END_PAGE_NUM : END_COUNT_NUM;
        return (
            <div className="lc-pagination-container">
                <div className="lc-pagination-wrap">
                    {
                        START_NUM > 1 ?
                            (
                                <button className="lc-pagination-btn left" onClick={(e) => this.goPage(e, START_NUM - 1)}/>
                            ) : null
                    }
                    <ol className="lc-pagination-list">
                        {this.createNumberPage(START_NUM, CURRENT_NUM, END_NUM)}
                    </ol>
                    {
                        END_PAGE_NUM < END_COUNT_NUM ?
                            (
                                <button className="lc-pagination-btn right" onClick={(e) => this.goPage(e, END_NUM + 1)}/>
                            ) : null
                    }
                </div>
            </div>
        );
    }
}

export default Pagination;