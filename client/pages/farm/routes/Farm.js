import React, {Component, Fragment} from 'react';

import {getQuery} from "../../../utils/route";
import {useCheckPanel} from "../contexts/checkPanel";
import {useAlert} from "../contexts/alert";
import {useSocket} from "../contexts/socket";
import {useHouse} from "../contexts/house";
import {useNavScroll} from "../contexts/navScroll";
import Pesticide from '../articles/pesticide'
import Cctv from '../articles/Cctv';
import Remote from '../articles/Remote';
import AutoControl from '../articles/AutoControl';
import Motor from '../articles/Motor';
import Power from '../articles/Power';
import HouseInfo from '../articles/HouseInfo';
import OuterSensor from '../articles/OuterSensor';
import InnerSensor from '../articles/InnerSensor';
import TotalGraph from '../articles/TotalGraph';
import Notice from '../articles/Notice';
import Journal from '../articles/Journal';
import ExcelExport from '../articles/ExcelExport';
import FluidIframe from '../articles/FluidIframe';
import InnerSensorAvg from '../articles/InnerSensorAvg';
import OuterSensorAvg from '../articles/OuterSensorAvg';
import {addListener, on} from "../utils/socket";

import screenManager from '../managers/screen';
import refreshManager from '../managers/refresh';
import href from '../../../utils/href';
import {noSession} from "../utils/session";

import constant from '../constants/constant';
import checkMobile from "../../../utils/checkMobile";

class Farm extends Component {
    constructor(props) {
        super(props);
        let query = location.search.substr(1);
        let queryArr = query.split("&");
        this.isPanel = queryArr.indexOf("p") !== -1;
        if(this.isPanel) {
            let HTML = document.getElementsByTagName('html')[0];
            HTML.classList.add('lc-panel');
            document.body.classList.add('lc-panel');
        }
        this.timeIntervalInstance = null;
        const screens = window.screens;
        const length = Math.floor(screens.length / 3);
        this.state = {
            house_id: props.getHouseId(),
            screens,
            //firstLength: length + (screens.length % 3 ? 0 : 0),
            firstLength:4,
            secondLength:10,
            grade : window.user ? (window.user.grade?window.user.grade:'member') : "member"
            //secondLength: length * 2 + (screens.length % 3)
        };
    }

    componentDidMount() {
        if(this.isPanel || (checkMobile() && window.innerWidth > 600)) {
            this.setTimeIntervalInstance();
            window.addEventListener('touchstart', this.touchInterval);
            window.addEventListener('mousemove', this.touchInterval);
            window.addEventListener('click', this.touchInterval);
        }
        const _this = this;
        this.props.setRootCallback(this.changeHouseId);
        _this.findScreens();
        this.resizeScreen();
        window.addEventListener('resize', this.resizeScreen);

        addListener(this, this.listener);
        setInterval(() => {
            refreshManager.check({}, noSession(this.props.showDialog)((status, data) => {
                if (status === 200) {
                    if (data.data.current !== window.current) {
                        location.reload();
                    }
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        }, constant.checkRefreshPeriod);
    }

    componentWillUnmount() {
        const socket = this.props.socket;
        window.removeEventListener('resize', this.resizeScreen);
        if(this.isPanel || (checkMobile() && window.innerWidth > 600)) {
            window.removeEventListener('touchstart', this.touchInterval);
            window.removeEventListener('mousemove', this.touchInterval);
            window.removeEventListener('click', this.touchInterval);
            this.clearTimeIntervalInstance();
        }
        socket.removeListener('screens');
    }

    touchInterval = () => {
        this.clearTimeIntervalInstance();
        this.setTimeIntervalInstance();
    };

    setTimeIntervalInstance = () => {
        const _this = this;
        this.timeIntervalInstance = setInterval(() => {
            _this.clearTimeIntervalInstance();
            href("/farm/screen?p");
        }, (1000 * 60 * 5));
    };

    clearTimeIntervalInstance = () => {
        if(this.timeIntervalInstance) clearInterval(this.timeIntervalInstance);
    };

    resizeScreen = (e) => {
        this.setState({
            innerWidth: window.innerWidth
        });
    };

    changeHouseId = (house_id) => {
        const _this = this;
        this.setState({house_id}, () => {
            _this.findScreens();
        });
    };

    listener = (socket) => {
        const _this = this;
        on(socket)('screens', (data) => {
            if(data && data.house_id && data.house_id === _this.state.house_id) {
                _this.findScreens();
            }
            if(!data || data.house_id === undefined) {
                _this.findScreens();
            }
        });
    };

    findScreens = () => {
        const _this = this;
        screenManager.findAll({
            is_visible: true,
            house_id: _this.state.house_id
        }, noSession(this.props.showDialog)((status, data) => {
            if (status === 200) {
              
                const screens = data.data.rows;
                const length = Math.floor(screens.length / 3);                
                _this.setState({
                    screens,
                   // firstLength: length + (screens.length % 3 ? 0 : 0),
                  //  secondLength: length * 2 + (screens.length % 3)
                  firstLength:4,
                  secondLength:10,
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    renderArticle = ({type}, key) => {
        switch(type) {
            case 'cctv':                
                return this.state.grade=='owner'? (<Cctv isPanel={this.isPanel} key={key}/>):null
            case 'control':
                return this.state.grade=='owner'? (                    
                    <Fragment key={key}>
                        <Remote/>
                        <AutoControl/>
                    </Fragment>
                ):null;
            case 'motor':
                return (<Motor key={key}/>);
            case 'power':
                return (<Power key={key}/>);
            case 'house':
                return this.state.innerWidth >= 1201 ? (<HouseInfo isPanel={this.isPanel} key={key}/>) : null;
            case 'outerSensorAvg' :
                return this.state.grade=='owner'?(<OuterSensorAvg key = {key}/>):null;
            case 'outerSensor':
                return (<OuterSensor key={key}/>);
            case 'innerSensorAvg' :
                return this.state.grade=='owner'?(<InnerSensorAvg key = {key}/>):null;
            case 'innerSensor':
                return (<InnerSensor key={key}/>);
            case 'chart':
                return (<TotalGraph key={key}/>);
            case 'notice':
                return (<Notice key={key}/>);
            case 'report':
                return (<Journal key={key}/>);
            case 'csv':
                return (
                    <Fragment key={key}>
                        { this.isPanel || this.state.grade!='owner'?
                            null :
                            <ExcelExport/>
                        }
                    </Fragment>
                );
            case 'pesticide':
                return this.state.grade=='owner'? (<Pesticide key={key}/>) :null;

            case 'fluid' :
                return (<FluidIframe key = {key}/>);

            
        }
    };

    render() {
        return (
            <article id="lcFarmWrap">
                <ul id="lcListContainer">
                    <li>
                        {
                            this.state.innerWidth < 1201 ?
                                <div className="fixed-on-mobile">
                                    <HouseInfo isPanel={this.isPanel}/>
                                </div> : null
                        }
                        { this.state.screens.map((screen, index) => {
                            if (index < this.state.firstLength) {                                
                                return this.renderArticle(screen, index);
                            } else {
                            }
                        })}
                    </li>
                    <li>
                        { this.state.screens.map((screen, index) => {
                            if (index >= this.state.firstLength && index < this.state.secondLength) {
                                return this.renderArticle(screen, index);
                            } else {
                                return null;
                            }
                        })}
                    </li>
                    <li>
                        { this.state.screens.map((screen, index) => {
                            if (index >= this.state.secondLength) {
                                return this.renderArticle(screen, index);
                            } else {
                                return null;
                            }
                        })}
                    </li>
                </ul>
            </article>
        );
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId,
        setRootCallback: actions.setRootCallback
    })
)(useAlert(
    ({state, actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(useSocket(
    ({state, actions}) => ({
        socket: state.socket,
        setSocket: actions.setSocket
    })
)(useCheckPanel(
    ({state, actions}) => ({
        isPanel: state.isPanel,
        setIsPanel: actions.setIsPanel
    })
)(Farm))));
