import React, {Component, Fragment} from 'react';
import {Redirect} from 'react-router-dom';
import {useSocket} from "../contexts/socket";
import {useHouse} from "../contexts/house";
import {useAlert} from "../contexts/alert";
import sensorManager from '../managers/sensor';
import alarmManager from "../managers/alarm";
import {noSession} from "../utils/session";
import deepcopy from "../../../utils/deepcopy";
import {setValue} from "../utils/sensorHandler";
import href from '../../../utils/href';
import {sanitize} from "../utils/sensor";
import refreshManager from "../managers/refresh";
import constant from "../constants/constant";

const SENSOR_POSITION_IN = "in";
const SENSOR_POSITION_OUT = "out";

class ScreenSaver extends Component {
    constructor(props) {
        super(props);

        let query = location.search.substr(1);
        let queryArr = query.split("&");
        this.isPanel = queryArr.indexOf("p") !== -1;
        if(!this.isPanel) {
            href(location.protocol + '//' + location.host + '/' + 'farm');
        }
        this.alarmHash = {};
        this.state = {
            touched: false,
            sensorIndex: 0,
            sensors: []
        };
    }

    componentDidMount() {
        const _this = this;
        setInterval(() => {
            refreshManager.check({}, noSession(this.props.showDialog)((status, data) => {
                if (status === 200) {
                    if (data.data.current !== window.current) {
                        location.reload();
                    }
                } else {
                    _this.props.alertError(status, data);
                }
            }));
        }, constant.checkRefreshPeriod);
        const sensorFindInterval = setInterval(() => this.findAllSensors(), (1000 * 60));
        this.setState({sensorFindInterval});
        window.addEventListener('touchstart', this.touchPanel);
        window.addEventListener('mousemove', this.touchPanel);
        window.addEventListener('click', this.touchPanel);
        this.findAllSensors();
    }

    componentWillUnmount() {
        clearInterval(this.timeInterval);
        clearInterval(this.state.sensorFindInterval);
        window.removeEventListener('touchstart', this.touchPanel);
        window.removeEventListener('mousemove', this.touchPanel);
        window.removeEventListener('click', this.touchPanel);
    }

    touchPanel = () => {
        this.setState({
            touched: true
        });

        href('/farm');
    };

    findAllSensors = () => {
        const _this = this;
        let query = {
            isTotal: true
        };
        sensorManager.findAll(query, noSession(this.props.showDialog)((status, data) => {
            if(status === 200) {
                _this.setState({
                    sensors: data.data.rows
                }, () => {
                    _this.drawSensorsByTimeout();
                });
            } else if (status === 404) {
                _this.setState({
                    sensors: []
                });
            } else {
                _this.props.alertError(status, data);
            }
        }));
    };

    drawSensorsByTimeout = () => {
        const _this = this;
        if(!this.timeInterval) {
            this.timeInterval = setInterval(() => {
                let sensors = deepcopy(_this.state.sensors);
                if(sensors.length) {
                    let length = sensors.length;
                    let sensorIndex = deepcopy(_this.state.sensorIndex);
                    if(_this.state.sensorIndex === length-1) {
                        sensorIndex = 0;
                    } else {
                        sensorIndex++;
                    }
                    _this.setState({sensorIndex});
                }
            }, 3000);
        }
    };

    render() {
        return (
            <Fragment>
                {
                    this.state.touched ? <Redirect to="/farm"/> : null
                }
                <article id="lcScreenSaver">
                    {
                        this.state.sensors && this.state.sensors.length ?
                            (
                                <div className="sensor-info-wrap">
                                    <span className="sensor-name">{this.state.sensors[this.state.sensorIndex].sensor_name}</span>
                                    <span className="sensor-value">{sanitize(this.state.sensors[this.state.sensorIndex])}</span>
                                </div>
                            ) : null
                    }
                    <img src="/public/images/farm/screen-saver-logo@2x.png" alt="로고" className="logo-img"/>
                </article>
            </Fragment>
        )
    }
}

export default useHouse(
    ({actions}) => ({
        getHouseId: actions.getHouseId
    })
)(useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(ScreenSaver));
