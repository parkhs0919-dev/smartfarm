import React, { Component } from 'react';
import { useBreedDetail } from "../contexts/modals/breedDetail";
import { useAlert } from "../contexts/alert";
import { getPath, returnPath, returnQuery, getQuery } from "../../../utils/route";

import href from "../../../utils/href";
import entityManager from '../managers/entity';
import CONSTANT from '../constants/constant';
import deepcopy from '../../../utils/deepcopy';
import Pagination from "../components/Pagination";

const DEFAULT_QUERY = {
    offset: 0,
    size: CONSTANT.defaultLoadSize,
    //size: 2,
    category: {
        count: 0,
        rows: [],
    },
    currnet_category: 0,
    searchItem: '',

    animal_type_id: 1,
    animals: {
        count: 0,
        rows: [],
    },
    initialLoading: true
};

class Breed extends Component {
    constructor(props) {
        super(props);
        this.destroy = null;
        this.state = deepcopy(DEFAULT_QUERY);
    }

    componentDidMount() {
        const _this = this;
        this.findCategory();

        this.destroy = this.props.history.listen(() => {
            if (getPath() === 'animal/breed') {
                _this.findAnimals();
            }
        });
    }

    componentWillUnmount() {
        this.destroy();
        this.destroy = null;
        this.setState(deepcopy(DEFAULT_QUERY));
    }

    pagination = (offset) => {
        let query = {
            page: (offset / this.state.size) + 1
        };
        if (this.state.searchItem) query.searchItem = this.state.searchItem;
        this.props.history.push(returnPath(getPath()) + returnQuery(query));
    };

    getOffset = (index) => {
        return ((index - 1) * this.state.size);
    };

    findCategory = () => {
        const _this = this;
        this.setState({
            category: [],
            currnet_category: 0,
        });

        _this.setState(() => {
            let query = {
                size: 5,
                offset: 0
            };
            entityManager.getCategory(query, (status, data) => {
                if (status === 200) {
                    if (data.data.rows) {
                        data.data.rows.forEach((item, index) => {
                        });
                    }
                    _this.setState({
                        category: data.data,
                        currnet_category: data.data.rows[0].id
                    });
                    return this.findAnimals();
                } else if (status === 404) {
                    _this.props.alertError(status, data);
                } else {
                    _this.props.alertError(status, data);
                }
            });

        });

    }

    findAnimals = (callback = null) => {
        const _this = this;
        this.setState({
            animals: {
                count: 0,
                rows: [],
            },
            initialLoading: true,
        });

        let getQueryItem = getQuery();
        if (!getQueryItem.page) {
            getQueryItem.page = 1;
        }
        getQueryItem.offset = (getQueryItem.page - 1) * this.state.size;
        getQueryItem.searchItem = getQueryItem.searchItem ? getQueryItem.searchItem : undefined;

        _this.setState(getQueryItem, () => {
            let query = {
                size: this.state.size,
            };
            if (getQueryItem.page) {
                query.offset = this.state.offset;
            }

            entityManager.find_animalsByTypeID(this.state.animal_type_id, query, (status, data) => {
                if (status === 200) {
                    _this.setState({
                        animals: data.data,
                        initialLoading: false
                    });
                    if (callback) callback();
                } else if (status === 404) {
                    _this.setState({
                        initialLoading: false
                    });
                    if (callback) callback();
                } else {
                    _this.props.alertError(status, data);
                }
            });
        });
    };

    checkCurrentCategory = (id) => {
        if (this.state.currnet_category == id) {
            return "tab_on";
        } else {
            return "";
        }
    }

    changeCategory = (e) => {
        this.setState({
            animal_type_id: e.target.value,
            currnet_category: e.target.value
        })
        this.findAnimals();
    }

    openBreedDetailModal = (e, data) => {
        this.props.openBreedDetailModal(data, this.enableMotherModalClose);
    };

    disableMotherModalClose = () => {
        document.removeEventListener('keyup', this.closeOnEscape);
    };

    enableMotherModalClose = () => {
        this.disableMotherModalClose();
        this.findAnimals();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    render() {
        return (
            <article id="lcBreedWrap" className="lc-animal-route-wrap">
                <div className="lc-animal">
                    <ul className="lc-animal-tab">
                    {
                            this.state.category && this.state.category.rows && this.state.category.rows.length ?
                                (
                                    this.state.category.rows.map((type, index) => {
                                        return (
                                            <li className={this.checkCurrentCategory(type.id)} onClick={e => this.changeCategory(e)} value={type.id} key={index}>{type.animal_type_name}</li>
                                        )
                                    })
                                ) :
                                (
                                    <div className="no-data-wrap">카테고리 등록이 필요합니다.</div>
                                )
                        }
                    </ul>
                    <div className="lc-animal-list">
                    {
                            this.state.animals && this.state.animals.rows && this.state.animals.rows.length ?
                                (
                                    <table>
                                        <thead>
                                            <tr>
                                                <th className="entity-id">개체 ID</th>
                                                <th className="entity-status">발정</th>
                                                <th className="entity-status">수정</th>
                                                <th className="entity-status">임신감정</th>
                                                <th className="entity-status">건유</th>
                                                <th className="entity-status">분만</th>
                                                <th className="entity-birth on-pc">출생일</th>
                                                <th className="entity-move on-pc">전입일</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.animals.rows.map((animal, index) => {
                                                    return (
                                                        <tr key={index} onClick={e => this.openBreedDetailModal(e, animal)}>
                                                            <td>{animal.animal_code}</td>
                                                            <td>{animal.estrus != 0 && animal.estrus != null ? (<div className="circle-icon"/>): '-'}</td>
                                                            <td>{animal.insemination != 0 && animal.insemination != null ? (<div className="circle-icon"/>): '-'}</td>
                                                            <td>{animal.check != 0 && animal.check != null ? (<div className="circle-icon"/>): '-'}</td>
                                                            <td>{animal.dryUp != 0 && animal.dryUp != null ? (<div className="circle-icon"/>): '-'}</td>
                                                            <td>{animal.birth != 0 && animal.birth != null ? (<div className="circle-icon"/>): '-'}</td>
                                                            <td className="on-pc">{animal.birth_day}</td>
                                                            <td className="on-pc">{animal.move_day}</td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                ) :
                                (
                                    <div className="no-data-wrap">데이터가 없습니다.</div>
                                )
                        }                        
                        <div className="pagination">
                            {this.state.animals && this.state.animals.rows.length ?
                                <Pagination count={this.state.animals.count}
                                    offset={this.state.offset}
                                    size={this.state.size}
                                    searchPage={this.pagination} /> :
                                null
                            }
                        </div>
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useBreedDetail(
    ({ actions }) => ({
        openBreedDetailModal: actions.open
    })
)(Breed));