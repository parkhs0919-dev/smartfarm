import React, {Component} from 'react';
import {useEntityDetail} from "../contexts/modals/entityDetail";
import {useAlert} from "../contexts/alert";
import deepcopy from '../../../utils/deepcopy';

import {useWeight} from "../contexts/modals/weight";
import {useFeed} from "../contexts/modals/feed";
import {useSupplyWater} from "../contexts/modals/supplyWater";
import {useMateCount} from "../contexts/modals/mateCount";
import entityManager from '../managers/entity';
import moment from "moment";
import href from "../../../utils/href";

const DEFAULT_QUERY = {
    id:'',  
    weight:{
        count:0,
        rows:[]
    },
    feed:{
        count:0,
        rows:[]
    },
    supplyWater:{
        count:0,
        rows:[]
    },
    mateCount:{
        count:0,
        rows:[]
    },
    memo:'',
};

class EntityDetail extends Component {
    constructor(props) {
        super(props);
        this.destroy = null;
        this.state = deepcopy(DEFAULT_QUERY);
        this.state = {
            animal_id: this.props.match.params.animal_id,
            entity:{
                animal_id: this.props.match.params.animal_id,
                animal_code: '',
                no: '',
                barcode: '',
                kind: '',   
                weight: '',
                health_state: '',
                breed_type: '',
                birth_day: '',
                move_day: '',
                mate_count:'',
                image_url:'',
                cnt:'',
                type_check:'',
                species:'',
            },
            weight:{
                count:0,
                rows:[]
            },
            feed:{
                count:0,
                rows:[]
            },
            supplyWater:{
                count:0,
                rows:[]
            },
            mateCount:{
                count:0,
                rows:[]
            },
            memo:'',
        };
    }

    close = (e) => {
        if (e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        href('/animal');
    };

    closeOnEscape = (e) => {        
        if (e.keyCode === 27) href('/animal');;
    };

    componentDidMount() {
        this._findEntityById();
    }

    disableMotherModalClose = () => {
        document.removeEventListener('keyup', this.closeOnEscape);
    };

    enableMotherModalClose = () => {
        this._findEntityById();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    _openEntityDetailModal = (e,data) => {
        this.disableMotherModalClose();
        this.props._openEntityDetailModal(data,this.enableMotherModalClose);
    };
    _openMateCountModal = (e,id) => {
        this.props._openMateCountModal(id);
    };
    _openSupplyWaterModal = (e,id) => {
        this.props._openSupplyWaterModal(id);
    };
    _openFeedModal = (e,id) => {
        this.props._openFeedModal(id);
    };
    _openWeightModal = (e,id) => {
        this.props._openWeightModal(id);
    };

    _findWeightList= () =>{
        const _this = this;
        let query = {
            animal_id: this.state.animal_id,
            offset:''
        }
        entityManager.list_weight(query, (status, data) => {
            if (status === 200) {
                _this.setState({
                    weight:data.data,
                })
            } else if (status === 404) {
                _this.props.alertError(status, data,"체중");
            } else {
                _this.props.alertError(status, data,"체중");
            }
        });
    }

    _findFeedList= () =>{
        const _this = this;
        let query = {
            animal_id: this.state.animal_id,
            offset:''
        }
        entityManager.list_feed(query, (status, data) => {
            if (status === 200) {
                _this.setState({
                    feed:data.data,
                })
            } else if (status === 404) {
                _this.props.alertError(status, data,"급이");
            } else {
                _this.props.alertError(status, data,"급이");
            }
        });
    }

    _findSupplyWaterList= () =>{
        const _this = this;
        let query = {
            animal_id: this.state.animal_id,
            offset:''
        }
        entityManager.list_supplyWater(query, (status, data) => {
            if (status === 200) {
                _this.setState({
                    supplyWater:data.data,
                })
            } else if (status === 404) {
                _this.props.alertError(status, data,"급수");
            } else {
                _this.props.alertError(status, data,"급수");
            }
        });
    }

    _findMateCountList= () =>{
        const _this = this;
        let query = {
            animal_id: this.state.animal_id,
            offset:''
        }
        entityManager.list_mateCount(query, (status, data) => {
            if (status === 200) {
                _this.setState({
                    mateCount:data.data,
                })
            } else if (status === 404) {
                _this.props.alertError(status, data,"교배차수");
            } else {
                _this.props.alertError(status, data,"교배차수");
            }
        });
    }
    _findImage=(typeid)=>{
        let entity = deepcopy(this.state.entity)
        
        let query = {
            size: 5,
            offset: 0
        };
        
        entityManager.getCategory(query, (status, data) => {
            if (status === 200) {
                 
                if (data.data.rows) {
                    let default_image='',species='';
                    data.data.rows.forEach((item, index) => {
                        if(typeid==item.id)
                        {
                            if(item.species=='cow'){
                                default_image='/public/images/animal/cow.png'
                                entity['species']=item.species
                            }else if(item.species=='pig'){
                                default_image='/public/images/animal/pig.png'
                                entity['species']=item.species
                            }else if(item.species=='horse'){
                                default_image='/public/images/animal/horse.png'
                                entity['species']=item.species
                            }else if(item.species=='sheep'){
                                default_image='/public/images/animal/sheep.png'
                                entity['species']=item.species
                            }else if(item.species=='goat'){
                                default_image='/public/images/animal/goat.png'
                                entity['species']=item.species
                            }
                            
                        }
                    });
            
                    this.setState({
                        entity,
                        type_check:default_image,
                    });
                }
                
                
                
            }
        });
    }
    _findEntityById=(callback = null)=>{
        const _this = this;
        entityManager.findById(this.state.animal_id, (status, data) => {
            if (status === 200) {
                if (data.data.rows) {
                    data.data.rows.forEach((item, index) => {
                        switch (item.health_state) {
                            case "good":
                                item.hs_name = "좋음";
                                break;
                            case "normal":
                                item.hs_name = "보통";
                                break;
                            case "bad":
                                item.hs_name = "나쁨";
                                break;
                        }          

                        item.breed_name = '';
                        if(item.check != 0 && item.check != null)
                            item.breed_name += '임신감정, ';
                        if(item.estrus != 0 && item.estrus != null)
                            item.breed_name += '발정, ';
                        if(item.insemination != 0 && item.insemination != null)
                            item.breed_name += '수정, ';
                        if(item.dryUp != 0 && item.dryUp != null)
                            item.breed_name += '건유, ';
                        if(item.birth != 0 && item.birth != null)
                            item.breed_name += '분만, ';
                        
                        item.breed_name =item.breed_name.substr(0, item.breed_name.length - 2)
                    });
                }
                
                _this.setState({
                    entity:{
                        id: this.props.match.params.animal_id,
                        animal_id: this.props.match.params.animal_id,
                        animal_code: data.data.rows[0].animal_code == null ? '':data.data.rows[0].animal_code,
                        no: data.data.rows[0].no == null ? '':data.data.rows[0].no,
                        barcode: data.data.rows[0].barcode == null ? '':data.data.rows[0].barcode,
                        kind: data.data.rows[0].kind == null ? '':data.data.rows[0].kind,
                        weight: data.data.rows[0].weight == null ? '':data.data.rows[0].weight,
                        health_state: data.data.rows[0].health_state == null ? '':data.data.rows[0].health_state,
                        breed_type: data.data.rows[0].breed_name == null ? '':data.data.rows[0].breed_name,
                        birth_day: data.data.rows[0].birth_day == null ? '':data.data.rows[0].birth_day,
                        move_day: data.data.rows[0].move_day == null ? '':data.data.rows[0].move_day,
                        mate_count: data.data.rows[0].cnt == null ? '':data.data.rows[0].cnt,
                        image_url: data.data.rows[0].image_url == null ? '':data.data.rows[0].image_url,
                        hs_name: data.data.rows[0].hs_name == null ? '':data.data.rows[0].hs_name,
                        species:'',
                    },                   
                    memo: data.data.rows[0].memo == null ? '':data.data.rows[0].memo,
                });

                this._findWeightList();
                this._findFeedList();
                this._findSupplyWaterList();
                this._findMateCountList();
                this._findImage(data.data.rows[0].animal_type_id);
                if (callback) callback();
            } else if (status === 404) {
                if (callback) callback();
            } else {
                _this.props.alertError(status, data);
            }
        });
    }

    _removeEntity = e =>{
        const _this = this;
        _this.props.showDialog({
            alertText: '이 개체정보를 삭제하시겠습니까?',
            cancelText: '아니오',
            submitText: '예',
            submitCallback: () => {
                entityManager.remove(this.state.animal_id, (status, data) => {
                    if(status === 200) {
                        _this.props.showDialog({
                            alertText: '개체정보가 삭제되었습니다.',
                            cancelText: "확인",
                            cancelCallback: () => {
                                href('/animal');
                            }
                        })
                    } else {
                        _this.props.alertError(status, data);
                    }
                });
            }
        });       
    }

    _handleInput = (e, key) => {
        let item = {};
        if(key == "memo"){
            if(e.target.value.length < 100){
                item[key] = e.target.value;
            }
        }else{
            item[key] = e.target.value;
        }
        this.setState(item);
    };

    _updateMemo = ()=>{
        const _this = this;
        let query = {
            animal_id: this.state.animal_id,
            memo: this.state.memo,
        };
        
        entityManager.update_memo(query, (status, data) => {
            if(status === 200) {               
                _this.props.showDialog({
                    alertText: '메모가 저장 되었습니다.',
                    cancelText: "확인",
                    cancelCallback: () => {
                        window.location.reload();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        });
    }

    _removeHistory=(key,id)=>{
        switch (key){
            case "weight" : 

        }
        const _this = this;
        _this.props.showDialog({
            alertText: '이 개체정보를 삭제하시겠습니까?',
            cancelText: '아니오',
            submitText: '예',
            submitCallback: () => {
                entityManager.remove_histroy(key,id, (status, data) => {
                    if(status === 200) {
                        _this.props.showDialog({
                            alertText: '개체정보가 삭제되었습니다.',
                            cancelText: "확인",
                            cancelCallback: () => {
                                this._findEntityById();
                            }
                        })
                    } else {
                        _this.props.alertError(status, data);
                    }
                });
            }
        });      
    }

    render() {
       
        return (
            <article id="lcEntityDetailWrap" className="lc-animal-route-wrap">
                <div className="animal">
                    <div className="animal-detail">
                        <div className="animal-info">
                            <div className="animal-info-img">
                                {
                                    this.state.entity.image_url != '' && this.state.entity.image_url != null ?
                                        (
                                            <img src={"/uploads/animal/" + this.state.entity.image_url} />
                                        ) : (
                                            <img src={this.state.type_check}/>
                                        )
                                }
                            </div>
                            <div className="animal-info-txt">
                                <div className="animal-info-detail">
                                    <ul>
                                        <li>
                                            <h2 className="id-txt">개체 ID</h2>
                                            <h2 className="id-number">{this.state.entity.animal_code}</h2>                                  
                                        </li>
                                        <li>
                                            <h3 className="detail-txt">모우(돈) ID</h3>
                                            <p className="detail-number">{this.state.entity.no}</p>
                                        </li>
                                        <li>
                                            <h3 className="detail-txt">바코드</h3>
                                            <p className="detail-number">{this.state.entity.barcode}</p>
                                        </li>
                                    </ul>
                                </div>                                
                                <div className="animal-info-btn-area">
                                    <button className="btn-line mr-12" onClick={e => this._removeEntity(e)}>개체 삭제</button>
                                    <button className="btn-line" onClick={e=>this._openEntityDetailModal(e, this.state.entity)}>정보 수정</button>
                                </div>
                            </div>
                            <div className="button-area">
                                    <button type="button"
                                            className="closeBtn"
                                            onClick={this.close}/>
                            </div>
                            <div className="button-area-mobile">
                                    <button type="button"
                                            className="closeBtn"
                                            onClick={this.close}/>
                            </div>
                            <div className="animal-info-btn-mobile">
                                <button className="btn-line mr-12" onClick={e => this._removeEntity(e)}>개체 삭제</button>
                                <button className="btn-line" onClick={e=>this._openEntityDetailModal(e, this.state.entity)}>정보 수정</button>
                            </div>

                        </div>
                        <div className="animal-entity-detail">
                            <h3 className="info-label">개체정보</h3>
                            <ul>
                                <li>
                                    <span className="title">품종</span>
                                    <span className="txt">{this.state.entity.kind}</span>
                                </li>
                                <li>
                                    <span className="title">체중</span>
                                    <span className="txt">{this.state.entity.weight} Kg</span>                                  
                                </li>
                                <li>
                                    <span className="title">건강</span>
                                    <span className="txt">{this.state.entity.hs_name}</span>
                                </li>
                                <li>
                                    <span className="title">번식</span>
                                    <span className="breed-txt">{this.state.entity.breed_type}</span>
                                </li>
                                <li>
                                    <span className="title">출생일</span>
                                    <span className="txt">{this.state.entity.birth_day}</span>
                                </li>
                                <li>
                                    <span className="title">전입일</span>
                                    <span className="txt">{this.state.entity.move_day}</span>
                                </li>
                                <li>
                                    <span className="title">교배 차수</span>
                                    <span className="txt">총 {this.state.entity.mate_count}회</span>
                                </li>
                            </ul>
                            {/*<table>*/}
                                {/*<tbody>*/}
                                {/*<tr>*/}
                                    {/*<th>품종</th>*/}
                                    {/*<td>0000</td>*/}
                                    {/*<th>체중</th>*/}
                                    {/*<td>500kg</td>*/}
                                    {/*<th>건강</th>*/}
                                    {/*<td>보통</td>*/}
                                {/*</tr>*/}
                                {/*<tr>*/}
                                    {/*<th>번식</th>*/}
                                    {/*<td>임신,건유</td>*/}
                                    {/*<th>출생일</th>*/}
                                    {/*<td>2018.12.31</td>*/}
                                    {/*<th>전입일</th>*/}
                                    {/*<td>2019.01.01</td>*/}
                                {/*</tr>*/}
                                {/*</tbody>*/}
                            {/*</table>*/}
                        </div>
                    </div>
                    <div className="animal-manage">
                        <div className="manage-entity mr-22">
                            <div className="manage-entity-btn-area">
                                <h3 className="info-label">체중</h3>
                                <button className="btn-line" onClick={e => this._openWeightModal(e, this.state.animal_id)}>입력</button>
                            </div>

                            {
                                this.state.weight && this.state.weight.rows && this.state.weight.count ?
                                    (
                                        <div className="manage-entity-list exists">
                                            <ul>
                                                {
                                                    this.state.weight.rows.map((info, index) => {
                                                        return (
                                                            <li key={index}>
                                                                <span className="date">{moment(info.date).format('YYYY.MM.DD')}</span>
                                                                <span className="dot">·</span>
                                                                <span className="manage-type">{info.weight}kg</span>
                                                                <span className="dot">·</span>
                                                                <span className="weight-note">{info.memo}</span>

                                                                <div className="button-sub-area">
                                                                    <button type="button"
                                                                        className="subCloseBtn"
                                                                        onClick={e =>this._removeHistory("weights",info.id)} />
                                                                </div>
                                                            </li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    ) :
                                    (
                                        <div className="manage-entity-list not-exists">
                                            <h5>등록된 체중 정보가 없습니다.</h5>
                                        </div>
                                    )
                            }
                        </div>
                        <div className="manage-entity">
                            <div className="manage-entity-btn-area">
                                <h3 className="info-label">급이</h3>
                                <button className="btn-line" onClick={e => this._openFeedModal(e, this.state.animal_id)}>입력</button>
                            </div>
                            {
                                this.state.feed && this.state.feed.rows && this.state.feed.count ?
                                    (
                                        <div className="manage-entity-list exists">
                                            <ul>
                                                {
                                                    this.state.feed.rows.map((info, index) => {
                                                        return (
                                                            <li key={index}>
                                                                <span className="date">{moment(info.date).format('YYYY.MM.DD HH:mm')}</span>
                                                                <span className="dot">·</span>
                                                                <span className="manage-type">{info.feed}</span>
                                                                <span className="manage-type">{info.unit}</span>
                                                                <span className="dot">·</span>
                                                                <span className="feed-note">{info.memo}</span>

                                                                <div className="button-sub-area">
                                                                    <button type="button"
                                                                        className="subCloseBtn"
                                                                        onClick={e =>this._removeHistory("feeds",info.id)} />
                                                                </div>
                                                            </li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    ) :
                                    (
                                        <div className="manage-entity-list not-exists">
                                            <h5>등록된 급이 정보가 없습니다.</h5>
                                        </div>
                                    )
                            }                           
                            
                        </div>
                        <div className="manage-entity mr-22 mt-57">
                            <div className="manage-entity-btn-area">
                                <h3 className="info-label">급수</h3>
                                <button className="btn-line" onClick={e=> this._openSupplyWaterModal(e, this.state.animal_id)}>입력</button>
                            </div>
                            {
                                this.state.supplyWater && this.state.supplyWater.rows && this.state.supplyWater.count ?
                                    (
                                        <div className="manage-entity-list exists">
                                            <ul>
                                                {
                                                    this.state.supplyWater.rows.map((info, index) => {
                                                        return (
                                                            <li key={index}>
                                                                <span className="date">{moment(info.date).format('YYYY.MM.DD HH:mm')}</span>
                                                                <span className="dot">·</span>
                                                                <span className="manage-type">{info.water}L</span>
                                                                <span className="dot">·</span>
                                                                <span className="water-note">{info.memo}</span>

                                                                <div className="button-sub-area">
                                                                    <button type="button"
                                                                        className="subCloseBtn"
                                                                        onClick={e =>this._removeHistory("waters",info.id)} />
                                                                </div>
                                                            </li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    ) :
                                    (
                                        <div className="manage-entity-list not-exists">
                                            <h5>등록된 급수 정보가 없습니다.</h5>
                                        </div>
                                    )
                            }            
                        </div>
                        <div className="manage-entity mt-57">
                            <div className="manage-entity-btn-area">
                                <h3 className="info-label">교배차수</h3>
                                <button className="btn-line" onClick={e =>this._openMateCountModal(e, this.state.animal_id)}>입력</button>
                            </div>
                            {
                                this.state.mateCount && this.state.mateCount.rows && this.state.mateCount.count ?
                                    (
                                        <div className="manage-entity-list exists">
                                            <ul>
                                                {
                                                    this.state.mateCount.rows.map((info, index) => {
                                                        return (
                                                            <li key={index}>
                                                                <span className="date">{moment(info.date).format('YYYY.MM.DD')}</span>
                                                                <span className="dot">·</span>
                                                                <span className="manage-type">{this.state.mateCount.count-index}차</span>
                                                                <span className="dot">·</span>
                                                                <span className="mc-note">{info.memo}</span>
                                                                <div className="button-sub-area">
                                                                    <button type="button"
                                                                        className="subCloseBtn"
                                                                        onClick={e => this._removeHistory("mates", info.id)} />
                                                                </div>

                                                            </li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    ) :
                                    (
                                        <div className="manage-entity-list not-exists">
                                            <h5>등록된 교배차수 정보가 없습니다.</h5>
                                        </div>
                                    )
                            }                             
                        </div>

                        <div className="mt-50">
                            <div className="manage-entity-btn-area">
                                <h3 className="info-label">메모</h3>
                            </div>
                            <div className="manage-entity-note">
                                <textarea placeholder="메모를 입력하세요." value={this.state.memo} onChange={e => this._handleInput(e, 'memo')}></textarea>
                            </div>
                        </div>
                        <div className="btn-area">
                            <button className="btn-green" onClick={this._updateMemo}>메모 저장</button>
                        </div>
                    </div>
                </div>

                {/*<button type="button" style={{display: "block", marginTop: "120px", fontSize: "16px"}}*/}
                {/*onClick={this.openEntityDetailModal}>개체수정(상세) 모달*/}
                {/*</button>*/}
                {/*<button type="button" style={{display: "block", marginTop: "20px", fontSize: "16px"}}*/}
                {/*onClick={this.openWeightModal}>체중입력 모달*/}
                {/*</button>*/}
                {/*<button type="button" style={{display: "block", marginTop: "20px", fontSize: "16px"}}*/}
                {/*onClick={this.openFeedModal}>급이입력 모달*/}
                {/*</button>*/}
                {/*<button type="button" style={{display: "block", marginTop: "20px", fontSize: "16px"}}*/}
                {/*onClick={this.openSupplyWaterModal}>급수입력 모달*/}
                {/*</button>*/}
                {/*<button type="button" style={{display: "block", marginTop: "20px", fontSize: "16px"}}*/}
                {/*onClick={this.openMateCountModal}>교배차수입력 모달*/}
                {/*</button>*/}
            </article>
        )
    }
}

export default useMateCount(
    ({actions}) => ({
        _openMateCountModal: actions.open
    })
)(useSupplyWater(
    ({actions}) => ({
        _openSupplyWaterModal: actions.open
    })
)(useFeed(
    ({actions}) => ({
        _openFeedModal: actions.open
    })
)(useWeight(
    ({actions}) => ({
        _openWeightModal: actions.open
    })
)(useEntityDetail(
    ({actions}) => ({
        _openEntityDetailModal: actions.open
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError
    })
)(EntityDetail))))));