import React, { Component } from "react";
import { useEntityCreate } from "../contexts/modals/entityCreate";
import { useAlert } from "../contexts/alert";
import { getPath, returnPath, returnQuery, getQuery } from "../../../utils/route";
import href from '../../../utils/href';
import entityManager from '../managers/entity';
import CONSTANT from '../constants/constant';
import deepcopy from '../../../utils/deepcopy';
import Pagination from "../components/Pagination";
import XLSX from 'xlsx';
import moment from "moment";

const DEFAULT_QUERY = {
    offset: 0,
    size: CONSTANT.defaultLoadSize,
    //size: 2,
    category: {
        count: 0,
        rows: [],
    },
    type_check:'',
    currnet_category: 0,
    searchItem: '',
    animal_type:[],
    animal_type_id: 1,
    animals: {
        count: 0,
        rows: [],
    },
    newAnimals: {
        rows: []
    },
    initialLoading: true
};

class Animal extends Component {
    constructor(props) {
        super(props);
        this.destroy = null;
        this.state = deepcopy(DEFAULT_QUERY);
    }
    componentDidMount() {
        const _this = this;
        this.findCategory();
        this._findKinds();
        document.getElementById("upload").addEventListener("change", this.handleFile, false);
        this.destroy = this.props.history.listen(() => {
            if (getPath() === 'animal') {
                _this.findAnimals();
            }
        });
    }

    componentWillUnmount() {
        this.destroy();
        this.destroy = null;
        this.setState(deepcopy(DEFAULT_QUERY));
    }



    pagination = (offset) => {
        let query = {
            page: (offset / this.state.size) + 1
        };
        if (this.state.searchItem) query.searchItem = this.state.searchItem;
        this.props.history.push(returnPath(getPath()) + returnQuery(query));
    };

    getOffset = (index) => {
        return ((index - 1) * this.state.size);
    };

    search = (e) => {
        const _this = this;
        this.setState({
            animals: {
                count: 0,
                rows: [],
            },
            initialLoading: true,
        });

        let getQueryItem = getQuery();
        if (!getQueryItem.page) {
            getQueryItem.page = 1;
        }
        getQueryItem.offset = (getQueryItem.page - 1) * this.state.size;
        getQueryItem.searchItem = getQueryItem.searchItem ? getQueryItem.searchItem : undefined;


        let query = {
            size: this.state.size,
            animal_code: this.state.searchItem
        };
        _this.setState(getQueryItem, () => {
            if (getQueryItem.page) {
                query.offset = this.state.offset;
            }
            this.findAnimalsByTypeID(this.state.animal_type_id, query);
        });
    };

    findAnimalsByTypeID = (type_id, query) => {
        const _this = this;

        entityManager.find_animalsByTypeID(type_id, query, (status, data) => {
            if (status === 200) {
                if (data.data.rows) {

                    data.data.rows.forEach((item, index) => {
                        switch (item.health_state) {
                            case "good":
                                item.hs_name = "좋음";
                                break;
                            case "normal":
                                item.hs_name = "보통";
                                break;
                            case "bad":
                                item.hs_name = "나쁨";
                                break;
                        }

                        item.breed_name = '';
                        if (item.check != 0 && item.check != null)
                            item.breed_name += '임신감정, ';
                        if (item.estrus != 0 && item.estrus != null)
                            item.breed_name += '발정, ';
                        if (item.insemination != 0 && item.insemination != null)
                            item.breed_name += '수정, ';
                        if (item.dryUp != 0 && item.dryUp != null)
                            item.breed_name += '건유, ';
                        if (item.birth != 0 && item.birth != null)
                            item.breed_name += '분만, ';

                        item.breed_name = item.breed_name.substr(0, item.breed_name.length - 2)
                    });
                }
                _this.setState({
                    animals: data.data,
                    initialLoading: false
                });
            } else if (status === 404) {
                _this.setState({
                    initialLoading: false
                });
            } else {
                _this.props.alertError(status, data);
            }
        });
    }

    inputSearchItem = (e) => {
        this.setState({
            searchItem: e.target.value
        });
    };

    onInputKeyUp = (e) => {
        if (e.key === "Enter") {
            this.search();
        }
    };

    _findKinds = (callback = null) => {
        const _this = this;

        entityManager.find_kinds((status, data) => {
            if (status === 200) {
                _this.setState({
                    kinds: data.data,
                });
                if (callback) callback()
            } else if (status === 404) {
                if (callback) callback()
            } else {
                _this.props.alertError(status, data);
            }
        });
    }

    findCategory = () => {
        const _this = this;
        this.setState({
            category: [],
            currnet_category: 0,
        });


        _this.setState(() => {
            let query = {
                size: 5,
                offset: 0
            };
            entityManager.getCategory(query, (status, data) => {
                if (status === 200) {
                    if (data.data.rows) {
                        data.data.rows.forEach((item, index) => {
                        });
                    }
                    
                    let default_image='';
                    if(data.data.rows[0].species=='cow'){
                        default_image='/public/images/animal/cow.png'
                    }else if(data.data.rows[0].species=='pig'){
                        default_image='/public/images/animal/pig.png'
                    }else if(data.data.rows[0].species=='horse'){
                        default_image='/public/images/animal/horse.png'
                    }else if(data.data.rows[0].species=='sheep'){
                        default_image='/public/images/animal/sheep.png'
                    }else if(data.data.rows[0].species=='goat'){
                        default_image='/public/images/animal/goat.png'
                    }

                    _this.setState({
                        category: data.data,
                        currnet_category: data.data.rows[0].id,
                        animal_type:data.data.rows[0],
                        type_check:default_image
                    });
                    return this.findAnimals();
                } else if (status === 404) {
                    _this.props.alertError(status, data);
                } else {
                    _this.props.alertError(status, data);
                }
            });

        });

    }

    findAnimals = (callback = null) => {
        const _this = this;
        this.setState({
            animals: {
                count: 0,
                rows: [],
            },
            initialLoading: true,
        });

        let getQueryItem = getQuery();
        if (!getQueryItem.page) {
            getQueryItem.page = 1;
        }
        getQueryItem.offset = (getQueryItem.page - 1) * this.state.size;
        getQueryItem.searchItem = getQueryItem.searchItem ? getQueryItem.searchItem : undefined;

        _this.setState(getQueryItem, () => {
            let query = {
                size: this.state.size,
            };
            if (getQueryItem.page) {
                query.offset = this.state.offset;
            }

            this.findAnimalsByTypeID(this.state.animal_type_id, query);
        });
    };

    openEntityCreateModal = () => {
        const _this = this;

        if (this.state.animal_type_id != null && this.state.animal_type_id != '') {
            this.props.openEntityCreateModal(this.state.animal_type, this.enableMotherModalClose);
        } else {
            _this.props.showDialog({
                alertText: '카테고리 등록이 필요합니다.',
                cancelText: "확인",
                cancelCallback: () => {
                    window.location.reload();
                }
            })
        }
    };

    disableMotherModalClose = () => {
        document.removeEventListener('keyup', this.closeOnEscape);
    };

    enableMotherModalClose = () => {
        this.disableMotherModalClose();
        this.findAnimals();
        document.addEventListener('keyup', this.closeOnEscape);
    };

    goToDetail = (e, id) => {
        //this.props.openAnimalDetail(contact);
        href('/animal/detail/' + id);
    };

    checkCurrentCategory = (id) => {
        if (this.state.currnet_category == id) {
            return "tab_on";
        } else {
            return "";
        }
    }

    changeCategory = (e,data) => {
        this.searchinput.value = ''
        let default_image='';
        if(data.species=='cow'){
            default_image='/public/images/animal/cow.png'
        }else if(data.species=='pig'){
            default_image='/public/images/animal/pig.png'
        }else if(data.species=='horse'){
            default_image='/public/images/animal/horse.png'
        }else if(data.species=='sheep'){
            default_image='/public/images/animal/sheep.png'
        }else if(data.species=='goat'){
            default_image='/public/images/animal/goat.png'
        }
        this.setState({
            animal_type_id: e.target.value,
            animal_type:data,
            currnet_category: e.target.value,
            type_check:default_image
        })
        this.findAnimals();
    }

    handleFile = (e) => {
        var files = e.target.files, f = files[0];

        if (f.name.indexOf(".xlsx") == -1) {
            this.props.showDialog({
                alertText: '알맞은 엑셀파일(.xlsx)이 \n 아닙니다.',
                cancelText: "확인",
                cancelCallback: () => {
                    document.getElementById("upload").value = "";
                }
            })
        } else {
            var reader = new FileReader();
            let upload = [];
            let Animals = {};
            const _this = this;
            const healty = ["good", "bad", "normal"];
            const kinds = this.state.kinds.rows;

            reader.onload = function (e) {
                var data = new Uint8Array(e.target.result);
                var workbook = XLSX.read(data, { type: 'array' });

                var sheetArea = workbook.Sheets[Object.keys(workbook.Sheets)[0]]["!ref"];
                var sheetValue = workbook.Sheets[Object.keys(workbook.Sheets)[0]];
                var strReg = /^[A-Za-z0-9]+$/;
                let resultCheck = '';

                let count = 0;
                if (sheetArea.length == 5) {
                    count = sheetArea.substr(sheetArea.length - 1);
                } else if (sheetArea.length == 6) {
                    count = sheetArea.substr(sheetArea.length - 2);
                } else if (sheetArea.length == 7) {
                    count = sheetArea.substr(sheetArea.length - 3);
                } else {
                    count = 4;
                }

                for (let i = 2; i <= count; i++) {
                    if (!sheetValue['A' + i] && !sheetValue['B' + i]) break;
                    else if (!sheetValue['A' + i] && sheetValue['B' + i]) resultCheck = "개체 ID";
                    Animals = {
                        animal_type_id: _this.state.animal_type_id,
                        animal_code: sheetValue['A' + i] != undefined && strReg.test(sheetValue['A' + i].w)
                            && sheetValue['A' + i].w.length <= 20
                            ? sheetValue['A' + i].w : '',
                        kind: sheetValue['B' + i] != undefined
                            && kinds.find(c => c.name === sheetValue['B' + i].w)
                            ? sheetValue['B' + i].w : '',
                        weight: sheetValue['C' + i] != undefined
                            && !isNaN(sheetValue['C' + i].w)
                            && sheetValue['C' + i].w.length <= 3
                            ? sheetValue['C' + i].w : '',
                        birth_day: sheetValue['D' + i] != undefined
                            && moment(sheetValue['D' + i].w, 'M/D/YY', true).isValid()
                            ? moment(sheetValue['D' + i].w).format('YYYY-MM-DD') : '',
                        move_day: sheetValue['E' + i] != undefined
                            && moment(sheetValue['E' + i].w, 'M/D/YY', true).isValid()
                            ? moment(sheetValue['E' + i].w).format('YYYY-MM-DD') : '',
                        health_state: sheetValue['F' + i] != undefined
                            && healty.indexOf(sheetValue['F' + i].w) != -1
                            ? sheetValue['F' + i].w : '',
                        no: sheetValue['G' + i] != undefined
                            && strReg.test(sheetValue['G' + i].w)
                            && sheetValue['G' + i].w.length <= 20
                            ? sheetValue['G' + i].w : '',
                        barcode: sheetValue['H' + i] != undefined
                            && strReg.test(sheetValue['H' + i].w)
                            && sheetValue['H' + i].w.length == 20
                            ? sheetValue['H' + i].w : '',
                    }
                    upload.push(Animals);
                }

                upload.forEach(e => {
                    if (e.animal_code == '') resultCheck = "개체 ID";
                    else if (e.kind == '') resultCheck = "품종";
                    else if (e.weight == '') resultCheck = "체중";
                    else if (e.birth_day == '') resultCheck = "출생일";
                    else if (e.move_day == '') resultCheck = "전입일";
                    else if (e.health_state == '') resultCheck = "건강 상태";
                    else if (e.birth_day > e.move_day) resultCheck = "wrong_day"
                    else resultCheck = true;
                });

                if (resultCheck == true) {
                    _this.createCollectiveEntity(upload);
                }
                else if (resultCheck == "wrong_day") {
                    _this.props.showDialog({
                        alertText: "전입일이 출생일보다 빠릅니다.",
                        cancelText: "확인",
                    })
                }
                else {
                    _this.props.showDialog({
                        alertText: resultCheck + '값이 형식에 맞지 않습니다.',
                        cancelText: "확인",
                    })
                }
                document.getElementById("upload").value = "";
            }
            reader.readAsArrayBuffer(f);
        };
    }

    createCollectiveEntity = (upload) => {
        const _this = this;
        let query = {
            upload: upload,
        }

        entityManager.create_multi(query, (status, data) => {
            if (status === 200) {
                if (data.data.length > 0) {
                    let msg = '';
                    data.data.forEach(element => {
                        msg += element.animal_code + '\n'
                    });
                    _this.props.showDialog({
                        alertText: '중복된 개체 ID가 있습니다. \n' + msg,
                        cancelText: "확인",
                        cancelCallback: () => {
                        }
                    })
                } else if (data.data) {
                    _this.props.showDialog({
                        alertText: '개체가 등록 되었습니다.',
                        cancelText: "확인",
                        cancelCallback: () => {
                            _this.findAnimals();
                        }
                    })
                }
            } else if (status === 404) {
                _this.props.alertError(status, data);
            } else if (data.code === "0501" && status === 400) {
                _this.props.alertError(status, data);
            }
        });
    }

    render() {
        return (
            <article id="lcEntityWrap" className="lc-animal-route-wrap">
                <div className="lc-animal">
                    <ul className="lc-animal-tab">
                        {
                            this.state.category && this.state.category.rows && this.state.category.rows.length ?
                                (
                                    this.state.category.rows.map((type, index) => {
                                        return (
                                            <li className={this.checkCurrentCategory(type.id)} onClick={e => this.changeCategory(e,type)} value={type.id} key={index}>{type.animal_type_name}</li>
                                        )
                                    })
                                ) :
                                (
                                    <div className="no-data-wrap">데이터가 없습니다.</div>
                                )
                        }
                    </ul>
                    <div className="lc-animal-search">
                        <input type="text" ref={ref => { this.searchinput = ref }} className="search-box" placeholder="검색"
                            onKeyUp={e => this.onInputKeyUp(e)}
                            onChange={e => this.inputSearchItem(e)}
                        />
                        <button className="searchBtn" onClick={e => this.search(e)}><img src="/public/images/commons/btn-search@2x.png" /></button>
                    </div>
                    <div className="add-btn-area-mobile">
                        <button className="btn-green" onClick={this.openEntityCreateModal}>개체 등록</button>
                        &nbsp;
                        &nbsp;
                        <div className="filebox">
                            <label htmlFor="upload">일괄 등록</label>
                            <input type="file" id="upload" />
                        </div>
                    </div>
                    <div className="lc-animal-list">
                        {
                            this.state.animals && this.state.animals.rows && this.state.animals.rows.length ?
                                (
                                    <table>
                                        <thead>
                                            <tr>
                                                <th className="entity-img"></th>
                                                <th className="entity-id">개체 ID</th>
                                                <th className="entity-health">건강</th>
                                                <th className="entity-weight">체중</th>
                                                <th className="entity-breed">번식</th>
                                                <th className="entity-birth on-pc">출생일</th>
                                                <th className="entity-move on-pc">전입일</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                this.state.animals.rows.map((animal, index) => {
                                                    return (
                                                        <tr key={index} onClick={e => this.goToDetail(e, animal.id)}>
                                                            <td>
                                                                {
                                                                    animal.image_url != '' && animal.image_url != null ?
                                                                        (
                                                                            <img src={"/uploads/animal/" + animal.image_url} className="list-image" />
                                                                        ) : (
                                                                            <img src={this.state.type_check} className="list-image" />
                                                                        )
                                                                }
                                                            </td>
                                                            <td>{animal.animal_code}</td>
                                                            <td>{animal.hs_name}</td>
                                                            <td>{animal.weight}</td>
                                                            <td>{animal.breed_name}</td>
                                                            <td className="on-pc">{animal.birth_day}</td>
                                                            <td className="on-pc">{animal.move_day}</td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                        </tbody>
                                    </table>
                                ) :
                                (
                                    <div className="no-data-wrap">데이터가 없습니다.</div>
                                )
                        }

                        <div className="pagination">
                            {this.state.animals && this.state.animals.rows.length ?
                                <Pagination count={this.state.animals.count}
                                    offset={this.state.offset}
                                    size={this.state.size}
                                    searchPage={this.pagination} /> :
                                null
                            }
                        </div>
                    </div>
                </div>
                <div className="add-btn-area">
                    <button className="btn-green" onClick={this.openEntityCreateModal}>개체 등록</button>
                    &nbsp;
                    &nbsp;

                    <div className="filebox">
                        <label htmlFor="upload">일괄 등록</label>
                        <input type="file" id="upload" />
                    </div>
                </div>

                {/*<p>이곳에 개체관리 마크업하시면됩니다 ㅎ-ㅎ</p>*/}

                {/*<Link to="/animal/detail"><p style={{marginTop: "150px", border: "2px solid"}}>누르면 개체관리 상세로 가지는 버튼 (조심하셈)</p></Link>*/}

                {/*<button type="button" style={{marginTop: "100px", fontSize: "16px"}} onClick={this.openEntityCreateModal}>이거누르면 개체등록 모달나옴</button>*/}
            </article>
        );
    }
}

export default useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
        alertTextError: actions.alertTextError,
    })
)(useEntityCreate(
    ({ actions }) => ({
        openEntityCreateModal: actions.open
    })
)(Animal));
