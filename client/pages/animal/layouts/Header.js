import React, {Component} from 'react';
import {Link,withRouter} from 'react-router-dom';
import href from "../../../utils/href";

class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            erpHost: window.erpHost,
            user: window.user,
            leftSideNavOpen: false,
        };
    }
    

    // backToErp = (e) => {
    //     e.preventDefault();
    //     href(this.state.erpHost);
    // };

    backToErp = (e, key) => {
        e.preventDefault();
        href(this.state.erpHost + (key ? '/' + key : ''));
    };

    toggleLeftSideNav = () => {
        this.setState({
            leftSideNavOpen: !this.state.leftSideNavOpen
        });

    };

    refresh = (e) => {
        e.preventDefault();
        location.reload();
    };

    render() {
        return (
            <header id="lcHeader">
                <div id="mobileHamburgerWrap" className={this.state.leftSideNavOpen ? "lc-active" : ""}>
                    <button onClick={this.toggleLeftSideNav} id="mobileHamburger"></button>
                </div>
                <div id="mobileMenuOverlay" className={this.state.leftSideNavOpen ? "lc-active" : ""}></div>
                {
                    this.isPanel ? null :
                        <button type="button" id="lcMenuButton" onClick={e => this.backToErp(e, 'animal')}/>
                }
                <h1 id="lcLogo" onClick={this.refresh}>
                    <span>팜랩스</span>
                </h1>
                <nav id="an-navi" className={this.state.leftSideNavOpen ? "lc-active" : ""}>
                    <div className="side-nav-header">
                        <button className="close-nav-btn" onClick={this.toggleLeftSideNav}></button>
                    </div>
                    <div className="side-nav-content-wrap">
                        <ul className="side-nav-ul">
                            <li>
                                <div className="left-side-nav-menu">
                                <span>
                                    축산 관리
                                </span>
                                </div>
                                <ul className="left-side-child-ul">
                                    <li className="left-side-child-li" onClick={this.toggleLeftSideNav}>
                                        <Link to={"/animal?page=1"}><span>개체 관리</span></Link>
                                    </li>
                                    <li className="left-side-child-li" onClick={this.toggleLeftSideNav}>
                                        <Link to={"/animal/breed?page=1"}><span>번식 관리</span></Link>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
                {/*<nav id="lcHeaderNav">*/}
                    {/*<ul>*/}
                        {/*<li>*/}
                            {/*<Link to={"/animal"}><span>개체관리</span></Link>*/}
                        {/*</li>*/}
                        {/*<li>*/}
                            {/*<Link to={"/animal/breed"}><span>번식관리</span></Link>*/}
                        {/*</li>*/}
                    {/*</ul>*/}
                {/*</nav>*/}
                {/*<div id="lcMyInfoWrap" className={this.state.rightSideNavOpen ? "lc-active" : ""} onClick={this.toggleRightSideNav}>*/}
                    {/*<i/>*/}
                    {/*<p>{this.state.user.name} 님</p>*/}
                {/*</div>*/}
            </header>
        );
    }
}

export default withRouter(Header);
