import http, {process, validator} from '../../../utils/http';

import resource from '../constants/resource';

//const ENTITY_RESOURCE = resource.Entity;

const create = (data, callback) => {
    validator(data, [
        "animal_type_id",
        "animal_code",
        "kind",
        "weight",
        "birth_day",
        "move_day",
        "health_state",
        "no",
        "mate_count",
        "barcode",
       
    ], [
        "animal_type_id",
        "animal_code",
        "kind",
        "weight",
        "birth_day",
        "move_day",
        "health_state",
    ], (body) => {
        const req = http(resource.ENTITY);
        req.post(body).then(process(callback));
    }, callback);
};

const create_multi =(body,callback)=>{
    const req = http(resource.ENTITY+'/multi');
    req.post(body).then(process(callback));
}

const update = (data, callback) => {
    validator(data, [
        "id",
        "animal_code",
        "kind",
        "weight",
        "birth_day",
        "move_day",
        "health_state",
        "no",
        "mate_count",
        "barcode",
    ], [
        "animal_code",
        "kind",
        "weight",
        "birth_day",
        "move_day",
        "health_state",
    ], (body) => {
        const req = http(resource.ENTITY);
        req.put(body).then(process(callback));
    }, callback);
};

const remove = (animal_id, callback) => {
    const req = http(resource.ENTITY+'/'+animal_id);
    req.del().then(process(callback));
};

const removeImg = (query, callback) => {
    const req = http(resource.DELIMG);
    req.post(query).then(process(callback));
};

const getCategory = (query, callback) => {
    const req = http(resource.ANIMAL_TYPES);
    req.gets(query).then(process(callback));
};

const find_animalsByTypeID = (animal_type_id, query, callback) => {
    const req = http(resource.ENTITY+"/"+animal_type_id);
    req.gets(query).then(process(callback));
};

const findById = (id, callback) => {
    const req = http(resource.ENTITY_DETAIL);
    req.get(id).then(process(callback));
};

const find_kinds = (callback) => {
    const req = http(resource.ANIMAL_KINDS);
    req.getsimple().then(process(callback));
};

const update_memo = (query, callback) => {
    const req = http(resource.ENTITY+"/"+query.animal_id + "/memo");
    req.put(query).then(process(callback));
};

const list_weigths = (query, callback) => {
    const req = http(resource.ANIMAL_WEIGHTS);
    req.getsWithId(query.animal_id, query).then(process(callback));
};

const list_feeds = (query, callback) => {
    const req = http(resource.ANIMAL_FEEDS);
    req.getsWithId(query.animal_id, query).then(process(callback));
};

const list_supplyWaters = (query, callback) => {
    const req = http(resource.ANIMAL_WATERS);
    req.getsWithId(query.animal_id, query).then(process(callback));
};

const list_mateCounts = (query, callback) => {
    const req = http(resource.ANIMAL_MATES);
    req.getsWithId(query.animal_id, query).then(process(callback));
};

const get_breeds = (id, query, callback) => {
    const req = http(resource.ANIMAL_BREEDS+'/detail/'+id);
    req.gets(query).then(process(callback));
};

const mate_create = (data, callback) => {
    validator(data, [
        "animal_id",
        "date",
        "memo",
    ], [
        "animal_id",
        "date",
    ], (body) => {
        const req = http(resource.ANIMAL_MATES);
        req.post(body).then(process(callback));
    }, callback);
};

const water_create = (data, callback) => {
    validator(data, [
        "animal_id",
        "date",
        "water",
        "memo",
    ], [
        "animal_id",
        "date",
        "water",
    ], (body) => {
        const req = http(resource.ANIMAL_WATERS);
        req.post(body).then(process(callback));
    }, callback);
};

const feed_create = (data, callback) => {
    validator(data, [
        "animal_id",
        "date",
        "feed",
        "memo",
        "unit",
    ], [
        "animal_id",
        "date",
        "feed",
        "unit",
    ], (body) => {
        const req = http(resource.ANIMAL_FEEDS);
        req.post(body).then(process(callback));
    }, callback);
};

const weight_create = (data, callback) => {
    validator(data, [
        "animal_id",
        "date",
        "weight",
        "memo",
    ], [
        "animal_id",
        "date",
        "weight",
    ], (body) => {
        const req = http(resource.ANIMAL_WEIGHTS);
        req.post(body).then(process(callback));
    }, callback);
};

const create_breed = (data, callback) => {
    validator(data, [
        "animal_id",
        "start_date",
        "end_date",
        "type",
        "memo",
    ], [
        "animal_id",
        "start_date",
        "end_date",
        "type",
    ], (body) => {
        const req = http(resource.ANIMAL_BREEDS);
        req.post(body).then(process(callback));
    }, callback);
};

const remove_breed  = (animal_id, callback) => {
    const req = http(resource.ANIMAL_BREEDS+'/'+animal_id);
    req.del().then(process(callback));
};

const update_breed = (data, callback) => {
    validator(data, [
        "id",
        "start_date",
        "end_date",
        "type",
        "memo",
    ], [
        "id",
        "start_date",
        "end_date",
        "type",
    ], (body) => {
        const req = http(resource.ANIMAL_BREEDS);
        req.put(body).then(process(callback));
    }, callback);
};

const remove_histroy = (key,id, callback)=>{
    const req = http('/api/v1/animal-'+key+'/'+id);
    req.del().then(process(callback));
}

export {
    create,
    update,
    remove,
    removeImg,
    find_animalsByTypeID,
    findById,
    getCategory,
    mate_create,
    water_create,
    feed_create,
    weight_create,
    list_weigths,
    list_mateCounts,
    list_feeds,
    list_supplyWaters,
    find_kinds,
    update_memo,
    create_breed,
    get_breeds,
    remove_breed,
    update_breed,
    remove_histroy,
    create_multi,
};

export default {
    create: create,  
    update: update,
    remove: remove, 
    removeImg : removeImg,
    find_animalsByTypeID: find_animalsByTypeID,
    findById : findById,
    getCategory: getCategory,
    mate_create: mate_create,
    water_create: water_create,
    feed_create: feed_create,
    weight_create: weight_create,
    list_weight: list_weigths,
    list_mateCount: list_mateCounts,
    list_feed: list_feeds,
    list_supplyWater: list_supplyWaters,
    find_kinds:find_kinds,
    update_memo:update_memo,
    create_breed:create_breed,
    get_breeds:get_breeds,
    remove_breed:remove_breed,
    update_breed:update_breed,
    remove_histroy:remove_histroy,
    create_multi:create_multi,
};
