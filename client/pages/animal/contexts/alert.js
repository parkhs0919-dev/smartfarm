import React, {Component, createContext} from 'react';
import {createUseConsumer} from '../../../utils/context';
import CONSTANT from '../constants/constant';

const Context = createContext();

const {Provider, Consumer: AlertConsumer} = Context;

const defaultState = CONSTANT.defaultAlertState;

class AlertProvider extends Component {
    state = Object.assign({}, defaultState);
    actions = {
        init: () => {
            this.setState(Object.assign({}, defaultState));
        },
        close: () => {
            this.setState({
                isVisible: false
            });
        },
        show: (option) => {
            let temp = Object.assign(Object.assign({}, defaultState), option);
            temp.isVisible = true;
            this.setState(temp);
        },
        alertError: (status, data) => {
            let text;
            if (!text && data && data.errors) {
                text = Object.values(data.errors)[0]
            }else if(status == "Invalid date" || status == "date_error"){
                text = "날짜 형식이 잘못되었습니다."
            }else if(status == "Invalid date" || status == "time_error"){
                text = "시간 형식이 잘못되었습니다."
            }else if(status == "wrong_start"){
                text = "종료일이 시작일보다 빠릅니다."
            }else if(status == "wrong_start_entity"){
                text = "전입일이 출생일보다 빠릅니다."
            }else if(status == "missing_data"){
                text = "필수 요청 값이 없습니다."
            }else if(status==400 && data.code == "0501"){
                text = "양식에 맞지않아\n등록에 실패했습니다."
            }else if(status==200 && data.data == "0522"){
                text = "이미 등록된 개체 ID 입니다."
            }

            let temp = Object.assign(Object.assign({}, defaultState), defaultState);
            temp.isVisible = true;
            temp.alertText = text;
            this.setState(temp);
        },
        alertTextError: (status, data, string) => {
            let text;
            if (!text && data && data.errors) {
                text = string + " " + Object.values(data.errors)[0]
            }else if(status == "check_data"){
                text = string;
            }


            let temp = Object.assign(Object.assign({}, defaultState), defaultState);
            temp.isVisible = true;
            temp.alertText = text;
            this.setState(temp);
        }
    };

    render() {
        const {state, actions} = this;
        const value = {state, actions};
        return (
            <Provider value={value}>
                {this.props.children}
            </Provider>
        );
    }
}

const useAlert = createUseConsumer(AlertConsumer);

export {
    AlertProvider,
    AlertConsumer,
    useAlert
};
