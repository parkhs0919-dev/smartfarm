import React from "react";
import {Route} from 'react-router-dom';

import {AlertProvider} from "./contexts/alert";
import {EntityCreateProvider} from "./contexts/modals/entityCreate";
import {EntityDetailProvider} from "./contexts/modals/entityDetail";
import {WeightProvider} from './contexts/modals/weight';
import {FeedProvider} from "./contexts/modals/feed";
import {SupplyWaterProvider} from "./contexts/modals/supplyWater";
import {MateCountProvider} from "./contexts/modals/mateCount";
import {BreedDetailProvider} from "./contexts/modals/breedDetail";
import {BreedCreateProvider} from "./contexts/modals/breedCreate";
import {BreedModifyProvider} from "./contexts/modals/breedModify";

import Alert from './components/Alert';

import ModalEntityCreate from './components/modals/EntityCreate';
import ModalEntityDetail from './components/modals/EntityDetail';
import ModalWeight from './components/modals/Weight';
import ModalFeed from './components/modals/Feed';
import ModalSupplyWater from './components/modals/SupplyWater'
import ModalMateCount from './components/modals/MateCount';
import ModalBreedDetail from './components/modals/BreedDetail';
import ModalBreedCreate from './components/modals/BreedCreate';
import ModalBreedModify from './components/modals/BreedModify';

import Header from './layouts/Header';
import Entity from './routes/Entity';
import EntityDetail from './routes/EntityDetail';
import Breed from './routes/Breed';

const AppProvider = ({contexts, children}) => contexts.reduce(
    (prev, context) => React.createElement(context, {
        children: prev
    }),
    children
);

const App = () => {
    return (
        <AppProvider contexts={[
            AlertProvider,

            EntityCreateProvider,
            EntityDetailProvider,
            WeightProvider,
            FeedProvider,
            SupplyWaterProvider,
            MateCountProvider,
            BreedDetailProvider,
            BreedCreateProvider,
            BreedModifyProvider,
        ]}>
            <Header/>
            <Alert/>
            <ModalEntityCreate/>
            <ModalEntityDetail/>
            <ModalWeight/>
            <ModalFeed/>
            <ModalSupplyWater/>
            <ModalMateCount/>
            <ModalBreedDetail/>
            <ModalBreedCreate/>
            <ModalBreedModify/>

            <div id="lcMainWrap">
                <Route exact path="/animal" component={Entity}/>
                <Route exact path="/animal/detail/:animal_id" component={EntityDetail}/>
                <Route path="/animal/breed" component={Breed}/>
            </div>
        </AppProvider>
    )
};

export default App;
