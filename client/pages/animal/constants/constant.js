const returnToken = (key) => {
    const metas = document.getElementsByTagName('meta');
    for (let i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute('name') === key) {
            return metas[i].getAttribute('content');
        }
    }
};

export default {
    csrfToken: returnToken('csrf-token'),
    defaultModalState: {
        isVisible: false,
        callback: undefined,
        syncCallback: undefined
    },
    defaultLoadingState: {
        loadingKey: {}
    },
    defaultNumberPageLength: 5,
    defaultLoadSize: 10,
    defaultAlertState: {
        isVisible: false,
        alertText: undefined,
        cancelClassName: undefined,
        cancelText: '확인',
        cancelCallback: undefined,
        cancelStyle: undefined,
        submitClassName: undefined,
        submitText: undefined,
        submitCallback: undefined,
        submitStyle: undefined
    },
}