export default {
    ENTITY: '/api/v1/animals',
    ENTITY_DETAIL: '/api/v1/animals/detail',
    ANIMAL_TYPES: '/api/v1/animal-types',
    ANIMAL_MATES: '/api/v1/animal-mates',
    ANIMAL_WATERS: '/api/v1/animal-waters',
    ANIMAL_FEEDS: '/api/v1/animal-feeds',
    ANIMAL_WEIGHTS: '/api/v1/animal-weights',
    ANIMAL_KINDS: '/api/v1/animal-kinds',
    DELIMG : '/api/v1/animals/delimg',
    ANIMAL_BREEDS:'/api/v1/animal-breeds',
}
