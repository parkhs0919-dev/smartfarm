import React, {Component} from 'react';
import {useFeed} from "../../contexts/modals/feed";
import {useAlert} from "../../contexts/alert";
import entityManager from '../../managers/entity';
import moment from "moment";

class ModalFeed extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {     
        const states = {
            animal_id: data,
            date:'',
            time:'',
            feed:'',
            memo:'',
            unit:'',
            check_time:false,
        };
        this.setState(states);    
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if (e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    _createFeed = e =>{ 
        e.preventDefault();
        const _this = this;
        if(this._checkCalendarValue() == false){
            _this.props.alertError("date_error",null);
            this.setState({date : ''});
            return;
        }else if(this._checkTimeValue() == false){
            _this.props.alertError("time_error",null);
            this.setState({time : ''});
            return;
        }

        let query = {
            animal_id: this.state.animal_id,
            date: this.state.date + " " + this.state.time,
            feed: this.state.feed,
            memo: this.state.memo,
            unit: this.state.unit,
        };
        
        entityManager.feed_create(query, (status, data) => {
            if(status === 200) {               
                _this.props.showDialog({
                    alertText: '급이가 등록 되었습니다.',
                    cancelText: "확인",
                    cancelCallback: () => {
                        window.location.reload();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        });
    }
    
    _handleInput = (e, key) => {
        let item = {};
        if(key == "memo"){
            if(e.target.value.length < 100){
                item[key] = e.target.value;
            }
        }else if(key == "unit"){
            item[key] = e.target.value;
        }else{
            item[key] = e.target.value.replace(/\D/,'').replace(/\D/,'');
        }
        this.setState(item);
    };

    _handleCalendarInput = (e, key) => {
        const _this = this;
        let item = {};
        if(e.target.value.length == 8){
            if(moment(e.target.value).format('YYYY-MM-DD') == "Invalid date"){
                _this.props.alertError("Invalid date", null);
                this.setState({date : ''});
            }else{
                var temp = moment(e.target.value).format('YYYY-MM-DD');
                e.target.value= temp;
                item[key] = e.target.value
            }
        }else if(e.target.value.length < 10){
            item[key] = e.target.value.replace(/\D/,'').replace(/\D/,'');
        }
        this.setState(item);
    };
    
    _onKeyDown = (e) => {     
        if(e.target.value.length == 5 && e.keyCode == 8){
            this.setState({check_time:true});
        }else{
            this.setState({check_time:false});
        }
    }

    _handleTimeInput = (e, key) => {
        const _this = this;
        let item = {};
        if(e.target.value.length == 4 && this.state.check_time == false){
            if(moment(e.target.value,"hmm").format('HH:mm') == "Invalid date"){
                _this.props.alertError("Invalid date", null);
                this.setState({time : ''});
            }else{
                var temp = moment(e.target.value,"hmm").format('HH:mm');
                e.target.value= temp;
                item[key] = e.target.value
            }
        }else if(e.target.value.length < 5){
            item[key] = e.target.value.replace(/\D/,'');
        }
        this.setState(item);
    };

    _checkCalendarValue=()=>{
        if(this.state.date.length == 10) return true;
        else return false;
    }

    _checkTimeValue=()=>{
        if(this.state.time.length == 5) return true;
        else return false;
    }

    render() {
        return (
            <article id="lcModalFeedWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')}
                     tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalFeed" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">급이입력</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div className="control-box">
                                        <div className="feed-box pr-6">
                                            <label className="control-info-label">날짜</label>
                                            <input className="control-input-box" type="text" maxLength="10" value={this.state.date} onChange={e => this._handleCalendarInput(e, 'date')}/>
                                        </div>
                                        <div className="feed-box pl-6">
                                            <label className="control-info-label">시간</label>
                                            <input className="control-input-box" type="text" maxLength="5" value={this.state.time} onKeyDown={e=> this._onKeyDown(e)} onChange={e => this._handleTimeInput(e, 'time')}/>
                                        </div>

                                        <div className="feed-box pr-6">
                                            <label className="control-info-label">급이량</label>
                                            <input className="control-input-box" type="text" maxLength="4" placeholder="숫자 입력" value={this.state.feed} onChange={e => this._handleInput(e, 'feed')}/>
                                        </div>
                                        <div className="feed-box pl-6">
                                            <label className="control-info-label">단위</label>
                                            <select className="control-unit-box" value={this.state.unit} onChange={e => this._handleInput(e, 'unit')}>
                                                <option value="" defaultValue disabled hidden>선택</option>
                                                <option value="g">g</option>
                                                <option value="kg">kg</option>
                                            </select>
                                        </div>

                                    </div>
                                    {/*<div className="control-box">*/}
                                    {/*<label className="control-info-label">급이량</label>*/}
                                    {/*<input className="control-input-box" type="text" placeholder="숫자 입력"/>*/}
                                    {/*<label className="control-unit-txt">g</label>*/}
                                    {/*</div>*/}
                                    <div className="control-box last">
                                        <label className="control-info-label">메모</label>
                                        <textarea className="control-textarea" placeholder="텍스트 입력(최대 100자)" value={this.state.memo} onChange={e => this._handleInput(e, 'memo')}></textarea>
                                    </div>

                                    <button className="btn-green" onClick={this._createFeed}>입력</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useFeed(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalFeed));