import React, {Component} from 'react';
import {useSupplyWater} from "../../contexts/modals/supplyWater";
import {useAlert} from "../../contexts/alert";
import entityManager from '../../managers/entity';
import moment from "moment";

class ModalSupplyWater extends Component {
    constructor(props) {
        super(props);

        this.state = {
            animal_id:'',
            date:'',
            time:'',
            water:'',
            memo:'',
            check_time:false,
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {     
        const states = {
            animal_id: data,
            date:'',
            time:'',
            water:'',
            memo:'',
            check_time:false,
        };
        this.setState(states);    
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    _createSupplyWater = e =>{ 
        e.preventDefault();
        const _this = this;
        if(this._checkCalendarValue() == false){
            _this.props.alertError("date_error",null);
            this.setState({date : ''});
            return;
        }else if(this._checkTimeValue() == false){
            _this.props.alertError("time_error",null);
            this.setState({time : ''});
            return;
        }

        let query = {
            animal_id: this.state.animal_id,
            date: this.state.date + " " + this.state.time,
            water: this.state.water,
            memo: this.state.memo,
        };
        
        entityManager.water_create(query, (status, data) => {
            if(status === 200) {               
                _this.props.showDialog({
                    alertText: '급수가 등록 되었습니다.',
                    cancelText: "확인",
                    cancelCallback: () => {
                        window.location.reload();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        });
    }
    
    _handleInput = (e, key) => {
        let item = {};
        if(key == "memo"){
            if(e.target.value.length < 100){
                item[key] = e.target.value;
            }
        }else{
            item[key] = e.target.value.replace(/\D/,'').replace(/\D/,'');
        }
        this.setState(item);
    };

    _handleCalendarInput = (e, key) => {
        const _this = this;
        let item = {};
        if(e.target.value.length == 8){
            if(moment(e.target.value).format('YYYY-MM-DD') == "Invalid date"){
                _this.props.alertError("Invalid date", null);
                this.setState({date : ''});
            }else{
                var temp = moment(e.target.value).format('YYYY-MM-DD');
                e.target.value= temp;
                item[key] = e.target.value
            }
        }else if(e.target.value.length < 10){
            item[key] = e.target.value.replace(/\D/,'').replace(/\D/,'');
        }
        this.setState(item);
    };
    
    _onKeyDown = (e) => {     
        if(e.target.value.length == 5 && e.keyCode == 8){
            this.setState({check_time:true});
        }else{
            this.setState({check_time:false});
        }
    }

    _handleTimeInput = (e, key) => {
        const _this = this;
        let item = {};
        if(e.target.value.length == 4 && this.state.check_time == false){
            if(moment(e.target.value,"hmm").format('HH:mm') == "Invalid date"){
                _this.props.alertError("Invalid date", null);
                this.setState({time : ''});
            }else{
                var temp = moment(e.target.value,"hmm").format('HH:mm');
                e.target.value= temp;
                item[key] = e.target.value
            }
        }else if(e.target.value.length < 5){
            item[key] = e.target.value.replace(/\D/,'');
        }
        this.setState(item);
    };

    _checkCalendarValue=()=>{
        if(this.state.date.length == 10) return true;
        else return false;
    }

    _checkTimeValue=()=>{
        if(this.state.time.length == 5) return true;
        else return false;
    }

    render() {
        return (
            <article id="lcModalSupplyWaterWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalSupplyWater" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">급수입력</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div className="control-box">
                                        <div className="feed-box pr-6">
                                            <label className="control-title">날짜</label>
                                            <input className="control-input-box" type="text" maxLength="10" value={this.state.date} onChange={e => this._handleCalendarInput(e, 'date')}/>
                                        </div>
                                        <div className="feed-box pl-6">
                                            <label className="control-title">시간</label>
                                            <input className="control-input-box" type="text" maxLength="5" value={this.state.time} onKeyDown={e=> this._onKeyDown(e)} onChange={e => this._handleTimeInput(e, 'time')}/>
                                        </div>
                                        <div className="feed-box-unit pr-6">
                                            <label className="control-title">급수량</label>
                                            <input className="control-input-box" type="text" maxLength="4" placeholder="숫자 입력" value={this.state.water} onChange={e => this._handleInput(e, 'water')}/>
                                            <label className="control-unit-txt">L</label>
                                        </div>

                                    </div>
                                    {/*<div className="control-box">*/}
                                        {/*<label className="control-title">급이량</label>*/}
                                        {/*<input className="control-input-box" type="text" placeholder="숫자 입력"/>*/}
                                        {/*<label className="control-unit-txt">L</label>*/}
                                    {/*</div>*/}
                                    <div className="control-box last">
                                        <label className="control-title">메모</label>
                                        <textarea className="control-textarea" placeholder="텍스트 입력(최대 100자)" value={this.state.memo} onChange={e => this._handleInput(e, 'memo')}></textarea>
                                    </div>

                                    <button className="btn-green" onClick={this._createSupplyWater}>입력</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useSupplyWater(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalSupplyWater));