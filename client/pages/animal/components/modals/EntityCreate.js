import React, { Component } from 'react';
import { useEntityCreate } from "../../contexts/modals/entityCreate";
import { useAlert } from "../../contexts/alert";
import entityManager from '../../managers/entity';
import moment from "moment";
const axios = require("axios");

class ModalEntityCreate extends Component {
    constructor(props) {
        super(props);

        this.state = {
            animal_type_id: '',
            animal_code: '',
            kind: '',
            weight: '',
            birth_day: '',
            move_day: '',
            health_state: '',
            no: '',
            mate_count: '',
            barcode: '',
            image_url: '',
            memo: '',
            default_type:'',
            kinds: {
                rows: [],
            },
            file: '',
            imagePreviewUrl: '',
        };
    }

    init = (data) => {
        let default_image='';
        if(data.species=='cow'){
            default_image='/public/images/animal/cow.png'
        }else if(data.species=='pig'){
            default_image='/public/images/animal/pig.png'
        }else if(data.species=='horse'){
            default_image='/public/images/animal/horse.png'
        }else if(data.species=='sheep'){
            default_image='/public/images/animal/sheep.png'
        }else if(data.species=='goat'){
            default_image='/public/images/animal/goat.png'
        }
        
        this.setState({ animal_type_id: data.id,default_type:default_image });
        document.addEventListener('keyup', this.closeOnEscape);
    };
    componentWillUnmount() {
        document.removeEventListener('keyup', this.closeOnEscape);
    }

    componentDidMount() {
        this.props.sync(this.init);
        this._findKinds();
    }

    reset() {
        const states = {
            animal_code: '',
            kind: '',
            weight: '',
            birth_day: '',
            move_day: '',
            health_state: '',
            no: '',
            mate_count: '',
            barcode: '',
            image_url: '',
            memo: '',
            file: '',
            imagePreviewUrl: '',
        };
        this.setState(states);
    }

    close = (e) => {
        if (e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.reset();
        this.props.callback && this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    _findKinds = (callback = null) => {
        const _this = this;

        entityManager.find_kinds((status, data) => {
            if (status === 200) {
                _this.setState({
                    kinds: data.data,
                });
                if (callback) callback()
            } else if (status === 404) {
                if (callback) callback()
            } else {
                _this.props.alertError(status, data);
            }
        });
    }

    _handleInput = (e, key) => {
        let item = {};
        const _this = this;
        var strReg = /^[A-Za-z0-9]+$/;

        if (key == "memo") {
            if (e.target.value.length < 100) {
                item[key] = e.target.value;
            }
        } else if (key == "weight") {
            item[key] = e.target.value.replace(/\D/, '').replace(/\D/, '');
        } else if (key == "birth_day" || key == "move_day") {
            if (e.target.value.length == 8) {
                if (moment(e.target.value).format('YYYY-MM-DD') == "Invalid date") {
                    _this.props.alertError("Invalid date", null);
                    if (key == "birth_day")
                        this.setState({ birth_day: '' });
                    else
                        this.setState({ move_day: '' });
                } else {
                    var temp = moment(e.target.value).format('YYYY-MM-DD');
                    e.target.value = temp;
                    item[key] = e.target.value
                }
            } else if (e.target.value.length < 10) {
                item[key] = e.target.value.replace(/\D/, '').replace(/\D/, '');
            }
        } else if (key == "animal_code" || key == "no" || key == "barcode") {
            if (strReg.test(e.target.value) || e.target.value == '') {
                item[key] = e.target.value;
                if (key == "animal_code")
                    item["barcode"] = e.target.value;
            }
        }
        else {
            item[key] = e.target.value;
        }
        this.setState(item);
    };

    _checkCalendarValue = () => {
        if (this.state.birth_day.length != 10)
            return "birth_day";
        else if (this.state.move_day.length != 10)
            return "move_day";
        else if (this.state.birth_day > this.state.move_day)
            return "wrong_start"
        else
            return true;
    }

    _handleSubmit = e => {
        e.preventDefault();
        const _this = this;

        if (!this.state.animal_code || this.state.animal_code == '') {
            _this.props.alertTextError("check_data", null, "개체 ID 값이 필요합니다.");
            return;
        } else if (!this.state.kind || this.state.kind == '') {
            _this.props.alertTextError("check_data", null, "품종이 선택 되지 않았습니다.");
            return;
        } else if (!this.state.weight || this.state.weight == '') {
            _this.props.alertTextError("check_data", null, "체중 값이 필요합니다.");
            return;
        } else if (!this.state.health_state || this.state.health_state == '') {
            _this.props.alertTextError("check_data", null, "건강상태가 선택 되지 않았습니다.");
            return;
        }

        let result = this._checkCalendarValue();
        if (result != true) {
            if (result == "birth_day") {
                _this.props.alertError("date_error", null);
                this.setState({ birth_day: '' });
            }
            else if (result == "end_date") {
                _this.props.alertError("date_error", null);
                this.setState({ move_day: '' });
            }
            else if (result == "wrong_start") {
                _this.props.alertError("wrong_start_entity", null);
                this.setState({ move_day: '' });
            }
            return;
        }
            
        const formData = new FormData(this.form);
        formData.append('userfile', this.state.file);
        formData.append('animal_type_id', this.state.animal_type_id);
        formData.append('animal_code', this.state.animal_code);
        formData.append('kind', this.state.kind);
        formData.append('weight', this.state.weight);
        formData.append('birth_day', this.state.birth_day);
        formData.append('move_day', this.state.move_day);
        formData.append('health_state', this.state.health_state);
        formData.append('no', this.state.no);
        formData.append('mate_count', this.state.mate_count);
        formData.append('barcode', this.state.barcode);

        const config = {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
        };

        axios.post("/api/v1/animals", formData, config)
            .then((response) => {
                if (response.status === 200) {
                    if (response.data.data == "0522") {
                        _this.props.alertError(response.status, response.data);
                    } else {
                        _this.props.showDialog({
                            alertText: '개체가 등록 되었습니다.',
                            cancelText: "확인",
                            cancelCallback: () => {
                                // window.location.reload();
                                this.close();
                            }
                        })
                    }
                } else {
                    _this.props.alertError("missing_data", null);
                }
            }).catch((error) => {
                _this.props.alertError("missing_data", null);
            });
    };

    _handleImageChange = e => {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];
        if (file.name.indexOf(".png") != -1 || file.name.indexOf(".jpg") != -1 || file.name.indexOf(".jpeg") != -1) {
            reader.onloadend = () => {
                this.setState({ file: file, imagePreviewUrl: reader.result });
            };
            reader.readAsDataURL(file);
        } else {
            this.props.showDialog({
                alertText: '알맞은 이미지파일 \n(.png .jpg .jpeg)가 \n 아닙니다.',
                cancelText: "확인",
                cancelCallback: () => {
                    document.getElementById("upload").value = "";
                }
            })
        }

    };

    _cancelImg = e => {
        e.preventDefault();
        this.setState({ imagePreviewUrl: '', file: '', })
    }

    render() {
        return (
            <article id="lcModalEntityCreateWrap"
                className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalEntityCreate" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">개체 등록</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>

                                <section className="modal-body">
                                    <div className="entity-create">
                                        <label className="entity-label">개체 ID</label>
                                        <input type="text" className="animal-input-box entity-id" maxLength="20" pattern="[0-9]*" value={this.state.animal_code} onChange={e => this._handleInput(e, 'animal_code')} />
                                    </div>
                                    <div className="entity-create">
                                        <div className="entity-create-detail pr-6  pd-20">
                                            <label className="entity-label">품종</label>
                                            <select className="animal-select-box" value={this.state.kind} onChange={e => this._handleInput(e, 'kind')}>
                                                <option value="" defaultValue disabled hidden>선택</option>
                                                {
                                                    this.state.kinds && this.state.kinds.rows && this.state.kinds.rows.length ?
                                                        (
                                                            this.state.kinds.rows.map((info, index) => {
                                                                return (
                                                                    <option key={index} value={info.name}>{info.name}</option>
                                                                )
                                                            })
                                                        ) : null
                                                }
                                            </select>
                                        </div>
                                        <div className="entity-create-detail pl-6 pb-20">
                                            <label className="entity-label">체중(kg)</label>
                                            <input type="text" className="animal-input-box" value={this.state.weight} maxLength="3" onChange={e => this._handleInput(e, 'weight')} />
                                        </div>
                                        <div className="entity-create-detail pr-6  pb-20">
                                            <label className="entity-label">출생일</label>
                                            <input type="text" name="birth_day" className="animal-input-box" maxLength="10" value={this.state.birth_day} onChange={e => this._handleInput(e, 'birth_day')} />

                                        </div>
                                        <div className="entity-create-detail pl-6  pb-20">
                                            <label className="entity-label">전입일</label>
                                            <input type="text" name="move_day" className="animal-input-box" maxLength="10" value={this.state.move_day} onChange={e => this._handleInput(e, 'move_day')} />
                                        </div>
                                        <div className="entity-create-detail pr-6">
                                            <label className="entity-label">건강 상태</label>
                                            <select className="animal-select-box" value={this.state.health_state} onChange={e => this._handleInput(e, 'health_state')}>
                                                <option value="" defaultValue disabled hidden>선택</option>
                                                <option value="good">좋음</option>
                                                <option value="normal">보통</option>
                                                <option value="bad">나쁨</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="entity-create">
                                        <div className="entity-create-detail pr-6">
                                            <div className="select-input-label">
                                                <label className="entity-label">모우(돈) ID</label>
                                                <label className="entity-select-label">(선택입력)</label>
                                            </div>
                                            <input type="text" className="animal-input-box" maxLength="20" value={this.state.no} onChange={e => this._handleInput(e, 'no')} />
                                        </div>

                                        <div className="entity-create-detail pl-6">
                                            <div className="select-input-label">
                                                <label className="entity-label">바코드 입력</label>
                                                <label className="entity-select-label">(선택 입력)</label>
                                            </div>
                                            <input type="text" className="animal-input-box" maxLength="20" value={this.state.barcode} onChange={e => this._handleInput(e, 'barcode')} />
                                        </div>
                                    </div>

                                    <form onSubmit={this._handleSubmit}>
                                        <div className="entity-create entity-image">
                                            <div className="entity-image-area mr-12">
                                                {
                                                    this.state.imagePreviewUrl != '' ?
                                                        (
                                                            <img src={this.state.imagePreviewUrl} />
                                                        ) : (
                                                            <img src={this.state.default_type} />
                                                        )
                                                }
                                                {/* {!this.state.imagePreview && <img src={this.state.imagePreviewUrl} />} */}

                                            </div>
                                            <div className="entity-image-btn">
                                                <input
                                                    type="file"
                                                    name="userfile"
                                                    onChange={this._handleImageChange}
                                                    className="km-btn-file"
                                                    id="upload"
                                                />
                                                <button type="submit" className="btn-line">개체 사진 등록</button>
                                                <button className="btn-line" onClick={this._cancelImg}>개체 사진 삭제</button>
                                                <br></br>
                                                <label className="img-label">{this.state.file.name}</label>
                                            </div>
                                        </div>
                                        <div className="entity-create btn-area">
                                            <button className="btn-line mr-6" onClick={this.close}>취소</button>
                                            <button type="submit" className="btn-green ml-6" >등록</button>
                                        </div>
                                    </form>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
        alertTextError: actions.alertTextError,
    })
)(useEntityCreate(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalEntityCreate));