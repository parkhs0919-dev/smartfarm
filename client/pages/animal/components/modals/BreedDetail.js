import React, { Component } from 'react';
import { useBreedDetail } from "../../contexts/modals/breedDetail";
import { useBreedCreate } from "../../contexts/modals/breedCreate";
import { useBreedModify } from "../../contexts/modals/breedModify";
import { useAlert } from "../../contexts/alert";
import moment from 'moment';
import entityManager from '../../managers/entity';

class ModalBreedDetail extends Component {   
    constructor(props) {
        super(props);
        this.state = {
            breedDate: {
                animalBreeds: [],
                data: [],
            },
            nowM: moment(new Date()).format('MM'),
            nowY: moment(new Date()).format('YYYY'),
        };
        // breedingIndex =======
        // estrus:1,       발정
        // insemination:2, 수정
        // check:3,        임신감정
        // dryUp:4,        건유
        // birth:5,        분만
        //======================
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        this.setState({
            animal_id: data.id,
            animal_code: data.animal_code,
            nowM: moment(new Date()).format('MM'),
            nowY: moment(new Date()).format('YYYY'),
        });
        this._findBreeds(data.id, this.state.nowY, this.state.nowM);
        document.addEventListener('keyup', this.closeOnEscape);        
    };

    close = (e) => {
        this._reset();
        if (e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.callback && this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    disableMotherModalClose = () => {
        document.removeEventListener('keyup', this.closeOnEscape);
    };

    enableMotherModalClose = () => {
        this._findBreeds(this.state.animal_id, this.state.nowY, this.state.nowM);
        document.addEventListener('keyup', this.closeOnEscape);
    };

    _openBreedCreate = () => {
        this.props.openBreedCreateModal(this.state.animal_id,this.enableMotherModalClose);
    };

    _openBreedModify = (e,data) => {
        this.disableMotherModalClose();
        this.props.openBreedModifyModal(data,this.enableMotherModalClose);
    };

    _reset=()=>{
        const states = {
            breedDate: {
                animalBreeds: [],
                data: [],
            },
            nowM: moment(new Date()).format('MM'),
            nowY: moment(new Date()).format('YYYY'),
        };
        this.setState(states);    
    }

    _findBreeds = (id, y, m) => {        
        const _this = this;

        this.setState({
            breedDate: {
                animalBreeds: [],
                data: [],
            },
        });
        let query = {
            date: y + '-' + m,
        };

        entityManager.get_breeds(id, query, (status, data) => {
            if (status === 200) {
                _this.setState({
                    breedDate: data.data
                });
            } else if (status === 404) {
            } else {
                _this.setState({
                    breedDate: null
                });
            }
        });
    };

    _createCalendar = (y, m) => {
        let nowY = moment(new Date()).format('YYYY'); //현재 연도
        let nowM = moment(new Date()).format('MM'); //현재 월

        y = (y != undefined) ? y : nowY;
        m = (m != undefined) ? m - 1 : nowM;

        y = parseInt(y);
        m = parseInt(m);

        var theDate = new Date(y, m, 1);
        var theDay = theDate.getDay();
        let last = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if (y % 4 == 0 && y % 100 != 0 || y % 400 == 0) lastDate = last[1] = 29;

        var lastDate = last[m]; //현재 월에 마지막이 몇일인지 구합니다.

        let row = Math.ceil((theDay + lastDate) / 7);
        let dNum = 1;
        let table = []

        for (let i = 1; i <= row; i++) {
            let children = []
            for (let k = 1; k <= 7; k++) {
                if (i == 1 && k <= theDay || dNum > lastDate) {
                    children.push(<td key={k}></td>)
                } else {
                    children.push(
                        <td key={k}>
                            <div className="lc-date-wrapper" >
                                <span className="date-span" >{dNum}</span>
                                {
                                    this.state.breedDate.data != null ? this._checkBreed(dNum, k) : null
                                }
                            </div>
                        </td>
                    )
                    dNum++;
                }
            }
            table.push(<tr key={i}>{children}</tr>)
        }
        return table
    }

    _checkBreed = (day, k) => {
        let breed = [];
        let date = '';

        if(day < 10)
            date = this.state.nowY+this.state.nowM+'0'+day;
        else
            date = this.state.nowY+this.state.nowM+day;

        date = moment(date).format('YYYY-MM-DD');

        if (this.state.breedDate.data.map(x => x.date).indexOf(date) != -1) {
            const breedtype = this.state.breedDate.data[this.state.breedDate.data.map(x => x.date).indexOf(date)];

            if (breedtype.estrus == 1) {
                breed.push(<div className="date-dot dot-red" key={k} />);
            }
            if (breedtype.check == 1) {
                breed.push(<div className="date-dot dot-blue" key={k + 2} />);
            }
            if (breedtype.birth == 1) {
                breed.push(<div className="date-dot dot-green" key={k + 3} />);
            }
            if (breedtype.dryUp == 1) {
                breed.push(<div className="date-dot dot-yellow" key={k + 4} />);
            }
            if (breedtype.insemination == 1) {
                breed.push(<div className="date-dot dot-orange" key={k + 5} />);
            }
        } else {
            breed = [];
        }
        
        return breed
    }

    _previousM = () => {
        let m = parseInt(this.state.nowM);
        let y = parseInt(this.state.nowY);

        if (m == 1) {
            y = String(y - 1);
            m = '12'
            this.setState({
                nowY: y,
                nowM: m
            })
        } else {
            if (m - 1 < 10) {
                m = "0" + String(m - 1);
            } else {
                m = String(m - 1);
            }
            this.setState({
                nowM: m,
            })
        }
        this._findBreeds(this.state.animal_id, y, m);

    }

    _nextM = () => {
        let m = parseInt(this.state.nowM);
        let y = parseInt(this.state.nowY);

        if (m == 12) {
            y = String(y + 1);
            m = '01'
            this.setState({
                nowY: y,
                nowM: m
            })
        } else {
            if (m + 1 < 10) {
                m = "0" + String(m + 1);
            } else {
                m = String(m + 1);
            }
            this.setState({
                nowM: m,
            })
        }
        this._findBreeds(this.state.animal_id, y, m);
    }

    _makeTypeName = (type) => {
        let result = '';
        // estrus:1,       발정
        // insemination:2, 수정
        // check:3,        임신감정
        // dryUp:4,        건유
        // birth:5,        분만
        switch (type) {
            case "estrus":
                result = "발정"
                break;
            case "insemination":
                result = "수정"
                break;
            case "check":
                result = "임신감정"
                break;
            case "dryUp":
                result = "건유"
                break;
            case "birth":
                result = "분만"
                break;
            default:
                result = null;
                break;
        }
        return result;

    }

    _makeTypeColor=(type)=>{
        return "status-color-"+type;
    }

    _removeBreed =(e,id)=>{
        const _this = this;
        _this.props.showDialog({
            alertText: '이 번식정보를 삭제하시겠습니까?',
            cancelText: '아니오',
            submitText: '예',
            submitCallback: () => {
                entityManager.remove_breed(id, (status, data) => {
                    if(status === 200) {
                        _this.props.showDialog({
                            alertText: '번식정보가 삭제되었습니다.',
                            cancelText: "확인",
                            cancelCallback: () => {
                                this._findBreeds(this.state.animal_id, this.state.nowY, this.state.nowM);
                                
                            }
                        })
                    } else {
                        _this.props.alertError(status, data);
                    }
                });
            }
        });       
    }   

    render() {
        return (
            <article id="lcModalBreedDetailWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')}
                tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        {this.props.isVisible ?
                            <div id="lcModalBreedDetail" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">번식상세</h3>
                                    <button type="button"
                                        className="lc-modal-close"
                                        onClick={this.close} />
                                </section>

                                <section className="modal-body">
                                    <div className="breed-title">
                                        <div className="breed-title-id">
                                            <h3>
                                                {this.state.animal_code}
                                            </h3>
                                        </div>

                                        <div className="breed-title-month pc">
                                            <span className="previous">
                                                <button onClick={this._previousM}>
                                                    <img src="/public/images/commons/btn-previous@2x.png" />
                                                </button>
                                            </span>
                                            <span className="date">{this.state.nowY}년 {this.state.nowM}월</span>
                                            <span className="next">
                                                <button onClick={this._nextM}>
                                                    <img src="/public/images/commons/btn-next@2x.png" />
                                                </button>
                                            </span>
                                        </div>

                                        <div className="breed-title-open-modal">
                                            <button className="btn-green" onClick={this._openBreedCreate}>번식추가</button>
                                        </div>
                                    </div>

                                    <div className="breed-title-month mobile">
                                        <span className="previous">
                                            <button onClick={this._previousM}>
                                                <img src="/public/images/commons/btn-previous@2x.png" />
                                            </button>
                                        </span>
                                        <span className="date">{this.state.nowY}년 {this.state.nowM}월</span>
                                        <span className="next">
                                            <button onClick={this._nextM}>
                                                <img src="/public/images/commons/btn-next@2x.png" />
                                            </button>
                                        </span>
                                    </div>

                                    <div className="breed-calendar-wrap">
                                        <table id="breedCalendar">
                                            <thead>
                                                <tr>
                                                    <th>일</th>
                                                    <th>월</th>
                                                    <th>화</th>
                                                    <th>수</th>
                                                    <th>목</th>
                                                    <th>금</th>
                                                    <th>토</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this._createCalendar(this.state.nowY, this.state.nowM)}
                                            </tbody>
                                        </table>
                                    </div>
                                    {
                                        this.state.breedDate && this.state.breedDate.animalBreeds && this.state.breedDate.animalBreeds.length ?
                                            (
                                                this.state.breedDate.animalBreeds.map((info, index) => {
                                                    return (
                                                        <div className="breed-content" key={index}>
                                                            <div className="content-list">
                                                                <ul>
                                                                    <li>
                                                                        <div className="list-title">
                                                                            <span className={this._makeTypeColor(info.type)}></span>
                                                                            <span className="status-txt">{this._makeTypeName(info.type)}</span>
                                                                            <span className="status-date">{moment(info.start_date).format('YYYY. MM. DD')}</span>
                                                                            <span className="status-date"> ~ {moment(info.end_date).format('YYYY. MM. DD')}</span>
                                                                            <div className="status-btn">
                                                                                <button onClick={e=> this._openBreedModify(e,info)}>수정</button>
                                                                                &nbsp;&nbsp;&nbsp;
                                                                                <button onClick={e=> this._removeBreed(e,info.id)}>삭제</button>
                                                                            </div>
                                                                        </div>
                                                                        <div className="list-content">
                                                                            <p>
                                                                                {
                                                                                    info.memo != '' && info.memo != null ?
                                                                                    (
                                                                                        info.memo
                                                                                    ) : "메모가 없습니다."
                                                                                }
                                                                            </p>
                                                                        </div>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    )
                                                })
                                            ) :
                                            (
                                                <div className="no-data-wrap">내역이 없습니다.</div>
                                            )
                                    }                                   
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useBreedModify(
    ({ actions }) => ({
        openBreedModifyModal: actions.open,
    })
)(useBreedCreate(
    ({ actions }) => ({
        openBreedCreateModal: actions.open
    })
)(useAlert(
    ({ actions }) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useBreedDetail(
    ({ state, actions }) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalBreedDetail))));