import React, {Component} from 'react';
import {useBreedModify} from "../../contexts/modals/breedModify";
import {useAlert} from "../../contexts/alert";
import entityManager from '../../managers/entity';
import moment from "moment";

class ModalBreedModify extends Component {
    constructor(props) {
        super(props);  
        this.state = {
            start_date: '',
            end_date: '',
            type:'',
            memo:'',
        };

    }
    _reset(){
        const states = {
            start_date: '',
            end_date: '',
            type:'',
            memo:'',
        };
        this.setState(states);    
    }   

    componentDidMount() {
        this.props.sync(this.init);
    }    

    componentWillUnmount() {
        document.removeEventListener('keyup', this.closeOnEscape);
    }

    init = (data) => {
        if(data.memo == null)
            data.memo = '';
        data.start_date = moment(data.start_date).format('YYYY-MM-DD');
        data.end_date = moment(data.end_date).format('YYYY-MM-DD');

        this.setState(data);
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        this._reset();
        this.props.callback && this.props.callback();
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    _handleInput = (e, key) => {
        let item = {};
        const _this = this;

        if(key == "memo"){
            if(e.target.value.length < 100){
                item[key] = e.target.value;
            }
        }else if(key == "start_date" || key == "end_date"){
            if(e.target.value.length == 8){
                if(moment(e.target.value).format('YYYY-MM-DD') == "Invalid date"){
                    _this.props.alertError("Invalid date", null);
                    if(key =="start_date")
                        this.setState({start_date : ''});
                    else
                        this.setState({end_date : ''});
                }else{
                    var temp = moment(e.target.value).format('YYYY-MM-DD');
                    e.target.value= temp;
                    item[key] = e.target.value
                }
            }else if(e.target.value.length < 10){
                item[key] = e.target.value.replace(/\D/,'').replace(/\D/,'');
            }
        }
        else{
            item[key] = e.target.value;
        }
        this.setState(item);
    }; 

    _checkCalendarValue=()=>{
        if(this.state.start_date.length != 10)
            return "start_date";
        else if(this.state.end_date.length != 10)
            return "end_date";        
        else if(this.state.start_date > this.state.end_date)
            return "wrong_start"
        else
            return true;
    }

    _updateBreed = () =>{
        const _this = this;
        let result = this._checkCalendarValue();
        if(result != true){
            if(result == "start_date"){
                _this.props.alertError("date_error",null);
                this.setState({start_date : ''});
            }
            else if(result == "end_date"){
                _this.props.alertError("date_error",null);
                this.setState({end_date : ''});
            }
            else if(result == "wrong_start"){
                _this.props.alertError("wrong_start",null);
                this.setState({end_date : ''});
            }
            return;
        }else{
            entityManager.update_breed(this.state, (status, data) => {
                if (status === 200) {
                    _this.props.showDialog({
                        alertText: '번식이 수정 되었습니다.',
                        cancelText: "확인",
                        cancelCallback: () => {
                            this.close();
                        }
                    })
                } else if (status === 404) {
                    _this.props.alertError(status, data);
                } else {
                    _this.props.alertError(status, data);
                }
            });
        }
    }

    render() {
        return (
            <article id="lcModalBreedCreateWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalBreedCreate" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">번식수정</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div className="control-box">
                                        <div className="breed-creat-date">
                                            <label className="control-title">시작일</label>
                                            <input className="breed-creat-box" type="text"  maxLength="10" value={this.state.start_date} onChange={e => this._handleInput(e, 'start_date')}/>
                                        </div>

                                        <div className="breed-creat-date">
                                            <label className="control-title">종료일</label>
                                            <input className="breed-creat-box" type="text"  maxLength="10" value={this.state.end_date} onChange={e => this._handleInput(e, 'end_date')}/>
                                        </div>

                                        <div className="control-inline-box">
                                            <label className="control-title">번식</label>
                                            <select className="animal-select-box" value={this.state.type} onChange={e => this._handleInput(e, 'type')}>
                                                <option value="" defaultValue disabled hidden>선택</option>
                                                <option value="estrus">발정</option>
                                                <option value="insemination">수정</option>
                                                <option value="check">임신감정</option>
                                                <option value="dryUp">건유</option>
                                                <option value="birth">분만</option>
                                            </select>
                                        </div>
                                        <div className="control-inline-box">
                                            <label className="control-title">메모</label>
                                            <textarea className="control-textarea" placeholder="텍스트 입력(최대 100자)" value={this.state.memo} onChange={e => this._handleInput(e, 'memo')}></textarea>
                                        </div>
                                    </div>

                                    <button className="btn-green" onClick={this._updateBreed}>입력</button>

                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useBreedModify(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalBreedModify));