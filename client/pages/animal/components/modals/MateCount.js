import React, {Component} from 'react';
import {useMateCount} from "../../contexts/modals/mateCount";
import {useAlert} from "../../contexts/alert";
import entityManager from '../../managers/entity';
import moment from "moment";

class ModalMateCount extends Component {
    constructor(props) {
        super(props);

        this.state = {
            animal_id:'',
            date:'',            
            memo:'',
        };
    }

    componentDidMount() {
        this.props.sync(this.init);
    }

    init = (data) => {
        const states = {
            animal_id: data,
            date:'',            
            memo:'',
        };
        this.setState(states);
        document.addEventListener('keyup', this.closeOnEscape);
    };

    close = (e) => {
        if(e) e.preventDefault();
        document.removeEventListener('keyup', this.closeOnEscape);
        this.props.close();
    };

    closeOnEscape = (e) => {
        if (e.keyCode === 27) this.close();
    };

    _createMateCount = e =>{ 
        e.preventDefault();
        const _this = this;
        if(this._checkCalendarValue() == false){
            _this.props.alertError("date_error",null);
            this.setState({date : ''});
            return;
        }
        entityManager.mate_create(this.state, (status, data) => {
            if(status === 200) {               
                _this.props.showDialog({
                    alertText: '교배차수가 등록 되었습니다.',
                    cancelText: "확인",
                    cancelCallback: () => {
                        window.location.reload();
                    }
                })
            } else {
                _this.props.alertError(status, data);
            }
        });
    }
    
    _handleInput = (e, key) => {
        let item = {};
        if(key == "memo"){
            if(e.target.value.length < 100){
                item[key] = e.target.value;
            }
        }else{
            item[key] = e.target.value;
        }
        this.setState(item);
    };

    _handleCalendarInput = (e, key) => {
        const _this = this;
        let item = {};
        if(e.target.value.length == 8){
            if(moment(e.target.value).format('YYYY-MM-DD') == "Invalid date"){
                _this.props.alertError("Invalid date", null);
                this.setState({date : ''});
            }else{
                var temp = moment(e.target.value).format('YYYY-MM-DD');
                e.target.value= temp;
                item[key] = e.target.value
            }
        }else if(e.target.value.length < 10){
            item[key] = e.target.value.replace(/\D/,'').replace(/\D/,'');
        }
        this.setState(item);
    };

    _checkCalendarValue=()=>{
        if(this.state.date.length == 10) return true;
        else return false;
    }
    render() {
        return (
            <article id="lcModalMateCountWrap" className={'modal-container' + (this.props.isVisible ? ' active' : '')} tabIndex="0">
                <div className="vertical-align-wrap">
                    <div className="vertical-align">
                        { this.props.isVisible ?
                            <div id="lcModalMateCount" className="modal-wrap">
                                <section className="modal-header">
                                    <h3 className="lc-modal-title">교배차수입력</h3>
                                    <button type="button"
                                            className="lc-modal-close"
                                            onClick={this.close}/>
                                </section>

                                <section className="modal-body">
                                    <div className="control-box">
                                        <label className="control-info-label">날짜</label>
                                        <input className="control-input-box" type="text" maxLength="10" value={this.state.date} onChange={e => this._handleCalendarInput(e, 'date')}/>
                                    </div>
                                    <div className="control-box last">
                                        <label className="control-info-label">메모</label>
                                        <textarea className="control-textarea" placeholder="텍스트 입력(최대 100자)" value={this.state.memo} onChange={e => this._handleInput(e, 'memo')}></textarea>
                                    </div>

                                    <button className="btn-green" onClick={this._createMateCount}>입력</button>
                                </section>
                            </div>
                            : null
                        }
                    </div>
                </div>
            </article>
        )
    }
}

export default useAlert(
    ({actions}) => ({
        showDialog: actions.show,
        alertError: actions.alertError,
    })
)(useMateCount(
    ({state, actions}) => ({
        isVisible: state.isVisible,
        callback: state.callback,
        sync: actions.sync,
        close: actions.close
    })
)(ModalMateCount));