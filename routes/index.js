const CONFIG = require('../server/config');

module.exports = (app) => {
    app.use(async (ctx, next) => {
        if (!ctx.user) {
            if (ctx.request.ip.indexOf('192.168') === -1) {
                ctx.throw(403);
            } else {
                /**
                 * todo generate cookie with token
                 */
                ctx.user = Object.assign({}, CONFIG.defaultUser);
            }
        }

        return next();
    });

    require('./farm')(app);
    require('./animal')(app);
    require('./redirect')(app);
};
