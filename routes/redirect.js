const path = require('path');
const Router = require('koa-router');
const router = new Router();

module.exports = (app) => {
    router.get('/', render);
    router.get('/*', render);

    app.use(router.routes(), router.allowedMethods());
};

async function render(ctx) {
    if (ctx.query && ctx.query.type) {
        let redirectUrl = `/${ctx.query.type}`;
        if (ctx.query['modal']) redirectUrl += `?modal=${ctx.query['modal']}`;
        ctx.redirect(redirectUrl);
    } else {
        ctx.throw(404);
        // await ctx.render(`${fileName}-render`, {});
    }
}
