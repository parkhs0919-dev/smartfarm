const path = require('path');
const Router = require('koa-router');
const router = new Router();
const CONFIG = require('../server/config');
const REFRESH = require('../server/methods/refresh');

const fileName = path.basename(__filename).split('.')[0];

const loadBase = require('../server/methods/load-base').animal;

module.exports = (app) => {
    if (CONFIG.animal.license) {
        router.get(`/${fileName}/`, render);
        router.get(`/${fileName}/*`, render);

        app.use(router.routes(), router.allowedMethods());
    }
};

async function render(ctx) {
    await loadBase(ctx);
    await ctx.render(`${fileName}-render`, {
        current: REFRESH.getCurrent(),
        erpHost: CONFIG.erp.host,
        user: ctx.user,
    });
}
