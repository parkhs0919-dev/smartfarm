const path = require('path');
const Router = require('koa-router');
const router = new Router();
const CONFIG = require('../server/config');
const REFRESH = require('../server/methods/refresh');
const getIpAddress = require('../server/cron/ip-refresh').getIpAddress;
     
const fileName = path.basename(__filename).split('.')[0];

const loadBase = require('../server/methods/load-base').farm;

module.exports = (app) => {
    if (CONFIG.farm.license) {
        router.get(`/${fileName}/`, render);
        router.get(`/${fileName}/*`, render);

        app.use(router.routes(), router.allowedMethods());
    }
};

async function render(ctx) {
    await loadBase(ctx);
    await ctx.render(`${fileName}-render`, {
        current: REFRESH.getCurrent(),
        erpHost: CONFIG.erp.host,
        user: ctx.user,
        screens: ctx.screens,
        currentAutoCompleteTime: ctx.currentAutoCompleteTime,
        // cctvSetting: ctx.cctvSetting,
        mode: ctx.mode,
        houses: ctx.houses,
        controlCount: ctx.controlCount,
        controls: ctx.controls,
        fluidHost: (CONFIG.fluid.screenIp ? CONFIG.fluid.screenIp : await getIpAddress()) + ':' + CONFIG.fluid.screenPort,
        // cctvCount: ctx.cctvCount,
        // cctvs: ctx.cctvs,
        // motorCount: ctx.motorCount,
        // motors: ctx.motors,
        // powerCount: ctx.powerCount,
        // powers: ctx.powers,
        // innerAlarmCount: ctx.innerAlarmCount,
        // innerAlarms: ctx.innerAlarms,
        // outerAlarmCount: ctx.outerAlarmCount,
        // outerAlarms: ctx.outerAlarms,
        // innerSensorCount: ctx.innerSensorCount,
        // innerSensors: ctx.innerSensors,
        // outerSensorCount: ctx.outerSensorCount,
        // outerSensors: ctx.outerSensors,
        sensorMainChart: ctx.sensorMainChart,
        userInfo: ctx.userInfo,
    });   
}   
